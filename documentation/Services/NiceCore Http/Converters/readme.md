﻿# Converters
In the channel system the way HttpContent is converted from/to is defined by the converters.
A converter is strictly assigned to a specific content type. This content type is drawn either as method parameter
when it is about the data that should be sent or from the content type header of the response.
Converters are divided into 2 categories:
- IPayloadConverter (converter handling the creation of httpcontent objects for the payload to be sent from the channel to the endpoint)
- IResponseConverter (converter handling the creation of the TReturn typed object from the response message that is returned after a call has been completed.) 
Currently only the content type ContentTypes.APP_JSON has defined converters. Any other converters you have to currently add by your self.

Here are the JSON converters as an example:
```csharp
/// <summary>
/// IPayloadConverter impl for JSON
/// </summary>
[Register(InterfaceType = typeof(IPayloadConverter))] //This is very important, any converter needs to be registered with the ServiceContainer to be picked up
public class JSONPayloadConverter : IPayloadConverter
{
  /// <summary>
  /// Content Type app/json. This property is a must on any converter
  /// </summary>
  public string ContentType => ContentTypes.APP_JSON;

  /// <summary>
  /// To StringContent
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <param name="i_Payload"></param>
  /// <returns></returns>
  public Task<HttpContent> ToContent<T>(T i_Payload)
  {
    return Task.FromResult(new StringContent(JSONSerialization.Serialize(i_Payload)) as HttpContent);
  }

  /// <summary>
  /// To StringContent not generic
  /// </summary>
  /// <param name="i_Payload"></param>
  /// <param name="i_Type"></param>
  /// <returns></returns>
  public Task<HttpContent> ToContent(object i_Payload, Type i_Type)
  {
    return Task.FromResult(new StringContent(JSONSerialization.Serialize(i_Payload, i_Type)) as HttpContent);
  }
}

/// <summary>
/// IResponseConverter impl dealing with app/json
/// </summary>
[Register(InterfaceType = typeof(IResponseConverter))]
public class JSONResponseConverter : IResponseConverter
{
  /// <summary>
  /// APP/Json This property is a must on any converter
  /// </summary>
  public string ContentType => ContentTypes.APP_JSON;

  /// <summary>
  /// Converts the content of i_Response to an instance of i_Type
  /// </summary>
  /// <param name="i_Response"></param>
  /// <param name="i_Type"></param>
  /// <returns></returns>
  public Task<object> ConvertObjectValue(HttpResponseMessage i_Response, Type i_Type)
  {
    return i_Response.Content.ReadFromJsonAsync(i_Type);
  }

  /// <summary>
  /// Converts the content of i_Response to an instance of T
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <param name="i_Response"></param>
  /// <returns></returns>
  public Task<T> ConvertValue<T>(HttpResponseMessage i_Response)
  {
    return i_Response.Content.ReadFromJsonAsync<T>();
  }
}
```
