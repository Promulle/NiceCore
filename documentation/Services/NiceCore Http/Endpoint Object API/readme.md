﻿# Endpoint Object Channel API
A lot of the send methods need a lot of parameters. To remedy this a bit NiceCore also provides the ability to pass endpoint objects which hold the relevant data.

Interfaces for these endpoint objects:

- IEndpointRequestInformation (the base interface for all. relevant for endpoints that do not take any parameters and do not return any value.)
- IEndpointReturningRequestInformation\<TReturn\> (Interface for endpoints that do not take any parameters and return something that is TReturn)
- IEndpointBodyRequestInformation\<TPayload\> (Interface for endpoints that send a value as body and return nothing)
- IEndpointReturningBodyRequestInformation\<TReturn, TPayload\> (Interface for endpoints that send a value as body and return something of type TReturn)

Most Extension methods have an overload that take such objects. Now on how to use them:
1. Define the endpoint you want to call as new class
```csharp
/// <summary>
/// Example impl, you can of course make this easier using base classes
/// e.x. Base class that defines the content type as app json and no funcs
/// And then you just create a new endpoint based on that which just defines the endpoint.
/// </summary>
public class ExamplePostEndpoint : BaseEndpointBodyRequestInformation<ExampleInstance>
{
  /// <summary>
  /// Content Type
  /// </summary>
  public override string ContentType { get; } = ContentTypes.APP_JSON;

  /// <summary>
  /// Paylaod
  /// </summary>
  public override ExampleInstance Payload { get; init; }

  /// <summary>
  /// Endpoint
  /// </summary>
  public override string Endpoint { get; } = POST_ENDPOINT;

  /// <summary>
  /// Header Func
  /// </summary>
  public override Func<HttpRequestHeaders, Task> HeaderFunc { get; }

  /// <summary>
  /// Auth func
  /// </summary>
  public override Func<Task<AuthenticationHeaderValue>> AuthenticationHeaderFunc { get; }

  /// <summary>
  /// Create
  /// </summary>
  /// <param name="i_Payload"></param>
  /// <returns></returns>
  public static ExamplePostEndpoint Create(ExampleInstance i_Payload)
  {
    return new ExamplePostEndpoint()
    {
      Payload = i_Payload
    };
  }
}
```
2. Use a new Instance when calling the channel
```csharp
await channel.Post(ExamplePostEndpoint.Create(new ExampleInstance()));
```

But now this brings another problem: What about dynamic endpoints for queries or other values in the url?
There is IUrlExtender for that.
 
## IUrlExtender
IUrlExtender is an interface designed to extend the urls given to endpoint object calls.
Any Send-method that takes a endpoint object as parameter also has an overload to accept an instance that implements IUrlExtender.
The interface:
```csharp
/// <summary>
/// Interface for extenders of Endpoint urls
/// </summary>
public interface IUrlExtender
{
  /// <summary>
  /// Extends i_OriginalEndpoint with the data set for this extender
  /// </summary>
  /// <param name="i_OriginalEndpoint"></param>
  /// <returns></returns>
  string Extend(string i_OriginalEndpoint);
}
```
NiceCore brings 2 default implementations for this interface:
- SimpleUrlExtender (adds all values in the format of "/\{value}")
- QueryExtender (adds all values in the format ?\{key}=\{value}&\{anyOtherKey}=\{anyOtherValue})

Using this you can call the endpoint the following way (for Simple):
```csharp
await channel.Get(new MyGetEndpoint(), UrlExtensions.Simple("5")) //effectively calls "{MyGetEndpoint.Endpoint}/5"
```
Implementing your own extender is quite straightforward -> just return whatever you want to be the result that is called