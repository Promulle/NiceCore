﻿# Http Support
NiceCore provides a convenience wrapper around HttpClient, that can also be integrated into NiceCore's dependency injection.
NcHttpChannels.
To use the channel you should first take a look at the base interface for all channels: INcHttpChannel.
```csharp
/// <summary>
/// Channel for interacting with endpoints
/// </summary>
public interface INcHttpChannel : IDisposable
{
  /// <summary>
  /// Sends a request of method i_method to the endpoint specified by i_Endpoint, using the content type i_ContentType and i_Payload as content.
  /// Can be authenticated by the auth header returned by i_AuthenticationHeaderFunc
  /// </summary>
  /// <typeparam name="TReturn">Type of the expected return object</typeparam>
  /// <typeparam name="TPayload">Type of the object sent as body</typeparam>
  /// <param name="i_Method">HttpMethod to be used</param>
  /// <param name="i_Endpoint">Endpoint to call</param>
  /// <param name="i_ContentType">Type of content to be used for encoding the body</param>
  /// <param name="i_Payload">Object that is used as content</param>
  /// <param name="i_HeaderFunc">Function that can be used to add additional headers for this request. may be null.</param>
  /// <param name="i_AuthenticationHeaderFunc">Function that provides the authentication for the request. may be null.</param>
  /// <returns></returns>
  Task<TReturn> Send<TReturn, TPayload>(HttpMethod i_Method, string i_Endpoint, string i_ContentType, TPayload i_Payload, Func<HttpRequestHeaders, Task> i_HeaderFunc, Func<Task<AuthenticationHeaderValue>> i_AuthenticationHeaderFunc);

  /// <summary>
  /// Sends a request of method i_method to the endpoint specified by i_Endpoint without content
  /// Can be authenticated by the auth header returned by i_AuthenticationHeaderFunc
  /// </summary>
  /// <typeparam name="TReturn"></typeparam>
  /// <param name="i_Method"></param>
  /// <param name="i_Endpoint"></param>
  /// <param name="i_HeaderFunc">Function that can be used to add additional headers for this request. may be null.</param>
  /// <param name="i_AuthenticationHeaderFunc">Function that provides the authentication for the request. may be null.</param>
  /// <returns></returns>
  Task<TReturn> Send<TReturn>(HttpMethod i_Method, string i_Endpoint, Func<HttpRequestHeaders, Task> i_HeaderFunc, Func<Task<AuthenticationHeaderValue>> i_AuthenticationHeaderFunc);

  /// <summary>
  /// Sends a request of method i_Method to i_Endpoint. Without authentication and content
  /// </summary>
  /// <typeparam name="TReturn"></typeparam>
  /// <param name="i_Method"></param>
  /// <param name="i_Endpoint"></param>
  /// <returns></returns>
  Task<TReturn> Send<TReturn>(HttpMethod i_Method, string i_Endpoint);
}
```
These are only the base methods. In the namespace NiceCore.Services.Http.Channels.Extensions you will find
extension methods for POST, GET, DELETE and PUT.

To use a channel you have to first create your own implementation that implements INcHttpChannel.
To make this a lot easier there is the BaseNcHttpChannel class. A small example:
```csharp
/// <summary>
/// Example implementation of a channel
/// </summary>
public class ExampleHttpChannel : BaseNcHttpChannel
{
  /// <summary>
  /// Configure the client that is used by this channel
  /// </summary>
  /// <param name="t_Client">Instance of the client to be used</param>
  protected override void ConfigureClient(HttpClient t_Client)
  {
    t_Client.BaseAddress = new Uri("http://127.0.0.1:8080/");
  }
}
```
The only thing you have to do in your channel is to provide an implementation for the abstract method "ConfigureClient". This method will be called the first time (and only on the first time) a call is made using the channel
You can now create/inject this implementation everywhere you need a channel.


NOTE: Channels can be safely created multiple times and safely disposed since the underlying HttpClientHandler is shared between all instances of HttpClient (there is one instance of this per channel).
This is only true for the default implementation of IHttpClientHandlerService that NiceCore.Services.Defaults provides. This might be different for other implementations.

Example usage:
```csharp
public class MyInjectedServiceImpl: IMyInjectedService
{
    public INcHttpChannel Channel { get; set; } //injected instance of the channel

    public async Task<MyObject> CreateObjectFromParameters(MyObjectParameters i_Parameters)
    {
        //this sends a post request to the endpoint with i_Parameters as body which is encoded as JSON without any authorization/authentication info
        return await Channel.Post("/example/api/createobjectfromparameters", ContentTypes.APP_JSON, i_Parameters);
    }

    public async Task<MyObject> CreateObjectFromParametersSecured(MyObjectParameters i_Parameters)
    {
        //this sends a post request to the endpoint with i_Parameters as body which is encoded as JSON with the provided auth header
        return await Channel.Post("/example/api/createobjectfromparameters", ContentTypes.APP_JSON, i_Parameters, CreateAuthHeader);
    }

    public async Task<MyObject> CreateObjectFromParametersSecured(MyObjectParameters i_Parameters)
    {
        //this sends a post request to the endpoint with i_Parameters as body which is encoded as JSON with the provided auth header
        return await Channel.Post("/example/api/createobjectfromparameters", ContentTypes.APP_JSON, i_Parameters, CreateAuthHeader);
    }

    public async Task<MyObject> CreateObjectFromParametersWithCustomHeader(MyObjectParameters i_Parameters)
    {
        //this sends a post request to the endpoint with i_Parameters as body which is encoded as JSON. AddCustomHeader here can manipulate the request headers before the request is sent.
        return await Channel.Post("/example/api/createobjectfromparameters", ContentTypes.APP_JSON, i_Parameters, AddCustomHeader);
    }

    private Task<AuthenticationHeaderValue> CreateAuthHeader()
    {
        //Create your auth header here, this method can be async
    }

    private Task AddCustomHeader(HttpRequestHeaders t_Headers)
    {
        //add/remove headers from t_Headers, this method can be async
    }
}
```
If the code that is returned by the call is not a success code, any Send-Method will throw a ChannelRequestFailedException with the status code embedded in it.