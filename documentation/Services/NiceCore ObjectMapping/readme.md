# NiceCore ObjectMapping

NiceCore provides APIs for object mapping using Mapster or AutoMapper (currently).
To use this API you will need the API-dll NiceCore.ObjectMapping and one implementation-dll (NiceCore.ObjectMapping.Mapster or NiceCore.ObjectMapping.AutoMapper).
If all required files are present, NiceCore should detect them on startup. To use it inject IObjectMappingService.
## Basic usage
```csharp
public class MyClassThatMaps
{
	public IObjectMappingService ObjectMappingService {get; set;}
	public void DoStuff()
	{
		var firstInstance = new InstanceType() 
		{
			Name = "Hello",
			Value = "World",
		};
		var secondInstance = ObjectMappingService.Map<InstanceType, SecondType>(firstInstance);
	} 
}
```
This example creates an instance of the class InstanceType and maps that instance to a new instance of SecondType.
## using Configuration
You can also ignore properties on the secondtype, those would not be mapped then, or map a property of the source object to a completely different property (if both have the same type.)
This behavior can be achieved using the mapping config.
```csharp
public class MyClassThatMaps
{
	public IObjectMappingService ObjectMappingService {get; set;}
	public void DoStuff()
	{
		var conf = NcMappingConfig.NewConfig<InstanceType, SecondType>()
			.Ignore(r => r.Value); //Value on SecondType will remain default
		var firstInstance = new InstanceType() 
		{
			Name = "Hello",
			Value = "World",
		};
		var secondInstance = ObjectMappingService.Map<InstanceType, SecondType>(firstInstance, conf);
	} 
}
```
```csharp
public class MyClassThatMaps
{	public IObjectMappingService ObjectMappingService {get; set;}
	public void DoStuff()
	{
		var conf = NcMappingConfig.NewConfig<InstanceType, SecondType>()
			.Map(r => r.OtherValue, r => r.Value); //this maps Value from InstanceType to OtherValue on SecondType
		var firstInstance = new InstanceType() 
		{
			Name = "Hello",
			Value = "World",
		};
		var secondInstance = ObjectMappingService.Map<InstanceType, SecondType>(firstInstance, conf);
	} 
}
```
You can also supply a different runtime based value to the mapping using the config.
```csharp
public class MyClassThatMaps
{
	public IObjectMappingService ObjectMappingService {get; set;}
	public void DoStuff()
	{
		var conf = NcMappingConfig.NewConfig<InstanceType, SecondType>()
			.RuntimeValue(r => r.Value, src => "ThisIsAWayBetterValue"); //this will set the Value Property on SecondType to the value that is supplied as second parameter
		var firstInstance = new InstanceType() 
		{
			Name = "Hello",
			Value = "World",
		};
		var secondInstance = ObjectMappingService.Map<InstanceType, SecondType>(firstInstance, conf);
	} 
}
```