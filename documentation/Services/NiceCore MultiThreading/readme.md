# Multithreading on a service level
NiceCore provides an abstraction over multithreading in form of the "IThreadService".
```csharp
namespace NiceCore.Services.Threads
{
  /// <summary>
  /// Service for easier Threading
  /// </summary>
  public interface IThreadService : IService
```
This service provides multiple methods dealing with threads
```csharp
    /// <summary>
    /// Runs i_Method in a separate new thread, id
    /// </summary>
    /// <param name="i_Method">Method that should be run by the thread</param>
    /// <param name="i_ThreadId">Name to be used for the thread (important for progressService)</param>
    /// <param name="i_UseThreadPool">If the action should be queued onto a threadpool, overrides more global settings</param>
    void Do(Action<CancellationToken> i_Method, Guid i_ThreadId, bool i_UseThreadPool);

    /// <summary>
    /// Runs i_Method in a new Thread with a new Guid as name. This method uses the global "UseThreadPool" setting to determine 
    /// whether to run the method on a thread or a threadPool.
    /// </summary>
    /// <param name="i_Method">Method that should be run by the thread</param>
    void Do(Action<CancellationToken> i_Method);

    /// <summary>
    /// Stops thread with name i_ThreadId
    /// </summary>
    /// <param name="i_ThreadId">Guid-Name of the thread that should be stopped</param>
    void Stop(Guid i_ThreadId);

    /// <summary>
    /// Blocks current thread while waiting for i_ThreadId
    /// You can not wait for threads which have been queued onto a threadpool!
    /// </summary>
    /// <param name="i_ThreadId">Guid-Name of the thread to wait for</param>
    void WaitFor(Guid i_ThreadId);
```

This Interface is implemented by the "DefaultThreadService" (inherited from abstract BaseThreadService).
```csharp
namespace NiceCore.Services.Default.Threads
{
  /// <summary>
  /// Service impl for IThreadService
  /// </summary>
  [Register(InterfaceType = typeof(IThreadService), LifeStyle = LifeStyles.Singleton)]
  [InitializeOnResolve]
  public class DefaultThreadService : BaseThreadService
```