﻿# Validation
NiceCore supports data validation using the IValidationService found in NiceCore.Services.
This is done building upon FluentValidation (for more information see: https://github.com/FluentValidation/FluentValidation)

To use this:
```csharp
var validationService = GetValidationServiceSomehow(); // retrieve an instance of the service (you can do this via ServiceLocation)
var validationResult = validationService.Validate(myCoolObject, "VALIDATE"); //the second parameter defines the operation, this helps differentiating between validators that should be used.
//inspect the validationResult as needed
```
This call would return an instance of an implementation of the following interface:
```csharp
/// <summary>
/// Validation result for object validations
/// </summary>
public interface INcValidationResult
{
  /// <summary>
  /// Instance that has been validated
  /// </summary>
  object Instance { get; }

  /// <summary>
  /// Whether the validation ran into any errors. Therefore it will be true if the "Errors" list is empty
  /// </summary>
  bool IsValid { get; }

  /// <summary>
  /// Operation that triggered the validation
  /// </summary>
  public string Operation { get; }

  /// <summary>
  /// Validation results that ended in Warning
  /// </summary>
  public List<ValidationFailure> Warnings { get; }

  /// <summary>
  /// Validation results that ended in Error
  /// </summary>
  public List<ValidationFailure> Errors { get; }

  /// <summary>
  /// Validation results that ended in Info
  /// </summary>
  public List<ValidationFailure> Info { get; }
}
```
To define your own validations (which are needed since NiceCore does not define any validations of it's own.) you should inherit a class from
BaseNcValidator\<T> like so:
```csharp
/// <summary>
/// Validator that validates instances of type "ExampleObject"
/// </summary>
[Register(InterfaceType = typeof(INcObjectValidator))] //register this validator so that it will be picked up by the valdiation service
[Register(InterfaceType = typeof(INcValidator<ExampleObject>))] //register this way if you ever want to only validate using this valdiator
public class ExampleObjectValidator : BaseNcValidator<ExampleObject>
{
  /// <summary>
  /// Operation that this validator is intended to be used for. The ValidationService throws an exception if this is null.
  /// </summary>
  public override string Operation { get; } = "VALIDATE";

  /// <summary>
  /// Defines the rules of this validator, is called once before the first time the rules are needed
  /// </summary>
  /// <param name="t_Validator"></param>
  protected override void DefineRules(AbstractValidator<ExampleObject> t_Validator)
  {
    //for more info about using the .RuleFor method see https://docs.fluentvalidation.net/
    t_Validator.RuleFor(r => r.Name).NotNull().WithSeverity(Severity.Error);
    t_Validator.RuleFor(r => r.Name).MinimumLength(5).WithSeverity(Severity.Warning);
    t_Validator.RuleFor(r => r.Name).MaximumLength(10).WithSeverity(Severity.Info);
    t_Validator.RuleFor(r => r.Number).Equal(42).WithSeverity(Severity.Error);
  }
}
```
If you try to validate objects which do not have a validator defined for their type or for an operation for which
no validator has been created, the result of the validation will always be valid.