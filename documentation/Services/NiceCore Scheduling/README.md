# Scheduling
NiceCore features the ability to schedule execution of code either by timespan or to exact times each day.
The easiest way to use this functionality is through scheduled services run by the ScheduledServiceHost.
## Scheduled Services
Example for incremental (every timespan) executed service:
```csharp
/// <summary>
  /// Service example
  /// </summary>
  [Register(InterfaceType = typeof(IScheduledService))] //register service
  public class ExampleIncrementalScheduledService : BaseService, IScheduledService // implement IScheduledService
  {
    /// <summary>
    /// List for checking if the service has run
    /// </summary>
    public List<string> ListToManipulate { get; set; }
    /// <summary>
    /// Return an incremental entry
    /// </summary>
    /// <returns></returns>
    public IScheduleEntry CreateScheduleEntry()
    {
      return new DefaultIncrementalScheduleEntry()
      {
        ExecuteOnScheduled = false, // no autostart of service
        Schedulable = this, //we want this service to be executed
        SpanBetweenExecutions = TimeSpan.FromMilliseconds(100), //100 miliseconds between each execution
      };
    }
    /// <summary>
    /// Add an entry to ListToManipulate
    /// </summary>
    /// <returns></returns>
    public bool Execute() //function that is executed when the entry is due for execution
    {
      if (ListToManipulate == null)
      {
        ListToManipulate = new List<string>();
      }
      ListToManipulate.Add(string.Empty);
      return true;
    }
  }
```
Example for service that is executed in a specific time:
```
/// <summary>
  /// Service example
  /// </summary>
  [Register(InterfaceType = typeof(IScheduledService))]
  public class ExampleExactScheduledService : BaseService, IScheduledService
  {
    /// <summary>
    /// List for checking if the service has run
    /// </summary>
    public List<string> ListToManipulate { get; set; }
    /// <summary>
    /// Return an incremental entry
    /// </summary>
    /// <returns></returns>
    public IScheduleEntry CreateScheduleEntry()
    {
      return new DefaultExactScheduleEntry() //other type of scheduled entry
      {
        ExecuteOnScheduled = false,
        Schedulable = this,
        ExecuteTime = GetIn100Miliseconds(), //sets this service to exactly execute in 100miliseconds, one time
      };
    }
    /// <summary>
    /// Add an entry to ListToManipulate
    /// </summary>
    /// <returns></returns>
    public bool Execute()
    {
      if (ListToManipulate == null)
      {
        ListToManipulate = new List<string>();
      }
      ListToManipulate.Add(string.Empty);
      return true;
    }
    private DateTime GetIn100Miliseconds()
    {
      return DateTime.Now.AddMilliseconds(100);
    }
  }
```
To get the services up and running you have to include the host into the app building:
```csharp
m_App = NcApplicationBuilder.Create()
        .UseDefaults()
        .ScheduleServiceHost.With<DefaultScheduleServiceHost>() //this is the default line needed, You can ofcourse use your own implementation
        .Build();
```
## Custom scheduling
If you don't want to use the host service, then you can create the schedule by your self and add entries manually.
```csharp
var schedule = new DefaultSchedule();
schedule.AddToSchedule(new DefaultIncrementalScheduleEntry() 
{
    ExecuteOnScheduled = true, //autostart
    Schedulable = GetSchedulable(), //object that implements ISchedulable -> the Execute() of this object will be run
    SpanBetweenExecutions = TimeSpan.FromMinutes(2), // run every 2 minutes
}
schedule.Enable(); //starts the schedule
```










`