# Setting up a NiceCore project via nuget
NiceCore has been built from the ground up to be very modular. This makes installing through nuget a bit more tricky.
Every single NiceCore project is on nuget as a single package.
## Pattern
Most of the projects follow this pattern (using storage as an example): 
NiceCore.Storage.dll - definitions for integration
NiceCore.Storage.NH.dll - concrete implementation of defintions made in NiceCore.Storage
So while picking the needed projects you will always need the definition project and an implementation.
The implementation of course can be provided by yourself, while the definitions project is often needed to have base
functionality.
## Recommended configurations
Although the NiceCore project itself and NiceCore.ServiceLocation is often needed in any project, the rest is quite customizable.
NiceCore.Rapid is kind of optional because it is not directly needed for a project, but it makes setting up an app WAY easier.
* NiceCore basic:
    * NiceCore
    * NiceCore.Services
    * NiceCore.Services.Default
    * NiceCore.ServiceLocation
    * NiceCore.ServiceLocation.Default
    * NiceCore.Rapid
    * NiceCore.Rapid.Default
* NiceCore Avalonia UI:
    * NiceCore basic
    * NiceCore.UI
    * NiceCore.UI.Avalonia
* NiceCore Avalonia UI (with database access):
    * NiceCore Avalonia UI
    * NiceCore.Storage
    * NiceCore.Storage.NH
* NiceCore Hosting:
    * NiceCore basic
    * NiceCore.Hosting
    * NiceCore.Hosting.Default
    * NiceCore.Rapid.Hosting
    * NiceCore.Rapid.Default
    * NiceCore.Storage.NH
    * NiceCore.DefaultHost (in case that you only want to build a dll with services)