﻿# Logging

NiceCore supports the abstraction of logging through the ILogger interface.
If you do not configure anything, NiceCore uses it's own default (and quite basic) logger implementation.
To retrieve an instance of ILogger to start logging call:
```csharp
NcLogging.GetLogger();
```
This static method will then return the logger that was configured for the default context.
To configure your own implementation you should call the following before you need the first instance of it (most likely where you do any configuration like program.cs or something similar):
```csharp
NcLogging.InitializeLogger(() => new MyCoolLogger());
```
This will put an instance of MyCoolLogger as the logger for the default context.

### Contexts
To enable using different instances for different circumstances NiceCore manages ILogger instances related to contexts.
For example every module of NiceCore uses it's own context so that debugging those modules separate would be easier.
To initialize an ILogger instance for a certain context call:
```csharp
NcLogging.InitializeLogger(() => new MyCoolLogger(), "myCoolContext");
```
This puts an instance of MyCoolLogger to the context of "myCoolContext". Yes contexts are just strings.
If you query an instance of ILogger for a context that has not been registered, the logger registered for the default context will be returned.
The default context is available using:
```csharp
NcLogging.CONTEXT_DEFAULT
```

### Actually Logging
Now you have the loggers configured to your hearts content, but how are they used?
If you have an instance of ILogger you can call multiple different overloads of the Log Method:
Available Log Methods:
```csharp
  /// <summary>
  /// Instance of the logger used for logging
  /// </summary>
  public interface ILogger
  {
    /// <summary>
    /// Logs i_LogMessage if current level is applicable
    /// </summary>
    /// <param name="i_LogMessage">Function that is called if the current loglevel is appropriate and retuns the message to be logged</param>
    /// <param name="i_Exception">Exception that was thrown</param>
    /// <param name="i_Level">Level this message should be logged to</param>
    /// <param name="i_LogCategory">Category that is logged to. This exists to support possible distribution to multiple loggers by the Implementatio wrapped by ILogger</param>
    /// <param name="i_RelatedObjects">All objects related to the incident -> this enables support for structured logging</param>
    void Log(Func<string> i_LogMessage, Exception i_Exception, LogLevels i_Level, string i_LogCategory, params object[] i_RelatedObjects);

    /// <summary>
    /// Logs i_LogMessage if current level is applicable
    /// </summary>
    /// <param name="i_LogMessage">Function that is called if the current loglevel is appropriate and retuns the message to be logged</param>
    /// <param name="i_Level">Level this message should be logged to</param>
    void Log(Func<string> i_LogMessage, LogLevels i_Level);

    /// <summary>
    /// Logs i_LogMessage if current level is applicable
    /// </summary>
    /// <param name="i_LogMessage">Function that is called if the current loglevel is appropriate and retuns the message to be logged</param>
    /// <param name="i_Exception">Exception that was thrown</param>
    /// <param name="i_Level">Level this message should be logged to</param>
    void Log(Func<string> i_LogMessage, Exception i_Exception, LogLevels i_Level);
  }
```
Usage:
```csharp
var logger = NcLogging.GetLogger();
logger.Log(() => "Someone needs to see this!!!!", LogLevels.Fatal);
```
Now you might ask, why the lambda and not the string itself? The lambda you input is only executed if the minimum level of the logger is actually lower than the level
of the message you want to log. This means that the string you pass is only instantiated if it will actually be logged.
There are a lot of extension methods provided like ".Warn" or ".Error" so that you can easier log with those.
### Exceptions
Logging might throw different exceptions in case some things go wrong.
- If something fails during the retrieval of the current minimum log level of the logger instance a RetrieveLogLevelException is thrown.
- If something fails during creation of the message to log a LogMessageCreationException is thrown.
- If something fails during creation of the logger instance a LoggerInitializationException is thrown.
- If something fails during the actual logging a LoggingException is thrown.
All of those exceptions have the cause exception as inner exception.

### Alternative way of setting up
If you are using NiceCore.Rapid there is an easy way to configure logging through the app builder:
```csharp
 var app = NcApplicationBuilder.Create()
        .UseDefaults()
        .UsedLoggers.Add(new LoggerInitialization() { ContextName = "myCoolContext", LoggerFactoryFunc = () => new MyCoolLogger})
        .Build();
```
A logger initialization is a tuple class used to feed the info to the app builder.
This will then be calling NcLogging.InitializeLogger for all provided UsedLoggers.

### Disabling the Logging

If you really want to disable logging completely you can do the following:
```csharp
NcLogging.InitializeLogger(() => new DummyLoggerWrapper());
```
This is an implementation that just does nothing.