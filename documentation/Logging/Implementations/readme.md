﻿# Implementations

By default NiceCore brings to implementations for the ILogger interface: it's default logger and a wrapper for serilog.
To use the serilog implementation you need to add the NiceCore.Logging.Serilog assembly and then call:
```csharp
NcLogging.InitializeLogger(() => SerilogLoggingWrapper.FromSerilog(logger));
```

## Implementing your own ILogger
To implement your own ILogger (wrapper or not) you need to implement the abstract class BaseLoggerWrapper:
```csharp
  /// <summary>
  /// My instance for the logger wrapper
  /// </summary>
  public class MyLoggerWrapper : BaseLoggerWrapper
  {
    /// <summary>
    /// Return internal log level
    /// </summary>
    /// <returns></returns>
    protected override LogLevels GetCurrentLogLevel()
    {
      //your way of getting the log level here
    }

    /// <summary>
    /// Log
    /// </summary>
    /// <param name="i_LogMessage"></param>
    /// <param name="i_Exception"></param>
    /// <param name="i_Level"></param>
    /// <param name="i_LogCategory"></param>
    /// <param name="i_RelatedObjects"></param>
    protected override void LogInternal(string i_LogMessage, Exception i_Exception, LogLevels i_Level, string i_LogCategory, params object[] i_RelatedObjects)
    {
      //your actual logging code here
      //this is only called if the loglevel is higher than the minimum
    }
  }
```
And then you only need to use this when initializing the logging.