﻿# Filter Support
NiceCore supports direct filtering of any IQueryable/IEnumerable using custom filters.
This is achieved using instances of INcFilter. An instance of INcFilter and its subfilters will be converted to a single statement/function that can be passed to anything
which accepts either a Func<T, bool> or Expression<Func<T, bool>>, using the .FilterBy Extension method.
```csharp
var myList = GetListWithObjectsThatHaveNameProperty();
var objectsCalledHans = myList
    .FilterBy(NcFilters.CreateAnd(NcFilterConditions.CreateEquals(nameof(MyObject.Name), "Hans")))
    .ToList();
```
This call would filter "myList" for all objects whose property "Name" equals "Hans".

Taking a closer look:
```csharp
NcFilters.CreateAnd //Creates a filter that links its conditions and subfilters with AND
NcFiltersConditions.CreateEquals //creates a condition used by the filter that operates using ==
```
A filter can have any number of conditions and subfilters. You can use the fitting override of the static create functions to create a filter that
fits your use case.

CAUTION: This is currently a lot slower than actual linq. Only use this if you cannot directly access the query/collection in question.


Interface of INcFilter:
```csharp
public interface INcFilter
{
  /// <summary>
  /// Sub Filters
  /// </summary>
  IEnumerable<INcFilter> SubFilters { get; }

  /// <summary>
  /// Conditions that get translated into .Where statements
  /// </summary>
  IEnumerable<INcFilterCondition> Conditions { get; }

  /// <summary>
  /// Link of Filter
  /// </summary>
  FilterLinks FilterLink { get; }
}
```
Available FilterLinks:
```csharp
/// <summary>
/// How conditions inside a filter are linked
/// </summary>
public enum FilterLinks
{
  /// <summary>
  /// And
  /// </summary>
  And,

  /// <summary>
  /// Or
  /// </summary>
  Or,

  /// <summary>
  /// Exclusive Or
  /// </summary>
  Xor,
}
```
Interface INcFilterCondition:
```csharp
/// <summary>
/// Condition used by INcFilter
/// </summary>
public interface INcFilterCondition
{
  /// <summary>
  /// name of the property that should be used for filtering
  /// </summary>
  string PropertyName { get; }

  /// <summary>
  /// Operation that compares the value of property name to Value
  /// </summary>
  FilterOperations Operation { get; }

  /// <summary>
  /// Expected value
  /// </summary>
  object Value { get; }
}
```
Available operations for NcFilterConditions:
```csharp
/// <summary>
/// Possible filter operations
/// </summary>
public enum FilterOperations
{
  /// <summary>
  /// ==
  /// </summary>
  Equals,

  /// <summary>
  /// greater
  /// </summary>
  Greater,

  /// <summary>
  /// greater or Equals
  /// </summary>
  GreaterOrEquals,

  /// <summary>
  /// less
  /// </summary>
  Less,

  /// <summary>
  /// less or equals
  /// </summary>
  LessOrEquals,

  /// <summary>
  /// Check if element is in collection -> the value passed must implement IList
  /// </summary>
  In,

  /// <summary>
  /// Checks if two intervals overlap -> both sides need to implement INcTimeInterval
  /// </summary>
  Overlaps
}
```