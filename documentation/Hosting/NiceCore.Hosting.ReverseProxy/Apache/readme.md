# Reverse Proxy using Apache
To reverse proxy a NiceCore.Hosting app using an apache server you will need to take the following steps:
I won't go into detail about installing apache, just the configuration needed to get a NiceCore.Hosting app reverse proxied.
## Configuring the NiceCore.Hosting app
```xml
<?xml version="1.0" encoding="utf-8" ?>
<configuration>
  <configSections>
    <section name="Hosting" type="NiceCore.Hosting.Config.HostingConfigSection, NiceCore.Hosting"/>
  </configSections>
  <Hosting Address="127.0.0.1" Port="5000" ContentDirectory="Content" MapEmptyPathToIndex="true">
    <HostingAliases>
      <Alias OriginalPath="somefile" Alias="index.html"/>
    </HostingAliases>
    <DefaultPaths>
      <DefaultPath Path="index.html"/>
    </DefaultPaths>
  </Hosting>
</configuration>
```
**Important**: address = 127.0.0.1, port = 5000
You will need those settings for the apache config.
## Configuring Apache
For me to get apache to reverse proxy the app I needed to do 2 things:
* Create a reverse proxy config
```xml
<VirtualHost *:*>
    RequestHeader set "X-Forwarded-Proto" expr=%{REQUEST_SCHEME}
</VirtualHost>

<VirtualHost *:80>
    ProxyPreserveHost On
    ProxyPass / http://127.0.0.1:5000/
    ProxyPassReverse / http://127.0.0.1:5000/
    ServerName www.example.com
    ServerAlias *.example.com
    ErrorLog ${APACHE_LOG_DIR}helloapp-error.log
    CustomLog ${APACHE_LOG_DIR}helloapp-access.log common
</VirtualHost>

```
I placed these lines in a file called **/etc/apache2/conf-enabled/dotnet_revproxy.conf**. You can of course choose the name yourself. 
**Important**: The address parts (in ProxyPass and ProxyReversePass) must match the ip and port you configured your NiceCore app to listen on (atleast it worked with them matching).
To read more: https://docs.microsoft.com/de-de/aspnet/core/host-and-deploy/linux-apache?view=aspnetcore-2.2
* Make Apache load needed modules
https://httpd.apache.org/docs/current/mod/mod_headers.html
https://httpd.apache.org/docs/2.4/mod/mod_proxy.html 
should be read because of security advises before loading the modules. 
For apache to be able to understand the commands used in the config file the following modules should be loaded:
```
LoadModule headers_module /usr/lib/apache2/modules/mod_headers.so
LoadModule proxy_module /usr/lib/apache2/modules/mod_proxy.so
LoadModule proxy_http_module /usr/lib/apache2/modules/mod_proxy_http.so
```
I placed them directly at the bottom of **/etc/apache2/apache2.conf**.