﻿# NiceCore.Hosting - Available Configuration Keys

| Key | Type | Description
| --- | ---- | -----------
| NiceCore.Hosting.Info.Logs.Enabled| bool | This enables/disables the logviewer that can be reached at /info/logs/raw
| NiceCore.Hosting.Info.Endpoints.Enabled| bool | This enables/disables the endpoint overview that can be reached at /info/endpoints/static
| NiceCore.Hosting.Token.Secret| string | The secret that is used to sign/validate JWT Tokens. If none is provided, JWT Tokens will not be signed.
| NiceCore.Hosting.Token.ExpirationTime| int | Number that represents the time until a JWT Token is considered expired. See ExpirationTimeKind to specify what unit this value represents
| NiceCore.Hosting.Token.ExpirationTimeKind| NiceCore.Base.TimeKind (enum)| This specifies the unit of time that should be used for ExpirationTime
| NiceCore.Hosting.Asp.UseNiceCoreContainer| bool| This specifies whether the NiceCore Container should replace the Microsoft.Extensions.DependencyInjection Container in ASP.NetCore apps 

## DataType Reference
Code reference for mentioned non-standard data types. For how the conversion works, you have to see how the corresponding configuration source handles these types of values.


NiceCore.Base.TimeKind:
```csharp
/// <summary>
/// Enum for kind of time operations (Millisecond, Second, Minute, Hour, Day)
/// </summary>
public enum TimeKind
{
  /// <summary>
  /// In ms
  /// </summary>
  Milliseconds,

  /// <summary>
  /// Seconds
  /// </summary>
  Seconds,

  /// <summary>
  /// Minutes
  /// </summary>
  Minutes,

  /// <summary>
  /// Hours
  /// </summary>
  Hours,

  /// <summary>
  /// Days
  /// </summary>
  Days
}
```