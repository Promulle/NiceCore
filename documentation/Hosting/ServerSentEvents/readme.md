﻿# Server Sent Events Support
NiceCore does support server sent events in an easier way.
To use thise just create a new implementation of IServerSentEventingEndpoint. If you are using ASPNetCore as hosting, the easiest way to do this would be to inherit from BaseAspNetCoreServerSentEventingEndpoint like so:
```csharp
public class MyClassUpdateEndpoint : BaseAspNetCoreServerSentEventingEndpoint<MyClassUpdatedMessage, MyClass>
{
    /// <summary>
    /// Endpoint that is used
    /// </summary>
    public string Endpoint { get; } = "/MyClassUpdated"
}
```
This will create an endpoint that can be used for SSE at the path "/sse/MyClassUpdated"
Anytime a message of the type MyClassUpdated is received, the message will be passed to "ToJSONData" and then the string result of that method be written and flushed.


Optionally you can also override the following methods:
```csharp
    /// <summary>
    /// Function that converts the value of i_Message to a json value that will be sent
    /// </summary>
    /// <param name="i_Message"></param>
    /// <returns></returns>
    protected virtual string ToJSONData(TMessage i_Message) //overriding this will change how the message is processed

    /// <summary>
    /// Returns the base endpoint name used
    /// </summary>
    /// <returns></returns> 
    protected virtual string GetEndpointGroup() //overriding this will change the path the endpoint is registered as, default this returns "/sse"
```