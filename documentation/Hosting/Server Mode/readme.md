﻿# NiceCore.Hosting.ASPNetCore - Server Modes
NiceCore supports hosting with aspnetcore on both Kestrel and IIS, to configure which to use specify the mode with the following key:

"Hosting.Asp.ServerMode" as string. Available options(case does not matter): "Kestrel" (default) or "IIS"

