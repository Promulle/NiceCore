# NiceCore Diagnostics

NiceCore enables internal system diagnostics that might show why a NiceCore program did not boot correctly.

## Enabling Core Diagnostics

The simple way to enable diagnostics is to call the following method before any NiceCore system starts.

`DiagnosticsCollector.Instance.Configure();`

This call can be further configured by passing a configuration object. (This doc will use the default settings as examples.)

For diagnostics to take effect, the app does need to have "--diagnose" as command line parameter. If this command is present, a configuration is initialized that consists of a .json file (default path: NcDiagnostic.json) the given command line parameters and environment variables with the specified prefix (default: Nc).

If the command line parameter is not present, all logger instances will point to NullLogger.Instance.

These settings will then be used to construct a logger factory that will be used to create ILogger instances which will be used by some NiceCore internal systems. As long as the NiceCore App Builder has not yet replaced the instances with logger instances that have been created according to the NiceCore App configuration.

Example for NcDiagnostics.json:

`{
	"Logging": {
		"LogLevel": {
			"Default": "Error"
		}
	}
}`

Settings in NcDiagnostics.json only apply to loggers in the diagnostic scope. They do not interfere with the NiceCore AppBuilder Configuration.

The loggers used for diagnostics are: A Console logger and a VERY simple file logger.