# The advanced presenter system

The advanced presenter system should enable a simple and flexible way to embed presenters. This enables the creation of systems that control when or when not presenters should be embedded and how. For example this enables the integration of a privilege access control system on a high and reusable level. The startview system also makes the application extremely flexible, because by just swapping the startview you can drastically change the appearance of the application without touching anything else.

## Presenters and parents

The first step in this system is, that a presenter has a presenter as parent and other presenters as children. This creates a presenter tree with the startview at its top. This enables to set presenter relationships. For example a tab of a presenter showing multiple sub presenter as tabs.

Important properties:

```csharp
.Children
.Parent
```

These can be set manually but this is not the recommended way of doing it.

## The PresenterExecutor

To provide a standardized way of executing presenters there is the interface IPresenterExecutor. Instances that implement IPresenterExecutor can execute presenters and set needed properties. Using this you could implement your own executor that for example checks privileges of a user or prevents multiple presenters of the same type to be executed. NiceCore provides a default implementation that sets parent child relationship on execution.

## The StartViewPresenter

Using the StartViewPresenter as top element in the presenter parent children tree completes the system. Also the StartViewPresenter is thought as the main style of your application. For example a ribbon or tree view. You just define the tree view in the StartViewPresenter and embed all other presenters into the startview for example. This makes the application really flexible, because you can just change the used StartViewPresenter and your application is different without really changing the rest. 

### How to pick a StartView

A StartViewPresenter can be chosen in different ways.

You can construct the instance by hand:

```csharp
var myStartView = new MyStartViewPresenter();
```

Or pick the startview in the application builder:

````csharp
var app = NcApplicationBuilder.Create<NcUIApplication>()
        .UseDefaults()
        .StartView().With(new MyStartViewPresenter())
        .Build();
````

Once you picked it, you have to set the content of your application to the content of the viewmodel of the StartViewPresenter. 

### Executing a normal presenter

