﻿# Migrations from NiceCore Versions before 0.5.4.0
Since the config system changed in a very impactful way, you most likely will need to change some things when migrating from earlier versions.
List of things that are possibly to do:
- Config Builders do not exist anymore
- Sections are no longer needed for xml configs
- xml config files changed dramatically, so you will have to change those (see examples from: https://docs.microsoft.com/en-us/dotnet/core/extensions/configuration-providers#xml-configuration-provider or the config provided in "Kinds of Configuration Sources")
- Configuration Keys do not need to be changed, just their representation in configs
