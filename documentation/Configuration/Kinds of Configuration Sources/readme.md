﻿# Kinds of Configuration Sources
NiceCore supports some pre built sources for common scenarios, to make defining them easier.

Currently supported:
- JSON with "BaseJSONFileNcConfigurationSource"
- XML with "BaseXMLFileNcConfigurationSource"
- InMemory (String dictionaries) with "BaseInMemoryNcConfigurationSource"
- Environment Variables (Prefixed only) with "BaseEnvironmentNcConfigurationSource"

Assembly based:
- BaseAssemblyJSONNcConfigurationSource
- BaseAssemblyXMLNcConfigurationSource

Default implementations:
- DefaultEnvironmentNcConfigurationSource (Environment Variables prefixed with "Nc_")
- EntryAssemblyJSONNcConfigurationSource (Json File that is called (nameofyourentryassembly).json)

Below there are examples that correspond to the following in program behavior:
```csharp
Conf.GetValue<string>("Key"); //this would return "Value"
Conf.GetValue<TestObject>("TestObject"); // this would return an instance of TestObject where Name = "Hans Paul" and Number = 5
Conf.GetValue<string>("TestObjet.Name"); //this would return "Hans Paul"
Conf.GetCollection<int>("Collection"); //this would return a list with [1,2,3,4] as elements
Conf.GetCollection<TestObject>("ObjectCollection"); //this would return a list with the objects as elements
```

## JSON
JSON configurations can be easily added using classes inheriting from BaseJSONFileNcConfigurationSource:
```csharp
public MyExampleJSONSource : BaseJSONFileNcConfigurationSource
{
    /// <summary>
    /// This defines if the file is needed or optional.
    /// If a file is marked as non-optional but not found, a FileNotFoundException will be thrown
    /// </summary>
    public override bool IsOptional { get; } = true;

    /// <summary>
    /// This defines whether the configuration should be reloaded whenever the file changes.
    /// </summary>
    public override bool ReloadOnChange { get; } = false;

    /// <summary>
    /// Priority this source is used with.
    /// Lower priorities will be overriden by hígher priorities (some overriding is defined by Microsoft.Extensions.Configuration so see that for more details on overriding)
    /// </summary>
    public override int Priority { get; } = 0;

    /// <summary>
    /// This returns the filepath of the json file that will be read for configuration values
    /// </summary>
    /// <returns></returns>
    protected override string GetFilePath()
    {
      return "TestData.json";
    }
}
```
TestData.json:
```json
{
    "Key": "Value",
    "Collection": [
        "1",
        "2",
        "3",
        "4"
    ],
    "TestObject": {
        "Name": "Hans Paul",
        "Number": "5"
    },
    "ObjectCollection": [
        {
            "Name": "Hans Paul",
            "Number": "5"
        },
        {
            "Name": "Heinz Gustav",
            "Number": "10"
        }
    ]
}
```

## XML
XML configurations can be easily added using classes inheriting from BaseXMLFileNcConfigurationSource:
```csharp
  /// <summary>
  /// Test config
  /// </summary>
  public class TestXmlConfigurationSource : BaseXMLFileNcConfigurationSource
  {
    /// <summary>
    /// optional
    /// </summary>
    public override bool IsOptional { get; } = true;

    /// <summary>
    /// Reload
    /// </summary>
    public override bool ReloadOnChange { get; } = true;

    /// <summary>
    /// Priority
    /// </summary>
    public override int Priority { get; } = 0;

    /// <summary>
    /// Get FilePath
    /// </summary>
    /// <returns></returns>
    protected override string GetFilePath()
    {
      return "TestData.xml";
    }
  }
```
TestData.xml:
```xml
<?xml version="1.0" encoding="utf-8" ?>
<configuration>
  <Key>Value</Key>
  <TestObject>
    <Name>Hans Paul</Name>
    <Number>5</Number>
  </TestObject>
  <Collection name="1">1</Collection>
  <Collection name="2">2</Collection>
  <Collection name="3">3</Collection>
  <Collection name="4">4</Collection>
  <ObjectCollection name="1">
    <Name name="1">Hans Paul</Name>
    <Number name="1">5</Number>
  </ObjectCollection>
  <ObjectCollection name="2">
    <Name name="2">Heinz Gustav</Name>
    <Number name="1">10</Number>
  </ObjectCollection>
</configuration>
```
Note: the name tags exist to avoid duplicate paths. (An exception will be thrown if a key/keypath is duplicate)

## InMemory
In memorty (string dictionary) configurations can be easily added using classes inheriting from BaseInMemoryNcConfigurationSource:
```csharp
  /// <summary>
  /// Test in memory
  /// </summary>
  public class TestInMemoryConfigurationSource : BaseInMemoryNcConfigurationSource
  {
    /// <summary>
    /// Values that are used for configuration values
    /// </summary>
    public override IDictionary<string, string> Values { get; } = CreateValues();

    /// <summary>
    /// Priority
    /// </summary>
    public override int Priority { get; } = 0;

    private static IDictionary<string, string> CreateValues()
    {
      return new Dictionary<string, string>()
      {
        { "Key", "Value" },
        { "TestObject:Name", "Hans Paul" },
        { "TestObject:Number", "5" },
        { "Collection:One", "1" },
        { "Collection:Two", "2" },
        { "Collection:Three", "3" },
        { "Collection:Four", "4" },
        { "ObjectCollection:One:Name", "Hans Paul" },
        { "ObjectCollection:One:Number", "5" },
        { "ObjectCollection:Two:Name", "Heinz Gustav" },
        { "ObjectCollection:Two:Number", "10" }
      };
    }
  }
```

## Environment Variables
Environment variables configurations can be easily added using classes inheriting from BaseEnvironmentNcConfigurationSource:
```csharp
  /// <summary>
  /// Example Imple
  /// </summary>
  public class TestEnvConfigurationSource : BaseEnvironmentNcConfigurationSource
  {
    /// <summary>
    /// Prefix, this is the prefix
    /// </summary>
    public static readonly string PREFIX = "NcExample_";

    /// <summary>
    /// Prefix that is used to determine whether an environment variable should be added as configuration
    /// </summary>
    protected override string Prefix { get; } = PREFIX;

    /// <summary>
    /// Priority
    /// </summary>
    public override int Priority { get; } = 0;
  }
```
EnvironmentVaribles would be something like this with "\{pre}" being the defined prefix:
```
{pre}Key="Value" <-- full key as example NcExample_Key="Value"
{pre}TestObject__Name="Hans Paul"
{pre}TestObject__Number="5"
{pre}Collection__First="1"
{pre}Collection__Second="2"
{pre}Collection__Third="3"
{pre}Collection__Fourth="4"

{pre}ObjectCollection__First__Name="Hans Paul
{pre}ObjectCollection__First__Number="5"

{pre}ObjectCollection__Second__Name="Heinz Gustav
{pre}ObjectCollection__Second__Number="10"
```

Note the "__" this is a delimiter for environment variabls that works on a lot of platforms.
This is converted and does not change how a key is used in program. Also the prefix is not part of the key when retrieving from the config instance.