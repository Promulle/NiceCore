﻿# Assembly based Configuration Sources
For xml and json there are also prebuilt configuration sources that are based on the assembly name.
For example you can use the pre defined EntryAssemblyJSONNcConfigurationSource to add a json file that has the same name as your entry assembly.

For example you have a console application that is called MyCoolApp.exe then you can add the EntryAssemblyJSONNcConfigurationSource 
and then the file that would be next to your MyCoolApp.exe and is called "MyCoolApp.json" would be read for configuration.
Or "MyCoolApp.xml.config" would be read if a EntryAssemblyXMLNcConfigurationSource would be used.

These Assembly based sources can be used for JSON and XML.
For both there are corresponding base classes:
- BaseAssemblyJSONNcConfigurationSource
- BaseAssemblyXMLNcConfigurationSource

by default, any source inheriting from these and NOT changing "GetAssembly" would provide a config file with the name of the assembly the inheriting class is defined in.

