﻿# Configuration
NiceCore supports configuration from multiple different sources.
To do this NiceCore builds upon Microsoft.Extensions.Configuration.
## Setup
The fastest way to setup the Configuration is by using UseDefault in your Application Builder call:
```csharp
var app = NcApplicationBuilder
        .Create()
        .UseDefaults() //adds the configuration with a config source that points at <yourEntryAssemblyname>.json and the default environment variables (prefix "Nc_")
        .Build();
``` 
Example of defining custom sources:
```csharp
var app = NcApplicationBuilder
        .Create()
        .Configuration.With<DefaultNcConfiguration>()
        .ConfigurationSources.Add<MyExampleJSONSource>() //adds a json source -> see "Defining Sources for more details"
        .ConfigurationSources.Add<MyExampleXMLSource>() //adds a xml source
        .Build();
```
To do this without an application builder follow this example:
```csharp
var conf = new DefaultNcConfiguration();
conf.ConfigurationSources.Add(new MyExampleJSONSource());
conf.ConfigurationSources.Add(new MyExampleXMLSource());
conf.Initialize(); //<-- this is very important, if you call any function before initializing the config, an InvalidOperationException will be thrown
```
## Usage
To use the configuration you need to obtain the instance that has been configured with the sources.
If you have obtained the instance, you can just call "GetValue\<T>" or "GetCollection\<T>" to retrieve stored values.
```csharp
var name = Configuration.GetValue<string>("Name"); //This retrieves the string value that is defined by the key "Name"
List<string> myCollection = Configuration.GetCollection<string>("CollectionKey"); //this retrieves all values defined by Key "CollectionKey"
var complexInstance = Configuration.GetValue<ComplexType>("Instance"); //This retrieves a full object graph that can be found under Key "Instance"
```
You can also "step" through an object graph using more complex Keys (This works for sub objects or collections aswell):
```csharp
var name = Configuration.GetValue<string>("Instance.Name"); //this retrieves only the name for the object that can be found under "Instance"
```
Keys for such operations delimit single layers with either "." or ":".

## Defining Sources
To configure a custom configuration source you need to implement the Interface "INcConfigurationSource".
```csharp
    /// <summary>
    /// Priority of this source, determines in what order sources are read
    /// </summary>
    int Priority { get; }

    /// <summary>
    /// Applies this source to the configuration builder
    /// </summary>
    /// <param name="t_Builder"></param>
    void Apply(ConfigurationBuilder t_Builder);
```
NiceCore already provides a lot of base classes for some scenarios like for example Json.
To use this, create a new class inheriting from "BaseJSONFileNcConfigurationSource":
```csharp
public MyExampleJSONSource : BaseJSONFileNcConfigurationSource
{
    /// <summary>
    /// This defines if the file is needed or optional.
    /// If a file is marked as non-optional but not found, a FileNotFoundException will be thrown
    /// </summary>
    public override bool IsOptional { get; } = true;

    /// <summary>
    /// This defines whether the configuration should be reloaded whenever the file changes.
    /// </summary>
    public override bool ReloadOnChange { get; } = false;

    /// <summary>
    /// Priority this source is used with.
    /// Lower priorities will be overriden by hígher priorities (some overriding is defined by Microsoft.Extensions.Configuration so see that for more details on overriding)
    /// </summary>
    public override int Priority { get; } = 0;

    /// <summary>
    /// This returns the filepath of the json file that will be read for configuration values
    /// </summary>
    /// <returns></returns>
    protected override string GetFilePath()
    {
      return "TestData.json";
    }
}
```
For details on the pre built sources, and how to use them, see: "Kinds of Configuration Sources"

## Known Issues

### GetCollection
There is a very important thing to note when it comes to the usage of GetCollection.
While GetCollection is type safe, if any value can not be auto-converted to the requested Type (for example "int" like in "GetCollection\<int>"), these values will be left out.
There currently is no way of notifying the caller about this, so you have to keep this in mind when defining configuration values.
Demo: if you define a collection somewhere in your config like that: [ "1", "2", "A", "3"], "GetCollection\<int>" would only return [1, 2, 3].

### GetValue
Currently GetValue cannot be called with Collections as generic parameter (for example "GetValue\<List\<string>>("MyKey")").
If you try this, a NotSupportedException will be thrown. So please use GetCollection instead.