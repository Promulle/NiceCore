﻿# Reloadable Settings
Since Microsoft.Extensions.Configurations supports reloading of config values under certain circumstances for certain sources,
NiceCore exposes another API set that aims at simplifying using those.
To do this NiceCore provides the interface IReloadableSetting\<T>:
```csharp
/// <summary>
  /// Interface for settings objects that refresh themselves (if the source this was configured by can be refreshed)
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public interface IReloadableSetting<T> : IDisposable
  {
    /// <summary>
    /// Event that is fired when this setting has been reloaded
    /// </summary>
    event EventHandler<SettingReloadedEventArgs<T>> WasReloaded;

    /// <summary>
    /// Key this setting is retrieved from
    /// </summary>
    string Key { get; }

    /// <summary>
    /// Context this setting is retrieved under. This could be for example the Service Instance that read the setting. Can be null
    /// </summary>
    string Context { get; }

    /// <summary>
    /// Returns the value of the setting
    /// </summary>
    /// <returns></returns>
    T GetValue();

    /// <summary>
    /// Reloads the setting. This should be called automatically, but can also be called manually
    /// </summary>
    void Reload();
```
The configuration can create instances that conform to this Interface with the following methods:
```csharp
var setting = conf.GetSetting<string>("Key", null); //creates a reloadable setting for the value that is defined by Key
var setting = conf.GetCollectionSetting<string>("CollectionKey", null); //creates a reloadable setting for the list of values that is defined by Key
```
As you see, You need to use GetCollectionSetting for collections. GetSetting will not work with collections akin to how GetValue does not either.