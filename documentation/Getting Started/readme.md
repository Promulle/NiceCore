# Getting Started

To get a NiceCore app up and running there are 2 main (default) ways.
You either use the builder route (which requires a reference to NiceCore.Rapid) with UseDefaults() (which requires a reference to NiceCore.Rapid.Default)
###  Application Builder
Default way of getting the app to run:
```csharp
var app = NcApplicationBuilder
        .Create()
        .UseDefaults() // <- this function requires a reference to NiceCore.Rapid.Default
        .Build();
```
Using this approach all default components will be used. And the app version will be retrieved from the 
assembly version of the entry assembly.
### Dependency Injection
NiceCore is based around dependency injection in various ways. Services that you want to inject via container must be registered first.
To register a service to the container you have multiple options:
* Manual:
```csharp
    //obtain a container in some way
    container.Register<TInterface, TImplementation>(); // generic register based on interface and impl type
    container.RegisterInstance<TInterface>(myInstance); //register a instance for a registration, if there is no registration for this interface, one wll be created
    //register methods have overloads that allow register by name and allow you to set a lifetime (Transient/Singleton)
    //lifetimes are handled a bit differently by the default implementation, more on that in "Configuring the ServiceLocation"
```
* Attribute
```csharp
[Register(InterfaceType = typeof(IMyServiceInterface))] //register for interfaceType
[Register(InterfaceType = typeof(IMyServiceInterface), Name = "myservice")] // register for type and name
[Register(InterfaceType = typeof(IMyServiceInterface), LifeStyles = LifeStyles.Singleton)]// register for type and changed lifestyle
//multiple attributes per class are allowed. But they are not inherited.
public class MyService : IMyServiceInterface
```
* XML files
if enabled, xml files ending in .registration.xml will be used for registering services too. Automated generated example below. 
```xml
    <?xml version="1.0"?>
<XmlRegistrationsFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Registrations>
    <XmlRegistrationInfo>
      <InterfaceType>MyServiceInterface, MyInterfaceDll, Version=1.0.0.0 Culture=neutral, PublicKeyToken=null</InterfaceType>
      <ImplementorType>MyServiceImpl, MyImplDll, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null</ImplementorType>
      <Name xsi:nil="true" />
      <LifeStyle>Transient</LifeStyle>
      <IsExclusive>true</IsExclusive>
    </XmlRegistrationInfo>
  </Registrations>
</XmlRegistrationsFile>
```
So now that all registrations are done, lets get to how to actually use the injection.
NiceCore currently supports multiple ways of resolving registered services.
    * Manual
    ```csharp
    var instance = container.Resolve<TInterface>();
    var instanceName = container.Resolve<TInterface>("namedInstance");
    ```
    * Property Injection
    public properties of a resolved class are resolved aswell.
    ```csharp
    public class MyClass : MyInterface
    {
        public IMyService MyService {get; set;} // if an instance of MyClass is resolved, the Property MyService will be resolved too.
    }
    ```
    * Constructor Injection
    ```csharp
    public class MyClass : MyInterface
    {
        public MyClass(IMyService myService) //if IMyService has a registered impl and the instance of MyClass is resolved, myService will be set to a resolved instance
        {
            
        }
    }
    ```

Example for exchanging components :
```csharp
var customizedApp = NcApplicationBuilder.Create()
        .Logger.With(new MyLoggerImpl())
        .Container.With(new MyContainerImpl())
        .ApplicationVersion.With(new Version("0.0.0.1"))
        .InitAndConfigure();
```
In this example you see there is no configuration needed so none is created.
But the servicecontainer and discovery are swapped for custom implementations.
Example using injection:
```csharp
var customizedApp = NcApplicationBuilder.Create()
        .Logger.Inject<INewLogger>() //<- INewLogger has to conform to the ILoggingService interface
        .Container.With(new MyContainerImpl())
        .ApplicationVersion.WithEntryAssembly()
        .InitAndConfigure();
```
In this example the implementation for the logger will be resolved by the container, IF you provided one.
When InitAndConfigure is run, the NcApplication object that is created will be returned and registered in the container,
IF you provided one.
So after this has run you can just use injection to access the app class:
```csharp
public class MyServiceThatNeedsTheNcApp()
{
    public INcApplication Application {get; set;}
    public SomeMethod()
    {
        if (Application != null)
        {
            var importantNumber = Application.Configuration.GetValue<int>("Key");
        }
    }
}
```
### With Or Inject?
There are two functions that can be used on application properties to assign value to them:
```csharp
With<T>(T instance) // <- uses instance as value
With<T>() // <- creates an instance of t as value
Inject<T>() // <- resolves using the container
```
Using With will delete all discovered registrations of t from the container and register the used/created
instance so that injections stay reliable.
This does not happen if the property it is called on is a ICollectionPropertyBuilder of T.
Inject on the other hand does not interact with the registrations at all except from the use of container.resolve.
Values provided by inject are not going to be registered.
An important note is that not all components can be assigned by Inject.
### Manual
Of course you can build the necessary components yourself and register them to your liking.
But the INcApplication interface and implementations are currently housed in NiceCore.Rapid so if you want to have
the application class as bundle of components you will still need a reference to NiceCore.Rapid.
But here is an example of building the most important components manually.
```csharp
var container = new DefaultServiceContainer();
var discovery = new DefaultRegistrationDiscovery()
{
    Container = container,
};
discovery.Discover();
container.RegisterInstance<IServiceContainer>(container);
```
So for more components you would use similar code.

