﻿# Configuration
To use remote eventing with NiceCore you have to configure atleast one handler.

Here is an example using NiceCore.Rapid to configure the app while adding an event handler. (here the rabbitmq implementation that can be found in NiceCore.Eventing.RabbitMQ)
```csharp
//Create a rabbitmq handler configuration
  var cfg = new RabbitMQHandlerConfig()
  {
    HostName = hostName, //adress of the rabbitmq server
    Password = "yourUserName", //rabbitmq user name
    UserName = "yourPassword", //rabbitmq password
    VirtualHost = "/", //Rabbitmq specific virtualhost setting
    UsedExchangeType = ExchangeType.Fanout, //Type of exchange to use
    ApplicationQueueIdentifier = "Test-Sender" //Application Identifier, this needs to be provided to be able to have multiple queues. Has to be unique for each installed instance.
   };
   var app = NcApplicationBuilder.Create()
        .Discovery.With<LegacyRegistrationDiscovery>()
        .Container.With<DefaultServiceContainer>()
        .Container.DependsOn(r => r.Plugins.Add(new InitializePlugin()))
        .Container.DependsOn(r => r.Plugins.Add(new ReleaseOnDisposePlugin()))
        .EventingAdapter.Inject<IEventingAdapter>() //You need to declare an impl of IEventingAdapter but there is a default one that delegates to all handlers, so in the default case just use this one
        .UsedEventingHandlers.Add(EventingHandler.For<RabbitMQEventingHandler>().ConfiguredBy(cfg)) //This line adds a handler that is configured by the config we created before
        .Build();
```
At this point, if a messageService has been found, it will be linked to all registered event handlers.

For specific configuration parameters etc. See the corresponding implementations documentation.