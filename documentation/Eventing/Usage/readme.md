﻿# Usage
To use the eventing from NiceCore you just have to do the following steps to use the eventing:
## Defining messages
```csharp
[ProtoContract] //required Annotation from protobuf-net
public class TestObject
{
	[ProtoMember(1)] //required Annotation from protobuf-net
	public string Name {get; set;}
}

public class ExampleMessage : BaseBinaryFormattedMessage<TestObject>
{
	/// <summary>
	/// TOPIC
	/// </summary>
	public static readonly string TOPIC = "Test-Object-Message";

	/// <summary>
	/// TOPIC
	/// </summary>
	public override string MessageTopic { get; } = TOPIC;
}
```
This example defines a message for the Topic "Test-Object-Message", that holds the value as an instance of TestObject. The value will be formatted using binary formatting.
(Binary formatting requires the annotations for protobuf-net. See https://github.com/protobuf-net/protobuf-net for more details)

### Kinds of messages
Currently existing base implementations of messages:
- BaseJSONMessage -> Base for JSON formatted messages where you have to define the encoding
- BaseUTF8JSONMessage -> Base for JSON formatted messages with utf8 encoding
- BaseBinaryFormattedMessage -> Base for binary formatted messages that use protobuf-net for serialization


## Calling the MessageService
To now consume/send message you only have to call the correct methods.

### Consuming
To consume a message sent through the eventing you have to create a function that receives and processes the message and then register it using register.

```csharp
public void ExampleProcessingMethod(ExampleMessage i_Message)
{
	//Do Stuff with message
}

MessageService.Register<ExampleMessage, TestObject>(ExampleProcessingMethod);
```
Any time a message of type "ExampleMessage" is send locally or received from a remote server, ExampleProcessingMethod will be called.
You can register multiple functions for the same MessageType.
Methods will not process the same message multiple times if the call processing method did not throw an exception.
The MessageService identifies messages by the Message.Value.Identifier field, which is automatically filled when a message instance is created.

### Sending
To send a message simply call:
```csharp
var msg = new ExampleMessage()
{
	Value = new MessageValue<TestObject>()
	{	
		Value = new TestObject { Name = "Hans Paul" }
	}
};
// or the nicer api (slightly slower, performance fix in https://gitlab.com/Promulle/NiceCore/-/issues/49 )
var msg = MessageHelper.Create<ExampleMessage, TestObject>(new TestObject { Name = "Hans Paul" });

MessageService.SendMessage<ExampleMessage, TestObject>(msg);
```
This will call all registered functions and forward the message to all added handlers.