﻿# Eventing Support in NiceCore
NiceCore provides abstractions for connecting your application to eventing servers.
These reside in NiceCore.Eventing and any NiceCore.Eventing.* dlls.

## Concept
The general concept is the following:
NiceCore differentiates between two kinds of eventing:
- Local (MessageService calling registered functions for the message that was sent)
- Remote (Reaction to messages sent from a remote eventing server/sending messages to remote server)

Both of these can be used completely integrated.
Example:
```csharp
MessageService.Register<TestMessage, string>(MyFunction) 
//This registers MyFunction to be called if a message from type TestMessage is sent locally or received from a remote server
MessageService.SendMessage<TestMessage, string>(new TestMessage() { Value = new MessageValue<string>() {Value = "Hello" }});
//This sends the provided message instance -> locally all registered functions are called and the message is forwareded to any remote eventing handler 
```

### Handlers
EventingHandlers are the actual implementations that communicate with the remote eventing server.
(Currently there is only an implementation for RabbitMQ)
These handlers are defined by the IEventingHandler interface
```csharp
    /// <summary>
    /// Event that is fired when any message was received from the actual server and converted to an IObjectMessage
    /// </summary>
    event EventHandler<IObjectMessage> MessageReceivedEvent;

    /// <summary>
    /// Configuration for this handler
    /// </summary>
    IHandlerConfig HandlerConfiguration { get; set; }

    /// <summary>
    /// Sends a message
    /// </summary>
    /// <typeparam name="TMsg"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <param name="i_Message"></param>
    /// <returns></returns>
    Task Send<TMsg, TValue>(TMsg i_Message) where TMsg : IMessage<TValue>;

    /// <summary>
    /// Sends a message
    /// </summary>
    /// <param name="i_Message"></param>
    /// <returns></returns>
    Task SendObjectMessage(IObjectMessage i_Message);

    /// <summary>
    /// prepares for consuming for message of a topic
    /// </summary>
    /// <param name="i_Topic"></param>
    void Subscribe<TMsg, TValue>(string i_Topic) where TMsg : IMessage<TValue>;

    /// <summary>
    /// Not generic overload for subscribe
    /// </summary>
    /// <param name="i_Topic"></param>
    /// <param name="i_MessageType"></param>
    void Subscribe(string i_Topic, Type i_MessageType);

    /// <summary>
    /// Unsubscribes this handler from i_Topic
    /// </summary>
    /// <param name="i_Topic"></param>
    void Unsubscribe(string i_Topic);
```

You can directly call the handler if you really only want to send a message to the remote eventing server, but It is recommended to always use the MessageService.
This is because the message service will be linked to any eventhandler registered.

As you might have guessed by now, this system supports the usage of multiple different eventhandler implementations at once, if you want to receive/send messages from/to multiple remote servers.

### Topics
Topics are used in NiceCore as string identifiers of message types. So there should be a unique value for each type of message.
More see the usage documentation.