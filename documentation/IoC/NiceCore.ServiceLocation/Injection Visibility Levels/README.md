﻿# Injection Visibility Levels
By default, only "Public" properties will be injected by the container.
This can be changed in 2 different ways:

- Injection Visibility Level Attribue applied to the class in question, "Max" here means going from lowest (Public) to highest (private).
    ```csharp
    /// <summary>
  /// Impl where the protected property should be injected
  /// </summary>
  [InjectionVisibilitity(MaxInjectionVisibility = Interface.MethodVisibility.Protected)]
  public class ClassWithDifferentVisibility : IClassWithDifferentVisibility
  {
    /// <summary>
    /// Test Class
    /// </summary>
    protected ITestClass TestClass { get; set; }

    /// <summary>
    /// Second property
    /// </summary>
    public ISingletonTest SingletonTestProperty { get; set; }
    }
    ```
- Overriding the check for which properties to be injected into -> Properties To Inject Service
    ```csharp
    /// <summary>
    /// Service for getting properties to be injected
  /// </summary>
  public interface IPropertiesToInjectService
  {
        /// <summary>
        /// Gets all properties of the type of i_Instance that match/are less than i_MaxVisbility
        /// </summary>
        /// <param name="i_Instance"></param>
        /// <param name="i_MaxVisibility"></param>
        /// <returns>List of property infos that can be injected</returns>
        IEnumerable<PropertyInfo> GetPropertiesToInject(object i_Instance, MethodVisibility i_MaxVisibility);
  }

    ///implement the your version of the service:
  public class ExamplePropertiesToInjectService: IPropertiesToInjectService
  {
        /// <summary>
        /// Gets all properties of the type of i_Instance that match/are less than i_MaxVisbility
        /// </summary>
        /// <param name="i_Instance"></param>
        /// <param name="i_MaxVisibility"></param>
        /// <returns>List of property infos that can be injected</returns>
        public IEnumerable<PropertyInfo> GetPropertiesToInject(object i_Instance, MethodVisibility i_MaxVisibility)
        {
            return i_Instance.GetType().GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
        }
  }
    ///set service in app builder (or directly)
    NcApplicationBuilder
        .Create()
        .Container.With<DefaultServiceContainer>()
        .Container.DependsOn(r => r.PropertiesToInjectService = new ExamplePropertiesToInjectService())
    ```