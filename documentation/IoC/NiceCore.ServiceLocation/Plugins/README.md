# Dependency-Injection Lifecycle Plugins

NiceCore provides the ability to run code when an instance has been released/resolved from the container.

This enables functionality to be used by different container implementations easily.

To use a plugin add it to the IServiceContainer.Plugins collection.

```c#
public void MyFunc()
{
    var container = new DefaultServiceContainer();
    container.Plugins.Add(new InitializePlugin()); //Initialize plugin initializes instances that inherit from IInitializable and are marked with InitializeOnResolveAttribute
    //to activate the plugins you either call 
    container.EnablePlugins() // or container.Initialize() because initialize calls EnablePlugins in the default implementation
}
```

## List of Avalailable Plugins

- InitializePlugin - initializes instances that inherit from IInitializable and are marked with InitializeOnResolveAttribute

- ReleaseOnDisposePlugin - releases an instance from the container if it is dispose


## Creating your own plugin

  All plugins must implement IDependencyInjectionLifecyclePlugin. you can simplify this with inheriting from BasePlugin

```c#
public class TestInjectionPlugin : BasePlugin
{
    //function that you can use to prepare the plugin before use. Return whether the preparation was successfull. If you return false, the plugin will not be activated
    protected override bool InternalEnablePlugin(IServiceContainer i_Container)
    {
      return true;
    }
    //this is called when an instance was released. It receives the released instance
    protected override void InternalInstanceReleased(object t_Instance)
    {
     
    }
    //this is called when an instance has been resolved. It receives the instance that has been resolved and for what type that instance was registered. This only happens on new Resolves. You wont get singletons here. The registration type refers to the instance itself and not to a parent object if the element has been resolved as property of the parent object.
    protected override void InternalInstanceResolved(object t_Instance, Type i_InterfaceType)
    {
     
    }
  } 
```

