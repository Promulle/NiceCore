# Constrained Registrationdiscovery
If you use the DefaultRegistrationDiscovery in directories with a lot of DLL files, the NiceCore-app might suffer from heavy ram usage. There is the ability the restrict which DLL files are searched. To create such a restriction you can use .ncmodules files. A .ncmodules file consists of names of DLL files that should be searched. You can use absolute paths or just the name. Non-absolute paths are combined with the specified search directories.
Example .ncmodules file:
```
NiceCore.dll
NiceCore.Services.Default.dll
//This is a comment
;This is also a comment
```
Using this example file tells the discovery to only search in NiceCore.dll and NiceCore.Services.Default.dll.
So only attribute registrations in those dll files will be used.
For these .ncmodules files to work, they need to be created in the directory where the NiceCore DLL files reside.
## Configuring constrained discovery
To enable constrained discovery you need to set the "AssemblyTargetMode" property of the discovery  to either "Constrained" or "ConstrainedSingleFile". The "TargetMode" property has to be set to either "Assemblies" or "AssembliesAndFiles".
Example using the app builder:
```csharp
var custApp = NcApplicationBuilder.Create()
        .Discovery.DependsOn(r => r.AssemblyTargetMode = AssemblyProvidingModes.Constrained)
        .UseDefaults()
        .Build();
```
Example without app builder:
```csharp
var container = new DefaultServiceContainer();
var discovery = new DefaultRegistrationDiscovery()
{
	Container = container,
	AssemblyTargetMode = Constrained,
	TargetMode = AssembliesAndFiles,
};
discovery.Discover();
```
## Constrained vs ConstrainedSingleFile
The modes "Constrained" and "ConstrainedSingleFile" differ in the way how module files are provided. When using "ConstrainedSingleFile" only the NiceCore.ncmodules file will be used. In the "Constrained" mode all files ending on .ncmodules are used.