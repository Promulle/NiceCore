﻿# Fetching Relations
Besides the typical .Fetch/.ThenFetch etc. methods NiceCore also supports a different way of fetching object graphs: FetchStrategies.

Simple example:
```csharp
var fetchStrategy = FetchStrategies.Create("ExampleProperty.ThenProperty.DeepProperty");
conv.Query<ExampleObject>()
    .ApplyFetchStrategy(fetchStrategy) //this applies the relation to the query
    .ToList();
```
In the example the object graph would look as follows:

```
ExampleObject
    - ExampleProperty (type: ThenExampleObject)
        - ThenProperty (type: SecondThenExampleObject)
            - DeepProperty (type: DeepExampleObject)
```
The resulting instances of ExampleObject should have all these properties filled with the correct objects.
You can define multiple relations per IFetchStrategy instance.