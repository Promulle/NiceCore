﻿# Enabling retrying of connections

NiceCore can retry creating a connection or certain operations if the operation failed.
To do this NiceCore uses Polly. (for details see: https://github.com/App-vNext/Polly)
The policies created by Polly can either be passed to the method on a per call basis or put into an instance of
IDefaultConnectionRetryStrategy:
```csharp
var retryPolicy = Policy.Handle<Exception>().Retry(3);
var defaultStrategy = new DefaultConnectionRetryStrategy()
{
    SyncPolicy = retryPolicy, //this is the policy that will be used for sync function calls, use AsyncPolicy for calls that return Task
};
```
If the corresponding policy is not defined, the DefaultConnectionRetryStrategy will just call the function normally.


This strategy can then be passed to the IStorageService 
```csharp
  /// <summary>
  /// Service for interacting with defined storage Connections/Conversations
  /// </summary>
  public interface IStorageService : IService
  {
    /// <summary>
    /// All query limiting objects
    /// </summary>
    IEnumerable<IQueryLimiter> Limiters { get; }

    /// <summary>
    /// Strategy that should be used when creating a connection errors out
    /// </summary>
    IDefaultConnectionRetryStrategy ConnectionRetryStrategy { get; set; }

    /// <summary>
    /// Whether Initialize initializes the service completely or if service should initialize parts if needed
    /// </summary>
    bool EnableFullInitialize { get; set; }

    /// <summary>
    /// Container managing registrations
    /// </summary>
    IServiceContainer Container { get; set; }

    /// <summary>
    /// returns a new Conversation which is identified by name
    /// </summary>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    IConversation Conversation(string i_Name);
  }
```
To achieve this in a scenario using rapid call the .DependsOn function with your custom strategy as parameter.
If a conversation was created by a storage service that has the DefaultConnectionRetryStrategy defined, every call that does not explicitly defines a strategy to use, will use the default strategy instead.