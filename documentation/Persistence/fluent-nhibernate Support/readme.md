# Support for FluentNhibernate
For more information regarding FluentNhibernate:
https://github.com/FluentNHibernate/fluent-nhibernate
NiceCore.Storage.NH does support the FluentNHibernate library.
It does however not support the automapping aspect of it.
Fluent mapping example:
```csharp
  /// <summary>
  /// Mapping for fluently mapped entity
  /// </summary>
  [Register(InterfaceType = typeof(IFluentlyMapped))] // this is needed for the storage service to pick this mapping up
  public class FluentlyMappedEntityMapping : ClassMap<FluentlyMappedEntity>, IFluentlyMapped //so your mapping has to implement IFluentlyMapped too.
  {
    /// <summary>
    /// Default constructor
    /// </summary>
    public FluentlyMappedEntityMapping()
    {
      Id(r => r.ID);
      Map(r => r.Name);
      References(r => r.Entity);
      Table(nameof(FluentlyMappedEntity));
      SchemaAction.All();
    }
  }
```
Fluent mapping convention example:
```csharp
/// <summary>
  /// Example fluent convention
  /// </summary>
  [Register(InterfaceType = typeof(IFluentConvention))] //register convention 
  public class FluentConventions : IFluentConvention // implement IFluentConvention
  {
    /// <summary>
    /// Returns conventions
    /// </summary>
    /// <returns></returns>
    public IConvention[] GetConventions() //Impl of GetConventions -> this is called by the storage service
    {
      return new IConvention[] { ConventionBuilder.Id.Always(r => r.Column("Id")) };
    }
  }
```
Using it has some problems though:
* The dll the mappings are in has to reference FluentNHibernate
* The dll the mappings are in has to reference NiceCore.Storage.NH

So when using FluentNHibernate you lose the ability to abstract what kind of database access you use.