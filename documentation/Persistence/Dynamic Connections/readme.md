﻿# Dynamic Connections
## Important Types
NiceCore supports defining connections at runtime using the IConnectionConfigurationManagementService Interface
```c#
/// <summary>
/// Service for managing existing configurations based on connection definitions
/// </summary>
public interface IConnectionConfigurationManagementService<TDefinition, TConfig> where TDefinition: IConnectionDefinition
```
Type Parameters:
- TDefinition = Type used for connection definitions, needs to implement IConnectionDefinition
- TConfig = actual type of config saved/retrieved for each connection name

The service that implements this controls how configs are created/stored/retrieved.

and IConnectionDefinitionManagementService
```c#
public interface IConnectionDefinitionManagementService
```

This is a simple lookup service for the currently defined connection definitions.

Since ConnectionDefinition and the config are intertwined, the connection name always refers to both.

This also means that any NiceCore.Storage implementation must also implement:
- IConnectionConfigurationManagementService

## Usage - Adding a new Connection
(Implementing such a service will be discussed in a different document. This section will use the NiceCore.Storage.NH Implementation as example for usage)

Connections can be dynamically added in 2 ways:
- From model (an in memory model that gets translated to the actual config retrieved when a conversation is requested but its connection name does not have a session factory yet)
- From definition (the connection definition defining the connection. The instance that implements IConnectionDefinition must contain the information used to create the configuration instance)

Normally, adding a configuration from model should also add a corresponding ConnectionDefinition.

Both services should be registered with NiceCores DependencyInjection system, so you can just inject the service you want to get an instance for it.

IConnectionConfigurationManagementService has to be retrieved with its correct (and specific) type parameters.

If you want to add a connection you can just take a look at this snippet from the tests:
```c#
var service = Container.Resolve<IConnectionConfigurationManagementService<ConnectionDefinition, Configuration>>();
var newConnectionName = Guid.NewGuid().ToString();
var nhModel = new NHConfigModel()
{
    ConnectionString = $"Data Source={NiceCoreStorageNHTestsConstants.GetFileName(GetType())};Version=3",
    Dialect = "NHibernate.Dialect.SQLiteDialect",
    DriverClass = "NHibernate.Driver.SQLite20Driver",
    ShowSql = "false"
};
var connDefinition = new ConnectionDefinition()
{
    Name = newConnectionName,
    AllowAuditing = true,
    ConfigFile = null,
    SchemaUpdate = true,
    UseSerialization = false
};
await service.AddConfigurationFromConfigurationModel(newConnectionName, nhModel, new List<IDomainBuilder>() { new TestDomainBuilder() }, connDefinition).ConfigureAwait(false);
```
### Step by Step (I will ignore the connection name generation)
To start, we retrieve an instance of the connection configuration management service with the nh specific type parameters
```c#
var service = Container.Resolve<IConnectionConfigurationManagementService<ConnectionDefinition, Configuration>>();
```
Then we create the nhconfigmodel (the class that will be used) as model for the configuration
```c#
var nhModel = new NHConfigModel()
{
    ConnectionString = $"Data Source={NiceCoreStorageNHTestsConstants.GetFileName(GetType())};Version=3",
    Dialect = "NHibernate.Dialect.SQLiteDialect",
    DriverClass = "NHibernate.Driver.SQLite20Driver",
    ShowSql = "false"
};
```
The next step is optional, since an implementation of IConnectionConfigurationManagementService has to provide a reasonable default definition.
(But I needed SchemaUpdate to be true so I pass a custom one)
```c#
var connDefinition = new ConnectionDefinition()
{
    Name = newConnectionName,
    AllowAuditing = true,
    ConfigFile = null,
    SchemaUpdate = true,
    UseSerialization = false
};
```
To finally add the connection we just call this method:
```c#
await service.AddConfigurationFromConfigurationModel(newConnectionName, nhModel, new List<IDomainBuilder>() { new TestDomainBuilder() }, connDefinition).ConfigureAwait(false);
```
After this call has finished, newConnectionName can be used to retrieve connections. This does not affect running conversations.

Notes:
- DomainBuilders might not be needed, depending on the use case, pass an empty list then