﻿# Cascading relations
NiceCore supports relations that enable client based cascading in backend based saveOrUpdate/Delete operations.
To understand this you should know what basic database relations are.
To create such an relation just use the static helper API:
```csharp
var cascades = new List<ICascadingRelation>()
{
    CascadingRelations.CreateManyToOne(nameof(TestEntity.OtherEntity), new List<ICascadingRelation>()
    {
        CascadingRelations.CreateManyToOne(nameof(ReferencedEntity.ThenFetchEntity))
    }),
    CascadingRelations.CreateOneToMany(nameof(TestEntity.OtherEntities), nameof(ReferencedEntity.ParentId))
};
```
This creates a relation of manyToOne from the parent object "TestEntity" to it's property "OtherEntity" with the manyToOne subrelation 
from "OtherEntity" to it's property "ThenFetchEntity" and another oneToMany relation from the parent "TestEntity" to a collection of children which are
held in property "OtherEntities". 

This relation also defines the property "ParentId" as the place to store the id of the parent object after the parent object has been saved.

The data model would look like this:
```csharp
var ent = new TestEntity()
{
    Name = "EntityManyToOne",
    OtherEntity = new ReferencedEntity()
    {
        Name = "CascadedEntity",
        ThenFetchEntity = new ThenEntity()
        {
          Name = "DeepCascadedEntity"
        }
        },
        OtherEntities = new HashSet<ReferencedEntity>()
        {
          new ReferencedEntity()
          {
            Name = "ManyToOneEntityOne"
          },
          new ReferencedEntity()
          {
            Name = "ManyToOneEntityTwo"
          }
        }
   }
};
```
The beforementioned cascade relations (which can indefinitely reused since they are only a model and are not changed by any call using them) can then be passed to SaveOrUpdateAsync or DeleteAsync:
```csharp
//save or update
await conv.SaveOrUpdateAsync<TestEntity, long>(ent, Policy.Handle<Exception>().RetryAsync(1), cascades).ConfigureAwait(false);
//delete
await conv.DeleteAsync<TestEntity, long>(ent, Policy.Handle<Exception>().RetryAsync(1), cascades).ConfigureAwait(false);
```
Currently only these 2 async methods are supported. There might be support for different and even sync calls later.