﻿# Storage messages
NiceCore supports messages sent for storage events.

There are 3 different types:
- Created (EntityCreatedStorageMessage)
- Deleted (EntityDeletedStorageMessage)
- Updated (EntityUpdatedStorageMessage)

All of these messages contain the entity that was operated on.
So the ID property will be filled on Created for example.

## Usage
To listen for these messages simply register a handler for the specific type:
```csharp
msgService.Register<EntityDeletedStorageMessage<TestEntity, long>, TestEntity>(EntityDeleted);
msgService.Register<EntityCreatedStorageMessage<TestEntity, long>, TestEntity>(EntityCreated);
msgService.Register<EntityUpdatedStorageMessage<TestEntity, long>, TestEntity>(EntityUpdated);

//Handlers:
private void EntityDeleted(EntityDeletedStorageMessage<TestEntity, long> i_Message)
{
    var res = i_Message.GetActualValue();
    if (res.Success) //success means there is a value that differs from default(T)
    {
        //Do stuff with this
    }
}

private void EntityCreated(EntityCreatedStorageMessage<TestEntity, long> i_Message)
{
    var res = i_Message.GetActualValue();
    if (res.Success) //success means there is a value that differs from default(T)
    {
        //Do stuff with this
    }
}

private void EntityUpdated(EntityUpdatedStorageMessage<TestEntity, long> i_Message)
{
    var res = i_Message.GetActualValue();
    if (res.Success) //success means there is a value that differs from default(T)
    {
        //Do stuff with this
    }
}
```
