﻿# Lifecycle events
NiceCore supports lifecycle events for the possible events of an INcApplication.
All event implementations should inherit from IApplicationLifecycleEvent:
```csharp
public class ExampleLifecycleEvent : IApplicationLifecycleEvent
{
    /// <summary>
    /// The Stage this should be executed at (see below for more information)
    /// </summary>
    public ApplicationLifecycleStages Stage { get; } = ApplicationLifecycleStages.Starting;

    /// <summary>
    /// This indicates if the execution of this should be awaited by the application or whether the application
    /// should just precede without waiting
    /// </summary>
    public bool BlocksCurrentStage { get; } = false;

    /// <summary>
    /// Function that is executed, can be async
    /// </summary>
    /// <returns></returns>
    public Task Execute()
    {
      //Do stuff here
    }
}
```
The code that is defined in Execute is executed at the stage that is assigned to Stage.
Available stages:
```csharp
public enum ApplicationLifecycleStages
{
    /// <summary>
    /// App.Run has just been called
    /// </summary>
    Starting,

    /// <summary>
    /// All things that were executed in App.Run have finished (possibly excluding async event executions)
    /// </summary>
    Started,

    /// <summary>
    /// App.Shutdown has just been called
    /// </summary>
    Shuttingdown,

    /// <summary>
    /// All things that were executed in App.Shutdown have finished (possibly excluding async event executions)
    /// </summary>
    Shutdown
}
```
To use the defined events just add them when creating the app builder, for example:
```csharp
var app = NcApplicationBuilder
        .Create()
        .LifecycleEvents.AddInjected<IApplicationLifecycleEvent>() //this injects all lifecycle events registered to the default interface
        .UseDefaults()
        .Build();
```