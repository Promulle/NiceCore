SET command=dotnet build
%command% .\NiceCore\NiceCore\NiceCore.csproj							    	
%command% .\NiceCore\NiceCore.ServiceLocation\NiceCore.ServiceLocation.csproj  	
%command% .\NiceCore\NiceCore.Services\NiceCore.Services.csproj					
%command% .\NiceCore\NiceCore.Services.Default\NiceCore.Services.Default.csproj 
%command% .\NiceCore\NiceCore.Hosting\NiceCore.Hosting.csproj					
%command% .\NiceCore\NiceCore.Hosting.Default\NiceCore.Hosting.Default.csproj   
%command% .\NiceCore\NiceCore.Storage\NiceCore.Storage.csproj					
%command% .\NiceCore\NiceCore.Storage.NH\NiceCore.Storage.NH.csproj 			
%command% .\NiceCore.UI\NiceCore.UI\NiceCore.UI.csproj                       	
%command% .\NiceCore.UI\NiceCore.UI.Avalonia\NiceCore.UI.Avalonia.csproj