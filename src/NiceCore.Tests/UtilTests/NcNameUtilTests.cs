using System.Collections.Generic;
using FluentAssertions;
using NiceCore.Text;
using NUnit.Framework;

namespace NiceCore.Tests.UtilTests
{
  [TestFixture]
  [Parallelizable]
  public class NcNameUtilTests
  {
    private const string BASE_NAME = "baseName";
    private const string BASE_NAME_PLUS_ZERO = "baseName (0)";
    private const string BASE_NAME_PLUS_ONE = "baseName (1)";
    private const string BASE_NAME_PLUS_TWO = "baseName (2)";
    
    [Test]
    [Parallelizable]
    public void TestGetNextNotRepeatingNameNoExisting()
    {
      var name = NcNameUtils.GetNextNotRepeatingName(BASE_NAME, null, false);
      name.Should().Be(BASE_NAME);
    }
    
    [Test]
    [Parallelizable]
    public void TestGetNextNotRepeatingNameNoExistingAppendOnZero()
    {
      var name = NcNameUtils.GetNextNotRepeatingName(BASE_NAME, null, true);
      name.Should().Be(BASE_NAME_PLUS_ZERO);
    }
    
    [Test]
    [Parallelizable]
    public void TestGetNextNotRepeatingNameEmptyExisting()
    {
      var name = NcNameUtils.GetNextNotRepeatingName(BASE_NAME, new List<string>(), false);
      name.Should().Be(BASE_NAME);
    }
    
    [Test]
    [Parallelizable]
    public void TestGetNextNotRepeatingOneExisting()
    {
      var name = NcNameUtils.GetNextNotRepeatingName(BASE_NAME, new List<string>()
      {
        BASE_NAME
      }, false);
      name.Should().Be(BASE_NAME_PLUS_ONE);
    }
    
    [Test]
    [Parallelizable]
    public void TestGetNextNotRepeatingTwoExisting()
    {
      var name = NcNameUtils.GetNextNotRepeatingName(BASE_NAME, new List<string>()
      {
        BASE_NAME,
        BASE_NAME_PLUS_ONE
      }, false);
      name.Should().Be(BASE_NAME_PLUS_TWO);
    }
    
    [Test]
    [Parallelizable]
    public void TestGetNextNotRepeatingNoneMatching()
    {
      var name = NcNameUtils.GetNextNotRepeatingName(BASE_NAME, new List<string>()
      {
        "Hallo",
        "Hallo 2"
      }, false);
      name.Should().Be(BASE_NAME);
    }
    
    [Test]
    [Parallelizable]
    public void TestGetNextNotRepeatingOnlyNotRepeatingNotActualFound()
    {
      var name = NcNameUtils.GetNextNotRepeatingName(BASE_NAME, new List<string>()
      {
        BASE_NAME_PLUS_ONE,
        BASE_NAME_PLUS_TWO
      }, false);
      name.Should().Be(BASE_NAME);
    }
  }
}