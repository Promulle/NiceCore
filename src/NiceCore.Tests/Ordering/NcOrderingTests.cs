﻿using FluentAssertions;
using NiceCore.Ordering;
using NiceCore.Tests.Ordering.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace NiceCore.Tests.Ordering
{
  /// <summary>
  /// Ordering Tests
  /// </summary>
  [TestFixture]
  public class NcOrderingTests
  {
    /// <summary>
    /// Test Order ascending
    /// </summary>
    [Test]
    public void OrderingAscending()
    {
      var list = new List<MyTestOrderClass>()
      {
        new MyTestOrderClass()
        {
          Priority = 3,
          Name = "10"
        },
        new MyTestOrderClass()
        {
          Priority = 1,
          Name = "3"
        },
        new MyTestOrderClass()
        {
          Priority = 0,
          Name = "1"
        },
        new MyTestOrderClass()
        {
          Priority = 2,
          Name = "24"
        }
      };
      var descriptor = new NcOrderDescriptor()
      {
        Direction = NcOrderDirections.Ascending,
        PropertyName = nameof(MyTestOrderClass.Priority)
      };
      var ordered = NcOrderTranslation.Translate(list, new[] { descriptor }).ToList();
      for (var i = 0; i < 4; i++)
      {
        if (ordered[i].Priority != i)
        {
          throw new InvalidOperationException("Collection is not ordered.");
        }
      }
    }

    /// <summary>
    /// Test Order ascending
    /// </summary>
    [Test]
    public void OrderingDescending()
    {
      var list = new List<MyTestOrderClass>()
      {
        new MyTestOrderClass()
        {
          Priority = 3,
          Name = "10"
        },
        new MyTestOrderClass()
        {
          Priority = 1,
          Name = "3"
        },
        new MyTestOrderClass()
        {
          Priority = 0,
          Name = "1"
        },
        new MyTestOrderClass()
        {
          Priority = 2,
          Name = "24"
        }
      };
      var descriptor = new NcOrderDescriptor()
      {
        Direction = NcOrderDirections.Descending,
        PropertyName = nameof(MyTestOrderClass.Priority)
      };
      var ordered = NcOrderTranslation.Translate(list, new[] { descriptor }).ToList();
      for (var i = 0; i < 4; i++)
      {
        if (ordered[i].Priority != (3 - i))
        {
          throw new InvalidOperationException("Collection is not ordered.");
        }
      }
    }

    /// <summary>
    /// Test deep ordering
    /// </summary>
    [Test]
    public void DeepOrdering()
    {
      var list = new List<MyTestDeepOrderClass>()
      {
        new MyTestDeepOrderClass()
        {
          Instance = new MyTestOrderClass()
          {
            Priority = 3
          },
          Name = "Some Random"
        },
        new MyTestDeepOrderClass()
        {
          Instance = new MyTestOrderClass()
          {
            Priority = 1
          },
          Name = "Hans Paul"
        },
        new MyTestDeepOrderClass()
        {
          Instance = new MyTestOrderClass()
          {
            Priority = 0,
          },
          Name = "Heinz Gustav"
        }
      };
      var descriptor = new NcOrderDescriptor()
      {
        Direction = NcOrderDirections.Ascending,
        PropertyName = $"{nameof(MyTestDeepOrderClass.Instance)}.{nameof(MyTestOrderClass.Priority)}"
      };
      var ordered = NcOrderTranslation.Translate(list, new[] { descriptor }).ToList();
      ordered[0].Name.Should().Be("Heinz Gustav");
      ordered[1].Name.Should().Be("Hans Paul");
      ordered[2].Name.Should().Be("Some Random");
    }
  }
}
