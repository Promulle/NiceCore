﻿namespace NiceCore.Tests.Ordering.Model
{
  /// <summary>
  /// Test class
  /// </summary>
  public class MyTestOrderClass
  {
    /// <summary>
    /// Priotiry
    /// </summary>
    public int Priority { get; init; }

    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; init; }
  }
}
