﻿namespace NiceCore.Tests.Ordering.Model
{
  /// <summary>
  /// MyTestDeepOrder Class 
  /// </summary>
  public class MyTestDeepOrderClass
  {
    /// <summary>
    /// Instance
    /// </summary>
    public MyTestOrderClass Instance { get; init; }

    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; init; }
  }
}
