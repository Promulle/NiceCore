﻿using FluentAssertions;
using NiceCore.Base.Caching.Exceptions;
using NiceCore.Base.Instances;
using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace NiceCore.Tests.InstanceFactory
{
  /// <summary>
  /// Class for testing of instance factory
  /// </summary>
  [TestFixture]
  public class NcInstanceFactoryTests
  {
    private static readonly DateTime NEW_TIME = DateTime.Now.AddDays(2);
    private static readonly string NAME = "Hans Paul";
    private static readonly int NUMBER = 42;

    /// <summary>
    /// tests creation of a new instance without parameters
    /// </summary>
    [Test]
    public void CreateNewInstanceNoParameters()
    {
      var inst = NcInstanceFactory.CreateInstance<ExampleClass>();

      inst.Should().NotBeNull();
      inst.Name.Should().BeNull();
      inst.Number.Should().Be(0);
      inst.Time.Should().Be(ExampleClass.TIME);
    }

    /// <summary>
    /// Tests creation of a new Instance with all parameters
    /// </summary>
    [Test]
    public void CreateNewInstanceFullParameters()
    {
      var inst = NcInstanceFactory.CreateInstance<ExampleClass>(NAME, NUMBER, NEW_TIME);

      inst.Should().NotBeNull();
      inst.Name.Should().Be(NAME);
      inst.Number.Should().Be(NUMBER);
      inst.Time.Should().Be(NEW_TIME);
    }

    /// <summary>
    /// Tests creation of instances using any InstanceParameter override
    /// </summary>
    [Test]
    public void CreateNewInstanceWithNullParameter()
    {
      var parameters = new List<InstanceParameter>()
      {
        InstanceParameter.For(null, typeof(string)),
        InstanceParameter.For(42),
        InstanceParameter.For(NEW_TIME)
      };
      var inst = NcInstanceFactory.CreateInstance<ExampleClass>(parameters);

      inst.Should().NotBeNull();
      inst.Name.Should().BeNull();
      inst.Number.Should().Be(NUMBER);
      inst.Time.Should().Be(NEW_TIME);
    }

    /// <summary>
    /// Tests creation of a new instance with 2 parameters
    /// </summary>
    [Test]
    public void CreateNewInstancePartialParameters()
    {
      var inst = NcInstanceFactory.CreateInstance<ExampleClass>(NAME, NUMBER);
      inst.Should().NotBeNull();
      inst.Name.Should().Be(NAME);
      inst.Number.Should().Be(NUMBER);
      inst.Time.Should().Be(ExampleClass.TIME);
    }

    /// <summary>
    /// Tests that the call fails if atleast one parameter is null
    /// </summary>
    [Test]
    public void FailWithNullParameter()
    {
      Action act = () => NcInstanceFactory.CreateInstance<ExampleClass>(null, 10);
      act.Should().Throw<NotSupportedException>();
    }

    /// <summary>
    /// Tests that the call fails if the arguments are passed in the wrong order
    /// </summary>
    [Test]
    public void FailOnMisorderedParameters()
    {
      Action act = () => NcInstanceFactory.CreateInstance<ExampleClass>(NEW_TIME, NUMBER, NAME);
      act.Should().Throw<CacheValueException>().WithInnerException<InvalidOperationException>();
    }
  }
}
