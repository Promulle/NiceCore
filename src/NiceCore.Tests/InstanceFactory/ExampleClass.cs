﻿using System;

namespace NiceCore.Tests.InstanceFactory
{
  /// <summary>
  /// Class with different constructors
  /// </summary>
  public class ExampleClass
  {
    /// <summary>
    /// Time
    /// </summary>
    public static readonly DateTime TIME = DateTime.Now.AddDays(-1);

    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; init; }

    /// <summary>
    /// Number
    /// </summary>
    public int Number { get; init; }

    /// <summary>
    /// Time
    /// </summary>
    public DateTime Time { get; init; } = TIME;

    /// <summary>
    /// empty ctor
    /// </summary>
    public ExampleClass()
    {
    }

    /// <summary>
    /// Partially filling constructor
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Number"></param>
    public ExampleClass(string i_Name, int i_Number)
    {
      Name = i_Name;
      Number = i_Number;
    }

    /// <summary>
    /// fill ctor
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Number"></param>
    /// <param name="i_Time"></param>
    public ExampleClass(string i_Name, int i_Number, DateTime i_Time) : this(i_Name, i_Number)
    {
      Time = i_Time;
    }
  }
}
