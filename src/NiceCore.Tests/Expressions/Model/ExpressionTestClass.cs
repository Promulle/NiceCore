namespace NiceCore.Tests.Expressions.Model
{
  /// <summary>
  /// Expression Test Class
  /// </summary>
  public class ExpressionTestClass
  {
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; } 
    
    /// <summary>
    /// Sub Class
    /// </summary>
    public ExpressionTestSubClass SubClass { get; set; }
  }
}