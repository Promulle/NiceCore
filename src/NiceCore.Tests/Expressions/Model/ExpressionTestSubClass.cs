namespace NiceCore.Tests.Expressions.Model
{
  /// <summary>
  /// Expression Test SubClass
  /// </summary>
  public class ExpressionTestSubClass
  {
    /// <summary>
    /// Number
    /// </summary>
    public int Number { get; set; }
  }
}