using FluentAssertions;
using NiceCore.Storage.Extensions.Expressions;
using NUnit.Framework;

namespace NiceCore.Tests.Expressions.Storage.QueryExtensions
{
  /// <summary>
  /// QueryExtensionExpressionMethodsTests
  /// </summary>
  [Parallelizable]
  [TestFixture]
  public class QueryExtensionExpressionMethodsTests
  {
    /// <summary>
    /// GetApplyFetchStrategyMethod
    /// </summary>
    [Test]
    public void GetApplyFetchStrategyMethod()
    {
      var result = QueryExtensionExpressionMethods.GetApplyFetchStrategyMethod(typeof(string));
      result.Should().NotBeNull();
    }
  }
}