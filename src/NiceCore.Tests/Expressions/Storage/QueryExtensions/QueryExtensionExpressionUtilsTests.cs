using System.Linq;
using System.Linq.Expressions;
using FluentAssertions;
using NiceCore.Storage.Extensions.Expressions;
using NiceCore.Storage.Fetching;
using NUnit.Framework;

namespace NiceCore.Tests.Expressions.Storage.QueryExtensions
{
  /// <summary>
  /// QueryExtensionExpressionUtilsTests
  /// </summary>
  [TestFixture]
  [Parallelizable]
  public class QueryExtensionExpressionUtilsTests
  {
    /// <summary>
    /// Test Call Apply Fetch Strategy
    /// </summary>
    [Test]
    public void TestCallApplyFetchStrategy()
    {
      var fetchStrategyExpression = Expression.Constant(null, typeof(IFetchStrategy));
      var queryExpr = Expression.Constant(null, typeof(IQueryable<string>));
      var callExpr = QueryExtensionExpressionUtils.CallApplyFetchStrategyOnQueryable(queryExpr, fetchStrategyExpression, typeof(string));
      callExpr.Should().NotBeNull();
    }
  }
}