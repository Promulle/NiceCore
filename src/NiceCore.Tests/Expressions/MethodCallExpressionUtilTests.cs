using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using FluentAssertions;
using NiceCore.Expressions;
using NiceCore.Tests.Expressions.Model;
using NUnit.Framework;
using Expression = System.Linq.Expressions.Expression;

namespace NiceCore.Tests.Expressions
{
  /// <summary>
  /// Tests for <see cref="MethodCallExpressionUtils"/>
  /// </summary>
  [TestFixture]
  [Parallelizable]
  public class MethodCallExpressionUtilTests
  {
    /// <summary>
    /// Test Call await To Object on Task <see cref="MethodCallExpressionUtils.CallAwaitToObjectOnTask"/>
    /// </summary>
    [Test]
    [Parallelizable]
    public void TestCallAwaitToObjectOnTask()
    {
      var taskExpr = System.Linq.Expressions.Expression.Constant(TaskWrapper());
      var callExpr = MethodCallExpressionUtils.CallAwaitToObjectOnTask(taskExpr, typeof(string));
      callExpr.Should().NotBeNull();
    }

    private static Task<string> TaskWrapper()
    {
      return Task.FromResult("str");
    }

    /// <summary>
    /// GetCallToArrayExpression
    /// </summary>
    [Test]
    [Parallelizable]
    public void GetCallToArrayExpression()
    {
      var expr = Expression.Parameter(typeof(IEnumerable<string>));
      var call = MethodCallExpressionUtils.OnEnumerable.GetCallToArrayExpression(expr, typeof(string));
      call.Should().NotBeNull();
      var lambda = Expression.Lambda<Func<IEnumerable<string>, string[]>>(call, expr).Compile();

      var list = new List<string>();
      var result = lambda.Invoke(list);
      result.Should().BeOfType<string[]>();
    }

    /// <summary>
    /// GetCallToOrderByExpression
    /// </summary>
    [Test]
    [Parallelizable]
    public void GetCallToOrderByExpression()
    {
      var expr = Expression.Parameter(typeof(IEnumerable<ExpressionTestClass>));
      var call = MethodCallExpressionUtils.OnEnumerable.Ordering.GetCallToOrderByExpression(expr, typeof(ExpressionTestClass), nameof(ExpressionTestClass.Name));
      call.Should().NotBeNull();
    }
    
    /// <summary>
    /// GetCallToOrderByExpression
    /// </summary>
    [Test]
    [Parallelizable]
    public void GetCallToOrderByDescendingExpression()
    {
      var expr = Expression.Parameter(typeof(IEnumerable<ExpressionTestClass>));
      var call = MethodCallExpressionUtils.OnEnumerable.Ordering.GetCallToOrderByDescendingExpression(expr, typeof(ExpressionTestClass), nameof(ExpressionTestClass.Name));
      call.Should().NotBeNull();
    }
    
    /// <summary>
    /// GetCallToOrderByExpression
    /// </summary>
    [Test]
    [Parallelizable]
    public void GetCallToThenByDescendingExpression()
    {
      var expr = Expression.Parameter(typeof(IOrderedEnumerable<ExpressionTestClass>));
      var call = MethodCallExpressionUtils.OnEnumerable.Ordering.GetCallToThenByDescendingExpression(expr, typeof(ExpressionTestClass), nameof(ExpressionTestClass.Name));
      call.Should().NotBeNull();
    }
    
    /// <summary>
    /// GetCallToOrderByExpression
    /// </summary>
    [Test]
    [Parallelizable]
    public void GetCallToThenByExpression()
    {
      var expr = Expression.Parameter(typeof(IOrderedEnumerable<ExpressionTestClass>));
      var call = MethodCallExpressionUtils.OnEnumerable.Ordering.GetCallToThenByExpression(expr, typeof(ExpressionTestClass), nameof(ExpressionTestClass.Name));
      call.Should().NotBeNull();
    }
  }
}