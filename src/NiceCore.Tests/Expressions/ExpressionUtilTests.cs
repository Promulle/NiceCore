using System.Linq.Expressions;
using System.Threading.Tasks;
using FluentAssertions;
using NiceCore.Expressions;
using NiceCore.Tests.Expressions.Model;
using NUnit.Framework;

namespace NiceCore.Tests.Expressions
{
  /// <summary>
  /// Expression Util Tests
  /// </summary>
  [TestFixture]
  [Parallelizable]
  public class ExpressionUtilTests
  {
    /// <summary>
    /// Test Get Generic Task Type
    /// </summary>
    [Test]
    [Parallelizable]
    public void TestGetGenericTaskType()
    {
      var taskType = ExpressionUtils.GetGenericTaskType(typeof(string));
      taskType.Should().NotBeNull().And.Be(typeof(Task<string>));
    }
    
    /// <summary>
    /// Test get Property Expression
    /// </summary>
    [Test]
    [Parallelizable]
    public void TestGetPropertyExpressionSimple()
    {
      var paramExpression = Expression.Parameter(typeof(ExpressionTestClass));
      var expr = ExpressionUtils.GetPropertyExpression(paramExpression, nameof(ExpressionTestClass.Name), new());
      expr.Should().NotBeNull();
      expr.Member.Name.Should().Be(nameof(ExpressionTestClass.Name));
      expr.Member.DeclaringType.Should().Be(typeof(ExpressionTestClass));
    }

    /// <summary>
    /// Test get Property Expression
    /// </summary>
    [Test]
    [Parallelizable]
    public void TestGetPropertyExpressionByCompletePath()
    {
      var paramExpression = Expression.Parameter(typeof(ExpressionTestClass));
      var expr = ExpressionUtils.GetPropertyExpression(paramExpression, $"{nameof(ExpressionTestClass.SubClass)}.{nameof(ExpressionTestSubClass.Number)}", new());
      expr.Should().NotBeNull();
      expr.Member.Name.Should().Be(nameof(ExpressionTestSubClass.Number));
      expr.Member.DeclaringType.Should().Be(typeof(ExpressionTestSubClass));
    }
    
    /// <summary>
    /// Test get Property Expression
    /// </summary>
    [Test]
    [Parallelizable]
    public void TestGetPropertyExpressionByParts()
    {
      var paramExpression = Expression.Parameter(typeof(ExpressionTestClass));
      var expr = ExpressionUtils.GetPropertyExpression(paramExpression, new []{nameof(ExpressionTestClass.SubClass), nameof(ExpressionTestSubClass.Number)}, new());
      expr.Should().NotBeNull();
      expr.Member.Name.Should().Be(nameof(ExpressionTestSubClass.Number));
      expr.Member.DeclaringType.Should().Be(typeof(ExpressionTestSubClass));
    }
  }
}