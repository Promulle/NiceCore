using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using NiceCore.Concurrency.Channels;
using NiceCore.Tests.Concurrency.Model;
using NUnit.Framework;

namespace NiceCore.Tests.Concurrency
{
  /// <summary>
  /// Tests for Callback Channels
  /// </summary>
  [TestFixture]
  public class CallbackChannelTests
  {
    /// <summary>
    /// Tests simple callback channel
    /// </summary>
    [Test]
    public async Task TestSimple()
    {
      var list = new List<string>();
      var channel = CallbackChannelFactory.CreateSimple<string, string>((val, _) =>
      {
        list.Add(val);
        return ValueTask.CompletedTask;
      }, null);
      await channel.WriteAsync("1").ConfigureAwait(false);
      await Task.Delay(10).ConfigureAwait(false);
      list.Should().NotBeEmpty().And.HaveCount(1);
    }

    /// <summary>
    /// Tests Batched Channel
    /// </summary>
    /// <returns></returns>
    [Test]
    public async Task TestBatched()
    {
      var results = new List<BatchResult>();
      var valList = new List<string>();
      for (var i = 0; i < 100; i++)
      {
        valList.Add("Value: " + i);
      }

      var batchNumber = 1;
      var channel = CallbackChannelFactory.CreateBatched<string, string>((val, _) =>
      {
        foreach (var value in val)
        {
          results.Add(new()
          {
            BatchNumber = batchNumber,
            Value = value
          });  
        }
        batchNumber++;
        return ValueTask.CompletedTask;
      }, null, 30);
      foreach (var value in valList)
      {
        await channel.WriteAsync(value).ConfigureAwait(false);
      }
      await Task.Delay(100);
      results.Should().NotBeEmpty();
      var firstBatch = results.FindAll(r => r.BatchNumber == 1);
      var secondBatch = results.FindAll(r => r.BatchNumber == 2);
      var thirdBatch = results.FindAll(r => r.BatchNumber == 3);
      var last = results.FindAll(r => r.BatchNumber == 4);
      firstBatch.Should().NotBeEmpty().And.HaveCount(30);
      secondBatch.Should().NotBeEmpty().And.HaveCount(30);
      thirdBatch.Should().NotBeEmpty().And.HaveCount(30);
      last.Should().NotBeEmpty().And.HaveCount(10);
    }
  }
}