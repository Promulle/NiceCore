namespace NiceCore.Tests.Concurrency.Model
{
  /// <summary>
  /// Model for a result of a batched Reader
  /// </summary>
  public sealed class BatchResult
  {
    /// <summary>
    /// Value
    /// </summary>
    public string Value { get; init; }
    
    /// <summary>
    /// BatchNumber
    /// </summary>
    public int BatchNumber { get; init; }
  }
}