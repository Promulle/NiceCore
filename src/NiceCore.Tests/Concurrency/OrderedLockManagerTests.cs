using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using NiceCore.Concurrency;
using NiceCore.Extensions;
using NUnit.Framework;

namespace NiceCore.Tests.Concurrency
{
  /// <summary>
  /// Ordered Lock Manager Tests
  /// </summary>
  [TestFixture]
  public sealed class OrderedLockManagerTests
  {
    /// <summary>
    /// Test Single Entry
    /// </summary>
    /// <returns></returns>
    [Test]
    public async Task TestSingleKey()
    {
      var lockInstance = new OrderedLockManager<string>();
      const string key = "A";
      var res = new List<int>();
      var firstBurden = await lockInstance.LockItems(key).ConfigureAwait(false);
      res.Add(0);
      firstBurden.Release();
      var secondBurden = await lockInstance.LockItems(key).ConfigureAwait(false);
      res.Add(1);
      secondBurden.Release();
      var thirdBurden = await lockInstance.LockItems(key).ConfigureAwait(false);
      res.Add(2);
      thirdBurden.Release();
      res.Should().HaveElementAt(0, 0)
        .And.HaveElementAt(1, 1)
        .And.HaveElementAt(2, 2);
    }

    /// <summary>
    /// Test MultiKey
    /// </summary>
    [Test]
    public async Task TestMultiKey()
    {
      var lockManager = new OrderedLockManager<string>();
      var keys = new HashSet<string>()
      {
        "A",
        "B",
        "C"
      };
      var res = new List<int>();
      var firstBurden = await lockManager.LockMultiple(keys).ConfigureAwait(false);
      res.Add(0);
      firstBurden.Release();
      var secondBurden = await lockManager.LockMultiple(keys).ConfigureAwait(false);
      res.Add(1);
      secondBurden.Release();
      var thirdBurden = await lockManager.LockMultiple(keys).ConfigureAwait(false);
      res.Add(2);
      thirdBurden.Release();
      res.Should().HaveElementAt(0, 0)
        .And.HaveElementAt(1, 1)
        .And.HaveElementAt(2, 2);
      lockManager.HasLockForKey("A").Should().BeFalse();
      lockManager.HasLockForKey("B").Should().BeFalse();
      lockManager.HasLockForKey("C").Should().BeFalse();
    }

    /// <summary>
    /// Test MultiAccess
    /// </summary>
    [Test]
    public async Task TestMultiAccess()
    {
      var lockManager = new OrderedLockManager<string>();
      var res = new ConcurrentBag<string>();
      var firstKeys = new HashSet<string>()
      {
        "A",
        "B"
      };
      var secondKeys = new HashSet<string>()
      {
        "C",
        "D"
      };
      Func<OrderedLockManager<string>, HashSet<string>, Task> func = async (lockManagerParam, keys) =>
      {
        var burden = await lockManagerParam.LockMultiple(keys).ConfigureAwait(false);
        await Task.Delay(1).ConfigureAwait(false);
        keys.ForEachItem(r => res.Add(r));
        burden.Release();
      };
      var taskOne = func(lockManager, firstKeys);
      var taskTwo = func(lockManager, secondKeys);
      await Task.WhenAll(taskOne, taskTwo).ConfigureAwait(false);
      res.Should().Contain("A").And.Contain("B").And.Contain("C").And.Contain("D");
      lockManager.HasLockForKey("A").Should().BeFalse();
      lockManager.HasLockForKey("B").Should().BeFalse();
      lockManager.HasLockForKey("C").Should().BeFalse();
      lockManager.HasLockForKey("D").Should().BeFalse();
    }

    /// <summary>
    /// Test Key Overlap Access
    /// </summary>
    [Test]
    public async Task TestKeyOverlapAccess()
    {
      var lockManager = new OrderedLockManager<string>();
      var res = new List<string>();
      var firstKeys = new HashSet<string>()
      {
        "A",
        "B"
      };
      var secondKeys = new HashSet<string>()
      {
        "C",
        "D",
        "A"
      };
      var random = new Random();
      Func<OrderedLockManager<string>, HashSet<string>, Task> func = async (lockManagerParam, keys) =>
      {
        var burden = await lockManagerParam.LockMultiple(keys).ConfigureAwait(false);
        await Task.Delay(random.Next(1, 150)).ConfigureAwait(false);
        keys.ForEachItem(r => res.Add(r));
        burden.Release();
      };
      var taskOne = func(lockManager, firstKeys);
      var taskTwo = func(lockManager, secondKeys);
      await Task.WhenAll(taskOne, taskTwo).ConfigureAwait(false);
      res.Should().HaveElementAt(0, "A")
        .And.HaveElementAt(1, "B")
        .And.HaveElementAt(2, "C")
        .And.HaveElementAt(3, "D")
        .And.HaveElementAt(4, "A");
      var thirdRunKeys = new HashSet<string>()
      {
        "B"
      };
      await func(lockManager, thirdRunKeys).ConfigureAwait(false);
      res.Should().HaveElementAt(5, "B");
      lockManager.HasLockForKey("A").Should().BeFalse();
      lockManager.HasLockForKey("B").Should().BeFalse();
      lockManager.HasLockForKey("C").Should().BeFalse();
      lockManager.HasLockForKey("D").Should().BeFalse();
    }
  }
}
