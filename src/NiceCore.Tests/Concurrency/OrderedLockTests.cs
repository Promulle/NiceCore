using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using NiceCore.Concurrency;
using NUnit.Framework;

namespace NiceCore.Tests.Concurrency
{
  /// <summary>
  /// Ordered Lock
  /// </summary>
  [TestFixture]
  public class OrderedLockTests
  {
    /// <summary>
    /// Test
    /// </summary>
    [Test]
    public async Task Test()
    {
      var lockInstance = new OrderedLock();
      var stopWatch = Stopwatch.StartNew();
      await lockInstance.Wait().ConfigureAwait(false);
      lockInstance.Release();      
      stopWatch.Stop();
      Console.WriteLine();
    }
    /// <summary>
    /// Tests that entering one after another is possible
    /// </summary>
    [Test]
    public async Task TestSingleEntry()
    {
      var lockInstance = new OrderedLock();
      var res = new List<int>();
      await lockInstance.Wait().ConfigureAwait(false);
      res.Add(0);
      lockInstance.Release();
      await lockInstance.Wait().ConfigureAwait(false);
      res.Add(1);
      lockInstance.Release();
      await lockInstance.Wait().ConfigureAwait(false);
      res.Add(2);
      lockInstance.Release();
      res.Should().HaveElementAt(0, 0)
        .And.HaveElementAt(1, 1)
        .And.HaveElementAt(2, 2);
    }

    /// <summary>
    /// is Actually Locking
    /// </summary>
    [Test]
    public async Task IsActuallyLocking()
    {
      var lockInstance = new OrderedLock();
      var res = new List<int>();
      var funcs = new List<Func<Task>>();
      for(var i = 0; i <= 100; i++)
      {
        var info = new Info()
        {
          Number = i,
          Id = Guid.NewGuid()
        };
        funcs.Add(async  () =>
        {
          await lockInstance.Wait();
          //await semaphore.WaitAsync();
          //var delay = rand.Next(10, 100);
          await Task.Delay(100);
          //semaphore.Release();
          res.Add(info.Number);
          lockInstance.Release();
        });
      }
      
      var tasks = funcs.ConvertAll(r => r());
      await Task.WhenAll(tasks).ConfigureAwait(false);
      res.Should().HaveCount(101);
      for (var i = 0; i <= 100; i++)
      {
        res[i].Should().Be(i);
      }
    }
  }

  class Info
  {
    public Guid Id { get; init; }
    public int Number { get; init; }  
  }
}