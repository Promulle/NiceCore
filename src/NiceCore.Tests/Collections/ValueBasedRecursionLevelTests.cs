using FluentAssertions;
using NiceCore.Collections;
using NUnit.Framework;

namespace NiceCore.Tests.Collections
{
  /// <summary>
  /// Tests for <see cref="ValueBasedRecursionLevel{T}"/>
  /// </summary>
  [TestFixture]
  public class ValueBasedRecursionLevelTests
  {
    /// <summary>
    /// Tests contains cycle for value
    /// </summary>
    [Test]
    public void TestContainsCyclesForValueOnceInTree()
    {
      var parent = new ValueBasedRecursionLevel<int>()
      {
        Value = 1
      };
      var secondNode = new ValueBasedRecursionLevel<int>()
      {
        Value = 2,
        Parent = parent
      };
      var thirdNode = new ValueBasedRecursionLevel<int>()
      {
        Value = 3,
        Parent = secondNode
      };
      thirdNode.ContainsCycleForValue(2).Should().BeTrue();
    }
    
    /// <summary>
    /// Tests contains cycle for value
    /// </summary>
    [Test]
    public void TestContainsCyclesForValueNoCycle()
    {
      var parent = new ValueBasedRecursionLevel<int>()
      {
        Value = 1
      };
      var secondNode = new ValueBasedRecursionLevel<int>()
      {
        Value = 2,
        Parent = parent
      };
      var thirdNode = new ValueBasedRecursionLevel<int>()
      {
        Value = 3,
        Parent = secondNode
      };
      thirdNode.ContainsCycleForValue(4).Should().BeFalse();
    }
  }
}