namespace NiceCore.Tests.Timers
{
  /// <summary>
  /// Timer Test model
  /// </summary>
  public class TimerTestModel
  {
    /// <summary>
    /// Run
    /// </summary>
    public bool Run { get; set; }
  }
}