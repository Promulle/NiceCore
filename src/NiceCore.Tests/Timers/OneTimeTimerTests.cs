using System;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using NiceCore.Timers;
using NUnit.Framework;

namespace NiceCore.Tests.Timers
{
  /// <summary>
  /// Tests for OneTime Timer
  /// </summary>
  [TestFixture]
  public class OneTimeTimerTests
  {
    /// <summary>
    /// Test Timer Runs
    /// </summary>
    /// <returns></returns>
    [Test]
    public async Task TestTimerExecutesCallback()
    {
      var timerTestModel = new TimerTestModel();
      var timer = new OneTimeTimer<TimerTestModel>(TimeSpan.FromMilliseconds(2), static (i_Model, i_Token) =>
      {
        i_Model.Run = true;
        return ValueTask.CompletedTask;
      }, timerTestModel);
      await timer.Run(CancellationToken.None).ConfigureAwait(false);
      timerTestModel.Run.Should().BeTrue();
    }
    
    /// <summary>
    /// Test Timer Runs
    /// </summary>
    /// <returns></returns>
    [Test]
    public async Task TestTimerDoesNotExecuteCallback()
    {
      var timerTestModel = new TimerTestModel();
      var timer = new OneTimeTimer<TimerTestModel>(TimeSpan.FromMilliseconds(5), static (i_Model, i_Token) =>
      {
        i_Model.Run = true;
        return ValueTask.CompletedTask;
      }, timerTestModel);
      var timerTask = timer.Run(CancellationToken.None);
      timer.Dispose();
      await timerTask.ConfigureAwait(false);
      timerTestModel.Run.Should().BeFalse();
    }
  }
}