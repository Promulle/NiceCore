namespace NiceCore.Tests.Filters
{
  public class ObjectWithNullableEnum
  {
    public MyTestEnum? Enum { get; set; }
  }
}