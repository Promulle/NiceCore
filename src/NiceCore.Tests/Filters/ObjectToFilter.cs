﻿using NiceCore.Base;

namespace NiceCore.Tests.Filters
{
  /// <summary>
  /// Test impl
  /// </summary>
  public class ObjectToFilter
  {
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Description
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// Number
    /// </summary>
    public int Number { get; set; }

    /// <summary>
    /// Time From
    /// </summary>
    public DefaultNcTimeInterval TimeInterval { get; set; }
  }
}
