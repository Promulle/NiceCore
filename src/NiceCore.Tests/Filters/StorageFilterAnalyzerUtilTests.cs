using FluentAssertions;
using NiceCore.Filtering;
using NiceCore.Storage.Filters.Utils;
using NUnit.Framework;
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

namespace NiceCore.Tests.Filters
{
  [TestFixture]
  [Parallelizable]
  public class StorageFilterAnalyzerUtilTests
  {
    [Test]
    [Parallelizable]
    public void TestAnyComplexPropertyPathNoComplexPath()
    {
      var subFilter = NcFilters.CreateAnd(new[]
      {
        NcFilterConditions.CreateEquals("SubProperty", null),
        NcFilterConditions.CreateEquals("SubProperty.ID", null)
      });
      var filter = NcFilters.CreateAnd(new[] {subFilter}, new[]
      {
        NcFilterConditions.CreateEquals("Property", null),
        NcFilterConditions.CreateEquals("Property.ID", null),
      });
      var result = StorageFilterAnalyzerUtils.AnyComplexPropertyPath(filter);
      result.Should().Be(false);
    }
    
    [Test]
    [Parallelizable]
    public void TestAnyComplexPropertyPathConditionComplexPath()
    {
      var subFilter = NcFilters.CreateAnd(new[]
      {
        NcFilterConditions.CreateEquals("SubProperty", null),
        NcFilterConditions.CreateEquals("SubProperty.ID", null)
      });
      var filter = NcFilters.CreateAnd(new []{subFilter},new[]
      {
        NcFilterConditions.CreateEquals("Property", null),
        NcFilterConditions.CreateEquals("Property.OtherProperty.Name", null)
      });
      var result = StorageFilterAnalyzerUtils.AnyComplexPropertyPath(filter);
      result.Should().Be(true);
    }
    
    [Test]
    [Parallelizable]
    public void TestAnyComplexPropertyPathSubFilterConditionComplexPath()
    {
      var subFilter = NcFilters.CreateAnd(new[]
      {
        NcFilterConditions.CreateEquals("SubProperty", null),
        NcFilterConditions.CreateEquals("SubProperty.Name", null)
      });
      var filter = NcFilters.CreateAnd(new []{subFilter},new[]
      {
        NcFilterConditions.CreateEquals("Property", null),
        NcFilterConditions.CreateEquals("Property.ID", null)
      });
      var result = StorageFilterAnalyzerUtils.AnyComplexPropertyPath(filter);
      result.Should().Be(true);
    }
  }
}
