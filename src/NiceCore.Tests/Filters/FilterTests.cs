﻿using FluentAssertions;
using NiceCore.Base;
using NiceCore.Filtering;
using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace NiceCore.Tests.Filters
{
  /// <summary>
  /// Filter tests
  /// </summary>
  [TestFixture]
  public class FilterTests
  {
    private static List<ObjectToFilter> CreateFilterableObjects()
    {
      return new List<ObjectToFilter>()
      {
        new ObjectToFilter()
        {
          Description = "Desc_0",
          Name = "Name_0",
          Number = 1,
          TimeInterval = new DefaultNcTimeInterval()
          {
            TimeFrom = DateTime.Now.AddDays(-1),
            TimeTo = DateTime.Now.AddDays(1)
          },
        },
        new ObjectToFilter()
        {
          Description = "Desc_1",
          Name = "Name_1",
          Number = 2,
          TimeInterval = new DefaultNcTimeInterval()
          {
            TimeFrom = DateTime.Now.AddDays(2),
            TimeTo = DateTime.Now.AddDays(3)
          },
        },
        new ObjectToFilter()
        {
          Description = "Desc_2",
          Name = "Name_2",
          Number = 3,
          TimeInterval = new DefaultNcTimeInterval()
          {
            TimeFrom = DateTime.Now.AddDays(-4),
            TimeTo = DateTime.Now.AddDays(-2)
          }
        },
        new ObjectToFilter()
        {
          Description = "Desc_3",
          Name = "Name_3",
          Number = 4,
          TimeInterval = new DefaultNcTimeInterval()
          {
            TimeFrom = DateTime.Now.AddDays(5),
            TimeTo = DateTime.Now.AddDays(10)
          }
        },
        new ObjectToFilter()
        {
          Description = "Desc",
          Name = "Name",
          Number = 5,
          TimeInterval = new DefaultNcTimeInterval()
          {
            TimeFrom = DateTime.Now.AddDays(11),
            TimeTo = DateTime.Now.AddDays(13)
          }
        },
        new ObjectToFilter()
        {
          Description = "Desc_6",
          Name = "Name",
          Number = 6,
          TimeInterval = new DefaultNcTimeInterval()
          {
            TimeFrom = DateTime.Now.AddDays(-6),
            TimeTo = DateTime.Now.AddDays(-4)
          }
        },
        new ObjectToFilter()
        {
          Description = "Desc_6",
          Name = "ObjectWithAVeryVeryLongName",
          Number = 100,
          TimeInterval = new DefaultNcTimeInterval()
          {
            TimeFrom = DateTime.Now.AddDays(-10),
            TimeTo = DateTime.Now.AddDays(-7)
          },
        },
      };
    }

    [Test]
    public void TestFilterWithNullableEnum()
    {
      var filter = NcFilters.CreateAnd(NcFilterConditions.CreateEquals(nameof(ObjectWithNullableEnum.Enum), MyTestEnum.Less.ToString()));
      var list = new List<ObjectWithNullableEnum>()
      {
        new()
        {
          Enum = MyTestEnum.None
        },
        new()
        {
          Enum = MyTestEnum.Less
        }
      };
      var resList = list.FilterBy(filter, new ()
      {
        AdditionalProviders = AdditionalFilterOperationProviders.Empty()
      }).ToList();
      resList.Should().HaveCount(1);
    }

    /// <summary>
    /// Tests Simple filter case
    /// </summary>
    [Test]
    public void FilterSimpleSingleResult()
    {
      var simpleFilter = NcFilters.CreateAnd(NcFilterConditions.CreateEquals(nameof(ObjectToFilter.Name), "Name_0"));
      var objectsToFilter = CreateFilterableObjects();
      var resList = objectsToFilter.FilterBy(simpleFilter, new ()
      {
        AdditionalProviders = AdditionalFilterOperationProviders.Empty()
      }).ToList();
      resList.Should().HaveCount(1);
      resList.FirstOrDefault().Description.Should().Be("Desc_0");
    }

    /// <summary>
    /// Tests Simple filter case
    /// </summary>
    [Test]
    public void FilterSimpleNoResult()
    {
      var simpleFilter = NcFilters.CreateAnd(NcFilterConditions.CreateEquals(nameof(ObjectToFilter.Name), null));
      var objectsToFilter = CreateFilterableObjects();
      var resList = objectsToFilter.FilterBy(simpleFilter, new ()
      {
        AdditionalProviders = AdditionalFilterOperationProviders.Empty()
      }).ToList();
      resList.Should().HaveCount(0);
    }
    
    [Test]
    public void TestFilterContains()
    {
      var containsFilter = NcFilters.CreateAnd(NcFilterConditions.CreateContains(nameof(ObjectToFilter.Name), "e_3"));
      var objectsToFilter = CreateFilterableObjects();
      var resList = objectsToFilter.FilterBy(containsFilter, new ()
      {
        AdditionalProviders = AdditionalFilterOperationProviders.Empty()
      }).ToList();
      resList.Should().HaveCount(1);
    }

    /// <summary>
    /// Tests Simple filter case
    /// </summary>
    [Test]
    public void FilterSimpleEmptyFilter()
    {
      var simpleFilter = NcFilters.CreateAnd(new List<DefaultNcFilterCondition>());
      var objectsToFilter = CreateFilterableObjects();
      var resList = objectsToFilter.FilterBy(simpleFilter, new ()
      {
        AdditionalProviders = AdditionalFilterOperationProviders.Empty()
      }).ToList();
      resList.Should().HaveCount(objectsToFilter.Count);
    }

    /// <summary>
    /// Tests Simple filter case
    /// </summary>
    [Test]
    public void FilterSimpleNullFilter()
    {
      var objectsToFilter = CreateFilterableObjects();
      var resList = objectsToFilter.FilterBy(null, new ()
      {
        AdditionalProviders = AdditionalFilterOperationProviders.Empty()
      }).ToList();
      resList.Should().HaveCount(objectsToFilter.Count);
    }

    /// <summary>
    /// Tests Simple filter case
    /// </summary>
    [Test]
    public void FilterSimpleMultiResult()
    {
      var simpleFilter = NcFilters.CreateAnd(NcFilterConditions.CreateEquals(nameof(ObjectToFilter.Name), "Name"));
      var objectsToFilter = CreateFilterableObjects();
      var resList = objectsToFilter.FilterBy(simpleFilter, new ()
      {
        AdditionalProviders = AdditionalFilterOperationProviders.Empty()
      }).ToList();
      resList.Should().HaveCount(2);
      resList[0].Number.Should().Be(5);
      resList[1].Number.Should().Be(6);
    }

    /// <summary>
    /// Tests Simple filter case
    /// </summary>
    [Test]
    public void FilterMultiConditionSingleResult()
    {
      var conditions = new List<DefaultNcFilterCondition>()
      {
        NcFilterConditions.CreateEquals(nameof(ObjectToFilter.Name), "Name"),
        NcFilterConditions.CreateEquals(nameof(ObjectToFilter.Number), 6)
      };
      var simpleFilter = NcFilters.CreateAnd(conditions);
      var objectsToFilter = CreateFilterableObjects();
      var resList = objectsToFilter.FilterBy(simpleFilter, new ()
      {
        AdditionalProviders = AdditionalFilterOperationProviders.Empty()
      }).ToList();
      resList.Should().HaveCount(1);
      resList.FirstOrDefault().Description.Should().Be("Desc_6");
    }

    /// <summary>
    /// Tests Simple filter case
    /// </summary>
    [Test]
    public void FilterOrMultiConditionMultiResult()
    {
      var conditions = new List<DefaultNcFilterCondition>()
      {
        NcFilterConditions.CreateEquals(nameof(ObjectToFilter.Name), "Name_0"),
        NcFilterConditions.CreateEquals(nameof(ObjectToFilter.Number), 2)
      };
      var simpleFilter = NcFilters.CreateOr(conditions);
      var objectsToFilter = CreateFilterableObjects();
      var resList = objectsToFilter.FilterBy(simpleFilter, new ()
      {
        AdditionalProviders = AdditionalFilterOperationProviders.Empty()
      }).ToList();
      resList.Should().HaveCount(2);
    }

    /// <summary>
    /// Tests nested condition
    /// </summary>
    [Test]
    public void FilterNestedCondition()
    {
      var simpleFilter = NcFilters.CreateOr(NcFilterConditions.CreateGreater($"{nameof(ObjectToFilter.Name)}.{nameof(ObjectToFilter.Name.Length)}", 20));
      var objectsToFilter = CreateFilterableObjects();
      var resList = objectsToFilter.FilterBy(simpleFilter, new ()
      {
        AdditionalProviders = AdditionalFilterOperationProviders.Empty()
      }).ToList();
      resList.Should().HaveCount(1);
      resList.FirstOrDefault().Number.Should().Be(100);
    }

    /// <summary>
    /// tests the in condition
    /// </summary>
    [Test]
    public void FilterInCondition()
    {
      var nameColl = new List<string>()
      {
        "Name_0",
        "Name_1"
      };
      var simpleFilter = NcFilters.CreateOr(NcFilterConditions.CreateIn(nameof(ObjectToFilter.Name), nameColl));
      var objectsToFilter = CreateFilterableObjects();
      var resList = objectsToFilter.FilterBy(simpleFilter, new ()
      {
        AdditionalProviders = AdditionalFilterOperationProviders.Empty()
      }).ToList();
      resList.Should().HaveCount(2);
    }

    /// <summary>
    /// tests the overlaps condition
    /// </summary>
    [Test]
    public void FilterOverlaps()
    {
      var interval = new DefaultNcTimeInterval()
      {
        TimeFrom = DateTime.Now.AddDays(-1),
        TimeTo = DateTime.Now.AddDays(2).AddHours(2)
      };
      var simpleFilter = NcFilters.CreateAnd(NcFilterConditions.CreateOverlaps(nameof(ObjectToFilter.TimeInterval), interval));
      var objectsToFilter = CreateFilterableObjects();
      var resList = objectsToFilter.FilterBy(simpleFilter, new ()
      {
        AdditionalProviders = AdditionalFilterOperationProviders.Empty()
      }).ToList();
      resList.Should().HaveCount(2);
    }

    /// <summary>
    /// Tests Simple filter case
    /// </summary>
    [Test]
    public void FilterSubFiltersSingleResult()
    {
      var filterOne = NcFilters.CreateAnd(NcFilterConditions.CreateEquals(nameof(ObjectToFilter.Name), "Name"));
      var filterTwo = NcFilters.CreateAnd(NcFilterConditions.CreateEquals(nameof(ObjectToFilter.Number), 6));
      var simpleFilter = NcFilters.Create(new List<DefaultNcFilterCondition>(), new List<DefaultNcFilter>() { filterOne, filterTwo }, FilterLinks.And);
      var objectsToFilter = CreateFilterableObjects();
      var resList = objectsToFilter.FilterBy(simpleFilter, new ()
      {
        AdditionalProviders = AdditionalFilterOperationProviders.Empty()
      }).ToList();
      resList.Should().HaveCount(1);
      resList.FirstOrDefault().Description.Should().Be("Desc_6");
    }

    /// <summary>
    /// Tests Simple filter case
    /// </summary>
    [Test]
    public void FilterSubFiltersComplex()
    {
      var filterOneConditions = new List<DefaultNcFilterCondition>()
      {
        NcFilterConditions.CreateEquals(nameof(ObjectToFilter.Name), "Name"),
        NcFilterConditions.CreateEquals(nameof(ObjectToFilter.Description), "Desc")
      };
      var filterTwoConditions = new List<DefaultNcFilterCondition>()
      {
        NcFilterConditions.CreateEquals(nameof(ObjectToFilter.Number), 5),
        NcFilterConditions.CreateGreater(nameof(ObjectToFilter.Number), 1)
      };
      var filterOne = NcFilters.CreateAnd(filterOneConditions);
      var filterTwo = NcFilters.CreateOr(filterTwoConditions);
      var simpleFilter = NcFilters.Create(new List<DefaultNcFilterCondition>(), new List<DefaultNcFilter>() { filterOne, filterTwo }, FilterLinks.And);
      var objectsToFilter = CreateFilterableObjects();
      var resList = objectsToFilter.FilterBy(simpleFilter, new ()
      {
        AdditionalProviders = AdditionalFilterOperationProviders.Empty()
      }).ToList();
      resList.Should().HaveCount(1);
      resList.FirstOrDefault().Description.Should().Be("Desc");
    }
  }
}
