﻿using FluentAssertions;
using NiceCore.Extensions;
using System.Collections.Generic;
using NUnit.Framework;

namespace NiceCore.Tests
{
  /// <summary>
  /// tests for extension methods
  /// </summary>
  [TestFixture]
  public class ExtensionTests
  {
    /// <summary>
    /// Test for ForEachItem function
    /// </summary>
    [Test]
    public void ForEachItemCollection()
    {
      var expList = new List<string>()
      {
        "123",
        "124",
        "125",
      };
      var addList = new List<string>();
      expList.ForEachItem(r => addList.Add(r));
      addList.Count.Should().Be(expList.Count);
    }

    /// <summary>
    /// Test for cancel
    /// </summary>
    [Test]
    public void ForEachItemWithCancel()
    {
      var expList = new List<string>()
      {
        "123",
        "124",
        "125",
      };
      var addList = new List<string>();
      expList.ForEachItem(r =>
      {
        addList.Add(r);
        return r != "124";
      });
      addList.Count.Should().Be(expList.Count - 1);
    }
  }
}
