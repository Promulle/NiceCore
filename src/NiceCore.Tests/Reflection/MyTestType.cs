﻿namespace NiceCore.Tests.Reflection
{
  /// <summary>
  /// My Test Type
  /// </summary>
  public class MyTestType
  {
    /// <summary>
    /// Func
    /// </summary>
    /// <param name="i_Name"></param>
    public void MyFunc(string i_Name)
    {
    }

    /// <summary>
    /// MyFunc
    /// </summary>
    public void MyFunc()
    {
    }

    /// <summary>
    /// MyFunc
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Parameter"></param>
    /// <param name="i_Name"></param>
    public void MyFunc<T>(T i_Parameter, string i_Name)
    {
    }

    /// <summary>
    /// MyFunc
    /// </summary>
    /// <param name="i_Parameter"></param>
    /// <param name="i_Name"></param>
    public void MyFunc(int i_Parameter, string i_Name)
    {
    }

    /// <summary>
    /// MySecondFunc
    /// </summary>
    public void MySecondFunc()
    {

    }
  }
}
