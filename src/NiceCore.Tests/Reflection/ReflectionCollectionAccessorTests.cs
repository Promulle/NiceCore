using System.Collections.Generic;
using FluentAssertions;
using NiceCore.Reflection;
using NUnit.Framework;

namespace NiceCore.Tests.Reflection
{
  /// <summary>
  /// Reflection Collection Accessor tests
  /// </summary>
  [TestFixture]
  public class ReflectionCollectionAccessorTests
  {
    /// <summary>
    /// Test Count
    /// </summary>
    [Test]
    public void TestCount()
    {
      IList<string> list = new List<string>()
      {
        "1",
        "2",
        "3",
        "4"
      };
      var actualCount = list.Count;

      var count = ReflectionCollectionAccessor.GetCountFor(list);
      count.Should().Be(actualCount);
    }
  }
}