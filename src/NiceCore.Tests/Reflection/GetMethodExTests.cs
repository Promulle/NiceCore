﻿using FluentAssertions;
using NiceCore.Reflection;
using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace NiceCore.Tests.Reflection
{
  /// <summary>
  /// GetMethod Ex tests
  /// </summary>
  [TestFixture]
  public class GetMethodExTests
  {
    /// <summary>
    /// Tests getMethodEx for generic method
    /// </summary>
    [Test]
    public void GetGeneric()
    {
      var parameters = new List<MethodParameterType>()
      {
        MethodParameterType.GenericPlaceHolder(),
        MethodParameterType.Of(typeof(string))
      };
      var methInfo = ReflectionUtils.GetMethodEx(typeof(MyTestType), nameof(MyTestType.MyFunc), parameters);
      methInfo.Should().NotBeNull();
    }

    /// <summary>
    /// Tests getMethodEx for generic method
    /// </summary>
    [Test]
    public void GetGenericCached()
    {
      var firstStart = DateTime.Now;
      var parameters = new List<MethodParameterType>()
      {
        MethodParameterType.GenericPlaceHolder(),
        MethodParameterType.Of(typeof(string))
      };
      var methInfo = ReflectionUtils.GetMethodEx(typeof(MyTestType), nameof(MyTestType.MyFunc), parameters);
      var firstTime = (DateTime.Now - firstStart).Ticks;
      methInfo.Should().NotBeNull();
      var secondStart = DateTime.Now;
      var secondParameters = new List<MethodParameterType>()
      {
        MethodParameterType.GenericPlaceHolder(),
        MethodParameterType.Of(typeof(string))
      };
      var secondMethInfo = ReflectionUtils.GetMethodEx(typeof(MyTestType), nameof(MyTestType.MyFunc), parameters);
      var secondTime = (DateTime.Now - secondStart).Ticks;
      secondMethInfo.Should().NotBeNull();
      secondMethInfo.Should().BeSameAs(methInfo);
      secondTime.Should().BeLessThan(firstTime);
    }

    /// <summary>
    /// Tests getMethodEx for generic method
    /// </summary>
    [Test]
    public void GetNoParameters()
    {
      var methInfo = ReflectionUtils.GetMethodEx(typeof(MyTestType), nameof(MyTestType.MyFunc), null);
      methInfo.Should().NotBeNull();
    }

    /// <summary>
    /// Tests getMethodEx for generic method
    /// </summary>
    [Test]
    public void GetSingleParameter()
    {
      var parameters = new List<MethodParameterType>()
      {
        MethodParameterType.Of(typeof(string))
      };
      var methInfo = ReflectionUtils.GetMethodEx(typeof(MyTestType), nameof(MyTestType.MyFunc), parameters);
      methInfo.Should().NotBeNull();
    }
  }
}
