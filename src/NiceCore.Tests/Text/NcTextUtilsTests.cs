using FluentAssertions;
using NiceCore.Text;
using NUnit.Framework;

namespace NiceCore.Tests.Text
{
  /// <summary>
  /// NcTextUtilsTests
  /// </summary>
  [Parallelizable]
  [TestFixture]
  public class NcTextUtilsTests
  {
    private const string SUBSTRING_SOURCE =
      "This is a long text maybe with even more text the longer this needs to be.";
      
    /// <summary>
    /// TestSubstringWordAware
    /// </summary>
    [Test]
    public void TestSubstringWordAware()
    {
      var (sub, actualEnd) = NcTextUtils.SubstringWordAware(SUBSTRING_SOURCE, 0, 11);
      sub.Should().Be("This is a");
      actualEnd.Should().Be(9);
    }

    /// <summary>
    /// TestSubstringWordAwareGreaterLength
    /// </summary>
    [Test]
    public void TestSubstringWordAwareGreaterLengthStartZero()
    {
      var (sub, actualEnd) = NcTextUtils.SubstringWordAware(SUBSTRING_SOURCE, 0, SUBSTRING_SOURCE.Length);
      sub.Should().Be(SUBSTRING_SOURCE);
      actualEnd.Should().Be(SUBSTRING_SOURCE.Length - 1);
    }
    
    /// <summary>
    /// TestSubstringWordAwareGreaterLength
    /// </summary>
    [Test]
    public void TestSubstringWordAwareGreaterLengthStartNonZero()
    {
      var (sub, actualEnd) = NcTextUtils.SubstringWordAware(SUBSTRING_SOURCE, 62, 20);
      sub.Should().Be("needs to be.");
      actualEnd.Should().Be(SUBSTRING_SOURCE.Length - 1);
    }
  }
}