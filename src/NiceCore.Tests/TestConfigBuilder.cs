﻿using NiceCore.Services.Config.Impl.AssemblyBased;

namespace NiceCore.Tests
{
  /// <summary>
  /// Config builder for NiceCore.Tests
  /// </summary>
  public class TestConfigBuilder : BaseAssemblyXMLNcConfigurationSource
  {
    /// <summary>
    /// optional true
    /// </summary>
    public override bool IsOptional { get; } = true;

    /// <summary>
    /// reload
    /// </summary>
    public override bool ReloadOnChange { get; set; } = true;

    /// <summary>
    /// priority
    /// </summary>
    public override int Priority { get; } = 0;
  }
}
