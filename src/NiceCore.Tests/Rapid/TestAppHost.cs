﻿using NiceCore.Rapid.Application;

namespace NiceCore.Tests.Rapid
{
  /// <summary>
  /// testclass
  /// </summary>
  public class TestAppHost : ITestAppHost
  {
    /// <summary>
    /// App
    /// </summary>
    public INcApplication App { get; set; }
  }
}
