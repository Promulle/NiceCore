﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace NiceCore.Tests.Rapid.Logging
{
  /// <summary>
  /// Rapid Test In Memory Logger
  /// </summary>
  public class RapidTestInMemoryLogger : ILogger
  {
    /// <summary>
    /// Messages
    /// </summary>
    public List<string> Messages { get; } = new List<string>();
    
    /// <summary>
    /// log
    /// </summary>
    /// <param name="logLevel"></param>
    /// <param name="eventId"></param>
    /// <param name="state"></param>
    /// <param name="exception"></param>
    /// <param name="formatter"></param>
    /// <typeparam name="TState"></typeparam>
    public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
    {
      try
      {
        Messages.Add(formatter(state, exception));
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
      }  
    }

    /// <summary>
    /// Is Enabled
    /// </summary>
    /// <param name="logLevel"></param>
    /// <returns></returns>
    public bool IsEnabled(LogLevel logLevel)
    {
      return true;
    }

    /// <summary>
    /// Scope
    /// </summary>
    /// <param name="state"></param>
    /// <typeparam name="TState"></typeparam>
    /// <returns></returns>
    public IDisposable BeginScope<TState>(TState state)
      where TState : notnull
    {
      return null;
    }
  }
}