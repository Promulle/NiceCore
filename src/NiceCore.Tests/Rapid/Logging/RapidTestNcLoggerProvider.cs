﻿using Microsoft.Extensions.Logging;
using NiceCore.Logging;

namespace NiceCore.Tests.Rapid.Logging
{
  /// <summary>
  /// Test impl for logger
  /// </summary>
  public class RapidTestNcLoggerProvider : INcLoggingProvider
  {
    /// <summary>
    /// Provider
    /// </summary>
    public RapidTestLoggerProvider Provider { get; } = new RapidTestLoggerProvider();
    
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; } = "Test";
    
    /// <summary>
    /// Apply To Logging builder
    /// </summary>
    /// <param name="t_Builder"></param>
    public void ApplyToLoggingBuilder(ILoggingBuilder t_Builder)
    {
      t_Builder.AddProvider(Provider);
    }
  }
}
