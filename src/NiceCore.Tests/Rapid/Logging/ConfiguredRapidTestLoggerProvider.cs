﻿using Microsoft.Extensions.Logging;

namespace NiceCore.Tests.Rapid.Logging
{
  /// <summary>
  /// Rapid Test Logger Provider
  /// </summary>
  public class ConfiguredRapidTestLoggerProvider : ILoggerProvider
  {
    /// <summary>
    /// provider
    /// </summary>
    public static readonly ConfiguredRapidTestLoggerProvider Provider = new();
    
    /// <summary>
    /// Instance
    /// </summary>
    public RapidTestInMemoryLogger Instance { get; } = new();
    
    /// <summary>
    /// Dispose
    /// </summary>
    public void Dispose()
    {
    }

    /// <summary>
    /// Create Logger
    /// </summary>
    /// <param name="categoryName"></param>
    /// <returns></returns>
    public ILogger CreateLogger(string categoryName)
    {
      return Instance;
    }
  }
}