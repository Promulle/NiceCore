﻿using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using NiceCore.Services.Config.Impl;

namespace NiceCore.Tests.Rapid.Logging
{
  /// <summary>
  /// logging Configuration Source
  /// </summary>
  public class LoggingConfigurationSource : BaseInMemoryNcConfigurationSource
  {
    /// <summary>
    /// Priority
    /// </summary>
    public override int Priority { get; } = 0;

    /// <summary>
    /// Values
    /// </summary>
    public override IDictionary<string, string> Values { get; init; } = new Dictionary<string, string>()
    {
      {"NiceCore:Logging:Providers:One", "Configured-Test"},
      {"NiceCore:Logging:LogLevel:Default", LogLevel.Debug.ToString()}
    };
  }
}