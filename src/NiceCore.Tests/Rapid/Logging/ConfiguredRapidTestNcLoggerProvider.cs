﻿using Microsoft.Extensions.Logging;
using NiceCore.Logging;
using NiceCore.ServiceLocation;

namespace NiceCore.Tests.Rapid.Logging
{
  /// <summary>
  /// Configured Rapid Test Nc Logger Provider
  /// </summary>
  [Register(InterfaceType = typeof(INcLoggingProvider))]
  public class ConfiguredRapidTestNcLoggerProvider : INcLoggingProvider
  {
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; } = "Configured-Test";

    /// <summary>
    /// 
    /// </summary>
    /// <param name="t_Builder"></param>
    public void ApplyToLoggingBuilder(ILoggingBuilder t_Builder)
    {
      t_Builder.AddProvider(ConfiguredRapidTestLoggerProvider.Provider);
    }
  }
}