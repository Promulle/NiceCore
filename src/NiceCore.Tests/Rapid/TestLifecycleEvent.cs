﻿using NiceCore.Rapid.Application;
using NiceCore.ServiceLocation;
using System.Threading.Tasks;

namespace NiceCore.Tests.Rapid
{
  /// <summary>
  /// Test event
  /// </summary>
  [Register(InterfaceType = typeof(IApplicationLifecycleEvent))]
  public class TestLifecycleEvent : IApplicationLifecycleEvent
  {
    /// <summary>
    /// Stage
    /// </summary>
    public ApplicationLifecycleStages Stage { get; } = ApplicationLifecycleStages.Starting;

    /// <summary>
    /// Block
    /// </summary>
    public bool BlocksCurrentStage { get; } = false;

    /// <summary>
    /// Priority
    /// </summary>
    public int Priority { get; } = 0;

    /// <summary>
    /// execute
    /// </summary>
    /// <returns></returns>
    public Task Execute()
    {
      LifecycleTestHelper.Flag = true;
      return Task.CompletedTask;
    }
  }
}
