﻿using NiceCore.Rapid.Application;

namespace NiceCore.Tests.Rapid
{
  /// <summary>
  /// Testinterface
  /// </summary>
  public interface ITestAppHost
  {
    /// <summary>
    /// App
    /// </summary>
    INcApplication App { get; set; }
  }
}