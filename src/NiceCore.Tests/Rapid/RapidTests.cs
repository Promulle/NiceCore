﻿using FluentAssertions;
using NiceCore.Rapid;
using NiceCore.Rapid.Application;
using NiceCore.Rapid.ApplicationBuilding;
using NiceCore.Rapid.Default;
using NiceCore.Rapid.Hosting;
using NiceCore.Rapid.Storage;
using NiceCore.ServiceLocation;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.ServiceLocation.CastleWindsor;
using NiceCore.ServiceLocation.Discovery.DefaultDirectoryProviders;
using NiceCore.Tests.Rapid.Logging;
using NUnit.Framework;

namespace NiceCore.Tests.Rapid
{
  /// <summary>
  /// TestFixture for rapid tests
  /// </summary>
  [TestFixture]
  public class RapidTests
  {
    private const string TEST_ERROR_APP_NULL = "App is null.";
    private const string TEST_ERROR_APP_CONTAINER_NULL = "Container of app is null.";
    private const string TEST_ERROR_APP_VERSION_NULL = "Version of app is null.";
    private INcApplication m_App;

    /// <summary>
    /// Setup run once
    /// </summary>
    [OneTimeSetUp]
    public async Task SingleSetup()
    {
      m_App = await NcApplicationBuilder
        .Create()
        .UseDefaults()
        .Build().ConfigureAwait(false);
    }

    /// <summary>
    /// Setup
    /// </summary>
    [SetUp]
    public void MultiSetup()
    {
      LifecycleTestHelper.Flag = false;
    }

    /// <summary>
    /// Tests lifecycle events
    /// </summary>
    [Test]
    public async Task LifecycleEvents()
    {
      var app = await NcApplicationBuilder
        .Create()
        .UseDefaults()
        .Build().ConfigureAwait(false);
      await app.Run().ConfigureAwait(false);
      LifecycleTestHelper.Flag.Should().BeTrue();
    }

    /// <summary>
    /// Tests default build
    /// </summary>
    [Test]
    public void TestDefaultBuild()
    {
      TestApp(m_App);
    }

    /// <summary>
    /// Tests build with custom set instances
    /// </summary>
    [Test]
    public async Task TestBuildWithInstances()
    {
      var custApp = await NcApplicationBuilder
        .Create()
        .ServiceLocation.UseContainerType<CastleWindsorServiceContainer>()
        .ApplicationVersion.UseEntryAssemblyVersion()
        .Build().ConfigureAwait(false);
      TestApp(custApp);
    }
    
    /// <summary>
    /// Tests build with custom set instances
    /// </summary>
    [Test]
    public async Task TestBuildWithDirectLogging()
    {
      var extStore = new DefaultAppBuilderExtensionStore();
      var provider = new RapidTestNcLoggerProvider();
      var custApp = await NcApplicationBuilder
        .Create<NcHostingApplication>()
        .UseDefaults()
        .ApplicationVersion.UseEntryAssemblyVersion()
        .Logging.Direct(new []{ provider }, LogLevel.Debug)
        .HostService(extStore).Default()
        .Build().ConfigureAwait(false);
      var factory = custApp.ServiceContainer.Resolve<ILoggerFactory>();
      factory.Should().NotBeNull();
      var logger = factory.CreateLogger(string.Empty);
      logger.LogInformation("Some line");
      provider.Provider.Instance.Messages.Should().NotBeEmpty();
    }
    
    /// <summary>
    /// Tests build with custom set instances
    /// </summary>
    [Test]
    public async Task TestBuildWithConfiguredLogging()
    {
      var custApp = await NcApplicationBuilder
        .Create()
        .Configuration.Default()
        .Configuration.ConfigurationSources.Add<LoggingConfigurationSource>()
        .ServiceLocation.UseContainerType<CastleWindsorServiceContainer>()
        .ServiceLocation.DefaultPlugins()
        .ApplicationVersion.UseEntryAssemblyVersion()
        .Logging.Configured()
        .Build().ConfigureAwait(false);
      var factory = custApp.ServiceContainer.Resolve<ILoggerFactory>();
      factory.Should().NotBeNull();
      var logger = factory.CreateLogger(string.Empty);
      logger.LogDebug("Some line");
      ConfiguredRapidTestLoggerProvider.Provider.Instance.Messages.Should().NotBeEmpty();
    }

    /// <summary>
    /// Test registration during usedefaults
    /// </summary>
    [Test]
    public void TestAppRegistration()
    {
      var container = m_App.ServiceContainer;
      var regApp = container.Resolve<INcApplication>();
      regApp.Should().NotBeNull(TEST_ERROR_APP_NULL);
    }

    /// <summary>
    /// Tests injection ability of property builders
    /// </summary>
    [Test]
    public async Task TestBuildWithInjection()
    {
      var custApp = await NcApplicationBuilder.Create()
        .UseDefaults()
        .ServiceLocation.UseDiscoveryRunConfiguration(new()
        {
          TargetMode = DiscoveryModes.Assemblies,
          AssemblyTargetMode = AssemblyProvidingModes.All,
          DiscoveryDefaultDirectoryProvider = new ExecutingAssemblyDefaultDirectoryProvider(),
          AllowSearchOfLoadedAssemblies = false,
          DiscoveredNamespaceStorage = new PublicTypeDiscoveredNamespaceStorage()
        })
        .Build().ConfigureAwait(false);
      TestApp(custApp);
    }

    /// <summary>
    /// Test for construction of storage app
    /// </summary>
    [Test]
    public async Task StorageAppBuilder()
    {
      var extStore = new DefaultAppBuilderExtensionStore();
      var custApp = await NcApplicationBuilder.Create<NcStorageApplication>()
        .UseDefaults()
        .StorageService(extStore).Default()
        .Build().ConfigureAwait(false);
      custApp.Should().NotBeNull();
      custApp.StorageService.Should().NotBeNull();
      custApp.StorageService.IsInitialized.Should().BeTrue();
    }

    /// <summary>
    /// Tests variable builder
    /// </summary>
    [Test]
    public async Task VariableBuilder()
    {
      var extStore = new DefaultAppBuilderExtensionStore();
      var hostApp = await NcApplicationBuilder.Create<NcHostingApplication>()
        .UseDefaults()
        .HostService(extStore).Default()
        .Build().ConfigureAwait(false);
      hostApp.Should().NotBeNull();
      hostApp.HostService.Should().NotBeNull();
    }

    private void TestApp(INcApplication i_App)
    {
      i_App.Should().NotBeNull(TEST_ERROR_APP_VERSION_NULL);
      i_App.Should().NotBeNull(TEST_ERROR_APP_NULL);
      i_App.ServiceContainer.Should().NotBeNull(TEST_ERROR_APP_CONTAINER_NULL);
    }
  }
}
