﻿namespace NiceCore.Tests.Rapid
{
  /// <summary>
  /// Test helper class
  /// </summary>
  public static class LifecycleTestHelper
  {
    /// <summary>
    /// Flag
    /// </summary>
    public static bool Flag { get; set; }
  }
}
