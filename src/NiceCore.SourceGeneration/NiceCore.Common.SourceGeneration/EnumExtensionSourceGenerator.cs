﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Text;
using System.Linq;
using System.Text;

namespace NiceCore.Common.SourceGeneration
{
  [Generator]
  public class EnumExtensionSourceGenerator : ISourceGenerator
  {
    public void Execute(GeneratorExecutionContext context)
    {
      foreach (var tree in context.Compilation.SyntaxTrees)
      {
        foreach (var namesSpaceNode in tree.GetRoot().DescendantNodes().OfType<NamespaceDeclarationSyntax>())
        {
          foreach (var enumNode in namesSpaceNode.DescendantNodes().OfType<EnumDeclarationSyntax>())
          {
            var toStringCases = GetToStringCases(enumNode);
            AddEnumExtensions(enumNode, toStringCases, namesSpaceNode, context);
            var fromStringCases = GetFromStringCases(enumNode);
            AddConversionHelper(enumNode, fromStringCases, namesSpaceNode, context);
          }
        }
      }
    }

    private static string GetFromStringCases(EnumDeclarationSyntax i_Syntax)
    {
      var builder = new StringBuilder();
      var enumName = i_Syntax.Identifier;
      foreach (var member in i_Syntax.Members)
      {
        builder.Append("nameof(")
          .Append(enumName)
          .Append(".")
          .Append(member.Identifier)
          .Append(") => ")
          .Append(enumName)
          .Append(".")
          .Append(member.Identifier)
          .AppendLine(",");
      }
      return builder.ToString();
    }

    private static string GetToStringCases(EnumDeclarationSyntax i_Syntax)
    {
      var builder = new StringBuilder();
      var enumName = i_Syntax.Identifier;
      foreach (var member in i_Syntax.Members)
      {
        builder.Append(enumName)
          .Append(".")
          .Append(member.Identifier)
          .Append(" => nameof(")
          .Append(enumName)
          .Append(".")
          .Append(member.Identifier)
          .AppendLine("),");
      }
      return builder.ToString();
    }

    private static void AddEnumExtensions(EnumDeclarationSyntax i_DeclarationSyntax, string i_Cases, NamespaceDeclarationSyntax i_NameSpace, GeneratorExecutionContext i_Context)
    {
      var newSource = $@"
using System;
namespace {i_NameSpace.Name}
{{
  /// <summary>
  /// Extension Methods For {i_DeclarationSyntax.Identifier}
  /// </summary>
  public static class Generated{i_DeclarationSyntax.Identifier}EnumExtensions
  {{
    /// <summary>
    /// ToStringFast method for {i_DeclarationSyntax.Identifier}
    /// </summary>
    public static string ToStringFast(this {i_DeclarationSyntax.Identifier} i_Enum)
    {{
      return i_Enum switch
      {{
        {i_Cases}
        _ => throw new ArgumentException()
      }};
    }}
  }}
}}";

      i_Context.AddSource($"Generated{i_DeclarationSyntax.Identifier}EnumExtensions", SourceText.From(newSource, Encoding.UTF8));
    }

    private static void AddConversionHelper(EnumDeclarationSyntax i_DeclarationSyntax, string i_FromStringCases, NamespaceDeclarationSyntax i_Namespace, GeneratorExecutionContext i_Context)
    {
      var newSource = $@"
namespace {i_Namespace.Name}
{{
  /// <summary>
  /// ConversionHelper for {i_DeclarationSyntax.Identifier}
  /// </summary>
  public static class {i_DeclarationSyntax.Identifier}ConversionHelper
  {{
    /// <summary>
    /// FromStringFast method for {i_DeclarationSyntax.Identifier}
    /// </summary>
    public static {i_DeclarationSyntax.Identifier} FromStringFast(string i_Value, {i_DeclarationSyntax.Identifier} i_DefaultValue)
    {{
      return i_Value switch
      {{
        {i_FromStringCases}
        _ => i_DefaultValue
      }};
    }}

    /// <summary>
    /// FromStringFast method for {i_DeclarationSyntax.Identifier}
    /// </summary>
    public static {i_DeclarationSyntax.Identifier}? FromStringFast(string i_Value)
    {{
      return i_Value switch
      {{
        {i_FromStringCases}
        _ => null
      }};
    }}
  }}
}}";
      i_Context.AddSource($"{i_DeclarationSyntax.Identifier}ConversionHelper", SourceText.From(newSource, Encoding.UTF8));
    }

    public void Initialize(GeneratorInitializationContext context)
    {
    }
  }
}
