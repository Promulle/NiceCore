﻿namespace NiceCore.Services.Validation
{
  /// <summary>
  /// Severities for validation
  /// </summary>
  public enum ValidationSeverities
  {
    /// <summary>
    /// ERROR
    /// </summary>
    Error = 0,

    /// <summary>
    /// WARNING
    /// </summary>
    Warning = 1,

    /// <summary>
    /// INFO
    /// </summary>
    Info = 2,
  }
}
