﻿using System.Collections.Generic;
using System.Linq;

namespace NiceCore.Services.Validation.Message
{
  /// <summary>
  /// Error Message with variables
  /// </summary>
  public class ValidationFailureMessage
  {
    /// <summary>
    /// Message that maybe contains placeholders for values filled by Values Collection
    /// </summary>
    public string PlaceholderMessage { get; init; }

    /// <summary>
    /// Store for variables in placeholder message
    /// </summary>
    public List<ValidationFailureMessageVariable> Values { get; init; }

    /// <summary>
    /// Factory method for Validation FailureMessage
    /// </summary>
    /// <param name="i_PlaceholderMessage"></param>
    /// <param name="i_Variables"></param>
    /// <returns></returns>
    public static ValidationFailureMessage Of(string i_PlaceholderMessage, params ValidationFailureMessageVariable[] i_Variables)
    {
      return Of(i_PlaceholderMessage, i_Variables.ToList());
    }
    
    /// <summary>
    /// Factory method for Validation FailureMessage
    /// </summary>
    /// <param name="i_PlaceholderMessage"></param>
    /// <param name="i_Variables"></param>
    /// <returns></returns>
    public static ValidationFailureMessage Of(string i_PlaceholderMessage, List<ValidationFailureMessageVariable> i_Variables)
    {
      return new()
      {
        PlaceholderMessage = i_PlaceholderMessage,
        Values = i_Variables
      };
    }
  }
}
