﻿using System;

namespace NiceCore.Services.Validation.Message
{
  /// <summary>
  /// Variable for a single value in a placeholder message
  /// </summary>
  public class ValidationFailureMessageVariable
  {
    /// <summary>
    /// Values as string
    /// </summary>
    public string StringValue { get; init; }

    /// <summary>
    /// Value as int
    /// </summary>
    public int? IntValue { get; init; }

    /// <summary>
    /// Value as float
    /// </summary>
    public float? FloatValue { get; init; }

    /// <summary>
    /// Value as long
    /// </summary>
    public long? LongValue { get; init; }

    /// <summary>
    /// Value as double
    /// </summary>
    public double? DoubleValue { get; init; }

    /// <summary>
    /// Value as guid
    /// </summary>
    public Guid? GuidValue { get; init; }

    /// <summary>
    /// As Int
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public static ValidationFailureMessageVariable AsInt(int i_Value)
    {
      return new ValidationFailureMessageVariable()
      {
        IntValue = i_Value,
      };
    }

    /// <summary>
    /// As Long
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public static ValidationFailureMessageVariable AsLong(long i_Value)
    {
      return new ValidationFailureMessageVariable()
      {
        LongValue = i_Value
      };
    }

    /// <summary>
    /// As Float
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public static ValidationFailureMessageVariable AsFloat(float i_Value)
    {
      return new ValidationFailureMessageVariable()
      {
        FloatValue = i_Value
      };
    }

    /// <summary>
    /// As String
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public static ValidationFailureMessageVariable AsString(string i_Value)
    {
      return new ValidationFailureMessageVariable()
      {
        StringValue = i_Value
      };
    }

    /// <summary>
    /// As Double
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public static ValidationFailureMessageVariable AsDouble(double i_Value)
    {
      return new ValidationFailureMessageVariable()
      {
        DoubleValue = i_Value
      };
    }

    /// <summary>
    /// As Guid
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public static ValidationFailureMessageVariable AsGuid(Guid i_Value)
    {
      return new ValidationFailureMessageVariable()
      {
        GuidValue = i_Value
      };
    }
  }
}
