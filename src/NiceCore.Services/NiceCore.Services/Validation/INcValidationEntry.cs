﻿using NiceCore.Services.Validation.Message;

namespace NiceCore.Services.Validation
{
  /// <summary>
  /// Validation Entry for a single validation
  /// </summary>
  public interface INcValidationEntry
  {
    /// <summary>
    /// PropertyName
    /// </summary>
    string PropertyName { get; set; }

    /// <summary>
    /// Validation ID of the instance that was validated, might be null
    /// </summary>
    string ValidationID { get; set; }

    /// <summary>
    /// Message describing the validation problem for this entry
    /// </summary>
    ValidationFailureMessage ProblemMessage { get; }

    /// <summary>
    /// Value of property that is not valid
    /// </summary>
    object Value { get; }

    /// <summary>
    /// Severity of this entry
    /// </summary>
    ValidationSeverities Severity { get; }
  }
}
