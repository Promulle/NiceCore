namespace NiceCore.Services.Validation
{
  /// <summary>
  /// Interface for Transformers that transform values that have been validated before they are put into validation results
  /// </summary>
  public interface INcValidationResultValueTransformer
  {
    /// <summary>
    /// Transform Validation Result Value
    /// </summary>
    /// <param name="i_OriginalValue"></param>
    /// <returns></returns>
    object TransformValidationResultValue(object i_OriginalValue);
  }
}
