﻿using System.Collections.Generic;
using NiceCore.Services.Validation.Results;

namespace NiceCore.Services.Validation
{
  /// <summary>
  /// Validation result for object validations
  /// </summary>
  public interface INcValidationResult
  {
    /// <summary>
    /// Instance that has been validated
    /// </summary>
    object Instance { get; }

    /// <summary>
    /// Whether the validation ran into any errors
    /// </summary>
    bool IsValid { get; }

    /// <summary>
    /// Operation that triggered the validation
    /// </summary>
    public string Operation { get; }

    /// <summary>
    /// Validation results that ended in Warning
    /// </summary>
    public List<DefaultNcValidationEntry> Warnings { get; }

    /// <summary>
    /// Validation results that ended in Error
    /// </summary>
    public List<DefaultNcValidationEntry> Errors { get; }

    /// <summary>
    /// Validation results that ended in Info
    /// </summary>
    public List<DefaultNcValidationEntry> Info { get; }
  }
}
