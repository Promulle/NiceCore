﻿using NiceCore.Base;
using NiceCore.Services.Validation.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NiceCore.Services.Validation.Contexts;

namespace NiceCore.Services.Validation
{
  /// <summary>
  /// Validator for untyped values
  /// </summary>
  public interface INcObjectValidator : IInitializable
  {
    /// <summary>
    /// Type of the instance that should be validated
    /// </summary>
    Type InstanceType { get; }

    /// <summary>
    /// the operations this validator should be used for. Should not be null and not contain null values or be empty
    /// </summary>
    IEnumerable<string> Operations { get; }

    /// <summary>
    /// Validates i_Instance for the operation specified in "Operation"
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_Operation"></param>
    /// <param name="i_Node"></param>
    /// <returns></returns>
    public Task<INcValidationResult> Validate(object i_Instance, string i_Operation, SubValidationNode i_Node);

    /// <summary>
    /// Validate
    /// </summary>
    /// <param name="i_ValidationRequest">Request with info about requested validation</param>
    /// <returns></returns>
    public Task<INcValidationResult> ValidateWithContext(NcValidationRequest i_ValidationRequest);
  }
}
