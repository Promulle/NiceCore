using System;
using System.Collections.Generic;

namespace NiceCore.Services.Validation.Storage
{
  /// <summary>
  /// Interface for objects holding the validators to be used by validators/services to validate
  /// </summary>
  public interface IValidatorStorage
  {
    /// <summary>
    /// Clears the storage and then fills it with validators passed with i_Validators
    /// </summary>
    /// <param name="i_Validators"></param>
    void FillWith(IEnumerable<INcObjectValidator> i_Validators);

    /// <summary>
    /// Gets all matching validators
    /// </summary>
    /// <param name="i_Type"></param>
    /// <param name="i_Operation"></param>
    /// <returns></returns>
    IEnumerable<INcObjectValidator> GetMatchingValidators(Type i_Type, string i_Operation);
  }
}