using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Logging;
using NiceCore.Diagnostics;
using NiceCore.Extensions;
using NiceCore.Logging.Extensions;

namespace NiceCore.Services.Validation.Storage
{
  /// <summary>
  /// Default impl for IValidatorStorage
  /// </summary>
  public class DefaultValidatorStorage : IValidatorStorage
  {
    private readonly object m_Lock = new ();
    private readonly ILogger m_Log = DiagnosticsCollector.Instance.GetLogger<DefaultValidatorStorage>();
    private readonly IDictionary<string, IDictionary<Type, IEnumerable<INcObjectValidator>>> m_ValidatorMap = new Dictionary<string, IDictionary<Type, IEnumerable<INcObjectValidator>>>();

    /// <summary>
    /// Default ctor
    /// </summary>
    public DefaultValidatorStorage()
    {
    }
    
    /// <summary>
    /// Default Validator Storage
    /// </summary>
    /// <param name="i_Logger"></param>
    public DefaultValidatorStorage(ILogger i_Logger)
    {
      m_Log = i_Logger;
    }
    
    /// <summary>
    /// Fill the validator storage
    /// </summary>
    /// <param name="i_Validators"></param>
    public void FillWith(IEnumerable<INcObjectValidator> i_Validators)
    {
      lock (m_Lock)
      {
        m_ValidatorMap.Clear();
        var allValidators = i_Validators.ToList();
        LogForNullOperations(allValidators);
        var validatorByOperation = new Dictionary<string, List<INcObjectValidator>>();
        foreach (var validator in allValidators)
        {
          validator.Operations.ForEachItem(op =>
          {
            if (!validatorByOperation.TryGetValue(op, out var validators))
            {
              validators = new List<INcObjectValidator>();
              validatorByOperation[op] = validators;
            }

            validators.Add(validator);
          });
        }

        validatorByOperation
          .ForEachItem(byOperationGroup => m_ValidatorMap[byOperationGroup.Key] = CreateDictionaryByTpes(byOperationGroup));
      }
    }
    
    /// <summary>
    /// Gets all matching validators
    /// </summary>
    /// <param name="i_Type"></param>
    /// <param name="i_Operation"></param>
    /// <returns></returns>
    public IEnumerable<INcObjectValidator> GetMatchingValidators(Type i_Type, string i_Operation)
    {
      if (m_ValidatorMap.TryGetValue(i_Operation, out var typeDictionary) && typeDictionary.TryGetValue(i_Type, out var validators))
      {
        return validators;
      }
      m_Log.Warning($"Validation of an instance of type {i_Type.FullName} for operation {i_Operation} was attempted, but there are no validators registered for the operation or the type.");
      return new List<INcObjectValidator>();
    }
    
    private IDictionary<Type, IEnumerable<INcObjectValidator>> CreateDictionaryByTpes(KeyValuePair<string, List<INcObjectValidator>> i_ByOperationGroup)
    {
      m_Log.Information($"Adding {i_ByOperationGroup.Value.Count} validator(s) for operation {i_ByOperationGroup.Key}.");
      var dict = new Dictionary<Type, IEnumerable<INcObjectValidator>>();
      i_ByOperationGroup.Value.GroupBy(r => r.InstanceType)
        .ForEachItem(byTypeGroup =>
        {
          m_Log.Information($"Adding {byTypeGroup.Count()} validator(s) for type {byTypeGroup.Key.FullName}: [{BuildArrayMessageForValidators(byTypeGroup)}]");
          dict[byTypeGroup.Key] = byTypeGroup;
        });
      return dict;
    }
    
    private void LogForNullOperations(IEnumerable<INcObjectValidator> i_Validators)
    {
      var nullOperationValidators = i_Validators.Where(r => r.Operations?.Any() != true).ToList();
      if (nullOperationValidators.Count > 0)
      {
        m_Log.Warning($"Some validators have not defined their operation. Following validators will be ignored because of this: [{BuildArrayMessageForValidators(nullOperationValidators)}]");
      }
    }

    private static string BuildArrayMessageForValidators(IEnumerable<INcObjectValidator> i_Validators)
    {
      var builder = new StringBuilder();
      var validatorList = i_Validators.ToList();
      for (var i = 0; i < validatorList.Count; i++)
      {
        var validator = validatorList[i];
        builder.Append(validator.GetType().FullName);
        if (i < validatorList.Count - 1)
        {
          builder.Append(", ");
        }
      }
      return builder.ToString();
    }
  }
}