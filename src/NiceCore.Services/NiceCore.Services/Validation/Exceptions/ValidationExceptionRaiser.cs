﻿using System.Collections.Generic;

namespace NiceCore.Services.Validation.Exceptions
{
  /// <summary>
  /// Helper class that throws sepcific exceptions for validation results
  /// </summary>
  public static class ValidationExceptionRaiser
  {
    /// <summary>
    /// Throws a new validation exception if i_Result is not valid
    /// </summary>
    /// <param name="i_Result">Result that is checked</param>
    /// <param name="i_TreatWarningsAsErrors">Whether any warning also invalidates the result. If true, adds warning entries to exception</param>
    public static void ThrowIfNotValid(INcValidationResult i_Result, bool i_TreatWarningsAsErrors)
    {
      if (!i_Result.IsValid || (i_TreatWarningsAsErrors && i_Result.Warnings != null && i_Result.Warnings.Count > 0))
      {
        var entries = i_Result.Errors != null ? new List<INcValidationEntry>(i_Result.Errors) : new List<INcValidationEntry>();
        if (i_TreatWarningsAsErrors && i_Result.Warnings != null)
        {
          entries.AddRange(i_Result.Warnings);
        }
        throw new NcValidationException(entries, i_Result.Instance);
      }
    }
  }
}
