﻿using NiceCore.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NiceCore.Services.Validation.Exceptions
{
  /// <summary>
  /// Exception thrown if an object is not valid. Contains entries that are wrong
  /// </summary>
#pragma warning disable RCS1194 // Implement exception constructors.
  public class NcValidationException : Exception
#pragma warning restore RCS1194 // Implement exception constructors.
  {
    /// <summary>
    /// Entries
    /// </summary>
    public List<INcValidationEntry> Entries { get; }

    /// <summary>
    /// Instance whose validation failed
    /// </summary>
    public object Instance { get; }

    /// <summary>
    /// Constructor filling validation exception with entries
    /// </summary>
    /// <param name="i_Entries"></param>
    /// <param name="i_Instance">Instance that was validated</param>
    public NcValidationException(IEnumerable<INcValidationEntry> i_Entries, object i_Instance) : base(BuildMessage(i_Entries, i_Instance))
    {
      Entries = i_Entries.ToList();
      Instance = i_Instance;
    }

    private static string BuildMessage(IEnumerable<INcValidationEntry> i_Entries, object i_Instance)
    {
      var builder = new StringBuilder("Validation for instance of type ")
        .Append(i_Instance.GetType().Name)
        .AppendLine(" failed because of following problems:");
      i_Entries.ForEachItem(entry =>
      {
        builder.Append("Property: ")
          .Append(entry.PropertyName)
          .Append(" with value ")
          .Append(entry.Value)
          .Append(" is not valid, reason: ")
          .Append(entry.ProblemMessage.PlaceholderMessage);
        if (entry.ProblemMessage.Values != null && entry.ProblemMessage.Values.Count > 0)
        {
          builder.AppendLine("(Values: ");
          for (var i = 0; i < entry.ProblemMessage.Values.Count; i++)
          {
            var value = entry.ProblemMessage.Values[i];
            builder.Append("[int: ")
              .Append(value.IntValue)
              .Append("], [long: ")
              .Append(value.LongValue)
              .Append("], [double: ")
              .Append(value.DoubleValue)
              .Append("], [float: ")
              .Append(value.FloatValue)
              .Append("], [string: ")
              .Append(value.StringValue)
              .Append("], [guid: ")
              .Append(value.GuidValue)
              .Append(']');
            if (i < entry.ProblemMessage.Values.Count - 1)
            {
              builder.AppendLine(",");
            }
          }
          builder.Append(')');
        }
        builder.Append(" Severity: ")
          .Append(entry.Severity);
      });
      return builder.ToString();
    }
  }
}
