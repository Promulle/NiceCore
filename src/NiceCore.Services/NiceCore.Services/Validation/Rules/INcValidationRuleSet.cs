﻿using System;
using System.Collections.Generic;
using NiceCore.Services.Validation.Contexts;

namespace NiceCore.Services.Validation.Rules
{
  /// <summary>
  /// Rule set combining rules under a condition
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public interface INcValidationRuleSet<T>
  {
    /// <summary>
    /// Optional identifier for this rule set
    /// </summary>
    string Identifier { get; }

    /// <summary>
    /// Condition used to determine if this set shoud be used
    /// </summary>
    Func<NcValidationRequest, bool> SetCondition { get; }

    /// <summary>
    /// Rules of this ruleset
    /// </summary>
    List<INcValidationRule<T>> Rules { get; }
  }
}
