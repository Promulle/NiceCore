﻿namespace NiceCore.Services.Validation.Rules
{
  /// <summary>
  /// parameters for a single Execution of a rule
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <typeparam name="TValue"></typeparam>
  public class ValidationRuleExecutionParameters<T, TValue>
  {
    /// <summary>
    /// Property Value
    /// </summary>
    public TValue PropertyValue { get; init; }

    /// <summary>
    /// Instance the propert in question is being validated for
    /// </summary>
    public T Instance { get; init; }

    /// <summary>
    /// Property name
    /// </summary>
    public string PropertyName { get; init; }
  }
}
