﻿using NiceCore.Services.Validation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NiceCore.Services.Validation.Contexts;
using NiceCore.Services.Validation.Results;

namespace NiceCore.Services.Validation.Rules
{
  /// <summary>
  /// Validation Rule used for sub validation
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <typeparam name="TSub"></typeparam>
  /// <typeparam name="TProperty"></typeparam>
  public class SubNcValidationRule<T, TSub, TProperty> : BaseNcValidationRule<T>
  {
    /// <summary>
    /// Sub Validator, this overwrites the validator that would have been searched by type and operation.
    /// </summary>
    public INcValidator<TSub> SubValidator { get; set; }

    /// <summary>
    /// Property Accessor func
    /// </summary>
    public Func<T, TProperty> PropertyAccessorFunc { get; set; }

    /// <summary>
    /// Validate By Rule
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_Request"></param>
    /// <returns></returns>
    public override async Task<IEnumerable<DefaultNcValidationEntry>> ValidateByRule(T i_Instance, NcValidationRequest i_Request)
    {
      if (i_Request.Call.SubValidationNode != null)
      {
        var subValidatorsEnumerable = SubValidator != null ? new [] { SubValidator } : i_Request.Validation.Validators.GetMatchingValidators(typeof(TSub), i_Request.Process.Operation);
        var subValidators = subValidatorsEnumerable.ToList();
        var propVal = PropertyAccessorFunc.Invoke(i_Instance);
        var subResults = new List<INcValidationResult>();
        if (propVal is TSub casted)
        {
          subResults.AddRange(await RunSubValidators(casted, subValidators, i_Request).ConfigureAwait(false));
        }
        else if (propVal is IEnumerable<TSub> castedList)
        {
          foreach (var value in castedList)
          {
            subResults.AddRange(await RunSubValidators(value, subValidators, i_Request).ConfigureAwait(false));
          }
        }
        var allEntries = subResults.SelectMany(res => res.Errors.Concat(res.Info).Concat(res.Warnings)).ToList();
        allEntries.ForEach(entry => entry.PropertyName = $"{PropertyName}.{entry.PropertyName}");
        return allEntries;
      }
      return new List<DefaultNcValidationEntry>();
    }

    private static async Task<List<INcValidationResult>> RunSubValidators(TSub i_Instance, IEnumerable<INcObjectValidator> i_SubValidators, NcValidationRequest i_Request)
    {
      var validationResults = new List<INcValidationResult>();
      foreach (var subValidator in i_SubValidators)
      {
        var request = i_Request.ForCall(new DefaultNcValidatorCallContext()
        {
          Instance = i_Instance,
          SubValidationNode = i_Request.Call.SubValidationNode
        });
        validationResults.Add(await subValidator.ValidateWithContext(request).ConfigureAwait(false));
      }
      return validationResults;
    }
  }
}
