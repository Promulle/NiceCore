﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NiceCore.Services.Validation.Contexts;
using NiceCore.Services.Validation.Results;

namespace NiceCore.Services.Validation.Rules
{
  /// <summary>
  /// Base NC Validation Rule
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public abstract class BaseNcValidationRule<T> : INcValidationRule<T>
  {
    /// <summary>
    /// Property name
    /// </summary>
    public string PropertyName { get; set; }

    /// <summary>
    /// Validate by rule
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_Request"></param>
    /// <returns></returns>
    public abstract Task<IEnumerable<DefaultNcValidationEntry>> ValidateByRule(T i_Instance, NcValidationRequest i_Request);

    /// <summary>
    /// Returns the validation id of i_Instance, if i_Instance is a IHasValidationID
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <returns></returns>
    protected string GetValidationID(object i_Instance)
    {
      if (i_Instance is IHasValidationID casted)
      {
        return casted.ValidationID;
      }
      return null;
    }
  }
}
