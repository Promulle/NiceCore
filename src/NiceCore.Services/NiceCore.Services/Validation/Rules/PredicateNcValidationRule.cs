﻿using NiceCore.Services.Validation.Message;
using System;
using System.Threading.Tasks;
using NiceCore.Services.Validation.Contexts;
using NiceCore.Services.Validation.Results;

namespace NiceCore.Services.Validation.Rules
{
  /// <summary>
  /// Validation rule based on predicates
  /// </summary>
  public class PredicateNcValidationRule<T, TValue> : BaseSingleResultValidationRule<T>
  {
    /// <summary>
    /// Func used to check T
    /// </summary>
    public Func<ValidationRuleExecutionParameters<T, TValue>, NcValidationRequest, Task<bool>> CheckFunc { get; init; }

    /// <summary>
    /// Func for accessing the value
    /// </summary>
    public Func<T, TValue> AccessorFunc { get; init; }

    /// <summary>
    /// Func that returns a message on validation failure
    /// </summary>
    public Func<T, TValue, ValidationFailureMessage> FailureMessageFunc { get; init; }

    /// <summary>
    /// Severity of this rule
    /// </summary>
    public ValidationSeverities Severity { get; init; }

    /// <summary>
    /// Validates i_Instance given the predefined Predicate
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_Context"></param>
    /// <returns></returns>
    protected override async Task<DefaultNcValidationEntry> ValidateBySingleResultRule(T i_Instance, NcValidationRequest i_Context)
    {
      var valueToValidate = AccessorFunc.Invoke(i_Instance);
      var parameters = new ValidationRuleExecutionParameters<T, TValue>()
      {
        Instance = i_Instance,
        PropertyName = PropertyName,
        PropertyValue = valueToValidate
      };
      var validationSuccess = await CheckFunc.Invoke(parameters, i_Context).ConfigureAwait(false);
      if (!validationSuccess)
      {
        return new ()
        {
          ProblemMessage = FailureMessageFunc.Invoke(i_Instance, valueToValidate),
          ValidationID = GetValidationID(i_Instance),
          PropertyName = PropertyName,
          Severity = Severity,
          Value = i_Context.Process.ResultValueTransformer?.TransformValidationResultValue(valueToValidate) ?? valueToValidate
        };
      }
      return null;
    }
  }
}
