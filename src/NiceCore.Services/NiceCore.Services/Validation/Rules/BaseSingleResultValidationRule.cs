﻿using NiceCore.Services.Validation.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using NiceCore.Services.Validation.Contexts;
using NiceCore.Services.Validation.Results;

namespace NiceCore.Services.Validation.Rules
{
  /// <summary>
  /// Base for rules returning a single result
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public abstract class BaseSingleResultValidationRule<T> : BaseNcValidationRule<T>
  {
    /// <summary>
    /// Validate by rule
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_Request"></param>
    /// <returns></returns>
    public override async Task<IEnumerable<DefaultNcValidationEntry>> ValidateByRule(T i_Instance, NcValidationRequest i_Request)
    {
      return new[] { await ValidateBySingleResultRule(i_Instance, i_Request).ConfigureAwait(false) };
    }

    /// <summary>
    /// Actual validation
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_Request"></param>
    /// <returns></returns>
    protected abstract Task<DefaultNcValidationEntry> ValidateBySingleResultRule(T i_Instance, NcValidationRequest i_Request);
  }
}
