using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NiceCore.Services.Validation.Contexts;
using NiceCore.Services.Validation.Message;
using NiceCore.Services.Validation.Results;

namespace NiceCore.Services.Validation.Rules.Standalone
{
  /// <summary>
  /// Base Class for a simpler validation rule 
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public abstract class NcStandaloneValidationRule<T> : BaseSingleResultValidationRule<T>, INcStandaloneValidationRule
  {
    /// <summary>
    /// Type to validate
    /// </summary>
    public Type TypeToValidate { get; } = typeof(T);
    
    /// <summary>
    /// Severity
    /// </summary>
    protected abstract ValidationSeverities GetSeverity();

    /// <summary>
    /// Returns the operations to use this rule for
    /// </summary>
    /// <returns></returns>
    public abstract IEnumerable<string> GetOperations();

    /// <summary>
    /// Validate the instance for this rule
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_Request"></param>
    /// <returns></returns>
    protected abstract Task<NcStandaloneValidationResult> CheckInstance(T i_Instance, NcValidationRequest i_Request);
    
    /// <summary>
    /// Validate by single result rule
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_Request"></param>
    /// <returns></returns>
    protected override async Task<DefaultNcValidationEntry> ValidateBySingleResultRule(T i_Instance, NcValidationRequest i_Request)
    {
      var res = await CheckInstance(i_Instance, i_Request).ConfigureAwait(false);
      if (!res.IsValid)
      {
        return new()
        {
          Severity = GetSeverity(),
          Value = i_Request.Process.ResultValueTransformer?.TransformValidationResultValue(i_Instance) ?? i_Instance,
          PropertyName = null,
          ProblemMessage = ValidationFailureMessage.Of(res.Message, res.MessageVariables.ToList()),
          ValidationID = GetValidationID(i_Instance)
        };
      }

      return null;
    }
  }
}