using System;
using System.Collections.Generic;
using NiceCore.Services.Validation.Message;

namespace NiceCore.Services.Validation.Rules.Standalone
{
  /// <summary>
  /// Result of the validation by a standalone rule
  /// </summary>
  public class NcStandaloneValidationResult
  {
    /// <summary>
    /// Is the validation result valid
    /// </summary>
    public bool IsValid { get; private init; }
    
    /// <summary>
    /// Message to be returned
    /// </summary>
    public string Message { get; private init; }

    /// <summary>
    /// Message Variables
    /// </summary>
    public IEnumerable<ValidationFailureMessageVariable> MessageVariables { get; private init; }
    
    /// <summary>
    /// Protected ctor to force used of static methods
    /// </summary>
    protected NcStandaloneValidationResult()
    {
      
    }

    /// <summary>
    /// Returns a valid Result
    /// </summary>
    /// <returns></returns>
    public static NcStandaloneValidationResult Valid()
    {
      return new()
      {
        IsValid = true,
        MessageVariables = Array.Empty<ValidationFailureMessageVariable>(),
        Message = null
      };
    }

    /// <summary>
    /// Not Valid
    /// </summary>
    /// <param name="i_Message"></param>
    /// <param name="i_Variables"></param>
    /// <returns></returns>
    public static NcStandaloneValidationResult NotValid(string i_Message, params ValidationFailureMessageVariable[] i_Variables)
    {
      return new()
      {
        IsValid = false,
        MessageVariables = i_Variables,
        Message = i_Message
      };
    }
  }
}