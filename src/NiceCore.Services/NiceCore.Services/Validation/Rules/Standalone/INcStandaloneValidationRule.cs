using System;
using System.Collections.Generic;

namespace NiceCore.Services.Validation.Rules.Standalone
{
  /// <summary>
  /// Interface for standalone validation rules
  /// </summary>
  public interface INcStandaloneValidationRule
  {
    /// <summary>
    /// The type of the validator
    /// </summary>
    Type TypeToValidate { get; }

    /// <summary>
    /// Returns the operations to be used
    /// </summary>
    /// <returns></returns>
    IEnumerable<string> GetOperations();
  }
}