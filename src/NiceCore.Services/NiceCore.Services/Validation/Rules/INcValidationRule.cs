﻿using NiceCore.Services.Validation.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using NiceCore.Services.Validation.Contexts;
using NiceCore.Services.Validation.Results;

namespace NiceCore.Services.Validation.Rules
{
  /// <summary>
  /// Interface for a single rule used for validation
  /// </summary>
  public interface INcValidationRule<T>
  {
    /// <summary>
    /// Property name
    /// </summary>
    string PropertyName { get; set; }

    /// <summary>
    /// Validates i_Instance according to what this rule has defined
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_Request"></param>
    /// <returns></returns>
    Task<IEnumerable<DefaultNcValidationEntry>> ValidateByRule(T i_Instance, NcValidationRequest i_Request);
  }
}
