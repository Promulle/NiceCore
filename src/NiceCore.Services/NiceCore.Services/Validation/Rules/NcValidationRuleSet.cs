﻿using System;
using System.Collections.Generic;
using NiceCore.Services.Validation.Contexts;

namespace NiceCore.Services.Validation.Rules
{
  /// <summary>
  /// Default rule set
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public class NcValidationRuleSet<T> : INcValidationRuleSet<T>
  {
    /// <summary>
    /// Condition to use
    /// </summary>
    public Func<NcValidationRequest, bool> SetCondition { get; init; }

    /// <summary>
    /// All Rules
    /// </summary>
    public List<INcValidationRule<T>> Rules { get; init; } = new();

    /// <summary>
    /// Identifier
    /// </summary>
    public string Identifier { get; init; }

    /// <summary>
    /// Creates a new rule set for i_Condition
    /// </summary>
    /// <param name="i_ConditionFunc"></param>
    /// <returns></returns>
    public static NcValidationRuleSet<T> For(Func<NcValidationRequest, bool> i_ConditionFunc)
    {
      return new ()
      {
        SetCondition = i_ConditionFunc
      };
    }
  }
}
