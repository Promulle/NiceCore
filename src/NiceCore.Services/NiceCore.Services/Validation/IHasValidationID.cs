﻿namespace NiceCore.Services.Validation
{
  /// <summary>
  /// Interface marking objects with validation ids
  /// </summary>
  public interface IHasValidationID
  {
    /// <summary>
    /// Validation ID
    /// </summary>
    string ValidationID { get; set; }
  }
}
