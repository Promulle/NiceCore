﻿using NiceCore.Base;
using NiceCore.Services.Validation.Model;
using System.Threading.Tasks;
using NiceCore.Services.Validation.Contexts;

namespace NiceCore.Services.Validation
{
  /// <summary>
  /// Service to for validation of data
  /// </summary>
  public interface IValidationService : IInitializable
  {
    /// <summary>
    /// Validates i_Instance for the given operation
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_Operation">Operation to validate for. If none is specified validation will be skipped. </param>
    /// <param name="i_Node"></param>
    /// <param name="i_Context">Calling code provided context that is available inside validators</param>
    /// <returns></returns>
    Task<INcValidationResult> Validate(object i_Instance, string i_Operation, SubValidationNode i_Node, INcCustomValidationProcessContext i_Context);
  }
}
