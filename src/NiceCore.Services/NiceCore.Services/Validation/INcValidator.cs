﻿using NiceCore.Services.Validation.Model;
using System.Threading.Tasks;
using NiceCore.Services.Validation.Contexts;

namespace NiceCore.Services.Validation
{
  /// <summary>
  /// Validator for instances of type T
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public interface INcValidator<T> : INcObjectValidator
  {
    /// <summary>
    /// Validates i_Instance for the operation specified in "Operation"
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_Operation"></param>
    /// <param name="i_Node"></param>
    /// <returns></returns>
    Task<INcValidationResult> Validate(T i_Instance, string i_Operation, SubValidationNode i_Node);

    /// <summary>
    /// Validate
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_Operation"></param>
    /// <param name="i_Node"></param>
    /// <param name="i_Context"></param>
    /// <returns></returns>
    Task<INcValidationResult> Validate(T i_Instance, string i_Operation, SubValidationNode i_Node, INcCustomValidationProcessContext i_Context);
  }
}
