using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging.Abstractions;
using NiceCore.Services.Validation.Rules.Standalone;
using NiceCore.Services.Validation.Validators;

namespace NiceCore.Services.Validation.Standalone
{
  /// <summary>
  /// Factory that is used to create validators from standalone rules
  /// </summary>
  public class StandaloneValidatorFactory
  {
    /// <summary>
    /// Creates the validators for i_Rules
    /// </summary>
    /// <param name="i_Rules"></param>
    /// <returns></returns>
    public IEnumerable<INcObjectValidator> CreateValidators(IEnumerable<INcStandaloneValidationRule> i_Rules)
    {
      var list = new List<INcObjectValidator>();
      foreach (var ruleGrouping in i_Rules.GroupBy(r => r.TypeToValidate))
      {
        var type = typeof(NcStandaloneRuleValidator<>).MakeGenericType(ruleGrouping.Key);
        var byOperations = GroupRulesByOperations(ruleGrouping);
        foreach (var (operation, rules) in byOperations)
        {
          var args = new object[]
          {
            rules,
            new[] {operation},
            NullLogger.Instance
          };
          var validator = Activator.CreateInstance(type, args);
          if (validator is INcObjectValidator castedValidator)
          {
            list.Add(castedValidator);  
          }
          else
          {
            throw new InvalidOperationException($"Could not create validator for type {type}: {validator == null} (true == not created, false == created) or validator does not inherit from {typeof(INcObjectValidator).FullName}");
          }
        }
      }
      return list;
    }

    private static Dictionary<string, List<INcStandaloneValidationRule>> GroupRulesByOperations(IEnumerable<INcStandaloneValidationRule> i_Rules)
    {
      var dict = new Dictionary<string, List<INcStandaloneValidationRule>>();
      foreach (var rule in i_Rules)
      {
        foreach (var operation in rule.GetOperations())
        {
          if (!dict.TryGetValue(operation, out var list))
          {
            list = new();
            dict[operation] = list;
          }
          list.Add(rule);
        }
      }
      return dict;
    }
  }
}