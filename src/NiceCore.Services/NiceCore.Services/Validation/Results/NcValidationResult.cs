﻿using System.Collections.Generic;

namespace NiceCore.Services.Validation.Results
{
  /// <summary>
  /// Validation Result for a certain object
  /// </summary>
  public class NcValidationResult : INcValidationResult
  {
    /// <summary>
    /// Instance that has been validated
    /// </summary>
    public object Instance { get; init; }

    /// <summary>
    /// Whether the validation ran into any errors
    /// </summary>
    public bool IsValid
    {
      get { return Errors == null || Errors.Count == 0; }
    }

    /// <summary>
    /// Operation that triggered the validation
    /// </summary>
    public string Operation { get; init; }

    /// <summary>
    /// Validation results that ended in Warning
    /// </summary>
    public List<DefaultNcValidationEntry> Warnings { get; init; }

    /// <summary>
    /// Validation results that ended in Error
    /// </summary>
    public List<DefaultNcValidationEntry> Errors { get; init; }

    /// <summary>
    /// Validation results that ended in Info
    /// </summary>
    public List<DefaultNcValidationEntry> Info { get; init; }
  }
}
