﻿using NiceCore.Services.Validation.Message;

namespace NiceCore.Services.Validation.Results
{
  /// <summary>
  /// Default impl for IValidationEntry
  /// </summary>
  public class DefaultNcValidationEntry : INcValidationEntry
  {
    /// <summary>
    /// PropertyName
    /// </summary>
    public string PropertyName { get; set; }

    /// <summary>
    /// Problem Message
    /// </summary>
    public ValidationFailureMessage ProblemMessage { get; init; }

    /// <summary>
    /// Value
    /// </summary>
    public object Value { get; init; }

    /// <summary>
    /// Severity
    /// </summary>
    public ValidationSeverities Severity { get; init; }

    /// <summary>
    /// Validation ID
    /// </summary>
    public string ValidationID { get; set; }
  }
}
