﻿using System.Collections.Generic;

namespace NiceCore.Services.Validation.Model
{
  /// <summary>
  /// Node in the SubValidation Model
  /// </summary>
  public class SubValidationNode
  {
    /// <summary>
    /// Child Nodes
    /// </summary>
    public Dictionary<string, SubValidationNode> ChildNodes { get; init; }

    /// <summary>
    /// PropertyName
    /// </summary>
    public string PropertyName { get; init; }
  }
}
