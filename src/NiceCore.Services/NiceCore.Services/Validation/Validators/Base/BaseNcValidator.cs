﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Base.Services;
using NiceCore.Services.Validation.Contexts;
using NiceCore.Services.Validation.Model;
using NiceCore.Services.Validation.Results;
using NiceCore.Services.Validation.Storage;

namespace NiceCore.Services.Validation.Validators.Base
{
  /// <summary>
  /// Base impl for all nc validator
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public abstract class BaseNcValidator<T> : BaseService, INcValidator<T>
  {
    /// <summary>
    /// Type of the instance for this validator
    /// </summary>
    public Type InstanceType { get; } = typeof(T);

    /// <summary>
    /// Operation of the validator
    /// </summary>
    public abstract IEnumerable<string> Operations { get; }
    
    /// <summary>
    /// Logger
    /// </summary>
    public abstract ILogger Logger { get; }

    /// <summary>
    /// Validate
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_Operation"></param>
    /// <param name="i_Node"></param>
    /// <returns></returns>
    public abstract Task<INcValidationResult> Validate(T i_Instance, string i_Operation, SubValidationNode i_Node);

    /// <summary>
    /// Validate
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_Operation"></param>
    /// <param name="i_Node"></param>
    /// <param name="i_Context"></param>
    /// <returns></returns>
    public abstract Task<INcValidationResult> Validate(T i_Instance, string i_Operation, SubValidationNode i_Node, INcCustomValidationProcessContext i_Context);

    /// <summary>
    /// Validate
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_Operation"></param>
    /// <param name="i_Node"></param>
    /// <returns></returns>
    public abstract Task<INcValidationResult> Validate(object i_Instance, string i_Operation, SubValidationNode i_Node);

    /// <summary>
    /// Validate with context
    /// </summary>
    /// <param name="i_Request"></param>
    /// <returns></returns>
    public abstract Task<INcValidationResult> ValidateWithContext(NcValidationRequest i_Request);

    /// <summary>
    /// Create Request for
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_Operation"></param>
    /// <param name="i_Node"></param>
    /// <param name="i_CustomContext"></param>
    /// <param name="i_AdditionalValidators"></param>
    /// <returns></returns>
    protected NcValidationRequest CreateRequestFor(object i_Instance, string i_Operation, SubValidationNode i_Node, INcCustomValidationProcessContext i_CustomContext, IEnumerable<INcObjectValidator> i_AdditionalValidators)
    {
      var validatorStorage = new DefaultValidatorStorage(Logger);
      if (i_AdditionalValidators != null)
      {
        validatorStorage.FillWith(i_AdditionalValidators);
      }
      return new()
      {
        Call = new DefaultNcValidatorCallContext()
        {
          Instance = i_Instance,
          SubValidationNode = i_Node
        },
        Process = new DefaultNcValidationProcessContext()
        {
          Operation = i_Operation,
          CustomContext = i_CustomContext
        },
        Validation = new DefaultNcValidationContext()
        {
          Validators = validatorStorage
        }
      };
    }

    /// <summary>
    /// Ensures that the type of the instance is correct
    /// </summary>
    /// <param name="i_Instance"></param>
    protected T EnsureCorrectType(object i_Instance)
    {
      if (i_Instance is not T instance)
      {
        throw new InvalidOperationException($"Objects that should be validated by a validator of type {GetType().FullName} should inherit/implement atleast {typeof(T).FullName}. Type of object which was tried to be validated {i_Instance.GetType().FullName}");
      }
      return instance;
    }

    /// <summary>
    /// Ensures that we have a context that can be used for validation
    /// </summary>
    /// <param name="i_Context"></param>
    /// <returns></returns>
    protected static INcValidationContext EnsureWorkingContext(INcValidationContext i_Context)
    {
      return i_Context ?? new DefaultNcValidationContext();
    }

    /// <summary>
    /// Filters a list of nc entries for a certain severity
    /// </summary>
    /// <param name="i_Results"></param>
    /// <param name="i_Severity"></param>
    /// <returns></returns>
    protected static List<DefaultNcValidationEntry> FilterForSeverity(IEnumerable<DefaultNcValidationEntry> i_Results, ValidationSeverities i_Severity)
    {
      return i_Results
        .Where(r => r.Severity.Equals(i_Severity))
        .ToList();
    }
  }
}
