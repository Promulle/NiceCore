﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NiceCore.ServiceLocation;
using NiceCore.Services.Validation.Contexts;
using NiceCore.Services.Validation.Model;
using NiceCore.Services.Validation.Results;
using NiceCore.Services.Validation.Rules;

namespace NiceCore.Services.Validation.Validators.Base
{
  /// <summary>
  /// Validator that validates by defined rules, which might change.
  /// More dynamic than the fluent validation impl for nicecore
  /// </summary>
  public abstract class BaseNcRuleValidator<T> : BaseNcValidator<T>
  {
    private readonly SemaphoreSlim m_LockObject = new(1, 1);
    private bool m_RulesWereDefined = false;
    private readonly List<INcValidationRuleSet<T>> m_RuleSets = new();

    /// <summary>
    /// Container
    /// </summary>
    public IServiceContainer Container { get; set; }

    /// <summary>
    /// Validate
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_Operation"></param>
    /// <param name="i_Node"></param>
    /// <returns></returns>
    public override Task<INcValidationResult> Validate(T i_Instance, string i_Operation, SubValidationNode i_Node)
    {
      var request = CreateRequestFor(i_Instance, i_Operation, i_Node, null, null);
      return ValidateWithContext(request);
    }

    /// <summary>
    /// Validate
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_Operation"></param>
    /// <param name="i_Node"></param>
    /// <param name="i_Context"></param>
    /// <returns></returns>
    public override Task<INcValidationResult> Validate(T i_Instance, string i_Operation, SubValidationNode i_Node, INcCustomValidationProcessContext i_Context)
    {
      var request = CreateRequestFor(i_Instance, i_Operation, i_Node, i_Context, null);
      return ValidateWithContext(request);
    }

    /// <summary>
    /// Validate
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_Operation"></param>
    /// <param name="i_Node"></param>
    /// <returns></returns>
    public override Task<INcValidationResult> Validate(object i_Instance, string i_Operation, SubValidationNode i_Node)
    {
      var request = CreateRequestFor(i_Instance, i_Operation, i_Node, null, null);
      return ValidateWithContext(request);
    }

    /// <summary>
    /// Validate
    /// </summary>
    /// <param name="i_Request"></param>
    /// <returns></returns>
    public override async Task<INcValidationResult> ValidateWithContext(NcValidationRequest i_Request)
    {
      var inst = EnsureCorrectType(i_Request.Call.Instance);
      await DefineRulesIfNeeded().ConfigureAwait(false);
      var rulesToUse = FilterRulesForContext(i_Request);

      var allEntries = new List<DefaultNcValidationEntry>();
      foreach (var rule in rulesToUse)
      {
        var subNode = GetSubNode(rule.PropertyName, i_Request.Call.SubValidationNode);
        //create request fitting for the rule call -> correct subNode and instance are set, rest stays the same as entered
        var request = i_Request.ForCall(new DefaultNcValidatorCallContext()
        {
          Instance = inst,
          SubValidationNode = subNode
        });
        //execute rule with new context
        allEntries.AddRange(await rule.ValidateByRule(inst, request).ConfigureAwait(false));
      }
      var entries = allEntries.Where(r => r != null).ToList();
      return new NcValidationResult()
      {
        Instance = i_Request.Call.Instance,
        Operation = i_Request.Process.Operation,
        Info = FilterForSeverity(entries, ValidationSeverities.Info),
        Warnings = FilterForSeverity(entries, ValidationSeverities.Warning),
        Errors = FilterForSeverity(entries, ValidationSeverities.Error)
      };
    }

    private static SubValidationNode GetSubNode(string i_PropertyName, SubValidationNode i_Node)
    {
      if (i_Node != null && i_PropertyName != null && i_Node.ChildNodes.TryGetValue(i_PropertyName, out var child))
      {
        return child;
      }
      return null;
    }

    /// <summary>
    /// Define the rule sets if none are defined
    /// </summary>
    /// <returns></returns>
    private async Task DefineRulesIfNeeded()
    {
      await m_LockObject.WaitAsync().ConfigureAwait(false);
      try
      {
        if (!m_RulesWereDefined)
        {
          m_RuleSets.AddRange(await DefineRuleSets().ConfigureAwait(false));
          m_RulesWereDefined = true;
        }
      }
      finally
      {
        m_LockObject.Release();
      }
    }

    /// <summary>
    /// Define the resultsets used by the validation
    /// </summary>
    /// <returns></returns>
    protected abstract Task<IEnumerable<INcValidationRuleSet<T>>> DefineRuleSets();

    /// <summary>
    /// Filters all rulesets to be used for i_Context
    /// </summary>
    /// <param name="i_Context"></param>
    /// <returns></returns>
    protected List<INcValidationRuleSet<T>> FilterRuleSetsForContext(NcValidationRequest i_Context)
    {
      return m_RuleSets.Where(r => r.SetCondition(i_Context)).ToList();
    }

    /// <summary>
    /// Searches all rules to be used for a validation of an instance of t under context of i_Context
    /// </summary>
    /// <param name="i_Context"></param>
    /// <returns></returns>
    protected List<INcValidationRule<T>> FilterRulesForContext(NcValidationRequest i_Context)
    {
      return FilterRuleSetsForContext(i_Context).SelectMany(r => r.Rules).ToList();
    }

    /// <summary>
    /// Adds a new rule set
    /// </summary>
    /// <param name="i_RuleSet"></param>
    protected async void AddRuleSet(INcValidationRuleSet<T> i_RuleSet)
    {
      await m_LockObject.WaitAsync().ConfigureAwait(false);
      try
      {
        m_RuleSets.Add(i_RuleSet);
      }
      finally
      {
        m_LockObject.Release();
      }
    }

    /// <summary>
    /// removes the rule set
    /// </summary>
    /// <param name="i_RuleSet"></param>
    protected async Task RemoveRuleSet(INcValidationRuleSet<T> i_RuleSet)
    {
      await m_LockObject.WaitAsync().ConfigureAwait(false);
      try
      {
        m_RuleSets.Remove(i_RuleSet);
      }
      finally
      {
        m_LockObject.Release();
      }
    }

    /// <summary>
    /// Searches all available rulesets for the ruleset defined by i_Identifier
    /// </summary>
    /// <param name="i_Identifier"></param>
    /// <returns></returns>
    protected Task<INcValidationRuleSet<T>> FindFirsRuleSetByIdentifier(string i_Identifier)
    {
      return Task.FromResult(m_RuleSets.Find(r => r.Identifier == i_Identifier));
    }
  }
}
