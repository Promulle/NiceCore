﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NiceCore.Services.Validation.Contexts;
using NiceCore.Services.Validation.Message;
using NiceCore.Services.Validation.Rules;

namespace NiceCore.Services.Validation.Validators.Base
{
  /// <summary>
  /// Base Rule Validator, that returns a single ruleSet with a defined set of rules
  /// </summary>
  /// <typeparam name="T">Type to be validated</typeparam>
  public abstract class BaseSingleRuleSetValidator<T> : BaseNcRuleValidator<T>
  {
    /// <summary>
    /// Returns identifier for the rule set
    /// </summary>
    /// <returns></returns>
    protected virtual string GetIdentifier()
    {
      return null;
    }
    
    /// <summary>
    /// Condition this set is used under. for example: only a specific validation context will use this
    /// </summary>
    /// <returns></returns>
    protected virtual Func<NcValidationRequest, bool> GetSetCondition()
    {
      return _ => true;
    }
    
    /// <summary>
    /// Define the ruleset
    /// </summary>
    /// <returns></returns>
    protected override Task<IEnumerable<INcValidationRuleSet<T>>> DefineRuleSets()
    {
      IEnumerable<INcValidationRuleSet<T>> ruleSets = new[]
      {
        new NcValidationRuleSet<T>()
        {
          Identifier = GetIdentifier(),
          SetCondition = GetSetCondition(),
          Rules = CreateRules()
        }
      };
      return Task.FromResult(ruleSets);
    }

    /// <summary>
    /// method that has to be overriden for creation of rules
    /// </summary>
    /// <returns></returns>
    protected abstract List<INcValidationRule<T>> CreateRules();

    /// <summary>
    /// Function for simplyfying rule creation
    /// </summary>
    /// <param name="i_Severity"></param>
    /// <param name="i_CheckFunc"></param>
    /// <param name="i_PropertyName"></param>
    /// <param name="i_FailureMessageFunc">This may return a string that is used as parameter for ValidationFailureMessage.Of</param>
    /// <typeparam name="TProperty">Type of property</typeparam>
    /// <returns></returns>
    protected INcValidationRule<T> CreatePredicateRule<TProperty>(ValidationSeverities i_Severity,
      Func<ValidationRuleExecutionParameters<T, TProperty>, NcValidationRequest, Task<bool>> i_CheckFunc,
      string i_PropertyName,
      Func<T, string> i_FailureMessageFunc)
    {
      return CreatePredicateRule(i_Severity, 
        _ => default, 
        i_PropertyName, 
        (inst, _) => ValidationFailureMessage.Of(i_FailureMessageFunc.Invoke(inst)),
        i_CheckFunc);
    }

    /// <summary>
    /// Function for simplyfying rule creation
    /// </summary>
    /// <param name="i_Severity"></param>
    /// <param name="i_AccessorFunc"></param>
    /// <param name="i_PropertyName"></param>
    /// <param name="i_FailureMessageFunc"></param>
    /// <param name="i_CheckFunc"></param>
    /// <typeparam name="TProperty"></typeparam>
    /// <returns></returns>
    protected INcValidationRule<T> CreatePredicateRule<TProperty>(ValidationSeverities i_Severity,
      Func<T, TProperty> i_AccessorFunc,
      string i_PropertyName,
      Func<T, TProperty, ValidationFailureMessage> i_FailureMessageFunc,
      Func<ValidationRuleExecutionParameters<T, TProperty>, NcValidationRequest, Task<bool>> i_CheckFunc)
    {
      return new PredicateNcValidationRule<T, TProperty>()
      {
        Severity = i_Severity,
        AccessorFunc = i_AccessorFunc,
        PropertyName = i_PropertyName,
        FailureMessageFunc =  i_FailureMessageFunc,
        CheckFunc = i_CheckFunc
      };
    }
  }
}