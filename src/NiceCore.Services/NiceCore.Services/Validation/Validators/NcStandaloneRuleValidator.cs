using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Services.Validation.Rules;
using NiceCore.Services.Validation.Rules.Standalone;
using NiceCore.Services.Validation.Validators.Base;

namespace NiceCore.Services.Validation.Validators
{
  /// <summary>
  /// Validator used to execute standalone validation rules
  /// </summary>
  public class NcStandaloneRuleValidator<T> : BaseNcRuleValidator<T>
  {
    private readonly List<INcValidationRule<T>> m_Rules;
    private readonly ILogger m_Logger;
    
    /// <summary>
    /// Operations 
    /// </summary>
    public override IEnumerable<string> Operations { get; }

    /// <summary>
    /// logger
    /// </summary>
    public override ILogger Logger
    {
      get
      {
        return m_Logger;
      }
    }
    
    /// <summary>
    /// Ctor filling rules and operations
    /// </summary>
    /// <param name="i_ValidationRules"></param>
    /// <param name="i_Operations"></param>
    /// <param name="i_Logger"></param>
    public NcStandaloneRuleValidator(IEnumerable<INcStandaloneValidationRule> i_ValidationRules, IEnumerable<string> i_Operations, ILogger i_Logger)
    {
      m_Logger = i_Logger;
      m_Rules = i_ValidationRules.OfType<INcValidationRule<T>>().ToList();
      Operations = i_Operations;
    }
    
    /// <summary>
    /// Define Rule Sets
    /// </summary>
    /// <returns></returns>
    protected override Task<IEnumerable<INcValidationRuleSet<T>>> DefineRuleSets()
    {
      var ruleSets = new List<INcValidationRuleSet<T>>()
      {
        new NcValidationRuleSet<T>()
        {
          Identifier = null,
          Rules = m_Rules.ToList(),
          SetCondition = r => true
        }
      };
      return Task.FromResult<IEnumerable<INcValidationRuleSet<T>>>(ruleSets);
    }
    
    
  }
}