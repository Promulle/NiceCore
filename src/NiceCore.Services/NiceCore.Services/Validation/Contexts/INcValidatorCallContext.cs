using FluentValidation;
using NiceCore.Services.Validation.Model;

namespace NiceCore.Services.Validation.Contexts
{
  /// <summary>
  /// Interface for objects that represent a context for a single call on a validator
  /// </summary>
  public interface INcValidatorCallContext
  {
    /// <summary>
    /// The instance to be validated
    /// </summary>
    object Instance { get; }
    
    /// <summary>
    /// The current sub validation noce
    /// </summary>
    SubValidationNode SubValidationNode { get; }

    /// <summary>
    /// To Context
    /// </summary>
    /// <returns></returns>
    ValidationContext<T> ToContext<T>();
  }
}