﻿using FluentValidation;
using NiceCore.Services.Validation.Storage;

namespace NiceCore.Services.Validation.Contexts
{
  /// <summary>
  /// Default context, just wrapping the entity
  /// </summary>
  public class DefaultNcValidationContext : INcValidationContext
  {
    /// <summary>
    /// Validators
    /// </summary>
    public IValidatorStorage Validators { get; set; }
  }
}
