namespace NiceCore.Services.Validation.Contexts
{
  /// <summary>
  /// Class summarizing contexts
  /// </summary>
  public class NcValidationRequest
  {
    /// <summary>
    /// Call Context
    /// </summary>
    public INcValidatorCallContext Call { get; init; }
    
    /// <summary>
    /// Process
    /// </summary>
    public INcValidationProcessContext Process { get; init; }
    
    /// <summary>
    /// Overarching Context
    /// </summary>
    public INcValidationContext Validation { get; init; }

    /// <summary>
    /// Creates a new, call specific, context from this context
    /// </summary>
    /// <param name="i_CallContext"></param>
    /// <returns></returns>
    public NcValidationRequest ForCall(INcValidatorCallContext i_CallContext)
    {
      return new ()
      {
        Call = i_CallContext,
        Process = Process,
        Validation = Validation
      };
    }
  }
}