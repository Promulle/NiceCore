using FluentValidation;

namespace NiceCore.Services.Validation.Contexts
{
  /// <summary>
  /// interface for the context of a single validation process 
  /// </summary>
  public interface INcValidationProcessContext
  {
    /// <summary>
    /// Operation that is validated for by this validation process
    /// </summary>
    string Operation { get;  }
    
    /// <summary>
    /// Custom Context 
    /// </summary>
    INcCustomValidationProcessContext CustomContext { get; }
    
    /// <summary>
    /// Result value transformer to be used during this validation process
    /// </summary>
    INcValidationResultValueTransformer ResultValueTransformer { get; }
  }
}