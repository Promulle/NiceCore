namespace NiceCore.Services.Validation.Contexts
{
  /// <summary>
  /// Additional context info for the current validation process, provided by the calling code 
  /// </summary>
  public interface INcCustomValidationProcessContext
  {
  }
}