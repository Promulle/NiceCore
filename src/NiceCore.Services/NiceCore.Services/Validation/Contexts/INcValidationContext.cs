﻿using FluentValidation;
using NiceCore.Services.Validation.Storage;

namespace NiceCore.Services.Validation.Contexts
{
  /// <summary>
  /// Context for a single validation to be run under
  /// </summary>
  public interface INcValidationContext
  {
    /// <summary>
    /// Validators
    /// </summary>
    IValidatorStorage Validators { get; }
  }
}
