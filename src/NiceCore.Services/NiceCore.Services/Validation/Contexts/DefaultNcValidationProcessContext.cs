namespace NiceCore.Services.Validation.Contexts
{
  /// <summary>
  /// Default Nc Validation Process Context
  /// </summary>
  public class DefaultNcValidationProcessContext : INcValidationProcessContext
  {
    /// <summary>
    /// Operation
    /// </summary>
    public string Operation { get; set; }
    
    /// <summary>
    /// Custom Context
    /// </summary>
    public INcCustomValidationProcessContext CustomContext { get; set; }

    /// <summary>
    /// Result Value Transformer
    /// </summary>
    public INcValidationResultValueTransformer ResultValueTransformer { get; set; }
  }
}