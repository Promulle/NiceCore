using FluentValidation;
using NiceCore.Services.Validation.Model;

namespace NiceCore.Services.Validation.Contexts
{
  /// <summary>
  /// Default impl for INcValidatorCallContext
  /// </summary>
  public class DefaultNcValidatorCallContext : INcValidatorCallContext
  {
    /// <summary>
    /// Instance to be validated
    /// </summary>
    public object Instance { get; init; }
    
    /// <summary>
    /// Current SubValidation Node
    /// </summary>
    public SubValidationNode SubValidationNode { get; init; }
    
    /// <summary>
    /// To Context
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public ValidationContext<T> ToContext<T>()
    {
      return new ((T) Instance);
    }
  }
}