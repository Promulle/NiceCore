﻿using NiceCore.Base.Progress;
using NiceCore.Base.Services;
using System;

namespace NiceCore.Services.Progress
{
  /// <summary>
  /// Service that holds progress
  /// </summary>
  public interface IProgressService : IService
  {
    /// <summary>
    /// Event that is fired when a progress is explicitly unregistered or due to being finished
    /// </summary>
    event EventHandler<ProgressEventArgs> ProgressUnregistered;
    /// <summary>
    /// Event that is fired when the state of a progress has changed
    /// </summary>
    event EventHandler<ProgressEventArgs> ProgressChanged;
    /// <summary>
    /// Event that is fired when a new progress has been registered
    /// </summary>
    event EventHandler<ProgressEventArgs> NewProgressRegistered;
    /// <summary>
    /// Registers a new progress object to be tracked. i_Name usually corresponds to the ThreadName
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Current"></param>
    /// <param name="i_Max"></param>
    /// <param name="i_IsInditerminate"></param>
    void RegisterProgress(string i_Name, long i_Current, long i_Max, bool i_IsInditerminate);
    /// <summary>
    /// Stops tracking of progress which corresponds to i_Name.
    /// This method is called if AddProgress reaches or surpasses max value of progress, but is explicit needed, when inditerminate
    /// </summary>
    /// <param name="i_Name"></param>
    void UnregisterProgress(string i_Name);
    /// <summary>
    /// Returns the state of the progress that corresponds to i_Name.
    /// Returns null if not found.
    /// </summary>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    ProgressState GetState(string i_Name);
    /// <summary>
    /// Adds progress to an existing progress-object
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Progress"></param>
    void AddProgress(string i_Name, long i_Progress);
    /// <summary>
    /// Adds progress to an existing progress-object
    /// </summary>
    /// <param name="i_Progress"></param>
    void AddProgress(long i_Progress);
    /// <summary>
    /// Subtracts progress from the progress-object, that corresponds to i_Name.
    /// i_Progress should be smaller than 0 for this function to work correctly.
    /// </summary>
    /// <param name="i_Progress"></param>
    void SubtractProgress(long i_Progress);
    /// <summary>
    /// Refreshes the progress called i_Name with the other parameters as values
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Current"></param>
    /// <param name="i_Max"></param>
    /// <param name="i_IsIndeterminate"></param>
    void RefreshProgress(string i_Name, long i_Current, long i_Max, bool i_IsIndeterminate);
    /// <summary>
    /// Refreshes the progress called i_Name with the other parameters as values
    /// </summary>
    /// <param name="i_Current"></param>
    /// <param name="i_Max"></param>
    /// <param name="i_IsIndeterminate"></param>
    void RefreshProgress(long i_Current, long i_Max, bool i_IsIndeterminate);
  }
}
