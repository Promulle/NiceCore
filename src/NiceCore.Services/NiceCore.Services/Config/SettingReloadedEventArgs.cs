﻿namespace NiceCore.Services.Config
{
  /// <summary>
  /// Event args for setting reloaded event
  /// </summary>
  public struct SettingReloadedEventArgs<T>
  {
    /// <summary>
    /// Key the setting is retrieved by
    /// </summary>
    public string Key { get; init; }

    /// <summary>
    /// Context this setting is retrieved under. This could be for example the Service Instance that read the setting. Can be null
    /// </summary>
    public string Context { get; init; }

    /// <summary>
    /// Value that was retrieved during the reload
    /// </summary>
    public T Value { get; init; }
  }
}
