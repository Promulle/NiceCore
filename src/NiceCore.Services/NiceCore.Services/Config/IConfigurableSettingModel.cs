﻿using System;

namespace NiceCore.Services.Config
{
  /// <summary>
  /// Interface for which all possible registrations should be registered
  /// </summary>
  public interface IConfigurableSettingModel
  {
    /// <summary>
    /// Key the setting can be found under
    /// </summary>
    string Key { get; }

    /// <summary>
    /// Type of setting
    /// </summary>
    Type SettingType { get; }
  }
}
