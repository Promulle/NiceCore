using System;
using NiceCore.Eventing.Config;
using NiceCore.ServiceLocation;

namespace NiceCore.Services.Config.Settings.Messages
{
  /// <summary>
  /// Setting for ClientIdentifier for messaging
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class MessagingClientIdentifierSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// key
    /// </summary>
    public string Key { get; } = MessagingClientIdentifierSettingConstants.KEY;
    
    /// <summary>
    /// Type
    /// </summary>
    public Type SettingType { get; } = typeof(string);
  }
}