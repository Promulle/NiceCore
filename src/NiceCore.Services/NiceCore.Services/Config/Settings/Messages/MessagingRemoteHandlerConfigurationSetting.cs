﻿using System;
using NiceCore.Eventing;
using NiceCore.ServiceLocation;

namespace NiceCore.Services.Config.Settings.Messages
{
  /// <summary>
  /// Messaging Remote Handler Configuration
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class MessagingRemoteHandlerConfigurationSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// Key
    /// </summary>
    public const string KEY = "NiceCore.Messages.Remote.Configuration";

    /// <summary>
    /// Key
    /// </summary>
    public string Key { get; } = KEY;
    
    /// <summary>
    /// Setting Type
    /// </summary>
    public Type SettingType { get; } = typeof(IHandlerConfig);
  }
}