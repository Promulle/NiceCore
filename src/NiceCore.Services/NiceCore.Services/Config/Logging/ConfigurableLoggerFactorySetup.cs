﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using NiceCore.Logging;
using NiceCore.ServiceLocation;
using NiceCore.ServiceLocation.Helpers;
using NiceCore.Services.Config.Logging.Utils;

namespace NiceCore.Services.Config.Logging
{
  /// <summary>
  /// Configurable Logger Factory setup
  /// </summary>
  public sealed class ConfigurableLoggerFactorySetup : ILoggerFactorySetup
  {
    private readonly IServiceContainer m_ServiceContainer;
    private readonly INcConfiguration m_Configuration;
    
    /// <summary>
    /// LoggerFactory Setup that uses configuration and the ServiceContainer
    /// </summary>
    /// <param name="i_Container"></param>
    /// <param name="i_Configuration"></param>
    public ConfigurableLoggerFactorySetup(IServiceContainer i_Container, INcConfiguration i_Configuration)
    {
      m_ServiceContainer = i_Container;
      m_Configuration = i_Configuration;
    }
    
    /// <summary>
    /// Setups a logger factory
    /// </summary>
    /// <returns></returns>
    /// <exception cref="InvalidOperationException">If no INcLoggingProvider is registered</exception>
    public ILoggerFactory SetupLoggerFactory()
    {
      return SetupLoggerFactoryFor(m_ServiceContainer, m_Configuration);
    }
    
    /// <summary>
    /// Setup Logger Factory for
    /// </summary>
    /// <param name="i_Container"></param>
    /// <param name="i_Configuration"></param>
    /// <returns></returns>
    /// <exception cref="InvalidOperationException"></exception>
    public static ILoggerFactory SetupLoggerFactoryFor(IServiceContainer i_Container, INcConfiguration i_Configuration)
    {
      using var loggerProviders = i_Container.LocateAll<INcLoggingProvider>();
      if (loggerProviders.HasInstances())
      {
        var providersToUse = i_Configuration.GetCollection<string>(LoggerProvidersSetting.KEY);
        var providerInstances = loggerProviders.Instances();
        return LoggerFactory.Create(builder =>
        {
          ApplyAdditionalScopes(builder, i_Configuration);
          builder.AddConfiguration(i_Configuration.GetSection("NiceCore:Logging"));
          foreach (var provider in GetProvidersToUse(providerInstances, providersToUse))
          {
            provider.ApplyToLoggingBuilder(builder);
          }
        });
      }
      throw new InvalidOperationException("Could not create loggerFactory with ConfigurableLoggerFactory. No loggerProviders registered for INcLoggingProvider");
    }

    private static void ApplyAdditionalScopes(ILoggingBuilder t_Builder, INcConfiguration i_Configuration)
    {
      var additionalScopes = i_Configuration.GetCollection<ActivityTrackingOptions>(AdditionalLoggerScopesSetting.KEY);
      if (additionalScopes.Count == 1)
      {
        t_Builder.Configure(opt => opt.ActivityTrackingOptions = additionalScopes[0]);
      }
      if (additionalScopes.Count > 1)
      {
        t_Builder.Configure(opt => opt.ActivityTrackingOptions = AdditionalScopesEnumUtil.ComputeXOROf(additionalScopes));
      }
    }

    private static IEnumerable<INcLoggingProvider> GetProvidersToUse(IEnumerable<INcLoggingProvider> i_Providers, List<string> i_AllowedNames)
    {
      if (i_AllowedNames.Count == 0)
      {
        return i_Providers;
      }

      var allowedNamesSet = i_AllowedNames.ToHashSet();
      var res = new List<INcLoggingProvider>();
      foreach (var ncLoggingProvider in i_Providers)
      {
        if (allowedNamesSet.Contains(ncLoggingProvider.Name))
        {
          res.Add(ncLoggingProvider);
        }
      }
      return res;
    }
  }
}