﻿using System;
using System.Collections.Generic;
using NiceCore.ServiceLocation;

namespace NiceCore.Services.Config.Logging
{
  /// <summary>
  /// Setting for Logger Providers to use
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public sealed class LoggerProvidersSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// KEY
    /// </summary>
    public const string KEY = "NiceCore.Logging.Providers";

    /// <summary>
    /// Key
    /// </summary>
    public string Key { get; } = KEY;

    /// <summary>
    /// Type
    /// </summary>
    public Type SettingType { get; } = typeof(List<string>);
  }
}