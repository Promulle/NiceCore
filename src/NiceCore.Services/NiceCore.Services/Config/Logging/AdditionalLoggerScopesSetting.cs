// Copyright 2021 Sycorax Systemhaus GmbH

using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using NiceCore.ServiceLocation;

namespace NiceCore.Services.Config.Logging
{
  /// <summary>
  /// Additional Logger Scopes
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class AdditionalLoggerScopesSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// KEY
    /// </summary>
    public const string KEY = "NiceCore.Logging.AdditionalLoggerScopes";
    
    /// <summary>
    /// KEY
    /// </summary>
    public string Key { get; } = KEY;

    /// <summary>
    /// Setting Type
    /// </summary>
    public Type SettingType { get; } = typeof(List<ActivityTrackingOptions>);
  }
}
