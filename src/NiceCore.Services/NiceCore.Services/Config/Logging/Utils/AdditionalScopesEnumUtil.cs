using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace NiceCore.Services.Config.Logging.Utils
{
  /// <summary>
  /// Util functions for ActivityTrackingOptions
  /// </summary>
  public static class AdditionalScopesEnumUtil
  {
    /// <summary>
    /// Computes XOR of all values in i_Enums
    /// </summary>
    /// <param name="i_Enums"></param>
    /// <returns></returns>
    public static ActivityTrackingOptions ComputeXOROf(List<ActivityTrackingOptions> i_Enums)
    {
      if (i_Enums == null || i_Enums.Count == 0)
      {
        throw new ArgumentException($"Can not {nameof(ComputeXOROf)} because enum list is either null or empty.");
      }
      var computedVal = i_Enums[0];
      for (var i = 1; i < i_Enums.Count; i++)
      {
        computedVal |= i_Enums[1];
      }

      return computedVal;
    }
  }
}