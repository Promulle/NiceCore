using System;
using System.Threading.Tasks;

namespace NiceCore.Services.Config
{
  /// <summary>
  /// Async Reloadable Setting
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public interface IAsyncReloadableSetting<T> : IReloadableSetting<T> 
  {
    /// <summary>
    /// Returns the value of the setting
    /// </summary>
    /// <returns></returns>
    ValueTask<T> GetValue();

    /// <summary>
    /// Reloads the setting. This should be called automatically, but can also be called manually
    /// </summary>
    ValueTask Reload();
    
    /// <summary>
    /// Add a new listener that reacts on the changed setting. This also calls i_ListenerFunc if the current value is not default
    /// </summary>
    /// <param name="i_ListenerFunc"></param>
    /// <param name="i_DirectlyCallListener"></param>
    ValueTask AddListener(Func<SettingReloadedEventArgs<T>, ValueTask> i_ListenerFunc, bool i_DirectlyCallListener);
  }
}