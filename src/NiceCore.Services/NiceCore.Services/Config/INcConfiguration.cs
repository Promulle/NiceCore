﻿using System;
using Microsoft.Extensions.Configuration;
using NiceCore.Base;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace NiceCore.Services.Config
{
  /// <summary>
  /// Configuration for nicecore apps
  /// </summary>
  public interface INcConfiguration : IInitializable, IBaseDisposable, IConfiguration
  {
    /// <summary>
    /// Configuration sources used to build the underlying configuration
    /// </summary>
    IList<INcConfigurationSource> ConfigurationSources { get; }
    
    /// <summary>
    /// is Ready
    /// </summary>
    bool IsReady { get; }

    /// <summary>
    /// Set Logger
    /// </summary>
    /// <param name="i_Logger"></param>
    void SetLogger(ILogger i_Logger);

    /// <summary>
    /// Returns all elements defined for key i_Key
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Key"></param>
    /// <returns></returns>
    List<T> GetCollection<T>(string i_Key);
    
    /// <summary>
    /// Searches the configuration for a value defined by i_Key
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Key"></param>
    /// <param name="i_DefaultValue"></param>
    /// <returns></returns>
    T GetValue<T>(string i_Key, T i_DefaultValue);

    /// <summary>
    /// Searches the configuration for a value defined by i_Key. i_Type is used for binding the result to an object. It is then cast to typeof(T) to be returned
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Key"></param>
    /// <param name="i_Type"></param>
    /// <param name="i_DefaultValue"></param>
    /// <returns></returns>
    T GetValue<T>(string i_Key, Type i_Type, T i_DefaultValue);

    /// <summary>
    /// Returns a settings instance that can be automatically reloaded
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Key"></param>
    /// <param name="i_DefaultValue"></param>
    /// <returns></returns>
    ValueTask<IAsyncReloadableSetting<T>> GetSetting<T>(string i_Key, T i_DefaultValue);
    
    /// <summary>
    /// Returns a settings instance that can be automatically reloaded
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Key"></param>
    /// <param name="i_DefaultValue"></param>
    /// <returns></returns>
    ISyncReloadableSetting<T> GetSyncReloadableSetting<T>(string i_Key, T i_DefaultValue);

    /// <summary>
    /// Returns a settings instance that can be automatically reloaded
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Key"></param>
    /// <returns></returns>
    ValueTask<IAsyncReloadableSetting<List<T>>> GetCollectionSetting<T>(string i_Key);


    /// <summary>
    /// Returns a string representation of all available values that can be configured
    /// </summary>
    /// <returns></returns>
    public string GetAvailableValues();
  }
}
