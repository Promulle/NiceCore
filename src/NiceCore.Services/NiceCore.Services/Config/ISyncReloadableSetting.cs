using System;

namespace NiceCore.Services.Config
{
  /// <summary>
  /// Sync Reloadable Setting
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public interface ISyncReloadableSetting<T> : IReloadableSetting<T>
  {
    /// <summary>
    /// Returns the value of the setting
    /// </summary>
    /// <returns></returns>
    T GetValue();

    /// <summary>
    /// Reloads the setting. This should be called automatically, but can also be called manually
    /// </summary>
    void Reload();
    
    /// <summary>
    /// Add a new listener that reacts on the changed setting. This also calls i_ListenerFunc if the current value is not default
    /// </summary>
    /// <param name="i_ListenerFunc"></param>
    /// <param name="i_DirectlyCallListener"></param>
    void AddListener(Action<SettingReloadedEventArgs<T>> i_ListenerFunc, bool i_DirectlyCallListener);
  }
}