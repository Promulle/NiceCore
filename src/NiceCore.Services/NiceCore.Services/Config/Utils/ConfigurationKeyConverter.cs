namespace NiceCore.Services.Config.Utils
{
  /// <summary>
  /// Converter for converting key formats
  /// </summary>
  public static class ConfigurationKeyConverter
  {
    /// <summary>
    /// Converts nc format to MS Format
    /// </summary>
    /// <param name="i_Key"></param>
    /// <returns></returns>
    public static string ConvertKeyToMSFormat(string i_Key)
    {
      return i_Key.Replace('.', ':');
    }
  }
}