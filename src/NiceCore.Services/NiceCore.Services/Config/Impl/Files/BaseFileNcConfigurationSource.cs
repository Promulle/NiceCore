namespace NiceCore.Services.Config.Impl.Files
{
  /// <summary>
  /// Base File Nc Configuration Source
  /// </summary>
  public abstract class BaseFileNcConfigurationSource : BaseNcConfigurationSource
  {
    /// <summary>
    /// whether this source is an optional file. Non optional sources raise FileNotFound Exceptions if the defined file could not be located
    /// </summary>
    public abstract bool IsOptional { get; }

    /// <summary>
    /// Whether the defined file should be monitored for changes
    /// </summary>
    public abstract bool ReloadOnChange { get; set; }
  }
}