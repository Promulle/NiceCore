using System.Collections.Generic;
using Microsoft.Extensions.Configuration;

namespace NiceCore.Services.Config.Impl.Files
{
  /// <summary>
  /// Base Class for multi file configuration sources
  /// </summary>
  public abstract class BaseMultiFileNcConfigurationSource : BaseFileNcConfigurationSource
  {
    /// <summary>
    /// Gets the file paths to use for this configuration source
    /// </summary>
    /// <returns></returns>
    public abstract IReadOnlyCollection<string> GetFilePaths();

    /// <summary>
    /// Apply Single Builder
    /// </summary>
    /// <param name="i_File"></param>
    /// <param name="t_Builder"></param>
    protected abstract void ApplySingleFile(string i_File, ConfigurationBuilder t_Builder);
     
    /// <summary>
    /// Apply
    /// </summary>
    /// <param name="t_Builder"></param>
    public override void Apply(ConfigurationBuilder t_Builder)
    {
      foreach (var filePath in GetFilePaths())
      {
        ApplySingleFile(filePath, t_Builder);
      }
    }
  }
}