using System.Collections.Generic;
using System.IO;

namespace NiceCore.Services.Config.Impl.Files
{
  /// <summary>
  /// Base Simple Pattrn Multi File Nc Configuration
  /// </summary>
  public abstract class BaseSimplePatternMultiFileNcConfiguration : BaseMultiFileNcConfigurationSource
  {
    private static readonly EnumerationOptions DEFAULT_OPTIONS = new()
    {
      ReturnSpecialDirectories = false,
      MatchCasing = MatchCasing.CaseInsensitive
    };
    
    /// <summary>
    /// Returns directory
    /// </summary>
    /// <returns></returns>
    public abstract string GetFileDirectory();
    
    /// <summary>
    /// Get Pattern
    /// </summary>
    /// <returns></returns>
    public abstract string GetPattern();

    /// <summary>
    /// Determine how strings are matched to the simple pattern
    /// </summary>
    /// <returns></returns>
    public virtual EnumerationOptions GetDirectoryEnumerationOptions()
    {
      return DEFAULT_OPTIONS;
    }

    /// <summary>
    /// Gets file path based on simple pattern
    /// </summary>
    /// <returns></returns>
    public override IReadOnlyCollection<string> GetFilePaths()
    {
      return Directory.GetFiles(GetFileDirectory(), GetPattern(), GetDirectoryEnumerationOptions());
    }
  }
}