﻿using Microsoft.Extensions.Configuration;

namespace NiceCore.Services.Config.Impl.Files.XML
{
  /// <summary>
  /// Base impl for xml file source
  /// </summary>
  public abstract class BaseXMLFileNcConfigurationSource : BaseSingleFileNcConfigurationSource
  {
    /// <summary>
    /// Add defined xml file to builder
    /// </summary>
    /// <param name="t_Builder"></param>
    public override void Apply(ConfigurationBuilder t_Builder)
    {
      t_Builder.AddXmlFile(GetFilePath(), IsOptional, ReloadOnChange);
    }
  }
}
