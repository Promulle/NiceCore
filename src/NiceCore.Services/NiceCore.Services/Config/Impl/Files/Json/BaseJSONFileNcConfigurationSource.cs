﻿using Microsoft.Extensions.Configuration;

namespace NiceCore.Services.Config.Impl.Files.Json
{
  /// <summary>
  /// base impl for JSON configuration sources
  /// </summary>
  public abstract class BaseJSONFileNcConfigurationSource : BaseSingleFileNcConfigurationSource
  {
    /// <summary>
    /// Apply that adds json file
    /// </summary>
    /// <param name="t_Builder"></param>
    public override void Apply(ConfigurationBuilder t_Builder)
    {
      t_Builder.AddJsonFile(GetFilePath(), IsOptional, ReloadOnChange);
    }
  }
}
