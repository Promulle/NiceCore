using Microsoft.Extensions.Configuration;

namespace NiceCore.Services.Config.Impl.Files.Json
{
  /// <summary>
  /// Base Json Multi File Nc Configuration Source
  /// </summary>
  public abstract class BaseJSONSimplePatternMultiFileNcConfigurationSource : BaseSimplePatternMultiFileNcConfiguration
  {
    /// <summary>
    /// Apply Single file
    /// </summary>
    /// <param name="i_File"></param>
    /// <param name="t_Builder"></param>
    protected override void ApplySingleFile(string i_File, ConfigurationBuilder t_Builder)
    {
      t_Builder.AddJsonFile(i_File, IsOptional, ReloadOnChange);
    }
  }
}