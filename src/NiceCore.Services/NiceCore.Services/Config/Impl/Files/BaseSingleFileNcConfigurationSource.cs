﻿namespace NiceCore.Services.Config.Impl.Files
{
  /// <summary>
  /// Base impl for all file sources
  /// </summary>
  public abstract class BaseSingleFileNcConfigurationSource : BaseFileNcConfigurationSource
  {
    /// <summary>
    /// Returns the file path that should be used
    /// </summary>
    /// <returns></returns>
    protected abstract string GetFilePath();
  }
}
