﻿using System.Reflection;

namespace NiceCore.Services.Config.Impl.AssemblyBased
{
  /// <summary>
  /// Default Assembly json config from entry assembly
  /// </summary>
  public class EntryAssemblyJSONNcConfigurationSource : BaseAssemblyJSONNcConfgigurationSource
  {
    /// <summary>
    /// isoptional
    /// </summary>
    public override bool IsOptional { get; } = true;

    /// <summary>
    /// Reload on change
    /// </summary>
    public override bool ReloadOnChange { get; set; } = true;

    /// <summary>
    /// Priority
    /// </summary>
    public override int Priority { get; } = 0;

    /// <summary>
    /// Override using entry assembly
    /// </summary>
    /// <returns></returns>
    protected override Assembly GetAssembly()
    {
      return Assembly.GetEntryAssembly();
    }
  }
}
