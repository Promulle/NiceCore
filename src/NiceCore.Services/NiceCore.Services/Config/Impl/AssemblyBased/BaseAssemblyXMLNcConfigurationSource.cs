﻿using Microsoft.Extensions.Configuration;

namespace NiceCore.Services.Config.Impl.AssemblyBased
{
  /// <summary>
  /// Base class for assembly based config files in xml format
  /// </summary>
  public abstract class BaseAssemblyXMLNcConfigurationSource : BaseAssemblyConfigNcConfigurationSource
  {
    private const string EXTENSION = ".xml.config";
    /// <summary>
    /// .xml.config
    /// </summary>
    protected override string FileExtension { get; } = EXTENSION;

    /// <summary>
    /// Apply
    /// </summary>
    /// <param name="t_Builder"></param>
    public override void Apply(ConfigurationBuilder t_Builder)
    {
      t_Builder.AddXmlFile(GetFilePath(), IsOptional, ReloadOnChange);
    }
  }
}
