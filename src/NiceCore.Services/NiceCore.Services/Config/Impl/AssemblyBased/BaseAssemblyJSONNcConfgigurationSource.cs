﻿using Microsoft.Extensions.Configuration;

namespace NiceCore.Services.Config.Impl.AssemblyBased
{
  /// <summary>
  /// Base impl for assemby ba
  /// </summary>
  public abstract class BaseAssemblyJSONNcConfgigurationSource : BaseAssemblyConfigNcConfigurationSource
  {
    private const string EXTENSION = ".json";
    /// <summary>
    /// File extension
    /// </summary>
    protected override string FileExtension { get; } = EXTENSION;

    /// <summary>
    /// Apply
    /// </summary>
    /// <param name="t_Builder"></param>
    public override void Apply(ConfigurationBuilder t_Builder)
    {
      t_Builder.AddJsonFile(GetFilePath(), IsOptional, ReloadOnChange);
    }
  }
}
