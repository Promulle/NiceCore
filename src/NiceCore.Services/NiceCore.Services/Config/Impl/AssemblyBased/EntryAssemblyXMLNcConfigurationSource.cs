﻿using System.Reflection;

namespace NiceCore.Services.Config.Impl.AssemblyBased
{
  /// <summary>
  /// Entry assembly config for xml files
  /// </summary>
  public class EntryAssemblyXMLNcConfigurationSource : BaseAssemblyXMLNcConfigurationSource
  {
    /// <summary>
    /// isoptional
    /// </summary>
    public override bool IsOptional { get; } = true;

    /// <summary>
    /// Reload
    /// </summary>
    public override bool ReloadOnChange { get; set; } = true;

    /// <summary>
    /// Priority
    /// </summary>
    public override int Priority { get; } = 0;

    /// <summary>
    /// Returns the assembly
    /// </summary>
    /// <returns></returns>
    protected override Assembly GetAssembly()
    {
      return Assembly.GetEntryAssembly();
    }
  }
}
