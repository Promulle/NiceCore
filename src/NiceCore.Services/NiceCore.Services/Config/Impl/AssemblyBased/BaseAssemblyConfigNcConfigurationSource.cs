﻿using System.IO;
using System.Reflection;
using NiceCore.Services.Config.Impl.Files;

namespace NiceCore.Services.Config.Impl.AssemblyBased
{
  /// <summary>
  /// Configuration Source for dll configs of the assembly
  /// </summary>
  public abstract class BaseAssemblyConfigNcConfigurationSource : BaseSingleFileNcConfigurationSource
  {
    /// <summary>
    /// File extension
    /// </summary>
    protected abstract string FileExtension { get; }

    /// <summary>
    /// uses the file path of the dll config of the assembly this type is defined in
    /// </summary>
    /// <returns></returns>
    protected override string GetFilePath()
    {
      var assembly = GetAssembly();
      return $"{Path.GetDirectoryName(assembly.Location)}{Path.DirectorySeparatorChar}{assembly.GetName().Name}{FileExtension}";
    }

    /// <summary>
    /// Function that returns the assembly to be used
    /// </summary>
    /// <returns></returns>
    protected virtual Assembly GetAssembly()
    {
      return GetType().Assembly;
    }
  }
}
