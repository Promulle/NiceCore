﻿using Microsoft.Extensions.Configuration;

namespace NiceCore.Services.Config.Impl
{
  /// <summary>
  /// base impl for environment variable configuration
  /// </summary>
  public abstract class BaseEnvironmentNcConfigurationSource : BaseNcConfigurationSource
  {
    /// <summary>
    /// Prefix that is used for environment variables added by this configurator
    /// </summary>
    protected abstract string Prefix { get; }

    /// <summary>
    /// apply environment config to builder
    /// </summary>
    /// <param name="t_Builder"></param>
    public override void Apply(ConfigurationBuilder t_Builder)
    {
      t_Builder.AddEnvironmentVariables(Prefix);
    }
  }
}
