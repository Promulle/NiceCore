﻿using System.Collections.Generic;

namespace NiceCore.Services.Config.Impl
{
  /// <summary>
  /// In Memory config source that sets the dictionary from its constructor
  /// </summary>
  public sealed class DefaultInMemoryNcConfigurationSource : BaseInMemoryNcConfigurationSource
  {
    /// <summary>
    /// Values
    /// </summary>
    public override IDictionary<string, string> Values { get; init; }

    /// <summary>
    /// Priority
    /// </summary>
    public override int Priority { get; }

    /// <summary>
    /// constructor filling values
    /// </summary>
    public DefaultInMemoryNcConfigurationSource()
    {
    }

    /// <summary>
    /// constructor filling values
    /// </summary>
    public DefaultInMemoryNcConfigurationSource(IDictionary<string, string> i_Values)
    {
      Values = i_Values;
    }
  }
}
