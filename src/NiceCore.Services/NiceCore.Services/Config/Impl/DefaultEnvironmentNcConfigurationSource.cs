﻿namespace NiceCore.Services.Config.Impl
{
  /// <summary>
  /// Default config source for env variables
  /// </summary>
  public class DefaultEnvironmentNcConfigurationSource : BaseEnvironmentNcConfigurationSource
  {
    /// <summary>
    /// Priority
    /// </summary>
    public override int Priority { get; } = 1;

    /// <summary>
    /// Prefix
    /// </summary>
    protected override string Prefix { get; } = "Nc_";
  }
}
