﻿using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace NiceCore.Services.Config.Impl
{
  /// <summary>
  /// Base impl for in memory configuration sources
  /// </summary>
  public abstract class BaseInMemoryNcConfigurationSource : BaseNcConfigurationSource
  {
    /// <summary>
    /// Values used for configuration, if this is null, nothing is added to the configuration
    /// </summary>
    public abstract IDictionary<string, string> Values { get; init; }

    /// <summary>
    /// Apply
    /// </summary>
    /// <param name="t_Builder"></param>
    public override void Apply(ConfigurationBuilder t_Builder)
    {
      if (Values != null)
      {
        t_Builder.AddInMemoryCollection(Values);
      }
    }
  }
}
