﻿using Microsoft.Extensions.Configuration;

namespace NiceCore.Services.Config.Impl
{
  /// <summary>
  /// Base impl for configuration sources
  /// </summary>
  public abstract class BaseNcConfigurationSource : INcConfigurationSource
  {
    /// <summary>
    /// Priority, lower priorities will be overriden by hígher priorities
    /// </summary>
    public abstract int Priority { get; }

    /// <summary>
    /// Apply
    /// </summary>
    /// <param name="t_Builder"></param>
    public abstract void Apply(ConfigurationBuilder t_Builder);
  }
}
