namespace NiceCore.Services.Config.SettingsRepository
{
  /// <summary>
  /// Model for a nicecore based setting
  /// </summary>
  public sealed class NcSettingModelReference
  {
    /// <summary>
    /// Key of the setting
    /// </summary>
    public string Key { get; set; }
    
    /// <summary>
    /// Name of the setting (Type Name)
    /// </summary>
    public string Name { get; set; }
  }
}