using System;
using System.Collections.Generic;

namespace NiceCore.Services.Config.SettingsRepository
{
  /// <summary>
  /// Single node in configuration value tree
  /// </summary>
  public sealed class NcSettingModelNode
  {
    /// <summary>
    /// The Key up to this point
    /// </summary>
    public string Key { get; set; }
    
    /// <summary>
    /// Name of the node
    /// </summary>
    public string Name { get; set; }
    
    /// <summary>
    /// Value that is set, if this is a value node
    /// </summary>
    public string Value { get; set; }
    
    /// <summary>
    /// Provider that set the value 
    /// </summary>
    public string ValueSource { get; set; }
    
    /// <summary>
    /// Is Preview
    /// </summary>
    public bool IsPreview { get; set; }
    
    /// <summary>
    /// Possible Values
    /// </summary>
    public string PossibleValues { get; set; }
    
    /// <summary>
    /// Value Type
    /// </summary>
    public string ValueType { get; set; }
    
    /// <summary>
    /// Setting that this node represents
    /// </summary>
    public NcSettingModelReference Setting { get; set; }

    /// <summary>
    /// Sub Nodes set by configuration
    /// </summary>
    public List<NcSettingModelNode> SubNodes { get; set; } = new();
  }
}