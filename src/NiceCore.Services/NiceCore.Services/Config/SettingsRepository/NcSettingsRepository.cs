using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using NiceCore.Reflection;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config.Utils;
using NiceCore.Services.Scheduling;
using NiceCore.Services.Scheduling.Configuration.Model;

namespace NiceCore.Services.Config.SettingsRepository
{
  /// <summary>
  /// Nc Settings Repository
  /// </summary>
  [Register(InterfaceType = typeof(INcSettingsRepository))]
  public class NcSettingsRepository : INcSettingsRepository
  {
    /// <summary>
    /// Configuration
    /// </summary>
    public INcConfiguration Configuration { get; set; }

    /// <summary>
    /// Configured Setting models
    /// </summary>
    public IList<IConfigurableSettingModel> ConfiguredSettingModels { get; set; }
    
    /// <summary>
    /// All possible scheduled services
    /// </summary>
    public IList<IScheduledService> ScheduledServices { get; set; }

    /// <summary>
    /// Get applied settings overview
    /// </summary>
    /// <returns></returns>
    public NcSettingsOverview GetAppliedSettingsOverview()
    {
      if (ConfiguredSettingModels == null)
      {
        return NcSettingsOverview.EMPTY;
      }

      if (Configuration == null)
      {
        throw new InvalidOperationException("Could not get applied settings overview, no config registered.");
      }

      if (Configuration is IHasConfigurationRoot config)
      {
        var root = config.GetConfigurationRoot();
        return GetOverviewForRoot(root, ConfiguredSettingModels, ScheduledServices);
      }

      return NcSettingsOverview.EMPTY;
    }

    private static NcSettingsOverview GetOverviewForRoot(IConfigurationRoot i_Root, IList<IConfigurableSettingModel> i_Models, IList<IScheduledService> i_ScheduledServices)
    {
      var allModels = GetAllModels(i_Models, i_ScheduledServices);
      var availableSettings = GetAvailableSettings(allModels);
      return new()
      {
        ConfiguredSettings = GetTopLevelNodesFor(i_Root, availableSettings),
        AvailableSettings = availableSettings
      };
    }

    private static IList<IConfigurableSettingModel> GetAllModels(IList<IConfigurableSettingModel> i_Models, IList<IScheduledService> i_ScheduledServices)
    {
      var configurableServices = i_ScheduledServices != null ? i_ScheduledServices.OfType<BaseConfigurableScheduledService>() : Enumerable.Empty<BaseConfigurableScheduledService>();
      var res = new List<IConfigurableSettingModel>(i_Models);
      foreach (var configurableService in configurableServices)
      {
        res.Add(new ConfiguredSettingModelCopy()
        {
          Key = configurableService.GetConfigurationKey(),
          SettingType = typeof(SchedulingServiceEntryConfigurationModel)
        });
      }
      return res;
    }

    private static List<NcSettingModel> GetAvailableSettings(IList<IConfigurableSettingModel> i_Models)
    {
      var res = new List<NcSettingModel>(i_Models.Count);
      for (var i = 0; i < i_Models.Count; i++)
      {
        res.Add(GetAvailableSetting(i_Models[i]));
      }

      return res;
    }

    private static NcSettingModel GetAvailableSetting(IConfigurableSettingModel i_Model)
    {
      var key = ConfigurationKeyConverter.ConvertKeyToMSFormat(i_Model.Key);
      return new()
      {
        IsSet = false,
        SettingInstance = i_Model,
        Key = key,
        TypeName = i_Model.SettingType.FullName,
        Nodes = GetNodeStructureForType(i_Model.SettingType, key)
      };
    }

    private static NcSettingModelNode GetNodeStructureForType(Type i_Type, string i_ParentKey)
    {
      var nodeName = i_ParentKey.Split(':', StringSplitOptions.RemoveEmptyEntries).Last();
      var nodeType = GetTypeNullableSafe(i_Type);
      if (IsSimpleType(nodeType))
      {
        return new()
        {
          Key = i_ParentKey,
          IsPreview = true,
          ValueType = nodeType.Name,
          Name = nodeName
        };
      }

      if (nodeType.IsEnum)
      {
        return new()
        {
          Key = i_ParentKey,
          IsPreview = true,
          PossibleValues = string.Join(", ", Enum.GetValues(nodeType).OfType<object>()),
          ValueType = nodeType.Name,
          Name = nodeName
        };
      }

      if (IsCollection(nodeType))
      {
        if (IsDictionary(nodeType))
        {
          var dictionarySubNodes = GetSubNodesStructureForGenericCollectionType(nodeType, i_ParentKey, 2);
          return new()
          {
            Key       = i_ParentKey,
            Name      = nodeName,
            IsPreview = true,
            ValueType = i_Type.Name,
            SubNodes  = dictionarySubNodes
          };
        }
        var collectionSubNodes = GetSubNodesStructureForGenericCollectionType(nodeType, i_ParentKey, 1);
        return new()
        {
          Key = i_ParentKey,
          Name = nodeName,
          IsPreview = true,
          ValueType = i_Type.Name,
          SubNodes = collectionSubNodes
        };
      }

      //else: is complex type -> traverse properties
      return GetNodeStructureForComplexType(nodeType, i_ParentKey, nodeName);
    }

    private static List<NcSettingModelNode> GetSubNodesStructureForGenericCollectionType(Type i_Type, string i_ParentKey, int i_ArgPosition)
    {
      var genericArguments = i_Type.GetGenericArguments();
      if (genericArguments.Length == i_ArgPosition)
      {
        return new()
        {
          GetNodeStructureForType(genericArguments[i_ArgPosition - 1], i_ParentKey + ":ValueElement")
        };
      }
      return new();
    }

    private static Type GetTypeNullableSafe(Type i_Type)
    {
      var genericArgs = i_Type.GetGenericArguments();
      if (genericArgs.Length == 1 && !IsCollection(i_Type) && IsNullableSimpleType(i_Type) )
      {
        return genericArgs[0];
      }

      return i_Type;
    }

    private static bool IsNullableSimpleType(Type i_Type)
    {
      var res = ReflectionUtils.OpenGenericTypeIsBaseFor(typeof(Nullable<>), i_Type);
      return res.Success;
    }

    private static bool IsDictionary(Type i_Type)
    {
      var res = ReflectionUtils.OpenGenericTypeIsBaseFor(typeof(Dictionary<,>), i_Type) ;
      return res.Success;
    }
    
    private static bool IsCollection(Type i_Type)
    {
      return typeof(ICollection).IsAssignableFrom(i_Type);
    }

    private static NcSettingModelNode GetNodeStructureForComplexType(Type i_Type, string i_Key, string i_NodeName)
    {
      var properties = GetAllPropertiesOf(i_Type);
      var res = new List<NcSettingModelNode>();
      foreach (var property in properties)
      {
        res.Add(GetNodeStructureForType(property.PropertyType, i_Key + ":" + property.Name));
      }

      return new()
      {
        SubNodes = res,
        IsPreview = true,
        Key = i_Key,
        Name = i_NodeName,
        ValueType = i_Type.Name,
      };
    }

    private static List<PropertyInfo> GetAllPropertiesOf(Type i_Type)
    {
      var res = new List<PropertyInfo>();
      res.AddRange(i_Type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic));
      if (i_Type.BaseType != null && i_Type.BaseType != typeof(object))
      {
        res.AddRange(GetAllPropertiesOf(i_Type.BaseType));
      }

      return res;
    }

    private static bool IsSimpleType(Type i_Type)
    {
      return i_Type == typeof(Guid) ||
             i_Type == typeof(string) ||
             i_Type == typeof(int) ||
             i_Type == typeof(int?) ||
             i_Type == typeof(long) ||
             i_Type == typeof(long?) ||
             i_Type == typeof(short) ||
             i_Type == typeof(short?) ||
             i_Type == typeof(float) ||
             i_Type == typeof(float?) ||
             i_Type == typeof(double) ||
             i_Type == typeof(double?) ||
             i_Type == typeof(bool) ||
             i_Type == typeof(bool?) ||
             i_Type == typeof(Type);
    }

    private static List<NcSettingModelNode> GetTopLevelNodesFor(IConfigurationRoot i_Root, List<NcSettingModel> i_Models)
    {
      var configChildren = i_Root.GetChildren().ToArray();
      var subNodes = new List<NcSettingModelNode>(configChildren.Length);
      for (var i = 0; i < configChildren.Length; i++)
      {
        subNodes.Add(GetNodeFor(configChildren[i], i_Root.Providers.Reverse().ToList(), i_Models, null));
      }

      return subNodes;
    }

    private static NcSettingModelNode GetNodeFor(IConfigurationSection i_Section, List<IConfigurationProvider> i_Providers, List<NcSettingModel> i_Models, NcSettingModelNode i_NodeStructureFromSetting)
    {
      var (setting, model) = GetSettingModelReference(i_Section.Path, i_Models);
      var configChildren = i_Section.GetChildren().ToArray();
      var subNodes = new List<NcSettingModelNode>(configChildren.Length);
      foreach (var child in configChildren)
      {
        var definedSubNode = model?.Nodes?.SubNodes?.FirstOrDefault(r => r.Name == child.Key) ?? i_NodeStructureFromSetting?.SubNodes?.FirstOrDefault(r => r.Name == child.Key);
        subNodes.Add(GetNodeFor(child, i_Providers, i_Models, definedSubNode));
      }

      var additionalNodes = model?.Nodes?.SubNodes ?? i_NodeStructureFromSetting?.SubNodes;
      if (additionalNodes != null)
      {
        foreach (var modelNode in additionalNodes)
        {
          if (subNodes.All(structureNode => structureNode.Name != modelNode.Name))
          {
            subNodes.Add(modelNode);
          }
        }
      }
      
      var (value, provider) = CheckValue(i_Providers, i_Section.Path);
      return new()
      {
        SubNodes = subNodes,
        Key = i_Section.Path,
        Value = value,
        ValueSource = provider?.ToString(),
        IsPreview = false,
        Setting = setting,
        Name = i_Section.Key
      };
    }

    private static (string Value, IConfigurationProvider Provider) CheckValue(List<IConfigurationProvider> i_Providers, string i_Key)
    {
      foreach (var provider in i_Providers)
      {
        if (provider.TryGet(i_Key, out var val))
        {
          return (val, provider);
        }
      }

      return (null, null);
    }

    private static (NcSettingModelReference Reference, NcSettingModel Model) GetSettingModelReference(string i_Path, List<NcSettingModel> i_Models)
    {
      var setting = GetSettingForPath(i_Path, i_Models);
      if (setting != null)
      {
        setting.IsSet = true;
        return (new()
        {
          Name = setting.SettingInstance.GetType().Name,
          Key = setting.Key
        }, setting);
      }

      return (null, null);
    }

    private static NcSettingModel GetSettingForPath(string i_Path, List<NcSettingModel> i_Models)
    {
      foreach (var settingModel in i_Models)
      {
        if (i_Path == settingModel.Key)
        {
          return settingModel;
        }
      }

      return null;
    }
  }
}