namespace NiceCore.Services.Config.SettingsRepository
{
  /// <summary>
  /// Settings repository
  /// </summary>
  public interface INcSettingsRepository
  {
    /// <summary>
    /// Gets all applied settings for overview
    /// </summary>
    /// <returns></returns>
    NcSettingsOverview GetAppliedSettingsOverview();
  }
}