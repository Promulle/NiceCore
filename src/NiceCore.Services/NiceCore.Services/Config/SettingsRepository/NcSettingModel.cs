using System;
using System.Text.Json.Serialization;

namespace NiceCore.Services.Config.SettingsRepository
{
  /// <summary>
  /// Model for a setting
  /// </summary>
  public sealed class NcSettingModel
  {
    /// <summary>
    /// Key
    /// </summary>
    public string Key { get; set; }
    
    /// <summary>
    /// Type Name
    /// </summary>
    public string TypeName { get; set; }
    
    /// <summary>
    /// Whether the setting itself is set
    /// </summary>
    public bool IsSet { get; set; }
    
    /// <summary>
    /// Instance of the setting
    /// </summary>
    [JsonIgnore]
    public IConfigurableSettingModel SettingInstance { get; set; }

    /// <summary>
    /// Nodes
    /// </summary>
    public NcSettingModelNode Nodes { get; set; }
  }
}