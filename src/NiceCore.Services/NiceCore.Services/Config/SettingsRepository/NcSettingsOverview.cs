using System;
using System.Collections.Generic;

namespace NiceCore.Services.Config.SettingsRepository
{
  /// <summary>
  /// Nc Settings Overview
  /// </summary>
  public sealed class NcSettingsOverview
  {
    /// <summary>
    /// Empty
    /// </summary>
    public static readonly NcSettingsOverview EMPTY = new();
    
    /// <summary>
    /// Configured Settings
    /// </summary>
    public List<NcSettingModelNode> ConfiguredSettings { get; set; } = new();

    /// <summary>
    /// All Available Settings
    /// </summary>
    public List<NcSettingModel> AvailableSettings { get; set; } = new();
  }
}