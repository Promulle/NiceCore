using System;
using NiceCore.Services.Config.Utils;

namespace NiceCore.Services.Config.SettingsRepository
{
  /// <summary>
  /// Setting model that is used when the key needs to be converted MS Format 
  /// </summary>
  internal sealed class ConfiguredSettingModelCopy : IConfigurableSettingModel
  {
    /// <summary>
    /// Key in MS Format
    /// </summary>
    public string Key { get; init; }
    
    /// <summary>
    /// Type
    /// </summary>
    public Type SettingType { get; init; }

    internal static ConfiguredSettingModelCopy FromSetting(IConfigurableSettingModel i_Model)
    {
      return new ConfiguredSettingModelCopy()
      {
        SettingType = i_Model.SettingType,
        Key = ConfigurationKeyConverter.ConvertKeyToMSFormat(i_Model.Key)
      };
    }
  }
}