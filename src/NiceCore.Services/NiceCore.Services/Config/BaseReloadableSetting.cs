using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using NiceCore.Base;

namespace NiceCore.Services.Config
{
  /// <summary>
  /// Base Reloadable Setting
  /// </summary>
  /// <typeparam name="TValue"></typeparam>
  /// <typeparam name="TListener"></typeparam>
  public abstract class BaseReloadableSetting<TValue, TListener> : BaseDisposable
  {
    /// <summary>
    /// Reload Func
    /// </summary>
    protected readonly Func<TValue> m_ReloadFunc;
    
    /// <summary>
    /// Listeners
    /// </summary>
    protected readonly List<TListener> m_Listeners = new();
    
    /// <summary>
    /// Value
    /// </summary>
    protected TValue m_Value;
    
    /// <summary>
    /// Key of the setting
    /// </summary>
    public string Key { get; }

    /// <summary>
    /// Context of this settings instance
    /// </summary>
    public string Context { get; }
    
    /// <summary>
    /// logger
    /// </summary>
    public ILogger Logger { get; }

    /// <summary>
    /// Base Reloadable Setting
    /// </summary>
    /// <param name="i_ReloadFunc"></param>
    /// <param name="i_Key"></param>
    /// <param name="i_Context"></param>
    /// <param name="i_Logger"></param>
    protected BaseReloadableSetting(Func<TValue> i_ReloadFunc, string i_Key, string i_Context, ILogger i_Logger)
    {
      m_ReloadFunc = i_ReloadFunc;
      Key = i_Key;
      Context = i_Context;
      Logger = i_Logger;
    }

    /// <summary>
    /// Create Args
    /// </summary>
    /// <returns></returns>
    protected SettingReloadedEventArgs<TValue> CreateArgs()
    {
      return new SettingReloadedEventArgs<TValue>()
      {
        Context = Context,
        Key = Key,
        Value = m_Value
      };
    }

    /// <summary>
    /// Dispose, clear event
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    protected override void Dispose(bool i_IsDisposing)
    {
      m_Listeners.Clear();
      base.Dispose(i_IsDisposing);
    }
  }
}
