﻿using Microsoft.Extensions.Configuration;

namespace NiceCore.Services.Config
{
  /// <summary>
  /// Definition for a single source of configuration
  /// </summary>
  public interface INcConfigurationSource
  {
    /// <summary>
    /// Priority of this source, determines in what order sources are read
    /// </summary>
    int Priority { get; }

    /// <summary>
    /// Applies this source to the configuration builder
    /// </summary>
    /// <param name="t_Builder"></param>
    void Apply(ConfigurationBuilder t_Builder);
  }
}
