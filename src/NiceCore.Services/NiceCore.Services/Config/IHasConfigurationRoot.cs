using Microsoft.Extensions.Configuration;

namespace NiceCore.Services.Config
{
  /// <summary>
  /// Interface for INcConfiguration Impls that are based on IConfigurationRoot
  /// </summary>
  public interface IHasConfigurationRoot
  {
    /// <summary>
    /// returns the root of the configuration
    /// </summary>
    /// <returns></returns>
    IConfigurationRoot GetConfigurationRoot();
  }
}