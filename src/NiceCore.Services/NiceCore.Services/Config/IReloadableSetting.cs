﻿using System;
using System.Threading.Tasks;

namespace NiceCore.Services.Config
{
  /// <summary>
  /// Interface for settings objects that refresh themselves (if the source this was configured by can be refreshed)
  /// </summary>
  /// <typeparam name="TValue"></typeparam>
  public interface IReloadableSetting<TValue> : IDisposable
  {
    /// <summary>
    /// Key this setting is retrieved from
    /// </summary>
    string Key { get; }

    /// <summary>
    /// Context this setting is retrieved under. This could be for example the Service Instance that read the setting. Can be null
    /// </summary>
    string Context { get; }
  }
}
