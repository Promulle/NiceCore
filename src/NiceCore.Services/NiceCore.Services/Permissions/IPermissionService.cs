﻿using NiceCore.Permissions;
using System.Threading.Tasks;

namespace NiceCore.Services.Permissions
{
  /// <summary>
  /// Interface that has to be implemented by the app that uses nicecore.
  /// This service will be called by IPermissionAdapter
  /// </summary>
  public interface IPermissionService
  {
    /// <summary>
    /// Checks if the user with i_Userid has the permission for i_PermissionKey
    /// </summary>
    /// <param name="i_UserId"></param>
    /// <param name="i_PermissionKey"></param>
    /// <param name="i_Context">Context supplying additional info to permission service</param>
    /// <returns></returns>
    Task<bool> IsAllowed(object i_UserId, IPermissionKey i_PermissionKey, IPermissionServiceContext i_Context);
  }
}
