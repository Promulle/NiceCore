﻿using NiceCore.Permissions;
using System.Threading.Tasks;

namespace NiceCore.Services.Permissions
{
  /// <summary>
  /// Adapter to external permission systems defining interactions when checking if a user is authorized
  /// </summary>
  public interface IPermissionAdapter
  {
    /// <summary>
    /// Checks that the user that can be resolved from i_UseCase is allowed to execute the usecase protected by the permission key i_Key
    /// </summary>
    /// <param name="i_UseCase">UseCase that should be checked</param>
    /// <param name="i_Key">Key defining the permission for the check</param>
    /// <returns></returns>
    Task<bool> IsAllowed(IPermissionUseCase i_UseCase, IPermissionKey i_Key);
  }
}
