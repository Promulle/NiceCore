﻿using System;

namespace NiceCore.Services.Permissions
{
  /// <summary>
  /// Interface for context objects used in permission service calls
  /// </summary>
  public interface IPermissionServiceContext : IDisposable
  {
  }
}
