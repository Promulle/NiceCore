﻿using NiceCore.Permissions;
using System;
using System.Threading.Tasks;

namespace NiceCore.Services.Permissions
{
  /// <summary>
  /// Base impl that can be inherited from for easier validation
  /// </summary>
  public abstract class BasePermissionUseCaseValidator<T> : IPermissionUseCaseValidator where T : IPermissionUseCase
  {
    /// <summary>
    /// Type to use
    /// </summary>
    public Type UseCaseType { get; } = typeof(T);

    /// <summary>
    /// Validation
    /// </summary>
    /// <param name="i_UseCase"></param>
    /// <returns></returns>
    public Task<bool> Validate(IPermissionUseCase i_UseCase)
    {
      if (i_UseCase is T casted)
      {
        return ValidateInternal(casted);
      }
      return Task.FromResult(true); //return true, since there are multiple validators and if really a wrong validator is used, it should be transparent to the rest of the process
    }

    /// <summary>
    /// Actually perform the validation of the use case here
    /// </summary>
    /// <param name="i_UseCase"></param>
    /// <returns></returns>
    protected abstract Task<bool> ValidateInternal(T i_UseCase);
  }
}
