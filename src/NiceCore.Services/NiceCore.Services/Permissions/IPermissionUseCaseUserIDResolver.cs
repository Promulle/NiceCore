﻿using NiceCore.Permissions;
using System;
using System.Threading.Tasks;

namespace NiceCore.Services.Permissions
{
  /// <summary>
  /// Interface that enables support of custom use cases/use cases where getting the userid is not trivial and needs to be implemented by the app 
  /// </summary>
  public interface IPermissionUseCaseUserIDResolver
  {
    /// <summary>
    /// Type of use case this resolver should be used for
    /// </summary>
    Type UseCaseType { get; }

    /// <summary>
    /// Gets the user id
    /// </summary>
    /// <param name="i_UseCase"></param>
    /// <returns></returns>
    Task<object> GetUserID(IPermissionUseCase i_UseCase);
  }
}
