﻿using NiceCore.Permissions;
using System;
using System.Threading.Tasks;

namespace NiceCore.Services.Permissions
{
  /// <summary>
  /// Base impl for permission use case user id resolver
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public abstract class BasePermissionUseCaseUserIDResolver<T> : IPermissionUseCaseUserIDResolver where T : IPermissionUseCase
  {
    /// <summary>
    /// Use case type for this
    /// </summary>
    public Type UseCaseType { get; } = typeof(T);

    /// <summary>
    /// GetUserId
    /// </summary>
    /// <param name="i_UseCase"></param>
    /// <returns></returns>
    public Task<object> GetUserID(IPermissionUseCase i_UseCase)
    {
      if (i_UseCase is T casted)
      {
        return GetUserIDInternal(casted);
      }
      return Task.FromResult<object>(null);
    }

    /// <summary>
    /// Get User id interal
    /// </summary>
    /// <param name="i_UseCase"></param>
    /// <returns></returns>
    protected abstract Task<object> GetUserIDInternal(T i_UseCase);
  }
}
