﻿using NiceCore.Permissions;
using System;
using System.Threading.Tasks;

namespace NiceCore.Services.Permissions
{
  /// <summary>
  /// Interface for which all validators that can validate a certain kind of UseCase should be registered
  /// </summary>
  public interface IPermissionUseCaseValidator
  {
    /// <summary>
    /// The type of use case this validator should validate
    /// </summary>
    Type UseCaseType { get; }

    /// <summary>
    /// Validate the use case
    /// </summary>
    /// <param name="i_UseCase"></param>
    /// <returns></returns>
    Task<bool> Validate(IPermissionUseCase i_UseCase);
  }
}
