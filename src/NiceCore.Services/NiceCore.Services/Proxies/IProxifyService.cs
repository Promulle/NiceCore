﻿using Castle.DynamicProxy;

namespace NiceCore.Services.Proxies
{
  /// <summary>
  /// Service that proxies/unproxies objects
  /// </summary>
  public interface IProxifyService
  {
    /// <summary>
    /// Proxifies i_Instance with all interceptors given in i_Interceptors
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Instance"></param>
    /// <param name="i_Interceptors"></param>
    /// <returns></returns>
    T Proxify<T>(T i_Instance, params IInterceptor[] i_Interceptors) where T : class;

    /// <summary>
    /// UnProxify i_Instance if it is a proxy, otherwise just return i_Instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Instance"></param>
    /// <returns></returns>
    T UnProxify<T>(T i_Instance);

    /// <summary>
    /// Checks if i_Instance is a proxy or not
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Instance"></param>
    /// <returns></returns>
    bool IsProxy<T>(T i_Instance);
  }
}
