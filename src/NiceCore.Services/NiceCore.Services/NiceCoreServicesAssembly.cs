﻿namespace NiceCore.Services
{
  /// <summary>
  /// Constants for this assembly
  /// </summary>
  internal static class NiceCoreServicesAssembly
  {
    /// <summary>
    /// Name of assembly
    /// </summary>
    public const string ASSEMBLY_NAME = "NiceCore.Services";
  }
}
