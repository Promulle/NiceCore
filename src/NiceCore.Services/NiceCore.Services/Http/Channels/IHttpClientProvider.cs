using System.Net.Http;

namespace NiceCore.Services.Http.Channels
{
  /// <summary>
  /// Service that creates http clients
  /// </summary>
  public interface IHttpClientProvider
  {
    /// <summary>
    /// Create Client
    /// </summary>
    /// <returns></returns>
    HttpClient CreateClient();
  }
}
