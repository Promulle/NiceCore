﻿using NiceCore.Base;
using NiceCore.Logging;
using NiceCore.Serialization;
using NiceCore.Services.Http.Converters;
using NiceCore.Services.Http.Exceptions;
using Polly;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Diagnostics;
using NiceCore.Logging.Extensions;

namespace NiceCore.Services.Http.Channels
{
  /// <summary>
  /// Base impl for INcHttpChannel
  /// </summary>
  public abstract class BaseNcHttpChannel : BaseDisposable, INcHttpChannel
  {
    /// <summary>
    /// Name for default handlers to be retrieved under
    /// </summary>
    protected const string DEFAULT_HANDLER_NAME = "default";
    private readonly SemaphoreSlim m_LockObject = new(1, 1);
    private HttpClient m_Client;
    private Func<HttpClient> m_CreateClientFunc;
    private bool m_Reconfigure = false;

    /// <summary>
    /// Service for HttpClientHandler instances
    /// </summary>
    public IHttpClientHandlerService HttpClientHandlerService { get; set; }

    /// <summary>
    /// Service for retrieving converters used for converting payloads and responses
    /// </summary>
    public IConverterService ConverterService { get; set; }
    
    /// <summary>
    /// Provider for HTTPClient Provider
    /// </summary>
    public IHttpClientProvider HttpClientProvider { get; set; }

    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<BaseNcHttpChannel> Logger { get; set; } = DiagnosticsCollector.Instance.GetLogger<BaseNcHttpChannel>();

    /// <summary>
    /// Ctor setting client factory
    /// </summary>
    /// <param name="i_ClientFactory"></param>
    public BaseNcHttpChannel(Func<HttpClient> i_ClientFactory)
    {
      m_CreateClientFunc = i_ClientFactory;
    }
    
    /// <summary>
    /// Default ctor
    /// </summary>
    public BaseNcHttpChannel()
    {
      
    }
    
    /// <summary>
    /// Sends a request of method i_method to the endpoint specified by i_Endpoint, using the content type i_ContentType and i_Payload as content.
    /// Can be authenticated by the auth header returned by i_AuthenticationHeaderFunc
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <typeparam name="TPayload"></typeparam>
    /// <param name="i_Method"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_ContentType"></param>
    /// <param name="i_Payload"></param>
    /// <param name="i_HeaderFunc">Function that can be used to add additional headers for this request. may be null.</param>
    /// <param name="i_AuthenticationHeaderFunc">Function that provides the authentication for the request. may be null.</param>
    /// <returns></returns>
    public Task<TReturn> Send<TReturn, TPayload>(HttpMethod i_Method, string i_Endpoint, string i_ContentType, TPayload i_Payload, Func<HttpRequestHeaders, Task> i_HeaderFunc, Func<Task<AuthenticationHeaderValue>> i_AuthenticationHeaderFunc)
    {
      return CreateRequestAndSendInternal<TReturn, TPayload>(i_Method, i_Endpoint, i_ContentType, i_Payload, i_HeaderFunc, i_AuthenticationHeaderFunc, HttpCompletionOption.ResponseContentRead);
    }

    /// <summary>
    /// Sends a request of method i_method to the endpoint specified by i_Endpoint without content
    /// Can be authenticated by the auth header returned by i_AuthenticationHeaderFunc
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <param name="i_Method"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_HeaderFunc">Function that can be used to add additional headers for this request. may be null.</param>
    /// <param name="i_AuthenticationHeaderFunc">Function that provides the authentication for the request. may be null.</param>
    /// <returns></returns>
    public Task<TReturn> Send<TReturn>(HttpMethod i_Method, string i_Endpoint, Func<HttpRequestHeaders, Task> i_HeaderFunc, Func<Task<AuthenticationHeaderValue>> i_AuthenticationHeaderFunc)
    {
      return CreateRequestAndSendInternal<TReturn, object>(i_Method, i_Endpoint, null, null, i_HeaderFunc, i_AuthenticationHeaderFunc, HttpCompletionOption.ResponseContentRead);
    }

    /// <summary>
    /// Sends a request of method i_Method to i_Endpoint. Without authentication and content
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <param name="i_Method"></param>
    /// <param name="i_Endpoint"></param>
    /// <returns></returns>
    public Task<TReturn> Send<TReturn>(HttpMethod i_Method, string i_Endpoint)
    {
      return CreateRequestAndSendInternal<TReturn, object>(i_Method, i_Endpoint, null, null, null, null, HttpCompletionOption.ResponseContentRead);
    }

    /// <summary>
    /// Sends a request to the endpoint using i_Method without converting the result
    /// </summary>
    /// <param name="i_Method"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_Option"></param>
    /// <returns></returns>
    public Task<HttpContent> SendRaw(HttpMethod i_Method, string i_Endpoint, HttpCompletionOption i_Option)
    {
      return SendRaw<object>(i_Method, i_Endpoint, null, null, i_Option);
    }

    /// <summary>
    /// Sends a request to the endpoint using i_Method without converting the result
    /// </summary>
    /// <param name="i_Method"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_ContentType"></param>
    /// <param name="i_Payload"></param>
    /// <param name="i_Option"></param>
    /// <returns></returns>
    public async Task<HttpContent> SendRaw<TPayload>(HttpMethod i_Method, string i_Endpoint, string i_ContentType, TPayload i_Payload, HttpCompletionOption i_Option)
    {
      var res = await CreateRequestAndSendInternalRaw<object>(i_Method, i_Endpoint, i_ContentType, i_Payload, null, null, i_Option).ConfigureAwait(false);
      return res.Content;
    }
    
    /// <summary>
    /// This function is called when a send was successfull and just before EnsureSuccess makes sure that the
    /// status code was OK.
    /// </summary>
    /// <param name="i_ResponseMessage"></param>
    /// <returns></returns>
    protected virtual Task OnResponseReceived(HttpResponseMessage i_ResponseMessage)
    {
      return Task.CompletedTask;
    }

    /// <summary>
    /// This function is called just before a message is sent
    /// </summary>
    /// <param name="i_RequestMessage"></param>
    /// <returns></returns>
    protected virtual Task OnBeforeRequestSent(HttpRequestMessage i_RequestMessage)
    {
      return Task.CompletedTask;
    }

    private async Task<TReturn> CreateRequestAndSendInternal<TReturn, TPayload>(HttpMethod i_Method, string i_Endpoint, string i_ContentType, TPayload i_Payload, Func<HttpRequestHeaders, Task> i_HeaderFunc, Func<Task<AuthenticationHeaderValue>> i_AuthenticationHeaderFunc, HttpCompletionOption i_Options)
    {
      var creationFunc = GetCorrectRequestMessageFunction(i_Method, i_Endpoint, i_ContentType, i_Payload, i_HeaderFunc, i_AuthenticationHeaderFunc);
      using (var requestMessage = await InvokeCreateRequestMessage(creationFunc).ConfigureAwait(false))
      {
        return await SendInternal<TReturn>(requestMessage, i_Options).ConfigureAwait(false);
      }
    }

    private async Task<HttpResponseMessage> CreateRequestAndSendInternalRaw<TPayload>(HttpMethod i_Method, string i_Endpoint, string i_ContentType, TPayload i_Payload, Func<HttpRequestHeaders, Task> i_HeaderFunc, Func<Task<AuthenticationHeaderValue>> i_AuthenticationHeaderFunc, HttpCompletionOption i_Options)
    {
      var creationFunc = GetCorrectRequestMessageFunction(i_Method, i_Endpoint, i_ContentType, i_Payload, i_HeaderFunc, i_AuthenticationHeaderFunc);
      using (var requestMessage = await InvokeCreateRequestMessage(creationFunc).ConfigureAwait(false))
      {
        return await SendInternalRaw(requestMessage, i_Options).ConfigureAwait(false);
      }
    }

    private Func<Task<HttpRequestMessage>> GetCorrectRequestMessageFunction<TPayload>(HttpMethod i_Method, string i_Endpoint, string i_ContentType, TPayload i_Payload, Func<HttpRequestHeaders, Task> i_HeaderFunc, Func<Task<AuthenticationHeaderValue>> i_AuthenticationHeaderFunc)
    {
      if (string.IsNullOrEmpty(i_ContentType) || i_Payload == null)
      {
        Logger.Information($"ContentType {i_ContentType} or payload {i_Payload} not provided. Sending request without body.");
        return () => CreateRequestMessage(i_Method, i_Endpoint, i_HeaderFunc, i_AuthenticationHeaderFunc);
      }
      return () =>
      {
        Logger.Information($"Creating request with ContentType {i_ContentType}.");
        var payloadConverter = ConverterService.GetPayloadConverter(i_ContentType);
        return CreateRequestMessageWithBody(i_Method, i_Endpoint, i_ContentType, i_Payload, payloadConverter, i_HeaderFunc, i_AuthenticationHeaderFunc);
      };
    }

    private async Task<HttpResponseMessage> SendInternalRaw(HttpRequestMessage i_Message, HttpCompletionOption i_Options)
    {
      await EnsureClientExists().ConfigureAwait(false);
      await OnBeforeRequestSent(i_Message).ConfigureAwait(false);
      var res = await m_Client.SendAsync(i_Message, i_Options).ConfigureAwait(false);
      await OnResponseReceived(res).ConfigureAwait(false);
      await EnsureSuccess(res).ConfigureAwait(false);
      if (res.Content?.Headers.ContentType?.MediaType != null)
      {
        Logger.Information($"received response with content of type {res.Content.Headers.ContentType.MediaType}");
        return res;
      }
      return default;
    }

    private async Task<TReturn> SendInternal<TReturn>(HttpRequestMessage i_Message, HttpCompletionOption i_Options)
    {
      using (var res = await SendInternalRaw(i_Message, i_Options).ConfigureAwait(false))
      {
        return await ConvertResponse<TReturn>(res).ConfigureAwait(false);
      }
    }

#pragma warning disable RCS1229 // I don't know why I should await here instead of returning the task
    private Task<HttpRequestMessage> InvokeCreateRequestMessage(Func<Task<HttpRequestMessage>> i_CreationFunc)
#pragma warning restore RCS1229 // I don't know why I should await here instead of returning the task
    {
      try
      {
        return i_CreationFunc.Invoke();
      }
      catch (Exception ex)
      {
        Logger.Error(ex, "Could not create Request message.");
        throw;
      }
    }

#pragma warning disable RCS1229 // I don't know why I should await here instead of returning the task
    private Task<TReturn> ConvertResponse<TReturn>(HttpResponseMessage i_ResponseMessage)
#pragma warning restore RCS1229 // I don't know why I should await here instead of returning the task
    {
      var converter = ConverterService.GetResponseConverter(i_ResponseMessage.Content.Headers.ContentType.MediaType);
      try
      {
        return converter.ConvertValue<TReturn>(i_ResponseMessage);
      }
      catch (Exception ex)
      {
        Logger.Error(ex, $"Could not convert response to content of type {i_ResponseMessage.Content.Headers.ContentType.MediaType}");
      }
      return Task.FromResult<TReturn>(default);
    }

    private async Task EnsureSuccess(HttpResponseMessage i_Message)
    {
      if (i_Message.StatusCode != HttpStatusCode.OK)
      {

        var errorText = await GetErrorText(i_Message).ConfigureAwait(false);
        var msg = $"Request for {i_Message.RequestMessage.RequestUri} returned status code {i_Message.StatusCode}.";
        Logger.LogError(msg);
        throw new ChannelRequestFailedException(msg, i_Message.StatusCode, errorText);
      }
    }

    private async Task<string> GetErrorText(HttpResponseMessage i_Message)
    {
      try
      {
        return await i_Message.Content.ReadAsStringAsync().ConfigureAwait(false);
      }
      catch
      {
        Logger.Error("Could not get result error for failed response.");
      }
      return null;
    }

    private static async Task<HttpRequestMessage> CreateRequestMessage(HttpMethod i_Method, string i_Endpoint, Func<HttpRequestHeaders, Task> i_HeaderFunc, Func<Task<AuthenticationHeaderValue>> i_AuthenticationHeaderFunc)
    {
      var request = new HttpRequestMessage(i_Method, i_Endpoint);
      if (i_HeaderFunc != null)
      {
        await i_HeaderFunc.Invoke(request.Headers).ConfigureAwait(false);
      }
      if (i_AuthenticationHeaderFunc != null)
      {
        var authHeader = await i_AuthenticationHeaderFunc.Invoke().ConfigureAwait(false);
        if (authHeader != null)
        {
          request.Headers.Authorization = authHeader;
        }
      }
      return request;
    }

    private static async Task<HttpRequestMessage> CreateRequestMessageWithBody<T>(
        HttpMethod i_Method,
        string i_Endpoint,
        string i_ContentType,
        T i_Value,
        IPayloadConverter i_PayloadConverter,
        Func<HttpRequestHeaders, Task> i_HeaderFunc,
        Func<Task<AuthenticationHeaderValue>> i_AuthenticationHeaderFunc)
    {
      var req = await CreateRequestMessage(i_Method, i_Endpoint, i_HeaderFunc, i_AuthenticationHeaderFunc).ConfigureAwait(false);
      req.Content = await i_PayloadConverter.ToContent(i_Value).ConfigureAwait(false);
      req.Content.Headers.ContentType = new MediaTypeHeaderValue(i_ContentType);
      return req;
    }

    /// <summary>
    /// Marks this channel for reconfiguration on next call
    /// </summary>
    public void MarkForReconfigure()
    {
      m_Reconfigure = true;
      }

    private async Task EnsureClientExists()
    {
      await m_LockObject.WaitAsync().ConfigureAwait(false);
      try
      {
        if (m_Reconfigure)
        {
          m_Client?.Dispose();
          m_Client = null;
          m_Reconfigure = false;
        }
        if (m_Client == null)
        {
          m_Client = m_CreateClientFunc?.Invoke() ?? HttpClientProvider?.CreateClient() ?? new HttpClient(GetHandler(), false);
          await ConfigureClient(m_Client).ConfigureAwait(false);
        }
      }
      finally
      {
        m_LockObject.Release();
      }
    }

    /// <summary>
    /// returns the func used to retrieve the wanted handler
    /// </summary>
    /// <returns></returns>
    protected virtual HttpClientHandler GetHandler()
    {
      return HttpClientHandlerService.GetHandler(DEFAULT_HANDLER_NAME, true, null);
    }

    /// <summary>
    /// Method that has to be overriden, defines additional configuration on the used client for this channel
    /// </summary>
    /// <param name="t_Client"></param>
    protected abstract Task ConfigureClient(HttpClient t_Client);

    /// <summary>
    /// Dispose the client we created for this channel
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    protected override void Dispose(bool i_IsDisposing)
    {
      m_Client?.Dispose(); //should not create any problems since the handler is shared but not owned by client
      base.Dispose(i_IsDisposing);
    }

    /// <summary>
    /// Consume a stream from the endpoint given.
    /// </summary>
    /// <typeparam name="TDTO">Type sent messages must conform to</typeparam>
    /// <param name="i_Endpoint">Endpoint to get stream from</param>
    /// <param name="i_ConsumerFunction">function that is run for every dto that is received</param>
    /// <param name="i_Policy">Retry/Failsafe policy to use, may be null</param>
    /// <param name="i_JSONOptions">options that should be used when deserializing the message to an object of Type TDTO</param>
    /// <returns></returns>
    public Task ConsumeStream<TDTO>(string i_Endpoint, Func<TDTO, Task> i_ConsumerFunction, IAsyncPolicy i_Policy, JsonSerializerOptions i_JSONOptions)
    {
      if (i_Policy != null)
      {
        return i_Policy.ExecuteAsync(() => ConsumeInternal(i_Endpoint, i_ConsumerFunction, i_JSONOptions));
      }
      return ConsumeInternal(i_Endpoint, i_ConsumerFunction, i_JSONOptions);
    }

    private async Task ConsumeInternal<TDTO>(string i_Endpoint, Func<TDTO, Task> i_ConsumerFunction, JsonSerializerOptions i_JSONOptions)
    {
      await EnsureClientExists().ConfigureAwait(false);
      using (var streamReader = new StreamReader(await m_Client.GetStreamAsync(i_Endpoint).ConfigureAwait(false)))
      {
        Logger.Information($"Stream to endpoint {i_Endpoint} opened.");
        while (!streamReader.EndOfStream)
        {
          var dtoMessage = await streamReader.ReadLineAsync().ConfigureAwait(false);
          var dto = i_JSONOptions != null ? JSONSerialization.DeserializeWithOptions<TDTO>(dtoMessage, i_JSONOptions) : JSONSerialization.Deserialize<TDTO>(dtoMessage);
          await i_ConsumerFunction.Invoke(dto).ConfigureAwait(false);
        }
        Logger.Information($"Stream to endpoint {i_Endpoint} will be closed.");
      }
    }
  }
}
