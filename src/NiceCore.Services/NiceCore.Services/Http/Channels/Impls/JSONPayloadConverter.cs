﻿using NiceCore.Base.Constants;
using NiceCore.Serialization;
using NiceCore.ServiceLocation;
using NiceCore.Services.Http.Converters;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace NiceCore.Services.Http.Channels.Impls
{
  /// <summary>
  /// IPayloadConverter impl for JSON
  /// </summary>
  [Register(InterfaceType = typeof(IPayloadConverter))]
  public class JSONPayloadConverter : IPayloadConverter
  {
    /// <summary>
    /// Content Type app/json
    /// </summary>
    public string ContentType => ContentTypes.APP_JSON;

    /// <summary>
    /// To StringContent
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Payload"></param>
    /// <returns></returns>
    public Task<HttpContent> ToContent<T>(T i_Payload)
    {
      return Task.FromResult(new StringContent(JSONSerialization.Serialize(i_Payload)) as HttpContent);
    }

    /// <summary>
    /// To StringContent not generic
    /// </summary>
    /// <param name="i_Payload"></param>
    /// <param name="i_Type"></param>
    /// <returns></returns>
    public Task<HttpContent> ToContent(object i_Payload, Type i_Type)
    {
      return Task.FromResult(new StringContent(JSONSerialization.Serialize(i_Payload, i_Type)) as HttpContent);
    }
  }
}
