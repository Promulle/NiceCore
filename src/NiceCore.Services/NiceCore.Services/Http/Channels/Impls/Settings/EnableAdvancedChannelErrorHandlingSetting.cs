using System;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Services.Http.Channels.Impls.Settings
{
  /// <summary>
  /// Enable Advanced Channel Error Handling Setting
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class EnableAdvancedChannelErrorHandlingSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// KEY
    /// </summary>
    public const string KEY = "NiceCore.Channels.EnableAdvancedErrorHandling";

    /// <summary>
    /// Key
    /// </summary>
    public string Key { get; } = KEY;
    
    /// <summary>
    /// Setting Type
    /// </summary>
    public Type SettingType { get; } = typeof(bool);
  }
}
