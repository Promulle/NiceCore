﻿using NiceCore.Base.Constants;
using NiceCore.ServiceLocation;
using NiceCore.Services.Http.Converters;
using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Diagnostics;
using NiceCore.Logging.Extensions;
using NiceCore.Services.Config;
using NiceCore.Services.Http.Channels.Impls.Settings;

namespace NiceCore.Services.Http.Channels.Impls
{
  /// <summary>
  /// IResponseConverter impl dealing with app/json
  /// </summary>
  [Register(InterfaceType = typeof(IResponseConverter))]
  public class JSONResponseConverter : IResponseConverter
  {
    /// <summary>
    /// APP/Json
    /// </summary>
    public string ContentType => ContentTypes.APP_JSON;
    
    /// <summary>
    /// Configuration
    /// </summary>
    public INcConfiguration Configuration { get; set; }

    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<JSONResponseConverter> Logger { get; set; } = DiagnosticsCollector.Instance.GetLogger<JSONResponseConverter>();

    /// <summary>
    /// Converts the content of i_Response to an instance of i_Type
    /// </summary>
    /// <param name="i_Response"></param>
    /// <param name="i_Type"></param>
    /// <returns></returns>
    public Task<object> ConvertObjectValue(HttpResponseMessage i_Response, Type i_Type)
    {
      return Convert<object>(i_Response, i_Type);
    }

    /// <summary>
    /// Converts the content of i_Response to an instance of T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Response"></param>
    /// <returns></returns>
    public Task<T> ConvertValue<T>(HttpResponseMessage i_Response)
    {
      return Convert<T>(i_Response, typeof(T));
    }

    private async Task<T> Convert<T>(HttpResponseMessage i_Response, Type i_Type)
    {
      var advancedErrorHandling = Configuration.GetValue(EnableAdvancedChannelErrorHandlingSetting.KEY, false);
      Logger.Debug($"JSON Response Converter - AdvancedErrorHandling with Config Key {EnableAdvancedChannelErrorHandlingSetting.KEY} is enabled: {advancedErrorHandling}");
      if (advancedErrorHandling)
      {
        await i_Response.Content.LoadIntoBufferAsync().ConfigureAwait(false);  
      }
      try
      {
        var res= await i_Response.Content.ReadFromJsonAsync(i_Type).ConfigureAwait(false);
        if (res is T castedResult)
        {
          return castedResult;
        }

        throw new InvalidOperationException("Types of serialized and expecation in deserialized Object did not match");
      }
      catch (JsonException ex)
      {
        if (advancedErrorHandling)
        {
          var strValue = await i_Response.Content.ReadAsStringAsync().ConfigureAwait(false);
          Logger.Error(ex, $"Could not Convert JSON Response from string value {strValue}");
        }
        throw;
      }
    }
  }
}
