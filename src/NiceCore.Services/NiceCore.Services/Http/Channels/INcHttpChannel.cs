﻿using Polly;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;

namespace NiceCore.Services.Http.Channels
{
  /// <summary>
  /// Channel for interacting with endpoints
  /// </summary>
  public interface INcHttpChannel : IDisposable
  {
    /// <summary>
    /// Sends a request of method i_method to the endpoint specified by i_Endpoint, using the content type i_ContentType and i_Payload as content.
    /// Can be authenticated by the auth header returned by i_AuthenticationHeaderFunc
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <typeparam name="TPayload"></typeparam>
    /// <param name="i_Method"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_ContentType"></param>
    /// <param name="i_Payload"></param>
    /// <param name="i_HeaderFunc">Function that can be used to add additional headers for this request. may be null.</param>
    /// <param name="i_AuthenticationHeaderFunc">Function that provides the authentication for the request. may be null.</param>
    /// <returns></returns>
    Task<TReturn> Send<TReturn, TPayload>(HttpMethod i_Method, string i_Endpoint, string i_ContentType, TPayload i_Payload, Func<HttpRequestHeaders, Task> i_HeaderFunc, Func<Task<AuthenticationHeaderValue>> i_AuthenticationHeaderFunc);

    /// <summary>
    /// Sends a request of method i_method to the endpoint specified by i_Endpoint without content
    /// Can be authenticated by the auth header returned by i_AuthenticationHeaderFunc
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <param name="i_Method"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_HeaderFunc">Function that can be used to add additional headers for this request. may be null.</param>
    /// <param name="i_AuthenticationHeaderFunc">Function that provides the authentication for the request. may be null.</param>
    /// <returns></returns>
    Task<TReturn> Send<TReturn>(HttpMethod i_Method, string i_Endpoint, Func<HttpRequestHeaders, Task> i_HeaderFunc, Func<Task<AuthenticationHeaderValue>> i_AuthenticationHeaderFunc);

    /// <summary>
    /// Sends a request of method i_Method to i_Endpoint. Without authentication and content
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <param name="i_Method"></param>
    /// <param name="i_Endpoint"></param>
    /// <returns></returns>
    Task<TReturn> Send<TReturn>(HttpMethod i_Method, string i_Endpoint);

    /// <summary>
    /// Consume a stream from the endpoint given.
    /// </summary>
    /// <typeparam name="TDTO">Type sent messages must conform to</typeparam>
    /// <param name="i_Endpoint">Endpoint to get stream from</param>
    /// <param name="i_ConsumerFunction">function that is run for every dto that is received</param>
    /// <param name="i_Policy">Retry/Failsafe policy to use, may be null</param>
    /// <param name="i_JSONOptions">options that should be used when deserializing the message to an object of Type TDTO</param>
    /// <returns></returns>
    Task ConsumeStream<TDTO>(string i_Endpoint, Func<TDTO, Task> i_ConsumerFunction, IAsyncPolicy i_Policy, JsonSerializerOptions i_JSONOptions);

    /// <summary>
    /// Sends a request to the endpoint using i_Method without converting the result
    /// </summary>
    /// <param name="i_Method"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_Option"></param>
    /// <returns></returns>
    Task<HttpContent> SendRaw(HttpMethod i_Method, string i_Endpoint, HttpCompletionOption i_Option);

    /// <summary>
    /// Sends a request to the endpoint using i_Method without converting the result
    /// </summary>
    /// <typeparam name="TPayload">Type of payload used</typeparam>
    /// <param name="i_Method"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_ContentType"></param>
    /// <param name="i_Payload"></param>
    /// <param name="i_Option"></param>
    /// <returns></returns>
    Task<HttpContent> SendRaw<TPayload>(HttpMethod i_Method, string i_Endpoint, string i_ContentType, TPayload i_Payload, HttpCompletionOption i_Option);
    
    /// <summary>
    /// Force to call configure client again, careful if sending multiple requests at the same time
    /// </summary>
    /// <returns></returns>
    void MarkForReconfigure();
  }
}
