﻿using NiceCore.Services.Http.Channels.Endpoints;
using NiceCore.Services.Http.Channels.Endpoints.UrlExtenders;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace NiceCore.Services.Http.Channels.Extensions
{
  /// <summary>
  /// ncHttpChannel extensions for Delete Method
  /// </summary>
  public static class DeleteExtensions
  {
    /// <summary>
    /// Extension for channel that sends get request
    /// </summary>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <returns></returns>
    public static Task Delete(this INcHttpChannel t_Channel, string i_Endpoint)
    {
      return Delete(t_Channel, i_Endpoint, null, null);
    }

    /// <summary>
    /// Extension for channel that sends get request with the headers manipulated by i_HeaderFunc
    /// </summary>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_HeaderFunc"></param>
    /// <returns></returns>
    public static Task Delete(this INcHttpChannel t_Channel, string i_Endpoint, Func<HttpRequestHeaders, Task> i_HeaderFunc)
    {
      return Delete(t_Channel, i_Endpoint, i_HeaderFunc, null);
    }

    /// <summary>
    /// Extension for channel that sends get request with the authentication provided by i_AuthFunc
    /// </summary>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_AuthFunc"></param>
    /// <returns></returns>
    public static Task Delete(this INcHttpChannel t_Channel, string i_Endpoint, Func<Task<AuthenticationHeaderValue>> i_AuthFunc)
    {
      return Delete(t_Channel, i_Endpoint, null, i_AuthFunc);
    }

    /// <summary>
    /// Extension for channel that sends get request with the authentication provided by i_AuthFunc and manipluates the headers using i_HeaderFunc
    /// </summary>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_HeaderFunc"></param>
    /// <param name="i_AuthFunc"></param>
    /// <returns></returns>
    public static Task Delete(this INcHttpChannel t_Channel, string i_Endpoint, Func<HttpRequestHeaders, Task> i_HeaderFunc, Func<Task<AuthenticationHeaderValue>> i_AuthFunc)
    {
      return t_Channel.Send<object>(HttpMethod.Delete, i_Endpoint, i_HeaderFunc, i_AuthFunc);
    }

    /// <summary>
    /// Extension for channel that sends get request with the authentication provided by i_AuthFunc and manipluates the headers using i_HeaderFunc
    /// </summary>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <returns></returns>
    public static Task Delete(this INcHttpChannel t_Channel, IEndpointRequestInformation i_Endpoint)
    {
      return t_Channel.Send<object>(HttpMethod.Delete, i_Endpoint.Endpoint, i_Endpoint.HeaderFunc, i_Endpoint.AuthenticationHeaderFunc);
    }

    /// <summary>
    /// Extension for channel that sends get request with the authentication provided by i_AuthFunc and manipluates the headers using i_HeaderFunc
    /// </summary>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_Extender">Extender that adds the provided data to the endpoint</param>
    /// <returns></returns>
    public static Task Delete(this INcHttpChannel t_Channel, IEndpointRequestInformation i_Endpoint, IUrlExtender i_Extender)
    {
      return t_Channel.Send<object>(HttpMethod.Delete, i_Extender.Extend(i_Endpoint.Endpoint), i_Endpoint.HeaderFunc, i_Endpoint.AuthenticationHeaderFunc);
    }
  }
}
