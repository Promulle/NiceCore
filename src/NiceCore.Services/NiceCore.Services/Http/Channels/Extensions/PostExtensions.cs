﻿using NiceCore.Services.Http.Channels.Endpoints;
using NiceCore.Services.Http.Channels.Endpoints.UrlExtenders;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace NiceCore.Services.Http.Channels.Extensions
{
  /// <summary>
  /// Extensions for post
  /// </summary>
  public static class PostExtensions
  {
    /// <summary>
    /// Extension for channel that sends post request
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <typeparam name="TPayload"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_ContentType"></param>
    /// <param name="i_Payload"></param>
    /// <returns></returns>
    public static Task<TReturn> Post<TReturn, TPayload>(this INcHttpChannel t_Channel, string i_Endpoint, string i_ContentType, TPayload i_Payload)
    {
      return Post<TReturn, TPayload>(t_Channel, i_Endpoint, i_ContentType, i_Payload, null, null);
    }

    /// <summary>
    /// Extension for channel that sends post request
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <returns></returns>
    public static Task<TReturn> Post<TReturn>(this INcHttpChannel t_Channel, string i_Endpoint)
    {
      return Post<TReturn>(t_Channel, i_Endpoint, null, null);
    }

    /// <summary>
    /// Extension for channel that sends post request
    /// </summary>
    /// <typeparam name="TPayload"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_ContentType"></param>
    /// <param name="i_Payload"></param>
    /// <returns></returns>
    public static Task Post<TPayload>(this INcHttpChannel t_Channel, string i_Endpoint, string i_ContentType, TPayload i_Payload)
    {
      return Post(t_Channel, i_Endpoint, i_ContentType, i_Payload, null, null);
    }

    /// <summary>
    /// Extension for channel that sends post request with the headers manipulated by i_HeaderFunc
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <typeparam name="TPayload"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_ContentType"></param>
    /// <param name="i_Payload"></param>
    /// <param name="i_HeaderFunc"></param>
    /// <returns></returns>
    public static Task<TReturn> Post<TReturn, TPayload>(this INcHttpChannel t_Channel, string i_Endpoint, string i_ContentType, TPayload i_Payload, Func<HttpRequestHeaders, Task> i_HeaderFunc)
    {
      return Post<TReturn, TPayload>(t_Channel, i_Endpoint, i_ContentType, i_Payload, i_HeaderFunc, null);
    }

    /// <summary>
    /// Extension for channel that sends post request with the headers manipulated by i_HeaderFunc
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_HeaderFunc"></param>
    /// <returns></returns>
    public static Task<TReturn> Post<TReturn>(this INcHttpChannel t_Channel, string i_Endpoint, Func<HttpRequestHeaders, Task> i_HeaderFunc)
    {
      return Post<TReturn>(t_Channel, i_Endpoint, i_HeaderFunc, null);
    }

    /// <summary>
    /// Extension for channel that sends post request with the headers manipulated by i_HeaderFunc
    /// </summary>
    /// <typeparam name="TPayload"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_ContentType"></param>
    /// <param name="i_Payload"></param>
    /// <param name="i_HeaderFunc"></param>
    /// <returns></returns>
    public static Task Post<TPayload>(this INcHttpChannel t_Channel, string i_Endpoint, string i_ContentType, TPayload i_Payload, Func<HttpRequestHeaders, Task> i_HeaderFunc)
    {
      return Post(t_Channel, i_Endpoint, i_ContentType, i_Payload, i_HeaderFunc, null);
    }

    /// <summary>
    /// Extension for channel that sends post request with the authentication provided by i_AuthFunc
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <typeparam name="TPayload"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_ContentType"></param>
    /// <param name="i_Payload"></param>
    /// <param name="i_AuthFunc"></param>
    /// <returns></returns>
    public static Task<TReturn> Post<TReturn, TPayload>(this INcHttpChannel t_Channel, string i_Endpoint, string i_ContentType, TPayload i_Payload, Func<Task<AuthenticationHeaderValue>> i_AuthFunc)
    {
      return Post<TReturn, TPayload>(t_Channel, i_Endpoint, i_ContentType, i_Payload, null, i_AuthFunc);
    }

    /// <summary>
    /// Extension for channel that sends post request with the authentication provided by i_AuthFunc
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_AuthFunc"></param>
    /// <returns></returns>
    public static Task<TReturn> Post<TReturn>(this INcHttpChannel t_Channel, string i_Endpoint, Func<Task<AuthenticationHeaderValue>> i_AuthFunc)
    {
      return Post<TReturn>(t_Channel, i_Endpoint, null, i_AuthFunc);
    }

    /// <summary>
    /// Extension for channel that sends post request with the authentication provided by i_AuthFunc
    /// </summary>
    /// <typeparam name="TPayload"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_ContentType"></param>
    /// <param name="i_Payload"></param>
    /// <param name="i_AuthFunc"></param>
    /// <returns></returns>
    public static Task Post<TPayload>(this INcHttpChannel t_Channel, string i_Endpoint, string i_ContentType, TPayload i_Payload, Func<Task<AuthenticationHeaderValue>> i_AuthFunc)
    {
      return Post(t_Channel, i_Endpoint, i_ContentType, i_Payload, null, i_AuthFunc);
    }

    /// <summary>
    /// Extension for channel that sends post request with the authentication provided by i_AuthFunc and manipluates the headers using i_HeaderFunc
    /// </summary>
    /// <typeparam name="TReturn">Type of the returned object</typeparam>
    /// <typeparam name="TPayload">Type of the object to be sent</typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_ContentType"></param>
    /// <param name="i_Payload"></param>
    /// <param name="i_HeaderFunc"></param>
    /// <param name="i_AuthFunc"></param>
    /// <returns></returns>
    public static Task<TReturn> Post<TReturn, TPayload>(this INcHttpChannel t_Channel, string i_Endpoint, string i_ContentType, TPayload i_Payload, Func<HttpRequestHeaders, Task> i_HeaderFunc, Func<Task<AuthenticationHeaderValue>> i_AuthFunc)
    {
      return t_Channel.Send<TReturn, TPayload>(HttpMethod.Post, i_Endpoint, i_ContentType, i_Payload, i_HeaderFunc, i_AuthFunc);
    }

    /// <summary>
    /// Extension for channel that sends post request with the authentication provided by i_AuthFunc and manipluates the headers using i_HeaderFunc
    /// </summary>
    /// <typeparam name="TReturn">Type of the returned object</typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_HeaderFunc"></param>
    /// <param name="i_AuthFunc"></param>
    /// <returns></returns>
    public static Task<TReturn> Post<TReturn>(this INcHttpChannel t_Channel, string i_Endpoint, Func<HttpRequestHeaders, Task> i_HeaderFunc, Func<Task<AuthenticationHeaderValue>> i_AuthFunc)
    {
      return t_Channel.Send<TReturn, object>(HttpMethod.Post, i_Endpoint, null, null, i_HeaderFunc, i_AuthFunc);
    }

    /// <summary>
    /// Extension for channel that sends post request with the authentication provided by i_AuthFunc and manipluates the headers using i_HeaderFunc
    /// </summary>
    /// <typeparam name="TPayload"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_ContentType"></param>
    /// <param name="i_Payload"></param>
    /// <param name="i_HeaderFunc"></param>
    /// <param name="i_AuthFunc"></param>
    /// <returns></returns>
    public static Task Post<TPayload>(this INcHttpChannel t_Channel, string i_Endpoint, string i_ContentType, TPayload i_Payload, Func<HttpRequestHeaders, Task> i_HeaderFunc, Func<Task<AuthenticationHeaderValue>> i_AuthFunc)
    {
      return t_Channel.Send<object, TPayload>(HttpMethod.Post, i_Endpoint, i_ContentType, i_Payload, i_HeaderFunc, i_AuthFunc);
    }

    /// <summary>
    /// Extension for channel that sends post request with the authentication provided by i_AuthFunc and manipluates the headers using i_HeaderFunc
    /// </summary>
    /// <typeparam name="TPayload"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <returns></returns>
    public static Task Post<TPayload>(this INcHttpChannel t_Channel, IEndpointBodyRequestInformation<TPayload> i_Endpoint)
    {
      return t_Channel.Send<object, TPayload>(HttpMethod.Post, i_Endpoint.Endpoint, i_Endpoint.ContentType, i_Endpoint.Payload, i_Endpoint.HeaderFunc, i_Endpoint.AuthenticationHeaderFunc);
    }

    /// <summary>
    /// Extension for channel that sends post request with the authentication provided by i_AuthFunc and manipluates the headers using i_HeaderFunc
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <typeparam name="TPayload"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <returns></returns>
    public static Task Post<TReturn, TPayload>(this INcHttpChannel t_Channel, IEndpointReturningBodyRequestInformation<TReturn, TPayload> i_Endpoint)
    {
      return t_Channel.Send<TReturn, TPayload>(HttpMethod.Post, i_Endpoint.Endpoint, i_Endpoint.ContentType, i_Endpoint.Payload, i_Endpoint.HeaderFunc, i_Endpoint.AuthenticationHeaderFunc);
    }

    /// <summary>
    /// Extension for channel that sends post request with the authentication provided by i_AuthFunc and manipluates the headers using i_HeaderFunc
    /// </summary>
    /// <typeparam name="TPayload"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_Extender">Extender that extendes the endpoint to the default endpoint</param>
    /// <returns></returns>
    public static Task Post<TPayload>(this INcHttpChannel t_Channel, IEndpointBodyRequestInformation<TPayload> i_Endpoint, IUrlExtender i_Extender)
    {
      return t_Channel.Send<object, TPayload>(HttpMethod.Post, i_Extender.Extend(i_Endpoint.Endpoint), i_Endpoint.ContentType, i_Endpoint.Payload, i_Endpoint.HeaderFunc, i_Endpoint.AuthenticationHeaderFunc);
    }

    /// <summary>
    /// Extension for channel that sends post request with the authentication provided by i_AuthFunc and manipluates the headers using i_HeaderFunc
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <typeparam name="TPayload"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_Extender">Extender that extendes the endpoint to the default endpoint</param>
    /// <returns></returns>
    public static Task Post<TReturn, TPayload>(this INcHttpChannel t_Channel, IEndpointReturningBodyRequestInformation<TReturn, TPayload> i_Endpoint, IUrlExtender i_Extender)
    {
      return t_Channel.Send<TReturn, TPayload>(HttpMethod.Post, i_Extender.Extend(i_Endpoint.Endpoint), i_Endpoint.ContentType, i_Endpoint.Payload, i_Endpoint.HeaderFunc, i_Endpoint.AuthenticationHeaderFunc);
    }

    /// <summary>
    /// Post Raw
    /// </summary>
    /// <typeparam name="TPayload"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_ContentType"></param>
    /// <param name="i_Payload"></param>
    /// <param name="i_Option"></param>
    /// <returns></returns>
    public static Task<HttpContent> PostRaw<TPayload>(this INcHttpChannel t_Channel, string i_Endpoint, string i_ContentType, TPayload i_Payload, HttpCompletionOption i_Option)
    {
      return t_Channel.SendRaw(HttpMethod.Post, i_Endpoint, i_ContentType, i_Payload, i_Option);
    }
  }
}
