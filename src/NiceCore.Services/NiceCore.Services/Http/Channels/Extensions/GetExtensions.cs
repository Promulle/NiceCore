﻿using NiceCore.Services.Http.Channels.Endpoints;
using NiceCore.Services.Http.Channels.Endpoints.UrlExtenders;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace NiceCore.Services.Http.Channels.Extensions
{
  /// <summary>
  /// Extensions for INcHttpChannel for get methods
  /// </summary>
  public static class GetExtensions
  {
    /// <summary>
    /// Extension for channel that sends get request
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <returns></returns>
    public static Task<TReturn> Get<TReturn>(this INcHttpChannel t_Channel, string i_Endpoint)
    {
      return Get<TReturn>(t_Channel, i_Endpoint, null, null);
    }

    /// <summary>
    /// Extension for channel that sends get request with the headers manipulated by i_HeaderFunc
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_HeaderFunc"></param>
    /// <returns></returns>
    public static Task<TReturn> Get<TReturn>(this INcHttpChannel t_Channel, string i_Endpoint, Func<HttpRequestHeaders, Task> i_HeaderFunc)
    {
      return Get<TReturn>(t_Channel, i_Endpoint, i_HeaderFunc, null);
    }

    /// <summary>
    /// Extension for channel that sends get request with the authentication provided by i_AuthFunc
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_AuthFunc"></param>
    /// <returns></returns>
    public static Task<TReturn> Get<TReturn>(this INcHttpChannel t_Channel, string i_Endpoint, Func<Task<AuthenticationHeaderValue>> i_AuthFunc)
    {
      return Get<TReturn>(t_Channel, i_Endpoint, null, i_AuthFunc);
    }

    /// <summary>
    /// Extension for channel that sends get request with the authentication provided by i_AuthFunc and manipluates the headers using i_HeaderFunc
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_HeaderFunc"></param>
    /// <param name="i_AuthFunc"></param>
    /// <returns></returns>
    public static Task<TReturn> Get<TReturn>(this INcHttpChannel t_Channel, string i_Endpoint, Func<HttpRequestHeaders, Task> i_HeaderFunc, Func<Task<AuthenticationHeaderValue>> i_AuthFunc)
    {
      return t_Channel.Send<TReturn>(HttpMethod.Get, i_Endpoint, i_HeaderFunc, i_AuthFunc);
    }

    /// <summary>
    /// Extension for channel that sends get request for the defined endpoint information
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <returns></returns>
    public static Task<TReturn> Get<TReturn>(this INcHttpChannel t_Channel, IEndpointReturningRequestInformation<TReturn> i_Endpoint)
    {
      return t_Channel.Send<TReturn>(HttpMethod.Get, i_Endpoint.Endpoint, i_Endpoint.HeaderFunc, i_Endpoint.AuthenticationHeaderFunc);
    }

    /// <summary>
    /// Extension for channel that sends get request for the defined endpoint information
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_Extender">Extender that extendes the endpoint to the default endpoint</param>
    /// <returns></returns>
    public static Task<TReturn> Get<TReturn>(this INcHttpChannel t_Channel, IEndpointReturningRequestInformation<TReturn> i_Endpoint, IUrlExtender i_Extender)
    {
      return t_Channel.Send<TReturn>(HttpMethod.Get, i_Extender.Extend(i_Endpoint.Endpoint), i_Endpoint.HeaderFunc, i_Endpoint.AuthenticationHeaderFunc);
    }

    /// <summary>
    /// Execute a raw get request to the endpoint
    /// </summary>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_Option"></param>
    /// <returns></returns>
    public static Task<HttpContent> GetRaw(this INcHttpChannel t_Channel, string i_Endpoint, HttpCompletionOption i_Option)
    {
      return t_Channel.SendRaw(HttpMethod.Get, i_Endpoint, i_Option);
    }
  }
}
