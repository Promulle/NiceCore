﻿using NiceCore.Services.Http.Channels.Endpoints;
using NiceCore.Services.Http.Channels.Endpoints.UrlExtenders;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace NiceCore.Services.Http.Channels.Extensions
{
  /// <summary>
  /// Extensions for NcHttpChannel for Put methods
  /// </summary>
  public static class PutExtensions
  {
    /// <summary>
    /// Extension for channel that sends put request
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <typeparam name="TPayload"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_ContentType"></param>
    /// <param name="i_Payload"></param>
    /// <returns></returns>
    public static Task<TReturn> Put<TReturn, TPayload>(this INcHttpChannel t_Channel, string i_Endpoint, string i_ContentType, TPayload i_Payload)
    {
      return Put<TReturn, TPayload>(t_Channel, i_Endpoint, i_ContentType, i_Payload, null, null);
    }

    /// <summary>
    /// Extension for channel that sends put request
    /// </summary>
    /// <typeparam name="TPayload"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_ContentType"></param>
    /// <param name="i_Payload"></param>
    /// <returns></returns>
    public static Task Put<TPayload>(this INcHttpChannel t_Channel, string i_Endpoint, string i_ContentType, TPayload i_Payload)
    {
      return Put<object, TPayload>(t_Channel, i_Endpoint, i_ContentType, i_Payload, null, null);
    }

    /// <summary>
    /// Extension for channel that sends put request with the headers manipulated by i_HeaderFunc
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <typeparam name="TPayload"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_ContentType"></param>
    /// <param name="i_Payload"></param>
    /// <param name="i_HeaderFunc"></param>
    /// <returns></returns>
    public static Task<TReturn> Put<TReturn, TPayload>(this INcHttpChannel t_Channel, string i_Endpoint, string i_ContentType, TPayload i_Payload, Func<HttpRequestHeaders, Task> i_HeaderFunc)
    {
      return Put<TReturn, TPayload>(t_Channel, i_Endpoint, i_ContentType, i_Payload, i_HeaderFunc, null);
    }

    /// <summary>
    /// Extension for channel that sends put request with the headers manipulated by i_HeaderFunc
    /// </summary>
    /// <typeparam name="TPayload"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_ContentType"></param>
    /// <param name="i_Payload"></param>
    /// <param name="i_HeaderFunc"></param>
    /// <returns></returns>
    public static Task Put<TPayload>(this INcHttpChannel t_Channel, string i_Endpoint, string i_ContentType, TPayload i_Payload, Func<HttpRequestHeaders, Task> i_HeaderFunc)
    {
      return Put<object, TPayload>(t_Channel, i_Endpoint, i_ContentType, i_Payload, i_HeaderFunc, null);
    }

    /// <summary>
    /// Extension for channel that sends put request with the authentication provided by i_AuthFunc
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <typeparam name="TPayload"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_ContentType"></param>
    /// <param name="i_Payload"></param>
    /// <param name="i_AuthFunc"></param>
    /// <returns></returns>
    public static Task<TReturn> Put<TReturn, TPayload>(this INcHttpChannel t_Channel, string i_Endpoint, string i_ContentType, TPayload i_Payload, Func<Task<AuthenticationHeaderValue>> i_AuthFunc)
    {
      return Put<TReturn, TPayload>(t_Channel, i_Endpoint, i_ContentType, i_Payload, null, i_AuthFunc);
    }

    /// <summary>
    /// Extension for channel that sends put request with the authentication provided by i_AuthFunc
    /// </summary>
    /// <typeparam name="TPayload"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_ContentType"></param>
    /// <param name="i_Payload"></param>
    /// <param name="i_AuthFunc"></param>
    /// <returns></returns>
    public static Task Put<TPayload>(this INcHttpChannel t_Channel, string i_Endpoint, string i_ContentType, TPayload i_Payload, Func<Task<AuthenticationHeaderValue>> i_AuthFunc)
    {
      return Put<object, TPayload>(t_Channel, i_Endpoint, i_ContentType, i_Payload, null, i_AuthFunc);
    }

    /// <summary>
    /// Extension for channel that sends put request with the authentication provided by i_AuthFunc and manipluates the headers using i_HeaderFunc
    /// </summary>
    /// <typeparam name="TReturn">Type of the returned object</typeparam>
    /// <typeparam name="TPayload">Type of the object to be sent</typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_ContentType"></param>
    /// <param name="i_Payload"></param>
    /// <param name="i_HeaderFunc"></param>
    /// <param name="i_AuthFunc"></param>
    /// <returns></returns>
    public static Task<TReturn> Put<TReturn, TPayload>(this INcHttpChannel t_Channel, string i_Endpoint, string i_ContentType, TPayload i_Payload, Func<HttpRequestHeaders, Task> i_HeaderFunc, Func<Task<AuthenticationHeaderValue>> i_AuthFunc)
    {
      return t_Channel.Send<TReturn, TPayload>(HttpMethod.Put, i_Endpoint, i_ContentType, i_Payload, i_HeaderFunc, i_AuthFunc);
    }

    /// <summary>
    /// Extension for channel that sends put request with the authentication provided by i_AuthFunc and manipluates the headers using i_HeaderFunc
    /// </summary>
    /// <typeparam name="TPayload">Type of the object to be sent</typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_ContentType"></param>
    /// <param name="i_Payload"></param>
    /// <param name="i_HeaderFunc"></param>
    /// <param name="i_AuthFunc"></param>
    /// <returns></returns>
    public static Task Put<TPayload>(this INcHttpChannel t_Channel, string i_Endpoint, string i_ContentType, TPayload i_Payload, Func<HttpRequestHeaders, Task> i_HeaderFunc, Func<Task<AuthenticationHeaderValue>> i_AuthFunc)
    {
      return t_Channel.Send<object, TPayload>(HttpMethod.Put, i_Endpoint, i_ContentType, i_Payload, i_HeaderFunc, i_AuthFunc);
    }

    /// <summary>
    /// Extension for channel that sends put request for the provided endpoint information
    /// </summary>
    /// <typeparam name="TPayload">Type of the object to be sent</typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <returns></returns>
    public static Task Put<TPayload>(this INcHttpChannel t_Channel, IEndpointBodyRequestInformation<TPayload> i_Endpoint)
    {
      return t_Channel.Send<object, TPayload>(HttpMethod.Put, i_Endpoint.Endpoint, i_Endpoint.ContentType, i_Endpoint.Payload, i_Endpoint.HeaderFunc, i_Endpoint.AuthenticationHeaderFunc);
    }

    /// <summary>
    /// Extension for channel that sends put request for the provided endpoint information
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <returns></returns>
    public static Task Put<TReturn>(this INcHttpChannel t_Channel, IEndpointReturningRequestInformation<TReturn> i_Endpoint)
    {
      return t_Channel.Send<TReturn, object>(HttpMethod.Put, i_Endpoint.Endpoint, null, null, i_Endpoint.HeaderFunc, i_Endpoint.AuthenticationHeaderFunc);
    }

    /// <summary>
    /// Extension for channel that sends put request for the provided endpoint information
    /// </summary>
    /// <typeparam name="TPayload">Type of the object to be sent</typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_Extender">Extender that extendes the endpoint to the default endpoint</param>
    /// <returns></returns>
    public static Task Put<TPayload>(this INcHttpChannel t_Channel, IEndpointBodyRequestInformation<TPayload> i_Endpoint, IUrlExtender i_Extender)
    {
      return t_Channel.Send<object, TPayload>(HttpMethod.Put, i_Extender.Extend(i_Endpoint.Endpoint), i_Endpoint.ContentType, i_Endpoint.Payload, i_Endpoint.HeaderFunc, i_Endpoint.AuthenticationHeaderFunc);
    }

    /// <summary>
    /// Extension for channel that sends put request for the provided endpoint information
    /// </summary>
    /// <typeparam name="TReturn"></typeparam>
    /// <param name="t_Channel"></param>
    /// <param name="i_Endpoint"></param>
    /// <param name="i_Extender">Extender that extendes the endpoint to the default endpoint</param>
    /// <returns></returns>
    public static Task Put<TReturn>(this INcHttpChannel t_Channel, IEndpointReturningRequestInformation<TReturn> i_Endpoint, IUrlExtender i_Extender)
    {
      return t_Channel.Send<TReturn, object>(HttpMethod.Put, i_Extender.Extend(i_Endpoint.Endpoint), null, null, i_Endpoint.HeaderFunc, i_Endpoint.AuthenticationHeaderFunc);
    }
  }
}
