﻿namespace NiceCore.Services.Http.Channels.Endpoints
{
  /// <summary>
  /// Information about a request to an endpoint using a body as content expecting a response of type TReturn
  /// </summary>
  /// <typeparam name="TReturn">Type of the object that is expected from the response</typeparam>
  /// <typeparam name="TPayload">Type of the payload that is used</typeparam>
  public interface IEndpointReturningBodyRequestInformation<TReturn, TPayload> : IEndpointBodyRequestInformation<TPayload>, IEndpointReturningRequestInformation<TReturn>
  {
  }
}
