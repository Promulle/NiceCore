﻿namespace NiceCore.Services.Http.Channels.Endpoints
{
  /// <summary>
  /// Base impl for IEndpointBodyRequestInformation
  /// </summary>
  /// <typeparam name="TPayload"></typeparam>
  public abstract class BaseEndpointBodyRequestInformation<TPayload> : BaseEndpointRequestInformation, IEndpointBodyRequestInformation<TPayload>
  {
    /// <summary>
    /// ContentType
    /// </summary>
    public abstract string ContentType { get; }

    /// <summary>
    /// Payload
    /// </summary>
    public abstract TPayload Payload { get; init; }
  }
}
