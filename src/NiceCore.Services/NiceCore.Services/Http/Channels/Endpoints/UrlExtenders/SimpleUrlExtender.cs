﻿using NiceCore.Extensions;
using System.Collections.Generic;
using System.Text;

namespace NiceCore.Services.Http.Channels.Endpoints.UrlExtenders
{
  /// <summary>
  /// Extender for simple value extensions
  /// </summary>
  public class SimpleUrlExtender : IUrlExtender
  {
    private readonly IEnumerable<string> m_ValuesToExtendWith;

    /// <summary>
    /// Constructor filling extension values with values
    /// </summary>
    /// <param name="i_Values"></param>
    public SimpleUrlExtender(IEnumerable<string> i_Values)
    {
      m_ValuesToExtendWith = i_Values;
    }

    /// <summary>
    /// Constructor fulling extension values with single value
    /// </summary>
    /// <param name="i_Value"></param>
    public SimpleUrlExtender(string i_Value)
    {
      m_ValuesToExtendWith = new List<string>() { i_Value };
    }

    /// <summary>
    /// Extend i_Original endpoint with the data passed to the constructor
    /// </summary>
    /// <param name="i_OriginalEndpoint"></param>
    /// <returns></returns>
    public string Extend(string i_OriginalEndpoint)
    {
      var builder = new StringBuilder(i_OriginalEndpoint);
      m_ValuesToExtendWith.ForEachItem(r => builder.Append('/').Append(r));
      return builder.ToString();
    }
  }
}
