﻿using System.Collections.Generic;

namespace NiceCore.Services.Http.Channels.Endpoints.UrlExtenders
{
  /// <summary>
  /// Static helper class for default IUrlExtensions
  /// </summary>
  public static class UrlExtensions
  {
    /// <summary>
    /// Creates a simple extender for the provided value
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public static IUrlExtender Simple(string i_Value)
    {
      return new SimpleUrlExtender(i_Value);
    }

    /// <summary>
    /// Creates a simple extender for all values provided in i_Values
    /// </summary>
    /// <param name="i_Values"></param>
    /// <returns></returns>
    public static IUrlExtender Simple(IEnumerable<string> i_Values)
    {
      return new SimpleUrlExtender(i_Values);
    }

    /// <summary>
    /// Creates a UrlExtender that will create a query
    /// </summary>
    /// <param name="i_KeyValuePairs"></param>
    /// <returns></returns>
    public static IUrlExtender QueryFor(IEnumerable<(string Key, string Value)> i_KeyValuePairs)
    {
      return new QueryUrlExtender(i_KeyValuePairs);
    }

    /// <summary>
    /// Creates a UrlExtender that will create a query
    /// </summary>
    /// <param name="i_KeyValuePairs"></param>
    /// <returns></returns>
    public static IUrlExtender QueryFor(params (string Key, string Value)[] i_KeyValuePairs)
    {
      return new QueryUrlExtender(i_KeyValuePairs);
    }
  }
}
