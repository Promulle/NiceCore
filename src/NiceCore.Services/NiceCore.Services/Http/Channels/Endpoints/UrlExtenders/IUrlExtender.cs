﻿namespace NiceCore.Services.Http.Channels.Endpoints.UrlExtenders
{
  /// <summary>
  /// Interface for extenders of Endpoint urls
  /// </summary>
  public interface IUrlExtender
  {
    /// <summary>
    /// Extends i_OriginalEndpoint with the data set for this extender
    /// </summary>
    /// <param name="i_OriginalEndpoint"></param>
    /// <returns></returns>
    string Extend(string i_OriginalEndpoint);
  }
}
