﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NiceCore.Services.Http.Channels.Endpoints.UrlExtenders
{
  /// <summary>
  /// Extender that extends the endpoint with a query string
  /// </summary>
  public class QueryUrlExtender : IUrlExtender
  {
    private readonly IEnumerable<(string Key, string Value)> m_Values;

    /// <summary>
    /// Constructor that fills key value pairs
    /// </summary>
    /// <param name="i_KeyValuePairs"></param>
    public QueryUrlExtender(IEnumerable<(string Key, string Value)> i_KeyValuePairs)
    {
      m_Values = i_KeyValuePairs;
    }

    /// <summary>
    /// returns i_OriginalEndpoint extended with the query string based on the key/value pairs provided
    /// </summary>
    /// <param name="i_OriginalEndpoint"></param>
    /// <returns></returns>
    public string Extend(string i_OriginalEndpoint)
    {
      var valueList = m_Values.ToList();
      var builder = new StringBuilder(i_OriginalEndpoint);
      for (var i = 0; i < valueList.Count; i++)
      {
        var (key, value) = valueList[i];
        var startChar = i == 0 ? '?' : '&';
        builder.Append(startChar)
            .Append(key)
            .Append('=')
            .Append(value);
      }
      return builder.ToString();
    }
  }
}
