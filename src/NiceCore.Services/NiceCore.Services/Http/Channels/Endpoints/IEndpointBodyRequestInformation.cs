﻿namespace NiceCore.Services.Http.Channels.Endpoints
{
  /// <summary>
  /// Information about a request that contains a body
  /// </summary>
  /// <typeparam name="TPayload">type of the object used as payload</typeparam>
  public interface IEndpointBodyRequestInformation<TPayload> : IEndpointRequestInformation
  {
    /// <summary>
    /// ContentType to be used
    /// </summary>
    string ContentType { get; }

    /// <summary>
    /// Payload to be serialized into the body of the request
    /// </summary>
    TPayload Payload { get; }
  }
}
