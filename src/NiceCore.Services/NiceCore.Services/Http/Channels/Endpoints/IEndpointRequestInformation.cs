﻿using System;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace NiceCore.Services.Http.Channels.Endpoints
{
  /// <summary>
  /// Request information for endpoint requests. Enables streamlining of http calls
  /// </summary>
  public interface IEndpointRequestInformation
  {
    /// <summary>
    /// Function that returns the endpoint to be used. This is a function to enable more dynamic endpoint uris like /value=10 as addendum in certain cases
    /// </summary>
    string Endpoint { get; }

    /// <summary>
    /// Optional func that manipulates the headers for just this request
    /// </summary>
    Func<HttpRequestHeaders, Task> HeaderFunc { get; }

    /// <summary>
    /// Optional func that adds an authorization header to the request
    /// </summary>
    Func<Task<AuthenticationHeaderValue>> AuthenticationHeaderFunc { get; }
  }
}
