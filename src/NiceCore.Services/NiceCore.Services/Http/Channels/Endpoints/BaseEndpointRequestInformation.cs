﻿using System;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace NiceCore.Services.Http.Channels.Endpoints
{
  /// <summary>
  /// Base class for implementations of IEndpointRequestInformation
  /// </summary>
  public abstract class BaseEndpointRequestInformation : IEndpointRequestInformation
  {
    /// <summary>
    /// Endpoint
    /// </summary>
    public abstract string Endpoint { get; }

    /// <summary>
    /// Header Func
    /// </summary>
    public abstract Func<HttpRequestHeaders, Task> HeaderFunc { get; }

    /// <summary>
    /// Authentication header func
    /// </summary>
    public abstract Func<Task<AuthenticationHeaderValue>> AuthenticationHeaderFunc { get; }
  }
}
