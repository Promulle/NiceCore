﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NiceCore.Services.Http.Channels.Endpoints
{
  /// <summary>
  /// Base impl for IEndpointReturiningBodyRequestInformation
  /// </summary>
  /// <typeparam name="TReturn"></typeparam>
  /// <typeparam name="TPayload"></typeparam>
    public abstract class BaseEndpointReturningBodyRequestInformation<TReturn, TPayload> : BaseEndpointBodyRequestInformation<TPayload>, IEndpointReturningBodyRequestInformation<TReturn, TPayload>
    {
    }
}
