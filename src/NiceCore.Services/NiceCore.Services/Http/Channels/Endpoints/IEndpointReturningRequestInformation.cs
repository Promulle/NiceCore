﻿namespace NiceCore.Services.Http.Channels.Endpoints
{
  /// <summary>
  /// Information about a request to an endpoint that returns an object of the expected type
  /// </summary>
  /// <typeparam name="TReturn">Expected type of the response object</typeparam>
  public interface IEndpointReturningRequestInformation<TReturn> : IEndpointRequestInformation
  {
  }
}
