﻿namespace NiceCore.Services.Http.Channels.Endpoints
{
  /// <summary>
  /// Base impl for IEndpointReturningRequestInformation
  /// </summary>
  /// <typeparam name="TReturn"></typeparam>
  public abstract class BaseEndpointReturningRequestInformation<TReturn> : BaseEndpointRequestInformation, IEndpointReturningRequestInformation<TReturn>
  {
  }
}
