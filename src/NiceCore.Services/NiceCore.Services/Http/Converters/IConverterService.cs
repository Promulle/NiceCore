﻿namespace NiceCore.Services.Http.Converters
{
  /// <summary>
  /// Service for retrieving converters for content types
  /// </summary>
  public interface IConverterService
  {
    /// <summary>
    /// Returns the payload converter associated with i_ContentType. Throws InvalidOperationException if no converter is specified for i_ContentType
    /// </summary>
    /// <param name="i_ContentType"></param>
    /// <returns></returns>
    IPayloadConverter GetPayloadConverter(string i_ContentType);

    /// <summary>
    /// Returns the response converter associated with i_ContentType. Throws InvalidOperationException if no converter is specified for i_ContentType
    /// </summary>
    /// <param name="i_ContentType"></param>
    /// <returns></returns>
    IResponseConverter GetResponseConverter(string i_ContentType);
  }
}
