﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace NiceCore.Services.Http.Converters
{
  /// <summary>
  /// Converter that converts responses to values
  /// </summary>
  public interface IResponseConverter : IContentTypeBasedConverter
  {
    /// <summary>
    /// Converts the content of i_Response to an instance of T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Response"></param>
    /// <returns></returns>
    Task<T> ConvertValue<T>(HttpResponseMessage i_Response);

    /// <summary>
    /// Converts the content of i_Response to an instance of i_Type
    /// </summary>
    /// <param name="i_Response"></param>
    /// <param name="i_Type"></param>
    /// <returns></returns>
    Task<object> ConvertObjectValue(HttpResponseMessage i_Response, Type i_Type);
  }
}
