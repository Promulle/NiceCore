﻿namespace NiceCore.Services.Http.Converters
{
  /// <summary>
  /// Interface that converters should inherit from that are specified by their content type
  /// </summary>
  public interface IContentTypeBasedConverter
  {
    /// <summary>
    /// Content Type of the converter
    /// </summary>
    string ContentType { get; }
  }
}
