﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace NiceCore.Services.Http.Converters
{
  /// <summary>
  /// Payload Converter
  /// </summary>
  public interface IPayloadConverter : IContentTypeBasedConverter
  {
    /// <summary>
    /// Converts i_Payload into the content that corresponds to this converters ContentType
    /// </summary>
    /// <typeparam name="T">Type of the payload</typeparam>
    /// <param name="i_Payload">Payload to be converted</param>
    /// <returns></returns>
    Task<HttpContent> ToContent<T>(T i_Payload);

    /// <summary>
    /// Converts i_Payload into the content that corresponds to this converters ContentType
    /// </summary>
    /// <param name="i_Payload">Payload to be converted</param>
    /// <param name="i_Type">Type of the payload</param>
    /// <returns></returns>
    Task<HttpContent> ToContent(object i_Payload, Type i_Type);
  }
}
