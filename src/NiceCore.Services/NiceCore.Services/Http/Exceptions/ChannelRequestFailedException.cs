﻿using System;
using System.Net;

namespace NiceCore.Services.Http.Exceptions
{
  /// <summary>
  /// Exception that is thrown if a request made by a channel did not return OK
  /// </summary>
#pragma warning disable RCS1194 // Implement exception constructors.
  public class ChannelRequestFailedException : Exception
#pragma warning restore RCS1194 // Implement exception constructors.
  {
    /// <summary>
    /// Code that was returned
    /// </summary>
    public HttpStatusCode Code { get; }
    
    /// <summary>
    /// Content that was read that should contain an error 
    /// </summary>
    public string Content { get; }

    /// <summary>
    /// Ctor filling message and code
    /// </summary>
    /// <param name="i_Message"></param>
    /// <param name="i_Code"></param>
    /// <param name="i_Content"></param>
    public ChannelRequestFailedException(string i_Message, HttpStatusCode i_Code, string i_Content) : base(i_Message)
    {
      Code = i_Code;
      Content = i_Content;
    }
  }
}
