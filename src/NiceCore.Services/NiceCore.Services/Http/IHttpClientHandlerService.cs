﻿using System;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace NiceCore.Services.Http
{
  /// <summary>
  /// Service that manages distribution of HttpClientHandler instances
  /// </summary>
  public interface IHttpClientHandlerService
  {
    /// <summary>
    /// Returns the handler
    /// </summary>
    /// <param name="i_HandlerName"></param>
    /// <param name="i_UseCookies"></param>
    /// <param name="i_CertificateValidationHandler"></param>
    /// <returns></returns>
    HttpClientHandler GetHandler(string i_HandlerName, bool i_UseCookies, Func<HttpRequestMessage, X509Certificate, X509Chain, SslPolicyErrors, bool> i_CertificateValidationHandler);

    /// <summary>
    /// Returns the handler
    /// </summary>
    /// <param name="i_HandlerName"></param>
    /// <returns></returns>
    HttpClientHandler GetHandler(string i_HandlerName);

    /// <summary>
    /// Returns the handler
    /// </summary>
    /// <param name="i_HandlerName"></param>
    /// <param name="i_UseCookies"></param>
    /// <returns></returns>
    HttpClientHandler GetHandler(string i_HandlerName, bool i_UseCookies);
  }
}
