﻿namespace NiceCore.Services.TaskResults
{
  /// <summary>
  /// Service for retrieving values from any kind of task. Only use this service if you have no way of using the direkt generic way to get the result
  /// </summary>
  public interface ITaskResultService
  {
    /// <summary>
    /// Returns the result of a task. If i_TaskObject is just a Task, the provided instance itself will be returned
    /// </summary>
    /// <param name="i_TaskObject"></param>
    /// <returns></returns>
    public object GetTaskResult(object i_TaskObject);
  }
}
