using System.Collections.Generic;
using System.Linq;
using NiceCore.Base.Services;
using NiceCore.ServiceLocation;
using NiceCore.Services.Chunks.Config;
using NiceCore.Services.Config;

namespace NiceCore.Services.Chunks
{
  /// <summary>
  /// Chunk Service
  /// </summary>
  [InitializeOnResolve]
  [Register(InterfaceType = typeof(IChunkService), LifeStyle = LifeStyles.Singleton)]
  public class ChunkService : BaseService, IChunkService
  {
    private int m_ChunkSize = -1;
    
    /// <summary>
    /// Configuration
    /// </summary>
    public INcConfiguration Configuration { get; set; }

    /// <summary>
    /// Initialize
    /// </summary>
    protected override void InternalInitialize()
    {
      m_ChunkSize = Configuration.GetValue(ChunkServiceConfigurationSettingModel.KEY, 2000);
    }

    /// <summary>
    /// Get Current CHunk Size
    /// </summary>
    /// <returns></returns>
    public int GetCurrentChunkSize()
    {
      return m_ChunkSize;
    }

    /// <summary>
    /// Get Chunks Of
    /// </summary>
    /// <param name="i_Input"></param>
    /// <param name="i_OverridingChunkSize"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public IEnumerable<IEnumerable<T>> GetChunksOf<T>(IEnumerable<T> i_Input, int? i_OverridingChunkSize = null)
    {
      if (m_ChunkSize < 1 || (i_OverridingChunkSize != null && i_OverridingChunkSize < 1))
      {
        return new[] {i_Input};
      }

      var chunkSize = i_OverridingChunkSize ?? m_ChunkSize;
      return i_Input.Chunk(chunkSize);
    }
  }
}