using System.Collections.Generic;

namespace NiceCore.Services.Chunks
{
  /// <summary>
  /// ChunkService
  /// </summary>
  public interface IChunkService
  {
    /// <summary>
    /// Get Current Chunk Size
    /// </summary>
    /// <returns></returns>
    int GetCurrentChunkSize();

    /// <summary>
    /// Get Hcunks of
    /// </summary>
    /// <param name="i_Input"></param>
    /// <param name="i_OverridingChunkSize"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    IEnumerable<IEnumerable<T>> GetChunksOf<T>(IEnumerable<T> i_Input, int? i_OverridingChunkSize = null);
  }
}