using System;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Services.Chunks.Config
{
  /// <summary>
  /// Chunk Service Configuration Setting Model
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class ChunkServiceConfigurationSettingModel : IConfigurableSettingModel
  {
    /// <summary>
    /// Key
    /// </summary>
    public const string KEY = "NiceCore.Services.ChunkService.ChunkSize";

    /// <summary>
    /// Key
    /// </summary>
    public string Key { get; } = KEY;

    /// <summary>
    /// Setting Type
    /// </summary>
    public Type SettingType { get; } = typeof(int);
  }
}