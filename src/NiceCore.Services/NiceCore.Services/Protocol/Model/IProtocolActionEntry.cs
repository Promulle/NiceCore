namespace NiceCore.Services.Protocol.Model
{
  /// <summary>
  /// Protocol action entry
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  public interface IProtocolActionEntry<TKey> : IProtocolModel<TKey>
  {
    /// <summary>
    /// The context this entry belongs to
    /// </summary>
    IProtocolActionEntryContext<TKey> Context { get; }
  }
}