namespace NiceCore.Services.Protocol.Model
{
  /// <summary>
  /// Base interface for all classes in the protocol system
  /// </summary>
  /// <typeparam name="TKey">Type of key to be used</typeparam>
  public interface IProtocolModel<TKey>
  {
    /// <summary>
    /// Key Value
    /// </summary>
    TKey Key { get; }

    /// <summary>
    /// add i_instance to the children of this instance
    /// </summary>
    /// <param name="i_Instance"></param>
    void AddToChildInstances(IProtocolModel<TKey> i_Instance);
  }
}