using System.Collections.Generic;

namespace NiceCore.Services.Protocol.Model
{
  /// <summary>
  /// Interface for a single action in the protocol context
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  public interface IProtocolAction<TKey> : IProtocolModel<TKey>
  {
    /// <summary>
    /// The protocol this context belongs to 
    /// </summary>
    IProtocol<TKey> Protocol { get; }

    /// <summary>
    /// add a context to this action
    /// </summary>
    /// <param name="i_Context"></param>
    void AddToContexts(IProtocolActionEntryContext<TKey> i_Context);
  }
}