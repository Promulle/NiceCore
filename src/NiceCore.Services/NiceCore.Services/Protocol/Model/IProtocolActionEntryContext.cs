using System.Collections.Generic;

namespace NiceCore.Services.Protocol.Model
{
  /// <summary>
  /// Interface for the class that implements the context -> Gives the entries below it some context 
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  public interface IProtocolActionEntryContext<TKey> : IProtocolModel<TKey>
  {
    /// <summary>
    /// Action which entries this context is used for
    /// </summary>
    IProtocolAction<TKey> Action { get; }

    /// <summary>
    /// Add To Entries
    /// </summary>
    /// <param name="i_Entry"></param>
    void AddToEntries(IProtocolActionEntry<TKey> i_Entry);
  }
}