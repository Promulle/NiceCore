using System.Collections.Generic;

namespace NiceCore.Services.Protocol.Model
{
  /// <summary>
  /// Interface for the overarching protocol class
  /// </summary>
  /// <typeparam name="TKey">Type of identifier to use for the protocol</typeparam>
  public interface IProtocol<TKey> : IProtocolModel<TKey>
  {
    /// <summary>
    /// Add an action to the protocol
    /// </summary>
    /// <param name="i_Action"></param>
    void AddToActions(IProtocolAction<TKey> i_Action);
  }
}