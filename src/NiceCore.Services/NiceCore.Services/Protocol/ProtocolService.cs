using System.Threading;
using Microsoft.Extensions.Logging;
using NiceCore.ServiceLocation;
using NiceCore.Services.Protocol.Abstractions.Builders;
using NiceCore.Services.Protocol.Abstractions.Progress;
using NiceCore.Services.Protocol.Process;

namespace NiceCore.Services.Protocol
{
  /// <summary>
  /// Protocol Service
  /// </summary>
  [Register(InterfaceType = typeof(IProtocolService))]
  public class ProtocolService : IProtocolService
  {
    /// <summary>
    /// logger
    /// </summary>
    public ILogger<ProtocolService> Logger { get; set; }
    
    /// <summary>
    /// Creates a new protocol for a given definition
    /// </summary>
    /// <param name="i_Definition"></param>
    /// <param name="i_Builder"></param>
    /// <typeparam name="TKey"></typeparam>
    /// <returns></returns>
    public InProgressProtocol<TKey> CreateProtocol<TKey>(IProtocolProcessDefinition<TKey> i_Definition, IProtocolBuilder<TKey> i_Builder)
    {
      var process = new ProtocolProcess<TKey>(Logger);
      process.StartProtocol(i_Definition, CancellationToken.None);
      return new(i_Builder, i_Definition.GetImplementingTypeProviders(), process);
    }
  }
}