using System;
using System.Threading.Tasks;

namespace NiceCore.Services.Protocol.Process.Actions
{
  /// <summary>
  /// Protocol process action
  /// </summary>
  public interface IProtocolProcessAction
  {
    /// <summary>
    /// Type of entity this action should apply to
    /// </summary>
    Type ProtocolRelatedType { get; }
    
    /// <summary>
    /// Time the action should be executed
    /// </summary>
    ProtocolActionExecutionTimes ExecutionTime { get; }

    /// <summary>
    /// Execute
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <returns></returns>
    Task Execute(object i_Instance);
  }
}