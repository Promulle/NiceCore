namespace NiceCore.Services.Protocol.Process.Actions
{
  /// <summary>
  /// Points in Time of the Lifecycle for a given instance
  /// </summary>
  public enum ProtocolActionExecutionTimes
  {
    /// <summary>
    /// The instance has just been added
    /// </summary>
    InstanceAdded,
    
    /// <summary>
    /// The instance has just been marked as completed by the in progress object
    /// </summary>
    InstanceCompleted,
  }
}