using System;
using System.Threading.Tasks;
using NiceCore.Services.Protocol.Model;

namespace NiceCore.Services.Protocol.Process.Actions
{
  /// <summary>
  /// Base for Protocol process action
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  /// <typeparam name="TProtocolModel"></typeparam>
  public abstract class BaseProtocolProcessAction<TKey, TProtocolModel> : IProtocolProcessAction
    where TProtocolModel : IProtocolModel<TKey>
  {
    /// <summary>
    /// Protocol related type
    /// </summary>
    public Type ProtocolRelatedType { get; } = typeof(TProtocolModel);
    
    /// <summary>
    /// Execution Time
    /// </summary>
    public abstract ProtocolActionExecutionTimes ExecutionTime { get; }

    /// <summary>
    /// Execute to be overriden by the actual implementation
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <returns></returns>
    protected abstract Task ExecuteGeneric(TProtocolModel i_Instance);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <returns></returns>
    public Task Execute(object i_Instance)
    {
      if (i_Instance != null && i_Instance is TProtocolModel casted)
      {
        return ExecuteGeneric(casted);
      }
      throw new InvalidOperationException($"Supplied instance is either null: {i_Instance == null} or is not assignable to {typeof(TProtocolModel).FullName}");
    }
  }
}