using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Logging;
using NiceCore.Logging.Extensions;

namespace NiceCore.Services.Protocol.Process.Actions
{
  /// <summary>
  /// Protocol Process Action Store
  /// </summary>
  public class ProtocolProcessActionStore<TKey>
  {
    private readonly ILogger m_Logger;
    
    /// <summary>
    /// ctor
    /// </summary>
    public ProtocolProcessActionStore(ILogger i_Logger)
    {
      m_Logger = i_Logger;
    }
    
    /// <summary>
    /// Actions to be run when an instance was added
    /// </summary>
    public Dictionary<Type, List<IProtocolProcessAction>> InstanceAddedActions { get; } = new();

    /// <summary>
    /// Actions to be run when an instance was marked as completed
    /// </summary>
    public Dictionary<Type, List<IProtocolProcessAction>> InstanceCompletedActions { get; } = new();

    /// <summary>
    /// Initialize for a list of actions
    /// </summary>
    /// <param name="i_Actions"></param>
    public void InitializeFor(IReadOnlyCollection<IProtocolProcessAction> i_Actions)
    {
      foreach (var group in i_Actions.GroupBy(r => r.ProtocolRelatedType))
      {
        var addedActions = group.Where(r => r.ExecutionTime == ProtocolActionExecutionTimes.InstanceAdded).ToList();
        if (addedActions.Count > 0)
        {
          InstanceAddedActions[group.Key] = addedActions;
        }
        var completedActions = group.Where(r => r.ExecutionTime == ProtocolActionExecutionTimes.InstanceCompleted).ToList();
        if (completedActions.Count > 0)
        {
          InstanceCompletedActions[group.Key] = completedActions;
        }
      }
    }

    private async Task RunFunctionsFor(IReadOnlyDictionary<Type, List<IProtocolProcessAction>> i_Dictionary, object i_Instance)
    {
      if (i_Instance != null)
      {
        if (i_Dictionary.TryGetValue(i_Instance.GetType(), out var values))
        {
          foreach (var action in values)
          {
            try
            {
              await action.Execute(i_Instance).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
              m_Logger.Error(ex, $"Error while executing action for protocol instance: {action.ProtocolRelatedType.FullName}-{action.ExecutionTime}");
            }
          }
        }
      }
    }
    
    /// <summary>
    /// Runs all completed functions for the instance
    /// </summary>
    /// <param name="i_Instance"></param>
    public Task RunCompletedFor(object i_Instance)
    {
      return RunFunctionsFor(InstanceCompletedActions, i_Instance);
    }
    
    /// <summary>
    /// Run all added functions for the instance
    /// </summary>
    /// <param name="i_Instance"></param>
    public Task RunAddedFor(object i_Instance)
    {
      return RunFunctionsFor(InstanceAddedActions, i_Instance);
    }
  }
}