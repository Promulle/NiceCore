using System;

namespace NiceCore.Services.Protocol.Process.Instructions
{
  /// <summary>
  /// Run Completed actions for instructions
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  public class ProtocolRunCompletedActionsForInstructions<TKey> : IProtocolProcessInstruction<TKey>
  {
    /// <summary>
    /// Type of the instance
    /// </summary>
    public Type InstanceType { get; init; }
    
    /// <summary>
    /// Key of the instance
    /// </summary>
    public TKey Key { get; init; }
  }
}