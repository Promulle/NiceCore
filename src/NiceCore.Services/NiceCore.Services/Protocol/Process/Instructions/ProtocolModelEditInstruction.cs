using System;
using System.Threading.Tasks;
using NiceCore.Services.Protocol.Model;

namespace NiceCore.Services.Protocol.Process.Instructions
{
  /// <summary>
  /// Instruction that edits an existing instance
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  public class ProtocolModelEditInstruction<TKey> : IProtocolProcessInstruction<TKey>
  {
    /// <summary>
    /// Type of the instance
    /// </summary>
    public Type InstanceType { get; init; }
    
    /// <summary>
    /// Key of the instance
    /// </summary>
    public TKey Key { get; init; }
    
    /// <summary>
    /// Edit Func to be used
    /// </summary>
    public Func<IProtocolModel<TKey>, Task> EditFunc { get; init; }
  }
}
