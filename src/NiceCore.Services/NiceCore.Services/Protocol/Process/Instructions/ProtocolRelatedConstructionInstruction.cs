using System;
using NiceCore.Services.Protocol.Abstractions.Builders;

namespace NiceCore.Services.Protocol.Process.Instructions
{
  /// <summary>
  /// Construction info
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  public class ProtocolRelatedConstructionInstruction<TKey> : IProtocolProcessInstruction<TKey>
  {
    /// <summary>
    /// Builder
    /// </summary>
    public IProtocolRelatedBuilder<TKey> Builder { get; init; }
    
    /// <summary>
    /// Parent
    /// </summary>
    public ProtocolParentInfo<TKey> ParentInfo { get; init; }

    /// <summary>
    /// Type that this deals with -> Builder.ToInstance's results type
    /// </summary>
    public Type ProtocolRelatedType { get; init; }
  }
}