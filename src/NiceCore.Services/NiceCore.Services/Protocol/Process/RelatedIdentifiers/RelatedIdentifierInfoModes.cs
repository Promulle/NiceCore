namespace NiceCore.Services.Protocol.Process.RelatedIdentifiers
{
  /// <summary>
  /// Enum that defines what kind of related model can be searched by the related identifier info
  /// </summary>
  public enum RelatedIdentifierInfoModes
  {
    /// <summary>
    /// Context
    /// </summary>
    ActionEntryContext
  }
}