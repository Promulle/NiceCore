namespace NiceCore.Services.Protocol.Process.RelatedIdentifiers
{
  /// <summary>
  /// Info that enables finding the correct identifier for a value
  /// </summary>
  public class ProtocolRelatedIdentifierInfo
  {
    /// <summary>
    /// Mode
    /// </summary>
    public RelatedIdentifierInfoModes Mode { get; init; }
    
    /// <summary>
    /// Value that is used for matching
    /// </summary>
    public object Value { get; init; }
  }
}