using System;
using NiceCore.Services.Protocol.Process.Matchers;

namespace NiceCore.Services.Protocol.Process
{
  /// <summary>
  /// Parent info 
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  public class ProtocolParentInfo<TKey>
  {
    /// <summary>
    /// Parent Key, this is differently interpreted: if MatchingInfo is null this is the direct parent key
    /// if matching info is provided this is the key of the "parent-parent" object
    /// </summary>
    public TKey ParentKey { get; init; }
    
    /// <summary>
    /// Matching info
    /// </summary>
    public ProtocolMatchingInfo MatchingInfo { get; init; }
    
    /// <summary>
    /// Parent Type
    /// </summary>
    public Type ParentType { get; init; }
  }
}