namespace NiceCore.Services.Protocol.Process
{
  /// <summary>
  /// Interface for instructions that are executed when they are removed from the queue
  /// </summary>
  public interface IProtocolProcessInstruction<TKey>
  {
    
  }
}