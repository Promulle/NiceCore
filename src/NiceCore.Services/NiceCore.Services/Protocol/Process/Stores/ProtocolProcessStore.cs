using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using NiceCore.Services.Protocol.Abstractions.ParentKeyAccessors;
using NiceCore.Services.Protocol.Model;
using NiceCore.Services.Protocol.Process.Matchers;

namespace NiceCore.Services.Protocol.Process.Stores
{
  /// <summary>
  /// Store keeping track of entities for a single protocol process
  /// </summary>
  public class ProtocolProcessStore<TKey>
  {
    private static readonly List<IProtocolModel<TKey>> DEFAULT_EMPTY_LIST = new();

    private readonly ILogger m_Logger;
    private readonly List<IProtocolRelatedMatcher<TKey>> m_Matchers = new();
    private readonly ProtocolMatchedInstanceInstantiatorStore<TKey> m_MatcherInstantiatorStore;
    private IParentKeyAccessor<TKey> m_ParentKeyAccessor;

    /// <summary>
    /// Protocol to be used
    /// </summary>
    public IProtocol<TKey> Protocol { get; private set; }

    /// <summary>
    /// Instances used by the protocol
    /// </summary>
    public Dictionary<Type, List<IProtocolModel<TKey>>> ProtocolInstances { get; } = new();

    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_Logger"></param>
    public ProtocolProcessStore(ILogger i_Logger)
    {
      m_Logger = i_Logger;
      m_MatcherInstantiatorStore = new(i_Logger);
    }

    /// <summary>
    /// Initialize this store for definition
    /// </summary>
    /// <param name="i_Definition"></param>
    public void InitializeForDefinition(IProtocolProcessDefinition<TKey> i_Definition)
    {
      m_Matchers.AddRange(i_Definition.GetMatchers());
      m_MatcherInstantiatorStore.InitializeWith(i_Definition.GetInstantiators());
      m_ParentKeyAccessor = i_Definition.GetParentKeyAccessor();
    }

    /// <summary>
    /// Returns an instance for the type that matches the given key
    /// </summary>
    /// <param name="i_Type"></param>
    /// <param name="i_InstanceKey"></param>
    /// <returns></returns>
    public IProtocolModel<TKey> GetInstanceFor(Type i_Type, TKey i_InstanceKey)
    {
      if (i_Type == null)
      {
        throw new ArgumentException($"Method {nameof(GetInstanceFor)} was called with a null value for argument {nameof(i_Type)}. This is not allowed.");
      }

      if (typeof(IProtocol<TKey>).IsAssignableFrom(i_Type))
      {
        return Protocol;
      }

      if (ProtocolInstances.TryGetValue(i_Type, out var instances))
      {
        return instances.Find(r => EqualityComparer<TKey>.Default.Equals(r.Key, i_InstanceKey));
      }

      return null;
    }

    /// <summary>
    /// Add the output of a builder to the store
    /// </summary>
    /// <param name="i_Instance">Instance to be added, must not be null</param>
    /// <param name="i_Info"></param>
    public void Add(IProtocolModel<TKey> i_Instance, ProtocolParentInfo<TKey> i_Info)
    {
      if (i_Instance == null)
      {
        throw new InvalidOperationException("Can not add a null instance to the protocol process store");
      }

      if (i_Instance is IProtocol<TKey> protocol)
      {
        Protocol = protocol;
      }
      else
      {
        if (!ProtocolInstances.ContainsKey(i_Instance.GetType()))
        {
          ProtocolInstances[i_Instance.GetType()] = new();
        }

        PutIntoStructure(i_Instance, i_Info);
        ProtocolInstances[i_Instance.GetType()].Add(i_Instance);
      }
    }

    private IProtocolModel<TKey> GetParentFor(IProtocolModel<TKey> i_Instance, ProtocolParentInfo<TKey> i_Info)
    {
      if (i_Info.MatchingInfo == null)
      {
        if (ProtocolInstances.TryGetValue(i_Info.ParentType, out var possibleParents))
        {
          var parent = possibleParents.Find(r => EqualityComparer<TKey>.Default.Equals(r.Key, i_Info.ParentKey));
          if (parent == null)
          {
            throw new InvalidOperationException($"Could not find an instance of type {i_Info.ParentType.FullName} for key {i_Info.ParentKey}");
          }

          return parent;
        }

        throw new InvalidOperationException($"No instances defined for type {i_Info.ParentType.FullName}. Could not get a parent for the instance with key {i_Instance.Key}.");
      }

      var fittingMatcher = m_Matchers.FirstOrDefault(r => r.ProtocolRelatedType == i_Info.ParentType && r.CanBeUsedFor(i_Info.MatchingInfo.Value));
      if (fittingMatcher == null)
      {
        throw new ArgumentException($"Could not find a matcher that matches the type {i_Info.ParentType.FullName} and can be used for value {i_Info.MatchingInfo.Value}");
      }

      return fittingMatcher.GetOrCreateByValue(this, m_MatcherInstantiatorStore, m_ParentKeyAccessor, i_Info.MatchingInfo.Value, i_Info.ParentKey);
    }

    private void PutIntoStructure(IProtocolModel<TKey> i_Instance, ProtocolParentInfo<TKey> i_Info)
    {
      if (i_Instance is IProtocolAction<TKey> action)
      {
        if (Protocol == null)
        {
          throw new InvalidOperationException($"Could not add the action with key {i_Instance.Key} to the protocol. No protocol is currently in store.");
        }

        Protocol.AddToActions(action);
      }
      else
      {
        var parent = GetParentFor(i_Instance, i_Info);
        parent.AddToChildInstances(i_Instance);
      }
    }

    /// <summary>
    /// Return the data
    /// </summary>
    /// <typeparam name="TProtocolModel"></typeparam>
    /// <returns></returns>
    public ProtocolProcessStoreDataEnumerator<TKey, TProtocolModel> GetDataEnumerator<TProtocolModel>()
      where TProtocolModel : IProtocolModel<TKey>
    {
      if (ProtocolInstances.TryGetValue(typeof(TProtocolModel), out var values))
      {
        return new()
        {
          Collection = values
        };
      }

      return new()
      {
        Collection = DEFAULT_EMPTY_LIST
      };
    }
  }
}