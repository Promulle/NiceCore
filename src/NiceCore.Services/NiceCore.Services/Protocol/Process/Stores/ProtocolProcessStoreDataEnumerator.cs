using System;
using System.Collections.Generic;
using NiceCore.Services.Protocol.Model;

namespace NiceCore.Services.Protocol.Process.Stores
{
  /// <summary>
  /// Struct that provides the abilitiy to iterate over protocol process store data
  /// </summary>
  public ref struct ProtocolProcessStoreDataEnumerator<TKey, TRes>
  {
    /// <summary>
    /// Collection that is enumerated
    /// </summary>
    public IReadOnlyList<IProtocolModel<TKey>> Collection { get; init; }

    /// <summary>
    /// Is Null
    /// </summary>
    /// <returns></returns>
    public bool IsNull()
    {
      return Collection == null;
    }

    /// <summary>
    /// Finds first element that fits the where func given, provided parameter for less capturing
    /// </summary>
    /// <param name="i_WhereFunc"></param>
    /// <returns></returns>
    public TRes FirstOrDefault(Func<TRes, bool> i_WhereFunc)
    {
      if (Collection.Count == 0)
      {
        return default;
      }

      foreach (var instance in Collection)
      {
        if (instance is TRes casted)
        {
          if (i_WhereFunc.Invoke(casted))
          {
            return casted;
          }
        }
      }
      return default;
    }
  }
}