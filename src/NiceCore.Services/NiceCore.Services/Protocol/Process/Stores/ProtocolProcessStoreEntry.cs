using NiceCore.Services.Protocol.Model;

namespace NiceCore.Services.Protocol.Process.Stores
{
  /// <summary>
  /// Protocol process store entry
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  /// <typeparam name="TProtocolModel"></typeparam>
  public class ProtocolProcessStoreEntry<TKey, TProtocolModel>
    where TProtocolModel : IProtocolModel<TKey>
  {
    /// <summary>
    /// Parent key
    /// </summary>
    public TKey ParentKey { get; init; }

    /// <summary>
    /// Instance
    /// </summary>
    public TProtocolModel Instance { get; init; }
  }
}