using System;
using NiceCore.Services.Protocol.Abstractions.ParentKeyAccessors;
using NiceCore.Services.Protocol.Model;
using NiceCore.Services.Protocol.Process.Stores;

namespace NiceCore.Services.Protocol.Process.Matchers
{
  /// <summary>
  /// Matcher that searches a store for the element defined
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  public interface IProtocolRelatedMatcher<TKey>
  {
    /// <summary>
    /// Type that is covered by the matcher
    /// </summary>
    Type ProtocolRelatedType { get; }

    /// <summary>
    /// Defines whether the matcher should be used for this value -> enables distinct selection of matching for different types of values
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    bool CanBeUsedFor(object i_Value);
    
    /// <summary>
    /// Searches i_Store for an object that matches i_Value
    /// </summary>
    /// <param name="i_Store"></param>
    /// <param name="i_InstantiatorStore"></param>
    /// <param name="i_ParentKeyAccessor">Instance used to get parentkey for a given instance -> enables parent scoped matching</param>
    /// <param name="i_Value"></param>
    /// <param name="i_ParentKey"></param>
    /// <returns></returns>
    IProtocolModel<TKey> GetOrCreateByValue(ProtocolProcessStore<TKey> i_Store, ProtocolMatchedInstanceInstantiatorStore<TKey> i_InstantiatorStore, IParentKeyAccessor<TKey> i_ParentKeyAccessor, object i_Value, TKey i_ParentKey);
  }
}