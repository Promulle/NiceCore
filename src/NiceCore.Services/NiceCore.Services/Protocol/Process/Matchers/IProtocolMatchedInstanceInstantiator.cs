namespace NiceCore.Services.Protocol.Process.Matchers
{
  /// <summary>
  /// Interface for classes providing functionality that creates instances which were not found by matchers
  /// </summary>
  /// <typeparam name="TProtocolModel"></typeparam>
  public interface IProtocolMatchedInstanceInstantiator<out TProtocolModel> : INonGenericProtocolMatchedInstanceInstantiator
  {
    /// <summary>
    /// Create the instance for the given value
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    TProtocolModel CreateForGeneric(object i_Value);
  }
}