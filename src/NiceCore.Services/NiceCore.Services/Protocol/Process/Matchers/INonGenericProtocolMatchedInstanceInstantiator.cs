namespace NiceCore.Services.Protocol.Process.Matchers
{
  /// <summary>
  /// Non Generic base interface
  /// </summary>
  public interface INonGenericProtocolMatchedInstanceInstantiator
  {
    /// <summary>
    /// Checks whether this instantiator can be used for the value
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    bool AppliesTo(object i_Value);

    /// <summary>
    /// Create the instance for the given value
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    object CreateFor(object i_Value);
  }
}