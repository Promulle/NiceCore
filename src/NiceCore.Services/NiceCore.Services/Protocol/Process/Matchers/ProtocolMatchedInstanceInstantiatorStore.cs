using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using NiceCore.Logging;
using NiceCore.Logging.Extensions;
using NiceCore.Services.Protocol.Model;

namespace NiceCore.Services.Protocol.Process.Matchers
{
  /// <summary>
  /// Store that keeps all known matchedInstanceInstantiators
  /// </summary>
  public class ProtocolMatchedInstanceInstantiatorStore<TKey>
  {
    private readonly ILogger m_Logger;
    
    /// <summary>
    /// Action instantiators
    /// </summary>
    public List<IProtocolMatchedInstanceInstantiator<IProtocolAction<TKey>>> ActionInstantiators { get; } = new();

    /// <summary>
    /// EntryContext Instantiators
    /// </summary>
    public List<IProtocolMatchedInstanceInstantiator<IProtocolActionEntryContext<TKey>>> ActionEntryContextInstantiators { get; } = new();

    /// <summary>
    /// Action entry instantiators
    /// </summary>
    public List<IProtocolMatchedInstanceInstantiator<IProtocolActionEntry<TKey>>> ActionEntryInstantiators { get; } = new();

    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_Logger"></param>
    public ProtocolMatchedInstanceInstantiatorStore(ILogger i_Logger)
    {
      m_Logger = i_Logger;
    }
    
    /// <summary>
    /// Initializes this store with the provided instantiators
    /// </summary>
    /// <param name="i_Instantiators"></param>
    public void InitializeWith(List<INonGenericProtocolMatchedInstanceInstantiator> i_Instantiators)
    {
      i_Instantiators.ForEach(instantiator =>
      {
        switch (instantiator)
        {
          case IProtocolMatchedInstanceInstantiator<IProtocolAction<TKey>> action:
            ActionInstantiators.Add(action);
            break;
          case IProtocolMatchedInstanceInstantiator<IProtocolActionEntry<TKey>> entry:
            ActionEntryInstantiators.Add(entry);
            break;
          case IProtocolMatchedInstanceInstantiator<IProtocolActionEntryContext<TKey>> context:
            ActionEntryContextInstantiators.Add(context);
            break;
          default:
            m_Logger.Warning($"Could not sort match instantiator of type {instantiator.GetType().FullName} into a matching collection.");
            break;
        }
      });
    }

    /// <summary>
    /// Gets all instantiators for the type
    /// </summary>
    /// <typeparam name="TProtocolModel"></typeparam>
    /// <returns></returns>
    private ICollection<IProtocolMatchedInstanceInstantiator<TProtocolModel>> GetInstantiators<TProtocolModel>()
      where TProtocolModel : IProtocolModel<TKey>
    {
      //TODO: possible performance improvement when not using .OfType.ToList but just returning the correct collection
      if (typeof(IProtocolAction<TKey>).IsAssignableFrom(typeof(TProtocolModel)))
      {
        return ActionInstantiators.OfType<IProtocolMatchedInstanceInstantiator<TProtocolModel>>().ToList();
      }

      if (typeof(IProtocolActionEntry<TKey>).IsAssignableFrom(typeof(TProtocolModel)))
      {
        return ActionEntryInstantiators.OfType<IProtocolMatchedInstanceInstantiator<TProtocolModel>>().ToList();
      }

      if (typeof(IProtocolActionEntryContext<TKey>).IsAssignableFrom(typeof(TProtocolModel)))
      {
        return ActionEntryContextInstantiators.OfType<IProtocolMatchedInstanceInstantiator<TProtocolModel>>().ToList();
      }

      return null;
    }

    /// <summary>
    /// Get Instantiator
    /// </summary>
    /// <typeparam name="TProtocolModel"></typeparam>
    /// <returns></returns>
    public IProtocolMatchedInstanceInstantiator<TProtocolModel> GetInstantiatorFor<TProtocolModel>(object i_Value)
      where TProtocolModel : IProtocolModel<TKey>
    {
      var instantiators = GetInstantiators<TProtocolModel>();
      if (instantiators == null)
      {
        throw new InvalidOperationException($"Store does not even contain a definition for type {typeof(TProtocolModel)}");
      }
      return instantiators.FirstOrDefault(r => r.AppliesTo(i_Value));
    }
  }
}