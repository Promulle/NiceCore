namespace NiceCore.Services.Protocol.Process.Matchers
{
  /// <summary>
  /// Matching info when shortcuts should be taken
  /// </summary>
  public class ProtocolMatchingInfo
  {
    /// <summary>
    /// Value used for matching
    /// </summary>
    public object Value { get; init; }
    
    /// <summary>
    /// Target
    /// </summary>
    public ProtocolMatchingInfoTargets Target { get; init; }
  }
}