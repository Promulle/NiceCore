using System;
using System.Collections.Generic;
using System.Linq;
using NiceCore.Services.Protocol.Abstractions.ParentKeyAccessors;
using NiceCore.Services.Protocol.Model;
using NiceCore.Services.Protocol.Process.Stores;

namespace NiceCore.Services.Protocol.Process.Matchers
{
  /// <summary>
  /// Base impl for matchers used to find objects when taking shortcut apis when constructing a protocol
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  /// <typeparam name="TProtocolModel"></typeparam>
  /// <typeparam name="TProtocolModelParent">Type of parent of the entity to find a match for</typeparam>
  public abstract class BaseProtocolRelatedMatcher<TKey, TProtocolModel, TProtocolModelParent> : IProtocolRelatedMatcher<TKey>
    where TProtocolModel : IProtocolModel<TKey>
  {
    /// <summary>
    /// Protocol type
    /// </summary>
    public Type ProtocolRelatedType { get; } = typeof(TProtocolModel);

    /// <summary>
    /// Whether this matcher can be used for i_Value
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public abstract bool CanBeUsedFor(object i_Value);

    /// <summary>
    /// searches i_Store for the value or creates a new one using an instantiator that was predefined for the type/value definition
    /// </summary>
    /// <param name="t_Store"></param>
    /// <param name="i_InstantiatorStore"></param>
    /// <param name="i_ParentKeyAccessor">Instance used to get parentkey for a given instance -> enables parent scoped matching</param>
    /// <param name="i_Value"></param>
    /// <param name="i_ParentKey"></param>
    /// <returns></returns>
    /// <exception cref="InvalidOperationException"></exception>
    public IProtocolModel<TKey> GetOrCreateByValue(ProtocolProcessStore<TKey> t_Store, ProtocolMatchedInstanceInstantiatorStore<TKey> i_InstantiatorStore, IParentKeyAccessor<TKey> i_ParentKeyAccessor, object i_Value, TKey i_ParentKey)
    {
      var data = t_Store.GetDataEnumerator<TProtocolModel>();

      var found = data.FirstOrDefault((r) => IsInParentScope(i_ParentKeyAccessor, r, i_ParentKey) && Matches(r, i_Value));
      if (found == null)
      {
        var instantiator = i_InstantiatorStore.GetInstantiatorFor<TProtocolModel>(i_Value);
        if (instantiator == null)
        {
          throw new InvalidOperationException($"Could not match for type {typeof(TProtocolModel).FullName} for value {i_Value}. Neither a fitting object was nor a fitting instantiator defined.");
        }

        var newInstance = instantiator.CreateForGeneric(i_Value);
        var parentInfo = new ProtocolParentInfo<TKey>()
        {
          MatchingInfo = null,
          ParentType = typeof(TProtocolModelParent),
          ParentKey = i_ParentKey
        };
        t_Store.Add(newInstance, parentInfo);
        return newInstance;
      }

      return found;
    }

    private static bool IsInParentScope(IParentKeyAccessor<TKey> i_Accessor, TProtocolModel i_Model, TKey i_ParentKey)
    {
      //no accessor provided -> check can not be done
      if (i_Accessor == null)
      {
        return true;
      }

      var parentKeyOfInstance = i_Accessor.GetParentKey(i_Model);
      return EqualityComparer<TKey>.Default.Equals(parentKeyOfInstance, i_ParentKey);
    }

    /// <summary>
    /// Method to be overriden that checks whether i_Model fits i_Value
    /// </summary>
    /// <param name="i_Model"></param>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    protected abstract bool Matches(TProtocolModel i_Model, object i_Value);
  }
}