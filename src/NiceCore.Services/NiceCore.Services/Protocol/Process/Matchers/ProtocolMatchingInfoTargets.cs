namespace NiceCore.Services.Protocol.Process.Matchers
{
  /// <summary>
  /// Targets to apply matching to
  /// </summary>
  public enum ProtocolMatchingInfoTargets
  {
    /// <summary>
    /// Match to entry contexts
    /// </summary>
    ActionEntryContexts = 0
  }
}