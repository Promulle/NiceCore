using System.Collections.Generic;
using NiceCore.Services.Protocol.Abstractions.ParentKeyAccessors;
using NiceCore.Services.Protocol.Abstractions.TypeProviders;
using NiceCore.Services.Protocol.Process.Actions;
using NiceCore.Services.Protocol.Process.Matchers;

namespace NiceCore.Services.Protocol.Process
{
  /// <summary>
  /// Interface for classes providing protocol specific implementations for a single protocol process 
  /// </summary>
  public interface IProtocolProcessDefinition<TKey>
  {
    /// <summary>
    /// Returns all matchers used for finding objects when not directly accessed
    /// </summary>
    /// <returns></returns>
    List<IProtocolRelatedMatcher<TKey>> GetMatchers();

    /// <summary>
    /// Returns all the instantiators that might be used by the matchers
    /// </summary>
    /// <returns></returns>
    List<INonGenericProtocolMatchedInstanceInstantiator> GetInstantiators();

    /// <summary>
    /// Returns the type providers to be used for this protocol process
    /// </summary>
    /// <returns></returns>
    ProtocolImplementingTypeProviders GetImplementingTypeProviders();

    /// <summary>
    /// Returns actions that should be performed on instances at certain points
    /// </summary>
    /// <returns></returns>
    IReadOnlyCollection<IProtocolProcessAction> GetProcessActions();
    
    /// <summary>
    /// Returns the parent key accessor to be used.
    /// If null, matching of instances will not be parent scoped.
    /// </summary>
    /// <returns></returns>
    IParentKeyAccessor<TKey> GetParentKeyAccessor();
  }
}