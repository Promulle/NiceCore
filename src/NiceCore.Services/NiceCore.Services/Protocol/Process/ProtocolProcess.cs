using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Base;
using NiceCore.Extensions;
using NiceCore.Services.Protocol.Model;
using NiceCore.Services.Protocol.Process.Actions;
using NiceCore.Services.Protocol.Process.Instructions;
using NiceCore.Services.Protocol.Process.Stores;

namespace NiceCore.Services.Protocol.Process
{
  /// <summary>
  /// Class realizing logic behind protocol construction for a single protocol
  /// </summary>
  public class ProtocolProcess<TKey> : BaseDisposable
  {
    private readonly BlockingCollection<IProtocolProcessInstruction<TKey>> m_Queue       = new();
    private readonly ProtocolProcessStore<TKey> m_Store;
    private readonly ProtocolProcessActionStore<TKey> m_ActionStore;
    private          Task                                                  m_QueueLoopTask;
    private          bool                                                  m_ProcessAlreadyFinished;

    /// <summary>
    /// Protocol Process
    /// </summary>
    /// <param name="i_Logger"></param>
    public ProtocolProcess(ILogger i_Logger)
    {
      m_Store = new(i_Logger);
      m_ActionStore = new(i_Logger);
    }
    
    /// <summary>
    /// Start a protocol according to i_Definition
    /// </summary>
    /// <param name="i_Definition"></param>
    /// <param name="i_Token"></param>
    /// <exception cref="InvalidOperationException"></exception>
    public void StartProtocol(IProtocolProcessDefinition<TKey> i_Definition, CancellationToken i_Token)
    {
      if (m_ProcessAlreadyFinished)
      {
        throw new InvalidOperationException("Process cannot start a protocol because it already finished one already.");
      }

      if (m_QueueLoopTask != null)
      {
        throw new InvalidOperationException("A message loop is already running, cannot start another protocol process.");
      }

      m_QueueLoopTask = Task.Run(async () =>
      {
        HandleDefinition(i_Definition);
        await m_Queue.BlockUntilComplete((inst) => OnNewConstructionInfoSupplied(inst), i_Token).ConfigureAwait(false);
      }, i_Token);
    }

    /// <summary>
    /// Get Instance for
    /// </summary>
    /// <param name="i_Key"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public T GetInstanceFor<T>(TKey i_Key)
      where T : IProtocolModel<TKey>
    {
      return (T)m_Store.GetInstanceFor(typeof(T), i_Key);
    }

    private void HandleDefinition(IProtocolProcessDefinition<TKey> i_Definition)
    {
      m_Store.InitializeForDefinition(i_Definition);
      m_ActionStore.InitializeFor(i_Definition.GetProcessActions());
    }

    /// <summary>
    /// Adds an info to be processed later
    /// </summary>
    /// <param name="i_Instruction"></param>
    public void AddInstruction(IProtocolProcessInstruction<TKey> i_Instruction)
    {
      m_Queue.Add(i_Instruction);
    }

    /// <summary>
    /// This waits for all construction infos to be processed and then completes a protocol instance based on that
    /// </summary>
    /// <returns></returns>
    public async Task<IProtocol<TKey>> WaitForFinish()
    {
      if (m_ProcessAlreadyFinished)
      {
        throw new InvalidOperationException("Process can not be finished because it was already finished");
      }

      m_Queue.CompleteAdding();
      await m_QueueLoopTask.ConfigureAwait(false);
      m_Queue.Dispose();
      m_ProcessAlreadyFinished = true;
      return m_Store.Protocol;
    }

    /// <summary>
    /// Marks the instance as completed 
    /// </summary>
    /// <param name="i_Type"></param>
    /// <param name="i_InstanceKey"></param>
    private async Task CompleteInstance(Type i_Type, TKey i_InstanceKey)
    {
      var instance = m_Store.GetInstanceFor(i_Type, i_InstanceKey);
      if (instance != null)
      {
        await m_ActionStore.RunCompletedFor(instance).ConfigureAwait(false);
      }
    }

    private Task OnNewConstructionInfoSupplied(IProtocolProcessInstruction<TKey> i_Info)
    {
      if (i_Info is ProtocolRelatedConstructionInstruction<TKey> creationInfo)
      {
        return OnCreationInstructionReceived(creationInfo);
      }

      if (i_Info is ProtocolRunCompletedActionsForInstructions<TKey> runInstruction)
      {
        return OnRunCompletedActions(runInstruction);
      }

      if (i_Info is ProtocolModelEditInstruction<TKey> editInstruction)
      {
        return OnEditInstance(editInstruction);
      }

      return Task.CompletedTask;
    }

    private Task OnEditInstance(ProtocolModelEditInstruction<TKey> i_Info)
    {
      var inst = m_Store.GetInstanceFor(i_Info.InstanceType, i_Info.Key);
      return i_Info.EditFunc(inst);
    }

    private Task OnRunCompletedActions(ProtocolRunCompletedActionsForInstructions<TKey> i_Info)
    {
      return CompleteInstance(i_Info.InstanceType, i_Info.Key);
    }

    private Task OnCreationInstructionReceived(ProtocolRelatedConstructionInstruction<TKey> i_Info)
    {
      var inst = i_Info.Builder.ToInstance();
      m_Store.Add(inst, i_Info.ParentInfo);
      return m_ActionStore.RunAddedFor(inst);
    }

    /// <summary>
    /// Dispose
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    protected override void Dispose(bool i_IsDisposing)
    {
      if (!m_ProcessAlreadyFinished)
      {
        m_Queue.Dispose();
      }

      base.Dispose(i_IsDisposing);
    }
  }
}
