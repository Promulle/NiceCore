using NiceCore.Services.Protocol.Abstractions.Builders;
using NiceCore.Services.Protocol.Abstractions.Progress;
using NiceCore.Services.Protocol.Process;

namespace NiceCore.Services.Protocol
{
  /// <summary>
  /// Service for functionality regarding protocol
  /// </summary>
  public interface IProtocolService
  {
    /// <summary>
    /// Creates and starts a new protocol process with customized values from i_Definition
    /// </summary>
    /// <param name="i_Definition"></param>
    /// <param name="i_Builder"></param>
    /// <typeparam name="TKey"></typeparam>
    /// <returns></returns>
    InProgressProtocol<TKey> CreateProtocol<TKey>(IProtocolProcessDefinition<TKey> i_Definition, IProtocolBuilder<TKey> i_Builder);
  }
}