using System;
using System.Threading.Tasks;
using NiceCore.Services.Protocol.Abstractions.TypeProviders;
using NiceCore.Services.Protocol.Model;
using NiceCore.Services.Protocol.Process;
using NiceCore.Services.Protocol.Process.Instructions;

namespace NiceCore.Services.Protocol.Abstractions.Progress
{
  /// <summary>
  /// Base class for "service" objects that abstract the things need to be done during a process that requires a protocol are running
  /// </summary>
  public abstract class BaseProtocolInProcess<TKey>
  {
    /// <summary>
    /// Process
    /// </summary>
    public ProtocolProcess<TKey> Process { get; init; }
    
    /// <summary>
    /// Result Type
    /// </summary>
    public Type ResultType { get; init; }
    
    /// <summary>
    /// Implementing type providers
    /// </summary>
    public ProtocolImplementingTypeProviders ProtocolImplementingTypeProviders { get; init; }
    
    /// <summary>
    /// Key of object that is abstracted by the in progress object
    /// </summary>
    public TKey InstanceKey { get; init; }

    /// <summary>
    /// Return the instance queried by the instance key of this inprogress object
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public T GetInstance<T>()
      where T: IProtocolModel<TKey>
    {
      return Process.GetInstanceFor<T>(InstanceKey);
    }
    
    /// <summary>
    /// Adds an instruction to run completion actions for the instance that is abstracted by this object
    /// </summary>
    public void Complete()
    {
      Process.AddInstruction(new ProtocolRunCompletedActionsForInstructions<TKey>()
      {
        Key = InstanceKey,
        InstanceType = ResultType
      });
    }

    /// <summary>
    /// Edit the instance that is this in process instance
    /// </summary>
    /// <param name="i_EditFunc"></param>
    /// <typeparam name="T"></typeparam>
    public void Edit<T>(Func<IProtocolModel<TKey>, Task> i_EditFunc) where T: IProtocolModel<T>
    {
      Process.AddInstruction(new ProtocolModelEditInstruction<TKey>()
      {
        Key = InstanceKey,
        InstanceType = typeof(T),
        EditFunc = i_EditFunc
      });
    }
  }
}