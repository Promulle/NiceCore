using NiceCore.Services.Protocol.Abstractions.Builders;
using NiceCore.Services.Protocol.Process.Instructions;

namespace NiceCore.Services.Protocol.Abstractions.Progress
{
  /// <summary>
  /// In progress action entry context
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  public class InProgressActionEntryContext<TKey> : BaseProtocolInProcess<TKey>
  {
    /// <summary>
    /// With entry
    /// </summary>
    /// <param name="i_Builder"></param>
    /// <returns></returns>
    public InProgressActionEntryContext<TKey> WithEntry(IProtocolActionEntryBuilder<TKey> i_Builder)
    {
      Process.AddInstruction(new ProtocolRelatedConstructionInstruction<TKey>()
      {
        Builder = i_Builder,
        ParentInfo = new()
        {
          ParentType = ProtocolImplementingTypeProviders.ProtocolActionEntryContextImplementingTypeProvider.GetImplementingType(),
          ParentKey = InstanceKey,
        },
        ProtocolRelatedType = i_Builder.GetResultType()
      });
      return this;
    }
  }
}