using System.Threading.Tasks;
using NiceCore.Services.Protocol.Abstractions.Builders;
using NiceCore.Services.Protocol.Abstractions.TypeProviders;
using NiceCore.Services.Protocol.Model;
using NiceCore.Services.Protocol.Process;
using NiceCore.Services.Protocol.Process.Instructions;

namespace NiceCore.Services.Protocol.Abstractions.Progress
{
  /// <summary>
  /// In Progress Protocol
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  public class InProgressProtocol<TKey> : BaseProtocolInProcess<TKey>
  {
    /// <summary>
    /// Ctor filling initial fields for protocol
    /// </summary>
    /// <param name="i_ProtocolBuilder"></param>
    /// <param name="i_TypeProviders"></param>
    /// <param name="i_Process"></param>
    public InProgressProtocol(IProtocolBuilder<TKey> i_ProtocolBuilder, ProtocolImplementingTypeProviders i_TypeProviders, ProtocolProcess<TKey> i_Process)
    {
      Process = i_Process;
      InstanceKey = i_ProtocolBuilder.InstanceKey;
      ResultType = i_ProtocolBuilder.GetResultType();
      ProtocolImplementingTypeProviders = i_TypeProviders;
      Process.AddInstruction(new ProtocolRelatedConstructionInstruction<TKey>()
      {
        Builder = i_ProtocolBuilder,
        ParentInfo = new(),
        ProtocolRelatedType = i_ProtocolBuilder.GetResultType()
      });
    }
    
    /// <summary>
    /// Adds an action
    /// </summary>
    /// <param name="i_Builder"></param>
    /// <returns></returns>
    public InProgressAction<TKey> AddAction(IProtocolActionBuilder<TKey> i_Builder)
    {
      Process.AddInstruction(new ProtocolRelatedConstructionInstruction<TKey>()
      {
        Builder = i_Builder,
        ParentInfo = new()
        {
          ParentType = ProtocolImplementingTypeProviders.ProtocolImplementingTypeProvider.GetImplementingType(),
          ParentKey = InstanceKey,
        },
        ProtocolRelatedType = i_Builder.GetResultType()
      });
      return new()
      {
        Process = Process,
        ResultType = i_Builder.GetResultType(),
        ProtocolImplementingTypeProviders = ProtocolImplementingTypeProviders,
        InstanceKey = i_Builder.InstanceKey
      };
    }

    /// <summary>
    /// Returns the completed instance
    /// </summary>
    /// <returns></returns>
    public Task<IProtocol<TKey>> GetCompletedProtocol()
    {
      Complete();
      return Process.WaitForFinish();
    }
  }
}