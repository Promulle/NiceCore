using NiceCore.Services.Protocol.Abstractions.Builders;
using NiceCore.Services.Protocol.Process.Instructions;
using NiceCore.Services.Protocol.Process.Matchers;

namespace NiceCore.Services.Protocol.Abstractions.Progress
{
  /// <summary>
  /// In Progress Action
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  public class InProgressAction<TKey> : BaseProtocolInProcess<TKey>
  {
    /// <summary>
    /// Add a context
    /// </summary>
    /// <param name="i_Builder"></param>
    /// <returns></returns>
    public InProgressActionEntryContext<TKey> WithEntryContext(IProtocolActionEntryContextBuilder<TKey> i_Builder)
    {
      var info = new ProtocolRelatedConstructionInstruction<TKey>()
      {
        Builder = i_Builder,
        ParentInfo = new()
        {
          ParentKey = InstanceKey,
          ParentType = ProtocolImplementingTypeProviders.ProtocolActionImplementingTypeProvider.GetImplementingType()
        },
        ProtocolRelatedType = i_Builder.GetResultType()
      };
      Process.AddInstruction(info);
      return new()
      {
        Process = Process,
        ResultType = i_Builder.GetResultType(),
        ProtocolImplementingTypeProviders = ProtocolImplementingTypeProviders,
        InstanceKey = i_Builder.InstanceKey
      };
    }

    /// <summary>
    /// With Entry
    /// </summary>
    /// <param name="i_ContextValue"></param>
    /// <param name="i_EntryBuilder"></param>
    /// <returns></returns>
    public InProgressAction<TKey> WithEntry(object i_ContextValue, IProtocolActionEntryBuilder<TKey> i_EntryBuilder)
    {
      var info = new ProtocolRelatedConstructionInstruction<TKey>()
      {
        Builder = i_EntryBuilder,
        ParentInfo = new()
        {
          ParentKey = InstanceKey,
          ParentType = ProtocolImplementingTypeProviders.ProtocolActionEntryContextImplementingTypeProvider.GetImplementingType(),
          MatchingInfo =  new()
          {
            Target = ProtocolMatchingInfoTargets.ActionEntryContexts,
            Value = i_ContextValue
          }
        },
        ProtocolRelatedType = i_EntryBuilder.GetResultType()
      };
      Process.AddInstruction(info);
      return this;
    }
  }
}