using System;

namespace NiceCore.Services.Protocol.Abstractions.TypeProviders
{
  /// <summary>
  /// Protocol Implementing Type Provider
  /// </summary>
  public interface IProtocolImplementingTypeProvider
  {
    /// <summary>
    /// Returns the type that is used to implement the concept
    /// </summary>
    /// <returns></returns>
    Type GetImplementingType();
  }
}