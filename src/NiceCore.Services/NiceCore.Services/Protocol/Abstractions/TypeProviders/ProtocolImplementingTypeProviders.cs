namespace NiceCore.Services.Protocol.Abstractions.TypeProviders
{
  /// <summary>
  /// Class that contains the instances for the implementing type providers
  /// </summary>
  public class ProtocolImplementingTypeProviders
  {
    /// <summary>
    /// Implementing Type Provider for Protocol
    /// </summary>
    public IProtocolImplementingTypeProvider ProtocolImplementingTypeProvider { get; init; }
    
    /// <summary>
    /// Implementing Type Provider for Protocol
    /// </summary>
    public IProtocolImplementingTypeProvider ProtocolActionImplementingTypeProvider { get; init; }
    
    /// <summary>
    /// Implementing Type Provider for Protocol
    /// </summary>
    public IProtocolImplementingTypeProvider ProtocolActionEntryContextImplementingTypeProvider { get; init; }
    
    /// <summary>
    /// Implementing Type Provider for Protocol
    /// </summary>
    public IProtocolImplementingTypeProvider ProtocolActionEntryImplementingTypeProvider { get; init; }
  }
}