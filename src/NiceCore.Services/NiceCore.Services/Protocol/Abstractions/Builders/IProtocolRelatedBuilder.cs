using System;
using NiceCore.Services.Protocol.Model;

namespace NiceCore.Services.Protocol.Abstractions.Builders
{
  /// <summary>
  /// Interface for all Protocol related builders 
  /// </summary>
  public interface IProtocolRelatedBuilder<TKey>
  {
    /// <summary>
    /// Key for instance to be used
    /// </summary>
    TKey InstanceKey { get; }
    
    /// <summary>
    /// Returns the type that is the result of the builder
    /// </summary>
    /// <returns></returns>
    Type GetResultType();
    
    /// <summary>
    /// Creates an instance from this builder
    /// </summary>
    /// <returns></returns>
    IProtocolModel<TKey> ToInstance();
  }
}