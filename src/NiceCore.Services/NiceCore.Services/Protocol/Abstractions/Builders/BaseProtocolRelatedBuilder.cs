using System;
using NiceCore.Services.Protocol.Model;

namespace NiceCore.Services.Protocol.Abstractions.Builders
{
  /// <summary>
  /// Base impl for builders
  /// </summary>
  public abstract class BaseProtocolRelatedBuilder<TKey> : IProtocolRelatedBuilder<TKey>
  {
    /// <summary>
    /// Instance Key
    /// </summary>
    public TKey InstanceKey { get; protected set; }

    /// <summary>
    /// Result type
    /// </summary>
    /// <returns></returns>
    public abstract Type GetResultType();

    /// <summary>
    /// To Instance
    /// </summary>
    /// <returns></returns>
    public abstract IProtocolModel<TKey> ToInstance();
  }
}