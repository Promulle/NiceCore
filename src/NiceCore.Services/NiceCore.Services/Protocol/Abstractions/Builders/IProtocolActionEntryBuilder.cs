using NiceCore.Services.Protocol.Model;

namespace NiceCore.Services.Protocol.Abstractions.Builders
{
  /// <summary>
  /// Protocol entry builder
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  public interface IProtocolActionEntryBuilder<TKey> : IProtocolRelatedBuilder<TKey>
  {
    /// <summary>
    /// Converts the values supplied to the builder to an entry
    /// </summary>
    /// <returns></returns>
    IProtocolActionEntry<TKey> ToEntry();
  }
}