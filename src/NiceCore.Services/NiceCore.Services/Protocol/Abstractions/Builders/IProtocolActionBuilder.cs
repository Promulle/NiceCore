using NiceCore.Services.Protocol.Model;

namespace NiceCore.Services.Protocol.Abstractions.Builders
{
  /// <summary>
  /// Protocol action builder
  /// </summary>
  public interface IProtocolActionBuilder<TKey> : IProtocolRelatedBuilder<TKey>
  {
    /// <summary>
    /// Constructs an action from the values supplied to the builder
    /// </summary>
    /// <returns></returns>
    IProtocolAction<TKey> ToAction();
  }
}