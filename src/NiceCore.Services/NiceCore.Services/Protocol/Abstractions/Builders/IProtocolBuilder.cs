using System;
using NiceCore.Services.Protocol.Model;

namespace NiceCore.Services.Protocol.Abstractions.Builders
{
  /// <summary>
  /// Builder that is used
  /// </summary>
  public interface IProtocolBuilder<TKey> : IProtocolRelatedBuilder<TKey>
  {
    /// <summary>
    /// Constructs a protocol instance from the values supplied to the builder
    /// </summary>
    /// <returns></returns>
    IProtocol<TKey> ToProtocol();
  }
}