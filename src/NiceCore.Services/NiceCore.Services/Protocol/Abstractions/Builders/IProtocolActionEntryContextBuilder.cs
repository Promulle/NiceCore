using NiceCore.Services.Protocol.Model;

namespace NiceCore.Services.Protocol.Abstractions.Builders
{
  /// <summary>
  /// Protocol entry context builder
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  public interface IProtocolActionEntryContextBuilder<TKey>: IProtocolRelatedBuilder<TKey>
  {
    /// <summary>
    /// Constructs an entry context from the values supplied to the builder
    /// </summary>
    /// <returns></returns>
    IProtocolActionEntryContext<TKey> ToEntryContext();
  }
}