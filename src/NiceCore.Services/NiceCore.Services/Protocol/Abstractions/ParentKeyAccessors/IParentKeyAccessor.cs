using NiceCore.Services.Protocol.Model;

namespace NiceCore.Services.Protocol.Abstractions.ParentKeyAccessors
{
  /// <summary>
  /// Interface for parent key accesor, a flexible way to get parent keys for entities
  /// </summary>
  public interface IParentKeyAccessor<TKey>
  {
    /// <summary>
    /// Retrieves the key of the parent for i_Instance.
    /// Since this does not make any assumption about what type of key is returned, this should return default if
    /// no key was found.
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <typeparam name="TProtocolModel"></typeparam>
    /// <returns></returns>
    TKey GetParentKey<TProtocolModel>(TProtocolModel i_Instance) where TProtocolModel: IProtocolModel<TKey>;
  }
}