using NiceCore.Base;
using Polly;

namespace NiceCore.Services.Resilience.Config
{
  /// <summary>
  /// Retry Strategy Configuration Model
  /// </summary>
  public class RetryStrategyConfigurationModel
  {
    /// <summary>
    /// Enabled
    /// </summary>
    public bool Enabled { get; set; }
    
    /// <summary>
    /// Delay
    /// </summary>
    public ConfiguredTimeInterval Delay { get; set; }
    
    /// <summary>
    /// Backoff Type
    /// </summary>
    public DelayBackoffType BackoffType { get; set; } = DelayBackoffType.Constant;
    
    /// <summary>
    /// Max Delay
    /// </summary>
    public ConfiguredTimeInterval MaxDelay { get; set; }
    
    /// <summary>
    /// Max Retry Attempts
    /// </summary>
    public int? MaxRetryAttempts { get; set; }

    /// <summary>
    /// Use Jitter
    /// </summary>
    public bool UseJitter { get; set; }
  }
}