using System;
using System.Threading.Tasks;
using Polly;
using Polly.Retry;

namespace NiceCore.Services.Resilience
{
  /// <summary>
  /// Policy Service
  /// </summary>
  public interface IConfiguredPolicyService
  {
    /// <summary>
    /// Create From Configuration
    /// </summary>
    /// <param name="i_Key"></param>
    /// <param name="i_ShouldHandleFunc"></param>
    /// <param name="i_OnRetryFunc"></param>
    /// <returns></returns>
    IAsyncPolicy CreateFromConfiguration(string i_Key, 
      Func<RetryPredicateArguments<object>,ValueTask<bool>> i_ShouldHandleFunc,
      Func<OnRetryArguments<object>, ValueTask> i_OnRetryFunc);
  }
}