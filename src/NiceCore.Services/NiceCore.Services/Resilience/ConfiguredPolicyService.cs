using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using NiceCore.Services.Resilience.Config;
using Polly;
using Polly.Retry;

namespace NiceCore.Services.Resilience
{
  /// <summary>
  /// Policy Service
  /// </summary>
  [Register(InterfaceType = typeof(IConfiguredPolicyService))]
  public class ConfiguredPolicyService : IConfiguredPolicyService
  {
    /// <summary>
    /// Configuration
    /// </summary>
    public INcConfiguration Configuration { get; set; }

    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<ConfiguredPolicyService> Logger { get; set; }

    /// <summary>
    /// Create From Configuration
    /// </summary>
    /// <param name="i_Key"></param>
    /// <param name="i_ShouldHandleFunc"></param>
    /// <param name="i_OnRetryFunc"></param>
    /// <returns></returns>
    public IAsyncPolicy CreateFromConfiguration(string i_Key,
      Func<RetryPredicateArguments<object>, ValueTask<bool>> i_ShouldHandleFunc,
      Func<OnRetryArguments<object>, ValueTask> i_OnRetryFunc)
    {
      var retryStrategy = BuildStrategyFromConfiguration(i_Key, i_ShouldHandleFunc, i_OnRetryFunc);
      if (retryStrategy == null)
      {
        return null;
      }

      return new ResiliencePipelineBuilder().AddRetry(retryStrategy).Build().AsAsyncPolicy();
    }

    private RetryStrategyOptions BuildStrategyFromConfiguration(string i_Key,
      Func<RetryPredicateArguments<object>, ValueTask<bool>> i_ShouldHandleFunc,
      Func<OnRetryArguments<object>, ValueTask> i_OnRetryFunc)
    {
      var retrySettings = Configuration.GetValue<RetryStrategyConfigurationModel>(i_Key, null);
      if (retrySettings == null || retrySettings.Enabled == false)
      {
        return null;
      }

      if (retrySettings.Delay == null)
      {
        Logger.Error($"Configuration of retry policy for key {i_Key} was invalid: {nameof(retrySettings.Delay)} must be set");
        return null;
      }

      if (retrySettings.MaxRetryAttempts == null || retrySettings.MaxRetryAttempts < 1)
      {
        Logger.Error($"Configuration of retry policy for key {i_Key} was invalid: {nameof(retrySettings.MaxRetryAttempts)} must be set to a number bigger than zero");
        return null;
      }
      
      if (retrySettings.MaxDelay == null)
      {
        Logger.Error($"Configuration of retry policy for key {i_Key} was invalid: Non-Constant Backoff-Type {retrySettings.BackoffType} was chosen but {retrySettings.MaxDelay} was set.");
        return null;
      }

      return new RetryStrategyOptions()
      {
        BackoffType = retrySettings.BackoffType,
        ShouldHandle = i_ShouldHandleFunc,
        OnRetry = i_OnRetryFunc,
        Delay = retrySettings.Delay.ToTimeSpan(),
        MaxRetryAttempts = retrySettings.MaxRetryAttempts.Value,
        MaxDelay = retrySettings.MaxDelay?.ToTimeSpan(),
      };
    }
  }
}