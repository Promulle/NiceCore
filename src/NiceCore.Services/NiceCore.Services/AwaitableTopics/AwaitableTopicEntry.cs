using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NiceCore.Services.AwaitableTopics
{
  /// <summary>
  /// Awaitable Topic Entry
  /// </summary>
  public class AwaitableTopicEntry
  {
    /// <summary>
    /// Topic Instances.
    /// </summary>
    public required HashSet<Guid> TopicInstances { get; init; }
    
    /// <summary>
    /// Topic Name
    /// </summary>
    public required string TopicName { get; init; }
    
    /// <summary>
    /// Task Source
    /// </summary>
    public required TaskCompletionSource TaskSource { get; init; }
  }
}
