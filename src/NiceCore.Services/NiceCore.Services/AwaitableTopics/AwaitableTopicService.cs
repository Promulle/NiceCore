using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using NiceCore.ServiceLocation;

namespace NiceCore.Services.AwaitableTopics
{
  /// <summary>
  /// Awaitable Topic Service
  /// </summary>
  [Register(InterfaceType = typeof(IAwaitableTopicService), LifeStyle = LifeStyles.Singleton)]
  public class AwaitableTopicService : IAwaitableTopicService
  {
    private readonly SemaphoreSlim m_Lock = new SemaphoreSlim(1, 1);
    private readonly Dictionary<string, AwaitableTopicEntry> m_Entries = new();

    /// <summary>
    /// Starts a new Instance for the topic of i_Name
    /// </summary>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    public async ValueTask<StartedAwaitableTopic> Start(string i_Name)
    {
      await m_Lock.WaitAsync().ConfigureAwait(false);
      try
      {
        if (!m_Entries.TryGetValue(i_Name, out var entry))
        {
          entry = new()
          {
            TaskSource = new(TaskCreationOptions.RunContinuationsAsynchronously),
            TopicName = i_Name,
            TopicInstances = new()
          };
          m_Entries[i_Name] = entry;
        }

        var topic = new StartedAwaitableTopic(i_Name, Guid.NewGuid(), this);
        entry.TopicInstances.Add(topic.AwaitableTopicInstanceID);
        return topic;
      }
      finally
      {
        m_Lock.Release();
      }
    }

    /// <summary>
    /// Remove an instance. This also sets the task to be completed if this was the last instance to be completed
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_TopicInstance"></param>
    public async ValueTask Remove(string i_Name, Guid i_TopicInstance)
    {
      await m_Lock.WaitAsync().ConfigureAwait(false);
      try
      {
        if (m_Entries.TryGetValue(i_Name, out var entry))
        {
          entry.TopicInstances.Remove(i_TopicInstance);
          if (entry.TopicInstances.Count == 0)
          {
            m_Entries.Remove(i_Name);
            entry.TaskSource.SetResult();
          }
        }
      }
      finally
      {
        m_Lock.Release();
      }
    }

    /// <summary>
    /// Wait for the completion of the topic with i_Name
    /// </summary>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    public Task WaitForTopicCompletion(string i_Name)
    {
      return WaitForTopicCompletion(new[] {i_Name});
    }

    /// <summary>
    /// Wait for the completion of the topic with i_Name
    /// </summary>
    /// <param name="i_Names"></param>
    /// <returns></returns>
    public async Task WaitForTopicCompletion(IReadOnlyCollection<string> i_Names)
    {
      List<Task> tasks = new List<Task>(i_Names.Count);
      await m_Lock.WaitAsync().ConfigureAwait(false);
      try
      {
        foreach (var topic in i_Names)
        {
          if (m_Entries.TryGetValue(topic, out var entry))
          {
            tasks.Add(entry.TaskSource.Task);
          }
        }
        
      }
      finally
      {
        m_Lock.Release();
      }

      if (tasks.Count > 0)
      {
        await Task.WhenAll(tasks).ConfigureAwait(false);
      }
    }
  }
}