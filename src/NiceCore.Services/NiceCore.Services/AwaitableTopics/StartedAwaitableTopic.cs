using System;
using System.Threading.Tasks;

namespace NiceCore.Services.AwaitableTopics
{
  /// <summary>
  /// Started Awaitable Topic
  /// </summary>
  public class StartedAwaitableTopic : IAsyncDisposable
  {
    private readonly IAwaitableTopicService m_Service;
    
    /// <summary>
    /// Topic Instance ID
    /// </summary>
    public Guid AwaitableTopicInstanceID { get; }
    
    /// <summary>
    /// Topic Name
    /// </summary>
    public string TopicName { get; }

    /// <summary>
    /// Ctor filling the fields
    /// </summary>
    /// <param name="i_TopicName"></param>
    /// <param name="i_TopicInstanceID"></param>
    /// <param name="i_Service"></param>
    public StartedAwaitableTopic(string i_TopicName, Guid i_TopicInstanceID, IAwaitableTopicService i_Service)
    {
      m_Service = i_Service;
      TopicName = i_TopicName;
      AwaitableTopicInstanceID = i_TopicInstanceID;
    }

    /// <summary>
    /// Dispose async
    /// </summary>
    public async ValueTask DisposeAsync()
    {
      await m_Service.Remove(TopicName, AwaitableTopicInstanceID).ConfigureAwait(false);
    }
  }
}