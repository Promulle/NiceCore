using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NiceCore.Services.AwaitableTopics
{
  /// <summary>
  /// Awaitable Topic Service
  /// </summary>
  public interface IAwaitableTopicService
  {
    /// <summary>
    /// Starts a new Instance for the topic of i_Name
    /// </summary>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    ValueTask<StartedAwaitableTopic> Start(string i_Name);

    /// <summary>
    /// Remove an instance. This also sets the task to be completed if this was the last instance to be completed
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_TopicInstance"></param>
    ValueTask Remove(string i_Name, Guid i_TopicInstance);

    /// <summary>
    /// Wait for the completion of the topic with i_Name
    /// </summary>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    Task WaitForTopicCompletion(string i_Name);

    /// <summary>
    /// Wait for the completion of the topic with i_Name
    /// </summary>
    /// <param name="i_Names"></param>
    /// <returns></returns>
    Task WaitForTopicCompletion(IReadOnlyCollection<string> i_Names);
  }
}
