using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Base.Services;
using NiceCore.Logging.Extensions;
using NiceCore.Services.Config;

namespace NiceCore.Services.Encryption.Providers
{
  /// <summary>
  /// Base Class for ConfigurableEncryption Provider
  /// </summary>
  public abstract class BaseConfigurableEncryptionProvider<TConfigModel> : BaseService, INcEncryptionProvider
  {
    private readonly SemaphoreSlim m_Lock = new(1, 1);
    private IAsyncReloadableSetting<TConfigModel> m_ProviderSetting;

    /// <summary>
    /// Name
    /// </summary>
    public abstract string Name { get; }
    
    /// <summary>
    /// Logger
    /// </summary>
    public abstract ILogger Logger { get; }

    /// <summary>
    /// Configuration
    /// </summary>
    public INcConfiguration Configuration { get; set; }

    /// <summary>
    /// Initialize Setting If Needed
    /// </summary>
    protected async ValueTask InitializeSettingIfNeeded()
    {
      await m_Lock.WaitAsync().ConfigureAwait(false);
      try
      {
        if (m_ProviderSetting == null)
        {
          var key = "NiceCore.Encryption.Providers." + Name;
          m_ProviderSetting = await Configuration.GetSetting<TConfigModel>(key, default).ConfigureAwait(false);
          await m_ProviderSetting.AddListener(InitializeBySettings, true).ConfigureAwait(false);
        }
      }
      catch (Exception e)
      {
        Logger.Error(e, $"An Error occurred during setup of reloadable setting for encryption provider: {GetType().FullName}");
      }
      finally
      {
        m_Lock.Release();
      }
      
    }

    /// <summary>
    /// Initialize by settings. i_Model is loaded from Configuration. This method is NOT Called if i_Model is null. This might be called multiple times
    /// </summary>
    /// <param name="i_Model"></param>
    /// <returns></returns>
    protected abstract ValueTask InitializeBySettings(SettingReloadedEventArgs<TConfigModel> i_Model);

    /// <summary>
    /// Encrypt
    /// </summary>
    /// <param name="i_InputData"></param>
    /// <returns></returns>
    public abstract ValueTask<byte[]> Encrypt(byte[] i_InputData);
    
    /// <summary>
    /// Decrypt
    /// </summary>
    /// <param name="i_InputData"></param>
    /// <returns></returns>
    public abstract ValueTask<byte[]> Decrypt(byte[] i_InputData);
  }
}