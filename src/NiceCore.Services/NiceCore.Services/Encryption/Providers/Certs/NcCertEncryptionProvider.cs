using System;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Services.Encryption.Providers.Certs
{
  /// <summary>
  /// Nc Cert Encryption Provider
  /// </summary>
  [InitializeOnResolve]
  [Register(InterfaceType = typeof(INcEncryptionProvider))]
  public sealed class NcCertEncryptionProvider : BaseConfigurableEncryptionProvider<NcCertEncryptionProviderSettingModel>, INcEncryptionProvider
  {
    /// <summary>
    /// Path to the certificate used for encryption/decryption
    /// </summary>
    private string m_CertPath;

    /// <summary>
    /// Actual Logger
    /// </summary>
    public ILogger<NcCertEncryptionProvider> ActualLogger { get; set; }

    /// <summary>
    /// Logger
    /// </summary>
    public override ILogger Logger
    {
      get { return ActualLogger; }
    }

    /// <summary>
    /// NiceCore cert Provider
    /// </summary>
    public override string Name { get; } = NcEncryptionProviders.CERT_PROVIDER;

    /// <summary>
    /// Sets the certificate path
    /// </summary>
    /// <param name="i_CertificatePath"></param>
    public void SetCertificatePath(string i_CertificatePath)
    {
      m_CertPath = i_CertificatePath;
    }
    
    /// <summary>
    /// Initialize by settings
    /// </summary>
    /// <param name="i_Model"></param>
    /// <returns></returns>
    protected override ValueTask InitializeBySettings(SettingReloadedEventArgs<NcCertEncryptionProviderSettingModel> i_Model)
    {
      if (i_Model.Value?.CertificateFilePath != null)
      {
        m_CertPath = i_Model.Value.CertificateFilePath;  
      }
      else
      {
        m_CertPath = null;
        ActualLogger.Warning($"Setting for NcCertEncryptionProvider was reloaded but {nameof(i_Model.Value.CertificateFilePath)} was null. Cert set to null. Calls to Encrypt/Decrypt will fail in this state.");
      }
      return ValueTask.CompletedTask;
    }

    /// <summary>
    /// Encrypts i_InputData
    /// </summary>
    /// <param name="i_InputData"></param>
    /// <returns></returns>
    /// <exception cref="InvalidOperationException"></exception>
    /// <exception cref="FileNotFoundException"></exception>
    public override async ValueTask<byte[]> Encrypt(byte[] i_InputData)
    {
      await InitializeSettingIfNeeded().ConfigureAwait(false);
      if (m_CertPath == null)
      {
        throw new InvalidOperationException($"Cannot encrypt data with {typeof(NcCertEncryptionProvider)} because certificate to use was not set.");
      }
      if (!File.Exists(m_CertPath))
      {
        throw new FileNotFoundException($"Can not load Certificate for encryption in {typeof(NcCertEncryptionProvider)}. Given certificate path does not correspond to an existing file.");
      }

      var cert = await LoadConfiguredCertificate().ConfigureAwait(false);
      try
      {
        return Encrypt(cert, i_InputData);
      }
      catch (Exception e)
      {
        Logger.Error(e, "Encryption of data failed.");
        throw;
      }
    }

    /// <summary>
    /// Decrypts i_InputData
    /// </summary>
    /// <param name="i_InputData"></param>
    /// <returns></returns>
    /// <exception cref="InvalidOperationException"></exception>
    /// <exception cref="FileNotFoundException"></exception>
    public override async ValueTask<byte[]> Decrypt(byte[] i_InputData)
    {
      await InitializeSettingIfNeeded().ConfigureAwait(false);
      if (m_CertPath == null)
      {
        throw new InvalidOperationException($"Cannot decrypt data with {typeof(NcCertEncryptionProvider)} because certificate to use was not set.");
      }

      if (!File.Exists(m_CertPath))
      {
        throw new FileNotFoundException($"Can not load Certificate for decryption in {typeof(NcCertEncryptionProvider)}. Given certificate path does not correspond to an existing file.");
      }
      
      var cert = await LoadConfiguredCertificate().ConfigureAwait(false);
      try
      {
        return Decrypt(cert, i_InputData);
      }
      catch (Exception e)
      {
        Logger.Error(e, "Decryption of data failed.");
        throw;
      }
    }

    private async Task<X509Certificate2> LoadConfiguredCertificate()
    {
      try
      {
        var data = await File.ReadAllBytesAsync(m_CertPath).ConfigureAwait(false);
        return new X509Certificate2(data);
      }
      catch (Exception e)
      {
        ActualLogger.Error(e, "An Error occurred during load of certificate. Cannot procede with encryption/decryption.");
        throw;
      }
    }
    
    private static byte[] Encrypt(X509Certificate2 i_Cert, byte[] i_Data)
    {
      var cms = new EnvelopedCms(new ContentInfo(i_Data));
      cms.Encrypt(new CmsRecipient(i_Cert));
      return cms.Encode();
    }

    private static byte[] Decrypt(X509Certificate2 i_Cert, byte[] i_Data)
    {
      var cms = new EnvelopedCms();
      cms.Decode(i_Data);
      cms.Decrypt(new X509Certificate2Collection(i_Cert));
      return cms.ContentInfo.Content;
    }

  }
}