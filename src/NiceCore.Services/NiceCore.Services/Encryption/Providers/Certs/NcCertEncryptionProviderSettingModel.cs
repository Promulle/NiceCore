namespace NiceCore.Services.Encryption.Providers.Certs
{
  /// <summary>
  /// Setting Model for NcCertEncryption Provider
  /// </summary>
  public sealed class NcCertEncryptionProviderSettingModel
  {
    /// <summary>
    /// Path to certificate
    /// </summary>
    public string CertificateFilePath { get; set; }
  }
}