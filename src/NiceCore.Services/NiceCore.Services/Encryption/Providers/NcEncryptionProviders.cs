namespace NiceCore.Services.Encryption.Providers
{
  /// <summary>
  /// Nc Encryption Providers
  /// </summary>
  public static class NcEncryptionProviders
  {
    /// <summary>
    /// CERT based encryption provider
    /// </summary>
    public const string CERT_PROVIDER = "NcCertProvider";
  }
}