using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Base.Services;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation;

namespace NiceCore.Services.Encryption
{
  /// <summary>
  /// Service that enables encryption/decryption using registered providers
  /// </summary>
  [InitializeOnResolve]
  [Register(InterfaceType = typeof(INcEncryptionService), LifeStyle = LifeStyles.Singleton)]
  public class NcEncryptionService : BaseService, INcEncryptionService
  {
    private readonly Dictionary<string, INcEncryptionProvider> m_Providers = new();
    
    /// <summary>
    /// Available Providers
    /// </summary>
    public IList<INcEncryptionProvider> AvailableProviders { get; set; }

    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<NcEncryptionService> Logger { get; set; }

    /// <summary>
    /// Internal Initialize
    /// </summary>
    protected override void InternalInitialize()
    {
      if (AvailableProviders != null && AvailableProviders.Count > 0)
      {
        foreach (var provider in AvailableProviders)
        {
          Logger.Debug($"Registering Provider {provider.Name} of type {provider.GetType().FullName}");
          m_Providers[provider.Name] = provider;
        }  
      }
      else
      {
        Logger.Warning($"No Provider for type {typeof(INcEncryptionProvider)} was registered. Calls to {nameof(EncryptWith)} or {nameof(DecryptWith)} will fail.");
      }
    }

    /// <summary>
    /// Encrypts i_InputData with the provider that was registered for i_ProviderName
    /// </summary>
    /// <param name="i_InputData"></param>
    /// <param name="i_ProviderName"></param>
    /// <returns></returns>
    public ValueTask<byte[]> EncryptWith(byte[] i_InputData, string i_ProviderName)
    {
      if (m_Providers.TryGetValue(i_ProviderName, out var provider))
      {
        try
        {
          return provider.Encrypt(i_InputData);
        }
        catch (Exception e)
        {
          Logger.Error(e, $"Could not encrypt data with {i_ProviderName}, an error occurred during execution");
          throw;
        }
      }
      throw new NotSupportedException($"Encrypting data with provider {i_ProviderName} is not supported. No provider for name was registered.");
    }

    /// <summary>
    /// Decrypt with
    /// </summary>
    /// <param name="i_InputData"></param>
    /// <param name="i_ProviderName"></param>
    /// <returns></returns>
    public ValueTask<byte[]> DecryptWith(byte[] i_InputData, string i_ProviderName)
    {
      if (m_Providers.TryGetValue(i_ProviderName, out var provider))
      {
        try
        {
          return provider.Decrypt(i_InputData);
        }
        catch (Exception e)
        {
          Logger.Error(e, $"Could not decrypt data with {i_ProviderName}, an error occurred during execution");
          throw;
        }
      }
      throw new NotSupportedException($"Decrypting data with provider {i_ProviderName} is not supported. No provider for name was registered.");
    }
  }
}