using System.Threading.Tasks;

namespace NiceCore.Services.Encryption
{
  /// <summary>
  /// Encryption Provider
  /// </summary>
  public interface INcEncryptionProvider
  {
    /// <summary>
    /// Name of provider. This is the name this provider will be referenced by most of the time
    /// </summary>
    string Name { get; }

    /// <summary>
    /// Encrypts i_InputData
    /// </summary>
    /// <param name="i_InputData"></param>
    /// <returns></returns>
    ValueTask<byte[]> Encrypt(byte[] i_InputData);

    /// <summary>
    /// Decrypts i_InputData
    /// </summary>
    /// <param name="i_InputData"></param>
    /// <returns></returns>
    ValueTask<byte[]> Decrypt(byte[] i_InputData);
  }
}