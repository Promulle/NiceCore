using System.Threading.Tasks;

namespace NiceCore.Services.Encryption
{
  /// <summary>
  /// Service that enables encryption/decryption using registered providers
  /// </summary>
  public interface INcEncryptionService
  {
    /// <summary>
    /// Encrypts i_InputData with the provider that was registered for i_ProviderName
    /// </summary>
    /// <param name="i_InputData"></param>
    /// <param name="i_ProviderName"></param>
    /// <returns></returns>
    public ValueTask<byte[]> EncryptWith(byte[] i_InputData, string i_ProviderName);

    /// <summary>
    /// Decrypt with
    /// </summary>
    /// <param name="i_InputData"></param>
    /// <param name="i_ProviderName"></param>
    /// <returns></returns>
    public ValueTask<byte[]> DecryptWith(byte[] i_InputData, string i_ProviderName);
  }
}