using NiceCore.Eventing.Messaging.Impl;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using NiceCore.Services.Eventing.Configs;

namespace NiceCore.Services.Eventing
{
  /// <summary>
  /// Configurable Message Service Send Mode Provider
  /// </summary>
  [Register(InterfaceType = typeof(IMessageServiceSendModeProvider))]
  public class ConfigurableMessageServiceSendModeProvider : IMessageServiceSendModeProvider
  {
    /// <summary>
    /// Configuration
    /// </summary>
    public INcConfiguration Configuration { get; set; }
    
    /// <summary>
    /// Get Send Mode
    /// </summary>
    /// <returns></returns>
    public MessageServiceSendModes GetSendMode()
    {
      if (Configuration == null)
      {
        return MessageServiceSendModes.ForceAsync;
      }

      return Configuration.GetValue(MessageServiceSendModeSetting.KEY, MessageServiceSendModes.ForceAsync);
    }
  }
}