﻿using NiceCore.Eventing;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using NiceCore.Services.Config.Settings.Messages;

namespace NiceCore.Services.Eventing
{
  /// <summary>
  /// Configuration Handler Config Provider
  /// </summary>
  [Register(InterfaceType = typeof(IHandlerConfigProvider))]
  public class ConfigurationHandlerConfigProvider : IHandlerConfigProvider
  {
    /// <summary>
    /// Configuration
    /// </summary>
    public INcConfiguration Configuration { get; set; }
    
    /// <summary>
    /// Get Config For Handler
    /// </summary>
    /// <param name="i_Handler"></param>
    /// <returns></returns>
    public IHandlerConfig GetConfigForHandler<TConfig>(IEventingHandler i_Handler) where TConfig: IHandlerConfig
    {
      var path = MessagingRemoteHandlerConfigurationSetting.KEY + "." + i_Handler.HandlerName;
      return Configuration.GetValue<TConfig>(path, default);
    }
  }
}