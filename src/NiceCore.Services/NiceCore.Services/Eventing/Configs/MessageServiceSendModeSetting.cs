using System;
using NiceCore.Eventing.Messaging.Impl;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Services.Eventing.Configs
{
  /// <summary>
  /// Message Service Send Mode
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class MessageServiceSendModeSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// KEY
    /// </summary>
    public const string KEY = "NiceCore.Messages.Send.Mode";
    
    /// <summary>
    /// key
    /// </summary>
    public string Key { get; } = KEY;
    
    /// <summary>
    /// Setting Type
    /// </summary>
    public Type SettingType { get; } = typeof(MessageServiceSendModes);
  }
}