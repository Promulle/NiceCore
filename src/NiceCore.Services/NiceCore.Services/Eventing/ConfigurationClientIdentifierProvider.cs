﻿using NiceCore.Eventing.Config;
using NiceCore.Eventing.Impl;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Services.Eventing
{
  /// <summary>
  /// Configuration Client Identifier Provider
  /// </summary>
  [Register(InterfaceType = typeof(IClientIdentifierProvider))]
  public class ConfigurationClientIdentifierProvider : IClientIdentifierProvider
  {
    /// <summary>
    /// Configuration
    /// </summary>
    public INcConfiguration Configuration { get; set; }
    
    /// <summary>
    /// Get Client Identifier
    /// </summary>
    /// <returns></returns>
    public string GetClientIdentifier()
    {
      return Configuration.GetValue<string>(MessagingClientIdentifierSettingConstants.KEY, null);
    }
  }
}