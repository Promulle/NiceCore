﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NiceCore.Base;
using NiceCore.Services.Scheduling.Entries;

namespace NiceCore.Services.Scheduling
{
  /// <summary>
  /// Host for services that can be scheduled
  /// </summary>
  public interface IScheduleServiceHost
  {
    /// <summary>
    /// Returns all schedules this host is managing
    /// </summary>
    /// <returns></returns>
    List<ISchedule> GetAllSchedules();

    /// <summary>
    /// Returns the default schedule this service host uses
    /// </summary>
    /// <returns></returns>
    ISchedule GetDefaultSchedule();
    
    /// <summary>
    /// Adds a service to the default schedule
    /// </summary>
    /// <param name="i_Service"></param>
    ValueTask AddService(IScheduledService i_Service);
    
    /// <summary>
    /// Adds a service to a specified schedule
    /// </summary>
    /// <param name="i_Service"></param>
    /// <param name="i_Schedule"></param>
    ValueTask AddService(IScheduledService i_Service, ISchedule i_Schedule);

    /// <summary>
    /// Adds a schedule entry to the default schedule
    /// </summary>
    /// <param name="i_Entry"></param>
    ValueTask AddScheduleEntry(IScheduleEntry i_Entry);
    
    /// <summary>
    /// Adds a schedule entry to a schedule
    /// </summary>
    /// <param name="i_Entry"></param>
    /// <param name="i_Schedule"></param>
    ValueTask AddScheduleEntry(IScheduleEntry i_Entry, ISchedule i_Schedule);

    /// <summary>
    /// initializes the host
    /// </summary>
    ValueTask Initialize();
    
    /// <summary>
    /// Removes a service from being executed
    /// </summary>
    /// <param name="i_Service"></param>
    void RemoveService(IScheduledService i_Service);
    
    /// <summary>
    /// Stops all schedules
    /// </summary>
    void StopScheduling();
    
    /// <summary>
    /// Starts all schedules
    /// </summary>
    ValueTask StartScheduling();
  }
}
