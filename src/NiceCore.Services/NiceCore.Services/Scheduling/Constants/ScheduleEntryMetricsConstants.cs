#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
namespace NiceCore.Services.Scheduling.Constants
{
  public static class ScheduleEntryMetricsConstants
  {
    public const string SCHEDULING_METER = "nicecore.scheduling";
    public const string SCHEDULING_INSTRUMENT = SCHEDULING_METER + ".execution";
  }
}