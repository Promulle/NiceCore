
namespace NiceCore.Services.Scheduling.Configuration.Model
{
  /// <summary>
  /// Model for the configuration of a single service to be put on a schedule
  /// </summary>
  public class SchedulingScheduleServiceConfigurationModel
  {
    /// <summary>
    /// Type of service.
    /// Interface or abstract Class: DependencyInjection (<see cref="CreationKind"/> is ignored)
    /// Class: Direct if not overriden by <see cref="CreationKind"/>
    /// Normal Type.GetType(string) rules apply
    /// </summary>
    public string ServiceType { get; set; }
    
    /// <summary>
    /// Creation Kind, behavior see <see cref="ServiceType"/>
    /// </summary>
    public ServiceCreationKinds? CreationKind { get; set; }
    
    /// <summary>
    /// Entry used to schedule the service
    /// </summary>
    public SchedulingServiceEntryConfigurationModel Entry { get; set; }
  }
}