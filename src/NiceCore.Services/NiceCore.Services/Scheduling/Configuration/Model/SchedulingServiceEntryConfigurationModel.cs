namespace NiceCore.Services.Scheduling.Configuration.Model
{
  /// <summary>
  /// Configuration for an entry that determines when a service is run
  /// </summary>
  public class SchedulingServiceEntryConfigurationModel
  {
    /// <summary>
    /// Whether to run the service once when its entry was put on the determined schedule
    /// </summary>
    public bool ExecuteOnScheduled { get; set; }

    /// <summary>
    /// Collect Metrics
    /// </summary>
    public bool CollectMetrics { get; set; }
    
    /// <summary>
    /// Name of Entry to use as label for metric
    /// </summary>
    public string MetricName { get; set; }
    
    /// <summary>
    /// Enabled
    /// </summary>
    public bool? Enabled { get; set; }
    
    /// <summary>
    /// Type of entry to schedule
    /// </summary>
    public string EntryType { get; set; }

    /// <summary>
    /// Value to be parsed into actual useable value
    /// Cron: */5 * * * * *
    /// Exact: 11:30:45
    /// Timespan: 10 Seconds
    /// </summary>
    public string Value { get; set; }
  }
}