using System.Collections.Generic;

namespace NiceCore.Services.Scheduling.Configuration.Model
{
  /// <summary>
  /// Model for the configuration of a single schedule
  /// </summary>
  public class SchedulingScheduleConfigurationModel
  {
    /// <summary>
    /// Optional Type used to construct the schedule. If this is null, default is used. Not used if ScheduleName is empty/null/default
    /// </summary>
    public string ScheduleType { get; set; }
    
    /// <summary>
    /// Name of schedule, if this is not empty/null/default(case insensetive) a new schedule will be created for the entries using the provided type 
    /// </summary>
    public string ScheduleName { get; set; }
    
    /// <summary>
    /// Services to be put on the schedule
    /// </summary>
    public List<SchedulingScheduleServiceConfigurationModel> Services { get; set; }
  }
}