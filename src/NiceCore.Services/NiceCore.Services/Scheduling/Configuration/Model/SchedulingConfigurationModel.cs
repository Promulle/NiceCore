using System.Collections.Generic;

namespace NiceCore.Services.Scheduling.Configuration.Model
{
  /// <summary>
  /// ConfigurationModel
  /// </summary>
  public class SchedulingConfigurationModel
  {
    /// <summary>
    /// Schedules
    /// </summary>
    public List<SchedulingScheduleConfigurationModel> Schedules { get; set; }
    
    /// <summary>
    /// Configured Services
    /// </summary>
    public Dictionary<string, SchedulingServiceEntryConfigurationModel> ConfiguredServices { get; set; }
  }
}