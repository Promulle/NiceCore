namespace NiceCore.Services.Scheduling.Configuration.Model
{
  /// <summary>
  /// Kinds to create a service by
  /// </summary>
  public enum ServiceCreationKinds
  {
    /// <summary>
    /// Service is retrieved from DependencyInjection
    /// </summary>
    DependencyInjection,
    
    /// <summary>
    /// Instantiate the service directly
    /// </summary>
    Direct
  }
}