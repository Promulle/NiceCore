namespace NiceCore.Services.Scheduling.Configuration.Model
{
  /// <summary>
  /// Possible configurable entry types
  /// </summary>
  public enum SchedulingEntryTimeModelTypes
  {
    /// <summary>
    /// Entry Scheduled by cron expression
    /// </summary>
    Cron = 0,
    
    /// <summary>
    /// Entry scheduled by timespan (Type: incremental entry)
    /// </summary>
    Timespan = 1,
    
    /// <summary>
    /// Exact time (not date) this is run
    /// </summary>
    Exact = 2
  }
}