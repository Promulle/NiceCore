using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using NiceCore.Services.Scheduling.Configuration.Creators;
using NiceCore.Services.Scheduling.Configuration.Model;
using NiceCore.Services.Scheduling.Entries;

namespace NiceCore.Services.Scheduling.Configuration
{
  /// <summary>
  /// Configurator for ServiceHost
  /// </summary>
  [Register(InterfaceType = typeof(IScheduleServiceHostConfigurator))]
  public class ScheduleServiceHostConfigurator : IScheduleServiceHostConfigurator
  {
    /// <summary>
    /// Configuration
    /// </summary>
    public INcConfiguration Configuration { get; set; }

    /// <summary>
    /// Service Container
    /// </summary>
    public IServiceContainer ServiceContainer { get; set; }

    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<ScheduleServiceHostConfigurator> Logger { get; set; }

    /// <summary>
    /// Configure the Service Host with the settings
    /// </summary>
    /// <param name="i_ServiceHost"></param>
    public async ValueTask Configure(IScheduleServiceHost i_ServiceHost)
    {
      if (i_ServiceHost == null)
      {
        throw new ArgumentException("Can not configure serviceHost. ServiceHost was passed as null.");
      }

      if (Configuration != null)
      {
        Logger.Information($"Starting to configure servicehost of type {i_ServiceHost.GetType().FullName}");
        var setting = Configuration.GetValue<SchedulingConfigurationModel>(ScheduleServiceHostSetting.KEY, null);
        if (setting != null)
        {
          Logger.Information($"Found {setting.Schedules?.Count ?? 0} Schedules to configure.");
          var schedules = new Dictionary<string, ISchedule>();
          if (setting.Schedules != null)
          {
            foreach (var schedule in setting.Schedules)
            {
              Logger.Information($"Configuring Schedule: {schedule.ScheduleName}");
              await ApplyScheduleConfiguration(i_ServiceHost, schedule, schedules).ConfigureAwait(false);
            }
          }
        }
        else
        {
          Logger.Debug($"Configuration of serviceHost was attempted but no Setting was configured for key: {ScheduleServiceHostSetting.KEY}");
        }
      }
      else
      {
        Logger.Warning("Could not configure service host because Configuration property of configurator was not set.");
      }
    }

    private async ValueTask ApplyScheduleConfiguration(IScheduleServiceHost t_Host, SchedulingScheduleConfigurationModel i_Schedule, IDictionary<string, ISchedule> i_ExistingSchedules)
    {
      if (IsDefaultSchedule(i_Schedule))
      {
        Logger.Information("Configuring default schedule");
        var entries = await GetConfiguredEntries(i_Schedule.Services).ConfigureAwait(false);
        foreach (var entry in entries)
        {
          await t_Host.AddScheduleEntry(entry).ConfigureAwait(false);
        }
      }
      else
      {
        Logger.Information($"Configuring schedule {i_Schedule.ScheduleName}");
        var schedule = ScheduleCreator.CreateOrGetSchedule(i_Schedule, i_ExistingSchedules, t_Host, Logger);
        if (schedule != null)
        {
          var entries = await GetConfiguredEntries(i_Schedule.Services).ConfigureAwait(false);
          foreach (var entry in entries)
          {
            await t_Host.AddScheduleEntry(entry, schedule).ConfigureAwait(false);
          }
        }
        else
        {
          Logger.Error($"Could not configure schedule: {i_Schedule.ScheduleName}, could not retrieve instance for schedule.");
        }
      }
    }

    private async ValueTask<List<IScheduleEntry>> GetConfiguredEntries(List<SchedulingScheduleServiceConfigurationModel> i_Services)
    {
      var res = new List<IScheduleEntry>();
      foreach (var serviceModel in i_Services)
      {
        Logger.Information($"Configuring entry for service: {serviceModel.ServiceType}");
        var entry = await CreateScheduleEntryFrom(serviceModel).ConfigureAwait(false);
        if (entry != null)
        {
          Logger.Information($"Successfully created entry for service: {serviceModel.ServiceType}");
          res.Add(entry);
        }
      }

      return res;
    }

    private ValueTask<IScheduleEntry> CreateScheduleEntryFrom(SchedulingScheduleServiceConfigurationModel i_ServiceModel)
    {
      var service = ServiceInstanceCreator.InstantiateService(i_ServiceModel, Logger, ServiceContainer);
      //TODO: Someday enable runtime refresh of configured schedules
      return InstantiateEntry(i_ServiceModel.Entry, null, service);
    }

    private ValueTask<IScheduleEntry> InstantiateEntry(SchedulingServiceEntryConfigurationModel i_ServiceEntryModel, string i_Key, ISchedulable i_ServiceToSchedule)
    {
      return ScheduleEntryCreator.CreateEntryFromConfiguration(i_ServiceToSchedule, i_ServiceEntryModel, i_Key, Configuration, Logger);
    }

    private static bool IsDefaultSchedule(SchedulingScheduleConfigurationModel i_Schedule)
    {
      return string.IsNullOrEmpty(i_Schedule.ScheduleName) || i_Schedule.ScheduleName.Equals("default", StringComparison.OrdinalIgnoreCase);
    }
  }
}