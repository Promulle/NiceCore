using System;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using NiceCore.Services.Scheduling.Configuration.Model;

namespace NiceCore.Services.Scheduling.Configuration
{
  /// <summary>
  /// Schedule Service Host Setting
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class ScheduleServiceHostSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// Key
    /// </summary>
    public const string KEY = "NiceCore.Scheduling";

    /// <summary>
    /// Key
    /// </summary>
    public string Key { get; } = KEY;

    /// <summary>
    /// Type
    /// </summary>
    public Type SettingType { get; } = typeof(SchedulingConfigurationModel);
  }
}