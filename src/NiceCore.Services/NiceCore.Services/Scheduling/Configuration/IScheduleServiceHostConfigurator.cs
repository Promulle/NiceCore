using System.Threading.Tasks;

namespace NiceCore.Services.Scheduling.Configuration
{
  /// <summary>
  /// Configurator for service host
  /// </summary>
  public interface IScheduleServiceHostConfigurator
  {
    /// <summary>
    /// Configure the Service Host with the settings
    /// </summary>
    /// <param name="i_ServiceHost"></param>
    ValueTask Configure(IScheduleServiceHost i_ServiceHost);
  }
}