using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using NiceCore.Logging;
using NiceCore.Logging.Extensions;
using NiceCore.Services.Scheduling.Configuration.Model;

namespace NiceCore.Services.Scheduling.Configuration.Creators
{
  /// <summary>
  /// Creator used to instantiate/search schedules for configuration provided models
  /// </summary>
  public static class ScheduleCreator
  {
    /// <summary>
    /// Creates a new schedule or uses the correct schedule from i_ExistingSchedules
    /// </summary>
    /// <param name="i_ScheduleModel"></param>
    /// <param name="i_Schedules"></param>
    /// <param name="i_Host"></param>
    /// <param name="i_Logger"></param>
    /// <returns></returns>
    /// <exception cref="InvalidOperationException"></exception>
    public static ISchedule CreateOrGetSchedule(SchedulingScheduleConfigurationModel i_ScheduleModel,
      IDictionary<string, ISchedule> i_Schedules,
      IScheduleServiceHost i_Host,
      ILogger i_Logger)
    {
      if (i_ScheduleModel.ScheduleType == null)
      {
        return i_Host.GetDefaultSchedule();
      }

      if (i_Schedules.TryGetValue(i_ScheduleModel.ScheduleName, out var schedule))
      {
        return schedule;
      }
      
      try
      {
        var type = Type.GetType(i_ScheduleModel.ScheduleType);
        if (type == null)
        {
          throw new InvalidOperationException($"No type found for {i_ScheduleModel.ScheduleType}");
        }
        if (type.IsAbstract || type.IsInterface)
        {
          i_Logger.Error($"Type for schedule found: {type.FullName} but it is either abstract or an interface type. Can not be used to instantiate a schedule.");
          return null;
        }
        if (!typeof(ISchedule).IsAssignableFrom(type))
        {
          i_Logger.Error($"Instantiated type {type.FullName} but it does not implement {typeof(ISchedule).FullName}. Can not be used for schedule");
          return null;
        }
        i_Logger.Information($"Instantiated type {type.FullName} and creating an instance for it to be used as schedule.");
        var scheduleInstance = Activator.CreateInstance(type) as ISchedule;
        i_Schedules[i_ScheduleModel.ScheduleName] = scheduleInstance;
        return scheduleInstance;
      }
      catch (Exception ex)
      {
        i_Logger.Error(ex, $"Could not instantiate type for schedule from string {i_ScheduleModel.ScheduleType}");
        return null;
      }
    }
  }
}