using System;
using Microsoft.Extensions.Logging;
using NiceCore.Services.Scheduling.Configuration.Model;
using NiceCore.Services.Scheduling.Entries;
using NiceCore.Services.Scheduling.Entries.EntryTimeModels.Impls;

namespace NiceCore.Services.Scheduling.Configuration.Creators.Entries
{
  /// <summary>
  /// Exact entry creator
  /// </summary>
  public sealed class ExactEntryTimeModelCreator : ISpecificEntryTimeModelCreator
  {
    /// <summary>
    /// Create an exact entry
    /// </summary>
    /// <param name="i_EntryConfiguration"></param>
    /// <param name="i_Logger"></param>
    /// <returns></returns>
    public IScheduleEntryTimeModel CreateScheduleEntry(SchedulingServiceEntryConfigurationModel i_EntryConfiguration, ILogger i_Logger)
    {
      if (TimeOnly.TryParse(i_EntryConfiguration.Value, out var time))
      {
        return new DefaultExactScheduleEntryTimeModel()
        {
          ExecuteTime = time,
          IsTimeModelEnabled = i_EntryConfiguration.Enabled ?? true
        };
      }

      throw new ArgumentException($"Value for configured exact schedule entry should adhere to the following format: {TimeOnly.FromDateTime(DateTime.UtcNow)}");
    }
  }
}