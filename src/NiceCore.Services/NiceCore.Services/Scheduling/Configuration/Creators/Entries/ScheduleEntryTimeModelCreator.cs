using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using NiceCore.Logging.Extensions;
using NiceCore.Services.Scheduling.Configuration.Model;
using NiceCore.Services.Scheduling.Entries;

namespace NiceCore.Services.Scheduling.Configuration.Creators.Entries
{
  /// <summary>
  /// Entry Creator
  /// </summary>
  public static class ScheduleEntryTimeModelCreator
  {
    private static readonly IDictionary<SchedulingEntryTimeModelTypes, ISpecificEntryTimeModelCreator> m_SpecificCreators = new Dictionary<SchedulingEntryTimeModelTypes, ISpecificEntryTimeModelCreator>()
    {
      {SchedulingEntryTimeModelTypes.Cron, new CronEntryTimeModelCreator()},
      {SchedulingEntryTimeModelTypes.Timespan, new TimespanEntryTimeModelCreator()},
      {SchedulingEntryTimeModelTypes.Exact, new ExactEntryTimeModelCreator()}
    };

    /// <summary>
    /// Create an entry based on the configuration, puts the schedulable into the entry
    /// </summary>
    /// <param name="i_Model"></param>
    /// <param name="i_Logger"></param>
    /// <returns></returns>
    public static IScheduleEntryTimeModel CreateEntryTimeModel(SchedulingServiceEntryConfigurationModel i_Model, ILogger i_Logger)
    {
      if (i_Model.EntryType == null)
      {
        throw new InvalidOperationException($"Entry configuration is not correct. No kind specified using key: {nameof(i_Model.EntryType)}");
      }
      var kind = SchedulingEntryTimeModelTypesConversionHelper.FromStringFast(i_Model.EntryType);
      if (kind == null)
      {
        throw new InvalidOperationException($"Could not translate {i_Model.EntryType} to an actual entry type. Allowed values: '{SchedulingEntryTimeModelTypes.Cron}', '{SchedulingEntryTimeModelTypes.Exact}', '{SchedulingEntryTimeModelTypes.Timespan}'");
      }
      if (m_SpecificCreators.TryGetValue(kind.Value, out var creator))
      {
        return creator.CreateScheduleEntry(i_Model, i_Logger);
      }
      i_Logger.Warning($"Could not instantiate entry for kind: {kind.Value}. No creator defined for it.");
      return null;
    }
  }
}