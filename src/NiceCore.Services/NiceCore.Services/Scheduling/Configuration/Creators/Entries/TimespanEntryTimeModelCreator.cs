using System;
using Microsoft.Extensions.Logging;
using NiceCore.Base;
using NiceCore.Services.Scheduling.Configuration.Model;
using NiceCore.Services.Scheduling.Entries;
using NiceCore.Services.Scheduling.Entries.EntryTimeModels.Impls;

namespace NiceCore.Services.Scheduling.Configuration.Creators.Entries
{
  /// <summary>
  /// Creator for timespan entries
  /// </summary>
  public sealed class TimespanEntryTimeModelCreator : ISpecificEntryTimeModelCreator
  {
    /// <summary>
    /// Create a scheduled entry for timespan kind
    /// </summary>
    /// <param name="i_EntryConfiguration"></param>
    /// <param name="i_Logger"></param>
    /// <returns></returns>
    public IScheduleEntryTimeModel CreateScheduleEntry(SchedulingServiceEntryConfigurationModel i_EntryConfiguration, ILogger i_Logger)
    {
      var parsedTimeSpan = ParseTimespan(i_EntryConfiguration.Value);
      return new DefaultTimeSpanScheduleEntryTimeModel()
      {
        SpanBetweenExecutions = parsedTimeSpan,
        IsTimeModelEnabled = i_EntryConfiguration.Enabled ?? true
      };
    }

    private static TimeSpan ParseTimespan(string i_Value)
    {
      var split = i_Value.Split(' ', StringSplitOptions.RemoveEmptyEntries);
      if (split.Length == 2)
      {
        var kind = TimeKindConversionHelper.FromStringFast(split[1]);
        if (int.TryParse(split[0], out var timeCount) && kind != null)
        {
          return kind.Value.ToTimeSpan(timeCount);
        }
      }
      throw new ArgumentException($"Expected timespan entry value to be of format: '(number) (TimeKind)' but was {i_Value}");
    }
  }
}