using Microsoft.Extensions.Logging;
using NiceCore.Services.Scheduling.Configuration.Model;
using NiceCore.Services.Scheduling.Entries;

namespace NiceCore.Services.Scheduling.Configuration.Creators.Entries
{
  /// <summary>
  /// Interface to be implemented for each entry that could be defined in configuration
  /// </summary>
  public interface ISpecificEntryTimeModelCreator
  {
    /// <summary>
    /// Create a schedulable from configuration and the service it schedules
    /// </summary>
    /// <param name="i_EntryConfiguration"></param>
    /// <param name="i_Logger"></param>
    /// <returns></returns>
    IScheduleEntryTimeModel CreateScheduleEntry(SchedulingServiceEntryConfigurationModel i_EntryConfiguration, ILogger i_Logger);
  }
}