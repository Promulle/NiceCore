using System;
using Cronos;
using Microsoft.Extensions.Logging;
using NiceCore.Services.Scheduling.Configuration.Model;
using NiceCore.Services.Scheduling.Entries;
using NiceCore.Services.Scheduling.Entries.EntryTimeModels.Impls;

namespace NiceCore.Services.Scheduling.Configuration.Creators.Entries
{
  /// <summary>
  /// Cron Entry Creator
  /// </summary>
  public class CronEntryTimeModelCreator : ISpecificEntryTimeModelCreator
  {
    /// <summary>
    /// Create Schedulable
    /// </summary>
    /// <param name="i_EntryConfiguration"></param>
    /// <param name="i_Logger"></param>
    /// <returns></returns>
    public IScheduleEntryTimeModel CreateScheduleEntry(SchedulingServiceEntryConfigurationModel i_EntryConfiguration, ILogger i_Logger)
    {
      var format = TryToParseFormat(i_EntryConfiguration.Value);
      return new CronExpressionScheduleEntryTimeModel(i_EntryConfiguration.Value, format, i_Logger)
      {
        IsTimeModelEnabled = i_EntryConfiguration.Enabled ?? true
      };
    }

    private static CronFormat TryToParseFormat(string i_CronExpression)
    {
      var splitLength = i_CronExpression.Split(' ', StringSplitOptions.RemoveEmptyEntries).Length;
      if (splitLength == 6)
      {
        return CronFormat.IncludeSeconds;
      }
      return CronFormat.Standard;
    }
  }
}