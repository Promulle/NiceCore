using System;
using Microsoft.Extensions.Logging;
using NiceCore.Logging;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation;
using NiceCore.Services.Scheduling.Configuration.Model;

namespace NiceCore.Services.Scheduling.Configuration.Creators
{
  /// <summary>
  /// Class dealing with instantiating services
  /// </summary>
  public static class ServiceInstanceCreator
  {
    /// <summary>
    /// Create the instance for a service
    /// </summary>
    /// <param name="i_ServiceModel"></param>
    /// <param name="i_Logger"></param>
    /// <param name="i_ServiceContainer"></param>
    /// <returns></returns>
    public static ISchedulable InstantiateService(SchedulingScheduleServiceConfigurationModel i_ServiceModel,
      ILogger i_Logger,
      IServiceContainer i_ServiceContainer)
    {
      var type = Type.GetType(i_ServiceModel.ServiceType);
      if (type == null)
      {
        i_Logger.Error($"Could not find type for service: {i_ServiceModel.ServiceType}");
        return null;
      }
      
      if (type.IsAbstract || type.IsInterface)
      {
        i_Logger.Information($"type {type.FullName} is abstract or an interface, trying to resolve it by dependencyInjection.");
        return GetServiceFromDI(type, i_ServiceContainer);
      }

      i_Logger.Information($"type {type.FullName} is a class, looking whether to directly instantiate it or to use depencyInjection...");
      if (i_ServiceModel.CreationKind == null || i_ServiceModel.CreationKind == ServiceCreationKinds.Direct && IsCorrectType(type, i_Logger))
      {
        i_Logger.Information($"Directly instantiating instance for service of type {type.FullName}");
        return Activator.CreateInstance(type) as ISchedulable;
      }

      if (i_ServiceModel.CreationKind == ServiceCreationKinds.DependencyInjection)
      {
        i_Logger.Information("Resolving instance from DependencyInjection");
        return GetServiceFromDI(type, i_ServiceContainer);
      }

      i_Logger.Warning($"Somehow the service could not be instantiated for type {type.FullName}");
      return null;
    }

    private static bool IsCorrectType(Type i_Type, ILogger i_Logger)
    {
      if (!typeof(ISchedulable).IsAssignableFrom(i_Type))
      {
        i_Logger.Error($"Determined type to use as {i_Type.FullName} but this type does not implement {typeof(IScheduledService).FullName}");
        return false;
      }
      return true;
    }

    private static ISchedulable GetServiceFromDI(Type i_Type, IServiceContainer i_Container)
    {
      if (i_Container == null)
      {
        throw new InvalidOperationException($"Tried to resolve for type {i_Type.FullName} but no service container was passed.");
      }
      return i_Container.Resolve<ISchedulable>(i_Type);
    }
  }
}