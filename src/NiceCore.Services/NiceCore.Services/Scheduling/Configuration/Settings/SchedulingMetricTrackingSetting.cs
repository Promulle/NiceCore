using System;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Services.Scheduling.Configuration.Settings
{
  /// <summary>
  /// Schedule Metric Tracking Setting
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class SchedulingMetricTrackingSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// KEy
    /// </summary>
    public const string KEY = "NiceCore.Scheduling.EnableMetricTracking";
    
    /// <summary>
    /// KEy
    /// </summary>
    public string Key { get; } = KEY;
    
    /// <summary>
    /// Setting Type
    /// </summary>
    public Type SettingType { get; } = typeof(bool);
  }
}