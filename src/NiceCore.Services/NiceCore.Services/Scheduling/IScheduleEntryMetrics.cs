using System;
using NiceCore.Services.Scheduling.Entries;

namespace NiceCore.Services.Scheduling
{
  /// <summary>
  /// Schedule Entry Metrics
  /// </summary>
  public interface IScheduleEntryMetrics
  {
    /// <summary>
    /// Record Service Execution Time
    /// </summary>
    /// <param name="i_ElapsedTime"></param>
    /// <param name="i_Entry"></param>
    void RecordServiceExecutionTime(TimeSpan i_ElapsedTime, IScheduleEntry i_Entry);

    /// <summary>
    /// Is Metric Tracking Enabled
    /// </summary>
    /// <returns></returns>
    bool IsMetricTrackingEnabled();
  }
}