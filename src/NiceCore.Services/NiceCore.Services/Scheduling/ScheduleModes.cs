﻿namespace NiceCore.Services.Scheduling
{
  /// <summary>
  /// Modes for running schedules
  /// </summary>
  public enum ScheduleModes
  {
    /// <summary>
    /// Scheduled to run an exact time
    /// </summary>
    Exact,
    /// <summary>
    /// Scheduled every x time amount
    /// </summary>
    Incremental,
  }
}
