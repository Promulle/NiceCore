﻿using System.Threading.Tasks;
using NiceCore.Base.Services;
using NiceCore.Services.Scheduling.Entries;

namespace NiceCore.Services.Scheduling
{
  /// <summary>
  /// Service that runs on a schedule
  /// </summary>
  public interface IScheduledService : IService, ISchedulable
  {
    /// <summary>
    /// Returns a new schedule entry that can be scheduled.
    /// If you want this service to not be scheduled automatically, return null.
    /// A service that returns null on this function will NOT be scheduled
    /// </summary>
    /// <returns></returns>
    ValueTask<IScheduleEntry> CreateScheduleEntry();
  }
}
