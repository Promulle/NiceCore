using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Logging.Extensions;
using NiceCore.Services.Config;
using NiceCore.Services.Scheduling.Configuration.Creators.Entries;
using NiceCore.Services.Scheduling.Configuration.Model;
using NiceCore.Services.Scheduling.Entries;

namespace NiceCore.Services.Scheduling
{
  /// <summary>
  /// Base class for services that draw their entry directly from the provided configuration
  /// </summary>
  public abstract class BaseConfigurableScheduledService : BaseScheduledService
  {
    /// <summary>
    /// Default base key for services
    /// </summary>
    protected static readonly string BASE_SERVICE_CONFIG_KEY = "NiceCore.Scheduling.ConfiguredServices";
    
    /// <summary>
    /// Configuration
    /// </summary>
    public INcConfiguration Configuration { get; set; }

    /// <summary>
    /// Logger
    /// </summary>
    public abstract ILogger Logger { get; }
    
    /// <summary>
    /// Create the schedulable entry from Configuration
    /// </summary>
    /// <returns></returns>
    public override async ValueTask<IScheduleEntry> CreateScheduleEntry()
    {
      Logger.Information($"Creating schedule entry for Service {GetType().FullName} from configuration");
      if (Configuration == null)
      {
        if (SkipOnMissingConfiguration())
        {
          return null;
        }
        throw new InvalidOperationException($"Can not create schedule entry for service: {GetType().FullName}. Configuration is not filled.");
      }
      var serviceKey = GetConfigurationKey();
      Logger.Debug($"Key to be used to configure scheduled service {GetType().FullName}: {serviceKey}");
      var entry = Configuration.GetValue<SchedulingServiceEntryConfigurationModel>(serviceKey, null);
      if (entry == null)
      {
        if (SkipOnMissingConfiguration())
        {
          return null;
        }
        throw new InvalidOperationException($"Can not create schedule entry for service because no configuration was provided: {GetType().FullName}");
      }
      Logger.Information($"Creating entry: Type: {entry.EntryType}, Value: {entry.Value}, Async: true, ExecuteOnScheduled: {entry.ExecuteOnScheduled}");
      return await ScheduleEntryCreator.CreateEntryFromConfiguration(this, entry, serviceKey, Configuration, Logger).ConfigureAwait(false);
    }

    /// <summary>
    /// Get Configuration key
    /// </summary>
    /// <returns></returns>
    public virtual string GetConfigurationKey()
    {
      return $"{BASE_SERVICE_CONFIG_KEY}.{GetType().Name}";
    }

    /// <summary>
    /// Skip the service if it is not configured. An Exception will be thrown otherwise
    /// </summary>
    /// <returns></returns>
    protected virtual bool SkipOnMissingConfiguration()
    {
      return false;
    }
  }
}