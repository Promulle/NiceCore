using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using NiceCore.Services.Scheduling.Configuration.Settings;
using NiceCore.Services.Scheduling.Constants;
using NiceCore.Services.Scheduling.Entries;

namespace NiceCore.Services.Scheduling
{
  /// <summary>
  /// Schedule Entry Metrics
  /// </summary>
  [Register(InterfaceType = typeof(IScheduleEntryMetrics), LifeStyle = LifeStyles.Singleton)]
  public class ScheduleEntryMetrics : IScheduleEntryMetrics
  {
    private readonly Meter m_ExecutionTimeMeter = new(ScheduleEntryMetricsConstants.SCHEDULING_METER);
    private readonly Histogram<long> m_ExecutionTimeHistogram;
    
    /// <summary>
    /// Configuration
    /// </summary>
    public INcConfiguration Configuration { get; set; }
    
    /// <summary>
    /// Ctor
    /// </summary>
    public ScheduleEntryMetrics()
    {
      m_ExecutionTimeHistogram = m_ExecutionTimeMeter.CreateHistogram<long>(ScheduleEntryMetricsConstants.SCHEDULING_INSTRUMENT);
    }

    /// <summary>
    /// Is Metric Tracking enabled
    /// </summary>
    /// <returns></returns>
    public bool IsMetricTrackingEnabled()
    {
      return Configuration.GetValue(SchedulingMetricTrackingSetting.KEY, false);
    }
    
    /// <summary>
    /// Record Service Execution Time
    /// </summary>
    /// <param name="i_ElapsedTime"></param>
    /// <param name="i_Entry"></param>
    public void RecordServiceExecutionTime(TimeSpan i_ElapsedTime, IScheduleEntry i_Entry)
    {
      var serviceName = i_Entry.MetricName ?? i_Entry.Schedulable.GetType().Name;
      m_ExecutionTimeHistogram.Record((long) i_ElapsedTime.TotalMilliseconds, new KeyValuePair<string, object>("service.name", serviceName));
    }
  }
}