﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Base;
using NiceCore.Logging.Extensions;
using NiceCore.Services.Scheduling.Entries;

namespace NiceCore.Services.Scheduling
{
  /// <summary>
  /// Timer used for scheduling by the schedule
  /// </summary>
  public sealed class SchedulingTimer : BaseDisposable
  {
    private readonly ILogger m_Logger;
    private readonly CancellationToken m_CancellationToken;
    private PeriodicTimer m_Timer;
    
    /// <summary>
    /// Object that should be executed on elaped
    /// </summary>
    public IScheduleEntry Entry { get; }
    
    /// <summary>
    /// Schedule Entry Metrics
    /// </summary>
    public IScheduleEntryMetrics ScheduleEntryMetrics { get; }

    /// <summary>
    /// Create a new instance of Scheduling Timer that uses the given logger to log events
    /// </summary>
    /// <param name="i_Logger"></param>
    /// <param name="i_Entry"></param>
    /// <param name="i_Metrics"></param>
    /// <param name="i_Token"></param>
    public SchedulingTimer(ILogger i_Logger, IScheduleEntry i_Entry, IScheduleEntryMetrics i_Metrics, CancellationToken i_Token)
    {
      if (i_Entry == null)
      {
        throw new ArgumentException($"Could not create SchedulingTimer: {nameof(i_Entry)} was passed as null.");
      }

      ScheduleEntryMetrics = i_Metrics;
      Entry = i_Entry;
      Entry.AddTimeModelChangedListener(OnEntryTimeModelChanged);
      m_CancellationToken = i_Token;
      m_Logger = i_Logger;
    }

    private async ValueTask OnEntryTimeModelChanged(IScheduleEntry i_Entry)
    {
      await Start().ConfigureAwait(false);
    }
    
    /// <summary>
    /// Change the interval this entry is executed
    /// </summary>
    /// <param name="i_EntryTimeModel"></param>
    public async ValueTask ChangeInterval(IScheduleEntryTimeModel i_EntryTimeModel)
    {
      if (i_EntryTimeModel == null)
      {
        throw new ArgumentException($"Could not change interval of SchedulingTimer for entry {Entry?.GetStringRepresentation()}: Parameter {nameof(i_EntryTimeModel)} must not be null.");
      }
      m_Logger.Debug($"Attempting to change interval of timer for entry {Entry?.GetStringRepresentation()}");
      try
      {
        await ChangeIntervalInternal(i_EntryTimeModel).ConfigureAwait(false); 
      }
      catch (Exception ex)
      {
        m_Logger.Error(ex, $"An error occurred during change of interval for entry: {ex}");
      }
      m_Logger.Debug($"Finished changing interval of timer for entry {Entry?.GetStringRepresentation()}");
    }

    private async ValueTask ChangeIntervalInternal(IScheduleEntryTimeModel i_EntryTimeModel)
    {
      //TODO: .NET8 should introduce a .Period (or similar) property to change interval, use that to reduce memory waste if suitable since dispose also cancels current waits
      if (m_Timer != null)
      {
        m_Logger.Debug($"Deleting old timer for entry {Entry?.GetStringRepresentation()}");
        m_Timer.Dispose();
        m_Timer = null;
      }
      //i_EntryTimeModel.IsEnabled() == false -> disable the timer completely
      if (await i_EntryTimeModel.IsEnabled().ConfigureAwait(false))
      {
        var newTimeSpan = await i_EntryTimeModel.GetNextExecution().ConfigureAwait(false);
        m_Logger.Debug($"Creating new timer for entry {Entry?.GetStringRepresentation()}");
        m_Timer = new(newTimeSpan);  
      }
      else
      {
        m_Logger.Information($"Entry Time Model IsEnabled returned false -> disabling timer for entry {Entry?.GetStringRepresentation()}");
      }
    }

    /// <summary>
    /// Start
    /// </summary>
    public async ValueTask Start()
    {
      if (Entry == null)
      {
        throw new InvalidOperationException($"Could not start {nameof(SchedulingTimer)}");
      }
      m_Logger.Information($"Starting timer for entry {Entry.GetStringRepresentation()}. Configuring timer for start...");
      await ChangeInterval(Entry.TimeModel).ConfigureAwait(false);
      m_Logger.Information($"Starting timer for entry {Entry.GetStringRepresentation()}. Starting Task to run timer in...");
      //since this method calls changeInterval, the timer is guaranteed to be null only if Entry.TimeModel.isEnabled returned false which disables this timer
      if (m_Timer != null)
      {
#pragma warning disable CS4014 //here I fire and forget. Any errors are handled in HandleErrorsInContinueWith 
        Task.Factory.StartNew(static async schedulingTimer =>
        {
          if (schedulingTimer is SchedulingTimer casted)
          {
            await casted.TimerLoop().ConfigureAwait(false);
          }

        }, this, m_CancellationToken, TaskCreationOptions.LongRunning, TaskScheduler.Default).HandleErrorsInContinueWith(m_Logger);
#pragma warning restore CS4014
        m_Logger.Information($"Timer for entry {Entry.GetStringRepresentation()} started.");
      }
      else
      {
        m_Logger.Information($"Timer for entry {Entry.GetStringRepresentation()} was not started because the entry did request a TimeSpan that is equal to TimeSpan.Zero.");
      }
    }

    private async ValueTask TimerLoop()
    {
      while (await m_Timer!.WaitForNextTickAsync(m_CancellationToken) && !m_CancellationToken.IsCancellationRequested)
      {
        try
        {
          m_Logger.Debug($"Scheduling Timer Executing entry {Entry?.GetStringRepresentation()}");
          await ScheduleEntryExecutor.ExecuteEntry(Entry, ScheduleEntryMetrics).ConfigureAwait(false);
          //! since i do not know how entry would ever be null here
          await ChangeInterval(Entry!.TimeModel).ConfigureAwait(false);
          m_Logger.Debug($"Scheduling Timer executed entry {Entry?.GetStringRepresentation()} without exceptions");
        }
        catch (Exception e)
        {
          m_Logger.Error(e, $"An error occurred Executing entry {Entry?.GetStringRepresentation()}: {e}");
        }
      }

      m_Logger.Information($"End of timer for entry {Entry?.GetStringRepresentation()} reached.");
    }

    /// <summary>
    /// Stop this timer. Currently running execution will not be stopped
    /// </summary>
    public void Stop()
    {
      m_Logger.Information($"Stopping timer for entry {Entry.GetStringRepresentation()}.");
      m_Timer?.Dispose();
    }

    /// <summary>
    /// Untie event
    /// </summary>
    /// <param name="disposing"></param>
    protected override void Dispose(bool disposing)
    {
      if (m_Timer != null)
      {
        m_Timer.Dispose();
        m_Timer = null;
      }

      if (Entry is IDisposable disposableEntry)
      {
        disposableEntry.Dispose();
      }
      base.Dispose(disposing);
    }
  }
}
