﻿using System;
using NiceCore.Base;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Services.Scheduling.Entries;

namespace NiceCore.Services.Scheduling
{
  /// <summary>
  /// Schedule for timing
  /// </summary>
  public interface ISchedule : IBaseDisposable
  {
    /// <summary>
    /// Whether the schedule is enabled or not
    /// </summary>
    bool IsEnabled { get; }
    
    /// <summary>
    /// Logger
    /// </summary>
    ILogger Logger { get; }
    
    /// <summary>
    /// Enables this schedule
    /// </summary>
    ValueTask Enable();
    
    /// <summary>
    /// Disables this schedule -> no new scheduled entries will be executed
    /// </summary>
    void Disable();

    /// <summary>
    /// Changes this schedules logger to i_Logger. This is usefull when a logger should be set after creation
    /// because the schedules creator did not have a logger at the time
    /// </summary>
    /// <param name="i_Logger"></param>
    void SetLogger(ILogger i_Logger);

    /// <summary>
    /// Returns all entries that are scheduled
    /// </summary>
    /// <returns></returns>
    List<IScheduleEntry> GetAllScheduledEntries();

    /// <summary>
    /// Adds an entry to this schedule
    /// </summary>
    /// <param name="i_Entry"></param>
    /// <param name="i_AllowExecuteOnScheduled">Whether or not direct execution is allowed.</param>
    ValueTask AddToSchedule(IScheduleEntry i_Entry, bool i_AllowExecuteOnScheduled);
    
    /// <summary>
    /// Removes an entry from the schedule
    /// </summary>
    /// <param name="i_Entry"></param>
    void RemoveFromSchedule(IScheduleEntry i_Entry);

    /// <summary>
    /// Remove all from Schedule where i_Predicate returns true
    /// </summary>
    /// <param name="i_Predicate"></param>
    /// <param name="i_StateParam"></param>
    /// <typeparam name="TParam"></typeparam>
    void RemoveAllFromSchedule<TParam>(Func<TParam, IScheduleEntry, bool> i_Predicate, TParam i_StateParam);
    
    /// <summary>
    /// Searches for the entry that is scheduled for i_Schedulable
    /// </summary>
    /// <param name="i_Schedulable"></param>
    /// <returns></returns>
    IEnumerable<IScheduleEntry> GetEntriesBySchedulable(ISchedulable i_Schedulable);
  }
}
