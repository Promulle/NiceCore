﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using NiceCore.Services.Scheduling.Entries;

namespace NiceCore.Services.Scheduling
{
  /// <summary>
  /// Static class that deals with the execution of Scheduled Entries
  /// </summary>
  public static class ScheduleEntryExecutor
  {
    /// <summary>
    /// Executes the entry
    /// </summary>
    /// <param name="i_Entry"></param>
    /// <param name="i_Metrics"></param>
    /// <returns></returns>
    public static async ValueTask ExecuteEntry(IScheduleEntry i_Entry, IScheduleEntryMetrics i_Metrics)
    {
      if (await i_Entry.TimeModel.IsEnabled().ConfigureAwait(false))
      {
        i_Entry.ExecutionStarted();
        var start = StartMetricTracking(i_Entry, i_Metrics);
        await i_Entry.Schedulable.ExecuteAsync().ConfigureAwait(false);
        if (start != null && i_Metrics != null)
        {
          var elapsed = Stopwatch.GetElapsedTime(start.Value);
          i_Metrics.RecordServiceExecutionTime(elapsed, i_Entry);
        }
        i_Entry.ExecutionEnded();
      }
    }

    private static long? StartMetricTracking(IScheduleEntry i_Entry, IScheduleEntryMetrics i_Metrics)
    {
      if (i_Metrics != null && i_Metrics.IsMetricTrackingEnabled() && i_Entry.CollectMetrics)
      {
        return Stopwatch.GetTimestamp();
      }

      return null;
    }
  }
}