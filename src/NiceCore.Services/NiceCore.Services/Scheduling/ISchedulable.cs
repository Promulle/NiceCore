﻿using System.Threading.Tasks;

namespace NiceCore.Services.Scheduling
{
  /// <summary>
  /// Interface for things that can be set on a schedule
  /// </summary>
  public interface ISchedulable
  {
    /// <summary>
    /// Executes the schedulable async
    /// This is done if the entry has ExecuteAsync = true
    /// </summary>
    /// <returns></returns>
    ValueTask<bool> ExecuteAsync();
  }
}
