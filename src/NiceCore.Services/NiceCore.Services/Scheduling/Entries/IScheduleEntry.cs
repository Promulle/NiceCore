﻿using System;
using System.Threading.Tasks;

namespace NiceCore.Services.Scheduling.Entries
{
  /// <summary>
  /// Entry on a schedule that represents the timing needed
  /// </summary>
  public interface IScheduleEntry
  {
    /// <summary>
    /// Whether this entry should be executed once it is put onto an ISchedule
    /// </summary>
    bool ExecuteOnScheduled { get; }

    /// <summary>
    /// Collect Metrics for this entry
    /// </summary>
    bool CollectMetrics { get; }

    /// <summary>
    /// Metric Name
    /// </summary>
    string MetricName { get; }

    /// <summary>
    /// The element that should be executed
    /// </summary>
    ISchedulable Schedulable { get; }

    /// <summary>
    /// Time Model that designates when the next execution will take place
    /// </summary>
    IScheduleEntryTimeModel TimeModel { get; }

    /// <summary>
    /// Latest Execution Start
    /// </summary>
    DateTime? LatestExecutionStart { get; }

    /// <summary>
    /// Latest Execution End
    /// </summary>
    DateTime? LatestExecutionEnd { get; }

    /// <summary>
    /// Execution Started
    /// </summary>
    void ExecutionStarted();

    /// <summary>
    /// Execution Ended
    /// </summary>
    void ExecutionEnded();

    /// <summary>
    /// Returns a string representation of the entry
    /// </summary>
    /// <returns></returns>
    string GetStringRepresentation();

    /// <summary>
    /// Sets Time Model to entry and executes queued listeners
    /// </summary>
    /// <param name="i_Model"></param>
    /// <param name="i_CollectMetrics"></param>
    /// <param name="i_MetricName"></param>
    /// <exception cref="ArgumentException"></exception>
    ValueTask UpdateEntry(IScheduleEntryTimeModel i_Model, bool i_CollectMetrics, string i_MetricName);

    /// <summary>
    /// Add Model Changed Listener
    /// </summary>
    void AddTimeModelChangedListener(Func<IScheduleEntry, ValueTask> i_Listener);
  }
}
