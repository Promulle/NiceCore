using System;
using System.Threading.Tasks;

namespace NiceCore.Services.Scheduling.Entries
{
  /// <summary>
  /// Time Model for a ScheduleEntry
  /// </summary>
  public interface IScheduleEntryTimeModel
  {
    /// <summary>
    /// Is Enabled
    /// </summary>
    /// <returns></returns>
    ValueTask<bool> IsEnabled();
    
    /// <summary>
    /// Returns the timespan until the next execution should take place
    /// </summary>
    /// <returns></returns>
    ValueTask<TimeSpan> GetNextExecution();
    
    /// <summary>
    /// Get String Representation
    /// </summary>
    /// <returns></returns>
    string GetStringRepresentation();
  }
}