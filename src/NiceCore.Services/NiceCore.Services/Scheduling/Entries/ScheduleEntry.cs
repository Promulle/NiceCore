﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NiceCore.Services.Scheduling.Entries
{
  /// <summary>
  /// Entry used for scheduling base impl
  /// </summary>
  public class ScheduleEntry : IScheduleEntry
  {
    private List<Func<IScheduleEntry, ValueTask>> m_TimeModelChangedListeners = new();

    /// <summary>
    /// Whether this entry should be executed once it has been put on a schedule
    /// </summary>
    public bool ExecuteOnScheduled { get; init; }
    
    /// <summary>
    /// Collect Metrics
    /// </summary>
    public bool CollectMetrics { get; set; }
    
    /// <summary>
    /// Metric Name
    /// </summary>
    public string MetricName { get; set; }

    /// <summary>
    /// The Schedulable that should be executed
    /// </summary>
    public ISchedulable Schedulable { get; init; }
    
    /// <summary>
    /// Time Model that designates when the next execution will take place
    /// </summary>
    public IScheduleEntryTimeModel TimeModel { get; private set; }
    
    /// <summary>
    /// Latest Execution Start
    /// </summary>
    public DateTime? LatestExecutionStart { get; private set; }
    
    /// <summary>
    /// Latest Execution End
    /// </summary>
    public DateTime? LatestExecutionEnd { get; private set; }

    /// <summary>
    /// Execution Started
    /// </summary>
    public void ExecutionStarted()
    {
      LatestExecutionStart = DateTime.Now;
    }

    /// <summary>
    /// Execution Ended
    /// </summary>
    public void ExecutionEnded()
    {
      LatestExecutionEnd = DateTime.Now;
    }

    /// <summary>
    /// Default ctor
    /// </summary>
    public ScheduleEntry()
    {
      
    }

    /// <summary>
    /// Schedule Entry
    /// </summary>
    /// <param name="i_TimeModel"></param>
    public ScheduleEntry(IScheduleEntryTimeModel i_TimeModel)
    {
      TimeModel = i_TimeModel;
    }
    
    /// <summary>
    /// Get String Representation
    /// </summary>
    /// <returns></returns>
    public string GetStringRepresentation()
    {
      return "Entry for " + TimeModel.GetStringRepresentation() + " for Schedulable " + Schedulable.GetType().FullName;
    }

    /// <summary>
    /// Sets Time Model to entry and executes queued listeners
    /// </summary>
    /// <param name="i_Model"></param>
    /// <param name="i_CollectMetrics"></param>
    /// <param name="i_MetricName"></param>
    /// <exception cref="ArgumentException"></exception>
    public async ValueTask UpdateEntry(IScheduleEntryTimeModel i_Model, bool i_CollectMetrics, string i_MetricName)
    {
      if (i_Model == null)
      {
        throw new ArgumentException($"Cannot Set Time Model on entry: {nameof(i_Model)} can not be null");
      }
      TimeModel = i_Model;
      CollectMetrics = i_CollectMetrics;
      MetricName = i_MetricName;
      foreach (var listener in m_TimeModelChangedListeners)
      {
        await listener.Invoke(this).ConfigureAwait(false);
      }
    }

    /// <summary>
    /// Add Model Changed Listener
    /// </summary>
    public void AddTimeModelChangedListener(Func<IScheduleEntry, ValueTask> i_Listener)
    {
      m_TimeModelChangedListeners.Add(i_Listener);
    }
  }
}
