using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Abstractions;
using NiceCore.Services.Config;
using NiceCore.Services.Scheduling.Configuration.Creators.Entries;
using NiceCore.Services.Scheduling.Configuration.Model;

namespace NiceCore.Services.Scheduling.Entries
{
  /// <summary>
  /// Configured Schedule Entry
  /// </summary>
  public class ConfiguredScheduleEntry : ScheduleEntry, IDisposable
  {
    private IAsyncReloadableSetting<SchedulingServiceEntryConfigurationModel> m_Setting;

    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_TimeModel"></param>
    public ConfiguredScheduleEntry(IScheduleEntryTimeModel i_TimeModel) : base(i_TimeModel)
    {
      
    }
    
    /// <summary>
    /// Default ctor
    /// </summary>
    public ConfiguredScheduleEntry() : base()
    {
      
    }
    
    /// <summary>
    /// Setup for configuration reload
    /// </summary>
    /// <param name="i_Key"></param>
    /// <param name="i_Configuration"></param>
    public async ValueTask SetupForConfigurationReload(string i_Key, INcConfiguration i_Configuration)
    {
      m_Setting = await i_Configuration.GetSetting<SchedulingServiceEntryConfigurationModel>(i_Key, null).ConfigureAwait(false);
      await m_Setting.AddListener(args =>
      {
        var timeModel = ScheduleEntryTimeModelCreator.CreateEntryTimeModel(args.Value, NullLogger.Instance);
        return UpdateEntry(timeModel, args.Value.CollectMetrics, args.Value.MetricName);
      }, false).ConfigureAwait(false);
    }

    /// <summary>
    /// Dispose
    /// </summary>
    /// <param name="disposing"></param>
    protected virtual void Dispose(bool disposing)
    {
      if (disposing)
      {
        m_Setting?.Dispose();
      }
    }

    /// <summary>
    /// Dispose
    /// </summary>
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }
  }
}