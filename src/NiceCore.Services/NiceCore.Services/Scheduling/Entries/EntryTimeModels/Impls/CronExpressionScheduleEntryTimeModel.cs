﻿using System;
using System.Threading.Tasks;
using Cronos;
using Microsoft.Extensions.Logging;
using NiceCore.Diagnostics;
using NiceCore.Logging.Extensions;

namespace NiceCore.Services.Scheduling.Entries.EntryTimeModels.Impls
{
  /// <summary>
  /// Schedule entry that uses cron as format to determine next execution
  /// </summary>
  public class CronExpressionScheduleEntryTimeModel : BaseScheduleEntryTimeModel
  {
    private readonly CronExpression m_CronExpression;
    private readonly ILogger m_Logger;
    
    /// <summary>
    /// Ctor creating cron expression from string expression
    /// </summary>
    /// <param name="i_CronExpression">expression that should conform to the cronjob standard</param>
    public CronExpressionScheduleEntryTimeModel(string i_CronExpression) : this(i_CronExpression, CronFormat.Standard, DiagnosticsCollector.Instance.GetLogger<CronExpressionScheduleEntryTimeModel>())
    {
    }

    /// <summary>
    /// Ctor creating cron expression from string expression
    /// </summary>
    /// <param name="i_CronExpression">expression that should conform to the cronjob standard</param>
    /// <param name="i_Logger"></param>
    public CronExpressionScheduleEntryTimeModel(string i_CronExpression, ILogger i_Logger) : this(i_CronExpression, CronFormat.Standard, i_Logger)
    {
    }

    /// <summary>
    /// Ctor creating cron expression from string expression
    /// </summary>
    /// <param name="i_CronExpression">expression that should conform to the cronjob standard</param>
    /// <param name="i_Format">Format of cron expression</param>
    /// <param name="i_Logger"></param>
    public CronExpressionScheduleEntryTimeModel(string i_CronExpression, CronFormat i_Format, ILogger i_Logger)
    {
      m_Logger = i_Logger;
      m_CronExpression = CreateConExpressionFromString(i_CronExpression, i_Format);
    }

    private CronExpression CreateConExpressionFromString(string i_CronExpression, CronFormat i_Format)
    {
      if (i_CronExpression == null)
      {
        throw new ArgumentException($"Could not create a new instance of {nameof(CronExpressionScheduleEntryTimeModel)}. Passed value for parameter {nameof(i_CronExpression)} was null. Null is not valid for the parameter.");
      }
      m_Logger.Information($"Creating schedule entry based on cron expression: {i_CronExpression}");
      try
      {
        return CronExpression.Parse(i_CronExpression, i_Format);
      }
      catch (CronFormatException ex)
      {
        m_Logger.Error(ex, $"Could not create cron schedule entry, expression: {i_CronExpression} is not a valid cron expression");
        throw ;
      }
    }
    
    /// <summary>
    /// Returns the next execution for this entry
    /// </summary>
    /// <returns></returns>
    public override ValueTask<TimeSpan> GetNextExecution()
    {
      m_Logger.Information("Calculating next execution for cron entry");
      var utcNow = DateTime.UtcNow;
      var utcOffSetNow = DateTimeOffset.UtcNow;
      var nextOccInUTC = m_CronExpression.GetNextOccurrence(utcOffSetNow, TimeZoneInfo.Utc);
      if (nextOccInUTC != null)
      {
        if (nextOccInUTC.Value.DateTime < utcNow)
        {
          m_Logger.Warning($"Somehow the execution time is after {utcNow} (utc) so scheduling will try to execute the service in 1 minute and then retry the cron expression.");
          return ValueTask.FromResult(TimeSpan.FromMinutes(1));
        }

        var timeSpan = nextOccInUTC.Value.DateTime - utcNow;
        m_Logger.Information($"Next execution of entry in {timeSpan.TotalMilliseconds} milliseconds.");
        return ValueTask.FromResult(timeSpan);
      }
      m_Logger.Warning($"Somehow {nameof(m_CronExpression.GetNextOccurrence)} returned null. Could not determine timespan till next execution.");
      return ValueTask.FromResult(TimeSpan.Zero);
    }

    /// <summary>
    /// Get String representation
    /// </summary>
    /// <returns></returns>
    public override string GetStringRepresentation()
    {
      return "Cron: " + m_CronExpression;
    }
  }
}