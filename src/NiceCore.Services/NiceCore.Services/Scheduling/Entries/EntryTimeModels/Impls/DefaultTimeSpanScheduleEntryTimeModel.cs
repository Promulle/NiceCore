﻿using System;
using System.Threading.Tasks;

namespace NiceCore.Services.Scheduling.Entries.EntryTimeModels.Impls
{
  /// <summary>
  /// SchedulEntry impl for Incremental times
  /// </summary>
  public class DefaultTimeSpanScheduleEntryTimeModel : BaseScheduleEntryTimeModel, ITimeSpanScheduleEntryTimeModel
  {
    /// <summary>
    /// The span between executions
    /// </summary>
    public TimeSpan SpanBetweenExecutions { get; set; }

    /// <summary>
    /// returns the time till next execution
    /// </summary>
    /// <returns></returns>
    public override ValueTask<TimeSpan> GetNextExecution()
    {
      return ValueTask.FromResult(SpanBetweenExecutions);
    }

    /// <summary>
    /// Get String Representation
    /// </summary>
    /// <returns></returns>
    public override string GetStringRepresentation()
    {
      return "Incremental: " + SpanBetweenExecutions.Milliseconds + " (ms) TimeSpan";
    }
  }
}
