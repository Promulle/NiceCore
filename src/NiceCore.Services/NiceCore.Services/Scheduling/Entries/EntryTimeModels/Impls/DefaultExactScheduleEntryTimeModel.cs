﻿using System;
using System.Threading.Tasks;

namespace NiceCore.Services.Scheduling.Entries.EntryTimeModels.Impls
{
  /// <summary>
  /// Impl of IExactScheduleEntry
  /// </summary>
  public sealed class DefaultExactScheduleEntryTimeModel : BaseScheduleEntryTimeModel, IExactScheduleEntryTimeModel
  {
    /// <summary>
    /// Exact time to be executed on
    /// </summary>
    public TimeOnly ExecuteTime { get; set; }

    /// <summary>
    /// Returns a span from now to ExecuteTime on today
    /// </summary>
    /// <returns></returns>
    public override ValueTask<TimeSpan> GetNextExecution()
    {
      return ValueTask.FromResult(ToSpan(ExecuteTime));
    }
    
    private static TimeSpan ToSpan(TimeOnly i_DateTime)
    {
      var nowTime = DateTime.Now;
      var mergedTime = new DateTime(nowTime.Year, nowTime.Month, nowTime.Day, i_DateTime.Hour, i_DateTime.Minute, i_DateTime.Second, i_DateTime.Millisecond);
      if (mergedTime < nowTime)
      {
        mergedTime = mergedTime.AddDays(1);
      }
      return mergedTime - nowTime;
    }
    
    /// <summary>
    /// Get String representation
    /// </summary>
    /// <returns></returns>
    public override string GetStringRepresentation()
    {
      return "Exact Time: " + ExecuteTime;
    }
  }
}
