using System;
using System.Threading.Tasks;

namespace NiceCore.Services.Scheduling.Entries.EntryTimeModels
{
  /// <summary>
  /// Base Schedule Entry Time Model
  /// </summary>
  public abstract class BaseScheduleEntryTimeModel : IScheduleEntryTimeModel
  {
    /// <summary>
    /// Is Time Model Enabled
    /// </summary>
    public bool IsTimeModelEnabled { get; init; }
    
    /// <summary>
    /// Is Enabled
    /// </summary>
    /// <returns></returns>
    public virtual ValueTask<bool> IsEnabled()
    {
      return  ValueTask.FromResult(IsTimeModelEnabled);
    }

    /// <summary>
    /// Abstract Get Next Execution
    /// </summary>
    /// <returns></returns>
    public abstract ValueTask<TimeSpan> GetNextExecution();
    
    /// <summary>
    /// Get String Representation
    /// </summary>
    /// <returns></returns>
    public abstract string GetStringRepresentation();
  }
}