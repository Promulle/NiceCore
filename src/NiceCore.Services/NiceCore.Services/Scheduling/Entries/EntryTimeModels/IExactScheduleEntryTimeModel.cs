﻿using System;

namespace NiceCore.Services.Scheduling.Entries.EntryTimeModels
{
  /// <summary>
  /// Entry that should run at a fixed time
  /// </summary>
  public interface IExactScheduleEntryTimeModel : IScheduleEntryTimeModel
  {
    /// <summary>
    /// Time this entry should be executed at
    /// </summary>
    TimeOnly ExecuteTime { get; set; }
  }
}
