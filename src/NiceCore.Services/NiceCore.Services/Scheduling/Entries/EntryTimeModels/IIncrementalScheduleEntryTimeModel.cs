﻿using System;

namespace NiceCore.Services.Scheduling.Entries.EntryTimeModels
{
  /// <summary>
  /// Entry that runs every x amount
  /// </summary>
  public interface ITimeSpanScheduleEntryTimeModel : IScheduleEntryTimeModel
  {
    /// <summary>
    /// The Span between executions
    /// </summary>
    TimeSpan SpanBetweenExecutions { get; set; }
  }
}
