using System;
using System.Threading.Tasks;
using Cronos;
using Microsoft.Extensions.Logging;
using NiceCore.Diagnostics;
using NiceCore.Services.Config;
using NiceCore.Services.Scheduling.Configuration.Creators.Entries;
using NiceCore.Services.Scheduling.Configuration.Model;
using NiceCore.Services.Scheduling.Entries.EntryTimeModels.Impls;

namespace NiceCore.Services.Scheduling.Entries
{
  /// <summary>
  /// Schedule Entry Creator
  /// </summary>
  public static class ScheduleEntryCreator
  {
    /// <summary>
    /// Create Time Span
    /// </summary>
    /// <param name="i_Schedulable"></param>
    /// <param name="i_TimeSpan"></param>
    /// <param name="i_ExecuteOnScheduled"></param>
    /// <returns></returns>
    public static IScheduleEntry CreateTimeSpan(ISchedulable i_Schedulable, TimeSpan i_TimeSpan, bool i_ExecuteOnScheduled)
    {
      return CreateTimeSpan(i_Schedulable, i_TimeSpan, i_ExecuteOnScheduled, false);
    }

    /// <summary>
    /// Create Time Span
    /// </summary>
    /// <param name="i_Schedulable"></param>
    /// <param name="i_TimeSpan"></param>
    /// <param name="i_ExecuteOnScheduled"></param>
    /// <param name="i_CollectMetrics"></param>
    /// <returns></returns>
    public static IScheduleEntry CreateTimeSpan(ISchedulable i_Schedulable, TimeSpan i_TimeSpan, bool i_ExecuteOnScheduled, bool i_CollectMetrics)
    {
      return CreateEntry(i_Schedulable, new DefaultTimeSpanScheduleEntryTimeModel()
      {
        SpanBetweenExecutions = i_TimeSpan,
        IsTimeModelEnabled = true
      }, i_ExecuteOnScheduled, i_CollectMetrics, null);
    }
    
    /// <summary>
    /// Create Exact
    /// </summary>
    /// <param name="i_Schedulable"></param>
    /// <param name="i_ExecutionTime"></param>
    /// <param name="i_ExecuteOnScheduled"></param>
    /// <returns></returns>
    public static IScheduleEntry CreateExact(ISchedulable i_Schedulable, TimeOnly i_ExecutionTime, bool i_ExecuteOnScheduled)
    {
      return CreateExact(i_Schedulable, i_ExecutionTime, i_ExecuteOnScheduled, false);
    }

    /// <summary>
    /// Create Exact
    /// </summary>
    /// <param name="i_Schedulable"></param>
    /// <param name="i_ExecutionTime"></param>
    /// <param name="i_ExecuteOnScheduled"></param>
    /// <param name="i_CollectMetrics"></param>
    /// <returns></returns>
    public static IScheduleEntry CreateExact(ISchedulable i_Schedulable, TimeOnly i_ExecutionTime, bool i_ExecuteOnScheduled, bool i_CollectMetrics)
    {
      return CreateEntry(i_Schedulable, new DefaultExactScheduleEntryTimeModel()
      {
        ExecuteTime = i_ExecutionTime,
        IsTimeModelEnabled = true
      }, i_ExecuteOnScheduled, i_CollectMetrics, null);
    }
    
    /// <summary>
    /// Create Entry
    /// </summary>
    /// <param name="i_Schedulable"></param>
    /// <param name="i_CronExpression"></param>
    /// <param name="i_ExecuteOnScheduled"></param>
    /// <returns></returns>
    public static IScheduleEntry CreateCron(ISchedulable i_Schedulable, string i_CronExpression, bool i_ExecuteOnScheduled)
    {
      return CreateCron(i_Schedulable, i_CronExpression, i_ExecuteOnScheduled, false);
    }

    /// <summary>
    /// Create Entry
    /// </summary>
    /// <param name="i_Schedulable"></param>
    /// <param name="i_CronExpression"></param>
    /// <param name="i_ExecuteOnScheduled"></param>
    /// <param name="i_CollectMetrics"></param>
    /// <returns></returns>
    public static IScheduleEntry CreateCron(ISchedulable i_Schedulable, string i_CronExpression, bool i_ExecuteOnScheduled, bool i_CollectMetrics)
    {
      return CreateCron(i_Schedulable, i_CronExpression, i_ExecuteOnScheduled, i_CollectMetrics, null);
    }

    /// <summary>
    /// Create Entry
    /// </summary>
    /// <param name="i_Schedulable"></param>
    /// <param name="i_CronExpression"></param>
    /// <param name="i_ExecuteOnScheduled"></param>
    /// <param name="i_CollectMetrics"></param>
    /// <param name="i_MetricName"></param>
    /// <returns></returns>
    public static IScheduleEntry CreateCron(ISchedulable i_Schedulable, string i_CronExpression, bool i_ExecuteOnScheduled, bool i_CollectMetrics, string i_MetricName)
    {
      return CreateEntry(i_Schedulable, new CronExpressionScheduleEntryTimeModel(i_CronExpression)
      {
        IsTimeModelEnabled = true
      }, i_ExecuteOnScheduled, i_CollectMetrics, i_MetricName);
    }

    /// <summary>
    /// Create Entry
    /// </summary>
    /// <param name="i_Schedulable"></param>
    /// <param name="i_CronExpression"></param>
    /// <param name="i_ExecuteOnScheduled"></param>
    /// <param name="i_Logger"></param>
    /// <returns></returns>
    public static IScheduleEntry CreateCronIncludingSeconds(ISchedulable i_Schedulable, string i_CronExpression, bool i_ExecuteOnScheduled, ILogger i_Logger)
    {
      return CreateCronIncludingSeconds(i_Schedulable, i_CronExpression, i_ExecuteOnScheduled, false, i_Logger);
    }

    /// <summary>
    /// Create Entry
    /// </summary>
    /// <param name="i_Schedulable"></param>
    /// <param name="i_CronExpression"></param>
    /// <param name="i_ExecuteOnScheduled"></param>
    /// <param name="i_CollectMetrics"></param>
    /// <param name="i_Logger"></param>
    /// <returns></returns>
    public static IScheduleEntry CreateCronIncludingSeconds(ISchedulable i_Schedulable, string i_CronExpression, bool i_ExecuteOnScheduled, bool i_CollectMetrics, ILogger i_Logger)
    {
      return CreateEntry(i_Schedulable, new CronExpressionScheduleEntryTimeModel(i_CronExpression, CronFormat.IncludeSeconds, i_Logger)
      {
        IsTimeModelEnabled = true
      }, i_ExecuteOnScheduled, i_CollectMetrics, null);
    }

    /// <summary>
    /// Creates an entry from configuration
    /// </summary>
    /// <param name="i_Schedulable"></param>
    /// <param name="i_Model"></param>
    /// <param name="i_ConfigurationKey"></param>
    /// <param name="i_Configuration"></param>
    /// <param name="i_Logger"></param>
    /// <returns></returns>
    public static async ValueTask<IScheduleEntry> CreateEntryFromConfiguration(ISchedulable i_Schedulable, SchedulingServiceEntryConfigurationModel i_Model, string i_ConfigurationKey, INcConfiguration i_Configuration, ILogger i_Logger)
    {
      var timeModel = ScheduleEntryTimeModelCreator.CreateEntryTimeModel(i_Model, i_Logger);
      var entry = new ConfiguredScheduleEntry(timeModel)
      {
        Schedulable = i_Schedulable,
        ExecuteOnScheduled = i_Model.ExecuteOnScheduled,
        CollectMetrics = i_Model.CollectMetrics
      };
      if (i_ConfigurationKey != null && i_Configuration != null)
      {
        await entry.SetupForConfigurationReload(i_ConfigurationKey, i_Configuration).ConfigureAwait(false);
      }

      return entry;
    }

    /// <summary>
    /// Create An Entry
    /// </summary>
    /// <param name="i_Schedulable"></param>
    /// <param name="i_TimeModel"></param>
    /// <param name="i_ExecuteOnScheduled"></param>
    /// <param name="i_CollectMetrics"></param>
    /// <param name="i_MetricName"></param>
    /// <returns></returns>
    public static IScheduleEntry CreateEntry(ISchedulable i_Schedulable, IScheduleEntryTimeModel i_TimeModel, bool i_ExecuteOnScheduled, bool i_CollectMetrics, string i_MetricName)
    {
      return new ScheduleEntry(i_TimeModel)
      {
        Schedulable = i_Schedulable,
        ExecuteOnScheduled = i_ExecuteOnScheduled,
        CollectMetrics = i_CollectMetrics,
        MetricName = i_MetricName
      };
    }
  }
}