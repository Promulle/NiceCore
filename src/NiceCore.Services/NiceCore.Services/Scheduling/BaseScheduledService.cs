using System.Threading.Tasks;
using NiceCore.Base.Services;
using NiceCore.Services.Scheduling.Entries;

namespace NiceCore.Services.Scheduling
{
  /// <summary>
  /// Base class for scheduled services
  /// </summary>
  public abstract class BaseScheduledService : BaseService, IScheduledService
  {
    /// <summary>
    /// Execute Async
    /// </summary>
    /// <returns></returns>
    public abstract ValueTask<bool> ExecuteAsync();

    /// <summary>
    /// Create the scheduled entry
    /// </summary>
    /// <returns></returns>
    public abstract ValueTask<IScheduleEntry> CreateScheduleEntry();
  }
}