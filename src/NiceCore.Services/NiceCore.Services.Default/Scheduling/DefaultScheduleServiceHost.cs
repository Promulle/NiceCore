﻿using NiceCore.ServiceLocation;
using NiceCore.Services.Scheduling;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Diagnostics;
using NiceCore.Logging.Extensions;
using NiceCore.Services.Scheduling.Configuration;
using NiceCore.Services.Scheduling.Entries;

namespace NiceCore.Services.Default.Scheduling
{
  /// <summary>
  /// Default impl of IScheduleServiceHost
  /// </summary>
  [InitializeOnResolve]
  [Register(InterfaceType = typeof(IScheduleServiceHost), LifeStyle = LifeStyles.Singleton)]
  public class DefaultScheduleServiceHost : IScheduleServiceHost, INeedsInjection
  {
    private List<ISchedule> m_Schedules;

    /// <summary>
    /// Whether this host is initialized or not
    /// </summary>
    public bool IsInitialized { get; set; }

    /// <summary>
    /// Allow Executed On Scheduled
    /// </summary>
    public bool AllowExecuteOnScheduled { get; set; } = true;

    /// <summary>
    /// Container for getting the services
    /// </summary>
    public IServiceContainer Container { get; set; }

    /// <summary>
    /// Schedule service host configurator
    /// </summary>
    public IScheduleServiceHostConfigurator ScheduleServiceHostConfigurator { get; set; }
    
    /// <summary>
    /// Schedule Entry metrics
    /// </summary>
    public IScheduleEntryMetrics ScheduleEntryMetrics { get; set; }

    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<DefaultScheduleServiceHost> Logger { get; set; } = DiagnosticsCollector.Instance.GetLogger<DefaultScheduleServiceHost>();

    private void InitializeSchedule()
    {
      m_Schedules = new List<ISchedule>
      {
        //the default schedule will ALWAYS be on entry 0
        GetDefaultSchedule()
      };
    }

  /// <summary>
    /// Returns all schedules this host is managing
    /// </summary>
    /// <returns></returns>
    public List<ISchedule> GetAllSchedules()
    {
      return m_Schedules;
    }

    /// <summary>
    /// Return the default schedule for this service host
    /// </summary>
    /// <returns></returns>
    public ISchedule GetDefaultSchedule()
    {
      if (m_Schedules == null || m_Schedules.Count == 0)
      {
        return new DefaultSchedule(Logger, ScheduleEntryMetrics);
      }

      return m_Schedules[0];
    }

    /// <summary>
    /// Adds a service to the default schedule
    /// </summary>
    /// <param name="i_Service"></param>
    public ValueTask AddService(IScheduledService i_Service)
    {
      return AddService(i_Service, GetDefaultSchedule());
    }

    /// <summary>
    /// Adds a service to a specific schedule
    /// </summary>
    /// <param name="i_Service"></param>
    /// <param name="i_Schedule"></param>
    public async ValueTask AddService(IScheduledService i_Service, ISchedule i_Schedule)
    {
      await AddScheduleEntry(await i_Service.CreateScheduleEntry().ConfigureAwait(false), i_Schedule).ConfigureAwait(false);
    }

    /// <summary>
    /// Add entry to the default schedule
    /// </summary>
    /// <param name="i_Entry"></param>
    public ValueTask AddScheduleEntry(IScheduleEntry i_Entry)
    {
      return AddScheduleEntry(i_Entry, GetDefaultSchedule());
    }

    /// <summary>
    /// Adds an entry to a schedule
    /// </summary>
    /// <param name="i_Entry"></param>
    /// <param name="i_Schedule"></param>
    public async ValueTask AddScheduleEntry(IScheduleEntry i_Entry, ISchedule i_Schedule)
    {
      if (i_Entry == null)
      {
        return;
      }

      if (!m_Schedules.Contains(i_Schedule))
      {
        m_Schedules.Add(i_Schedule);
      }
      
      await i_Schedule.AddToSchedule(i_Entry, AllowExecuteOnScheduled).ConfigureAwait(false);
    }

    /// <summary>
    /// Starts all schedules
    /// </summary>
    public async ValueTask StartScheduling()
    {
      foreach (var schedule in m_Schedules)
      {
        await schedule.Enable().ConfigureAwait(false);
      }
    }

    /// <summary>
    /// Stops all schedules
    /// </summary>
    public void StopScheduling()
    {
      foreach (var schedule in m_Schedules)
      {
        schedule.Disable();
      }
    }

    /// <summary>
    /// initializes the host
    /// </summary>
    public async ValueTask Initialize()
    {
      InitializeSchedule();
      if (Container != null)
      {
        if (Logger == null)
        {
          Logger = Container.Resolve<ILogger<DefaultScheduleServiceHost>>();
        }

        SetLoggerToSchedules();
        var services = Container.ResolveAll<IScheduledService>();
        foreach (var service in services)
        {
          Logger.Information($"Adding service {service.GetType().FullName} which was registered for IScheduledService to default schedule.");
          await AddService(service).ConfigureAwait(false);
          Container.Release(service);
        }
      }
      //Container is null but logger was filled a different way
      else if (Logger != null)
      {
        SetLoggerToSchedules();
      }

      if (ScheduleServiceHostConfigurator != null)
      {
        Logger.Information("Configurator is set, configuring schedule service host...");
        await ScheduleServiceHostConfigurator.Configure(this).ConfigureAwait(false);
      }
    }

    private void SetLoggerToSchedules()
    {
      //set logger for all schedules
      foreach (var schedule in m_Schedules)
      {
        if (schedule.Logger == null)
        {
          schedule.SetLogger(Logger);
        }
      }
    }

    /// <summary>
    /// Removes a service
    /// </summary>
    /// <param name="i_Service"></param>
    public void RemoveService(IScheduledService i_Service)
    {
      //delete entries from service from all schedules
      foreach (var schedule in m_Schedules)
      {
        foreach (var entry in new List<IScheduleEntry>(schedule.GetEntriesBySchedulable(i_Service)))
        {
          schedule.RemoveFromSchedule(entry);
        }
      }
    }
  }
}