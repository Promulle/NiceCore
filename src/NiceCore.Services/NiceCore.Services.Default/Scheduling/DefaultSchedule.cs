﻿using System;
using NiceCore.Base;
using NiceCore.Services.Scheduling;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Diagnostics;
using NiceCore.Logging.Extensions;
using NiceCore.Services.Scheduling.Entries;

namespace NiceCore.Services.Default.Scheduling
{
  /// <summary>
  /// Schedule
  /// </summary>
  public class DefaultSchedule : BaseDisposable, ISchedule
  {
    private readonly List<SchedulingTimer> m_Timers = new ();

    /// <summary>
    /// Whether this schedule is enabled or not
    /// </summary>
    public bool IsEnabled { get; protected set; }
    
    /// <summary>
    /// Metrics
    /// </summary>
    public IScheduleEntryMetrics Metrics { get; }

    /// <summary>
    /// Logger
    /// </summary>
    public ILogger Logger { get; private set; } = DiagnosticsCollector.Instance.GetLogger<DefaultSchedule>();
    
    /// <summary>
    /// Ctor
    /// </summary>
    public DefaultSchedule()
    {
    }

    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_Logger"></param>
    /// <param name="i_Metrics"></param>
    public DefaultSchedule(ILogger i_Logger, IScheduleEntryMetrics i_Metrics)
    {
      Logger = i_Logger;
      Metrics = i_Metrics;
    }
    
    /// <summary>
    /// Get all scheduled entries
    /// </summary>
    /// <returns></returns>
    public List<IScheduleEntry> GetAllScheduledEntries()
    {
      return m_Timers.Select(r => r.Entry).ToList();
    }

    /// <summary>
    /// Adds an entry to the schedule
    /// </summary>
    /// <param name="i_Entry"></param>
    /// <param name="i_AllowExecuteOnScheduled"></param>
    public async ValueTask AddToSchedule(IScheduleEntry i_Entry, bool i_AllowExecuteOnScheduled)
    {
      if (i_Entry != null)
      {
        await AddNewTimer(i_Entry, i_AllowExecuteOnScheduled).ConfigureAwait(false);
      }
    }

    /// <summary>
    /// Changes this schedules logger to i_Logger. This is usefull when a logger should be set after creation
    /// because the schedules creator did not have a logger at the time
    /// </summary>
    /// <param name="i_Logger"></param>
    public void SetLogger(ILogger i_Logger)
    {
      Logger = i_Logger;
    }

    /// <summary>
    /// Removes an entry from the schedule
    /// </summary>
    /// <param name="i_Entry"></param>
    public void RemoveFromSchedule(IScheduleEntry i_Entry)
    {
      Logger.Information($"Searching timer for entry: {i_Entry.Schedulable.GetType().FullName}");
      var timer = m_Timers.Find(r => r.Entry == i_Entry);
      if (timer != null)
      {
        Logger.Information("Timer found, stopping and deleting timer.");
        m_Timers.Remove(timer);
        timer.Dispose();
      }
      else
      {
        Logger.Debug($"No timer found for schedulable {i_Entry.Schedulable.GetType().FullName}");
      }
    }

    /// <summary>
    /// Remove all from Schedule where i_Predicate returns true
    /// </summary>
    /// <param name="i_Predicate"></param>
    /// <param name="i_StateParam"></param>
    /// <typeparam name="TParam"></typeparam>
    public void RemoveAllFromSchedule<TParam>(Func<TParam, IScheduleEntry, bool> i_Predicate, TParam i_StateParam)
    {
      foreach (var timer in m_Timers.ToArray())
      {
        if (i_Predicate.Invoke(i_StateParam, timer.Entry))
        {
          m_Timers.Remove(timer);
          timer.Dispose();
        }
      }
    }

    /// <summary>
    /// Searches for the entry that is scheduled for i_Schedulable
    /// </summary>
    /// <param name="i_Schedulable"></param>
    /// <returns></returns>
    public IEnumerable<IScheduleEntry> GetEntriesBySchedulable(ISchedulable i_Schedulable)
    {
      return m_Timers.Select(r => r.Entry)
        .Where(r => r.Schedulable == i_Schedulable);
    }

    private async ValueTask AddNewTimer(IScheduleEntry i_Entry, bool i_AllowExecuteOnScheduled)
    {
      var newTimer = new SchedulingTimer(Logger, i_Entry, Metrics, CancellationToken.None);
      Logger.Information($"Initializing new timer for entry {i_Entry.GetType().FullName} of schedulable {i_Entry.Schedulable.GetType().FullName}");
      if (i_AllowExecuteOnScheduled && i_Entry.ExecuteOnScheduled)
      {
        Logger.Information($"ExecuteOnScheduled is true -> executing entry {i_Entry.GetType().FullName} of schedulable {i_Entry.Schedulable.GetType().FullName}");
        await ScheduleEntryExecutor.ExecuteEntry(i_Entry, Metrics).ConfigureAwait(false);
      }
      m_Timers.Add(newTimer);
      if (IsEnabled)
      {
        await newTimer.Start().ConfigureAwait(false);
      }
    }

    /// <summary>
    /// Disables this schedule
    /// </summary>
    public void Disable()
    {
      if (IsEnabled)
      {
        foreach (var timer in m_Timers)
        {
          timer.Stop();
        }
        IsEnabled = false;
      }
    }

    /// <summary>
    /// Enables this Schedule
    /// </summary>
    public async ValueTask Enable()
    {
      if (!IsEnabled)
      {
        foreach (var timer in m_Timers)
        {
          await timer.Start().ConfigureAwait(false);
        }
        IsEnabled = true;
      }
    }

    /// <summary>
    /// Clean up timers
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    protected override void Dispose(bool i_IsDisposing)
    {
      if (m_Timers != null)
      {
        foreach (var timer in m_Timers)
        {
          timer.Dispose();
        }
      }
      base.Dispose(i_IsDisposing);
    }
  }
}
