﻿using NiceCore.Base.Services;
using NiceCore.Extensions;
using NiceCore.Logging;
using NiceCore.ServiceLocation;
using NiceCore.Services.Http.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using NiceCore.Logging.Extensions;

namespace NiceCore.Services.Default.Http.Converters
{
  /// <summary>
  /// Default impl for IConverterService
  /// </summary>
  [InitializeOnResolve]
  [Register(InterfaceType = typeof(IConverterService), LifeStyle = LifeStyles.Singleton)]
  public class DefaultConverterService : BaseService, IConverterService, INeedsInjection
  {
    private readonly Dictionary<string, IPayloadConverter> m_PayloadConverters = new();
    private readonly Dictionary<string, IResponseConverter> m_ResponseConverters = new();

    /// <summary>
    /// Container
    /// </summary>
    public IServiceContainer Container { get; set; }
    
    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<DefaultConverterService> Logger { get; set; }

    /// <summary>
    /// Initialize -> get registered converters
    /// </summary>
    protected override void InternalInitialize()
    {
      base.InternalInitialize();
      GetAllConverters(m_PayloadConverters);
      GetAllConverters(m_ResponseConverters);
    }

    private void GetAllConverters<TConverterInterface>(Dictionary<string, TConverterInterface> t_Dictionary) where TConverterInterface : IContentTypeBasedConverter
    {
      var payloadConverters = Container.ResolveAll<TConverterInterface>().ToList();
      Logger.Debug($"Resolved {payloadConverters.Count()} converters for {typeof(TConverterInterface).FullName}.");
      payloadConverters.ForEachItem(r =>
      {
        Logger.Debug($"Trying to add {r.GetType().FullName}.");
        if (r.ContentType == null)
        {
          var msg = $"Converter {r.GetType().FullName} does not define a ContentType. Set the property to do so.";
          Logger.LogError(msg);
          throw new InvalidOperationException(msg);
        }
        t_Dictionary[r.ContentType] = r;
      });
    }

    /// <summary>
    /// Returns the payload converter for i_ContentType, throws an exception if no converter is found for i_Contenttype
    /// </summary>
    /// <param name="i_ContentType"></param>
    /// <returns></returns>
    public IPayloadConverter GetPayloadConverter(string i_ContentType)
    {
      return GetForContentType(m_PayloadConverters, i_ContentType);
    }

    /// <summary>
    /// Returns the response converter for i_ContentType, throws an exception if no converter is found for i_Contenttype
    /// </summary>
    /// <param name="i_ContentType"></param>
    /// <returns></returns>
    public IResponseConverter GetResponseConverter(string i_ContentType)
    {
      return GetForContentType(m_ResponseConverters, i_ContentType);
    }

    private T GetForContentType<T>(Dictionary<string, T> i_Dictionary, string i_Key)
    {
      if (i_Dictionary.TryGetValue(i_Key, out var conv))
      {
        Logger.Information($"Found converter {conv.GetType().FullName} for type {i_Key}");
        return conv;
      }
      var msg = $"No Converter of type {typeof(T)} found for content type {i_Key}.";
      Logger.LogError(msg);
      throw new InvalidOperationException(msg);
    }
  }
}
