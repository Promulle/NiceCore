﻿using NiceCore.Base;
using NiceCore.Extensions;
using NiceCore.ServiceLocation;
using NiceCore.Services.Http;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace NiceCore.Services.Default.Http
{
  /// <summary>
  /// Default impl of IHttpClientHandlerService
  /// </summary>
  [Register(InterfaceType = typeof(IHttpClientHandlerService), LifeStyle = LifeStyles.Singleton)]
  public class DefaultHttpClientHandlerService : BaseDisposable, IHttpClientHandlerService
  {
    private readonly object m_LockObject = new();
    private readonly Dictionary<string, HttpClientHandler> m_Handlers = new();

    /// <summary>
    /// Returns the handler
    /// </summary>
    /// <param name="i_HandlerName"></param>
    /// <param name="i_UseCookies"></param>
    /// <param name="i_CertificateValidationHandler"></param>
    /// <returns></returns>
    public HttpClientHandler GetHandler(string i_HandlerName, bool i_UseCookies, Func<HttpRequestMessage, X509Certificate, X509Chain, SslPolicyErrors, bool> i_CertificateValidationHandler)
    {
      lock (m_LockObject)
      {
        if (!m_Handlers.TryGetValue(i_HandlerName, out var handler))
        {
          handler = CreateNewHandler(i_UseCookies, i_CertificateValidationHandler);
          m_Handlers[i_HandlerName] = handler;
        }
        return handler;
      }
    }

    /// <summary>
    /// Returns the handler
    /// </summary>
    /// <param name="i_HandlerName"></param>
    /// <returns></returns>
    public HttpClientHandler GetHandler(string i_HandlerName)
    {
      return GetHandler(i_HandlerName, true, null);
    }

    /// <summary>
    /// Returns the handler
    /// </summary>
    /// <param name="i_HandlerName"></param>
    /// <param name="i_UseCookies"></param>
    /// <returns></returns>
    public HttpClientHandler GetHandler(string i_HandlerName, bool i_UseCookies)
    {
      return GetHandler(i_HandlerName, i_UseCookies, null);
    }

    private static HttpClientHandler CreateNewHandler(bool i_UseCookies, Func<HttpRequestMessage, X509Certificate, X509Chain, SslPolicyErrors, bool> i_CertificateValidationHandler)
    {
      var handler = new HttpClientHandler()
      {
        UseCookies = i_UseCookies,
        CookieContainer = new CookieContainer(),
      };
      if (i_CertificateValidationHandler != null)
      {
        handler.ServerCertificateCustomValidationCallback = i_CertificateValidationHandler;
      }
      return handler;
    }

    /// <summary>
    /// Dispose
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    protected override void Dispose(bool i_IsDisposing)
    {
      m_Handlers.Values.ForEachItem(handler => handler.Dispose());
      base.Dispose(i_IsDisposing);
    }
  }
}
