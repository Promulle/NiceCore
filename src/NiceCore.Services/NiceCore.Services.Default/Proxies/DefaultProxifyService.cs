﻿using Castle.DynamicProxy;
using NiceCore.ServiceLocation;
using NiceCore.Services.Proxies;

namespace NiceCore.Services.Default.Proxies
{
  /// <summary>
  /// Default impl for IProxifyService
  /// </summary>
  [Register(InterfaceType = typeof(IProxifyService))]
  public class DefaultProxifyService : IProxifyService
  {
    private readonly static ProxyGenerator m_ProxyGenerator = new ProxyGenerator();

    /// <summary>
    /// Proxifies i_Instance with all interceptors given in i_Interceptors
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Instance"></param>
    /// <param name="i_Interceptors"></param>
    /// <returns></returns>
    public T Proxify<T>(T i_Instance, params IInterceptor[] i_Interceptors) where T : class
    {
      if (IsProxy(i_Instance))
      {
        return i_Instance;
      }
      return m_ProxyGenerator.CreateClassProxyWithTarget(i_Instance, i_Interceptors);
    }

    /// <summary>
    /// UnProxify i_Instance if it is a proxy, otherwise just return i_Instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Instance"></param>
    /// <returns></returns>
    public T UnProxify<T>(T i_Instance)
    {
      if (IsProxy(i_Instance))
      {
        var proxy = i_Instance as IProxyTargetAccessor;
        return (T) proxy?.DynProxyGetTarget();
      }
      return i_Instance;
    }

    /// <summary>
    /// Checks if i_Instance is a proxy or not
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Instance"></param>
    /// <returns></returns>
    public bool IsProxy<T>(T i_Instance)
    {
      return i_Instance is IProxyTargetAccessor;
    }
  }
}
