﻿namespace NiceCore.Services.Default
{
  /// <summary>
  /// Constants for NiceCore.Services.Default
  /// </summary>
  public static class NiceCoreServicesDefaultConstants
  {
    /// <summary>
    /// Assembly name
    /// </summary>
    public static readonly string ASSEMBLY_NAME = "NiceCore.Services.Default";
  }
}
