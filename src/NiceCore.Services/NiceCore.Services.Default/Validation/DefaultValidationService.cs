﻿using NiceCore.Base.Services;
using NiceCore.Extensions;
using NiceCore.Logging;
using NiceCore.ServiceLocation;
using NiceCore.Services.Validation;
using NiceCore.Services.Validation.Message;
using NiceCore.Services.Validation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Exceptions;
using NiceCore.Logging.Extensions;
using NiceCore.Services.Validation.Contexts;
using NiceCore.Services.Validation.Results;
using NiceCore.Services.Validation.Rules.Standalone;
using NiceCore.Services.Validation.Standalone;
using NiceCore.Services.Validation.Storage;

namespace NiceCore.Services.Default.Validation
{
  /// <summary>
  /// Default validation service impl
  /// </summary>
  [InitializeOnResolve]
  [Register(InterfaceType = typeof(IValidationService), LifeStyle = LifeStyles.Singleton)]
  public class DefaultValidationService : BaseService, IValidationService, INeedsInjection
  {
    private IValidatorStorage m_Validators;
    private readonly StandaloneValidatorFactory m_StandaloneValidatorFactory = new();
    
    /// <summary>
    /// Container
    /// </summary>
    public IServiceContainer Container { get; set; }

    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<DefaultValidationService> Logger { get; set; }
    
    /// <summary>
    /// Validation Result Value Transformer
    /// </summary>
    public INcValidationResultValueTransformer ValidationResultValueTransformer { get; set; }

    /// <summary>
    /// Initialize by resolving all validators and sorting them by operation
    /// </summary>
    protected override void InternalInitialize()
    {
      base.InternalInitialize();
      m_Validators = new DefaultValidatorStorage(Logger);
      var resolvedValidators = Container.ResolveAll<INcObjectValidator>();
      var allStandaloneRules = Container.ResolveAll<INcStandaloneValidationRule>();
      var standaloneValidators = m_StandaloneValidatorFactory.CreateValidators(allStandaloneRules);
      var allValidators = resolvedValidators.Concat(standaloneValidators);
      Logger.Information($"Resolved {allValidators.Count()} validators.");
      m_Validators.FillWith(allValidators);
    }

    /// <summary>
    /// Validate
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_Operation"></param>
    /// <param name="i_Node"></param>
    /// <param name="i_Context"></param>
    /// <returns></returns>
    public async Task<INcValidationResult> Validate(object i_Instance, string i_Operation, SubValidationNode i_Node, INcCustomValidationProcessContext i_Context)
    {
      if (i_Instance == null)
      {
        throw new ArgumentException("Instance parameter was null. Cannot validate null.");
      }
      if (string.IsNullOrEmpty(i_Operation) || string.IsNullOrWhiteSpace(i_Operation))
      {
        Logger.Warning("Operation was passed as null or empty. Skipping validation.");
        return CreateDefault(i_Instance);
      }
      var matchingValidators = m_Validators.GetMatchingValidators(i_Instance.GetType(), i_Operation).ToList();
      if (matchingValidators.Count > 0)
      {
        var validationContext = new DefaultNcValidationContext()
        {
          Validators = m_Validators
        };
        var processContext = new DefaultNcValidationProcessContext()
        {
          Operation = i_Operation,
          CustomContext = i_Context,
          ResultValueTransformer = ValidationResultValueTransformer
        };
        var results = new List<INcValidationResult>();
        foreach (var validator in matchingValidators)
        {
          validator.Initialize();
          results.Add(await RunValidator(validator, i_Instance, i_Operation, i_Node, validationContext, processContext).ConfigureAwait(false));
        }
        return MergeResults(results);
      }
      return CreateDefault(i_Instance);
    }

    private async Task<INcValidationResult> RunValidator(INcObjectValidator i_Validator,
      object i_Instance,
      string i_Operation,
      SubValidationNode i_Node,
      INcValidationContext i_ValidationContext,
      INcValidationProcessContext i_Context)
    {
      try
      {
        var validationRequest = new NcValidationRequest()
        {
          Call = new DefaultNcValidatorCallContext()
          {
            SubValidationNode = i_Node,
            Instance = i_Instance
          },
          Process = i_Context,
          Validation = i_ValidationContext
        };
        return await i_Validator.ValidateWithContext(validationRequest).ConfigureAwait(false);
      }
      catch (Exception ex)
      {
        var msg = ValidationFailureMessage.Of($"An error occured during execution of validator: {i_Validator.GetType().FullName}. {ExceptionUtils.FormatException(ex)}");
        Logger.LogError(msg.PlaceholderMessage, ex);
        return CreateExceptionResult(i_Operation, msg, i_Instance);
      }
    }

    private static INcValidationResult CreateExceptionResult(string i_Operation, ValidationFailureMessage i_Message, object i_Instance)
    {
      return new NcValidationResult()
      {
        Errors = new()
        {
          new ()
          {
            ProblemMessage = i_Message,
            Severity = ValidationSeverities.Error
          },
        },
        Warnings = new(),
        Operation = i_Operation,
        Info = new(),
        Instance = i_Instance
      };
    }
    
    private static INcValidationResult MergeResults(IEnumerable<INcValidationResult> i_Results)
    {
      var results = i_Results.ToList();
      var res = new NcValidationResult()
      {
        Errors = new(),
        Warnings = new(),
        Info = new(),
        Operation = results.First().Operation,
        Instance = results.First().Instance
      };
      results.ForEachItem(validationResult =>
      {
        res.Errors.AddRange(validationResult.Errors);
        res.Warnings.AddRange(validationResult.Warnings);
        res.Info.AddRange(validationResult.Info);
      });
      return res;
    }

    private static INcValidationResult CreateDefault(object i_Instance)
    {
      return new NcValidationResult()
      {
        Errors = new(),
        Warnings = new(),
        Instance = i_Instance,
        Info = new(),
      };
    }
  }
}
