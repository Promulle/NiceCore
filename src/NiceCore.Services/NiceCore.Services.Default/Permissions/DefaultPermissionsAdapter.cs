﻿using NiceCore.Logging;
using NiceCore.Permissions;
using NiceCore.ServiceLocation;
using NiceCore.Services.Permissions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Logging.Extensions;

namespace NiceCore.Services.Default.Permissions
{
  /// <summary>
  /// Default impl for IPermissionAdapter
  /// </summary>
  [Register(InterfaceType = typeof(IPermissionAdapter), LifeStyle = LifeStyles.Singleton)]
  public class DefaultPermissionsAdapter : IPermissionAdapter
  {
    /// <summary>
    /// Available use case validators
    /// </summary>
    public List<IPermissionUseCaseValidator> UseCaseValidators { get; set; } = new();

    /// <summary>
    /// Actual implementation of permission service
    /// </summary>
    public IPermissionService PermissionService { get; set; }

    /// <summary>
    /// User ID resolver
    /// </summary>
    public List<IPermissionUseCaseUserIDResolver> UserIDResolvers { get; set; }
    
    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<DefaultPermissionsAdapter> Logger { get; set; }

    /// <summary>
    /// IsAllowed
    /// </summary>
    /// <param name="i_UseCase"></param>
    /// <param name="i_Key"></param>
    /// <returns></returns>
    public async Task<bool> IsAllowed(IPermissionUseCase i_UseCase, IPermissionKey i_Key)
    {
      if (PermissionService == null)
      {
        Logger.Warning($"It was requested to check a use case for the permission to a key, but no actual implementation for {typeof(IPermissionService).Name} was found. Not Allowing operation. To fix this, implement the {typeof(IPermissionService).Name} interface and register the class for that interface.");
        return false; //defaulting to false is better since so noboby would be able to access protected information in case of permission system failure
      }
      var useCaseValidated = await ValidateUseCase(i_UseCase).ConfigureAwait(false);
      if (useCaseValidated)
      {
        var userId = await GetUserId(i_UseCase).ConfigureAwait(false);
        //if no key is given, but a user id is retrieved -> should be allowed -> this is the "any user is authenticated" case
        if (i_Key == null && userId != null)
        {
          return true;
        }
        return await PermissionService.IsAllowed(userId, i_Key, null).ConfigureAwait(false);
      }
      Logger.Warning("The provided use case is not valid.");
      return false;
    }

    private Task<object> GetUserId(IPermissionUseCase i_UseCase)
    {
      var resolver = UserIDResolvers.Find(r => r.UseCaseType.Equals(i_UseCase.GetType()));
      if (resolver == null)
      {
        Logger.Error($"No user id resolver defined for use cases of type {i_UseCase.GetType()}");
        throw new NotSupportedException("Could not retrieve user id from use case. Cannot check permission.");
      }
      return resolver.GetUserID(i_UseCase);
    }

    private Task<bool> ValidateUseCase(IPermissionUseCase i_UseCase)
    {
      var validator = UseCaseValidators.Find(r => r.UseCaseType.Equals(i_UseCase.GetType()));
      if (validator != null)
      {
        return validator.Validate(i_UseCase);
      }
      return Task.FromResult(true);
    }
  }
}
