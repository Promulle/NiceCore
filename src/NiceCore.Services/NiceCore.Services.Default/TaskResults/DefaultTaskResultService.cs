﻿using NiceCore.ServiceLocation;
using NiceCore.Services.TaskResults;

namespace NiceCore.Services.Default.TaskResults
{
  /// <summary>
  /// Default task result service impl
  /// </summary>
  [Register(InterfaceType = typeof(ITaskResultService), LifeStyle = LifeStyles.Singleton)]
  public class DefaultTaskResultService : ITaskResultService
  {
    private readonly TaskValueRetrievalFuncCache m_Cache = new();

    /// <summary>
    /// Impl for GetTaskResult
    /// </summary>
    /// <param name="i_TaskObject"></param>
    /// <returns></returns>
    public object GetTaskResult(object i_TaskObject)
    {
      var func = m_Cache.GetResultFunc(i_TaskObject.GetType());
      return func.Invoke(i_TaskObject);
    }
  }
}
