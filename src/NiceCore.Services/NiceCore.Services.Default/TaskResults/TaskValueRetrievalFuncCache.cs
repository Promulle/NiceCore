﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace NiceCore.Services.Default.TaskResults
{
  /// <summary>
  /// Cache that saves compiled lambdas used to retrieve the values of tasks
  /// </summary>
  public class TaskValueRetrievalFuncCache
  {
    private readonly object m_LockObject = new();
    private const string RESULT_NAME = "Result";
    private readonly Dictionary<Type, Func<object, object>> m_Funcs = new()
    {
      { typeof(Task), res => res }
    };

    /// <summary>
    /// Returns the func that can be used to retrieve teh result of a task
    /// </summary>
    /// <param name="i_Type">Actual type of Task -> Task of ? </param>
    /// <returns></returns>
    public Func<object, object> GetResultFunc(Type i_Type)
    {
      lock (m_LockObject)
      {
        if (m_Funcs.TryGetValue(i_Type, out var func))
        {
          return func;
        }
        var newFunc = CreateNewFunc(i_Type);
        m_Funcs[i_Type] = newFunc;
        return newFunc;
      }
    }

    private static Func<object, object> CreateNewFunc(Type i_Type)
    {
      var objParam = Expression.Parameter(typeof(object));
      var taskParamCasted = Expression.Convert(objParam, i_Type);
      var propertyAccess = Expression.Property(taskParamCasted, RESULT_NAME);
      return Expression.Lambda<Func<object, object>>(propertyAccess, objParam).Compile();
    }
  }
}
