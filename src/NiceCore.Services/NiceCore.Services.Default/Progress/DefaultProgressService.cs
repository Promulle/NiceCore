﻿using NiceCore.Base.Progress;
using NiceCore.Base.Services;
using NiceCore.ServiceLocation;
using NiceCore.Services.Progress;
using System;
using System.Collections.Generic;
using System.Threading;

namespace NiceCore.Services.Default.Progress
{
  /// <summary>
  /// Default implementation of IProgressService
  /// </summary>
  [Register(InterfaceType = typeof(IProgressService), LifeStyle = LifeStyles.Singleton)]
  [InitializeOnResolve]
  public class DefaultProgressService : BaseService, IProgressService
  {
    /// <summary>
    /// Dictionary holding currently tracked states
    /// </summary>
    public Dictionary<string, ProgressState> States { get; set; }
    /// <summary>
    /// Event that is fired when a progress is explicitly unregistered or due to being finished
    /// </summary>
    public event EventHandler<ProgressEventArgs> ProgressUnregistered;
    /// <summary>
    /// Event that is fired when the state of a progress has changed
    /// </summary>
    public event EventHandler<ProgressEventArgs> ProgressChanged;
    /// <summary>
    /// Event that is fired when a new progress has been registered
    /// </summary>
    public event EventHandler<ProgressEventArgs> NewProgressRegistered;
    /// <summary>
    /// Initializes the service
    /// </summary>
    protected override void InternalInitialize()
    {
      base.InternalInitialize();
      States = new Dictionary<string, ProgressState>();
    }
    /// <summary>
    /// Adds progress to an existing progress-object
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Progress"></param>
    public void AddProgress(string i_Name, long i_Progress)
    {
      if (States.TryGetValue(i_Name, out var state))
      {
        state.Current += i_Progress;
        Raise(ProgressChanged, new ProgressEventArgs() { State = state });
        if (state.IsDone)
        {
          UnregisterProgress(i_Name);
        }
      }
    }
    /// <summary>
    /// Refreshes the progress called i_Name with the other parameters as values
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Current"></param>
    /// <param name="i_Max"></param>
    /// <param name="i_IsIndeterminate"></param>
    public void RefreshProgress(string i_Name, long i_Current, long i_Max, bool i_IsIndeterminate)
    {
      if (States.TryGetValue(i_Name, out var state))
      {
        state.Current = i_Current;
        state.MaxValue = i_Max;
        state.IsIndeterminate = i_IsIndeterminate;
        Raise(ProgressChanged, new ProgressEventArgs() { State = state });
      }
    }
    /// <summary>
    /// Refreshes the progress called i_Name with the other parameters as values
    /// </summary>
    /// <param name="i_Current"></param>
    /// <param name="i_Max"></param>
    /// <param name="i_IsIndeterminate"></param>
    public void RefreshProgress(long i_Current, long i_Max, bool i_IsIndeterminate)
    {
      RefreshProgress(Thread.CurrentThread.Name, i_Current, i_Max, i_IsIndeterminate);
    }
    /// <summary>
    /// Returns the state of the progress that corresponds to i_Name.
    /// Returns null if not found.
    /// </summary>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    public ProgressState GetState(string i_Name)
    {
      States.TryGetValue(i_Name, out var state);
      return state;
    }
    /// <summary>
    /// Registers a new progress object to be tracked. i_Name usually corresponds to the ThreadName
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Current"></param>
    /// <param name="i_Max"></param>
    /// <param name="i_IsIndeterminate"></param>
    public void RegisterProgress(string i_Name, long i_Current, long i_Max, bool i_IsIndeterminate)
    {
      if (!States.ContainsKey(i_Name))
      {
        var state = new ProgressState()
        {
          Current = i_Current,
          MaxValue = i_Max,
          Name = i_Name,
          IsIndeterminate = i_IsIndeterminate,
        };
        States.Add(i_Name, state);
        Raise(NewProgressRegistered, new ProgressEventArgs() { State = state });
      }
    }
    /// <summary>
    /// Subtracts progress from the progress-object, that corresponds to i_Name.
    /// i_Progress should be smaller than 0 for this function to work correctly.
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Progress"></param>
    public void SubtractProgress(string i_Name, long i_Progress)
    {
      if (States.TryGetValue(i_Name, out var state))
      {
        if (state.Current - i_Progress < 0)
        {
          state.Current = 0;
        }
        else
        {
          state.Current += i_Progress;
        }
        Raise(ProgressChanged, new ProgressEventArgs() { State = state });
      }
    }
    /// <summary>
    /// Stops tracking of progress which corresponds to i_Name.
    /// This method is called if AddProgress reaches or surpasses max value of progress, but is explicit needed, when indeterminate
    /// </summary>
    /// <param name="i_Name"></param>
    public void UnregisterProgress(string i_Name)
    {
      if (States.ContainsKey(i_Name))
      {
        var state = States[i_Name];
        States.Remove(i_Name);
        Raise(ProgressUnregistered, new ProgressEventArgs() { State = state });
      }
    }
    /// <summary>
    /// Adds progress to an existing progress-object
    /// </summary>
    /// <param name="i_Progress"></param>
    public void AddProgress(long i_Progress)
    {
      AddProgress(Thread.CurrentThread.Name, i_Progress);
    }
    /// <summary>
    /// Subtracts progress from the progress-object, that corresponds to i_Name.
    /// i_Progress should be smaller than 0 for this function to work correctly.
    /// </summary>
    /// <param name="i_Progress"></param>
    public void SubtractProgress(long i_Progress)
    {
      SubtractProgress(Thread.CurrentThread.Name, i_Progress);
    }
  }
}
