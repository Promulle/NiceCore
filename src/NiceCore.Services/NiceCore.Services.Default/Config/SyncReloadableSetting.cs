using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using NiceCore.Logging.Extensions;
using NiceCore.Services.Config;

namespace NiceCore.Services.Default.Config
{
  /// <summary>
  /// Sync Reloadable Setting
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public class SyncReloadableSetting<T>: BaseReloadableSetting<T, Action<SettingReloadedEventArgs<T>>>, ISyncReloadableSetting<T>
  {
    private readonly object m_Lock = new();

    /// <summary>
    /// ctor
    /// </summary>
    /// <param name="i_ReloadFunc"></param>
    /// <param name="i_Key"></param>
    /// <param name="i_Context"></param>
    /// <param name="i_Logger"></param>
    public SyncReloadableSetting(Func<T> i_ReloadFunc, string i_Key, string i_Context, ILogger i_Logger) : base(i_ReloadFunc, i_Key, i_Context, i_Logger)
    {
    }

    /// <summary>
    /// Add a new listener that reacts on the changed setting. This also calls i_ListenerFunc if the current value is not default
    /// </summary>
    /// <param name="i_ListenerFunc"></param>
    /// <param name="i_DirectlyCallListener"></param>
    public void AddListener(Action<SettingReloadedEventArgs<T>> i_ListenerFunc, bool i_DirectlyCallListener)
    {
      lock (m_Lock)
      {
          m_Listeners.Add(i_ListenerFunc);
          //if value is already set, call the listener once if i_DirectlyCallListener is true
          if (i_DirectlyCallListener && !EqualityComparer<T>.Default.Equals(m_Value, default))
          {
            ExecuteListener(CreateArgs(), i_ListenerFunc);
          }
      }
    }

    private void ExecuteAllListeners(SettingReloadedEventArgs<T> i_Args)
    {
      foreach (var listener in m_Listeners)
      {
        listener.Invoke(i_Args);
      }
    }

    private void ExecuteListener(SettingReloadedEventArgs<T> i_Args, Action<SettingReloadedEventArgs<T>> i_Func)
    {
      try
      {
        i_Func.Invoke(i_Args);
      }
      catch (Exception ex)
      {
        Logger.Error(ex, $"Error executing listener of reloadable Setting for Key: {Key}");
      }
    }

    /// <summary>
    /// Return the value of this setting
    /// </summary>
    /// <returns></returns>
    public T GetValue()
    {
      lock (m_Lock)
      {
        return m_Value;
      }
    }

    /// <summary>
    /// Reload the setting
    /// </summary>
    public void Reload()
    {
      lock (m_Lock)
      {
        var newVal = m_ReloadFunc.Invoke();
        if (!Equals(m_Value, newVal))
        {
          m_Value = m_ReloadFunc.Invoke();
          ExecuteAllListeners(CreateArgs());
        }
      }
    }
  }
}