﻿using NiceCore.Base;
using NiceCore.Services.Config;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Logging.Extensions;

namespace NiceCore.Services.Default.Config
{
  /// <summary>
  /// Default impl for IReloadable Setting
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public class DefaultReloadableSetting<T> : BaseReloadableSetting<T, Func<SettingReloadedEventArgs<T>, ValueTask>>, IAsyncReloadableSetting<T>
  {
    private readonly SemaphoreSlim m_LockObject = new(1,1);

    /// <summary>
    /// Constructor filling needed things
    /// </summary>
    /// <param name="reloadFunc"></param>
    /// <param name="i_Key"></param>
    /// <param name="i_Context"></param>
    /// <param name="i_Logger"></param>
    public DefaultReloadableSetting(Func<T> reloadFunc, string i_Key, string i_Context, ILogger i_Logger): base(reloadFunc, i_Key, i_Context, i_Logger)
    {
    }

    /// <summary>
    /// Add a new listener that reacts on the changed setting. This also calls i_ListenerFunc if the current value is not default
    /// </summary>
    /// <param name="i_ListenerFunc"></param>
    /// <param name="i_DirectlyCallListener"></param>
    public async ValueTask AddListener(Func<SettingReloadedEventArgs<T>, ValueTask> i_ListenerFunc, bool i_DirectlyCallListener)
    {
      await m_LockObject.WaitAsync().ConfigureAwait(false);
      try
      {
        m_Listeners.Add(i_ListenerFunc);
        //if value is already set, call the listener once if i_DirectlyCallListener is true
        if (i_DirectlyCallListener && !EqualityComparer<T>.Default.Equals(m_Value, default))
        {
          await ExecuteListener(CreateArgs(), i_ListenerFunc).ConfigureAwait(false);
        }
      }
      finally
      {
        m_LockObject.Release();
      }
    }

    private async ValueTask ExecuteAllListeners(SettingReloadedEventArgs<T> i_Args)
    {
      foreach (var listener in m_Listeners)
      {
        await listener.Invoke(i_Args).ConfigureAwait(false);
      }
    }

    private async ValueTask ExecuteListener(SettingReloadedEventArgs<T> i_Args, Func<SettingReloadedEventArgs<T>, ValueTask> i_Func)
    {
      try
      {
        await i_Func.Invoke(i_Args).ConfigureAwait(false);
      }
      catch (Exception ex)
      {
        Logger.Error(ex, $"Error executing listener of reloadable Setting for Key: {Key}");
      }
    }

    /// <summary>
    /// Return the value of this setting
    /// </summary>
    /// <returns></returns>
    public async ValueTask<T> GetValue()
    {
      await m_LockObject.WaitAsync().ConfigureAwait(false);
      try
      {
        return m_Value;
      }
      finally
      {
        m_LockObject.Release();
      }
    }

    /// <summary>
    /// Reload the setting
    /// </summary>
    public async ValueTask Reload()
    {
      await m_LockObject.WaitAsync().ConfigureAwait(false);
      try
      {
        var newVal = m_ReloadFunc.Invoke();
        if (!Equals(m_Value, newVal))
        {
          m_Value = m_ReloadFunc.Invoke();
          await ExecuteAllListeners(CreateArgs()).ConfigureAwait(false);
        }
      }
      finally
      {
        m_LockObject.Release();
      }
    }
  }
}
