﻿using NiceCore.Services.Config;
using System.Collections.Generic;

namespace NiceCore.Services.Default.Config
{
  /// <summary>
  /// Comparer for NcConfigSource
  /// </summary>
  public class NcConfigSourceComparer : IComparer<INcConfigurationSource>
  {
    /// <summary>
    /// Compare by endpoint
    /// </summary>
    /// <param name="i_LeftSource"></param>
    /// <param name="i_RightSource"></param>
    /// <returns></returns>
    public int Compare(INcConfigurationSource i_LeftSource, INcConfigurationSource i_RightSource)
    {
      if (i_LeftSource.Priority < i_RightSource.Priority)
      {
        return -1;
      }
      else if (i_LeftSource.Priority > i_RightSource.Priority)
      {
        return 1;
      }
      return 0;
    }
  }
}
