﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using NiceCore.Base.Caching;
using NiceCore.Base.Services;
using NiceCore.Reflection;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Diagnostics;
using NiceCore.Logging.Extensions;
using NiceCore.Services.Config.Impl;
using NiceCore.Services.Config.Impl.Files;
using NiceCore.Services.Config.Utils;

namespace NiceCore.Services.Default.Config
{
  /// <summary>
  /// Default impl of INcConfiguration
  /// </summary>
  [Register(InterfaceType = typeof(INcConfiguration), LifeStyle = LifeStyles.Singleton)]
  public class DefaultNcConfiguration : BaseService, INcConfiguration, INeedsInjection, IHasConfigurationRoot
  {
    private readonly SimpleCache<Type, bool> m_GetValueTypeIsCollectionMap = new();
    private IConfigurationRoot m_Configuration;

    /// <summary>
    /// Service container
    /// </summary>
    public IServiceContainer Container { get; set; }

    /// <summary>
    /// Generally Disables Reload on Change for File Sources 
    /// </summary>
    public bool DisableReloadOnChange { get; set; }

    /// <summary>
    /// Configuration sources used to build the underlying configuration
    /// </summary>
    public IList<INcConfigurationSource> ConfigurationSources { get; set; }

    /// <summary>
    /// Whether the Configuration is ready to be used. Note: Not thread safe: might yield not correct result if the configuration is initialized at the same time this is queried
    /// </summary>
    public bool IsReady
    {
      get { return m_Configuration != null; }
    }

    /// <summary>
    /// Types of Configuration Sources to ignore
    /// </summary>
    public HashSet<Type> IgnoredConfigurationSources { get; } = new();

    /// <summary>
    /// Logger
    /// </summary>
    public ILogger Logger { get; set; } = DiagnosticsCollector.Instance.GetLogger<DefaultNcConfiguration>();

    /// <summary>
    /// Internal initialize
    /// </summary>
    protected override void InternalInitialize()
    {
      base.InternalInitialize();
      Logger.Information($"Initializing configuration with {ConfigurationSources.Count} sources.");
      var builder = new ConfigurationBuilder();
      //TODO: Why does the injection not work here?
      if (ConfigurationSources != null)
      {
        var configurations = new List<INcConfigurationSource>(ConfigurationSources) ;
        configurations.Sort(new NcConfigSourceComparer());
        configurations.ForEach(src =>
        {
          ApplySource(src, builder, Logger);
        });
      }
      m_Configuration = builder.Build();
      Logger.Information("Config build complete.");
    }

    /// <summary>
    /// Get The configuration Root
    /// </summary>
    /// <returns></returns>
    public IConfigurationRoot GetConfigurationRoot()
    {
      EnsureConfigInitialized();
      return m_Configuration;
    }

    /// <summary>
    /// Sets the correct logger
    /// </summary>
    /// <param name="i_Logger"></param>
    public void SetLogger(ILogger i_Logger)
    {
      Logger = i_Logger;
    }

    /// <summary>
    /// Returns all elements defined for key i_Key
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Key"></param>
    /// <returns></returns>
    public virtual List<T> GetCollection<T>(string i_Key)
    {
      EnsureConfigInitialized();
      if (i_Key != null)
      {
        Logger.Debug($"Searching for configuration collection values defined by key {i_Key}.");
        var formattedKey = ConfigurationKeyConverter.ConvertKeyToMSFormat(i_Key);
        Logger.Debug($"Key {i_Key} converted to MS format: {formattedKey}");
        var sect = m_Configuration.GetSection(formattedKey);
        if (sect != null)
        {
          //retrieve list from section -> if null set empty list
          var list = sect.Get<List<T>>() ?? new List<T>();
          if (list.Count == 0)
          {
            Logger.Warning($"Section for key {i_Key} was found but an empty list was returned. If this is unexpected, make sure that all values in the configuration can be cast to {typeof(T).Name}");
          }

          return list;
        }
      }

      Logger.Warning("Null key was passed to GetValue. Returning an empty list instead.");
      return new List<T>();
    }

    /// <summary>
    /// Searches the configuration for a value defined by i_Key
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Key"></param>
    /// <param name="i_DefaultValue"></param>
    /// <returns></returns>
    public virtual T GetValue<T>(string i_Key, T i_DefaultValue)
    {
      return GetValue(i_Key, typeof(T), i_DefaultValue);
    }

    /// <summary>
    /// Searches the configuration for a value defined by i_Key. i_Type is used for binding the result to an object. It is then cast to typeof(T) to be returned
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Key"></param>
    /// <param name="i_Type"></param>
    /// <param name="i_DefaultValue"></param>
    /// <returns></returns>
    public virtual T GetValue<T>(string i_Key, Type i_Type, T i_DefaultValue)
    {
      EnsureConfigInitialized();
      EnsureIsNotCollection(typeof(T), () => $"GetValue was called with a generic parameter of {typeof(T).FullName}, but this is a collection and not supported by {nameof(GetValue)}. Use {nameof(GetCollection)} instead.");
      if (i_Key != null)
      {
        Logger.Debug($"Searching for configuration value defined by key {i_Key}.");
        var formattedKey = ConfigurationKeyConverter.ConvertKeyToMSFormat(i_Key);
        Logger.Debug($"Key {i_Key} converted to MS format: {formattedKey}");
        try
        {
          if (typeof(T).IsPrimitive || typeof(T).IsEnum || typeof(string).IsAssignableFrom(typeof(T)))
          {
            return m_Configuration.GetValue(formattedKey, i_DefaultValue);
          }

          var complexVal = m_Configuration.GetSection(formattedKey).Get(i_Type);
          if (complexVal == null)
          {
            return i_DefaultValue;
          }

          if (complexVal is not T castedComplexVal)
          {
            throw new InvalidOperationException($"Tried getting value for key {formattedKey}, but value can not be cast to required type {typeof(T)}. It is of type {complexVal.GetType()}");
          }

          return castedComplexVal;
        }
        catch (Exception ex)
        {
          Logger.Warning(ex, $"Could not read setting for key {i_Key}, returning {i_DefaultValue} instead.");
        }

        return i_DefaultValue;
      }

      Logger.Warning($"Null key was passed to GetValue. Returning default value {i_DefaultValue} instead.");
      return default;
    }

    /// <summary>
    /// Returns a settings instance that can be automatically reloaded
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Key"></param>
    /// <param name="i_DefaultValue"></param>
    /// <returns></returns>
    public virtual async ValueTask<IAsyncReloadableSetting<T>> GetSetting<T>(string i_Key, T i_DefaultValue)
    {
      EnsureConfigInitialized();
      EnsureIsNotCollection(typeof(T), () => $"{nameof(GetSetting)} was called with a generic parameter of {typeof(T).FullName}, but this is a collection and not supported by {nameof(GetSetting)}. Use {nameof(GetCollectionSetting)} instead.");
      var inst = new DefaultReloadableSetting<T>(() => GetValue(i_Key, i_DefaultValue), i_Key, null, Logger);
      // ReSharper disable once AsyncVoidLambda -> I do not really have a choice here, So I just catch the error
      ChangeToken.OnChange(() => m_Configuration.GetReloadToken(), () => ReloadInstance(inst));
      await inst.Reload().ConfigureAwait(false);
      return inst;
    }
    
    /// <summary>
    /// Returns a settings instance that can be automatically reloaded
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Key"></param>
    /// <param name="i_DefaultValue"></param>
    /// <returns></returns>
    public virtual ISyncReloadableSetting<T> GetSyncReloadableSetting<T>(string i_Key, T i_DefaultValue)
    {
      EnsureConfigInitialized();
      EnsureIsNotCollection(typeof(T), () => $"{nameof(GetSetting)} was called with a generic parameter of {typeof(T).FullName}, but this is a collection and not supported by {nameof(GetSetting)}. Use {nameof(GetCollectionSetting)} instead.");
      var inst = new SyncReloadableSetting<T>(() => GetValue(i_Key, i_DefaultValue), i_Key, null, Logger);
      // ReSharper disable once AsyncVoidLambda -> I do not really have a choice here, So I just catch the error
      ChangeToken.OnChange(() => m_Configuration.GetReloadToken(), () => inst.Reload());
       inst.Reload();
      return inst;
    }

    /// <summary>
    /// Returns a settings instance that can be automatically reloaded
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Key"></param>
    /// <returns></returns>
    public virtual async ValueTask<IAsyncReloadableSetting<List<T>>> GetCollectionSetting<T>(string i_Key)
    {
      EnsureConfigInitialized();
      var inst = new DefaultReloadableSetting<List<T>>(() => GetCollection<T>(i_Key), i_Key, null, Logger);
      // ReSharper disable once AsyncVoidLambda -> I do not really have a choice here, So I just catch the error
      ChangeToken.OnChange(() => m_Configuration.GetReloadToken(), () => ReloadInstance(inst));
      await inst.Reload().ConfigureAwait(false);
      return inst;
    }

    private void ReloadInstance<T>(IAsyncReloadableSetting<T> i_Setting)
    {
      Task.Run(async () => await i_Setting.Reload()).HandleErrorsInContinueWith(Logger);
    }

    /// <summary>
    /// Returns a string representation of all available values that can be configured
    /// </summary>
    /// <returns></returns>
    public string GetAvailableValues()
    {
      EnsureConfigInitialized();
      return m_Configuration.GetDebugView();
    }

    

    private void EnsureIsNotCollection(Type i_Type, Func<string> i_ErrorMessageFunc)
    {
      var isCollection = m_GetValueTypeIsCollectionMap.GetOrCreate(i_Type, () => CheckIfCollection(i_Type));
      if (isCollection)
      {
        throw new NotSupportedException(i_ErrorMessageFunc.Invoke());
      }
    }

    private static bool CheckIfCollection(Type i_Type)
    {
      if (i_Type.GetGenericArguments().Length > 0)
      {
        return ReflectionUtils.ConstructGenericEnumerableType(i_Type).IsAssignableFrom(i_Type);
      }

      return false;
    }

    private void EnsureConfigInitialized()
    {
      if (m_Configuration == null)
      {
        throw new InvalidOperationException("Configuration was not initialized yet. Please make sure Initialize is called before.");
      }
    }

    private void ApplySource(INcConfigurationSource i_Source, ConfigurationBuilder t_Builder, ILogger m_Log)
    {
      try
      {
        if (IgnoredConfigurationSources.Count == 0 || !IgnoredConfigurationSources.Contains(i_Source.GetType()))
        {
          if (DisableReloadOnChange && i_Source is BaseFileNcConfigurationSource fileSource)
          {
            fileSource.ReloadOnChange = false;
          }

          i_Source.Apply(t_Builder);  
        }
      }
      catch (Exception ex)
      {
        m_Log.Error(ex, $"Configuration source of type {i_Source.GetType().FullName} could not be applied to builder.");
      }
    }

    /// <summary>
    /// Accessor - IConfiguration Compatibility
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public string this[string key]
    {
      get { return m_Configuration[key]; }
      set { m_Configuration[key] = value; }
    }

    /// <summary>
    /// Get section - IConfiguration Compatibility
    /// </summary>
    /// <param name="i_Key"></param>
    /// <returns></returns>
    public IConfigurationSection GetSection(string i_Key)
    {
      return m_Configuration.GetSection(i_Key);
    }

    /// <summary>
    /// Get Children - IConfiguration Compatibility
    /// </summary>
    /// <returns></returns>
    public IEnumerable<IConfigurationSection> GetChildren()
    {
      return m_Configuration.GetChildren();
    }

    /// <summary>
    /// get reload token - IConfiguration Compatibility
    /// </summary>
    /// <returns></returns>
    public IChangeToken GetReloadToken()
    {
      return m_Configuration.GetReloadToken();
    }
  }
}