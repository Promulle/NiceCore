﻿using NiceCore.Services.Config;
using NiceCore.Services.Default.Tests.Configuration.Impl;
using System.Collections.Generic;
using NUnit.Framework;

namespace NiceCore.Services.Default.Tests.Configuration
{
  /// <summary>
  /// tests for json assembly based configs
  /// </summary>
  [TestFixture]
  [Parallelizable]
  public class JSONAssemblyConfigurationSourceTests : BaseConfigurationSourceTest
  {
    /// <summary>
    /// Sources
    /// </summary>
    /// <returns></returns>
    protected override IEnumerable<INcConfigurationSource> GetSources()
    {
      return new List<INcConfigurationSource>() { new TestAssemblyJSONConfigurationSource() };
    }
  }
}
