﻿using NiceCore.Services.Config;
using NiceCore.Services.Default.Tests.Configuration.Impl;
using System.Collections.Generic;
using NUnit.Framework;

namespace NiceCore.Services.Default.Tests.Configuration
{
  /// <summary>
  /// Tests for xml config source
  /// </summary>
  [TestFixture]
  [Parallelizable]
  public class XMLConfigurationSourceTests : BaseConfigurationSourceTest
  {
    /// <summary>
    /// Sources
    /// </summary>
    /// <returns></returns>
    protected override IEnumerable<INcConfigurationSource> GetSources()
    {
      return new List<INcConfigurationSource>() { new TestXmlConfigurationSource() };
    }
  }
}
