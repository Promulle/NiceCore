﻿using NiceCore.Services.Config.Impl;
using NiceCore.Services.Config.Impl.Files;
using NiceCore.Services.Config.Impl.Files.XML;

namespace NiceCore.Services.Default.Tests.Configuration.Impl
{
  /// <summary>
  /// Test config
  /// </summary>
  public class TestXmlConfigurationSource : BaseXMLFileNcConfigurationSource
  {
    /// <summary>
    /// optional
    /// </summary>
    public override bool IsOptional { get; } = true;

    /// <summary>
    /// Reload
    /// </summary>
    public override bool ReloadOnChange { get; set; } = true;

    /// <summary>
    /// Priority
    /// </summary>
    public override int Priority { get; } = 0;

    /// <summary>
    /// Get FilePath
    /// </summary>
    /// <returns></returns>
    protected override string GetFilePath()
    {
      return "TestData.xml";
    }
  }
}
