﻿namespace NiceCore.Services.Default.Tests.Configuration.Impl
{
  /// <summary>
  /// Test object
  /// </summary>
  public class TestObject : ITestInterface
  {
    /// <summary>
    /// name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Number
    /// </summary>
    public int Number { get; set; }
  }
}
