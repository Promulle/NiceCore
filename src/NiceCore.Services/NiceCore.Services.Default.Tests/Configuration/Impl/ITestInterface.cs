namespace NiceCore.Services.Default.Tests.Configuration.Impl
{
  public interface ITestInterface
  {
    /// <summary>
    /// name
    /// </summary>
    public string Name { get; }

    /// <summary>
    /// Number
    /// </summary>
    public int Number { get; }
  }
}