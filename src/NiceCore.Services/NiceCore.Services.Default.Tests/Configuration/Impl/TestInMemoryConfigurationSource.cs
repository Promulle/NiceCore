﻿using NiceCore.Services.Config.Impl;
using System.Collections.Generic;

namespace NiceCore.Services.Default.Tests.Configuration.Impl
{
  /// <summary>
  /// Test in memory
  /// </summary>
  public class TestInMemoryConfigurationSource : BaseInMemoryNcConfigurationSource
  {
    /// <summary>
    /// Values
    /// </summary>
    public override IDictionary<string, string> Values { get; init; } = CreateValues();

    /// <summary>
    /// Priority
    /// </summary>
    public override int Priority { get; } = 0;

    private static IDictionary<string, string> CreateValues()
    {
      return new Dictionary<string, string>()
      {
        { "Key", "Value" },
        { "TestObject:Name", "Hans Paul" },
        { "TestObject:Number", "5" },
        { "Collection:One", "1" },
        { "Collection:Two", "2" },
        { "Collection:Three", "3" },
        { "Collection:Four", "4" },
        { "ObjectCollection:One:Name", "Hans Paul" },
        { "ObjectCollection:One:Number", "5" },
        { "ObjectCollection:Two:Name", "Heinz Gustav" },
        { "ObjectCollection:Two:Number", "10" },
        { "ObjectCollection:Three:Name", "Makise Kurisu" },
        { "ObjectCollection:Three:Number", "42" },
      };
    }
  }
}
