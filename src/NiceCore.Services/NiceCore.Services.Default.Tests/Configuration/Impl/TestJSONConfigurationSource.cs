﻿using NiceCore.Services.Config.Impl;
using NiceCore.Services.Config.Impl.Files;
using NiceCore.Services.Config.Impl.Files.Json;

namespace NiceCore.Services.Default.Tests.Configuration.Impl
{
  /// <summary>
  /// test impl
  /// </summary>
  public class TestJSONConfigurationSource : BaseJSONFileNcConfigurationSource
  {
    /// <summary>
    /// true
    /// </summary>
    public override bool IsOptional { get; } = true;

    /// <summary>
    /// False
    /// </summary>
    public override bool ReloadOnChange { get; set; } = false;

    /// <summary>
    /// Priority
    /// </summary>
    public override int Priority { get; } = 0;

    /// <summary>
    /// Filepath
    /// </summary>
    /// <returns></returns>
    protected override string GetFilePath()
    {
      return "TestData.json";
    }
  }
}
