﻿using NiceCore.Services.Config.Impl;

namespace NiceCore.Services.Default.Tests.Configuration.Impl
{
  /// <summary>
  /// Test impl
  /// </summary>
  public class TestEnvConfigurationSource : BaseEnvironmentNcConfigurationSource
  {
    /// <summary>
    /// prefix
    /// </summary>
    public static readonly string PREFIX = "NcTest_";

    /// <summary>
    /// Test prefix
    /// </summary>
    protected override string Prefix { get; } = PREFIX;

    /// <summary>
    /// Priority
    /// </summary>
    public override int Priority { get; } = 0;
  }
}
