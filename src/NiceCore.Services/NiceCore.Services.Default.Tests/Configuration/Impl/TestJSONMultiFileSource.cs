using System.IO;
using System.Reflection;
using NiceCore.Services.Config.Impl.Files.Json;

namespace NiceCore.Services.Default.Tests.Configuration.Impl
{
  public class TestJSONMultiFileSource : BaseJSONSimplePatternMultiFileNcConfigurationSource
  {
    public override int Priority { get; } = 0;
    
    public override bool IsOptional { get; } = true;
    
    public override bool ReloadOnChange { get; set; } = true;
    public override string GetFileDirectory()
    {
      return Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
    }

    public override string GetPattern()
    {
      return "*.config.json";
    }
  }
}