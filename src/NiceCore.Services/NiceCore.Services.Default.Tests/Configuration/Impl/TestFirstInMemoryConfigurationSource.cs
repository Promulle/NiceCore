﻿using NiceCore.Services.Config.Impl;
using System.Collections.Generic;

namespace NiceCore.Services.Default.Tests.Configuration.Impl
{
  /// <summary>
  /// In memory test 1
  /// </summary>
  public class TestFirstInMemoryConfigurationSource : BaseInMemoryNcConfigurationSource
  {
    /// <summary>
    /// Values
    /// </summary>
    public override IDictionary<string, string> Values { get; init; } = new Dictionary<string, string>()
    {
      { "Key", "0" }
    };

    /// <summary>
    /// Priority
    /// </summary>
    public override int Priority { get; } = 0;
  }
}
