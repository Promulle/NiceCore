﻿using NiceCore.Services.Config.Impl.AssemblyBased;

namespace NiceCore.Services.Default.Tests.Configuration.Impl
{
  /// <summary>
  /// Test impl
  /// </summary>
  public class TestAssemblyJSONConfigurationSource : BaseAssemblyJSONNcConfgigurationSource
  {
    /// <summary>
    /// optional
    /// </summary>
    public override bool IsOptional { get; } = true;

    /// <summary>
    /// Reload on change
    /// </summary>
    public override bool ReloadOnChange { get; set; } = false;

    /// <summary>
    /// Priority
    /// </summary>
    public override int Priority { get; } = 0;
  }
}
