﻿using NiceCore.Services.Config.Impl.AssemblyBased;

namespace NiceCore.Services.Default.Tests.Configuration.Impl
{
  /// <summary>
  /// Test
  /// </summary>
  public class TestAssemblyXMLConfigurationSource : BaseAssemblyXMLNcConfigurationSource
  {
    /// <summary>
    /// optional
    /// </summary>
    public override bool IsOptional { get; } = true;

    /// <summary>
    /// Reload
    /// </summary>
    public override bool ReloadOnChange { get; set; } = false;

    /// <summary>
    /// Priority
    /// </summary>
    public override int Priority { get; } = 0;
  }
}
