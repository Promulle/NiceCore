﻿using NiceCore.Services.Config.Impl;
using System.Collections.Generic;

namespace NiceCore.Services.Default.Tests.Configuration.Impl
{
  /// <summary>
  /// Test second in memory
  /// </summary>
  public class TestSecondInMemoryConfigurationSource : BaseInMemoryNcConfigurationSource
  {
    /// <summary>
    /// Values
    /// </summary>
    public override IDictionary<string, string> Values { get; init; } = new Dictionary<string, string>()
    {
      { "Key", "1" }
    };

    /// <summary>
    /// Priority
    /// </summary>
    public override int Priority { get; } = 1;
  }
}