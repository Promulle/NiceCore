﻿using FluentAssertions;
using NiceCore.Extensions;
using NiceCore.Services.Config;
using NiceCore.Services.Default.Config;
using NiceCore.Services.Default.Tests.Configuration.Impl;
using System.Collections.Generic;
using NUnit.Framework;

namespace NiceCore.Services.Default.Tests.Configuration
{
  /// <summary>
  /// Base Test for ConfigurationSources
  /// </summary>
  public abstract class BaseConfigurationSourceTest
  {
    /// <summary>
    /// Get Value
    /// </summary>
    [Test]
    public void GetValue()
    {
      EnvironmentSetup();
      var cfg = CreateConfig();
      var val = cfg.GetValue<string>("Key", null);
      val.Should().NotBeNull();
      val.Should().Be("Value");

      var objectVal = cfg.GetValue<TestObject>("TestObject", null);
      objectVal.Should().NotBeNull();
      objectVal.Name.Should().Be("Hans Paul");
      objectVal.Number.Should().Be(5);
    }

    /// <summary>
    /// Tests get Collection
    /// </summary>
    [Test]
    public void GetCollection()
    {
      EnvironmentSetup();
      var cfg = CreateConfig();
      var list = cfg.GetCollection<int>("Collection");
      list.Should().NotBeNull();
      list.Should().NotBeEmpty();
      list.Should().Contain(1);
      list.Should().Contain(2);
      list.Should().Contain(3);
      list.Should().Contain(4);

      var objList = cfg.GetCollection<TestObject>("ObjectCollection");
      objList.Should().NotBeNull();
      objList.Should().NotBeEmpty();
      objList.Should().HaveCount(3);
    }

    private INcConfiguration CreateConfig()
    {
      var conf = new DefaultNcConfiguration()
      {
        ConfigurationSources = new List<INcConfigurationSource>()
      };
      GetSources().ForEachItem(conf.ConfigurationSources.Add);
      conf.Initialize();
      return conf;
    }

    /// <summary>
    /// Return the corresponding sources
    /// </summary>
    /// <returns></returns>
    protected abstract IEnumerable<INcConfigurationSource> GetSources();

    /// <summary>
    /// maybe needed setup before each method is run
    /// </summary>
    protected virtual void EnvironmentSetup()
    {

    }
  }
}
