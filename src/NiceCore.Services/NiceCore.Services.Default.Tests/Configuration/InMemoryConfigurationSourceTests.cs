﻿using NiceCore.Services.Config;
using NiceCore.Services.Default.Tests.Configuration.Impl;
using System.Collections.Generic;
using NUnit.Framework;

namespace NiceCore.Services.Default.Tests.Configuration
{
  /// <summary>
  /// Test for inmemory source
  /// </summary>
  [TestFixture]
  [Parallelizable]
  public class InMemoryConfigurationSourceTests : BaseConfigurationSourceTest
  {
    /// <summary>
    /// Get sources
    /// </summary>
    /// <returns></returns>
    protected override IEnumerable<INcConfigurationSource> GetSources()
    {
      return new List<INcConfigurationSource>() { new TestInMemoryConfigurationSource() };
    }
  }
}
