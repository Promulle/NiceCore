using System.Collections.Generic;
using NiceCore.Services.Config;
using NiceCore.Services.Default.Tests.Configuration.Impl;
using NUnit.Framework;

namespace NiceCore.Services.Default.Tests.Configuration
{
  [Parallelizable]
  [TestFixture]
  public class JSONMultiFileConfigurationSourceTests : BaseConfigurationSourceTest
  {
    protected override IEnumerable<INcConfigurationSource> GetSources()
    {
      return new[]
      {
        new TestJSONMultiFileSource()
      };
    }
  }
}