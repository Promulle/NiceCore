﻿using NiceCore.Services.Config;
using NiceCore.Services.Default.Tests.Configuration.Impl;
using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace NiceCore.Services.Default.Tests.Configuration
{
  /// <summary>
  /// Tests for Environment Variables
  /// </summary>
  [TestFixture]
  [Parallelizable]
  public class EnvConfigurationSourceTests : BaseConfigurationSourceTest
  {
    /// <summary>
    /// Sources
    /// </summary>
    /// <returns></returns>
    protected override IEnumerable<INcConfigurationSource> GetSources()
    {
      return new List<INcConfigurationSource>() { new TestEnvConfigurationSource() };
    }

    /// <summary>
    /// Setup
    /// </summary>
    protected override void EnvironmentSetup()
    {
      var pre = TestEnvConfigurationSource.PREFIX;
      Environment.SetEnvironmentVariable($"{pre}Key", "Value");
      Environment.SetEnvironmentVariable($"{pre}TestObject__Name", "Hans Paul");
      Environment.SetEnvironmentVariable($"{pre}TestObject__Number", "5");
      Environment.SetEnvironmentVariable($"{pre}Collection__First", "1");
      Environment.SetEnvironmentVariable($"{pre}Collection__Second", "2");
      Environment.SetEnvironmentVariable($"{pre}Collection__Third", "3");
      Environment.SetEnvironmentVariable($"{pre}Collection__Fourth", "4");

      Environment.SetEnvironmentVariable($"{pre}ObjectCollection__First__Name", "Hans Paul");
      Environment.SetEnvironmentVariable($"{pre}ObjectCollection__First__Number", "5");

      Environment.SetEnvironmentVariable($"{pre}ObjectCollection__Second__Name", "Heinz Gustav");
      Environment.SetEnvironmentVariable($"{pre}ObjectCollection__Second__Number", "10");

      Environment.SetEnvironmentVariable($"{pre}ObjectCollection__Third__Name", "Makise Kurisu");
      Environment.SetEnvironmentVariable($"{pre}ObjectCollection__Third__Number", "42");
    }
  }
}
