﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using NiceCore.Services.Config;
using NiceCore.Services.Default.Config;
using NiceCore.Services.Default.Tests.Configuration.Impl;
using NUnit.Framework;

namespace NiceCore.Services.Default.Tests.Configuration
{
  /// <summary>
  /// Configuration Tests
  /// </summary>
  [TestFixture]
  [Parallelizable]
  public class ConfigurationTests
  {
    /// <summary>
    /// Tests that config initialization works
    /// </summary>
    [Test]
    public void ConfigInitialized()
    {
      var conf = new DefaultNcConfiguration();
      conf.Initialize();
      var val = conf.GetValue<string>("some random stuff", null);
      val.Should().BeNull();
    }

    /// <summary>
    /// Tests that an exception is thrown if configuration is not initialized before calls to getvalue
    /// </summary>
    [Test]
    public void ConfigNotInitialized()
    {
      var conf = new DefaultNcConfiguration();
      Action act = () => conf.GetValue<string>("some random stuff", null);
      act.Should().Throw<InvalidOperationException>();
    }

    /// <summary>
    /// Tests that sources are correctly sorted if more than one was applied
    /// </summary>
    [Test]
    public void ConfigSourcesSort()
    {
      var conf = new DefaultNcConfiguration()
      {
        ConfigurationSources = new List<INcConfigurationSource>()
      };
      conf.ConfigurationSources.Add(new TestSecondInMemoryConfigurationSource());
      conf.ConfigurationSources.Add(new TestFirstInMemoryConfigurationSource());
      conf.Initialize();
      var val = conf.GetValue<string>("Key", null);
      val.Should().NotBeNull();
      val.Should().Be("1");
    }

    /// <summary>
    /// Tests retrieving values using a complex key
    /// </summary>
    [Test]
    public void ConfigComplexKey()
    {
      var conf = new DefaultNcConfiguration()
      {
        ConfigurationSources = new List<INcConfigurationSource>()
      };
      conf.ConfigurationSources.Add(new TestJSONConfigurationSource());
      conf.Initialize();

      var name = conf.GetValue<string>("TestObject.Name", null);
      name.Should().NotBeNull();
      name.Should().Be("Hans Paul");
    }
    
    /// <summary>
    /// Tests retrieving values using a complex key
    /// </summary>
    [Test]
    public void ConfigCastResultValue()
    {
      var conf = new DefaultNcConfiguration()
      {
        ConfigurationSources = new List<INcConfigurationSource>()
      };
      conf.ConfigurationSources.Add(new TestJSONConfigurationSource());
      conf.Initialize();

      var name = conf.GetValue<ITestInterface>("TestObject", typeof(TestObject), null);
      name.Should().NotBeNull();
      name.Name.Should().Be("Hans Paul");
      name.Number.Should().Be(5);
    }

    /// <summary>
    /// Tests that getsetting can not be called with a collection
    /// </summary>
    [Test]
    public void GetSettingCanNotBeCalledWithCollection()
    {
      var conf = new DefaultNcConfiguration();
      conf.Initialize();
      var act = async () => await conf.GetSetting<List<string>>("some random stuff", null).ConfigureAwait(false);
      act.Should().ThrowAsync<NotSupportedException>();
    }

    /// <summary>
    /// Tests that GetValue can not be called with a collection
    /// </summary>
    [Test]
    public void GetValueCanNotBeCalledWithCollection()
    {
      var conf = new DefaultNcConfiguration();
      conf.Initialize();
      Func<List<string>> act = () => conf.GetValue<List<string>>("some random stuff", null);
      act.Should().Throw<NotSupportedException>();
    }
  }
}
