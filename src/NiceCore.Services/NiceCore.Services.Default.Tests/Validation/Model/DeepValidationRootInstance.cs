﻿namespace NiceCore.Services.Default.Tests.Validation.Model
{
  /// <summary>
  /// Instance to be deep validated
  /// </summary>
  public class DeepValidationRootInstance
  {
    /// <summary>
    /// Root Name
    /// </summary>
    public string RootName { get; set; }

    /// <summary>
    /// Sub Instance
    /// </summary>
    public DeepValidationLevel1Instance SubInstance { get; set; }

    /// <summary>
    /// Second Sub Instance
    /// </summary>
    public DeepValidationLevel1Instance SecondSubInstance { get; set; }
  }
}
