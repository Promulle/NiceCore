﻿using System.Collections.Generic;

namespace NiceCore.Services.Default.Tests.Validation.Model
{
  /// <summary>
  /// DeepValidation level 2
  /// </summary>
  public class DeepValidationLevel2Instance
  {
    /// <summary>
    /// Value
    /// </summary>
    public string Value { get; set; }

    /// <summary>
    /// Sub Collection
    /// </summary>
    public ISet<DeepValidationLevel3CollectionInstance> SubCollection { get; set; }
  }
}
