﻿namespace NiceCore.Services.Default.Tests.Validation.Model
{
  /// <summary>
  /// Deep Valiation level 3 collection instances
  /// </summary>
  public class DeepValidationLevel3CollectionInstance
  {
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Value
    /// </summary>
    public long Value { get; set; }
  }
}
