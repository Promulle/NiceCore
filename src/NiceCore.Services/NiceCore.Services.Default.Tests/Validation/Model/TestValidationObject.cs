﻿namespace NiceCore.Services.Default.Tests.Validation.Model
{
  /// <summary>
  /// Object
  /// </summary>
  public class TestValidationObject
  {
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Number
    /// </summary>
    public int Number { get; set; }
  }
}
