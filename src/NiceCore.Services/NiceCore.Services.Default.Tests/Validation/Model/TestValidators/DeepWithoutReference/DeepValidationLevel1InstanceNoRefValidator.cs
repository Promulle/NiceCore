﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NiceCore.Services.Validation;
using NiceCore.Services.Validation.Message;
using NiceCore.Services.Validation.Rules;

namespace NiceCore.Services.Default.Tests.Validation.Model.TestValidators.DeepWithoutReference
{
  /// <summary>
  /// Valdiator
  /// </summary>
  public class DeepValidationLevel1InstanceNoRefValidator : BaseTestValidator<DeepValidationLevel1Instance>
  {
    /// <summary>
    /// Rule sets
    /// </summary>
    /// <returns></returns>
    protected override Task<IEnumerable<INcValidationRuleSet<DeepValidationLevel1Instance>>> DefineRuleSets()
    {
      return Task.FromResult((IEnumerable<INcValidationRuleSet<DeepValidationLevel1Instance>>) new List<INcValidationRuleSet<DeepValidationLevel1Instance>>()
      {
        GetSubRuleSet(),
        GetDefaultRules()
      });
    }

    private static INcValidationRuleSet<DeepValidationLevel1Instance> GetDefaultRules()
    {
      var rules = new List<INcValidationRule<DeepValidationLevel1Instance>>()
      {
        new PredicateNcValidationRule<DeepValidationLevel1Instance, long>()
        {
          AccessorFunc = inst => inst.Level1Number,
          CheckFunc = (param, ctx) => Task.FromResult(param.PropertyValue > 10),
          PropertyName = nameof(DeepValidationLevel1Instance.Level1Number),
          FailureMessageFunc = (inst, val) => ValidationFailureMessage.Of("Level1-LevelNumber must be greater than 10"),
          Severity = ValidationSeverities.Error
        }
      };
      return new NcValidationRuleSet<DeepValidationLevel1Instance>()
      {
        Identifier = Guid.NewGuid().ToString(),
        SetCondition = _ => true,
        Rules = rules
      };
    }

    private static INcValidationRuleSet<DeepValidationLevel1Instance> GetSubRuleSet()
    {
      var rules = new List<INcValidationRule<DeepValidationLevel1Instance>>()
      {
        new SubNcValidationRule<DeepValidationLevel1Instance, DeepValidationLevel2Instance, DeepValidationLevel2Instance>()
        {
          PropertyAccessorFunc = inst => inst.Level2Instance,
          PropertyName = nameof(DeepValidationLevel1Instance.Level2Instance)
        }
      };
      return new NcValidationRuleSet<DeepValidationLevel1Instance>()
      {
        Identifier = Guid.NewGuid().ToString(),
        SetCondition = _ => true,
        Rules = rules
      };
    }
  }
}
