﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NiceCore.Services.Default.Tests.Validation.Model.TestValidators.DeepWithReference;
using NiceCore.Services.Validation;
using NiceCore.Services.Validation.Message;
using NiceCore.Services.Validation.Rules;

namespace NiceCore.Services.Default.Tests.Validation.Model.TestValidators.DeepWithoutReference
{
  /// <summary>
  /// Deep Validition root validator
  /// </summary>
  public class DeepValidationRootNoRefValidator : BaseTestValidator<DeepValidationRootInstance>
  {
    /// <summary>
    /// Define Rulesets
    /// </summary>
    /// <returns></returns>
    protected override Task<IEnumerable<INcValidationRuleSet<DeepValidationRootInstance>>> DefineRuleSets()
    {

      return Task.FromResult((IEnumerable<INcValidationRuleSet<DeepValidationRootInstance>>) new List<INcValidationRuleSet<DeepValidationRootInstance>>()
      {
        GetSubRuleSet(),
        GetDefaultRules()
      });
    }

    private static INcValidationRuleSet<DeepValidationRootInstance> GetDefaultRules()
    {
      var rules = new List<INcValidationRule<DeepValidationRootInstance>>()
      {
        new PredicateNcValidationRule<DeepValidationRootInstance, string>()
        {
          AccessorFunc = inst => inst.RootName,
          CheckFunc = (param, ctx) => Task.FromResult(param.PropertyValue != null),
          PropertyName = nameof(DeepValidationRootInstance.RootName),
          FailureMessageFunc = (inst, val) => ValidationFailureMessage.Of("RootName must not be null"),
          Severity = ValidationSeverities.Error
        }
      };
      return new NcValidationRuleSet<DeepValidationRootInstance>()
      {
        Identifier = Guid.NewGuid().ToString(),
        SetCondition = _ => true,
        Rules = rules
      };
    }

    private static INcValidationRuleSet<DeepValidationRootInstance> GetSubRuleSet()
    {
      var rules = new List<INcValidationRule<DeepValidationRootInstance>>()
      {
        new SubNcValidationRule<DeepValidationRootInstance, DeepValidationLevel1Instance, DeepValidationLevel1Instance>()
        {
          PropertyAccessorFunc = inst => inst.SubInstance,
          PropertyName = nameof(DeepValidationRootInstance.SubInstance)
        },
        new SubNcValidationRule<DeepValidationRootInstance, DeepValidationLevel1Instance, DeepValidationLevel1Instance>()
        {
          PropertyAccessorFunc = inst => inst.SecondSubInstance,
          PropertyName = nameof(DeepValidationRootInstance.SecondSubInstance)
        }
      };
      return new NcValidationRuleSet<DeepValidationRootInstance>()
      {
        Identifier = Guid.NewGuid().ToString(),
        SetCondition = _ => true,
        Rules = rules
      };
    }
  }
}
