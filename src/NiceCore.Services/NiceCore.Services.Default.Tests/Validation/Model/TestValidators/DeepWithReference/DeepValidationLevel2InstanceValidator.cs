﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NiceCore.Services.Validation;
using NiceCore.Services.Validation.Message;
using NiceCore.Services.Validation.Rules;

namespace NiceCore.Services.Default.Tests.Validation.Model.TestValidators.DeepWithReference
{
  /// <summary>
  /// Validator
  /// </summary>
  public class DeepValidationLevel2InstanceValidator : BaseTestValidator<DeepValidationLevel2Instance>
  {
    /// <summary>
    /// Define Rule sets
    /// </summary>
    /// <returns></returns>
    protected override Task<IEnumerable<INcValidationRuleSet<DeepValidationLevel2Instance>>> DefineRuleSets()
    {
      return Task.FromResult((IEnumerable<INcValidationRuleSet<DeepValidationLevel2Instance>>) new List<INcValidationRuleSet<DeepValidationLevel2Instance>>()
      {
        GetSubRuleSet(),
        GetDefaultRules()
      });
    }

    private static INcValidationRuleSet<DeepValidationLevel2Instance> GetDefaultRules()
    {
      var rules = new List<INcValidationRule<DeepValidationLevel2Instance>>()
      {
        new PredicateNcValidationRule<DeepValidationLevel2Instance, string>()
        {
          AccessorFunc = inst => inst.Value,
          CheckFunc = (param, ctx) => Task.FromResult(param.PropertyValue.Length > 10),
          PropertyName = nameof(DeepValidationLevel2Instance.Value),
          FailureMessageFunc = (inst, val) => ValidationFailureMessage.Of("Level2-Value must be longer than 10 characteers"),
          Severity = ValidationSeverities.Error
        }
      };
      return new NcValidationRuleSet<DeepValidationLevel2Instance>()
      {
        Identifier = Guid.NewGuid().ToString(),
        SetCondition = _ => true,
        Rules = rules
      };
    }

    private static INcValidationRuleSet<DeepValidationLevel2Instance> GetSubRuleSet()
    {
      var rules = new List<INcValidationRule<DeepValidationLevel2Instance>>()
      {
        new SubNcValidationRule<DeepValidationLevel2Instance, DeepValidationLevel3CollectionInstance, ISet<DeepValidationLevel3CollectionInstance>>()
        {
          PropertyAccessorFunc = inst => inst.SubCollection,
          PropertyName = nameof(DeepValidationLevel2Instance.SubCollection),
          SubValidator = new DeepValidationLevel3CollectionInstanceValidator()
        }
      };
      return new NcValidationRuleSet<DeepValidationLevel2Instance>()
      {
        Identifier = Guid.NewGuid().ToString(),
        SetCondition = _ => true,
        Rules = rules
      };
    }
  }
}
