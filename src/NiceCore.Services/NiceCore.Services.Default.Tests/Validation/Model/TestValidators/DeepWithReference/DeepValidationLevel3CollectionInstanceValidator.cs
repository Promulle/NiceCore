﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NiceCore.Services.Validation;
using NiceCore.Services.Validation.Message;
using NiceCore.Services.Validation.Rules;

namespace NiceCore.Services.Default.Tests.Validation.Model.TestValidators.DeepWithReference
{
  /// <summary>
  /// Valdiator
  /// </summary>
  public class DeepValidationLevel3CollectionInstanceValidator : BaseTestValidator<DeepValidationLevel3CollectionInstance>
  {
    /// <summary>
    /// Define Rule sets
    /// </summary>
    /// <returns></returns>
    protected override Task<IEnumerable<INcValidationRuleSet<DeepValidationLevel3CollectionInstance>>> DefineRuleSets()
    {
      return Task.FromResult((IEnumerable<INcValidationRuleSet<DeepValidationLevel3CollectionInstance>>) new List<INcValidationRuleSet<DeepValidationLevel3CollectionInstance>>()
      {
        GetDefaultRules()
      });
    }

    private static INcValidationRuleSet<DeepValidationLevel3CollectionInstance> GetDefaultRules()
    {
      var rules = new List<INcValidationRule<DeepValidationLevel3CollectionInstance>>()
      {
        new PredicateNcValidationRule<DeepValidationLevel3CollectionInstance, long>()
        {
          AccessorFunc = inst => inst.Value,
          CheckFunc = (param, ctx) => Task.FromResult(param.PropertyValue > 10),
          PropertyName = nameof(DeepValidationLevel3CollectionInstance.Value),
          FailureMessageFunc = (inst, val) => ValidationFailureMessage.Of("Level3-Value must be greater than 10 characteers"),
          Severity = ValidationSeverities.Error
        },
         new PredicateNcValidationRule<DeepValidationLevel3CollectionInstance, string>()
        {
          AccessorFunc = inst => inst.Name,
          CheckFunc = (param, ctx) => Task.FromResult(param.PropertyValue != null),
          PropertyName = nameof(DeepValidationLevel3CollectionInstance.Name),
          FailureMessageFunc = (inst, val) => ValidationFailureMessage.Of("Level3-Name must not be null"),
          Severity = ValidationSeverities.Error
        }
      };
      return new NcValidationRuleSet<DeepValidationLevel3CollectionInstance>()
      {
        Identifier = Guid.NewGuid().ToString(),
        SetCondition = _ => true,
        Rules = rules
      };
    }
  }
}
