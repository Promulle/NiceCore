﻿using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using NiceCore.Services.Validation.Validators.Base;

namespace NiceCore.Services.Default.Tests.Validation.Model.TestValidators
{
  /// <summary>
  /// Base test valiadtor
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public abstract class BaseTestValidator<T> : BaseNcRuleValidator<T>
  {
    /// <summary>
    /// Operations
    /// </summary>
    public override IEnumerable<string> Operations { get; } = new[] { "Validate" };

    /// <summary>
    /// logger
    /// </summary>
    public override ILogger Logger { get; } = NullLogger.Instance;
  }
}
