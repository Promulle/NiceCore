﻿namespace NiceCore.Services.Default.Tests.Validation.Model
{
  /// <summary>
  /// Class that i will not define a validator for
  /// </summary>
  public class SecondTestObject
  {
    /// <summary>
    /// Better name
    /// </summary>
    public string BetterName { get; set; }
  }
}
