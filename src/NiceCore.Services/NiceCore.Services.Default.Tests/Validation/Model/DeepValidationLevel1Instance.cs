﻿namespace NiceCore.Services.Default.Tests.Validation.Model
{
  /// <summary>
  /// Deep Validation level 1 Instance
  /// </summary>
  public class DeepValidationLevel1Instance
  {
    /// <summary>
    /// Level 1 Number
    /// </summary>
    public long Level1Number { get; set; }

    /// <summary>
    /// Level 2 Instance
    /// </summary>
    public DeepValidationLevel2Instance Level2Instance { get; set; }
  }
}
