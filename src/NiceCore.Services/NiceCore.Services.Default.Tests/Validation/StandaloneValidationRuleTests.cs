using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.Logging.Abstractions;
using NiceCore.ServiceLocation;
using NiceCore.ServiceLocation.CastleWindsor;
using NiceCore.Services.Default.Tests.Validation.Model;
using NiceCore.Services.Default.Tests.Validation.ValidationRules;
using NiceCore.Services.Default.Validation;
using NiceCore.Services.Validation.Rules.Standalone;
using NUnit.Framework;

namespace NiceCore.Services.Default.Tests.Validation
{
  [TestFixture]
  public class StandaloneValidationRuleTests
  {
    [Test]
    public async Task TestStandaloneRules()
    {
      var container = new CastleWindsorServiceContainer();
      container.Register<INcStandaloneValidationRule, SaveOrUpdateTestValidationRule>();
      container.Register<INcStandaloneValidationRule, DeleteTestValidationRule>();
      container.Register<INcStandaloneValidationRule, DeleteOrSaveOrUpdateValidationRule>();
      var validationService = new DefaultValidationService()
      {
        Container = container,
        Logger = NullLogger<DefaultValidationService>.Instance
      };
      var notValidTestObject = new TestValidationObject()
      {
        Number = 50,
      };
      var validTestObject = new TestValidationObject()
      {
        Number = 101,
        Name = string.Empty
      };
      
      validationService.Initialize();
      var validValidationResult = await validationService.Validate(validTestObject, SaveOrUpdateTestValidationRule.OPERATION, null, null).ConfigureAwait(false);
      var notValidValidationResult = await validationService.Validate(notValidTestObject, SaveOrUpdateTestValidationRule.OPERATION, null, null).ConfigureAwait(false);
      validValidationResult.Errors.Should().NotBeNull().And.BeEmpty();
      validValidationResult.Warnings.Should().NotBeNull().And.BeEmpty();
      validValidationResult.Info.Should().NotBeNull().And.BeEmpty();
      notValidValidationResult.Errors.Should().NotBeNullOrEmpty().And.HaveCount(2);
      notValidValidationResult.Errors.Should()
        .Contain(r => r.ProblemMessage.PlaceholderMessage == SaveOrUpdateTestValidationRule.EXPECTED_MESSAGE)
        .And.Contain(r => r.ProblemMessage.PlaceholderMessage == DeleteOrSaveOrUpdateValidationRule.MESSAGE);
    }
  }
}