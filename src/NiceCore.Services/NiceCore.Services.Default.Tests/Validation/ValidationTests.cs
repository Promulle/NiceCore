﻿using FluentAssertions;
using NiceCore.Extensions;
using NiceCore.ServiceLocation;
using NiceCore.Services.Default.Tests.Validation.Model;
using NiceCore.Services.Default.Validation;
using NiceCore.Services.Validation;
using NiceCore.Services.Validation.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Abstractions;
using NiceCore.ServiceLocation.CastleWindsor;
using NiceCore.Services.Default.Tests.Validation.Model.TestValidators.DeepWithoutReference;
using NiceCore.Services.Default.Tests.Validation.Model.TestValidators.DeepWithReference;
using NUnit.Framework;

namespace NiceCore.Services.Default.Tests.Validation
{
  /// <summary>
  /// Test class for validations
  /// </summary>
  [TestFixture]
  [Parallelizable]
  public class ValidationTests
  {
    /// <summary>
    /// Deep Validation
    /// </summary>
    /// <returns></returns>
    [Test]
    public async Task DeepValidationWithReferences()
    {
      var validators = new List<(Type interfaceType, Type implType)>()
      {
        { (typeof(INcObjectValidator), typeof(DeepValidationRootValidator)) },
        { (typeof(INcObjectValidator), typeof(DeepValidationLevel1InstanceValidator)) },
        { (typeof(INcObjectValidator), typeof(DeepValidationLevel2InstanceValidator)) },
        { (typeof(INcObjectValidator), typeof(DeepValidationLevel3CollectionInstanceValidator)) }
      };
      var container = SetupContainer(validators);
      var valService = SetupValidationService(container);
      valService.Initialize();
      var node = GetValidationNode();
      var instances = GetDeepInstances();
      var res = await valService.Validate(instances, "Validate", node, null).ConfigureAwait(false);
      res.Should().NotBeNull();
      res.Errors.Should().NotBeEmpty();
      res.Errors.Should().HaveCount(5);
    }
    
    /// <summary>
    /// Deep Validation
    /// </summary>
    /// <returns></returns>
    [Test]
    public async Task DeepValidationNoReferences()
    {
      var validators = new List<(Type interfaceType, Type implType)>()
      {
        { (typeof(INcObjectValidator), typeof(DeepValidationRootNoRefValidator)) },
        { (typeof(INcObjectValidator), typeof(DeepValidationLevel1InstanceNoRefValidator)) },
        { (typeof(INcObjectValidator), typeof(DeepValidationLevel2InstanceNoRefValidator)) },
        { (typeof(INcObjectValidator), typeof(DeepValidationLevel3CollectionInstanceNoRefValidator)) }
      };
      var container = SetupContainer(validators);
      var valService = SetupValidationService(container);
      valService.Initialize();
      var node = GetValidationNode();
      var instances = GetDeepInstances();
      var res = await valService.Validate(instances, "Validate", node, null).ConfigureAwait(false);
      res.Should().NotBeNull();
      res.Errors.Should().NotBeEmpty();
      res.Errors.Should().HaveCount(5);
    }

    private static SubValidationNode GetValidationNode()
    {
      return new SubValidationNode()
      {
        PropertyName = null,
        ChildNodes = new Dictionary<string, SubValidationNode>()
        {
          { nameof(DeepValidationRootInstance.SubInstance), new SubValidationNode()
            {
              PropertyName = nameof(DeepValidationRootInstance.SubInstance),
              ChildNodes = new Dictionary<string, SubValidationNode>()
              {
                { nameof(DeepValidationLevel1Instance.Level2Instance), new SubValidationNode()
                  {
                    PropertyName = nameof(DeepValidationLevel1Instance.Level2Instance),
                    ChildNodes = new Dictionary<string, SubValidationNode>()
                    {
                      { nameof(DeepValidationLevel2Instance.SubCollection), new SubValidationNode()
                        {
                          PropertyName = nameof(DeepValidationLevel2Instance.SubCollection),
                          ChildNodes = new Dictionary<string, SubValidationNode>()
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      };
    }

    private static DeepValidationRootInstance GetDeepInstances()
    {
      return new DeepValidationRootInstance()
      {
        RootName = null,
        SecondSubInstance = new DeepValidationLevel1Instance()
        {
          Level1Number = 3,
          Level2Instance = new DeepValidationLevel2Instance()
          {
            Value = "Hi"
          }
        },
        SubInstance = new DeepValidationLevel1Instance()
        {
          Level1Number = 1,
          Level2Instance = new DeepValidationLevel2Instance()
          {
            Value = "Hi",
            SubCollection = new HashSet<DeepValidationLevel3CollectionInstance>()
            {
              new DeepValidationLevel3CollectionInstance()
              {
                Name = null,
                Value = 3
              },
              new DeepValidationLevel3CollectionInstance()
              {
                Name = "asjkdasdöklasdjalök",
                Value = 255
              }
            }
          }
        }
      };
    }

    private static DefaultValidationService SetupValidationService(IServiceContainer i_Container)
    {
      return new DefaultValidationService()
      {
        Container = i_Container,
        Logger = NullLogger<DefaultValidationService>.Instance
      };
    }

    private static IServiceContainer SetupContainer(IEnumerable<(Type interfaceType, Type implType)> i_ValidatorTuples)
    {
      var container = new CastleWindsorServiceContainer();
      i_ValidatorTuples.ForEachItem(tuple => container.Register(tuple.interfaceType, tuple.implType));
      return container;
    }
  }
}
