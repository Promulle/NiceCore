using System.Collections.Generic;
using System.Threading.Tasks;
using NiceCore.Services.Default.Tests.Validation.Model;
using NiceCore.Services.Validation;
using NiceCore.Services.Validation.Contexts;
using NiceCore.Services.Validation.Rules.Standalone;

namespace NiceCore.Services.Default.Tests.Validation.ValidationRules
{
  public class SaveOrUpdateTestValidationRule : NcStandaloneValidationRule<TestValidationObject>
  {
    public static readonly string EXPECTED_MESSAGE = "Number below 100";
    public static readonly string OPERATION = "SaveOrUpdate";
    protected override ValidationSeverities GetSeverity()
    {
      return ValidationSeverities.Error;
    }

    public override IEnumerable<string> GetOperations()
    {
      return new[] {OPERATION};
    }

    protected override Task<NcStandaloneValidationResult> CheckInstance(TestValidationObject i_Instance, NcValidationRequest i_Request)
    {
      if (i_Instance.Number < 100)
      {
        return Task.FromResult(NcStandaloneValidationResult.NotValid(EXPECTED_MESSAGE));
      }
      return Task.FromResult(NcStandaloneValidationResult.Valid());
    }
  }
}