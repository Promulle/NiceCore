using System.Collections.Generic;
using System.Threading.Tasks;
using NiceCore.Services.Default.Tests.Validation.Model;
using NiceCore.Services.Validation;
using NiceCore.Services.Validation.Contexts;
using NiceCore.Services.Validation.Rules.Standalone;

namespace NiceCore.Services.Default.Tests.Validation.ValidationRules
{
  public class DeleteTestValidationRule : NcStandaloneValidationRule<TestValidationObject>
  {
    public static readonly string OPERATION = "Delete";
    public static readonly string MESSAGE = "Value should be above 100";
    protected override ValidationSeverities GetSeverity()
    {
      return ValidationSeverities.Error;
    }

    public override IEnumerable<string> GetOperations()
    {
      return new[] { OPERATION };
    }

    protected override Task<NcStandaloneValidationResult> CheckInstance(TestValidationObject i_Instance, NcValidationRequest i_Request)
    {
      if (i_Instance.Number > 100)
      {
        return Task.FromResult(NcStandaloneValidationResult.NotValid(MESSAGE));
      }

      return Task.FromResult(NcStandaloneValidationResult.Valid());
    }
  }
}