using System.Collections.Generic;
using System.Threading.Tasks;
using NiceCore.Services.Default.Tests.Validation.Model;
using NiceCore.Services.Validation;
using NiceCore.Services.Validation.Contexts;
using NiceCore.Services.Validation.Rules.Standalone;

namespace NiceCore.Services.Default.Tests.Validation.ValidationRules
{
  public class DeleteOrSaveOrUpdateValidationRule : NcStandaloneValidationRule<TestValidationObject>
  {
    public static readonly string MESSAGE = "Name must not be null";
    protected override ValidationSeverities GetSeverity()
    {
      return ValidationSeverities.Error;
    }

    public override IEnumerable<string> GetOperations()
    {
      return new[] { DeleteTestValidationRule.OPERATION, SaveOrUpdateTestValidationRule.OPERATION };
    }

    protected override Task<NcStandaloneValidationResult> CheckInstance(TestValidationObject i_Instance, NcValidationRequest i_Request)
    {
      if (i_Instance.Name != null)
      {
        return Task.FromResult(NcStandaloneValidationResult.Valid()); 
      }
      return Task.FromResult(NcStandaloneValidationResult.NotValid(MESSAGE));
    }
  }
}