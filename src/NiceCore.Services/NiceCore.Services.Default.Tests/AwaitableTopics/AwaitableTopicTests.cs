using System.Threading.Tasks;
using FluentAssertions;
using NiceCore.Services.AwaitableTopics;
using NUnit.Framework;

namespace NiceCore.Services.Default.Tests.AwaitableTopics
{
  [TestFixture]
  [Parallelizable]
  public class AwaitableTopicTests
  {
    private const string TOPIC_ONE = "Test-1";
    private const string TOPIC_TWO = "Test-2";
    
    [Test]
    [Parallelizable]
    public async Task TestAwaitableTopics()
    {
      var service = new AwaitableTopicService();
      var instOne = new StatefulInstance();
      var instTwo = new StatefulInstance();
      var instThree = new StatefulInstance();
      var task = Task.Run(async () =>
      {
        await using var topic = await service.Start(TOPIC_ONE).ConfigureAwait(false);
        await Task.Delay(100).ConfigureAwait(false);
        instOne.Flag = true;
      });
      var task2 = Task.Run(async () =>
      {
        await using var topic = await service.Start(TOPIC_ONE).ConfigureAwait(false);
        await Task.Delay(200).ConfigureAwait(false);
        instTwo.Flag = true;
      });
      var task3 = Task.Run(async () =>
      {
        await using var topic = await service.Start(TOPIC_TWO).ConfigureAwait(false);
        await Task.Delay(300).ConfigureAwait(false);
        instThree.Flag = true;
      });
      await Task.Delay(10).ConfigureAwait(false);
      await service.WaitForTopicCompletion(TOPIC_ONE).ConfigureAwait(false);
      instOne.Flag.Should().BeTrue();
      instTwo.Flag.Should().BeTrue();
      instThree.Flag.Should().BeFalse();
      
      await task.ConfigureAwait(false);
      await task2.ConfigureAwait(false);
      await task3.ConfigureAwait(false);
    }
    
    [Test]
    [Parallelizable]
    public async Task TestAddWhileAwaiting()
    {
      var service = new AwaitableTopicService();
      var instOne = new StatefulInstance();
      var instTwo = new StatefulInstance();
      var instThree = new StatefulInstance();
      var instFour = new StatefulInstance();
      var task = Task.Run(async () =>
      {
        await using var topic = await service.Start(TOPIC_ONE).ConfigureAwait(false);
        await Task.Delay(100).ConfigureAwait(false);
        instOne.Flag = true;
      });
      var task2 = Task.Run(async () =>
      {
        await using var topic = await service.Start(TOPIC_ONE).ConfigureAwait(false);
        await Task.Delay(200).ConfigureAwait(false);
        instTwo.Flag = true;
      });
      var task3 = Task.Run(async () =>
      {
        await using var topic = await service.Start(TOPIC_TWO).ConfigureAwait(false);
        await Task.Delay(700).ConfigureAwait(false);
        instThree.Flag = true;
      });
      await Task.Delay(10).ConfigureAwait(false);
      var awaitTask = service.WaitForTopicCompletion(TOPIC_ONE);
      var task4 = Task.Run(async () =>
      {
        await using var topic = await service.Start(TOPIC_ONE).ConfigureAwait(false);
        await Task.Delay(400).ConfigureAwait(false);
        instFour.Flag = true;
      });
      await awaitTask.ConfigureAwait(false);
      instOne.Flag.Should().BeTrue();
      instTwo.Flag.Should().BeTrue();
      instThree.Flag.Should().BeFalse();
      instFour.Flag.Should().BeTrue();
      
      await task.ConfigureAwait(false);
      await task2.ConfigureAwait(false);
      await task3.ConfigureAwait(false);
      await task4.ConfigureAwait(false);
    }
    
    [Test]
    [Parallelizable]
    public async Task TestAwaitMultiple()
    {
      var service = new AwaitableTopicService();
      var instOne = new StatefulInstance();
      var instTwo = new StatefulInstance();
      var instThree = new StatefulInstance();
      var task = Task.Run(async () =>
      {
        await using var topic = await service.Start(TOPIC_ONE).ConfigureAwait(false);
        await Task.Delay(100).ConfigureAwait(false);
        instOne.Flag = true;
      });
      var task2 = Task.Run(async () =>
      {
        await using var topic = await service.Start(TOPIC_ONE).ConfigureAwait(false);
        await Task.Delay(200).ConfigureAwait(false);
        instTwo.Flag = true;
      });
      var task3 = Task.Run(async () =>
      {
        await using var topic = await service.Start(TOPIC_TWO).ConfigureAwait(false);
        await Task.Delay(700).ConfigureAwait(false);
        instThree.Flag = true;
      });
      await Task.Delay(10).ConfigureAwait(false);
      var awaitTask = service.WaitForTopicCompletion(new []{ TOPIC_ONE, TOPIC_TWO });
      await awaitTask.ConfigureAwait(false);
      instOne.Flag.Should().BeTrue();
      instTwo.Flag.Should().BeTrue();
      instThree.Flag.Should().BeTrue();
      
      await task.ConfigureAwait(false);
      await task2.ConfigureAwait(false);
      await task3.ConfigureAwait(false);
    }
  }
}