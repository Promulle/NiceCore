using System;
using System.Collections.Generic;
using NiceCore.Services.Protocol.Model;

namespace NiceCore.Services.Default.Tests.Protocol.Model.TestImpl
{
  /// <summary>
  /// test context
  /// </summary>
  public class TestActionEntryContext : BaseTestImpl, IProtocolActionEntryContext<Guid>
  {
    /// <summary>
    /// Entries
    /// </summary>
    public List<IProtocolActionEntry<Guid>> Entries { get; } = new();
    
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }
    
    /// <summary>
    /// Add to child
    /// </summary>
    /// <param name="i_Instance"></param>
    public override void AddToChildInstances(IProtocolModel<Guid> i_Instance)
    {
      if (i_Instance is IProtocolActionEntry<Guid> entry)
      {
        AddToEntries(entry);
      }
    }

    /// <summary>
    /// Action Instance
    /// </summary>
    public TestAction ActionInstance { get; set; }
    
    /// <summary>
    /// Action
    /// </summary>
    public IProtocolAction<Guid> Action => ActionInstance;
    
    /// <summary>
    /// Add to entries
    /// </summary>
    /// <param name="i_Entry"></param>
    public void AddToEntries(IProtocolActionEntry<Guid> i_Entry)
    {
      if (i_Entry is TestActionEntry entry)
      {
        entry.ContextInstance = this;
      }
      Entries.Add(i_Entry);
    }
  }
}