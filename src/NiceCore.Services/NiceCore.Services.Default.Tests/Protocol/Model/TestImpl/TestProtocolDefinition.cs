using System;
using System.Collections.Generic;
using NiceCore.Services.Default.Tests.Protocol.Model.TestImpl.TypeProviders;
using NiceCore.Services.Protocol.Abstractions.ParentKeyAccessors;
using NiceCore.Services.Protocol.Abstractions.TypeProviders;
using NiceCore.Services.Protocol.Process;
using NiceCore.Services.Protocol.Process.Actions;
using NiceCore.Services.Protocol.Process.Matchers;

namespace NiceCore.Services.Default.Tests.Protocol.Model.TestImpl
{
  /// <summary>
  /// Test definition to be used
  /// </summary>
  public class TestProtocolDefinition : IProtocolProcessDefinition<Guid>
  {
    /// <summary>
    /// Actions
    /// </summary>
    public IReadOnlyCollection<IProtocolProcessAction> Actions { get; init; }
    
    /// <summary>
    /// Get matchers
    /// </summary>
    /// <returns></returns>
    public List<IProtocolRelatedMatcher<Guid>> GetMatchers()
    {
      return new()
      {
        new ContextMatcher()
      };
    }

    /// <summary>
    /// get instantiators
    /// </summary>
    /// <returns></returns>
    public List<INonGenericProtocolMatchedInstanceInstantiator> GetInstantiators()
    {
      return new()
      {
        new ContextMatcherInstantiator()
      };
    }

    /// <summary>
    /// Get type providers
    /// </summary>
    /// <returns></returns>
    public ProtocolImplementingTypeProviders GetImplementingTypeProviders()
    {
      return new()
      {
        ProtocolImplementingTypeProvider = new TestProtocolImplementingProvider(),
        ProtocolActionImplementingTypeProvider = new TestActionImplementingProvider(),
        ProtocolActionEntryImplementingTypeProvider = new TestActionEntryImplementingProvider(),
        ProtocolActionEntryContextImplementingTypeProvider = new TestActionEntryContextImplementingProvider()
      };
    }

    /// <summary>
    /// get process actions
    /// </summary>
    /// <returns></returns>
    public IReadOnlyCollection<IProtocolProcessAction> GetProcessActions()
    {
      return Actions;
    }

    /// <summary>
    /// return the parent key accesor
    /// </summary>
    /// <returns></returns>
    public IParentKeyAccessor<Guid> GetParentKeyAccessor()
    {
      return new TestParentKeyAccessor();
    }
  }
}