using System;
using NiceCore.Services.Protocol.Abstractions.Builders;
using NiceCore.Services.Protocol.Model;

namespace NiceCore.Services.Default.Tests.Protocol.Model.TestImpl.Builders
{
  /// <summary>
  /// Test Action Entry Builder
  /// </summary>
  public class TestActionEntryBuilder : BaseTestBuilder, IProtocolActionEntryBuilder<Guid>
  {
    /// <summary>
    /// Get Result type
    /// </summary>
    /// <returns></returns>
    public override Type GetResultType()
    {
      return typeof(TestActionEntry);
    }

    /// <summary>
    /// To Instance
    /// </summary>
    /// <returns></returns>
    public override IProtocolModel<Guid> ToInstance()
    {
      return ToEntry();
    }

    /// <summary>
    /// To Entry
    /// </summary>
    /// <returns></returns>
    public IProtocolActionEntry<Guid> ToEntry()
    {
      return new TestActionEntry()
      {
        Key = InstanceKey
      };
    }
  }
}