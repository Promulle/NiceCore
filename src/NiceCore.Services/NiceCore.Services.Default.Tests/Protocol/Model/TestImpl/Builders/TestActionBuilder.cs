using System;
using NiceCore.Services.Protocol.Abstractions.Builders;
using NiceCore.Services.Protocol.Model;

namespace NiceCore.Services.Default.Tests.Protocol.Model.TestImpl.Builders
{
  /// <summary>
  /// Test Action Builder
  /// </summary>
  public class TestActionBuilder : BaseTestBuilder, IProtocolActionBuilder<Guid>
  {
    /// <summary>
    /// Get result type
    /// </summary>
    /// <returns></returns>
    public override Type GetResultType()
    {
      return typeof(TestAction);
    }

    /// <summary>
    /// To Instance
    /// </summary>
    /// <returns></returns>
    public override IProtocolModel<Guid> ToInstance()
    {
      return ToAction();
    }

    /// <summary>
    /// To Action
    /// </summary>
    /// <returns></returns>
    public IProtocolAction<Guid> ToAction()
    {
      return new TestAction()
      {
        Key = InstanceKey
      };
    }
  }
}