using System;
using NiceCore.Services.Protocol.Abstractions.Builders;

namespace NiceCore.Services.Default.Tests.Protocol.Model.TestImpl.Builders
{
  /// <summary>
  /// Base Test Builder
  /// </summary>
  public abstract class BaseTestBuilder : BaseProtocolRelatedBuilder<Guid>
  {
    /// <summary>
    /// Base Test Builder
    /// </summary>
    public BaseTestBuilder()
    {
      InstanceKey = Guid.NewGuid();
    }
  }
}