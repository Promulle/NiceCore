using System;
using NiceCore.Services.Protocol.Abstractions.Builders;
using NiceCore.Services.Protocol.Model;

namespace NiceCore.Services.Default.Tests.Protocol.Model.TestImpl.Builders
{
  /// <summary>
  /// Test Action entry context builder
  /// </summary>
  public class TestActionEntryContextBuilder : BaseTestBuilder, IProtocolActionEntryContextBuilder<Guid>
  {
    private string m_Name;
    
    /// <summary>
    /// With Name
    /// </summary>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    public TestActionEntryContextBuilder WithName(string i_Name)
    {
      m_Name = i_Name;
      return this;
    }
    
    /// <summary>
    /// Get Result Type
    /// </summary>
    /// <returns></returns>
    public override Type GetResultType()
    {
      return typeof(TestActionEntryContext);
    }

    /// <summary>
    /// To Instance
    /// </summary>
    /// <returns></returns>
    public override IProtocolModel<Guid> ToInstance()
    {
      return ToEntryContext();
    }

    /// <summary>
    /// To Entry Context
    /// </summary>
    /// <returns></returns>
    public IProtocolActionEntryContext<Guid> ToEntryContext()
    {
      return new TestActionEntryContext()
      {
        Key = InstanceKey,
        Name = m_Name
      };
    }
  }
}