using System;
using NiceCore.Services.Protocol.Abstractions.Builders;
using NiceCore.Services.Protocol.Model;

namespace NiceCore.Services.Default.Tests.Protocol.Model.TestImpl.Builders
{
  /// <summary>
  /// Test Protocol Builder
  /// </summary>
  public class TestProtocolBuilder : BaseTestBuilder, IProtocolBuilder<Guid>
  {
    /// <summary>
    /// Return result type
    /// </summary>
    /// <returns></returns>
    public override Type GetResultType()
    {
      return typeof(TestProtocol);
    }

    /// <summary>
    /// To Instance
    /// </summary>
    /// <returns></returns>
    public override IProtocolModel<Guid> ToInstance()
    {
      return ToProtocol();
    }

    /// <summary>
    /// To Protocol
    /// </summary>
    /// <returns></returns>
    public IProtocol<Guid> ToProtocol()
    {
      return new TestProtocol()
      {
        Key = InstanceKey
      };
    }
  }
}