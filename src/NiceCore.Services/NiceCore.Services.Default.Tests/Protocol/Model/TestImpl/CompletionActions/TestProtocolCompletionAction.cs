using System;
using System.Threading.Tasks;
using NiceCore.Services.Protocol.Process.Actions;

namespace NiceCore.Services.Default.Tests.Protocol.Model.TestImpl.CompletionActions
{
  /// <summary>
  /// Action modifying protocol
  /// </summary>
  public class TestProtocolCompletionAction : IProtocolProcessAction
  {
    /// <summary>
    /// Constant the test will check for
    /// </summary>
    public static readonly string VALUE = "VALUE";
    /// <summary>
    /// Type
    /// </summary>
    public Type ProtocolRelatedType { get; } = typeof(TestProtocol);

    /// <summary>
    /// Completed
    /// </summary>
    public ProtocolActionExecutionTimes ExecutionTime { get; } = ProtocolActionExecutionTimes.InstanceCompleted;
    
    /// <summary>
    /// Execute
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <returns></returns>
    public Task Execute(object i_Instance)
    {
      if (i_Instance is TestProtocol casted)
      {
        casted.Value = VALUE;
      }
      return Task.CompletedTask;
    }
  }
}