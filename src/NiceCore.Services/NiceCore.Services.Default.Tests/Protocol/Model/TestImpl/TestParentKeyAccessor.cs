using System;
using NiceCore.Services.Protocol.Abstractions.ParentKeyAccessors;
using NiceCore.Services.Protocol.Model;

namespace NiceCore.Services.Default.Tests.Protocol.Model.TestImpl
{
  /// <summary>
  /// Parent key accessor test impl
  /// </summary>
  public class TestParentKeyAccessor : IParentKeyAccessor<Guid>
  {
    /// <summary>
    /// Get Parent key
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <typeparam name="TProtocolModel"></typeparam>
    /// <returns></returns>
    public Guid GetParentKey<TProtocolModel>(TProtocolModel i_Instance) where TProtocolModel : IProtocolModel<Guid>
    {
      if (i_Instance is TestAction action)
      {
        return action.Protocol.Key;
      }
      if (i_Instance is TestActionEntryContext context)
      {
        return context.Action.Key;
      }
      if (i_Instance is TestActionEntry entry)
      {
        return entry.Context.Key;
      }
      return default;
    }
  }
}