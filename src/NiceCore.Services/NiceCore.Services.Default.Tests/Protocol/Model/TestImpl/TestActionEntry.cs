using System;
using NiceCore.Services.Protocol.Model;

namespace NiceCore.Services.Default.Tests.Protocol.Model.TestImpl
{
  /// <summary>
  /// Test Entry
  /// </summary>
  public class TestActionEntry : BaseTestImpl, IProtocolActionEntry<Guid>
  {
    /// <summary>
    /// Add to child instances
    /// </summary>
    /// <param name="i_Instance"></param>
    public override void AddToChildInstances(IProtocolModel<Guid> i_Instance)
    {
      //Nothing
    }

    /// <summary>
    /// Context instance
    /// </summary>
    public TestActionEntryContext ContextInstance { get; set; }

    /// <summary>
    /// Context
    /// </summary>
    public IProtocolActionEntryContext<Guid> Context => ContextInstance;
  }
}