using System;
using NiceCore.Services.Protocol.Abstractions.TypeProviders;

namespace NiceCore.Services.Default.Tests.Protocol.Model.TestImpl.TypeProviders
{
  /// <summary>
  /// Entry provider
  /// </summary>
  public class TestActionEntryImplementingProvider : IProtocolImplementingTypeProvider
  {
    /// <summary>
    /// Implementing type
    /// </summary>
    /// <returns></returns>
    public Type GetImplementingType()
    {
      return typeof(TestActionEntry);
    }
  }
}