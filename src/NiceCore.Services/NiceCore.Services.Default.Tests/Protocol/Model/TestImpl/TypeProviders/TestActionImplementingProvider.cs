using System;
using NiceCore.Services.Protocol.Abstractions.TypeProviders;

namespace NiceCore.Services.Default.Tests.Protocol.Model.TestImpl.TypeProviders
{
  /// <summary>
  /// Action provider
  /// </summary>
  public class TestActionImplementingProvider : IProtocolImplementingTypeProvider
  {
    /// <summary>
    /// Get implementing type
    /// </summary>
    /// <returns></returns>
    public Type GetImplementingType()
    {
      return typeof(TestAction);
    }
  }
}