using System;
using NiceCore.Services.Protocol.Abstractions.TypeProviders;

namespace NiceCore.Services.Default.Tests.Protocol.Model.TestImpl.TypeProviders
{
  /// <summary>
  /// Protocol provider
  /// </summary>
  public class TestProtocolImplementingProvider : IProtocolImplementingTypeProvider
  {
    /// <summary>
    /// Implementing type
    /// </summary>
    /// <returns></returns>
    public Type GetImplementingType()
    {
      return typeof(TestProtocol);
    }
  }
}