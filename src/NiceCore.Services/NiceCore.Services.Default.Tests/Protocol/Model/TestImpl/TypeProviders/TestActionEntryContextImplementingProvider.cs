using System;
using NiceCore.Services.Protocol.Abstractions.TypeProviders;

namespace NiceCore.Services.Default.Tests.Protocol.Model.TestImpl.TypeProviders
{
  /// <summary>
  /// Context provider
  /// </summary>
  public class TestActionEntryContextImplementingProvider : IProtocolImplementingTypeProvider
  {
    /// <summary>
    /// Get type
    /// </summary>
    /// <returns></returns>
    public Type GetImplementingType()
    {
      return typeof(TestActionEntryContext);
    }
  }
}