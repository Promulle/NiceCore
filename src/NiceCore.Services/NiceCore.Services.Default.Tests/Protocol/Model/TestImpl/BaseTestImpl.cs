using System;
using NiceCore.Services.Protocol.Model;

namespace NiceCore.Services.Default.Tests.Protocol.Model.TestImpl
{
  /// <summary>
  /// base for test impls
  /// </summary>
  public abstract class BaseTestImpl : IProtocolModel<Guid>
  {
    /// <summary>
    /// Key
    /// </summary>
    public Guid Key { get; set; }

    /// <summary>
    /// Add to child instances
    /// </summary>
    /// <param name="i_Instance"></param>
    public abstract void AddToChildInstances(IProtocolModel<Guid> i_Instance);
  }
}