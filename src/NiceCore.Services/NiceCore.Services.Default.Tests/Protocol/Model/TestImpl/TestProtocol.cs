using System;
using System.Collections.Generic;
using NiceCore.Services.Protocol.Model;

namespace NiceCore.Services.Default.Tests.Protocol.Model.TestImpl
{
  /// <summary>
  /// Test Protocl
  /// </summary>
  public class TestProtocol : BaseTestImpl, IProtocol<Guid>
  {
    /// <summary>
    /// Value
    /// </summary>
    public string Value { get; set; }

    /// <summary>
    /// Actions
    /// </summary>
    public List<IProtocolAction<Guid>> Actions { get; } = new();

    /// <summary>
    /// Add to
    /// </summary>
    /// <param name="i_Instance"></param>
    public override void AddToChildInstances(IProtocolModel<Guid> i_Instance)
    {
      if (i_Instance is IProtocolAction<Guid> action)
      {
        AddToActions(action);
      }
    }

    /// <summary>
    /// Add to actions
    /// </summary>
    /// <param name="i_Action"></param>
    public void AddToActions(IProtocolAction<Guid> i_Action)
    {
      if (i_Action is TestAction casted)
      {
        casted.ProtocolInst = this;
      }

      Actions.Add(i_Action);
    }
  }
}