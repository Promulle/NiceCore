using System;
using System.Collections.Generic;
using NiceCore.Services.Protocol.Model;

namespace NiceCore.Services.Default.Tests.Protocol.Model.TestImpl
{
  /// <summary>
  /// Test Action
  /// </summary>
  public class TestAction : BaseTestImpl, IProtocolAction<Guid>
  {
    /// <summary>
    /// Contexts
    /// </summary>
    public List<IProtocolActionEntryContext<Guid>> Contexts { get; } = new();
    
    /// <summary>
    /// Add to child instances
    /// </summary>
    /// <param name="i_Instance"></param>
    public override void AddToChildInstances(IProtocolModel<Guid> i_Instance)
    {
      if (i_Instance is IProtocolActionEntryContext<Guid> context)
      {
        AddToContexts(context);
      }
    }
    
    /// <summary>
    /// The actual protocol instance
    /// </summary>
    public TestProtocol ProtocolInst { get; set; }

    /// <summary>
    /// Protocol
    /// </summary>
    public IProtocol<Guid> Protocol => ProtocolInst;

    /// <summary>
    /// Add to context
    /// </summary>
    /// <param name="i_Context"></param>
    public void AddToContexts(IProtocolActionEntryContext<Guid> i_Context)
    {
      if (i_Context is TestActionEntryContext casted)
      {
        casted.ActionInstance = this;
      }
      Contexts.Add(i_Context);
    }
  }
}