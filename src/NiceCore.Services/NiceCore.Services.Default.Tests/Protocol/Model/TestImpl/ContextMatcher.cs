using System;
using NiceCore.Services.Protocol.Process.Matchers;

namespace NiceCore.Services.Default.Tests.Protocol.Model.TestImpl
{
  /// <summary>
  /// Context Matcher
  /// </summary>
  public class ContextMatcher : BaseProtocolRelatedMatcher<Guid, TestActionEntryContext, TestAction>
  {
    /// <summary>
    /// Can be used for
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public override bool CanBeUsedFor(object i_Value)
    {
      return i_Value is string;
    }

    /// <summary>
    /// Matches
    /// </summary>
    /// <param name="i_Model"></param>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    /// <exception cref="NotImplementedException"></exception>
    protected override bool Matches(TestActionEntryContext i_Model, object i_Value)
    {
      if (i_Value is string strVal)
      {
        return strVal == i_Model.Name;
      }
      return false;
    }
  }
}