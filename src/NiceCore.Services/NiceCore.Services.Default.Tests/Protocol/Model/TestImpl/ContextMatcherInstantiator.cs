using System;
using NiceCore.Services.Protocol.Process.Matchers;

namespace NiceCore.Services.Default.Tests.Protocol.Model.TestImpl
{
  /// <summary>
  /// Context Matcher Instantiator
  /// </summary>
  public class ContextMatcherInstantiator : IProtocolMatchedInstanceInstantiator<TestActionEntryContext>
  {
    /// <summary>
    /// Create for generic
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public TestActionEntryContext CreateForGeneric(object i_Value)
    {
      return new()
      {
        Name = i_Value as string,
        Key = Guid.NewGuid()
      };
    }

    /// <summary>
    /// Applies to
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public bool AppliesTo(object i_Value)
    {
      return i_Value is string;
    }

    /// <summary>
    /// Create for
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public object CreateFor(object i_Value)
    {
      return CreateForGeneric(i_Value);
    }
  }
}