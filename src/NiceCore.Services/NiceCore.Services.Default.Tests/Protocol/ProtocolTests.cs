using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using NiceCore.Services.Default.Tests.Protocol.Model.TestImpl;
using NiceCore.Services.Default.Tests.Protocol.Model.TestImpl.Builders;
using NiceCore.Services.Default.Tests.Protocol.Model.TestImpl.CompletionActions;
using NiceCore.Services.Protocol;
using NiceCore.Services.Protocol.Process.Actions;
using NUnit.Framework;

namespace NiceCore.Services.Default.Tests.Protocol
{
  /// <summary>
  /// Protocol Tests
  /// </summary>
  [TestFixture]
  public class ProtocolTests
  {
    /// <summary>
    /// Test protocol simple
    /// </summary>
    [Test]
    public async Task SimpleProtocolTest()
    {
      var service = new ProtocolService();
      var protocol = service.CreateProtocol(new TestProtocolDefinition()
      {
        Actions = new List<IProtocolProcessAction>()
      }, new TestProtocolBuilder());
      var actionOne = protocol.AddAction(new TestActionBuilder());
      var actionTwo = protocol.AddAction(new TestActionBuilder());
      const string actionOneContextName = "ActionOne_Context";
      const string actionTwoContextName = "ActionTwo_Context";
      var context = actionOne.WithEntryContext(new TestActionEntryContextBuilder().WithName(actionOneContextName));
      context.WithEntry(new TestActionEntryBuilder())
        .WithEntry(new TestActionEntryBuilder());
      actionTwo.WithEntry(actionTwoContextName, new TestActionEntryBuilder())
        .WithEntry(actionTwoContextName, new TestActionEntryBuilder());
      actionOne.Complete();
      var doneProtocol = await protocol.GetCompletedProtocol().ConfigureAwait(false);

      doneProtocol.Should().NotBeNull().And.BeOfType<TestProtocol>();
      var casted = doneProtocol as TestProtocol;
      casted.Actions.Should().NotBeNull().And.HaveCount(2);
      var doneActionOne = casted.Actions[0] as TestAction;
      var doneActionTwo = casted.Actions[1] as TestAction;
      //Test action one and children
      doneActionOne.Contexts.Should().NotBeNull().And.HaveCount(1);
      var doneActionOneContext = doneActionOne.Contexts[0] as TestActionEntryContext;
      doneActionOneContext.Name.Should().Be(actionOneContextName);
      doneActionOneContext.Entries.Should().NotBeNull().And.HaveCount(2);
      
      //Test action two and children
      doneActionTwo.Contexts.Should().NotBeNull().And.HaveCount(1);
      var doneActionTwoContext = doneActionTwo.Contexts[0] as TestActionEntryContext;
      doneActionTwoContext.Name.Should().Be(actionTwoContextName);
      doneActionTwoContext.Entries.Should().NotBeNull().And.HaveCount(2);
    }

    /// <summary>
    /// Tests that completion actions are run for protocols
    /// </summary>
    [Test]
    public async Task TestRunProtocolCompletionActions()
    {
      var service = new ProtocolService();
      var protocol = service.CreateProtocol(new TestProtocolDefinition()
      {
        Actions = new List<IProtocolProcessAction>()
        {
          new TestProtocolCompletionAction()
        }
      }, new TestProtocolBuilder());

      var finishedProtocol = await protocol.GetCompletedProtocol().ConfigureAwait(false) as TestProtocol;
      finishedProtocol.Should().NotBeNull();
      finishedProtocol.Value.Should().Be(TestProtocolCompletionAction.VALUE);
    }
    
    /// <summary>
    /// Test protocol simple
    /// </summary>
    [Test]
    public async Task ProtocolScopedContextTest()
    {
      var service = new ProtocolService();
      var protocol = service.CreateProtocol(new TestProtocolDefinition()
      {
        Actions = new List<IProtocolProcessAction>()
      }, new TestProtocolBuilder());
      var actionOne = protocol.AddAction(new TestActionBuilder());
      var actionTwo = protocol.AddAction(new TestActionBuilder());
      const string contextName = "context";
      var context = actionOne.WithEntryContext(new TestActionEntryContextBuilder().WithName(contextName));
      context.WithEntry(new TestActionEntryBuilder())
        .WithEntry(new TestActionEntryBuilder());
      actionTwo.WithEntry(contextName, new TestActionEntryBuilder())
        .WithEntry(contextName, new TestActionEntryBuilder());
      var doneProtocol = await protocol.GetCompletedProtocol().ConfigureAwait(false);
      
      doneProtocol.Should().NotBeNull().And.BeOfType<TestProtocol>();
      var casted = doneProtocol as TestProtocol;
      casted.Actions.Should().NotBeNull().And.HaveCount(2);
      var doneActionOne = casted.Actions[0] as TestAction;
      var doneActionTwo = casted.Actions[1] as TestAction;
      //Test action one and children
      doneActionOne.Contexts.Should().NotBeNull().And.HaveCount(1);
      var doneActionOneContext = doneActionOne.Contexts[0] as TestActionEntryContext;
      doneActionOneContext.Name.Should().Be(contextName);
      doneActionOneContext.Action.Key.Should().Be(doneActionOne.Key);
      doneActionOneContext.Entries.Should().NotBeNull().And.HaveCount(2);
      
      //Test action two and children
      doneActionTwo.Contexts.Should().NotBeNull().And.HaveCount(1);
      var doneActionTwoContext = doneActionTwo.Contexts[0] as TestActionEntryContext;
      doneActionTwoContext.Name.Should().Be(contextName);
      doneActionTwoContext.Key.Should().NotBe(doneActionOneContext.Key);
      doneActionTwoContext.Action.Key.Should().Be(doneActionTwo.Key);
      doneActionTwoContext.Entries.Should().NotBeNull().And.HaveCount(2);
    }
  }
}
