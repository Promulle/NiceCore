﻿using FluentAssertions;
using NiceCore.Services.Default.Proxies;
using NUnit.Framework;

namespace NiceCore.Services.Default.Tests.Proxies
{
  /// <summary>
  /// Test Fixture
  /// </summary>
  [TestFixture]
  [Parallelizable]
  public class ProxyTests
  {
    /// <summary>
    /// Tests if the Isproxy function works
    /// </summary>
    [Test]
    [Parallelizable]
    public void IsProxy()
    {
      var service = new DefaultProxifyService();
      var inst = new ClassToProxy();

      service.IsProxy(inst).Should().BeFalse();
      var proxiedInstance = service.Proxify(inst);
      service.IsProxy(proxiedInstance).Should().BeTrue();
    }

    /// <summary>
    /// Tests that proxify returns a proxy
    /// </summary>
    [Test]
    [Parallelizable]
    public void Proxify()
    {
      var service = new DefaultProxifyService();
      var inst = new ClassToProxy();

      var proxied = service.Proxify(inst);
      proxied.Should().NotBeNull();
      proxied.GetType().Should().NotBe(inst.GetType());
    }

    /// <summary>
    /// Tests that proxify returns a proxy
    /// </summary>
    [Test]
    [Parallelizable]
    public void UnProxify()
    {
      var service = new DefaultProxifyService();
      var proxied = service.Proxify(new ClassToProxy());

      var inst = service.UnProxify(proxied);
      inst.Should().NotBeNull();
      proxied.GetType().Should().NotBe(inst.GetType());
    }
  }
}
