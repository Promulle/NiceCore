using System;
using System.Threading.Tasks;
using NiceCore.Services.Scheduling;
using NiceCore.Services.Scheduling.Entries;

namespace NiceCore.Services.Default.Tests.Scheduling
{
  public class ExecuteOnScheduledService : BaseScheduledService, IScheduledService
  {
    public bool Flag { get; set; }

    public override ValueTask<bool> ExecuteAsync()
    {
      Flag = true;
      return ValueTask.FromResult(true);
    }

    public override ValueTask<IScheduleEntry> CreateScheduleEntry()
    {
      return ValueTask.FromResult(ScheduleEntryCreator.CreateTimeSpan(this, TimeSpan.FromHours(1), true));
    }
  }
}