﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Abstractions;
using NiceCore.Base.Services;
using NiceCore.Services.Scheduling;
using NiceCore.Services.Scheduling.Entries;

namespace NiceCore.Services.Default.Tests.Scheduling
{
  /// <summary>
  /// Example Cron Scheduled Service
  /// </summary>
  public class ExampleCronScheduledService : BaseService, IScheduledService
  {
    /// <summary>
    /// Flag
    /// </summary>
    public bool Flag { get; private set; } = false;

    /// <summary>
    /// Execute Async
    /// </summary>
    /// <returns></returns>
    public ValueTask<bool> ExecuteAsync()
    {
      Flag = true;
      return ValueTask.FromResult(true);
    }

    /// <summary>
    /// Create Schedule Entry
    /// </summary>
    /// <returns></returns>
    public ValueTask<IScheduleEntry> CreateScheduleEntry()
    {
      const string exp = "*/1 * * * * *";
      return ValueTask.FromResult(ScheduleEntryCreator.CreateCronIncludingSeconds(this, exp, false, NullLogger.Instance));
    }
  }
}