using System.Threading.Tasks;
using NiceCore.Services.Scheduling;

namespace NiceCore.Services.Default.Tests.Scheduling
{
  public class ExampleSchedulable : ISchedulable
  {
    public bool Flag { get; private set; }
    public ValueTask<bool> ExecuteAsync()
    {
      Flag = true;
      return ValueTask.FromResult(true);
    }
  }
}