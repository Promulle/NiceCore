﻿using System;
using System.Threading.Tasks;
using NiceCore.Base.Services;
using NiceCore.Services.Scheduling;
using NiceCore.Services.Scheduling.Entries;

namespace NiceCore.Services.Default.Tests.Scheduling
{
  /// <summary>
  /// Test servie for async execution
  /// </summary>
  public class ExampleAsyncScheduledService : BaseService, IScheduledService
  {
    /// <summary>
    /// Flag
    /// </summary>
    public bool Flag { get; private set; } = false;

    /// <summary>
    /// Create async entry
    /// </summary>
    /// <returns></returns>
    public ValueTask<IScheduleEntry> CreateScheduleEntry()
    {
      return ValueTask.FromResult(ScheduleEntryCreator.CreateTimeSpan(this, TimeSpan.FromMilliseconds(100), false));
    }

    /// <summary>
    /// Execute
    /// </summary>
    /// <returns></returns>
    public bool Execute()
    {
      return true;
    }

    /// <summary>
    /// Execute async
    /// </summary>
    /// <returns></returns>
    public ValueTask<bool> ExecuteAsync()
    {
      Flag = true;
      return ValueTask.FromResult(true);
    }
  }
}
