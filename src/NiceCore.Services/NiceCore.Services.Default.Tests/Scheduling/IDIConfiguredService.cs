namespace NiceCore.Services.Default.Tests.Scheduling
{
  /// <summary>
  /// interface for di test service
  /// </summary>
  public interface IDIConfiguredService
  {
    /// <summary>
    /// Flag
    /// </summary>
    bool Flag { get; set; }
  }
}