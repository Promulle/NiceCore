﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NiceCore.Base.Services;
using NiceCore.Services.Scheduling;
using NiceCore.Services.Scheduling.Entries;

namespace NiceCore.Services.Default.Tests.Scheduling
{
  /// <summary>
  /// Service example
  /// </summary>
  public class ExampleIncrementalScheduledService : BaseService, IScheduledService
  {
    /// <summary>
    /// List for checking if the service has run
    /// </summary>
    public List<string> ListToManipulate { get; set; }

    /// <summary>
    /// Return an incremental entry
    /// </summary>
    /// <returns></returns>
    public ValueTask<IScheduleEntry> CreateScheduleEntry()
    {
      return ValueTask.FromResult(ScheduleEntryCreator.CreateTimeSpan(this, TimeSpan.FromMilliseconds(100), false));
    }

    /// <summary>
    /// Impl
    /// </summary>
    /// <returns></returns>
    public ValueTask<bool> ExecuteAsync()
    {
      if (ListToManipulate == null)
      {
        ListToManipulate = new List<string>();
      }
      ListToManipulate.Add(string.Empty);
      return ValueTask.FromResult(true);
    }
  }
}
