using System.Threading.Tasks;
using NiceCore.Services.Scheduling;

namespace NiceCore.Services.Default.Tests.Scheduling
{
  /// <summary>
  /// Direct Configured Service
  /// </summary>
  public class DirectConfiguredService : ISchedulable
  {
    /// <summary>
    /// Flag
    /// </summary>
    public bool Flag { get; set; }
    
    /// <summary>
    /// Execute
    /// </summary>
    /// <returns></returns>
    public bool Execute()
    {
      Flag = true;
      return true;
    }

    /// <summary>
    /// Exeute
    /// </summary>
    /// <returns></returns>
    public ValueTask<bool> ExecuteAsync()
    {
      return ValueTask.FromResult(true);
    }
  }
}