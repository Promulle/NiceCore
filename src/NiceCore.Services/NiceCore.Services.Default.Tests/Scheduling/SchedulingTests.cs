﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.Logging.Abstractions;
using NiceCore.ServiceLocation;
using NiceCore.ServiceLocation.CastleWindsor;
using NiceCore.Services.Config;
using NiceCore.Services.Default.Config;
using NiceCore.Services.Default.Scheduling;
using NiceCore.Services.Default.Tests.Configuration.Impl;
using NiceCore.Services.Scheduling;
using NiceCore.Services.Scheduling.Configuration;
using NiceCore.Services.Scheduling.Entries;
using NiceCore.Services.Scheduling.Entries.EntryTimeModels.Impls;
using NUnit.Framework;

namespace NiceCore.Services.Default.Tests.Scheduling
{
  /// <summary>
  /// Tests for scheduling
  /// </summary>
  [TestFixture]
  [Parallelizable]
  public class SchedulingTests
  {
    private ExampleCronScheduledService m_CronService;
    private ExampleExactScheduledService m_ExactService;
    private ExampleIncrementalScheduledService m_IncService;
    private ExampleAsyncScheduledService m_AsyncService;
    private DefaultScheduleServiceHost Host;
    private ExecuteOnScheduledService m_ExecuteOnScheduledService;
    
    /// <summary>
    /// Setup
    /// </summary>
    [OneTimeSetUp]
    public async Task Setup()
    {
      m_ExactService = new ExampleExactScheduledService();
      m_IncService = new ExampleIncrementalScheduledService();
      m_AsyncService = new ExampleAsyncScheduledService();
      m_CronService = new ExampleCronScheduledService();
      Host = new DefaultScheduleServiceHost()
      {
        Logger = NullLogger<DefaultScheduleServiceHost>.Instance
      };
      m_ExecuteOnScheduledService = new ExecuteOnScheduledService();
      await Host.Initialize().ConfigureAwait(false);
      Host.GetAllSchedules().ForEach(r => r.SetLogger(NullLogger<DefaultScheduleServiceHost>.Instance));
    }

    /// <summary>
    /// Clear container registrations
    /// </summary>
    [SetUp]
    public void EveryTestSetup()
    {
      Host.AllowExecuteOnScheduled = true;
      Host.StopScheduling();
      Host.RemoveService(m_IncService);
      Host.RemoveService(m_ExactService);
      Host.RemoveService(m_AsyncService);
      Host.RemoveService(m_CronService);
      Host.RemoveService(m_ExecuteOnScheduledService);
      m_ExecuteOnScheduledService.Flag = false;
    }

    [Test]
    public async Task ChangeTimerIntervalOnTimeModelChange()
    {
      var service = new ExampleSchedulable();
      var entry = ScheduleEntryCreator.CreateTimeSpan(service, TimeSpan.FromDays(1), false);
      await Host.AddScheduleEntry(entry).ConfigureAwait(false);
      await Host.StartScheduling().ConfigureAwait(false);
      await Task.Delay(10).ConfigureAwait(false);
      service.Flag.Should().BeFalse();
      await entry.UpdateEntry(new DefaultTimeSpanScheduleEntryTimeModel()
      {
        SpanBetweenExecutions = TimeSpan.FromMilliseconds(10),
        IsTimeModelEnabled = true
      }, false, null);
      await Task.Delay(100).ConfigureAwait(false);
      service.Flag.Should().BeTrue();
    }

    /// <summary>
    /// Configured Services
    /// </summary>
    [Test]
    public async Task ConfiguredScheduling()
    {
      var conf = new DefaultNcConfiguration()
      {
        ConfigurationSources = new List<INcConfigurationSource>{ new TestAssemblyJSONConfigurationSource() }
      };
      conf.Initialize();
      var container = new CastleWindsorServiceContainer();
      container.Register<IDIConfiguredService, DIConfiguredService>(LifeStyles.Singleton, null);
      var host = new DefaultScheduleServiceHost()
      {
        Logger = NullLogger<DefaultScheduleServiceHost>.Instance,
        ScheduleServiceHostConfigurator = new ScheduleServiceHostConfigurator()
        {
          Logger = NullLogger<ScheduleServiceHostConfigurator>.Instance,
          Configuration = conf,
          ServiceContainer = container
        }
      };
      await host.Initialize().ConfigureAwait(false);
      host.RemoveService(m_IncService);
      host.RemoveService(m_ExactService);
      host.RemoveService(m_AsyncService);
      host.RemoveService(m_CronService);
      host.RemoveService(m_ExecuteOnScheduledService);

      var entries = host.GetAllSchedules().FirstOrDefault().GetAllScheduledEntries();
      entries.Count.Should().Be(2);
      entries.Any(r => r.Schedulable.GetType().Name == nameof(DIConfiguredService)).Should().BeTrue();
      entries.Any(r => r.Schedulable.GetType().Name == nameof(DirectConfiguredService)).Should().BeTrue();
    }

    /// <summary>
    /// Configured Services
    /// </summary>
    [Test]
    public async Task ConfiguredServices()
    {
      var conf = new DefaultNcConfiguration()
      {
        ConfigurationSources = new List<INcConfigurationSource>() { new TestAssemblyJSONConfigurationSource() }
      };
      conf.Initialize();
      var container = new CastleWindsorServiceContainer();
      container.RegisterInstance<INcConfiguration>(conf, LifeStyles.Singleton);
      container.Register<IScheduledService, ConfigurableService>(LifeStyles.Singleton, null);
      var service = container.Resolve<IScheduledService>() as ConfigurableService;
      var host = new DefaultScheduleServiceHost()
      {
        Logger = NullLogger<DefaultScheduleServiceHost>.Instance,
        Container = container
      };
      await host.Initialize().ConfigureAwait(false);
      await host.StartScheduling().ConfigureAwait(false);
      Thread.Sleep(200);
      service!.Flag.Should().BeTrue();
    }

    /// <summary>
    /// Tests incrementally executed service
    /// </summary>
    [Test]
    public async Task IncrementalService()
    {
      await Host.AddService(m_IncService).ConfigureAwait(false);
      await Host.StartScheduling().ConfigureAwait(false);
      Thread.Sleep(200);
      m_IncService.ListToManipulate.Should().NotBeNull()
        .And.NotBeEmpty();
    }

    /// <summary>
    /// Tests incrementally executed service
    /// </summary>
    [Test]
    public async Task AsyncService()
    {
      await Host.AddService(m_AsyncService).ConfigureAwait(false);
      await Host.StartScheduling().ConfigureAwait(false);
      await Task.Delay(500).ConfigureAwait(false);
      m_AsyncService.Flag.Should().BeTrue();
    }

    /// <summary>
    /// Cron Service test
    /// </summary>
    [Test]
    public async Task CronService()
    {
      await Host.AddService(m_CronService).ConfigureAwait(false);
      await Host.StartScheduling().ConfigureAwait(false);
      Thread.Sleep(2000);
      m_CronService.Flag.Should().BeTrue();
    }

    /// <summary>
    /// Tests incrementally executed service
    /// </summary>
    [Test]
    public async Task FailOnNoServicesAdded()
    {
      await Host.StartScheduling().ConfigureAwait(false);
      Thread.Sleep(200);
    }

    /// <summary>
    /// tests exact service execution
    /// </summary>
    [Test]
    public async Task ExactService()
    {
      m_ExactService.SetExecuteTime();
      await Host.AddService(m_ExactService).ConfigureAwait(false);
      await Host.StartScheduling().ConfigureAwait(false);
      Thread.Sleep(200);
      m_ExactService.ListToManipulate.Should().NotBeNull();
    }

    [Test]
    public void TestExecuteOnScheduled()
    {
      Host.AddService(m_ExecuteOnScheduledService);
      Thread.Sleep(10);
      m_ExecuteOnScheduledService.Flag.Should().BeTrue();
    }
    
    [Test]
    public void TestDisallowExecuteOnScheduled()
    {
      Host.AllowExecuteOnScheduled = false;
      Host.AddService(m_ExecuteOnScheduledService);
      Thread.Sleep(10);
      m_ExecuteOnScheduledService.Flag.Should().BeFalse();
    }
  }
}
