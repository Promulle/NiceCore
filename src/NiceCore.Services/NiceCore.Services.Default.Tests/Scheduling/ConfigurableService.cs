using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using NiceCore.ServiceLocation;
using NiceCore.Services.Scheduling;

namespace NiceCore.Services.Default.Tests.Scheduling
{
  /// <summary>
  /// Configurable 
  /// </summary>
  [Register(InterfaceType = typeof(IScheduledService))]
  public class ConfigurableService : BaseConfigurableScheduledService
  {
    /// <summary>
    /// Flag
    /// </summary>
    public bool Flag { get; set; }
    
    public override ILogger Logger { get; } = NullLogger.Instance;

    /// <summary>
    /// Execute async
    /// </summary>
    /// <returns></returns>
    public override ValueTask<bool> ExecuteAsync()
    {
      Flag = true;
      return ValueTask.FromResult(true);
    }
  }
}