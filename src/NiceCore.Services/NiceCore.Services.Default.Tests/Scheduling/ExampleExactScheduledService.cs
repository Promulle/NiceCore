﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NiceCore.Base.Services;
using NiceCore.Services.Scheduling;
using NiceCore.Services.Scheduling.Entries;

namespace NiceCore.Services.Default.Tests.Scheduling
{
  /// <summary>
  /// Service example
  /// </summary>
  public class ExampleExactScheduledService : BaseService, IScheduledService
  {
    /// <summary>
    /// List for checking if the service has run
    /// </summary>
    public List<string> ListToManipulate { get; set; }
    
    /// <summary>
    /// Time of execute
    /// </summary>
    public TimeOnly ExecuteTime { get; set; }
    
    /// <summary>
    /// Return an incremental entry
    /// </summary>
    /// <returns></returns>
    public ValueTask<IScheduleEntry> CreateScheduleEntry()
    {
      return ValueTask.FromResult(ScheduleEntryCreator.CreateExact(this, ExecuteTime, false));
    }

    /// <summary>
    /// Execute async
    /// </summary>
    /// <returns></returns>
    public ValueTask<bool> ExecuteAsync()
    {
      if (ListToManipulate == null)
      {
        ListToManipulate = new List<string>();
      }
      ListToManipulate.Add(string.Empty);
      return ValueTask.FromResult(true);
    }

    /// <summary>
    /// Sets time of execution
    /// </summary>
    public void SetExecuteTime()
    {
      ExecuteTime = GetIn100Miliseconds();
    }
    
    private TimeOnly GetIn100Miliseconds()
    {
      return TimeOnly.FromDateTime(DateTime.Now.AddMilliseconds(100));
    }
  }
}
