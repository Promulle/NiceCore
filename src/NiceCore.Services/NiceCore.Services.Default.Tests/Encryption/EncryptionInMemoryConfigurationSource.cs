using System.Collections.Generic;
using NiceCore.Services.Config.Impl;

namespace NiceCore.Services.Default.Tests.Encryption
{
  public class EncryptionInMemoryConfigurationSource : BaseInMemoryNcConfigurationSource
  {
    private readonly string m_CertPath;
    public override int Priority { get; } = 0;

    public EncryptionInMemoryConfigurationSource(string i_CertPath)
    {
      m_CertPath = i_CertPath;
      Values = GetDict();
    }

    public override IDictionary<string, string> Values { get; init; }

    private IDictionary<string, string> GetDict()
    {
      return new Dictionary<string, string>()
      {
        {"NiceCore:Encryption:Providers:NcCertProvider:CertificateFilePath", m_CertPath}
      };
    }
  }
}