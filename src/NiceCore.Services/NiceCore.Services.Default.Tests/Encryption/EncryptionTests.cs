using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.Logging.Abstractions;
using NiceCore.ServiceLocation.CastleWindsor;
using NiceCore.Services.Config;
using NiceCore.Services.Default.Config;
using NiceCore.Services.Encryption;
using NiceCore.Services.Encryption.Providers;
using NiceCore.Services.Encryption.Providers.Certs;
using NUnit.Framework;

namespace NiceCore.Services.Default.Tests.Encryption
{
  [TestFixture]
  public class EncryptionTests
  {
    public static readonly string CERT_PATH = Path.Join(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "test-cert");

    [Test]
    public async Task TestEncryptAndDecrypt()
    {
      var encryptionService = new NcEncryptionService()
      {
        Logger = NullLogger<NcEncryptionService>.Instance,
        AvailableProviders = new List<INcEncryptionProvider>()
        {
          CreateCertProvider()
        }
      };
      encryptionService.Initialize();
      const string dataText = "This is some test Text that should be encrypted.";
      var textAsBytes = Encoding.UTF8.GetBytes(dataText);
      var encrypted = await encryptionService.EncryptWith(textAsBytes, NcEncryptionProviders.CERT_PROVIDER).ConfigureAwait(false);
      encrypted.Should().NotBeEmpty().And.NotBeEquivalentTo(textAsBytes);
      var decrypted = await encryptionService.DecryptWith(encrypted, NcEncryptionProviders.CERT_PROVIDER).ConfigureAwait(false);
      decrypted.Should().NotBeEmpty().And.NotBeEquivalentTo(encrypted);
      var textFromDecrypted = Encoding.UTF8.GetString(decrypted);
      textFromDecrypted.Should().Be(dataText);
    }

    private static INcEncryptionProvider CreateCertProvider()
    {
      var cfg = new DefaultNcConfiguration()
      {
        Logger = NullLogger.Instance,
        Container = new CastleWindsorServiceContainer(),
        DisableReloadOnChange = true,
        ConfigurationSources = new List<INcConfigurationSource>()
        {
          new EncryptionInMemoryConfigurationSource(CERT_PATH)
        }
      };
      cfg.Initialize();
      var provider = new NcCertEncryptionProvider()
      {
        Configuration = cfg,
        ActualLogger = NullLogger<NcCertEncryptionProvider>.Instance
      };
      return provider;
    }

    [OneTimeSetUp]
    public void Setup()
    {
      var rsaKey = RSA.Create();
      var certRequest = new CertificateRequest("CN=Self-Signed-Cert-Test", rsaKey, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
      var cert = certRequest.CreateSelfSigned(DateTimeOffset.Now.AddDays(-1), DateTimeOffset.Now.AddYears(10));
      var certData = cert.Export(X509ContentType.Pfx);
      if (File.Exists(CERT_PATH))
      {
        File.Delete(CERT_PATH);
      }
      File.WriteAllBytes(CERT_PATH, certData);
    }

    [OneTimeTearDown]
    public void Teardown()
    {
      try
      {
        File.Delete(CERT_PATH);
      }
      catch
      {
      }
    }
  }
}