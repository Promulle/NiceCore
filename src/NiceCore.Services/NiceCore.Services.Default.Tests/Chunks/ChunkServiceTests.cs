using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NiceCore.Services.Chunks;
using NiceCore.Services.Config;
using NiceCore.Services.Config.Impl;
using NiceCore.Services.Default.Config;
using NUnit.Framework;

namespace NiceCore.Services.Default.Tests.Chunks
{
  [TestFixture]
  public class ChunkServiceTests
  {
    private static INcConfiguration GetMockConfiguration(int i_ChunkSize)
    {
      var dict = new Dictionary<string, string>()
      {
        {"NiceCore:Services:ChunkService:ChunkSize", i_ChunkSize.ToString()}
      };
      var inMemorySource = new DefaultInMemoryNcConfigurationSource(dict);
      var config = new DefaultNcConfiguration();
      config.ConfigurationSources = new List<INcConfigurationSource>()
      {
        inMemorySource
      };
      config.Initialize();
      return config;
    }

    [Test]
    public void TestOneChunkWhenChunkSizeLowerThanOne()
    {
      var config = GetMockConfiguration(-1);
      var service = new ChunkService()
      {
        Configuration = config
      };
      service.Initialize();
      
      var source = Enumerable.Range(0, 1000);
      var chunks = service.GetChunksOf(source);
      var chunksAsList = chunks.ToList();
      chunksAsList.Should().HaveCount(1);
      var onlyChunk = chunksAsList[0].ToList();
      onlyChunk.Should().HaveCount(1000);
    }
    
    [Test]
    public void TestOneChunkWhenChunkSizeIsDefault()
    {
      var config = GetMockConfiguration(2000);
      var service = new ChunkService()
      {
        Configuration = config
      };
      service.Initialize();
      
      var source = Enumerable.Range(0, 1000);
      var chunks = service.GetChunksOf(source);
      var chunksAsList = chunks.ToList();
      chunksAsList.Should().HaveCount(1);
      var onlyChunk = chunksAsList[0].ToList();
      onlyChunk.Should().HaveCount(1000);
    }
    
    [Test]
    public void TestOneChunkWhenChunkSizeIsOverriden()
    {
      var config = GetMockConfiguration(2000);
      var service = new ChunkService()
      {
        Configuration = config
      };
      service.Initialize();
      
      var source = Enumerable.Range(0, 1000);
      var chunks = service.GetChunksOf(source, -1);
      var chunksAsList = chunks.ToList();
      chunksAsList.Should().HaveCount(1);
      var onlyChunk = chunksAsList[0].ToList();
      onlyChunk.Should().HaveCount(1000);
    }
    
    [Test]
    public void TestFourChunkWhenChunkSizeIsDefault()
    {
      var config = GetMockConfiguration(250);
      var service = new ChunkService()
      {
        Configuration = config
      };
      service.Initialize();
      
      var source = Enumerable.Range(0, 1000);
      var chunks = service.GetChunksOf(source);
      var chunksAsList = chunks.ToList();
      chunksAsList.Should().HaveCount(4);
      foreach (var chunk in chunksAsList)
      {
        chunk.Should().HaveCount(250);
      }
    }
    
    [Test]
    public void TestTwoChunkWhenChunkSizeIsOverriden()
    {
      var config = GetMockConfiguration(250);
      var service = new ChunkService()
      {
        Configuration = config
      };
      service.Initialize();
      
      var source = Enumerable.Range(0, 1000);
      var chunks = service.GetChunksOf(source, 500);
      var chunksAsList = chunks.ToList();
      chunksAsList.Should().HaveCount(2);
      foreach (var chunk in chunksAsList)
      {
        chunk.Should().HaveCount(500);
      }
      
    }
  }
}