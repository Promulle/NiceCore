﻿/*using NiceCore.Base.Constants;
using NiceCore.Services.Http.Channels.Endpoints;
using NiceCore.Test.Support.Hosting.TestServices;
using System;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace NiceCore.Services.Default.Tests.HttpChannels
{
  /// <summary>
  /// test impl
  /// </summary>
  public class TestPostEndpoint : BaseEndpointBodyRequestInformation<TestInstance>
  {
    /// <summary>
    /// Content Type
    /// </summary>
    public override string ContentType { get; } = ContentTypes.APP_JSON;

    /// <summary>
    /// Paylaod
    /// </summary>
    public override TestInstance Payload { get; init; }

    /// <summary>
    /// Endpoint
    /// </summary>
    public override string Endpoint { get; } = RESTTestConstants.POST_ENDPOINT;

    /// <summary>
    /// Header Func
    /// </summary>
    public override Func<HttpRequestHeaders, Task> HeaderFunc { get; }

    /// <summary>
    /// Auth func
    /// </summary>
    public override Func<Task<AuthenticationHeaderValue>> AuthenticationHeaderFunc { get; }

    /// <summary>
    /// Create
    /// </summary>
    /// <param name="i_Payload"></param>
    /// <returns></returns>
    public static TestPostEndpoint Create(TestInstance i_Payload)
    {
      return new TestPostEndpoint()
      {
        Payload = i_Payload
      };
    }
  }
}
*/