﻿/*using NiceCore.Services.Http.Channels.Endpoints;
using NiceCore.Test.Support.Hosting.TestServices;
using System;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace NiceCore.Services.Default.Tests.HttpChannels
{
  /// <summary>
  /// test impl
  /// </summary>
  public class TestGetSecureEndpoint : BaseEndpointReturningRequestInformation<TestInstance>
  {
    private readonly string m_Token;

    /// <summary>
    /// Endpoint
    /// </summary>
    public override string Endpoint { get; } = RESTTestConstants.GET_SECURE_ENDPOINT;
    /// <summary>
    /// HEader func
    /// </summary>
    public override Func<HttpRequestHeaders, Task> HeaderFunc { get; }

    /// <summary>
    /// Auth func
    /// </summary>
    public override Func<Task<AuthenticationHeaderValue>> AuthenticationHeaderFunc
    {
      get { return () => Task.FromResult(new AuthenticationHeaderValue("Bearer", m_Token)); }
    }

    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_Token"></param>
    public TestGetSecureEndpoint(string i_Token)
    {
      m_Token = i_Token;
    }
  }
}
*/