﻿/*using FluentAssertions;
using NiceCore.Base.Constants;
using NiceCore.Hosting.Utils;
using NiceCore.ServiceLocation.Default.Container;
using NiceCore.Services.Default.Http;
using NiceCore.Services.Default.Http.Converters;
using NiceCore.Services.Default.Tests.HttpChannels.Impls;
using NiceCore.Services.Http.Channels.Endpoints.UrlExtenders;
using NiceCore.Services.Http.Channels.Extensions;
using NiceCore.Services.Http.Channels.Impls;
using NiceCore.Services.Http.Converters;
using NiceCore.Test.Support.Hosting;
using NiceCore.Test.Support.Hosting.TestServices;
using NiceCore.Test.Support.Hosting.TestServices.Impls;
using NiceUnitTesting;
using System.Threading.Tasks;

namespace NiceCore.Services.Default.Tests.HttpChannels
{
  /// <summary>
  /// Class for tests regarding http channels
  /// </summary>
  //[TestClass]
  public class HttpChannelTests
  {
    TestServer m_Server;

    /// <summary>
    /// setup
    /// </summary>
    [OneTimeSetup]
    public void Setup()
    {
      m_Server = new TestServer();
      m_Server.StartServer(() => new TestHostService());
    }

    /// <summary>
    /// Tests get
    /// </summary>
    /// <returns></returns>
    [Test]
    public async Task Get()
    {
      using (var channel = CreateChannel())
      {
        var res = await channel.Get<TestInstance>(RESTTestConstants.GET_ENDPOINT).ConfigureAwait(false);
        res.Should().NotBeNull();
        TestInstanceProvider.Service.GetCalled.Should().BeTrue();
      }
    }

    /// <summary>
    /// Tests get
    /// </summary>
    /// <returns></returns>
    [Test]
    public async Task GetWithEndpoint()
    {
      using (var channel = CreateChannel())
      {
        var res = await channel.Get(new TestServiceGetEndpoint()).ConfigureAwait(false);
        res.Should().NotBeNull();
        TestInstanceProvider.Service.GetCalled.Should().BeTrue();
      }
    }

    /// <summary>
    /// Tests get
    /// </summary>
    /// <returns></returns>
    [Test]
    public async Task GetByIdWithEndpointAndExtender()
    {
      using (var channel = CreateChannel())
      {
        var res = await channel.Get(new TestServiceGetEndpoint(), UrlExtensions.Simple("1")).ConfigureAwait(false);
        res.Should().NotBeNull();
        TestInstanceProvider.Service.GetID.Should().Be(1);
      }
    }

    /// <summary>
    /// Tests get
    /// </summary>
    /// <returns></returns>
    [Test]
    public async Task GetByIdWithEndpointAndQueryExtender()
    {
      using (var channel = CreateChannel())
      {
        await channel.Get(new TestServiceGetQueryEndpoint(), UrlExtensions.QueryFor(("value", "MyValue"))).ConfigureAwait(false);
        TestInstanceProvider.Service.QueryValue.Should().Be("MyValue");
      }
    }

    /// <summary>
    /// Tests get
    /// </summary>
    /// <returns></returns>
    [Test]
    public async Task GetSecureWithEndpoint()
    {
      using (var channel = CreateChannel())
      {
        await channel.Get(new TestGetSecureEndpoint(NcJWTSupport.GenerateJWTToken(TestPermission.USER_ID))).ConfigureAwait(false);
        TestInstanceProvider.Service.GetSecureCalled.Should().BeTrue();
      }
    }

    /// <summary>
    /// Tests post
    /// </summary>
    /// <returns></returns>
    [Test]
    public async Task Post()
    {
      using (var channel = CreateChannel())
      {
        await channel.Post(RESTTestConstants.POST_ENDPOINT, ContentTypes.APP_JSON, TestInstanceProvider.GetNormal()).ConfigureAwait(false);
        TestInstanceProvider.Service.PostCalled.Should().BeTrue();
      }
    }

    /// <summary>
    /// Tests post
    /// </summary>
    /// <returns></returns>
    [Test]
    public async Task PostWithEndpoint()
    {
      using (var channel = CreateChannel())
      {
        await channel.Post(TestPostEndpoint.Create(TestInstanceProvider.GetNormal())).ConfigureAwait(false);
        TestInstanceProvider.Service.PostCalled.Should().BeTrue();
      }
    }

    /// <summary>
    /// Tests get
    /// </summary>
    /// <returns></returns>
    [Test]
    public async Task Delete()
    {
      using (var channel = CreateChannel())
      {
        await channel.Delete(RESTTestConstants.DELETE_ENDPOINT + "/4").ConfigureAwait(false);
        TestInstanceProvider.Service.DeleteCalled.Should().BeTrue();
      }
    }

    /// <summary>
    /// Tests get
    /// </summary>
    /// <returns></returns>
    [Test]
    public async Task Put()
    {
      using (var channel = CreateChannel())
      {
        await channel.Put(RESTTestConstants.PUT_ENDPOINT, ContentTypes.APP_JSON, TestInstanceProvider.GetNormal()).ConfigureAwait(false);
        TestInstanceProvider.Service.PutCalled.Should().BeTrue();
      }
    }

    /// <summary>
    /// Teardown
    /// </summary>
    [OneTimeTearDown]
    public void Teardown()
    {
      m_Server.KillServer();
    }

    private static TestHttpChannel CreateChannel()
    {
      var container = new DefaultServiceContainer();
      container.Register<IPayloadConverter, JSONPayloadConverter>();
      container.Register<IResponseConverter, JSONResponseConverter>();
      var converterService = new DefaultConverterService()
      {
        Container = container
      };
      converterService.Initialize();
      return new TestHttpChannel()
      {
        ConverterService = converterService,
        HttpClientHandlerService = new DefaultHttpClientHandlerService()
      };
    }
  }
}
*/