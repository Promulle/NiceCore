﻿using NiceCore.Services.Http.Channels;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace NiceCore.Services.Default.Tests.HttpChannels.Impls
{
  /// <summary>
  /// Test impl
  /// </summary>
  public class TestHttpChannel : BaseNcHttpChannel
  {
    /// <summary>
    /// Configure
    /// </summary>
    /// <param name="t_Client"></param>
    protected override Task ConfigureClient(HttpClient t_Client)
    {
      t_Client.BaseAddress = new Uri("http://127.0.0.1:8080/");
      return Task.CompletedTask;
    }
  }
}
