﻿using Microsoft.Extensions.Logging;
using NiceCore.ServiceLocation;

namespace NiceCore.Logging.Telemetry
{
  /// <summary>
  /// Logger Provider for Application Insights
  /// </summary>
  [Register(InterfaceType = typeof(INcLoggingProvider))]
  public class ApplicationInsightsNcLoggerProvider : INcLoggingProvider
  {
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; } = "Nc-ApplicationInsights";
    
    /// <summary>
    /// Apply TO login builder
    /// </summary>
    /// <param name="t_Builder"></param>
    public void ApplyToLoggingBuilder(ILoggingBuilder t_Builder)
    {
      t_Builder.AddApplicationInsights();
    }
  }
}