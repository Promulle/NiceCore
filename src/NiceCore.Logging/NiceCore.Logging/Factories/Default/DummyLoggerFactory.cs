﻿using System;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;

namespace NiceCore.Logging.Factories.Default
{
  /// <summary>
  /// Dummy impl for ILoggerFactory. This should prohibit any null pointers if no logger is registered
  /// </summary>
  public sealed class DummyLoggerFactory : ILoggerFactory
  {
    /// <summary>
    /// Dispose
    /// </summary>
    public void Dispose()
    {
      
    }

    /// <summary>
    /// Create Logger
    /// </summary>
    /// <param name="categoryName"></param>
    /// <returns></returns>
    public ILogger CreateLogger(string categoryName)
    {
      return NullLogger.Instance;
    }

    /// <summary>
    /// Add Provider
    /// </summary>
    /// <param name="provider"></param>
    public void AddProvider(ILoggerProvider provider)
    {
      throw new NotSupportedException($"Dummy Factory does not support {nameof(AddProvider)}");
    }
  }
}