﻿using Microsoft.Extensions.Logging;

namespace NiceCore.Logging
{
  /// <summary>
  /// Interface for instance used to create the logger factory
  /// </summary>
  public interface ILoggerFactorySetup
  {
    /// <summary>
    /// Setup a new instance of a logger factory
    /// </summary>
    /// <returns></returns>
    ILoggerFactory SetupLoggerFactory();
  }
}