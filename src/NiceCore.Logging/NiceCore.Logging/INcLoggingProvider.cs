﻿using Microsoft.Extensions.Logging;

namespace NiceCore.Logging
{
  /// <summary>
  /// Interface for logging providers that are used to configure logging
  /// </summary>
  public interface INcLoggingProvider
  {
    /// <summary>
    /// Name of the logging provider
    /// </summary>
    string Name { get; }
    
    /// <summary>
    /// Apply To Logging Builder
    /// </summary>
    /// <param name="t_Builder"></param>
    void ApplyToLoggingBuilder(ILoggingBuilder t_Builder);
  }
}