﻿using System.Runtime.CompilerServices;
using Microsoft.Extensions.Logging;

namespace NiceCore.Logging.StringHandlers
{
  /// <summary>
  /// Interpolation Handler for logging
  /// </summary>
  [InterpolatedStringHandler]
  public ref struct InformationLoggingInterpolatedStringHandler
  {
    private DefaultInterpolatedStringHandler m_Handler;

    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_LiteralLength"></param>
    /// <param name="i_FormattedCount"></param>
    /// <param name="i_Logger"></param>
    /// <param name="i_ShouldAppend"></param>
    public InformationLoggingInterpolatedStringHandler(int i_LiteralLength,
      int i_FormattedCount,
      ILogger i_Logger,
      out bool i_ShouldAppend)
    {
      var enabled = i_Logger.IsEnabled(LogLevel.Information);
      if (!enabled)
      {
        m_Handler = default;
        i_ShouldAppend = false;
        return;
      }
      m_Handler = new DefaultInterpolatedStringHandler(i_LiteralLength, i_FormattedCount);
      i_ShouldAppend = true;
    }

    /// <summary>
    /// Append Literal
    /// </summary>
    /// <param name="i_Value"></param>
    public void AppendLiteral(string i_Value)
    {
      m_Handler.AppendLiteral(i_Value);
    }

    /// <summary>
    /// Append Formatted
    /// </summary>
    /// <param name="i_Value"></param>
    /// <typeparam name="T"></typeparam>
    public void AppendFormatted<T>(T i_Value)
    {
      m_Handler.AppendFormatted(i_Value);
    }

    /// <summary>
    /// To String
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
      return m_Handler.ToString();
    }

    /// <summary>
    /// To String And Clear
    /// </summary>
    /// <returns></returns>
    public string ToStringAndClear()
    {
      return m_Handler.ToStringAndClear();
    }
  }
}