﻿using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace NiceCore.Logging
{
  /// <summary>
  /// Direct Logger Factory Setup impl
  /// </summary>
  public sealed class DirectLoggerFactorySetup : ILoggerFactorySetup
  {
    private readonly IEnumerable<INcLoggingProvider> m_Providers;
    private readonly LogLevel m_Level;
    
    /// <summary>
    /// Direct Logger Factory Setup that uses i_Providers for the creation of the factory
    /// </summary>
    /// <param name="i_Providers"></param>
    /// <param name="i_Level"></param>
    public DirectLoggerFactorySetup(IEnumerable<INcLoggingProvider> i_Providers, LogLevel i_Level)
    {
      m_Providers = i_Providers;
      m_Level = i_Level;
    }
    
    /// <summary>
    /// Creates a logger factory from the providers that were given to the constructor
    /// </summary>
    /// <returns></returns>
    public ILoggerFactory SetupLoggerFactory()
    {
      return SetupLoggerFactoryFor(m_Providers, m_Level);
    }

    /// <summary>
    /// Setup Logger Factory for
    /// </summary>
    /// <param name="i_Providers"></param>
    /// <param name="i_LogLevel"></param>
    /// <returns></returns>
    public static ILoggerFactory SetupLoggerFactoryFor(IEnumerable<INcLoggingProvider> i_Providers, LogLevel i_LogLevel)
    {
      return LoggerFactory.Create(builder =>
      {
        builder.SetMinimumLevel(i_LogLevel);
        foreach (var loggerProvider in i_Providers)
        {
          loggerProvider.ApplyToLoggingBuilder(builder);
        }
      });
    }
  }
}