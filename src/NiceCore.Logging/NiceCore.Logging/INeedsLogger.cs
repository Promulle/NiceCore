// Copyright 2021 Sycorax Systemhaus GmbH

using Microsoft.Extensions.Logging;

namespace NiceCore.Logging
{
  /// <summary>
  /// Interface for classes that need a later logger
  /// </summary>
  public interface INeedsLogger
  {
    /// <summary>
    /// Returns the category the logger should have
    /// </summary>
    /// <returns></returns>
    string GetLoggerCategory();
    
    /// <summary>
    /// Sets logger
    /// </summary>
    /// <param name="i_Logger"></param>
    void SetLogger(ILogger i_Logger);
  }
}
