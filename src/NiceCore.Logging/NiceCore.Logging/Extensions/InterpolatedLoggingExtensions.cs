﻿using System;
using System.Runtime.CompilerServices;
using Microsoft.Extensions.Logging;
using NiceCore.Logging.StringHandlers;

namespace NiceCore.Logging.Extensions
{
  /// <summary>
  /// Interpolated Logging Extensions
  /// </summary>
  public static class InterpolatedLoggingExtensions
  {
    /// <summary>
    /// Log Debug Message
    /// </summary>
    /// <param name="i_Logger"></param>
    /// <param name="i_Handler"></param>
    public static void Debug(this ILogger i_Logger,
      [InterpolatedStringHandlerArgument(nameof(i_Logger))]
      ref DebugLoggingInterpolatedStringHandler i_Handler)
    {
      i_Logger.LogDebug(i_Handler.ToStringAndClear());
    }

    /// <summary>
    /// Log Debug Message
    /// </summary>
    /// <param name="i_Logger"></param>
    /// <param name="i_Message"></param>
    public static void Debug(this ILogger i_Logger,
      string i_Message)
    {
      i_Logger.LogDebug(i_Message);
    }

    /// <summary>
    /// Log Debug Message
    /// </summary>
    /// <param name="i_Logger"></param>
    /// <param name="i_Exception"></param>
    /// <param name="i_Handler"></param>
    public static void Debug(this ILogger i_Logger,
      Exception i_Exception,
      [InterpolatedStringHandlerArgument(nameof(i_Logger))]
      ref DebugLoggingInterpolatedStringHandler i_Handler)
    {
      i_Logger.LogDebug(i_Exception, i_Handler.ToStringAndClear());
    }

    /// <summary>
    /// Log Debug Message
    /// </summary>
    /// <param name="i_Logger"></param>
    /// <param name="i_Exception"></param>
    /// <param name="i_Message"></param>
    public static void Debug(this ILogger i_Logger,
      Exception i_Exception,
      string i_Message)
    {
      i_Logger.LogDebug(i_Exception, i_Message);
    }
    
    /// <summary>
    /// Log Debug Message
    /// </summary>
    /// <param name="i_Logger"></param>
    /// <param name="i_Handler"></param>
    public static void Information(this ILogger i_Logger,
      [InterpolatedStringHandlerArgument(nameof(i_Logger))]
      ref InformationLoggingInterpolatedStringHandler i_Handler)
    {
      i_Logger.LogInformation(i_Handler.ToStringAndClear());
    }

    /// <summary>
    /// Log Debug Message
    /// </summary>
    /// <param name="i_Logger"></param>
    /// <param name="i_Message"></param>
    public static void Information(this ILogger i_Logger,
      string i_Message)
    {
      i_Logger.LogInformation(i_Message);
    }
    
    /// <summary>
    /// Log Debug Message
    /// </summary>
    /// <param name="i_Logger"></param>
    /// <param name="i_Handler"></param>
    public static void Warning(this ILogger i_Logger,
      [InterpolatedStringHandlerArgument(nameof(i_Logger))]
      ref WarningLoggingInterpolatedStringHandler i_Handler)
    {
      i_Logger.LogWarning(i_Handler.ToStringAndClear());
    }
    
    /// <summary>
    /// Log Debug Message
    /// </summary>
    /// <param name="i_Logger"></param>
    /// <param name="i_Message"></param>
    public static void Warning(this ILogger i_Logger,
      string i_Message)
    {
      i_Logger.LogWarning(i_Message);
    }

    /// <summary>
    /// Log Debug Message
    /// </summary>
    /// <param name="i_Logger"></param>
    /// <param name="i_Exception"></param>
    /// <param name="i_Message"></param>
    public static void Warning(this ILogger i_Logger,
      Exception i_Exception,
      string i_Message)
    {
      i_Logger.LogWarning(i_Exception, i_Message);
    }

    /// <summary>
    /// Log Debug Message
    /// </summary>
    /// <param name="i_Logger"></param>
    /// <param name="i_Exception"></param>
    /// <param name="i_Handler"></param>
    public static void Warning(this ILogger i_Logger,
      Exception i_Exception,
      [InterpolatedStringHandlerArgument(nameof(i_Logger))]
      ref WarningLoggingInterpolatedStringHandler i_Handler)
    {
      i_Logger.LogWarning(i_Exception, i_Handler.ToStringAndClear());
    }
    
    /// <summary>
    /// Log Debug Message
    /// </summary>
    /// <param name="i_Logger"></param>
    /// <param name="i_Handler"></param>
    public static void Trace(this ILogger i_Logger,
      [InterpolatedStringHandlerArgument(nameof(i_Logger))]
      ref TraceLoggingInterpolatedStringHandler i_Handler)
    {
      i_Logger.LogTrace(i_Handler.ToStringAndClear());
    }
    
    /// <summary>
    /// Log Debug Message
    /// </summary>
    /// <param name="i_Logger"></param>
    /// <param name="i_Message"></param>
    public static void Trace(this ILogger i_Logger,
      string i_Message)
    {
      i_Logger.LogTrace(i_Message);
    }

    /// <summary>
    /// Log Error Message
    /// </summary>
    /// <param name="i_Logger"></param>
    /// <param name="i_Handler"></param>
    public static void Error(this ILogger i_Logger,
      [InterpolatedStringHandlerArgument(nameof(i_Logger))]
      ref ErrorLoggingInterpolatedStringHandler i_Handler)
    {
      i_Logger.LogError(i_Handler.ToStringAndClear());
    }
    
    /// <summary>
    /// Log Error Message
    /// </summary>
    /// <param name="i_Logger"></param>
    /// <param name="i_Exception"></param>
    /// <param name="i_Handler"></param>
    public static void Error(this ILogger i_Logger,
      Exception i_Exception,
      [InterpolatedStringHandlerArgument(nameof(i_Logger))]
      ref ErrorLoggingInterpolatedStringHandler i_Handler)
    {
      i_Logger.LogError(i_Exception, i_Handler.ToStringAndClear());
    }

    /// <summary>
    /// Log Error Message
    /// </summary>
    /// <param name="i_Logger"></param>
    /// <param name="i_Message"></param>
    public static void Error(this ILogger i_Logger,
      string i_Message)
    {
      i_Logger.LogError(i_Message);
    }
    
    /// <summary>
    /// Log Error Message
    /// </summary>
    /// <param name="i_Logger"></param>
    /// <param name="i_Exception"></param>
    /// <param name="i_Message"></param>
    public static void Error(this ILogger i_Logger,
      Exception i_Exception,
      string i_Message)
    {
      i_Logger?.LogError(i_Exception, i_Message);
    }
    
    /// <summary>
    /// Log Debug Message
    /// </summary>
    /// <param name="i_Logger"></param>
    /// <param name="i_Handler"></param>
    public static void Critical(this ILogger i_Logger,
      [InterpolatedStringHandlerArgument(nameof(i_Logger))]
      ref CriticalLoggingInterpolatedStringHandler i_Handler)
    {
      i_Logger.LogCritical(i_Handler.ToStringAndClear());
    }
    
    /// <summary>
    /// Log Debug Message
    /// </summary>
    /// <param name="i_Logger"></param>
    /// <param name="i_Exception"></param>
    /// <param name="i_Handler"></param>
    public static void Critical(this ILogger i_Logger,
      Exception i_Exception,
      [InterpolatedStringHandlerArgument(nameof(i_Logger))]
      ref CriticalLoggingInterpolatedStringHandler i_Handler)
    {
      i_Logger.LogCritical(i_Exception, i_Handler.ToStringAndClear());
    }
    
    /// <summary>
    /// Log Debug Message
    /// </summary>
    /// <param name="i_Logger"></param>
    /// <param name="i_Message"></param>
    public static void Critical(this ILogger i_Logger,
      string i_Message)
    {
      i_Logger.LogCritical(i_Message);
    }
    
    /// <summary>
    /// Log Debug Message
    /// </summary>
    /// <param name="i_Logger"></param>
    /// <param name="i_Exception"></param>
    /// <param name="i_Message"></param>
    public static void Critical(this ILogger i_Logger,
      Exception i_Exception,
      string i_Message)
    {
      i_Logger.LogCritical(i_Exception, i_Message);
    }
  }
}