// Copyright 2021 Sycorax Systemhaus GmbH

using System;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Logging.Extensions.Models;

namespace NiceCore.Logging.Extensions
{
  /// <summary>
  /// Logging Related Extension Methods for Tasks
  /// </summary>
  public static class TaskLoggingExtensionMethods
  {
    /// <summary>
    /// Logs errors that might have appeared in the task. This is only applicable if the task actually throws an uncaught exception
    /// since it uses OnlyOnFaulted
    /// </summary>
    /// <param name="i_Task"></param>
    /// <param name="i_Logger"></param>
    /// <param name="i_CallerName"></param>
    /// <returns></returns>
    public static void HandleErrorsInContinueWith(this Task i_Task, ILogger i_Logger, [CallerMemberName] string i_CallerName = null)
    {
      var continuationInfo = new HandleErrorsContinuationInfo()
      {
        Logger     = i_Logger,
        CallerName = i_CallerName
      };
      _ = i_Task.ContinueWith(static (task, state) =>
      {
        if (state is HandleErrorsContinuationInfo contInfo)
        {
          if (task.Exception != null)
          {
            var strBuilder = new StringBuilder("Task that was started in ")
              .Append(contInfo.CallerName)
              .AppendLine(" faulted with the following exception(s):");
            foreach (var innerException in task.Exception.InnerExceptions)
            {
              strBuilder.Append("Exception: ")
                .Append(innerException)
                .AppendLine();
            }

            var errorsString = strBuilder.ToString();
            contInfo.Logger.LogError(errorsString);
          }
        }
      }, continuationInfo, TaskContinuationOptions.OnlyOnFaulted);
    }
    
    /// <summary>
    /// Executes i_Func as FireAndForget
    /// </summary>
    /// <param name="i_Func"></param>
    /// <param name="i_Logger"></param>
    public static void ExecuteAsForget(this Func<ValueTask> i_Func, ILogger i_Logger)
    {
      _ = ExecuteAsForgetAwaited(i_Func, i_Logger).Preserve();
    }

    /// <summary>
    /// Executes i_Func as FireAndForget
    /// </summary>
    /// <param name="i_Func"></param>
    /// <param name="i_Value"></param>
    /// <param name="i_Logger"></param>
    public static void ExecuteAsForget<T>(this Func<T, ValueTask> i_Func, T i_Value, ILogger i_Logger)
    {
      _ = ExecuteAsForgetAwaited(i_Func, i_Value, i_Logger).Preserve();
    }

    private static async ValueTask ExecuteAsForgetAwaited(Func<ValueTask> i_Func, ILogger i_Logger)
    {
      try
      {
        await i_Func().ConfigureAwait(false);
      }
      catch (Exception e)
      {
        i_Logger.Error(e, $"Error during {nameof(ExecuteAsForget)}.");
      }
    }
    
    private static async ValueTask ExecuteAsForgetAwaited<T>(Func<T, ValueTask> i_Func, T i_Value, ILogger i_Logger)
    {
      try
      {
        await i_Func(i_Value).ConfigureAwait(false);
      }
      catch (Exception e)
      {
        i_Logger.Error(e, $"Error during {nameof(ExecuteAsForget)}.");
      }
    }
  }
}
