// Copyright 2021 Sycorax Systemhaus GmbH

using Microsoft.Extensions.Logging;

namespace NiceCore.Logging.Extensions.Models
{
  internal struct HandleErrorsContinuationInfo
  {
    public ILogger Logger { get; init; }
    
    public string CallerName { get; init; }
  }
}
