// Copyright 2021 Sycorax Systemhaus GmbH

using System;
using Microsoft.Extensions.Logging;

namespace NiceCore.Logging.Enums
{
  /// <summary>
  /// log Level Converter
  /// </summary>
  public static class LogLevelConverter
  {
    /// <summary>
    /// Parses a logLevel from string
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    /// <exception cref="InvalidOperationException"></exception>
    public static LogLevel FromString(string i_Value)
    {
      if (i_Value == nameof(LogLevel.Critical))
      {
        return LogLevel.Critical;
      }

      if (i_Value == nameof(LogLevel.Debug))
      {
        return LogLevel.Debug;
      }

      if (i_Value == nameof(LogLevel.Trace))
      {
        return LogLevel.Trace;
      }

      if (i_Value == nameof(LogLevel.Error))
      {
        return LogLevel.Error;
      }

      if (i_Value == nameof(LogLevel.Information))
      {
        return LogLevel.Information;
      }

      if (i_Value == nameof(LogLevel.Warning))
      {
        return LogLevel.Warning;
      }

      if (i_Value == nameof(LogLevel.None))
      {
        return LogLevel.None;
      }

      throw new InvalidOperationException($"Could not convert string {i_Value} to enum {typeof(LogLevel).FullName}. Value is not supported.");
    }

    /// <summary>
    /// Converts i_Value to a string
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    /// <exception cref="InvalidOperationException"></exception>
    public static string ToString(LogLevel i_Value)
    {
      if (i_Value == LogLevel.Critical)
      {
        return nameof(LogLevel.Critical);
      }

      if (i_Value == LogLevel.Debug)
      {
        return nameof(LogLevel.Debug);
      }

      if (i_Value == LogLevel.Trace)
      {
        return nameof(LogLevel.Trace);
      }

      if (i_Value == LogLevel.Error)
      {
        return nameof(LogLevel.Error);
      }

      if (i_Value == LogLevel.Information)
      {
        return nameof(LogLevel.Information);
      }

      if (i_Value == LogLevel.Warning)
      {
        return nameof(LogLevel.Warning);
      }

      if (i_Value == LogLevel.None)
      {
        return nameof(LogLevel.None);
      }

      throw new InvalidOperationException($"Could not convert LogLevel {i_Value} to string. Value is not supported.");
    }
  }
}
