﻿using Microsoft.Extensions.Logging;

namespace NiceCore.Logging.Providers.Default
{
  /// <summary>
  /// Event Source Nc Logger Provider
  /// </summary>
  public sealed class EventSourceNcLoggerProvider : INcLoggingProvider
  {
    /// <summary>
    /// Event Source
    /// </summary>
    public string Name { get; } = "EventSource";
    
    /// <summary>
    /// Apply to logging Builder
    /// </summary>
    /// <param name="t_Builder"></param>
    public void ApplyToLoggingBuilder(ILoggingBuilder t_Builder)
    {
      t_Builder.AddEventSourceLogger();
    }
  }
}