﻿using Microsoft.Extensions.Logging;

namespace NiceCore.Logging.Providers.Default
{
  /// <summary>
  /// Console NcLogger Provider
  /// </summary>
  public sealed class ConsoleNcLoggerProvider : INcLoggingProvider
  {
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; } = "Console";
    
    /// <summary>
    /// Apply To Logging Builder
    /// </summary>
    /// <param name="t_Builder"></param>
    public void ApplyToLoggingBuilder(ILoggingBuilder t_Builder)
    {
      t_Builder.AddSimpleConsole(opt => opt.IncludeScopes = true);
    }
  }
}