﻿using Microsoft.Extensions.Logging;

namespace NiceCore.Logging.Providers.Default
{
  /// <summary>
  /// Debug Nc logger Provider
  /// </summary>
  public sealed class DebugNcLoggerProvider : INcLoggingProvider
  {
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; } = "Debug";
    
    /// <summary>
    /// Apply to logging Builder
    /// </summary>
    /// <param name="t_Builder"></param>
    public void ApplyToLoggingBuilder(ILoggingBuilder t_Builder)
    {
      t_Builder.AddDebug();
    }
  }
}