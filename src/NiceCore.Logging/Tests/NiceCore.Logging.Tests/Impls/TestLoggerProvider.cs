﻿using Microsoft.Extensions.Logging;

namespace NiceCore.Logging.Tests.Impls
{
  public class TestLoggerProvider : ILoggerProvider
  {
    public TestInMemoryLogger Instance { get; } = new();
    
    public void Dispose()
    {
    }

    public ILogger CreateLogger(string categoryName)
    {
      return Instance;
    }
  }
}