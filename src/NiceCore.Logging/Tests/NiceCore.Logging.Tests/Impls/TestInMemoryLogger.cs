﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace NiceCore.Logging.Tests.Impls
{
  public class TestInMemoryLogger : ILogger
  {
    public List<string> Messages { get; } = new List<string>();
    
    public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
    {
      try
      {
        Messages.Add(formatter(state, exception));
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
      }  
    }

    public bool IsEnabled(LogLevel logLevel)
    {
      return true;
    }

    public IDisposable BeginScope<TState>(TState state)
      where TState : notnull
    {
      return null;
    }
  }
}