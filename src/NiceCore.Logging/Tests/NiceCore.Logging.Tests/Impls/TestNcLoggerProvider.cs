﻿using Microsoft.Extensions.Logging;

namespace NiceCore.Logging.Tests.Impls
{
  /// <summary>
  /// Test impl for logger
  /// </summary>
  public class TestNcLoggerProvider : INcLoggingProvider
  {
    public TestLoggerProvider Provider { get; } = new TestLoggerProvider();
    
    public string Name { get; } = "Test";
    
    public void ApplyToLoggingBuilder(ILoggingBuilder t_Builder)
    {
      t_Builder.AddProvider(Provider);
    }
  }
}
