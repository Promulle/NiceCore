﻿using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using NiceCore.Logging.Extensions;
using NiceCore.Logging.Tests.Impls;
using NUnit.Framework;

namespace NiceCore.Logging.Tests
{
  [TestFixture]
  public class LoggerFactorySetupTests
  {
    [Test]
    public void TestLoggerFactoryCreate()
    {
      var factory = new DirectLoggerFactorySetup(new[] {new TestNcLoggerProvider()}, LogLevel.Trace).SetupLoggerFactory();
      factory.Should().NotBeNull();
    }
    
    [Test]
    public async Task TestLogging()
    {
      var provider = new TestNcLoggerProvider();
      var factory = new DirectLoggerFactorySetup(new[] {provider}, LogLevel.Trace).SetupLoggerFactory();
      factory.Should().NotBeNull();
      var logger = factory.CreateLogger(string.Empty);
      logger.Should().NotBeNull();
      logger.Information("Some log");
      await Task.Delay(1).ConfigureAwait(false);
      provider.Provider.Instance.Messages.Should().HaveCount(1);
    }
  }
}