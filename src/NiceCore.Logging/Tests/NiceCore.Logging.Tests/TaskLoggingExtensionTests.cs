using System;
using System.Threading.Tasks;
using FluentAssertions;
using NiceCore.Logging.Extensions;
using NiceCore.Logging.Tests.Impls;
using NUnit.Framework;

namespace NiceCore.Logging.Tests
{
  /// <summary>
  /// Test Fixture for TaskLoggingExtension
  /// </summary>
  [TestFixture]
  public class TaskLoggingExtensionTests
  {
    /// <summary>
    /// Test Handle Errors In Continue With
    /// </summary>
    /// <returns></returns>
    [Test]
    public async Task TestHandleErrorsInContinueWith()
    {
      var logger = new TestInMemoryLogger();
      FunctionThatThrowsException().HandleErrorsInContinueWith(logger);
      await Task.Delay(10);
      logger.Messages.Count.Should().Be(1);
    }

    private async Task FunctionThatThrowsException()
    {
      await Task.Delay(1);
      throw new Exception();
    }
  }
}