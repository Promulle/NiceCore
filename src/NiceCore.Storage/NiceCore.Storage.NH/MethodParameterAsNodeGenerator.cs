using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using NHibernate.Hql.Ast;
using NHibernate.Linq.Functions;
using NHibernate.Linq.Visitors;

namespace NiceCore.Storage.NH
{
  /// <summary>
  /// Method Parameter as Node Generator
  /// </summary>
  public class MethodParameterAsNodeGenerator : BaseHqlGeneratorForMethod
  {
    /// <summary>
    /// Build Hql
    /// </summary>
    /// <param name="method"></param>
    /// <param name="targetObject"></param>
    /// <param name="arguments"></param>
    /// <param name="treeBuilder"></param>
    /// <param name="visitor"></param>
    /// <returns></returns>
    public override HqlTreeNode BuildHql(MethodInfo method, Expression targetObject, ReadOnlyCollection<Expression> arguments, HqlTreeBuilder treeBuilder, IHqlExpressionVisitor visitor)
    {
      return visitor.Visit(arguments.First());
    }
  }
}