﻿namespace NiceCore.Storage.NH.Config
{
  /// <summary>
  /// Constants class for nh config property keys
  /// </summary>
  public static class NHConfigPropertyKeys
  {
    /// <summary>
    /// Connection provider class
    /// </summary>
    public static readonly string PROVIDER = "connection.provider";
    
    /// <summary>
    /// Driver class
    /// </summary>
    public static readonly string DRIVER_CLASS = "connection.driver_class";

    /// <summary>
    /// Connection string
    /// </summary>
    public static readonly string CONNECTION_STRING = "connection.connection_string";

    /// <summary>
    /// Dialect class
    /// </summary>
    public static readonly string DIALECT = "dialect";

    /// <summary>
    /// Show sql
    /// </summary>
    public static readonly string SHOW_SQL = "show_sql";
    
    /// <summary>
    /// Default schema
    /// </summary>
    public static readonly string DEFAULT_SCHEMA = "default_schema";
    
    /// <summary>
    /// Isolation level
    /// </summary>
    public static readonly string ISOLATION = "connection.isolation";
  }
}