﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NHibernate.Cfg;
using NiceCore.Base.Services;
using NiceCore.Storage.DomainBuilder;

namespace NiceCore.Storage.NH.Config.Services
{
  /// <summary>
  /// Service for creating/Loading Configurations for ConnectionDefinitions
  /// </summary>
  public interface INHConnectionConfigurationFileService : IService
  {
    /// <summary>
    /// Creates an NH Config from i_Definition
    /// </summary>
    /// <param name="i_Definition"></param>
    /// <param name="i_DomainBuilders"></param>
    /// <returns></returns>
    Task<Configuration> CreateFreshConfiguration(ConnectionDefinition i_Definition, IEnumerable<IDomainBuilder> i_DomainBuilders);

    /// <summary>
    /// Creates a new configuration and serializes it if wanted
    /// </summary>
    /// <param name="i_ConfigurationModel"></param>
    /// <param name="i_Definition"></param>
    /// <param name="i_ApplicableDomainBuilders"></param>
    /// <returns></returns>
    Task<Configuration> CreateFreshConfiguration(NHConfigModel i_ConfigurationModel, ConnectionDefinition i_Definition, IEnumerable<IDomainBuilder> i_ApplicableDomainBuilders);
  }
}