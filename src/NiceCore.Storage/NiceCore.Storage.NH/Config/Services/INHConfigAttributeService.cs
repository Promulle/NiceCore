namespace NiceCore.Storage.NH.Config.Services
{
  /// <summary>
  /// NH Config Attribute Service
  /// </summary>
  public interface INHConfigAttributeService
  {
    /// <summary>
    /// Gets the driver class for i_Definition WITHOUT fully initializing the connection
    /// </summary>
    /// <param name="i_Definition"></param>
    /// <returns></returns>
    NHConfigModel GetNHConfigAttributes(ConnectionDefinition i_Definition);
  }
}
