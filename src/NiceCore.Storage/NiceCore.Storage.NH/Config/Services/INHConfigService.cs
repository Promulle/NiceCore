﻿using System.Collections.Generic;
using NiceCore.Base.Services;

namespace NiceCore.Storage.NH.Config.Services
{
  /// <summary>
  /// Interface for NHConfigService
  /// </summary>
  public interface INHConfigService : IService
  {
    /// <summary>
    /// Returns list of defined connections
    /// </summary>
    /// <returns></returns>
    IEnumerable<ConnectionDefinition> GetConfiguredConnections();

    /// <summary>
    /// Returns the config model read from nicecore configuration
    /// </summary>
    /// <param name="i_Key"></param>
    /// <returns></returns>
    NHConfigModel GetConfigFromConfiguration(string i_Key);
  }
}