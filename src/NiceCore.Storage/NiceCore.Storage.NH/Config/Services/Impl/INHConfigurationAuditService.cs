using System;
using NHibernate.Envers.Configuration.Fluent;

namespace NiceCore.Storage.NH.Config.Services.Impl
{
  /// <summary>
  /// INHConfigurationAuditService
  /// </summary>
  public interface INHConfigurationAuditService
  {
    /// <summary>
    /// Apply Fluent Mapping
    /// </summary>
    /// <param name="i_NHFluentAudit"></param>
    /// <param name="i_AuditedType"></param>
    void ApplyFluentMapping(IFluentAudit i_NHFluentAudit, Type i_AuditedType);
  }
}