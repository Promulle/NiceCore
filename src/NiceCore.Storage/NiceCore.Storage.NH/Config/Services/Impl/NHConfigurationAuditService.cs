using System;
using System.Collections.Generic;
using NHibernate.Envers.Configuration.Fluent;
using NiceCore.Base.Services;
using NiceCore.ServiceLocation;
using NiceCore.Storage.NH.Auditing;

namespace NiceCore.Storage.NH.Config.Services.Impl
{
  /// <summary>
  /// NHConfigurationAuditService
  /// </summary>
  [InitializeOnResolve]
  [Register(InterfaceType = typeof(INHConfigurationAuditService), LifeStyle = LifeStyles.Singleton)]
  public class NHConfigurationAuditService : BaseService, INHConfigurationAuditService
  {
    private readonly Dictionary<Type, INcFluentAuditMapping> m_AuditMappings = new();
    
    /// <summary>
    /// Audit Mappings
    /// </summary>
    public IList<INcFluentAuditMapping> AuditMappings { get; set; }

    /// <summary>
    /// Apply Fluent Mapping
    /// </summary>
    /// <param name="i_NHFluentAudit"></param>
    /// <param name="i_AuditedType"></param>
    public void ApplyFluentMapping(IFluentAudit i_NHFluentAudit, Type i_AuditedType)
    {
      if (m_AuditMappings.TryGetValue(i_AuditedType, out var fluentAuditMapping))
      {
        fluentAuditMapping.Apply(i_NHFluentAudit);
      }
    }
    
    /// <summary>
    /// Initialize
    /// </summary>
    protected override void InternalInitialize()
    {
      if (AuditMappings != null)
      {
        foreach (var auditMapping in AuditMappings)
        {
          m_AuditMappings[auditMapping.MappedType] = auditMapping;
        }
      }
    }
  }
}