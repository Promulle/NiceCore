﻿using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using NiceCore.Base;
using NiceCore.Base.Services;
using NiceCore.Logging;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation;
using NiceCore.ServiceLocation.Helpers;
using NiceCore.Services.Config;

namespace NiceCore.Storage.NH.Config.Services.Impl
{
  /// <summary>
  /// Service for handling definitions of connections
  /// </summary>
  [Register(InterfaceType = typeof(INHConfigService))]
  public class NHConfigService : BaseService, INHConfigService
  {
    private const string KEY_DEFINITIONS = "ConnectionDefinitions.Definitions";

    /// <summary>
    /// Service for handling config
    /// </summary>
    public INcConfiguration Config { get; set; }

    /// <summary>
    /// Container used for resolving the value converter
    /// </summary>
    public IServiceContainer Container { get; set; }
    
    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<NHConfigService> Logger { get; set; }

    /// <summary>
    /// Returns configured connections
    /// </summary>
    /// <returns></returns>
    public IEnumerable<ConnectionDefinition> GetConfiguredConnections()
    {
      return Config.GetCollection<ConnectionDefinition>(KEY_DEFINITIONS);
    }

    /// <summary>
    /// Returns the config model read from nicecore configuration
    /// </summary>
    /// <param name="i_Key"></param>
    /// <returns></returns>
    public NHConfigModel GetConfigFromConfiguration(string i_Key)
    {
      Logger.Information("Searching for config model in configuration...");
      var model = Config.GetValue<NHConfigModel>(i_Key, null);
      //set connectionstring by key
      if (model?.ConnectionStringKey != null && model.ConnectionString == null)
      {
        Logger.Information($"Model found, but without connection string, using configuration key {model.ConnectionStringKey} to find the connection string.");
        var conStr = Config.GetValue<string>(model.ConnectionStringKey, null);
        using (var transformerUsing = Container.Locate<IOneWayValueTransformer<string, string>>(NcStorageServices.CONNECTION_STRING_TRANSFORMER))
        {
          if (transformerUsing.HasInstance())
          {
            Logger.Information($"Transforming found connection string using transferformer {transformerUsing.Instance().GetType().FullName}");
            conStr = transformerUsing.Instance().Transform(conStr);
          }
        }
        model.ConnectionString = conStr;
      }
      return model;
    }
  }
}
