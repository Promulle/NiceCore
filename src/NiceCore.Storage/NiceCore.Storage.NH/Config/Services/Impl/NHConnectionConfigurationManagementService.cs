﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NHibernate.Cfg;
using NiceCore.ServiceLocation;
using NiceCore.Storage.ConfigurationModel;
using NiceCore.Storage.DomainBuilder;
using NiceCore.Storage.Services;
using NiceCore.Storage.Services.Impl;

namespace NiceCore.Storage.NH.Config.Services.Impl
{
  /// <summary>
  /// NH connection configuration management service for nhibernate
  /// </summary>
  [Register(InterfaceType = typeof(IConnectionConfigurationManagementService<ConnectionDefinition, Configuration>), LifeStyle = LifeStyles.Singleton)]
  public class NHConnectionConfigurationManagementService : BaseConnectionConfigurationManagementService<ConnectionDefinition, Configuration>
  {
    /// <summary>
    /// NH File Service
    /// </summary>
    public INHConnectionConfigurationFileService NHConnectionConfigurationFileService { get; set; }
    
    /// <summary>
    /// Create config from definition
    /// </summary>
    /// <param name="i_Definition"></param>
    /// <param name="i_DomainBuilder"></param>
    /// <returns></returns>
    protected override async Task<Configuration> CreateConfigFromDefinition(ConnectionDefinition i_Definition, IEnumerable<IDomainBuilder> i_DomainBuilder)
    {
      return await NHConnectionConfigurationFileService.CreateFreshConfiguration(i_Definition, i_DomainBuilder).ConfigureAwait(false);
    }

    /// <summary>
    /// Get reasonable default definition
    /// </summary>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    protected override ConnectionDefinition GetReasonableDefaultDefinition(string i_Name)
    {
      return new ConnectionDefinition()
      {
        Name = i_Name,
        AllowAuditing = true,
        SchemaUpdate = false,
        UseSerialization = false
      };
    }

    /// <summary>
    /// Create config from model
    /// </summary>
    /// <param name="i_Model"></param>
    /// <param name="i_Definition"></param>
    /// <param name="i_DomainBuilder"></param>
    /// <returns></returns>
    /// <exception cref="NotSupportedException"></exception>
    protected override async Task<Configuration> CreateConfigFromModel(IConnectionConfiguration i_Model, ConnectionDefinition i_Definition, IEnumerable<IDomainBuilder> i_DomainBuilder)
    {
      if (i_Model is NHConfigModel model)
      {
        return await NHConnectionConfigurationFileService.CreateFreshConfiguration(model, i_Definition, i_DomainBuilder).ConfigureAwait(false);
      }

      throw new InvalidCastException($"Could not create config from Model. Argument {i_Model} of type {i_Model.GetType().FullName} could not be cast to {typeof(NHConfigModel).FullName}.");
    }
  }
}