using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Microsoft.Extensions.Logging;
using NiceCore.Logging;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation;

namespace NiceCore.Storage.NH.Config.Services.Impl
{
  /// <summary>
  /// NH Config Attribute Service
  /// </summary>
  [Register(InterfaceType = typeof(INHConfigAttributeService))]
  public sealed class NHConfigAttributeService : INHConfigAttributeService
  {
    /// <summary>
    /// Configuration
    /// </summary>
    public INHConfigService NHConfigService { get; set; }
    
    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<NHConfigAttributeService> Logger { get; set; }

    /// <summary>
    /// Get NH Config Attributes
    /// </summary>
    /// <param name="i_Definition"></param>
    /// <returns></returns>
    public NHConfigModel GetNHConfigAttributes(ConnectionDefinition i_Definition)
    {
      var model = NHConfigService.GetConfigFromConfiguration(i_Definition.ConfigFile);
      if (model != null)
      {
        return model;
      }

      if (File.Exists(i_Definition.ConfigFile))
      {
        try
        {
          var xDoc = XDocument.Load(i_Definition.ConfigFile);
          if (xDoc.Root == null)
          {
            return null;
          }
          var child = xDoc.Root.Nodes().FirstOrDefault();
          if (child is XElement sessionFactoryElement && sessionFactoryElement.Name.LocalName == "session-factory")
          {
            var elements = sessionFactoryElement.Nodes()
              .OfType<XElement>()
              .Where(r => r.Name.LocalName == "property" && r.HasAttributes && r.Attribute("name") != null)
              .ToArray();
            return ConvertXMLNodesToModel(elements);
          }
        }
        catch (Exception ex)
        {
          Logger.Error(ex, $"Error while reading xml file {i_Definition.ConfigFile}. Could not read driver class from that file");
        }
      }
      return null;
    }

    private static NHConfigModel ConvertXMLNodesToModel(XElement[] i_Elements)
    {
      var model = new NHConfigModel();
      foreach (var xmlNode in i_Elements)
      {
        var nameAttrValue = xmlNode.Attribute("name")!.Value;
        var nodeValue = xmlNode.Value;
        if (nameAttrValue == NHConfigPropertyKeys.DIALECT)
        {
          model.Dialect = nodeValue;
        }
        else if (nameAttrValue == NHConfigPropertyKeys.SHOW_SQL)
        {
          model.ShowSql = nodeValue;
        }
        else if (nameAttrValue == NHConfigPropertyKeys.PROVIDER)
        {
          model.ConnectionProvider = nodeValue;
        }
        else if (nameAttrValue == NHConfigPropertyKeys.ISOLATION)
        {
          model.IsolationLevel = nodeValue;
        }
        else if (nameAttrValue == NHConfigPropertyKeys.DRIVER_CLASS)
        {
          model.DriverClass = nodeValue;
        }
        else if (nameAttrValue == NHConfigPropertyKeys.DEFAULT_SCHEMA)
        {
          model.DefaultSchema = nodeValue;
        }
        else if (nameAttrValue == NHConfigPropertyKeys.CONNECTION_STRING)
        {
          model.ConnectionString = nodeValue;
        }
      }
      return model;
    }
  }
}
