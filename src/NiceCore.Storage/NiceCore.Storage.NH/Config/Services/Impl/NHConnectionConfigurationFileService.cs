﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.Extensions.Logging;
using NHibernate.Cfg;
using NHibernate.Envers.Configuration;
using NHibernate.Envers.Configuration.Fluent;
using NHibernate.Event;
using NHibernate.Mapping;
using NiceCore.Base.Services;
using NiceCore.Extensions;
using NiceCore.Logging;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using NiceCore.Services.Encryption;
using NiceCore.Storage.Auditing;
using NiceCore.Storage.Auditing.Attributes;
using NiceCore.Storage.Config.AvailableSettings.AdditionalEncryptionProviderMappings;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.DomainBuilder;
using NiceCore.Storage.NH.Auditing;
using NiceCore.Storage.NH.Config.AvailableSettings;
using NiceCore.Storage.NH.Conversation.EventListeners;
using NiceCore.Storage.NH.Mapping;
using NiceCore.Storage.NH.SessionFactories;

namespace NiceCore.Storage.NH.Config.Services.Impl
{
  /// <summary>
  /// Impl for INHConnectionConfigurationFileService
  /// </summary>
  [InitializeOnResolve]
  [Register(InterfaceType = typeof(INHConnectionConfigurationFileService))]
  public class NHConnectionConfigurationFileService : BaseService, INHConnectionConfigurationFileService
  {
    /// <summary>
    /// Service Container
    /// </summary>
    public IServiceContainer Container { get; set; }

    /// <summary>
    /// NHConfigService
    /// </summary>
    public INHConfigService NHConfigService { get; set; }

    /// <summary>
    /// Configuration
    /// </summary>
    public INcConfiguration Configuration { get; set; }

    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<NHConnectionConfigurationFileService> Logger { get; set; }

    /// <summary>
    /// Fluent Mapping Support Logger
    /// </summary>
    public ILogger<FluentMappingSupport> FluentMappingSupportLogger { get; set; }

    /// <summary>
    /// Encryption Service
    /// </summary>
    public INcEncryptionService NcEncryptionService { get; set; }

    /// <summary>
    /// Event Listener Factory
    /// </summary>
    public INcSessionEventListenerFactory EventListenerFactory { get; set; }

    /// <summary>
    /// Active Conversation store
    /// </summary>
    public IActiveConversationStore ActiveConversationStore { get; set; }

    /// <summary>
    /// Configuration Audit Service
    /// </summary>
    public INHConfigurationAuditService ConfigurationAuditService { get; set; }

  /// <summary>
    /// Audit On Perform Listener
    /// </summary>
    public IAuditOnPerformListener AuditOnPerformListener { get; set; }

    /// <summary>
    /// Creates a new configuration and serializes it if wanted
    /// </summary>
    /// <param name="i_Definition"></param>
    /// <param name="i_ApplicableDomainBuilders"></param>
    /// <returns></returns>
    public async Task<Configuration> CreateFreshConfiguration(ConnectionDefinition i_Definition, IEnumerable<IDomainBuilder> i_ApplicableDomainBuilders)
    {
      Logger.Information($"Creating new configuraion instance for connection: {i_Definition.Name} from file.");
      var conf = await ReadConfiguration(i_Definition.ConfigFile, GetEncryptionProvider(i_Definition)).ConfigureAwait(false);
      await ApplyNiceCoreSystemsToConfig(conf, i_Definition, i_ApplicableDomainBuilders).ConfigureAwait(false);
      return conf;
    }

    /// <summary>
    /// Creates a new configuration and serializes it if wanted
    /// </summary>
    /// <param name="i_ConfigurationModel"></param>
    /// <param name="i_Definition"></param>
    /// <param name="i_ApplicableDomainBuilders"></param>
    /// <returns></returns>
    public async Task<Configuration> CreateFreshConfiguration(NHConfigModel i_ConfigurationModel, ConnectionDefinition i_Definition, IEnumerable<IDomainBuilder> i_ApplicableDomainBuilders)
    {
      Logger.Information($"Creating new configuration instance for connection: {i_Definition.Name}");
      var conf = InstantiateConfigurationFromModel(i_ConfigurationModel);
      await ApplyNiceCoreSystemsToConfig(conf, i_Definition, i_ApplicableDomainBuilders).ConfigureAwait(false);
      return conf;
    }

    private string GetEncryptionProvider(ConnectionDefinition i_Definition)
    {
      if (i_Definition.EncryptionProvider != null)
      {
        return i_Definition.EncryptionProvider;
      }

      var additionalMappings = Configuration.GetCollection<AdditionalEncryptionProviderMapping>(AdditionalEncryptionProviderMappingsSetting.KEY);
      if (additionalMappings.Count > 0)
      {
        foreach (var addMapping in additionalMappings)
        {
          if (addMapping.ConnectionName == i_Definition.Name && addMapping.EncryptionProvider != null)
          {
            return addMapping.EncryptionProvider;
          }
        }
      }

      return null;
    }

    private static Configuration InstantiateConfigurationFromModel(NHConfigModel i_ConfigurationModel)
    {
      var cfg = new Configuration();
      cfg.SetProperties(i_ConfigurationModel.ConvertToDictionary());
      return cfg;
    }

    private async Task ApplyNiceCoreSystemsToConfig(Configuration t_Configuration, ConnectionDefinition i_Definition, IEnumerable<IDomainBuilder> i_ApplicableDomainBuilders)
    {
      t_Configuration.SetProperty("nhibernate.envers.audit_strategy", typeof(NcAuditStrategy).AssemblyQualifiedName);
      ApplyNHConfigurator(t_Configuration);
      i_ApplicableDomainBuilders.ForEachItem(builder =>
      {
        var assembly = builder.GetType().Assembly;
        Logger.Information($"adding mappings from Assembly {assembly.FullName}");
        t_Configuration.AddAssembly(assembly);
      });
      Logger.Information("Adding fluently mapped mappings.");
      await FluentMappingSupport.AddFluentMappingsToConfig(t_Configuration, i_Definition, Container, GetTypesToExcludeFromFluentMapping(), FluentMappingSupportLogger).ConfigureAwait(false);
      ApplyEnversIfNeeded(t_Configuration, i_Definition);
      ApplyNcEventListener(t_Configuration, i_Definition);
      t_Configuration.LinqToHqlGeneratorsRegistry<NcLinqToHqlGeneratorsRegistry>();
    }

    private HashSet<Type> GetTypesToExcludeFromFluentMapping()
    {
      var typesToExclude = Configuration.GetCollection<string>(ExcludeTypesFromMappingSetting.KEY);
      if (typesToExclude == null || typesToExclude.Count == 0)
      {
        return null;
      }

      var res = new HashSet<Type>();
      foreach (var typeString in typesToExclude)
      {
        var typeFromString = Type.GetType(typeString);
        if (typeFromString != null)
        {
          res.Add(typeFromString);
        }
      }
      return res;
    }

    private void ApplyNcEventListener(Configuration t_Config, ConnectionDefinition i_Definition)
    {
      var preEventListener = EventListenerFactory.CreateNewPreEventListener(i_Definition.Name);
      var postEventListener = EventListenerFactory.CreateNewPostEventListener(i_Definition.Name);
      
      var availableDelete = t_Config.EventListeners.DeleteEventListeners;
      var deleteListeners = new List<IDeleteEventListener>();
      deleteListeners.Add(preEventListener);
      deleteListeners.AddRange(availableDelete);
      deleteListeners.Add(postEventListener);
      
      t_Config.SetListeners(ListenerType.Delete, deleteListeners.ToArray());
      
      var availableSave = t_Config.EventListeners.SaveOrUpdateEventListeners;
      var saveListeners = new List<ISaveOrUpdateEventListener>();
      saveListeners.Add(preEventListener);
      saveListeners.AddRange(availableSave);
      saveListeners.Add(postEventListener);
      t_Config.SetListeners(ListenerType.SaveUpdate, saveListeners.ToArray());
    }

    private void ApplyEnversIfNeeded(NHibernate.Cfg.Configuration t_Config, ConnectionDefinition i_Definition)
    {
      var configInjected = Configuration != null;
      Logger.Debug($"Checking if envers support should be added, configuration exists: {configInjected}");
      if (configInjected)
      {
        var enableEnvers = i_Definition.AllowAuditing;
        Logger.Debug($"Auditing enabled for connection {i_Definition.Name}: {i_Definition.AllowAuditing}. To change this, change value for property {nameof(i_Definition.AllowAuditing)} of connection definition.");
        if (enableEnvers)
        {
          try
          {
            var enversCfg = CreateEnversCfgFor(t_Config);
            t_Config.IntegrateWithEnvers(enversCfg);
            var conf = AuditConfiguration.GetFor(t_Config);
            if (conf.GlobalCfg.AuditStrategy is INcAuditStrategy ncAuditStrategy)
            {
              ncAuditStrategy.IntegrateWithNiceCore(AuditOnPerformListener, Logger);
            }
          }
          catch (Exception ex)
          {
            Logger.Error(ex, "Support for auditing is enabled but something went wrong during creation of envers cfg or it's integration with the NHibernate configuration.");
            if (Configuration.GetValue(ConversationStrictModeSetting.KEY , false))
            {
              Logger.Error("Strict mode is on -> rethrowing exception that occured when configuring envers.");
              throw;
            }
          }
        }
      }
    }

    private FluentConfiguration CreateEnversCfgFor(NHibernate.Cfg.Configuration i_Config)
    {
      var cfg = new FluentConfiguration();
      var typesToIgnore = Configuration?.GetCollection<string>(EnversIgnoreTypeNamesSetting.KEY);
      ApplyCustomNamingIfNeeded(i_Config);
      ApplyEnversConfigurator(cfg);
      i_Config.ClassMappings.ForEachItem(mapping =>
      {
        var type = GetTypeToAudit(mapping);
        if (type != null && TypeIsOk(type, typesToIgnore))
        {
          var builder = cfg.Audit(type);
          ConfigurationAuditService.ApplyFluentMapping(builder, type);
          Logger.Information($"Added {type.FullName} to classes that should be audited.");
        }
      });
      return cfg;
    }

    private static bool TypeIsOk(Type i_Type, List<string> i_IgnoredTypes)
    {
      return (i_IgnoredTypes == null ||
             i_IgnoredTypes.Count == 0 ||
             !i_IgnoredTypes.Contains(i_Type.FullName)) && 
             (i_Type.GetCustomAttributes(typeof(DoNotAuditAttribute), false).OfType<DoNotAuditAttribute>().FirstOrDefault() == null);
    }

    private void ApplyEnversConfigurator(FluentConfiguration t_Config)
    {
      var configurator = Container?.Resolve<IEnversConfigurator>();
      if (configurator != null)
      {
        Logger.Information($"Applying envers configurator of type {configurator.GetType().Name}");
        configurator.ApplyChangesToConfig(t_Config);
        Container.Release(configurator);
      }
    }

    private void ApplyNHConfigurator(Configuration t_Config)
    {
      var configurator = Container?.Resolve<INHConfigurator>();
      if (configurator != null)
      {
        Logger.Information($"Applying nh configurator of type {configurator.GetType().Name}");
        configurator.ApplyChangesToConfig(t_Config);
        Container.Release(configurator);
      }
    }

    private void ApplyCustomNamingIfNeeded(Configuration t_Cfg)
    {
      Logger.Information($"Checking whether to use custom naming for envers. Configuration service exists: {Configuration != null}");
      if (Configuration?.GetValue(UseCustomNamingSetting.KEY, false) ?? false)
      {
        Logger.Debug($"Applying custom naming for envers. Type of Naming: {typeof(CustomEnversNamingStrategy).FullName}");
        t_Cfg.SetEnversProperty(ConfigurationKey.TableNameStrategy, typeof(CustomEnversNamingStrategy));
      }
    }

    private Type GetTypeToAudit(PersistentClass i_Mapping)
    {
      try
      {
        return Type.GetType(i_Mapping.ClassName);
      }
      catch (Exception ex)
      {
        Logger.Error(ex, $"Could not get type for class to be audited. ClassName: {i_Mapping.ClassName} for entity {i_Mapping.EntityName}");
      }
      return null;
    }

    /// <summary>
    /// Workaround method so that Configuration().Configure actually accepts paths with # and other characters
    /// </summary>
    /// <param name="i_FilePath"></param>
    /// <param name="i_EncryptionProvider"></param>
    /// <returns></returns>
    private async ValueTask<NHibernate.Cfg.Configuration> ReadConfiguration(string i_FilePath, string i_EncryptionProvider)
    {
      var conf = CreateConfigFromConfig(i_FilePath);
      if (conf == null)
      {
        conf = await CreateConfigFromXml(i_FilePath, i_EncryptionProvider).ConfigureAwait(false);
      }
      return conf;
    }

    private NHibernate.Cfg.Configuration CreateConfigFromConfig(string i_Key)
    {
      Logger.Information("trying to create config from NcConfiguration...");
      var confModel = NHConfigService.GetConfigFromConfiguration(i_Key);
      if (confModel != null)
      {
        Logger.Information($"Found confModel under key {i_Key}");
        return new NHibernate.Cfg.Configuration()
          .SetProperties(confModel.ConvertToDictionary());
      }
      Logger.Information($"no value for {i_Key} found");
      return null;
    }

    private async ValueTask<NHibernate.Cfg.Configuration> CreateConfigFromXml(string i_FilePath, string i_EncryptionProvider)
    {
      if (i_FilePath == null)
      {
        throw new ArgumentException($"Can not create config from XML: Argument {nameof(i_FilePath)} was provided as null but cannot be.");
      }
      NHibernate.Cfg.Configuration conf = null;
      Logger.Information($"trying to create config from file: {i_FilePath}");
      if (File.Exists(i_FilePath))
      {
        try
        {
          if (i_EncryptionProvider == null)
          {
            conf = ReadXMLConfigurationUnEncrypted(i_FilePath);
          }
          else
          {
            conf = await ReadXMLConfigurationEncrypted(i_FilePath, i_EncryptionProvider).ConfigureAwait(false);
          }
        }
        catch (Exception ex)
        {
          Logger.Error(ex, $"There was an error during reading of xml from file {i_FilePath}");
          throw;
        }
      }
      else
      {
        var completeExpectedPath = Path.GetFullPath(i_FilePath);
        throw new FileNotFoundException($"Could not create config from xml file because the xml file was not found. Expected file: {completeExpectedPath} to exist but does not.", i_FilePath);
      }
      Logger.Information($"succeeded to create config from {i_FilePath}: {conf != null}");
      return conf;
    }

    private async ValueTask<Configuration> ReadXMLConfigurationEncrypted(string i_FilePath, string i_EncryptionProviderName)
    {
      var text = await File.ReadAllTextAsync(i_FilePath).ConfigureAwait(false);
      var encryptedConfigData = Convert.FromBase64String(text);
      var actualConfigData = await NcEncryptionService.DecryptWith(encryptedConfigData, i_EncryptionProviderName).ConfigureAwait(false);
      using var strReader = new StringReader(Encoding.UTF8.GetString(actualConfigData));
      using var xmlReader = new XmlTextReader(strReader);
      return new NHibernate.Cfg.Configuration()
        .Configure(xmlReader);
    }

    private static Configuration ReadXMLConfigurationUnEncrypted(string i_FilePath)
    {
      using var fileStream = File.OpenRead(i_FilePath);
      using var xmlReader = new XmlTextReader(fileStream);
      return new NHibernate.Cfg.Configuration()
        .Configure(xmlReader);
    }
  }
}
