﻿using NHibernate.Envers.Configuration.Fluent;

namespace NiceCore.Storage.NH.Config
{
  /// <summary>
  /// Configurator that is applied to a created envers config
  /// </summary>
  public interface IEnversConfigurator
  {
    /// <summary>
    /// Apply changes to the configuration instance
    /// </summary>
    /// <param name="t_Configuration"></param>
    void ApplyChangesToConfig(FluentConfiguration t_Configuration);
  }
}
