﻿using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using System;

namespace NiceCore.Storage.NH.Config.AvailableSettings
{
  /// <summary>
  /// Setting for StrictMode of conversation
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class ConversationStrictModeSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// Key for strict mode
    /// </summary>
    public static readonly string KEY = "NiceCore.Storage.Conversation.Strict";

    /// <summary>
    /// key
    /// </summary>
    public string Key { get; } = KEY;

    /// <summary>
    /// Type
    /// </summary>
    public Type SettingType { get; } = typeof(bool);
  }
}
