﻿using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using System;

namespace NiceCore.Storage.NH.Config.AvailableSettings
{
  /// <summary>
  /// Setting that determines whether the custom envers naming is used or not
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class UseCustomNamingSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// KEY
    /// </summary>
    public static readonly string KEY = "NiceCore.Storage.NH.Envers.UseAlternativeNaming";

    /// <summary>
    /// Key of setting
    /// </summary>
    public string Key { get; } = KEY;

    /// <summary>
    /// Type of Setting
    /// </summary>
    public Type SettingType { get; } = typeof(bool);
  }
}
