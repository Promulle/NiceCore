using System;
using System.Collections.Generic;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Storage.NH.Config.AvailableSettings
{
  /// <summary>
  /// Setting that enables excluding of certain fluentlyMapped instances
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public sealed class ExcludeTypesFromMappingSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// KEY
    /// </summary>
    public const string KEY = "NiceCore.Storage.NH.ExcludedTypes";

    /// <summary>
    /// Key
    /// </summary>
    public string Key { get; } = KEY;

    /// <summary>
    /// Type of setting
    /// </summary>
    public Type SettingType { get; } = typeof(List<string>);
  }
}