﻿using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using System;
using System.Collections.Generic;

namespace NiceCore.Storage.NH.Config.AvailableSettings
{
  /// <summary>
  /// Setting for Types to be ignored by envers
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class EnversIgnoreTypeNamesSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// Types to ignore
    /// </summary>
    public static readonly string KEY = "NiceCore.Storage.NH.Envers.IgnoreTypes";

    /// <summary>
    /// Key
    /// </summary>
    public string Key { get; } = KEY;

    /// <summary>
    /// Type
    /// </summary>
    public Type SettingType { get; } = typeof(List<string>);
  }
}
