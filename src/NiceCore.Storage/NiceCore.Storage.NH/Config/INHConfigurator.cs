﻿using NHibernate.Cfg;

namespace NiceCore.Storage.NH.Config
{
  /// <summary>
  /// Configurator used to change settings in NHConfig
  /// </summary>
  public interface INHConfigurator
  {
    /// <summary>
    /// Apply changes to the configuration instance
    /// </summary>
    /// <param name="t_Configuration"></param>
    void ApplyChangesToConfig(Configuration t_Configuration);
  }
}
