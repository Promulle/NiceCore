﻿using System.Collections.Generic;
using NiceCore.Storage.ConfigurationModel;

namespace NiceCore.Storage.NH.Config
{
  /// <summary>
  /// Model for nh config read from NcConfiguration
  /// </summary>
  public class NHConfigModel : IConnectionConfiguration
  {
    /// <summary>
    /// Default schema
    /// </summary>
    public string DefaultSchema { get; set; }

    /// <summary>
    /// ShowSql
    /// </summary>
    public string ShowSql { get; set; }

    /// <summary>
    /// Connection String
    /// </summary>
    public string ConnectionString { get; set; }

    /// <summary>
    /// Connection String
    /// </summary>
    public string ConnectionStringKey { get; set; }

    /// <summary>
    /// Dialect
    /// </summary>
    public string Dialect { get; set; }

    /// <summary>
    /// Driver class
    /// </summary>
    public string DriverClass { get; set; }

    /// <summary>
    /// Connection Provider
    /// </summary>
    public string ConnectionProvider { get; set; }

    /// <summary>
    /// Isolation level
    /// </summary>
    public string IsolationLevel { get; set; }

    /// <summary>
    /// Converts this configmodel into a dictionary you can configure a nh config by
    /// </summary>
    /// <returns></returns>
    public IDictionary<string, string> ConvertToDictionary()
    {
      var dict = new Dictionary<string, string>();
      if (!string.IsNullOrEmpty(ConnectionString))
      {
        dict.Add(NHConfigPropertyKeys.CONNECTION_STRING, ConnectionString);
      }
      if (!string.IsNullOrEmpty(DefaultSchema))
      {
        dict.Add(NHConfigPropertyKeys.DEFAULT_SCHEMA, DefaultSchema);
      }
      if (!string.IsNullOrEmpty(Dialect))
      {
        dict.Add(NHConfigPropertyKeys.DIALECT, Dialect);
      }
      if (!string.IsNullOrEmpty(ConnectionProvider))
      {
        dict.Add(NHConfigPropertyKeys.PROVIDER, ConnectionProvider);
      }
      if (!string.IsNullOrEmpty(ShowSql))
      {
        dict.Add(NHConfigPropertyKeys.SHOW_SQL, ShowSql);
      }
      if (!string.IsNullOrEmpty(DriverClass))
      {
        dict.Add(NHConfigPropertyKeys.DRIVER_CLASS, DriverClass);
      }
      if (!string.IsNullOrEmpty(IsolationLevel))
      {
        dict.Add(NHConfigPropertyKeys.ISOLATION, IsolationLevel);
      }
      return dict;
    }
  }
}
