﻿using System.Configuration;

namespace NiceCore.Storage.NH.Config.Xml
{
  /// <summary>
  /// Section for Definitions of Connections
  /// </summary>
  public class ConnectionDefinitionsSection : ConfigurationSection, IStorageConfigSection
  {
    /// <summary>
    /// Name of ConnectionDefinitions
    /// </summary>
    public static readonly string SECTION_NAME = "ConnectionDefinitions";
    /// <summary>
    /// Key for DefinitionsCollection
    /// </summary>
    public const string KEY_DEFINITIONS = "Definitions";
    /// <summary>
    /// Key for single element in Collection
    /// </summary>
    public const string KEY_SINGLE_DEFINITION = "Connection";
    /// <summary>
    /// DefinitionsCollection
    /// </summary>
    [ConfigurationProperty(KEY_DEFINITIONS)]
    [ConfigurationCollection(typeof(ConnectionDefinitionElement), AddItemName = KEY_SINGLE_DEFINITION)]
    public ConnectionDefinitionCollection Definitions
    {
      get { return base[KEY_DEFINITIONS] as ConnectionDefinitionCollection; }
      set { base[KEY_DEFINITIONS] = value; }
    }
  }
}
