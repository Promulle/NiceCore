﻿using System.Collections.Generic;
using System.Configuration;

namespace NiceCore.Storage.NH.Config.Xml
{
  /// <summary>
  /// Collection of ConnectionDefinitions
  /// </summary>
  public class ConnectionDefinitionCollection : ConfigurationElementCollection
  {
    /// <summary>
    /// returns a new Element
    /// </summary>
    /// <returns></returns>
    protected override ConfigurationElement CreateNewElement()
    {
      return new ConnectionDefinitionElement();
    }
    /// <summary>
    /// returns key of element
    /// </summary>
    /// <param name="element"></param>
    /// <returns></returns>
    protected override object GetElementKey(ConfigurationElement element)
    {
      return (element as ConnectionDefinitionElement).Name;
    }
    /// <summary>
    /// Returns all definitions
    /// </summary>
    /// <returns></returns>
    public List<ConnectionDefinitionElement> GetAll()
    {
      var res = new List<ConnectionDefinitionElement>();
      foreach(var elementKey in BaseGetAllKeys())
      {
        res.Add(BaseGet(elementKey) as ConnectionDefinitionElement);
      }
      return res;
    }
  }
}
