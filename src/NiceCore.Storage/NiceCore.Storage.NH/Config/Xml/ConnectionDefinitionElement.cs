﻿using System.Configuration;

namespace NiceCore.Storage.NH.Config.Xml
{
  /// <summary>
  /// Configuration element that defines Location of Config File etc
  /// </summary>
  public class ConnectionDefinitionElement : ConfigurationElement
  {
    /// <summary>
    /// Key for name of Connection
    /// </summary>
    public const string KEY_NAME = "Name";
    /// <summary>
    /// Key for location of ConfigFile
    /// </summary>
    public const string KEY_FILE = "ConfigFile";
    /// <summary>
    /// Key for the value whether to serialize this connection or not
    /// </summary>
    public const string KEY_SERIAL = "UseSerialization";
    /// <summary>
    /// Key for the value whether to allow this connection to update its schema
    /// </summary>
    public const string KEY_SCHEMA_UPDATE = "SchemaUpdate";
    /// <summary>
    /// Name of Connection
    /// </summary>
    [ConfigurationProperty(KEY_NAME, IsRequired = true)]
    public string Name
    {
      get { return base[KEY_NAME] as string; }
      set { base[KEY_NAME] = value; }
    }
    /// <summary>
    /// Location of ConfigFile
    /// </summary>
    [ConfigurationProperty(KEY_FILE, IsRequired = true)]
    public string ConfigFile
    {
      get { return base[KEY_FILE] as string; }
      set { base[KEY_FILE] = value; }
    }
    /// <summary>
    /// Whether to use serialization or not
    /// </summary>
    [ConfigurationProperty(KEY_SERIAL)]
    public bool UseSerialization
    {
      get { return (bool)base[KEY_SERIAL]; }
      set { base[KEY_SERIAL] = value; }
    }
    /// <summary>
    /// Whether the defined connection is allowed to update it's schema
    /// </summary>
    [ConfigurationProperty(KEY_SCHEMA_UPDATE, DefaultValue = true)]
    public bool SchemaUpdate
    {
      get { return (bool)base[KEY_SCHEMA_UPDATE]; }
      set { base[KEY_SCHEMA_UPDATE] = value; }
    }
  }
}
