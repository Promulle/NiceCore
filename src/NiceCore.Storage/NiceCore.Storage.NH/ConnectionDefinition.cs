﻿using NiceCore.Storage.NH.Config.Xml;

namespace NiceCore.Storage.NH
{
  /// <summary>
  /// Class representing the definition of a single connection
  /// </summary>
  public class ConnectionDefinition : IConnectionDefinition
  {
    /// <summary>
    /// Name of Connection
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Path of ConfigFile
    /// </summary>
    public string ConfigFile { get; set; }

    /// <summary>
    /// Whether this connection can do a schema update
    /// </summary>
    public bool SchemaUpdate { get; set; }

    /// <summary>
    /// Whether this Connection should be serialized
    /// </summary>
    public bool UseSerialization { get; set; }

    /// <summary>
    /// Whether the connection should be audited or not, overrides the general setting
    /// </summary>
    public bool AllowAuditing { get; set; }
    
    /// <summary>
    /// Encryption provider
    /// </summary>
    public string EncryptionProvider { get; set; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public ConnectionDefinition()
    {
    }

    /// <summary>
    /// Constructor to fill 
    /// </summary>
    /// <param name="i_ConfigDefinition"></param>
    public ConnectionDefinition(ConnectionDefinitionElement i_ConfigDefinition)
    {
      Name = i_ConfigDefinition.Name;
      ConfigFile = i_ConfigDefinition.ConfigFile;
      UseSerialization = i_ConfigDefinition.UseSerialization;
      SchemaUpdate = i_ConfigDefinition.SchemaUpdate;
    }
  }
}
