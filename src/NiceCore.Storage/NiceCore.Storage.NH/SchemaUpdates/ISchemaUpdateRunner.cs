﻿using NHibernate.Cfg;
using NiceCore.Storage.DomainBuilder;
using System;
using System.Collections.Generic;

namespace NiceCore.Storage.NH.SchemaUpdates
{
  /// <summary>
  /// Runner for schema update that checks whether i_DomainBuilders allow a schema update
  /// </summary>
  public interface ISchemaUpdateRunner
  {
    /// <summary>
    /// Run schema update
    /// </summary>
    /// <param name="i_Config"></param>
    /// <param name="i_DomainBuilders"></param>
    /// <param name="i_SchemaUpdateCallBack"></param>
    /// <returns>true if scham update has been tried to run else false</returns>
    bool DoSchemaUpdate(Configuration i_Config, IEnumerable<IDomainBuilder> i_DomainBuilders, Action<string> i_SchemaUpdateCallBack);
  }
}