﻿using NHibernate.Tool.hbm2ddl;
using NiceCore.Logging;
using NiceCore.ServiceLocation;
using NiceCore.Storage.DomainBuilder;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using NiceCore.Logging.Extensions;

namespace NiceCore.Storage.NH.SchemaUpdates
{
  /// <summary>
  /// Runner for SchemaUpdate
  /// </summary>
  [Register(InterfaceType = typeof(ISchemaUpdateRunner))]
  public class SchemaUpdateRunner : ISchemaUpdateRunner
  {
    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<SchemaUpdateRunner> Logger { get; set; }

    /// <summary>
    /// Whether updating the schema is allowed
    /// </summary>
    /// <param name="i_DomainBuilders"></param>
    /// <returns></returns>
    private bool AllowSchemaUpdate(IEnumerable<IDomainBuilder> i_DomainBuilders)
    {
      var enabledByDomainBuilders = i_DomainBuilders.All(r => r.AllowSchemaUpdate);
      Logger.Information($"Schemaupdate is enabled by domain builders: {enabledByDomainBuilders}");
      return enabledByDomainBuilders;
    }

    /// <summary>
    /// Export schema
    /// </summary>
    /// <param name="i_Config"></param>
    /// <param name="i_DomainBuilders"></param>
    /// <param name="i_SchemaUpdateCallBack"></param>
    public bool DoSchemaUpdate(NHibernate.Cfg.Configuration i_Config, IEnumerable<IDomainBuilder> i_DomainBuilders, Action<string> i_SchemaUpdateCallBack)
    {
      if (AllowSchemaUpdate(i_DomainBuilders))
      {
        Logger.Information("Running SchemaUpdate.");
        var update = new SchemaUpdate(i_Config);
        if (i_SchemaUpdateCallBack != null)
        {
          update.Execute(i_SchemaUpdateCallBack, true);
        }
        else
        {
          update.Execute(false, true);
        }
        return true;
      }
      Logger.Information("Schemaupdate is disabled in one way or another.");
      return false;
    }
  }
}
