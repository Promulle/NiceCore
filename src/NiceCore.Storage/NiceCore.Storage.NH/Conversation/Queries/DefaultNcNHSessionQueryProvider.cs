using System.Linq;
using NHibernate.Engine;
using NHibernate.Linq;
using NiceCore.ServiceLocation;
using NiceCore.Storage.Queries;

namespace NiceCore.Storage.NH.Conversation.Queries
{
  /// <summary>
  /// Default Nc NH Session Query Provider
  /// </summary>
  [Register(InterfaceType = typeof(INHSessionQueryProvider))]
  public class DefaultNcNHSessionQueryProvider : INHSessionQueryProvider
  {
    /// <summary>
    /// Create New Query
    /// </summary>
    /// <param name="i_Session"></param>
    /// <param name="i_Options"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public IQueryable<T> CreateNewQuery<T>(ISessionImplementor i_Session, INcQueryOptions i_Options)
    {
      var provider = new NcNHQueryProvider<T>(i_Session, i_Options);
      var qry = new NhQueryable<T>(i_Session);
      return provider.CreateQuery<T>(qry.Expression);
    }
  }
}