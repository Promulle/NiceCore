using System.Linq;
using NHibernate.Engine;
using NiceCore.Storage.Queries;

namespace NiceCore.Storage.NH.Conversation.Queries
{
  /// <summary>
  /// NH Session Query Provider
  /// </summary>
  public interface INHSessionQueryProvider
  {
    /// <summary>
    /// Create New Query
    /// </summary>
    /// <param name="i_Session"></param>
    /// <param name="i_Options"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    IQueryable<T> CreateNewQuery<T>(ISessionImplementor i_Session, INcQueryOptions i_Options);
  }
}