using NHibernate.Engine;
using NHibernate.Linq;
using NiceCore.Storage.Queries;

namespace NiceCore.Storage.NH.Conversation.Queries
{
  /// <summary>
  /// Nc NH Query Provider
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public class NcNHQueryProvider<T> : DefaultQueryProvider, INcQueryProvider<T>
  {
    /// <summary>
    /// Options
    /// </summary>
    public INcQueryOptions Options { get; }
    
    /// <summary>
    /// ctor
    /// </summary>
    /// <param name="session"></param>
    /// <param name="i_Options"></param>
    public NcNHQueryProvider(ISessionImplementor session, INcQueryOptions i_Options) : base(session)
    {
      Options = i_Options;
    }

    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="session"></param>
    /// <param name="collection"></param>
    /// <param name="i_Options"></param>
    public NcNHQueryProvider(ISessionImplementor session, object collection, INcQueryOptions i_Options) : base(session, collection)
    {
      Options = i_Options;
    }

    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="session"></param>
    /// <param name="collection"></param>
    /// <param name="options"></param>
    /// <param name="i_Options"></param>
    protected NcNHQueryProvider(ISessionImplementor session, object collection, NhQueryableOptions options, INcQueryOptions i_Options) : base(session, collection, options)
    {
      Options = i_Options;
    }
  }
}