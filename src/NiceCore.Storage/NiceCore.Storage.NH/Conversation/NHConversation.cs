﻿using NHibernate;
using NHibernate.Engine;
using NiceCore.Entities;
using NiceCore.Extensions;
using NiceCore.ServiceLocation;
using NiceCore.Services.Validation;
using NiceCore.Services.Validation.Exceptions;
using NiceCore.Services.Validation.Model;
using NiceCore.Storage.Cascading;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Conversations.Queries;
using NiceCore.Storage.Diffing;
using NiceCore.Storage.LazyLoading;
using NiceCore.Storage.Messages;
using NiceCore.Storage.NH.Conversation.Caches;
using NiceCore.Storage.NH.Conversation.Utils;
using NiceCore.Storage.NH.Lazy;
using Polly;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Diagnostics;
using NiceCore.Infrastructure;
using NiceCore.Logging.Extensions;
using NiceCore.Storage.Auditing;
using NiceCore.Storage.Conversations.EventHooks;
using NiceCore.Storage.Conversations.Flags.Impls;
using NiceCore.Storage.Events;
using NiceCore.Storage.Events.LostEvents;
using NiceCore.Storage.NH.Auditing;
using NiceCore.Storage.NH.Conversation.Exceptions;
using NiceCore.Storage.NH.Conversation.Initialization;
using NiceCore.Storage.NH.Conversation.Queries;
using NiceCore.Storage.NH.Conversation.Untyped;
using NiceCore.Storage.Queries;

namespace NiceCore.Storage.NH.Conversation
{
  /// <summary>
  /// Conversation implementation for NHibernate
  /// </summary>
  [Register(InterfaceType = typeof(INHConversation))]
  public class NHConversation : BaseNHConversation, INHConversation
  {
    private ConversationRetryOperations m_Retry;
    private IMessageAwaiter             m_MessageAwaiter;

    /// <summary>
    /// Session of this Conversation
    /// </summary>
    protected ISession m_ConvSession;

    /// <summary>
    /// Currently used Transaction
    /// </summary>
    protected ITransaction m_CurrentTransaction;

    /// <summary>
    /// Interceptor providing events
    /// </summary>
    protected NiceCoreNHInterceptor m_Interceptor;

    /// <summary>
    /// Name of Conversation
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Gets the connection string for this conversation
    /// </summary>
    public string ConnectionString => m_ConvSession.Connection?.ConnectionString;

    /// <summary>
    /// Entity Modifiers used for modifying entities before the operation
    /// </summary>
    public IDictionary<EntityModifyOperations, List<IEntityOperationModifier>> EntityModifiers { get; set; }

    /// <summary>
    /// Loaded entity modifiers
    /// </summary>
    public List<ILoadedEntityModifier> LoadedEntityModifiers { get; set; }

    /// <summary>
    /// Before Add to Conversation Hooks
    /// </summary>
    public IList<IBeforeAddToConversationHook> BeforeAddToConversationHooksInjected { get; set; }

    /// <summary>
    /// Before Add to Conversation Hooks
    /// </summary>
    public List<IBeforeAddToConversationHook> BeforeAddToConversationHooks { get; set; }

    /// <summary>
    /// Storage message sender
    /// </summary>
    public IStorageMessageSender StorageMessageSender { get; set; }

    /// <summary>
    /// Service used for validation
    /// </summary>
    public IValidationService ValidationService { get; set; }

    /// <summary>
    /// Service Container
    /// </summary>
    public IServiceContainer Container { get; set; }

    /// <summary>
    /// Service for supporting lazy proxies
    /// </summary>
    public ILazyProxySupport LazyProxySupport { get; set; }

    /// <summary>
    /// Lost Storage Event Collector
    /// </summary>
    public ILostStorageEventCollector LostStorageEventCollector { get; set; }

    /// <summary>
    /// Storage Event Model Service
    /// </summary>
    public IStorageEventModelService StorageEventModelService { get; set; }

    /// <summary>
    /// Transaction Handler
    /// </summary>
    public INHTransactionHandler TransactionHandler { get; set; }

    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<NHConversation> ActualLogger { get; set; } = DiagnosticsCollector.Instance.GetLogger<NHConversation>();

    /// <summary>
    /// Active Conversation Store
    /// </summary>
    public IActiveConversationStore ActiveConversationStore { get; set; }

    /// <summary>
    /// Logger override
    /// </summary>
    public override ILogger Logger
    {
      get { return ActualLogger; }
    }

    /// <summary>
    /// Strict Mode
    /// </summary>
    public bool StrictMode { get; private set; }

    /// <summary>
    /// Whether this conversation should use a transaction or not
    /// </summary>
    public bool ExplicitTransactionHandling { get; private set; }

    /// <summary>
    /// Audit support providing functionality for auditing changes
    /// </summary>
    public INcAuditingSupport Audit { get; private set; }

    /// <summary>
    /// Return the underlying session. To use this is not advised as it circumvents all
    /// mechanics build into the conversation
    /// </summary>
    /// <returns></returns>
    public ISession GetSession()
    {
      return m_ConvSession;
    }

    /// <summary>
    /// Calling this method prevents this conversation from sending any messages
    /// </summary>
    public void DisableMessages()
    {
      StorageMessageSender?.ClearMessages();
      StorageMessageSender = null;
    }

    /// <summary>
    /// Initializes this Connection
    /// </summary>
    public async ValueTask Initialize(NHConversationInit i_Init, CancellationToken i_Token)
    {
      var enableAuditing = i_Init.ConnectionInfo.EnableAuditing;
      Logger.Information($"Initializing conversation for name {Name}, auditing enabled: {enableAuditing}");
      Context                     = i_Init.Context;
      ConnectionInfo              = i_Init.ConnectionInfo;
      StrictMode                  = i_Init.StrictMode;
      ExplicitTransactionHandling = i_Init.TransactionHandlingMode == TransactionHandlingMode.Explicit;
      EntityModifiers             = i_Init.EntityModifiers;
      LoadedEntityModifiers       = i_Init.LoadedEntityModifiers;
      m_Interceptor               = new(EntityModifiers, LoadedEntityModifiers, Flags, i_Init.Context, i_Init.DifferService, LostStorageEventCollector, StorageEventModelService.GetTransientStorageEventModel(i_Init.ConnectionInfo.ConnectionName), Logger);
      m_ConvSession               = await i_Init.SessionProvider.CreateSession(m_Interceptor, i_Init.ConnectionInfo, i_Token).ConfigureAwait(false);
      m_ConvSession.FlushMode     = FlushMode.Manual;
      Untyped                     = new UntypedNHConversationSupport(m_ConvSession);
      SetQueryLimiterSupport(i_Init.QueryLimiters);
      if (ExplicitTransactionHandling)
      {
        m_CurrentTransaction = await TransactionHandler.BeginTransactionAsync(m_ConvSession).ConfigureAwait(false);
      }

      if (enableAuditing)
      {
        Logger.Information("Auditing is enabled, retrieving auditreader for session.");
        Audit = new NcNHEnversAuditingSupport(m_ConvSession, Name, Logger);
      }

      if (i_Init.RetryStrategy != null)
      {
        m_Retry = new(m_ConvSession, i_Init.RetryStrategy);
      }

      //if the creation parameters supplied a Storage Message Sender, the conversation itself will not handle the clearing/sending of messages
      //so we create a not sending wrapper for this conversation
      if (i_Init.StorageMessageSender != null)
      {
        //clear old, just to be safe
        StorageMessageSender?.ClearMessages();
        StorageMessageSender = new NotSendingStorageMessageSenderWrapper(i_Init.StorageMessageSender);
      }

      if (StorageMessageSender == null)
      {
        Logger.Warning($"StorageMessage Sender not set in Conversation. This conversation for connection {Name} will not send any storage messages.");
      }
      else
      {
        StorageMessageSender.SetContext(i_Init.Context, i_Init.MessageSendingParameters, i_Init.MessageProviders);
        WireInterceptorEvents(m_Interceptor);
      }

      SortBeforeAddToConversationHooks();
    }

    /// <summary>
    /// Gets the identifier of the conversation
    /// </summary>
    /// <returns></returns>
    public Guid? GetIdentifier()
    {
      if (m_ConvSession == null)
      {
        throw new InvalidOperationException($"{nameof(GetIdentifier)} called before conversation was initialized.");
      }

      return m_ConvSession.GetSessionImplementation()?.SessionId;
    }

    private void SortBeforeAddToConversationHooks()
    {
      if (BeforeAddToConversationHooksInjected != null && BeforeAddToConversationHooksInjected.Count > 0)
      {
        BeforeAddToConversationHooks = BeforeAddToConversationHooksInjected.OrderBy(r => r.IsDestructive).ToList();
      }
    }

    private void WireInterceptorEvents(NiceCoreNHInterceptor i_Interceptor)
    {
      i_Interceptor.InterceptorEvent += OnInterceptorEvent;
    }

    private void OnInterceptorEvent(object i_Sender, NHInterceptorEvent i_Args)
    {
      var queueMessageFunc = ConversationCaches.QueueMessageFunctionCache.Get(i_Args.Entity.GetType());
      if (queueMessageFunc != null)
      {
        queueMessageFunc.Invoke(this, i_Args.Entity, i_Args.Operation, i_Args.Diff);
      }
    }

    /// <summary>
    /// Creates a lazy proxy
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <param name="i_ID"></param>
    /// <returns></returns>
    public T CreateLazyProxy<T, TId>(TId i_ID) where T : class, IDomainEntity<TId>
    {
      if (LazyProxySupport != null)
      {
        var lazyParam = new NHLazyCreationParameters<TId>()
        {
          ID      = i_ID,
          Session = m_ConvSession as ISessionImplementor
        };
        return LazyProxySupport.CreateLazyProxy<T, TId>(lazyParam);
      }

      throw new NotSupportedException($"Conversation for connection {Name} is not setup to support lazy proxy creation. Register a service for {typeof(ILazyProxySupport)} to fix this.");
    }

    /// <summary>
    /// Unproxy
    /// </summary>
    /// <param name="i_PossibleProxy"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    /// <exception cref="NotSupportedException"></exception>
    public T Unproxy<T, TId>(object i_PossibleProxy) where T : IDomainEntity<TId>
    {
      if (LazyProxySupport != null)
      {
        var param = new NHUnproxyParameters()
        {
          Session       = m_ConvSession as ISessionImplementor,
          PossibleProxy = i_PossibleProxy
        };
        return LazyProxySupport.Unproxy<T, TId>(param);
      }

      throw new NotSupportedException($"Conversation for connection {Name} is not setup to support lazy proxy unproxying. Register a service for {typeof(ILazyProxySupport)} to fix this.");
    }

    /// <summary>
    /// Unproxy
    /// </summary>
    /// <param name="i_PossibleProxy"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    public T UnproxyWithInitialize<T, TId>(object i_PossibleProxy) where T : IDomainEntity<TId>
    {
      if (LazyProxySupport != null)
      {
        var param = new NHUnproxyParameters()
        {
          Session       = m_ConvSession as ISessionImplementor,
          PossibleProxy = i_PossibleProxy
        };
        return LazyProxySupport.UnproxyWithInitialize<T, TId>(param);
      }

      throw new NotSupportedException($"Conversation for connection {Name} is not setup to support lazy proxy unproxying. Register a service for {typeof(ILazyProxySupport)} to fix this.");
    }

    /// <summary>
    /// Checks if i_PossibleProxy is a proxy
    /// </summary>
    /// <param name="i_PossibleProxy"></param>
    /// <returns></returns>
    public bool IsProxy(object i_PossibleProxy)
    {
      if (LazyProxySupport != null)
      {
        return LazyProxySupport.IsProxy(i_PossibleProxy);
      }

      throw new NotSupportedException($"Conversation for connection {Name} is not setup to support lazy proxy identification. Register a service for {typeof(ILazyProxySupport)} to fix this.");
    }

    /// <summary>
    /// Enables awaiting messages from this conversation
    /// </summary>
    /// <param name="i_Awaiter"></param>
    public void EnableMessageAwaiting(IMessageAwaiter i_Awaiter)
    {
      m_MessageAwaiter = i_Awaiter;
    }

    /// <summary>
    /// Disposes this Conversation
    /// </summary>
    protected override void Dispose(bool i_Disposing)
    {
      m_Interceptor.InterceptorEvent -= OnInterceptorEvent;
      m_Interceptor.Dispose();
      RemoveConversationFromActive();
      StorageMessageSender?.ClearMessages();
      EntityModifiers?.SelectMany(r => r.Value).ForEachItem(r => Container.Release(r));
      if (m_ConvSession.IsOpen)
      {
        m_ConvSession.Clear();
      }

      // if (m_CurrentTransaction?.IsActive == true)
      // {
      //   m_CurrentTransaction.Rollback();
      // }

      m_CurrentTransaction?.Dispose();
      m_ConvSession.Dispose();
    }

    #region IConversation

    /// <summary>
    /// Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Config"></param>
    /// <param name="i_Node"></param>
    public void Update<T, Tid>(T i_Instance, ISyncPolicy i_Policy, IConversationOperationConfiguration i_Config, SubValidationNode i_Node) where T : IDomainEntity<Tid>
    {
      CheckValidateInstance(i_Config, i_Instance, StorageValidationOperations.SAVE_OR_UPDATE, i_Node).GetAwaiter().GetResult();
      if (m_Retry != null)
      {
        m_Retry.Do(() => m_ConvSession.Update(i_Instance), i_Policy, nameof(Update), Logger);
      }
      else
      {
        if (SessionReadyChecker.IsSessionReady(m_ConvSession, nameof(Update), Logger))
        {
          m_ConvSession.Update(i_Instance);
        }
      }
    }

    /// <summary>
    /// Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Config"></param>
    /// <param name="i_Node"></param>
    public async Task UpdateAsync<T, Tid>(T i_Instance, IAsyncPolicy i_Policy, IConversationOperationConfiguration i_Config, SubValidationNode i_Node) where T : IDomainEntity<Tid>
    {
      await CheckValidateInstance(i_Config, i_Instance, StorageValidationOperations.SAVE_OR_UPDATE, i_Node).ConfigureAwait(false);
      if (m_Retry != null)
      {
        await m_Retry.DoAsync(async () => await m_ConvSession.UpdateAsync(i_Instance).ConfigureAwait(false), i_Policy, nameof(UpdateAsync), Logger).ConfigureAwait(false);
      }
      else
      {
        if (SessionReadyChecker.IsSessionReady(m_ConvSession, nameof(UpdateAsync), Logger))
        {
          await m_ConvSession.UpdateAsync(i_Instance).ConfigureAwait(false);
        }
      }
    }

    /// <summary>
    /// Saves instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Config"></param>
    /// <param name="i_Node"></param>
    public void Save<T, Tid>(T i_Instance, ISyncPolicy i_Policy, IConversationOperationConfiguration i_Config, SubValidationNode i_Node) where T : IDomainEntity<Tid>
    {
      CheckValidateInstance(i_Config, i_Instance, StorageValidationOperations.SAVE_OR_UPDATE, i_Node).GetAwaiter().GetResult();
      if (m_Retry != null)
      {
        m_Retry.Do(() => m_ConvSession.Save(i_Instance), i_Policy, nameof(Save), Logger);
      }
      else
      {
        if (SessionReadyChecker.IsSessionReady(m_ConvSession, nameof(Save), Logger))
        {
          m_ConvSession.Save(i_Instance);
        }
      }
    }

    /// <summary>
    /// Saves instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Config"></param>
    /// <param name="i_Node"></param>
    public async Task SaveAsync<T, Tid>(T i_Instance, IAsyncPolicy i_Policy, IConversationOperationConfiguration i_Config, SubValidationNode i_Node) where T : IDomainEntity<Tid>
    {
      await CheckValidateInstance(i_Config, i_Instance, StorageValidationOperations.SAVE_OR_UPDATE, i_Node).ConfigureAwait(false);
      if (m_Retry != null)
      {
        await m_Retry.DoAsync(async () => await m_ConvSession.SaveAsync(i_Instance).ConfigureAwait(false), i_Policy, nameof(SaveAsync), Logger).ConfigureAwait(false);
      }
      else
      {
        if (SessionReadyChecker.IsSessionReady(m_ConvSession, nameof(SaveAsync), Logger))
        {
          await m_ConvSession.SaveAsync(i_Instance).ConfigureAwait(false);
        }
      }
    }

    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Config"></param>
    /// <param name="i_Node"></param>
    public void SaveOrUpdate<T, Tid>(T i_Instance, ISyncPolicy i_Policy, IConversationOperationConfiguration i_Config, SubValidationNode i_Node) where T : IDomainEntity<Tid>
    {
      CheckValidateInstance(i_Config, i_Instance, StorageValidationOperations.SAVE_OR_UPDATE, i_Node).GetAwaiter().GetResult();
      if (m_Retry != null)
      {
        m_Retry.Do(() => m_ConvSession.SaveOrUpdate(i_Instance), i_Policy, nameof(SaveOrUpdate), Logger);
      }
      else
      {
        if (SessionReadyChecker.IsSessionReady(m_ConvSession, nameof(SaveOrUpdate), Logger))
        {
          m_ConvSession.SaveOrUpdate(i_Instance);
        }
      }
    }

    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Config"></param>
    /// <param name="i_Node"></param>
    public async Task SaveOrUpdateAsync<T, Tid>(T i_Instance, IAsyncPolicy i_Policy, IConversationOperationConfiguration i_Config, SubValidationNode i_Node) where T : IDomainEntity<Tid>
    {
      var canContinue = await ConversationHooks.ExecuteAllBeforeAdd(i_Instance, BeforeAddToConversationHooks, this).ConfigureAwait(false);
      if (!canContinue)
      {
        return;
      }

      await CheckValidateInstance(i_Config, i_Instance, StorageValidationOperations.SAVE_OR_UPDATE, i_Node).ConfigureAwait(false);
      if (m_Retry != null)
      {
        await m_Retry.DoAsync(async () => await m_ConvSession.SaveOrUpdateAsync(i_Instance).ConfigureAwait(false), i_Policy, nameof(SaveOrUpdateAsync), Logger).ConfigureAwait(false);
      }
      else
      {
        if (SessionReadyChecker.IsSessionReady(m_ConvSession, nameof(SaveOrUpdateAsync), Logger))
        {
          await m_ConvSession.SaveOrUpdateAsync(i_Instance).ConfigureAwait(false);
        }
      }
    }

    /// <summary>
    /// Save Or Update Async but without policy support. Hopefully higher performance than normal
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_Config"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    public async ValueTask SaveOrUpdateAsyncRaw<T, Tid>(T i_Instance, IConversationOperationConfiguration i_Config)
      where T : IDomainEntity<Tid>
    {
      var canContinue = await ConversationHooks.ExecuteAllBeforeAdd(i_Instance, BeforeAddToConversationHooks, this).ConfigureAwait(false);
      if (!canContinue)
      {
        return;
      }

      await CheckValidateInstance(i_Config, i_Instance, StorageValidationOperations.SAVE_OR_UPDATE, null).ConfigureAwait(false);
      await m_ConvSession.SaveOrUpdateAsync(i_Instance).ConfigureAwait(false);
    }

    /// <summary>
    /// Deletes an Instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Config"></param>
    /// <param name="i_Node"></param>
    public void Delete<T, Tid>(T i_Instance, ISyncPolicy i_Policy, IConversationOperationConfiguration i_Config, SubValidationNode i_Node) where T : IDomainEntity<Tid>
    {
      CheckValidateInstance(i_Config, i_Instance, StorageValidationOperations.DELETE, i_Node).GetAwaiter().GetResult();
      if (m_Retry != null)
      {
        m_Retry.Do(() => m_ConvSession.Delete(i_Instance), i_Policy, nameof(Delete), Logger);
      }
      else
      {
        if (SessionReadyChecker.IsSessionReady(m_ConvSession, nameof(Delete), Logger))
        {
          m_ConvSession.Delete(i_Instance);
        }
      }
    }

    /// <summary>
    /// Deletes an Instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Config"></param>
    /// <param name="i_Node"></param>
    public async Task DeleteAsync<T, Tid>(T i_Instance, IAsyncPolicy i_Policy, IConversationOperationConfiguration i_Config, SubValidationNode i_Node) where T : IDomainEntity<Tid>
    {
      await CheckValidateInstance(i_Config, i_Instance, StorageValidationOperations.DELETE, i_Node).ConfigureAwait(false);
      if (m_Retry != null)
      {
        await m_Retry.DoAsync(async () => await m_ConvSession.DeleteAsync(i_Instance).ConfigureAwait(false), i_Policy, nameof(DeleteAsync), Logger).ConfigureAwait(false);
      }
      else
      {
        if (SessionReadyChecker.IsSessionReady(m_ConvSession, nameof(DeleteAsync), Logger))
        {
          await m_ConvSession.DeleteAsync(i_Instance).ConfigureAwait(false);
        }
      }
    }

    #region Query

    /// <summary>
    /// Get Session Implementor
    /// </summary>
    /// <returns></returns>
    protected override ISessionImplementor GetSessionImplementor()
    {
      return m_ConvSession.GetSessionImplementation();
    }

    /// <summary>
    /// Create a dependent query
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    public async Task<IPreSelectDependentQuery<T, TId>> DependentQuery<T, TId>(IEnumerable<IQueryPreSelector> i_PreSelectors, IConversationContext i_Context, Func<IQueryable<T>, ValueTask<IQueryable<T>>> i_QueryModifier) where T : IDomainEntity<TId>
    {
      var qry = new PreSelectDependentQuery<T, TId>();
      await qry.ApplyPreSelection(this, i_Context, i_PreSelectors, i_QueryModifier).ConfigureAwait(false);
      return qry;
    }

    #endregion

    /// <summary>
    /// Applies everything this conversation has done
    /// </summary>
    /// <returns></returns>
    public virtual async ValueTask ApplyAsync()
    {
      try
      {
        var canApply = SessionReadyChecker.IsSessionReady(m_ConvSession, nameof(ApplyAsync), Logger);
        if (!canApply)
        {
          throw new InvalidOperationException("Could not apply conversation. Session is already closed.");
        }

        await m_ConvSession.FlushAsync().ConfigureAwait(false);
        if (ExplicitTransactionHandling)
        {
          await m_CurrentTransaction.CommitAsync().ConfigureAwait(false);
        }

        m_ConvSession.Clear();
        StorageMessageSender?.SendMessages(m_MessageAwaiter).HandleErrorsInContinueWith(Logger);
        RemoveConversationFromActive();
      }
      catch (Exception ex)
      {
        Logger.Error(ex, $"Error applying conversation for connection asynchronously: {Name}.");

        if (StrictMode)
        {
          throw;
        }
      }
    }

    private void RemoveConversationFromActive()
    {
      var id = GetIdentifier();
      if (id != null)
      {
        ActiveConversationStore.RemoveConversation(id.Value);
      }
    }

    private void AddConversationToActive()
    {
      var id = GetIdentifier();
      if (id != null)
      {
        ActiveConversationStore.AddActiveConversation(this);
      }
    }

    /// <summary>
    /// Cancels conversation
    /// </summary>
    public ValueTask CancelAsync()
    {
      return CancelAsyncInternal();
    }

    private async ValueTask CancelAsyncInternal()
    {
      try
      {
        if (ExplicitTransactionHandling && m_CurrentTransaction != null && m_CurrentTransaction.IsActive && !m_CurrentTransaction.WasRolledBack && !m_CurrentTransaction.WasCommitted)
        {
          await m_CurrentTransaction.RollbackAsync().ConfigureAwait(false);
        }

        StorageMessageSender?.ClearMessages();
        RemoveConversationFromActive();
      }
      catch (Exception ex)
      {
        Logger.Error(ex, $"Error rolling back conversation for connection asynchronously: {Name}. Rolling back changes...");
        if (StrictMode)
        {
          throw;
        }
      }
    }

    /// <summary>
    /// Attaches i_Entity to the conversation
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    public async Task Attach<T, TId>(T i_Entity) where T : IDomainEntity<TId>
    {
      await m_ConvSession.LockAsync(i_Entity, LockMode.None).ConfigureAwait(false);
    }

    /// <summary>
    /// Deletes every entry for T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Config"></param>
    /// <param name="i_Node"></param>
    public void Delete<T, Tid>(IConversationOperationConfiguration i_Config, SubValidationNode i_Node) where T : IDomainEntity<Tid>
    {
      if (SessionReadyChecker.IsSessionReady(m_ConvSession, nameof(Delete), Logger))
      {
        foreach (var entry in this.Query<T, Tid>().ToList())
        {
          Delete<T, Tid>(entry, null, i_Config, i_Node);
        }
      }
    }

    /// <summary>
    /// Deletes every entry for T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Config"></param>
    /// <param name="i_Node"></param>
    public async Task DeleteAsync<T, Tid>(IConversationOperationConfiguration i_Config, SubValidationNode i_Node) where T : IDomainEntity<Tid>
    {
      if (SessionReadyChecker.IsSessionReady(m_ConvSession, nameof(DeleteAsync), Logger))
      {
        foreach (var entry in this.Query<T, Tid>().ToList())
        {
          await DeleteAsync<T, Tid>(entry, null, i_Config, i_Node).ConfigureAwait(false);
        }
      }
    }

    /// <summary>
    /// Returns a command, useable for the connection of this conversation
    /// </summary>
    /// <param name="i_CommandText">Optional pre-fill of command text</param>
    /// <returns></returns>
    public DbCommand GetDbCommand(string i_CommandText = null)
    {
      if (m_Retry != null)
      {
        return m_Retry.Do(() => GetDbCommandOperation(i_CommandText), nameof(GetDbCommand), Logger);
      }

      if (SessionReadyChecker.IsSessionReady(m_ConvSession, nameof(GetDbCommand), Logger))
      {
        return GetDbCommandOperation(i_CommandText);
      }

      throw new SessionNotReadyException();
    }

    /// <summary>
    /// Returns a command, useable for the connection of this conversation
    /// </summary>
    /// <param name="i_Policy"></param>
    /// <param name="i_CommandText">Optional pre-fill of command text</param>
    /// <returns></returns>
    public DbCommand GetDbCommand(ISyncPolicy i_Policy, string i_CommandText = null)
    {
      if (m_Retry != null)
      {
        return m_Retry.Do(() => GetDbCommandOperation(i_CommandText), i_Policy, nameof(GetDbCommand), Logger);
      }

      if (SessionReadyChecker.IsSessionReady(m_ConvSession, nameof(GetDbCommand), Logger))
      {
        return GetDbCommandOperation(i_CommandText);
      }

      throw new SessionNotReadyException();
    }

    private DbCommand GetDbCommandOperation(string i_CommandText)
    {
      if (m_ConvSession.Connection != null)
      {
        var command = m_ConvSession.Connection.CreateCommand();
        if (m_CurrentTransaction != null && ExplicitTransactionHandling)
        {
          m_CurrentTransaction.Enlist(command);
        }

        Logger.Information("Created new dbCommand");
        if (i_CommandText != null)
        {
          Logger.Information($"Setting command text \"{i_CommandText}\" for command");
          command.CommandText = i_CommandText;
        }

        return command;
      }

      return null;
    }

    /// <summary>
    /// Executes i_SqlStatement, as non-Query.
    /// Returns number of affected rows
    /// </summary>
    /// <param name="i_SqlStatement"></param>
    /// <returns></returns>
    public int ExecSqlNonQuery(string i_SqlStatement)
    {
      if (m_Retry != null)
      {
        return m_Retry.Do(() => ExecuteSqlNonQueryOperation(i_SqlStatement), null, Logger);
      }

      if (SessionReadyChecker.IsSessionReady(m_ConvSession, nameof(ExecSqlNonQuery), Logger))
      {
        return ExecuteSqlNonQueryOperation(i_SqlStatement);
      }

      throw new SessionNotReadyException();
    }

    /// <summary>
    /// Execute an sql query
    /// </summary>
    /// <param name="i_SqlQueryStatement"></param>
    /// <returns></returns>
    public Task<DbDataReader> ExecSqlQueryAsync(string i_SqlQueryStatement)
    {
      if (m_Retry != null)
      {
        return m_Retry.DoAsync(() => ExecSqlQueryAsyncInternal(i_SqlQueryStatement), null, null, Logger);
      }

      if (SessionReadyChecker.IsSessionReady(m_ConvSession, nameof(ExecSqlQueryAsync), Logger))
      {
        return ExecSqlQueryAsyncInternal(i_SqlQueryStatement);
      }

      throw new SessionNotReadyException();
    }

    private async Task<DbDataReader> ExecSqlQueryAsyncInternal(string i_SqlQueryStatement)
    {
      try
      {
        using var command = GetDbCommand(i_SqlQueryStatement);
        return await command.ExecuteReaderAsync().ConfigureAwait(false);
      }
      catch (Exception ex)
      {
        Logger.Error(ex, $"Error executing sql {i_SqlQueryStatement} for connection {Name}. Returning -1 rows affected");
        if (StrictMode)
        {
          throw;
        }
      }

      return null;
    }

    /// <summary>
    /// Executes i_SqlStatement, as non-Query.
    /// Returns number of affected rows
    /// </summary>
    /// <param name="i_SqlStatement"></param>
    /// <param name="i_Policy">Resilience Policy for this operation</param>
    /// <returns></returns>
    public int ExecSqlNonQuery(string i_SqlStatement, ISyncPolicy i_Policy)
    {
      if (m_Retry != null)
      {
        return m_Retry.Do(() => ExecuteSqlNonQueryOperation(i_SqlStatement), i_Policy, nameof(ExecSqlNonQuery), Logger);
      }

      if (SessionReadyChecker.IsSessionReady(m_ConvSession, nameof(ExecSqlNonQuery), Logger))
      {
        return ExecuteSqlNonQueryOperation(i_SqlStatement);
      }

      throw new SessionNotReadyException();
    }

    /// <summary>
    /// Executes i_SqlStatement, as non-Query.
    /// Returns number of affected rows
    /// </summary>
    /// <param name="i_SqlStatement"></param>
    /// <returns></returns>
    public Task<int> ExecSqlNonQueryAsync(string i_SqlStatement)
    {
      if (m_Retry != null)
      {
        return m_Retry.DoAsync(() => ExecuteSqlNonQueryOperationAsync(i_SqlStatement), null, nameof(ExecSqlNonQueryAsync), Logger);
      }

      if (SessionReadyChecker.IsSessionReady(m_ConvSession, nameof(ExecSqlNonQueryAsync), Logger))
      {
        return ExecuteSqlNonQueryOperationAsync(i_SqlStatement);
      }

      throw new SessionNotReadyException();
    }

    /// <summary>
    /// Executes i_SqlStatement, as non-Query.
    /// Returns number of affected rows
    /// </summary>
    /// <param name="i_SqlStatement"></param>
    /// <param name="i_Policy">Resilience Policy for this operation</param>
    /// <returns></returns>
    public Task<int> ExecSqlNonQueryAsync(string i_SqlStatement, IAsyncPolicy i_Policy)
    {
      if (m_Retry != null)
      {
        return m_Retry.DoAsync(() => ExecuteSqlNonQueryOperationAsync(i_SqlStatement), i_Policy, nameof(ExecSqlNonQueryAsync), Logger);
      }

      if (SessionReadyChecker.IsSessionReady(m_ConvSession, nameof(ExecSqlNonQueryAsync), Logger))
      {
        return ExecuteSqlNonQueryOperationAsync(i_SqlStatement);
      }

      throw new SessionNotReadyException();
    }

    private int ExecuteSqlNonQueryOperation(string i_SqlStatement)
    {
      var bResult = -1;
      try
      {
        using (var command = GetDbCommand(i_SqlStatement))
        {
          return command.ExecuteNonQuery();
        }
      }
      catch (Exception ex)
      {
        Logger.Error(ex, $"Error executing sql {i_SqlStatement} for connection {Name}. Returning -1 rows affected");
        bResult = -1;
        if (StrictMode)
        {
          throw;
        }
      }

      return bResult;
    }

#pragma warning disable RCS1229 // I do not want to await the async call here
    private async Task<int> ExecuteSqlNonQueryOperationAsync(string i_SqlStatement)
#pragma warning restore RCS1229 // I do not want to await the async call here
    {
      var bResult = -1;
      try
      {
        using (var command = GetDbCommand(i_SqlStatement))
        {
          return await command.ExecuteNonQueryAsync().ConfigureAwait(false);
        }
      }
      catch (Exception ex)
      {
        Logger.Error(ex, $"Error executing sql {i_SqlStatement} for connection {Name}. Returning -1 rows affected");
        bResult = -1;
        if (StrictMode)
        {
          throw;
        }
      }

      return bResult;
    }

    /// <summary>
    /// Restarts the conversation
    /// </summary>
    public void Restart()
    {
      if (ExplicitTransactionHandling)
      {
        if (m_CurrentTransaction == null || (m_CurrentTransaction.WasRolledBack || m_CurrentTransaction.WasCommitted))
        {
          m_CurrentTransaction?.Dispose();
          m_CurrentTransaction = m_ConvSession.BeginTransaction();
        }
      }

      StorageMessageSender?.ClearMessages();
      AddConversationToActive();
    }

    [UsedByExpressionTree(nameof(QueueMessageFunctionCache))]
    private void QueueMessageForEntityEvent<T, TId>(T i_Entity, ConversationOperations i_Operation, StorageDiffResult i_UpdateDiff) where T : IDomainEntity<TId>
    {
      Logger.Trace($"Queueing Message {i_Operation} for entity of type {typeof(T).Name} with id {i_Entity.ID}");
      if (StorageMessageSender != null)
      {
        Logger.Trace($"Storage Message Sender defined, checking where to put message. DiffResult defined: {i_UpdateDiff != null}");
        switch (i_Operation)
        {
          case ConversationOperations.Delete:
            StorageMessageSender.EntityDeleted<T, TId>(i_Entity, null);
            break;
          case ConversationOperations.Save:
            StorageMessageSender.EntityCreated<T, TId>(i_Entity, null);
            break;
          case ConversationOperations.Update:
            StorageMessageSender.EntityUpdated<T, TId>(i_Entity, new()
            {
              DiffResult = i_UpdateDiff
            });
            break;
        }
      }
      else
      {
        Logger.Trace("No StorageMessage Sender defined, no message will be sent.");
      }
    }

    private async ValueTask CheckValidateInstance(IConversationOperationConfiguration i_Config, object i_Instance, string i_Operation, SubValidationNode i_Node)
    {
      if (ShouldValidate(i_Config))
      {
        Logger.Information($"Validating an instance of type {i_Instance.GetType().Name} before operation of type {i_Operation}");
        var res = await ValidationService.Validate(i_Instance, i_Operation, i_Node, Context?.ToValidationContext(this, Container)).ConfigureAwait(false);
        ValidationExceptionRaiser.ThrowIfNotValid(res, false);
      }
      else
      {
        Logger.Debug($"Validation skipped because {nameof(i_Config.ValidateInstance)} was set to false. Value: {i_Config.ValidateInstance}. Or the property {nameof(ValidationService)} of this conversation was not set. {nameof(ValidationService)} is set: {ValidationService != null}");
      }
    }

    private bool ShouldValidate(IConversationOperationConfiguration i_Config)
    {
      if (ValidationService == null)
      {
        return false;
      }

      if (i_Config == null || i_Config.IsDefaultInstance)
      {
        //search for validation disable flag
        var flagValue = Flags.TryGetFlagValue<bool>(DefaultConversationFlags.FLAG_VALIDATE_INSTANCES);
        if (flagValue.Found)
        {
          return flagValue.Value;
        }

        return true;
      }

      return i_Config.ValidateInstance;
    }

    /// <summary>
    /// Save or update async with policy and relations
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Relations"></param>
    /// <param name="i_Node"></param>
    /// <returns></returns>
    public async Task SaveOrUpdateAsync<T, Tid>(T i_Instance, IAsyncPolicy i_Policy, IEnumerable<ICascadingRelation> i_Relations, SubValidationNode i_Node) where T : IDomainEntity<Tid>
    {
      await SaveOrUpdateAsync<T, Tid>(i_Instance, i_Policy, ConversationOperationConfig.DEFAULT_INSTANCE, i_Node).ConfigureAwait(false);
      await CascadeRelationTranslation.ApplySaveCascadeRelation<T, Tid>(this, i_Instance, i_Policy, i_Relations).ConfigureAwait(false);
    }

    /// <summary>
    /// Delete async with policy and relations
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Relations"></param>
    /// <param name="i_Node"></param>
    /// <returns></returns>
    public async Task DeleteAsync<T, Tid>(T i_Instance, IAsyncPolicy i_Policy, IEnumerable<ICascadingRelation> i_Relations, SubValidationNode i_Node) where T : IDomainEntity<Tid>
    {
      await DeleteAsync<T, Tid>(i_Instance, i_Policy, ConversationOperationConfig.DEFAULT_INSTANCE, i_Node).ConfigureAwait(false);
      await CascadeRelationTranslation.ApplyDeleteCascadeRelation<T, Tid>(this, i_Instance, i_Policy, i_Relations).ConfigureAwait(false);
    }

    /// <summary>
    /// Gets the underlying connection
    /// </summary>
    /// <returns></returns>
    public IDbConnection GetConnection()
    {
      return m_ConvSession.Connection;
    }
  }

  #endregion
}
