using System.Threading.Tasks;
using NHibernate;

namespace NiceCore.Storage.NH.Conversation
{
  /// <summary>
  /// NH Transaction Handler
  /// </summary>
  public interface INHTransactionHandler
  {
    /// <summary>
    /// Begin Transaction Async
    /// </summary>
    /// <param name="i_Session"></param>
    /// <returns></returns>
    Task<ITransaction> BeginTransactionAsync(ISession i_Session);
  }
}