using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NHibernate.Event;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Conversations.EventListeners;

namespace NiceCore.Storage.NH.Conversation.EventListeners
{
  /// <summary>
  /// Nc Session Post Event Listener
  /// </summary>
  public class NcSessionPostEventListener : BaseNcSessionEventListener<INcConversationPostSaveOrUpdateEventListener, INcConversationPostDeleteEventListener>, INcSessionEventListener
  {
    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_ActiveConversationStore"></param>
    /// <param name="i_ConnectionName"></param>
    /// <param name="i_SaveOrUpdateEventListeners"></param>
    /// <param name="i_DeleteEventListeners"></param>
    /// <param name="i_Logger"></param>
    public NcSessionPostEventListener(IActiveConversationStore i_ActiveConversationStore,
      string i_ConnectionName,
      IList<INcConversationPostSaveOrUpdateEventListener> i_SaveOrUpdateEventListeners,
      IList<INcConversationPostDeleteEventListener> i_DeleteEventListeners,
      ILogger i_Logger) : base(i_ActiveConversationStore, i_ConnectionName, i_SaveOrUpdateEventListeners, i_DeleteEventListeners, i_Logger)
    {

    }

    /// <summary>
    /// Execute Save Listener
    /// </summary>
    /// <param name="i_Listener"></param>
    /// <param name="i_Event"></param>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    protected override ValueTask ExecuteSaveListener(INcConversationPostSaveOrUpdateEventListener i_Listener, SaveOrUpdateEvent i_Event, INotApplyingConversation t_Conversation, CancellationToken i_Token)
    {
      return i_Listener.PostSaveOrUpdate(i_Event.Entity, i_Event.ResultEntity, t_Conversation, i_Token);
    }

    /// <summary>
    /// Execute Delete Listener
    /// </summary>
    /// <param name="i_Listener"></param>
    /// <param name="i_Entity"></param>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    protected override ValueTask ExecuteDeleteListener(INcConversationPostDeleteEventListener i_Listener, object i_Entity, INotApplyingConversation t_Conversation, CancellationToken i_Token)
    {
      return i_Listener.PostDelete(i_Entity, t_Conversation, i_Token);
    }
  }
}