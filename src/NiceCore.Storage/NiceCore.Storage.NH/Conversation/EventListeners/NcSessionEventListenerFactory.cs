using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using NiceCore.ServiceLocation;
using NiceCore.ServiceLocation.Helpers;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Conversations.EventListeners;

namespace NiceCore.Storage.NH.Conversation.EventListeners
{
  /// <summary>
  /// Nc Session Event listener Factory
  /// </summary>
  [Register(InterfaceType = typeof(INcSessionEventListenerFactory))]
  public class NcSessionEventListenerFactory : INcSessionEventListenerFactory
  {
    /// <summary>
    /// Active ConversationStore
    /// </summary>
    public IActiveConversationStore ActiveConversationStore { get; set; }
    
    /// <summary>
    /// Service Container
    /// </summary>
    public IServiceContainer ServiceContainer { get; set; }
    
    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<INcSessionEventListener> Logger { get; set; }
    
    /// <summary>
    /// Creates
    /// </summary>
    /// <param name="i_ConnectionName"></param>
    /// <returns></returns>
    public INcSessionEventListener CreateNewPreEventListener(string i_ConnectionName)
    {
      using var deleteListeners = ServiceContainer.LocateAll<INcConversationPreDeleteEventListener>();
      using var saveOrUpdateListeners = ServiceContainer.LocateAll<INcConversationPreSaveOrUpdateEventListener>();
      return new NcSessionPreEventListener(ActiveConversationStore, i_ConnectionName, saveOrUpdateListeners.Instances().ToList(), deleteListeners.Instances().ToList(), Logger);
    }
    
    /// <summary>
    /// Creates
    /// </summary>
    /// <param name="i_ConnectionName"></param>
    /// <returns></returns>
    public INcSessionEventListener CreateNewPostEventListener(string i_ConnectionName)
    {
      using var deleteListeners = ServiceContainer.LocateAll<INcConversationPostDeleteEventListener>();
      using var saveOrUpdateListeners = ServiceContainer.LocateAll<INcConversationPostSaveOrUpdateEventListener>();
      return new NcSessionPostEventListener(ActiveConversationStore, i_ConnectionName, saveOrUpdateListeners.Instances().ToList(), deleteListeners.Instances().ToList(), Logger);
    }
  }
}