using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NHibernate.Event;
using NiceCore.Logging.Extensions;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Conversations.EventListeners;

namespace NiceCore.Storage.NH.Conversation.EventListeners
{
  /// <summary>
  /// Base Nc Session Event Listener
  /// </summary>
  /// <typeparam name="TSaveOrUpdateListener"></typeparam>
  /// <typeparam name="TDeleteListener"></typeparam>
  public abstract class BaseNcSessionEventListener<TSaveOrUpdateListener, TDeleteListener>
    where TSaveOrUpdateListener : IConnectionNameBasedNcConversationEventListener
    where TDeleteListener : IConnectionNameBasedNcConversationEventListener
  {
    /// <summary>
    /// Save Or Update Event Listeners
    /// </summary>
    public IList<TSaveOrUpdateListener> SaveOrUpdateEventListeners { get; }

    /// <summary>
    /// Delete Event Listeners
    /// </summary>
    public IList<TDeleteListener> DeleteEventListeners { get; }

    /// <summary>
    /// Logger
    /// </summary>
    public ILogger Logger { get; }

    /// <summary>
    /// ConnectionName
    /// </summary>
    public string ConnectionName { get; }

    /// <summary>
    /// Active Conversation Store
    /// </summary>
    public IActiveConversationStore ActiveConversationStore { get; }
    
    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_ActiveConversationStore"></param>
    /// <param name="i_ConnectionName"></param>
    /// <param name="i_SaveOrUpdateEventListeners"></param>
    /// <param name="i_DeleteEventListeners"></param>
    /// <param name="i_Logger"></param>
    protected BaseNcSessionEventListener(IActiveConversationStore i_ActiveConversationStore,
      string i_ConnectionName,
      IList<TSaveOrUpdateListener> i_SaveOrUpdateEventListeners,
      IList<TDeleteListener> i_DeleteEventListeners,
      ILogger i_Logger)
    {
      ConnectionName = i_ConnectionName;
      ActiveConversationStore = i_ActiveConversationStore;
      SaveOrUpdateEventListeners = i_SaveOrUpdateEventListeners;
      DeleteEventListeners = i_DeleteEventListeners;
      Logger = i_Logger;
    }

    /// <summary>
    /// On Delete Async
    /// </summary>
    /// <param name="event"></param>
    /// <param name="cancellationToken"></param>
    public async Task OnDeleteAsync(DeleteEvent @event, CancellationToken cancellationToken)
    {
      await RunDeleteListeners(@event.Entity, @event.Session, cancellationToken).ConfigureAwait(false);
    }

    /// <summary>
    /// On Delete Async
    /// </summary>
    /// <param name="event"></param>
    /// <param name="transientEntities"></param>
    /// <param name="cancellationToken"></param>
    public async Task OnDeleteAsync(DeleteEvent @event, ISet<object> transientEntities, CancellationToken cancellationToken)
    {
      await RunDeleteListeners(@event.Entity, @event.Session, cancellationToken).ConfigureAwait(false);
    }

    /// <summary>
    /// Execute Save Listener
    /// </summary>
    /// <param name="i_Listener"></param>
    /// <param name="i_Event"></param>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    protected abstract ValueTask ExecuteSaveListener(TSaveOrUpdateListener i_Listener, SaveOrUpdateEvent i_Event, INotApplyingConversation t_Conversation, CancellationToken i_Token);
    
    /// <summary>
    /// Execute Delete Listener
    /// </summary>
    /// <param name="i_Listener"></param>
    /// <param name="i_Entity"></param>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    protected abstract ValueTask ExecuteDeleteListener(TDeleteListener i_Listener, object i_Entity, INotApplyingConversation t_Conversation, CancellationToken i_Token);

    /// <summary>
    /// On SaveOrUpdate Async
    /// </summary>
    /// <param name="event"></param>
    /// <param name="cancellationToken"></param>
    public async Task OnSaveOrUpdateAsync(SaveOrUpdateEvent @event, CancellationToken cancellationToken)
    {
      if (SaveOrUpdateEventListeners == null || SaveOrUpdateEventListeners.Count == 0)
      {
        return;
      }

      var conv = GetConversationFor(@event.Session);
      foreach (var saveOrUpdateEventListener in SaveOrUpdateEventListeners)
      {
        try
        {
          if (saveOrUpdateEventListener.CanBeUsedForConnection(ConnectionName))
          {
            await ExecuteSaveListener(saveOrUpdateEventListener, @event, conv, cancellationToken).ConfigureAwait(false);
            
          }
          else
          {
            Logger.Debug($"{nameof(TSaveOrUpdateListener)} is not configured to run for connection {ConnectionName}");
          }
        }
        catch (Exception e)
        {
          Logger?.Error(e, $"An error occurred during execution of {nameof(TSaveOrUpdateListener)} of type {saveOrUpdateEventListener.GetType()}");
          throw;
        }
      }
    }

    private async ValueTask RunDeleteListeners(object i_Entity, IEventSource i_EventSource, CancellationToken i_CancellationToken)
    {
      if (DeleteEventListeners == null || DeleteEventListeners.Count == 0)
      {
        return;
      }

      var conv = GetConversationFor(i_EventSource);
      foreach (var deleteEventListener in DeleteEventListeners)
      {
        try
        {
          if (deleteEventListener.CanBeUsedForConnection(ConnectionName))
          {
            await ExecuteDeleteListener(deleteEventListener, i_Entity, conv, i_CancellationToken).ConfigureAwait(false);
          }
          else
          {
            Logger.Debug($"{nameof(TDeleteListener)} is not configured to run for connection {ConnectionName}");
          }
        }
        catch (Exception e)
        {
          Logger?.Error(e, $"An error occurred during execution of {nameof(TDeleteListener)} of type {deleteEventListener.GetType()}");
          throw;
        }
      }
    }

    private INotApplyingConversation GetConversationFor(IEventSource i_EventSource)
    {
      var id = i_EventSource.GetSessionImplementation()?.SessionId;
      if (id == null)
      {
        return null;
      }

      return ActiveConversationStore.GetActiveConversation(id.Value);
    }

    /// <summary>
    /// On Delete
    /// </summary>
    /// <param name="event"></param>
    public void OnDelete(DeleteEvent @event)
    {
      OnDeleteAsync(@event, CancellationToken.None).GetAwaiter().GetResult();
    }

    /// <summary>
    /// On Delete
    /// </summary>
    /// <param name="event"></param>
    /// <param name="transientEntities"></param>
    public void OnDelete(DeleteEvent @event, ISet<object> transientEntities)
    {
      OnDeleteAsync(@event, transientEntities, CancellationToken.None).GetAwaiter().GetResult();
    }

    /// <summary>
    /// On SaveOrUpdate - Sync
    /// </summary>
    /// <param name="event"></param>
    public void OnSaveOrUpdate(SaveOrUpdateEvent @event)
    {
      OnSaveOrUpdateAsync(@event, CancellationToken.None).GetAwaiter().GetResult();
    }
  }
}