namespace NiceCore.Storage.NH.Conversation.EventListeners
{
  /// <summary>
  /// Nc Session Event Listener Factory
  /// </summary>
  public interface INcSessionEventListenerFactory
  {
    /// <summary>
    /// Create new Pre Listener
    /// </summary>
    /// <param name="i_ConnectionName"></param>
    /// <returns></returns>
    public INcSessionEventListener CreateNewPreEventListener(string i_ConnectionName);
    
    /// <summary>
    /// Create New Post Listener
    /// </summary>
    /// <param name="i_ConnectionName"></param>
    /// <returns></returns>
    public INcSessionEventListener CreateNewPostEventListener(string i_ConnectionName);
  }
}