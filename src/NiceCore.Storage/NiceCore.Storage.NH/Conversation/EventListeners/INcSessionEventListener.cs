using NHibernate.Event;

namespace NiceCore.Storage.NH.Conversation.EventListeners
{
  /// <summary>
  /// Nc Session Event Listener
  /// </summary>
  public interface INcSessionEventListener : IDeleteEventListener, ISaveOrUpdateEventListener
  {
    
  }
}