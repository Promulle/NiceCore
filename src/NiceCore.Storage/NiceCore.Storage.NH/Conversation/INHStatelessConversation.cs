using NiceCore.Storage.Conversations;

namespace NiceCore.Storage.NH.Conversation
{
  /// <summary>
  /// NH Stateless Conversation
  /// </summary>
  public interface INHStatelessConversation : INHStatelessReadOnlyConversation, IStatelessConversation
  {
    
  }
}