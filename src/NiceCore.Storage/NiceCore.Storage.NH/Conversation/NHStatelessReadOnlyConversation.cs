using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NHibernate;
using NHibernate.Engine;
using NiceCore.Entities;
using NiceCore.ServiceLocation;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Conversations.Queries;
using NiceCore.Storage.NH.Conversation.Initialization;
using NiceCore.Storage.NH.Conversation.Untyped;

namespace NiceCore.Storage.NH.Conversation
{
  /// <summary>
  /// NH Stateless ReadOnly Conversation
  /// </summary>
  [Register(InterfaceType = typeof(INHStatelessReadOnlyConversation))]
  public class NHStatelessReadOnlyConversation : BaseNHConversation, INHStatelessReadOnlyConversation
  {
    /// <summary>
    /// Session
    /// </summary>
    protected IStatelessSession m_ConvSession;

    /// <summary>
    /// Logger
    /// </summary>
    public override ILogger Logger
    {
      get { return ActualLogger; }
    }

    /// <summary>
    /// Actual Logger
    /// </summary>
    public ILogger<NHStatelessReadOnlyConversation> ActualLogger { get; set; }

    /// <summary>
    /// Active Conversation Store
    /// </summary>
    public IActiveConversationStore ActiveConversationStore { get; set; }

    /// <summary>
    /// Initializes this Connection
    /// </summary>
    public virtual async ValueTask Initialize(NHStatelessConversationInit i_Init, CancellationToken i_Token)
    {
      m_ConvSession  = await i_Init.SessionProvider.CreateStatelessSession(i_Init.ConnectionInfo, i_Token).ConfigureAwait(false);
      Context        = i_Init.Context;
      ConnectionInfo = i_Init.ConnectionInfo;
      Untyped        = new UntypedNHStatelessConversationSupport(m_ConvSession);
      SetQueryLimiterSupport(i_Init.QueryLimiters);
    }

    /// <summary>
    /// Gets the identifier of the conversation
    /// </summary>
    /// <returns></returns>
    public Guid? GetIdentifier()
    {
      if (m_ConvSession == null)
      {
        throw new InvalidOperationException($"{nameof(GetIdentifier)} called before conversation was initialized.");
      }

      return m_ConvSession.GetSessionImplementation()?.SessionId;
    }

    /// <summary>
    /// Get Session Implementor
    /// </summary>
    /// <returns></returns>
    protected override ISessionImplementor GetSessionImplementor()
    {
      return m_ConvSession.GetSessionImplementation();
    }

    /// <summary>
    /// Create a dependent query
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    public async Task<IPreSelectDependentQuery<T, TId>> DependentQuery<T, TId>(IEnumerable<IQueryPreSelector> i_PreSelectors, IConversationContext i_Context, Func<IQueryable<T>, ValueTask<IQueryable<T>>> i_QueryModifier) where T : IDomainEntity<TId>
    {
      var qry = new PreSelectDependentQuery<T, TId>();
      await qry.ApplyPreSelection(this, i_Context, i_PreSelectors, i_QueryModifier).ConfigureAwait(false);
      return qry;
    }

    /// <summary>
    /// Dispose
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    protected override void Dispose(bool i_IsDisposing)
    {
      m_ConvSession?.Dispose();
      base.Dispose(i_IsDisposing);
    }
  }
}
