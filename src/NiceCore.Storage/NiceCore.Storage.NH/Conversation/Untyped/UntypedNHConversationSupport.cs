using System;
using System.Threading.Tasks;
using NHibernate;
using NiceCore.Storage.Conversations;

namespace NiceCore.Storage.NH.Conversation.Untyped
{
  /// <summary>
  /// NH Impl for untyped support
  /// </summary>
  public class UntypedNHConversationSupport : IUntypedConversationSupport
  {
    private readonly ISession m_Session;
    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_Session"></param>
    public UntypedNHConversationSupport(ISession i_Session)
    {
      m_Session = i_Session;
    }
    
    /// <summary>
    /// Get by ID async
    /// </summary>
    /// <param name="i_Type"></param>
    /// <param name="i_Id"></param>
    /// <returns></returns>
    public Task<object> GetById(Type i_Type, object i_Id)
    {
      return m_Session.GetAsync(i_Type, i_Id);
    }
  }
}
