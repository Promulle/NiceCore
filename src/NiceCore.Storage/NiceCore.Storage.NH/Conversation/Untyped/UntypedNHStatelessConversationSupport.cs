using System;
using System.Threading.Tasks;
using NHibernate;
using NiceCore.Storage.Conversations;

namespace NiceCore.Storage.NH.Conversation.Untyped
{
  /// <summary>
  /// Untyped Support
  /// </summary>
  public sealed class UntypedNHStatelessConversationSupport : IUntypedConversationSupport
  {
    private readonly IStatelessSession m_Session;
    
    /// <summary>
    /// Create Instance that uses i_Session
    /// </summary>
    /// <param name="i_Session"></param>
    public UntypedNHStatelessConversationSupport(IStatelessSession i_Session)
    {
      m_Session = i_Session;
    }
    
    /// <summary>
    /// Get By Id
    /// </summary>
    /// <param name="i_Type"></param>
    /// <param name="i_Id"></param>
    /// <returns></returns>
    public Task<object> GetById(Type i_Type, object i_Id)
    {
      return m_Session.GetAsync(i_Type.FullName, i_Id);
    }
  }
}