using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Conversations.Factories;

namespace NiceCore.Storage.NH.Conversation.Factories
{
  /// <summary>
  /// Extension methods that use nhConversationCreation Parameters
  /// </summary>
  public static class NHConversationFactoryExtensions
  {
    /// <summary>
    /// Conversation that gets a connection provided
    /// </summary>
    /// <param name="i_Factory"></param>
    /// <param name="i_Name"></param>
    /// <param name="i_Context"></param>
    /// <param name="i_Connection"></param>
    /// <returns></returns>
    public static ValueTask<IConversation> Conversation(this IConversationFactory i_Factory, string i_Name, IConversationContext i_Context, DbConnection i_Connection)
    {
      return i_Factory.CreateNewConversation(new NHConversationCreationParameters()
      {
        Connection = i_Connection,
        Context = i_Context,
        Name = i_Name,
        MessageSendingParameters = null,
        CreationRetryStrategy = null,
        ConversationConnectionRetryStrategy = null,
        TransactionHandlingMode = TransactionHandlingMode.Explicit
      }, CancellationToken.None);
    }
  }
}