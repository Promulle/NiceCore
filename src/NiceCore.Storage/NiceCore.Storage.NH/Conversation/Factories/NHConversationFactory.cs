using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Conversations.Config;
using NiceCore.Storage.Conversations.Factories;
using NiceCore.Storage.NH.Conversation.Initialization;
using NiceCore.Storage.NH.Conversation.Sessions;
using NiceCore.Storage.NH.Diffing;
using NiceCore.Storage.NH.SessionFactories;
using NiceCore.Storage.NH.SessionFactories.Model;

namespace NiceCore.Storage.NH.Conversation.Factories
{
  /// <summary>
  /// Conversation Factory 
  /// </summary>
  [Register(InterfaceType = typeof(IConversationFactory), LifeStyle = LifeStyles.Singleton)]
  public sealed class NHConversationFactory : IConversationFactory
  {
    /// <summary>
    /// Container
    /// </summary>
    public IServiceContainer Container { get; set; }
    
    /// <summary>
    /// Differ Service
    /// </summary>
    public INHDifferService DifferService { get; set; }
    
    /// <summary>
    /// Configuration
    /// </summary>
    public INcConfiguration Configuration { get; set; }
    
    /// <summary>
    /// Session Factories Service
    /// </summary>
    public ISessionFactoriesService SessionFactoriesService { get; set; }
    
    /// <summary>
    /// Conversation Init Factory
    /// </summary>
    public INHConversationInitFactory ConversationInitFactory { get; set; }
    
    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<NHConversationFactory> Logger { get; set; }
    
    /// <summary>
    /// Active Conversation Store
    /// </summary>
    public IActiveConversationStore ActiveConversationStore { get; set; }
    
    /// <summary>
    /// Configured Context Service
    /// </summary>
    public IConfiguredContextService ConfiguredContextService { get; set; }

    /// <summary>
    /// Creates a new Conversation
    /// </summary>
    /// <param name="i_Parameters"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    public async ValueTask<IStatelessConversation> CreateNewStatelessConversation(ConversationCreationParameters i_Parameters, CancellationToken i_Token)
    {
      var sessionInfo = await SessionFactoriesService.GetOrInitializeFactory(i_Parameters.Name, i_Parameters.CreationRetryStrategy, false);
      return await GetNewConversation<INHStatelessConversation, NHStatelessConversationInit>(sessionInfo, i_Parameters, ConversationInitFactory, i_Token).ConfigureAwait(false);
    }
    
    /// <summary>
    /// Creates a new Conversation
    /// </summary>
    /// <param name="i_Parameters"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    public async ValueTask<IReadonlyConversation> CreateNewStatelessReadOnlyConversation(ConversationCreationParameters i_Parameters, CancellationToken i_Token)
    {
      var sessionInfo = await SessionFactoriesService.GetOrInitializeFactory(i_Parameters.Name, i_Parameters.CreationRetryStrategy, false);
      return await GetNewConversation<INHStatelessReadOnlyConversation, NHStatelessConversationInit>(sessionInfo, i_Parameters, ConversationInitFactory, i_Token).ConfigureAwait(false);
    }

    /// <summary>
    /// Creates a new Conversation
    /// </summary>
    /// <param name="i_Parameters"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    public async ValueTask<IConversation> CreateNewConversation(ConversationCreationParameters i_Parameters, CancellationToken i_Token)
    {
      var sessionInfo = await SessionFactoriesService.GetOrInitializeFactory(i_Parameters.Name, i_Parameters.CreationRetryStrategy, false);
      var conv = await GetNewConversation<INHConversation, NHConversationInit>(sessionInfo, i_Parameters, ConversationInitFactory, i_Token).ConfigureAwait(false);
      ActiveConversationStore.AddActiveConversation(conv);
      return conv;
    }
    
    private async ValueTask<TConversation> GetNewConversation<TConversation, TInit>(SessionCreationInfo i_SessionCreationInfo, ConversationCreationParameters i_Parameters, IInitProvider<TInit> i_InitProvider, CancellationToken i_Token)
    where TConversation: IInitializableConversation<TInit>
    {
      Logger.Information($"Resolving new conversation for {i_Parameters.Name} ({typeof(TConversation).Name})");
      var conv = Container.Resolve<TConversation>();
      if (conv != null)
      {
        var parameters = EnsureContextInParameters(i_Parameters);
        Logger.Information($"Successfully resolved ({typeof(TConversation).Name}). Applying limiters and initializing conversation.");
        var sessionProvider = PickSessionProvider(i_SessionCreationInfo, i_Parameters);
        var init = i_InitProvider.ProvideInit(sessionProvider, i_SessionCreationInfo.ConnectionInfo, parameters);
        await conv.Initialize(init, i_Token).ConfigureAwait(false);
        Logger.Information($"Successfully initialized ({typeof(TConversation).Name}) for connection {parameters.Name}");
        return conv;
      }

      Logger.Error($"Could not create ({typeof(TConversation).Name}) for {i_Parameters.Name}");
      return default;
    }

    private ConversationCreationParameters EnsureContextInParameters(ConversationCreationParameters i_Parameter)
    {
      if (i_Parameter.Context != null)
      {
        return i_Parameter;
      }

      var autoApplyDefault = Configuration.GetValue(UseDefaultConversationContextAutomaticallySetting.KEY, false);
      if (!autoApplyDefault)
      {
        return i_Parameter;
      }
      var context = ConfiguredContextService.GetConfiguredDefaultContext();
      if (context == null)
      {
        return i_Parameter;
      }
      return ConversationCreationParameters.CopyWithContext(i_Parameter, context);
    }

    private static INcSessionProvider PickSessionProvider(SessionCreationInfo i_CreationInfo, ConversationCreationParameters i_Parameters)
    {
      if (i_Parameters is NHConversationCreationParameters nhParameters && nhParameters.Connection != null)
      {
        return new UserProvidedConnectionSessionProvider(i_CreationInfo.SessionFactory, nhParameters.Connection);
      }
      return i_CreationInfo.NativeSessionProvider;
    }
  }
}