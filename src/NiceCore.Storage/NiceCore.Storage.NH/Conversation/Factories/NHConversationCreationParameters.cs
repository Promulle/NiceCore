using System.Data.Common;
using NiceCore.Storage.Conversations.Factories;

namespace NiceCore.Storage.NH.Conversation.Factories
{
  /// <summary>
  /// NH Conversation Creation Parameters
  /// </summary>
  public sealed class NHConversationCreationParameters : ConversationCreationParameters
  {
    /// <summary>
    /// Connection to be provided
    /// </summary>
    public DbConnection Connection { get; init; }
  }
}