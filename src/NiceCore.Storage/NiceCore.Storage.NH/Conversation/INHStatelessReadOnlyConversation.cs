using NiceCore.Storage.Conversations;
using NiceCore.Storage.NH.Conversation.Initialization;

namespace NiceCore.Storage.NH.Conversation
{
  /// <summary>
  /// INHStateless ReadOnly Conversation
  /// </summary>
  public interface INHStatelessReadOnlyConversation : IReadonlyConversation, IInitializableConversation<NHStatelessConversationInit>
  {
  }
}