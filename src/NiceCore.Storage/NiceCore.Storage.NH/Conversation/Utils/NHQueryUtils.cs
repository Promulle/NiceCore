using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Engine;
using NiceCore.Entities;
using NiceCore.Storage.Limiters;
using NiceCore.Storage.NH.Conversation.Queries;
using NiceCore.Storage.Queries;

namespace NiceCore.Storage.NH.Conversation.Utils
{
  /// <summary>
  /// NH Query Utils
  /// </summary>
  public static class NHQueryUtils
  {
    /// <summary>
    /// Queries instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <returns></returns>
    public static IQueryable<T> Query<T, Tid>(ISessionImplementor i_ConvSession,
      INHSessionQueryProvider i_SessionQueryProvider,
      INcQueryOptions i_QueryOptions,
      QueryLimiterSupport i_QueryLimiterSupport,
      bool i_ApplyDefaultLimiter,
      IReadOnlyCollection<IQueryLimiter> i_CustomQueryLimiters) where T : IDomainEntity<Tid>
    {
      var query = i_SessionQueryProvider.CreateNewQuery<T>(i_ConvSession, i_QueryOptions);
      if (i_ApplyDefaultLimiter)
      {
        query = i_QueryLimiterSupport.ApplyLimiter(query);
      }

      if (i_CustomQueryLimiters != null && i_CustomQueryLimiters.Count > 0)
      {
        query = i_QueryLimiterSupport.ApplyCustomLimiter(query, i_CustomQueryLimiters);
      }

      return query;
    }
  }
}