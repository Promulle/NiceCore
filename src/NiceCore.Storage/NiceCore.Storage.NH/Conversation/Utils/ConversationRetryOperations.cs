﻿using NHibernate;
using NiceCore.Logging;
using Polly;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace NiceCore.Storage.NH.Conversation.Utils
{
  /// <summary>
  /// Static helper class having the impl for retrying operations for conversations
  /// </summary>
  internal class ConversationRetryOperations
  {
    private readonly ISession m_Session;
    private readonly IDefaultConnectionRetryStrategy m_ConnectionRetryStrategy;

    /// <summary>
    /// Constructor filling session and default retry strategy
    /// </summary>
    /// <param name="i_Session"></param>
    /// <param name="i_ConnectionRetryStrategy"></param>
    internal ConversationRetryOperations(ISession i_Session, IDefaultConnectionRetryStrategy i_ConnectionRetryStrategy)
    {
      m_Session = i_Session;
      m_ConnectionRetryStrategy = i_ConnectionRetryStrategy;
    }

    internal void Do(Action i_Operation, string i_OperationName, ILogger i_Logger)
    {
      if (IsSessionReady(i_OperationName, i_Logger))
      {
        if (m_ConnectionRetryStrategy != null)
        {
          m_ConnectionRetryStrategy.Execute(i_Operation);
        }
        else
        {
          i_Operation();
        }
      }
    }

    internal Task DoAsync(Func<Task> i_Operation, string i_OperationName, ILogger i_Logger)
    {
      if (IsSessionReady(i_OperationName, i_Logger))
      {
        if (m_ConnectionRetryStrategy != null)
        {
          return m_ConnectionRetryStrategy.Execute(i_Operation);
        }
        else
        {
          return i_Operation();
        }
      }
      return Task.CompletedTask;
    }

    internal T Do<T>(Func<T> i_Operation, string i_OperationName, ILogger i_Logger)
    {
      if (IsSessionReady(i_OperationName, i_Logger))
      {
        if (m_ConnectionRetryStrategy != null)
        {
          return m_ConnectionRetryStrategy.Execute(i_Operation);
        }
        else
        {
          return i_Operation();
        }
      }
      return default;
    }

    internal void Do(Action i_Operation, ISyncPolicy i_Policy, string i_OperationName, ILogger i_Logger)
    {
      if (IsSessionReady(i_OperationName, i_Logger))
      {
        if (i_Policy != null)
        {
          i_Policy.Execute(i_Operation);
        }
        else if (m_ConnectionRetryStrategy != null)
        {
          m_ConnectionRetryStrategy.Execute(i_Operation);
        }
        else
        {
          i_Operation.Invoke();
        }
      }
    }

    internal T Do<T>(Func<T> i_Operation, ISyncPolicy i_Policy, string i_OperationName, ILogger i_Logger)
    {
      if (IsSessionReady(i_OperationName, i_Logger))
      {
        if (i_Policy != null)
        {
          return i_Policy.Execute(i_Operation);
        }
        else if (m_ConnectionRetryStrategy != null)
        {
          return m_ConnectionRetryStrategy.Execute(i_Operation);
        }
        return i_Operation.Invoke();
      }
      return default;
    }

    internal Task DoAsync(Func<Task> i_Operation, IAsyncPolicy i_Policy, string i_OperationName, ILogger i_Logger)
    {
      if (IsSessionReady(i_OperationName, i_Logger))
      {
        if (i_Policy != null)
        {
          return i_Policy.ExecuteAsync(i_Operation);
        }
        else if (m_ConnectionRetryStrategy != null)
        {
          return m_ConnectionRetryStrategy.Execute(i_Operation);
        }
        return i_Operation.Invoke();
      }
      return Task.CompletedTask;
    }

    internal Task<T> DoAsync<T>(Func<Task<T>> i_Operation, IAsyncPolicy i_Policy, string i_OperationName, ILogger i_Logger)
    {
      if (IsSessionReady(i_OperationName, i_Logger))
      {
        if (i_Policy != null)
        {
          return i_Policy.ExecuteAsync(i_Operation);
        }
        else if (m_ConnectionRetryStrategy != null)
        {
          return m_ConnectionRetryStrategy.Execute(i_Operation);
        }
        return i_Operation.Invoke();
      }
      return Task.FromResult(default(T));
    }

    private bool IsSessionReady(string i_OperationName, ILogger i_Logger)
    {
      return SessionReadyChecker.IsSessionReady(m_Session, i_OperationName, i_Logger);
    }
  }
}
