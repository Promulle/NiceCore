﻿using Microsoft.Extensions.Logging;
using NHibernate;
using NiceCore.Logging.Extensions;

namespace NiceCore.Storage.NH.Conversation.Utils
{
  /// <summary>
  /// Helper class for checking whether a session is ready or not
  /// </summary>
  internal static class SessionReadyChecker
  {
    /// <summary>
    /// Checks if i_Session is ready to be used, logs if not
    /// </summary>
    /// <param name="i_Session">Session instance to check</param>
    /// <param name="i_OperationName">Name of operation for logging purposes</param>
    /// <param name="i_Logger"></param>
    /// <returns></returns>
    internal static bool IsSessionReady(ISession i_Session, string i_OperationName, ILogger i_Logger)
    {
      var rdy = i_Session != null && i_Session.IsOpen;
      if (!rdy)
      {
        i_Logger.Warning($"Attempted to execute an operation of type {i_OperationName} but session is either null or not open: Conversation=[{i_Session}] IsOpen=[{i_Session?.IsOpen}]");
      }
      return rdy;
    }
  }
}
