using System;
using System.Threading;
using System.Threading.Tasks;
using NHibernate;
using NiceCore.Entities;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation;
using NiceCore.Services.Validation;
using NiceCore.Services.Validation.Exceptions;
using NiceCore.Services.Validation.Model;
using NiceCore.Storage.NH.Conversation.Initialization;

namespace NiceCore.Storage.NH.Conversation
{
  /// <summary>
  /// Stateless Conversation
  /// </summary>
  [Register(InterfaceType = typeof(INHStatelessConversation))]
  public class NHStatelessConversation : NHStatelessReadOnlyConversation, INHStatelessConversation
  {
    private ITransaction m_Transaction;

    /// <summary>
    /// Validation Service
    /// </summary>
    public IValidationService ValidationService { get; set; }

    /// <summary>
    /// Container
    /// </summary>
    public IServiceContainer Container { get; set; }

    /// <summary>
    /// Initialize
    /// </summary>
    /// <param name="i_Init"></param>
    /// <param name="i_Token"></param>
    public override async ValueTask Initialize(NHStatelessConversationInit i_Init, CancellationToken i_Token)
    {
      await base.Initialize(i_Init, i_Token);
      if (i_Init.BatchSize != null)
      {
        m_ConvSession.SetBatchSize(i_Init.BatchSize.Value);
      }

      ConnectionInfo = i_Init.ConnectionInfo;
      m_Transaction  = m_ConvSession.BeginTransaction();
    }

    /// <summary>
    /// Save Or Update Async
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_Config"></param>
    /// <param name="i_Node"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    public async ValueTask SaveOrUpdateAsync<T, TId>(T i_Entity, IConversationOperationConfiguration i_Config, SubValidationNode i_Node)
      where T : IDomainEntity<TId>
    {
      var op = GetOperation<T, TId>(i_Entity);
      await CheckValidateInstance(i_Config, i_Entity, StorageValidationOperations.SAVE_OR_UPDATE, i_Node).ConfigureAwait(false);
      if (op == ConversationOperations.Save)
      {
        await m_ConvSession.InsertAsync(i_Entity).ConfigureAwait(false);
      }
      else if (op == ConversationOperations.Update)
      {
        await m_ConvSession.UpdateAsync(i_Entity).ConfigureAwait(false);
      }
      else
      {
        throw new NotSupportedException($"SaveOrUpdate Async returned a not supported operation {op}");
      }
    }

    /// <summary>
    /// Save Or Update Async
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_Config"></param>
    /// <param name="i_Node"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    public async ValueTask DeleteAsync<T, TId>(T i_Entity, IConversationOperationConfiguration i_Config, SubValidationNode i_Node)
      where T : IDomainEntity<TId>
    {
      await CheckValidateInstance(i_Config, i_Entity, StorageValidationOperations.DELETE, i_Node).ConfigureAwait(false);
      await m_ConvSession.DeleteAsync(i_Entity).ConfigureAwait(false);
    }

    private async Task CheckValidateInstance(IConversationOperationConfiguration i_Config, object i_Instance, string i_Operation, SubValidationNode i_Node)
    {
      if (i_Config.ValidateInstance && ValidationService != null)
      {
        Logger.Information($"Validating an instance of type {i_Instance.GetType().Name} before operation of type {i_Operation}");
        var res = await ValidationService.Validate(i_Instance, i_Operation, i_Node, Context?.ToValidationContext(null, Container)).ConfigureAwait(false);
        ValidationExceptionRaiser.ThrowIfNotValid(res, false);
      }
      else
      {
        Logger.Debug($"Validation skipped because {nameof(i_Config.ValidateInstance)} was set to false. Value: {i_Config.ValidateInstance}. Or the property {nameof(ValidationService)} of this conversation was not set. {nameof(ValidationService)} is set: {ValidationService != null}");
      }
    }

    /// <summary>
    /// Apply Async
    /// </summary>
    /// <returns></returns>
    public async ValueTask ApplyAsync()
    {
      try
      {
        await m_Transaction.CommitAsync().ConfigureAwait(false);
      }
      catch (Exception e)
      {
        Logger.Error(e, "An error occurred during commit of transaction in stateless conversation.");
        throw;
      }
    }

    /// <summary>
    /// Dispose
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    protected override void Dispose(bool i_IsDisposing)
    {
      if (m_Transaction != null && m_Transaction.IsActive)
      {
        try
        {
          m_Transaction.Rollback();
        }
        catch (Exception e)
        {
          Logger.Error(e, "Rolling back transaction during dispose of stateless conversation failed.");
        }
      }

      base.Dispose(i_IsDisposing);
    }
  }
}
