using System;
using System.Threading;
using System.Threading.Tasks;
using NHibernate;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using NiceCore.Storage.NH.Conversation.Config;

namespace NiceCore.Storage.NH.Conversation
{
  /// <summary>
  /// Default NH Transaction Handler
  /// </summary>
  [Register(InterfaceType = typeof(INHTransactionHandler), LifeStyle = LifeStyles.Singleton)]
  public class DefaultNHTransactionHandler : INHTransactionHandler
  {
    /// <summary>
    /// Configuration
    /// </summary>
    public INcConfiguration Configuration { get; set; }

    /// <summary>
    /// Begin Transaction Async
    /// </summary>
    /// <param name="i_Session"></param>
    /// <returns></returns>
    /// <exception cref="InvalidOperationException"></exception>
    public Task<ITransaction> BeginTransactionAsync(ISession i_Session)
    {
      if (i_Session == null)
      {
        throw new ArgumentException($"Could not {nameof(BeginTransactionAsync)}: parameter {nameof(i_Session)} was passed as null.", nameof(i_Session));
      }
      if (ForceAsync())
      {
        return Task.Factory.StartNew(session =>
        {
          if (session is ISession castedSession)
          {
            return castedSession.BeginTransaction();
          }

          throw new InvalidOperationException($"Could not {nameof(BeginTransactionAsync)}: passed object that should be used as Session, could not be cast to {typeof(ISession)}");
        }, i_Session);
      }

      var transaction = i_Session.BeginTransaction();
      return Task.FromResult(transaction);
    }

    private bool ForceAsync()
    {
      if (Configuration != null && Configuration.IsReady)
      {
        return Configuration.GetValue(ForceAsyncTransactionHandlingSetting.KEY, true);  
      }
      return false;
    }
  }
}