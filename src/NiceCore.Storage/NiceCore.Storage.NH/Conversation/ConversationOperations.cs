﻿namespace NiceCore.Storage.NH.Conversation
{
  /// <summary>
  /// Operation Type for single conversation operation
  /// </summary>
  public enum ConversationOperations
  {
    /// <summary>
    /// New Entity saved
    /// </summary>
    Save,

    /// <summary>
    /// Entity updated
    /// </summary>
    Update,

    /// <summary>
    /// Entity deleted
    /// </summary>
    Delete,
  }
}
