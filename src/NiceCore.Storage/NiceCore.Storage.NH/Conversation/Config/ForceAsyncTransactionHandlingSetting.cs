using System;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Storage.NH.Conversation.Config
{
  /// <summary>
  /// Force Async Transaction Handling
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class ForceAsyncTransactionHandlingSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// Key
    /// </summary>
    public const string KEY = "NiceCore.Storage.Conversation.ForceAsyncTransactionHandling";
    
    /// <summary>
    /// KEy
    /// </summary>
    public string Key { get; } = KEY;
    
    /// <summary>
    /// Setting Type
    /// </summary>
    public Type SettingType { get; } = typeof(bool);
  }
}