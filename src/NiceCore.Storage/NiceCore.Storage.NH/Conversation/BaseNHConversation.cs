using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using NHibernate.Engine;
using NiceCore.Base;
using NiceCore.Entities;
using NiceCore.Extensions;
using NiceCore.Storage.Connections;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Conversations.Flags;
using NiceCore.Storage.Conversations.Queries;
using NiceCore.Storage.Limiters;
using NiceCore.Storage.NH.Conversation.Queries;
using NiceCore.Storage.NH.Conversation.Utils;
using NiceCore.Storage.Queries;

namespace NiceCore.Storage.NH.Conversation
{
  /// <summary>
  /// Base Impl for nh conversations
  /// </summary>
  public abstract class BaseNHConversation : BaseDisposable
  {
    /// <summary>
    /// Options
    /// </summary>
    protected INcQueryOptions m_Options;
    
    /// <summary>
    /// Flags
    /// </summary>
    public ConversationOperationFlags Flags { get; } = new();
    
    /// <summary>
    /// Context
    /// </summary>
    public IConversationContext Context { get; protected set; }
    
    /// <summary>
    /// Query Limiter Support
    /// </summary>
    public QueryLimiterSupport QueryLimiterSupport { get; protected set; }
    
    /// <summary>
    /// Untyped
    /// </summary>
    public IUntypedConversationSupport Untyped { get; protected set; }
    
    /// <summary>
    /// Logger
    /// </summary>
    public abstract ILogger Logger { get; }
    
    /// <summary>
    /// Session Query Provider
    /// </summary>
    public INHSessionQueryProvider SessionQueryProvider { get; set; }
    
    /// <summary>
    /// Conversation Query Options Provider
    /// </summary>
    public IConversationQueryOptionsProvider ConversationQueryOptionsProvider { get; set; }

    /// <summary>
    /// Connection Name
    /// </summary>
    public string ConnectionName
    {
      get { return ConnectionInfo.ConnectionName; }
    }
    
    /// <summary>
    /// Connection Info
    /// </summary>
    public IConnectionInfo ConnectionInfo { get; protected set; }

    /// <summary>
    /// Get Session Implementor
    /// </summary>
    /// <returns></returns>
    protected abstract ISessionImplementor GetSessionImplementor();
    
    /// <summary>
    /// Returns operating to use
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    protected static ConversationOperations GetOperation<T, TId>(T i_Entity) where T : IDomainEntity<TId>
    {
      if (!i_Entity.ID.Equals(default(TId)))
      {
        return ConversationOperations.Update;
      }

      return ConversationOperations.Save;
    }

    /// <summary>
    /// Creates a new Instance for QueryLimiter Support and sets it
    /// </summary>
    /// <param name="i_Limiters"></param>
    protected void SetQueryLimiterSupport(IEnumerable<IQueryLimiter> i_Limiters)
    {
      QueryLimiterSupport = new QueryLimiterSupport(i_Limiters, false, Logger, Context, Flags);
    }
    
    /// <summary>
    /// Queries instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <returns></returns>
    public IQueryable<T> Query<T, Tid>(bool i_ApplyDefaultLimiter, IReadOnlyCollection<IQueryLimiter> i_CustomQueryLimiters, INcQueryOptions i_Options) where T : IDomainEntity<Tid>
    {
      var options = GetOptionsForQuery(typeof(T), i_Options);
      return NHQueryUtils.Query<T, Tid>(GetSessionImplementor(),
        SessionQueryProvider,
        options,
        QueryLimiterSupport,
        i_ApplyDefaultLimiter,
        i_CustomQueryLimiters);
    }
    
    /// <summary>
    /// Sets options for queries of this conversation
    /// </summary>
    /// <param name="i_Options"></param>
    public void UseOptions(INcQueryOptions i_Options)
    {
      m_Options = i_Options;
    }
    
    /// <summary>
    /// Gets applicable Options for query
    /// </summary>
    /// <param name="i_QueryableTargetType"></param>
    /// <param name="i_Options"></param>
    /// <returns></returns>
    protected INcQueryOptions GetOptionsForQuery(Type i_QueryableTargetType, INcQueryOptions i_Options)
    {
      if (i_Options != null)
      {
        return i_Options;
      }

      if (m_Options != null)
      {
        return m_Options;
      }
      
      if (ConversationQueryOptionsProvider != null)
      {
        return ConversationQueryOptionsProvider.GetOptions(i_QueryableTargetType);
      }

      return null;
    }
  }
}