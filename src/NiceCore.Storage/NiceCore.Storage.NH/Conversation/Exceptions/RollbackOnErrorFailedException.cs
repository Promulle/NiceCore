using System;

namespace NiceCore.Storage.NH.Conversation.Exceptions
{
  /// <summary>
  /// Exception that is thrown if the cancel on error process itself failed.
  /// Contains the exception that caused the cancel process and the exception that occured during cancellation
  /// </summary>
  public class RollbackOnErrorFailedException : Exception
  {
    /// <summary>
    /// The actual exception
    /// </summary>
    public Exception ActualException { get; }
    
    /// <summary>
    /// The exception that occured during cancellation
    /// </summary>
    public Exception CancellationException { get; }

    /// <summary>
    /// Ctor filling the exceptions
    /// </summary>
    /// <param name="i_ActualException"></param>
    /// <param name="i_CancellationException"></param>
    public RollbackOnErrorFailedException(Exception i_ActualException, Exception i_CancellationException): base(BuildMessage(i_ActualException, i_CancellationException))
    {
      ActualException = i_ActualException;
      CancellationException = i_CancellationException;
    }

    private static string BuildMessage(Exception i_ActualException, Exception i_CancellationException)
    {
      return $"Rollback caused by exception of type {i_ActualException.GetType().Name} failed because: {i_CancellationException.Message} ({i_CancellationException.GetType().Name}). Reason for rollback: {i_ActualException.Message} stackTrace: {i_ActualException.StackTrace}";
    }
  }
}
