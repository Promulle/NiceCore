using System;
using System.Runtime.CompilerServices;

namespace NiceCore.Storage.NH.Conversation.Exceptions
{
  /// <summary>
  /// Session Not Ready exception
  /// </summary>
  public class SessionNotReadyException : Exception
  {
    /// <summary>
    /// Name of operation that was attempted with a not ready session
    /// </summary>
    public string OperationName { get; }
    
    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_OperationName"></param>
    public SessionNotReadyException([CallerMemberName] string i_OperationName = null): base(GetMessage(i_OperationName))
    {
      OperationName = i_OperationName;
    }

    private static string GetMessage(string i_OperationName)
    {
      return $"Session is not ready while {i_OperationName} was attempted";
    }
  }
}