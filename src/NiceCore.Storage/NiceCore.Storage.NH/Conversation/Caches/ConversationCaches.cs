﻿namespace NiceCore.Storage.NH.Conversation.Caches
{
  /// <summary>
  /// Static caches used by NHConversation
  /// </summary>
  public static class ConversationCaches
  {
    /// <summary>
    /// Queue Message Function Cache
    /// </summary>
    public static QueueMessageFunctionCache QueueMessageFunctionCache { get; } = new();
  }
}
