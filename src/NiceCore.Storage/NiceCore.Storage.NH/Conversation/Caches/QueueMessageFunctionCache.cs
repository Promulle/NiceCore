﻿using NiceCore.Base.Caching;
using NiceCore.Caches;
using NiceCore.Entities;
using NiceCore.Storage.Diffing;
using System;
using System.Linq.Expressions;
using System.Reflection;

namespace NiceCore.Storage.NH.Conversation.Caches
{
  /// <summary>
  /// Specialized Cache for NHConversation message queuing
  /// </summary>
  public class QueueMessageFunctionCache : BaseSpecializedSimpleCache<Type, Action<NHConversation, object, ConversationOperations, StorageDiffResult>>
  {
    private static readonly string QUEUE_MESSAGE_FUNCTION = "QueueMessageForEntityEvent";

    /// <summary>
    /// Create Func
    /// </summary>
    /// <param name="i_EntityType"></param>
    /// <returns></returns>
    protected override Action<NHConversation, object, ConversationOperations, StorageDiffResult> CreateFunc(Type i_EntityType)
    {
      if (i_EntityType.GetProperty(nameof(IDomainEntity<object>.ID)) != null)
      {
        var idType = ExpressionCaches.PropertyTypeCache.Get(new PropertyTypeCacheKey()
        {
          DeclaringType = i_EntityType,
          PropertyName = nameof(IDomainEntity<object>.ID)
        });
        var conversationParamExpr = Expression.Parameter(typeof(NHConversation));
        var diffParamExpr = Expression.Parameter(typeof(StorageDiffResult));
        var objectParamExpr = Expression.Parameter(typeof(object));
        var castedObjectParamExpr = Expression.Convert(objectParamExpr, i_EntityType);
        var enumParamExpr = Expression.Parameter(typeof(ConversationOperations));
        var method = typeof(NHConversation).GetMethod(QUEUE_MESSAGE_FUNCTION, BindingFlags.Instance | BindingFlags.NonPublic);
        var genericMethod = method.MakeGenericMethod(i_EntityType, idType);
        var callMethod = Expression.Call(conversationParamExpr, genericMethod, castedObjectParamExpr, enumParamExpr, diffParamExpr);
        var lambdaexpr = Expression.Lambda<Action<NHConversation, object, ConversationOperations, StorageDiffResult>>(callMethod, conversationParamExpr, objectParamExpr, enumParamExpr, diffParamExpr);
        return lambdaexpr.Compile();
      }
      return null;
    }
  }
}
