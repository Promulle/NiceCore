﻿using NiceCore.Storage.Conversations;
using NiceCore.Storage.NH.Conversation.Initialization;

namespace NiceCore.Storage.NH.Conversation
{
  /// <summary>
  /// Interface for Conversations
  /// </summary>
  public interface INHConversation : IDatabaseConversation, IInitializableConversation<NHConversationInit>
  {
    /// <summary>
    /// Name of Conversation
    /// </summary>
    string Name { get; set; }
  }
}
