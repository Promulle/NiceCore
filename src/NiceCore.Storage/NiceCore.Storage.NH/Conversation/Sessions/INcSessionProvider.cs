using System.Threading;
using System.Threading.Tasks;
using NHibernate;
using NiceCore.Storage.Connections;

namespace NiceCore.Storage.NH.Conversation.Sessions
{
  /// <summary>
  /// Nc Session Provider
  /// </summary>
  public interface INcSessionProvider
  {
    /// <summary>
    /// Create Session
    /// </summary>
    /// <param name="i_Interceptor"></param>
    /// <param name="i_Connection"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    ValueTask<ISession> CreateSession(IInterceptor i_Interceptor, IConnectionInfo i_Connection, CancellationToken i_Token);

    /// <summary>
    /// Creates a new Stateless Session
    /// </summary>
    /// <returns></returns>
    ValueTask<IStatelessSession> CreateStatelessSession(IConnectionInfo i_Connection, CancellationToken i_Token);
  }
}