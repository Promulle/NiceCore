using System.Threading;
using System.Threading.Tasks;
using NHibernate;
using NiceCore.Storage.Connections;

namespace NiceCore.Storage.NH.Conversation.Sessions
{
  /// <summary>
  /// Session provider that relies on NHibernate to manage connections
  /// </summary>
  public sealed class NcNativeSessionProvider : BaseSessionProvider
  {
    /// <summary>
    /// Ctor filling the session factory
    /// </summary>
    /// <param name="i_Factory"></param>
    public NcNativeSessionProvider(ISessionFactory i_Factory) : base(i_Factory)
    {
    }

    /// <summary>
    /// Create a session that uses 
    /// </summary>
    /// <param name="i_Interceptor"></param>
    /// <param name="i_Connection"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    public override ValueTask<ISession> CreateSession(IInterceptor i_Interceptor, IConnectionInfo i_Connection, CancellationToken i_Token)
    {
      if (i_Interceptor == null)
      {
        return ValueTask.FromResult(m_SessionFactory.WithOptions().NoInterceptor().OpenSession());
      }

      return ValueTask.FromResult(m_SessionFactory.WithOptions().Interceptor(i_Interceptor).OpenSession());
    }

    /// <summary>
    /// Creates a stateless session
    /// </summary>
    /// <returns></returns>
    public override ValueTask<IStatelessSession> CreateStatelessSession(IConnectionInfo i_Connection, CancellationToken i_Token)
    {
      return ValueTask.FromResult(m_SessionFactory.OpenStatelessSession());
    }
  }
}