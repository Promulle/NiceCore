using System.Threading;
using System.Threading.Tasks;
using NHibernate;
using NiceCore.Storage.Connections;

namespace NiceCore.Storage.NH.Conversation.Sessions
{
  /// <summary>
  /// Base for session provider
  /// </summary>
  public abstract class BaseSessionProvider : INcSessionProvider
  {
    /// <summary>
    /// Session Factory to be used
    /// </summary>
    protected readonly ISessionFactory m_SessionFactory;

    /// <summary>
    /// Base ctor for session providers
    /// </summary>
    /// <param name="i_Factory"></param>
    protected BaseSessionProvider(ISessionFactory i_Factory)
    {
      m_SessionFactory = i_Factory;
    }

    /// <summary>
    /// Create Session
    /// </summary>
    /// <param name="i_Interceptor"></param>
    /// <param name="i_Connection"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    public abstract ValueTask<ISession> CreateSession(IInterceptor i_Interceptor, IConnectionInfo i_Connection, CancellationToken i_Token);

    /// <summary>
    /// Creates a new Stateless Session
    /// </summary>
    /// <returns></returns>
    public abstract ValueTask<IStatelessSession> CreateStatelessSession(IConnectionInfo i_Connection, CancellationToken i_Token);
  }
}