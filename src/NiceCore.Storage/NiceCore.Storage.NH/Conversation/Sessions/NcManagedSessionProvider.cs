using System.Threading;
using System.Threading.Tasks;
using NHibernate;
using NiceCore.Storage.Connections;

namespace NiceCore.Storage.NH.Conversation.Sessions
{
  /// <summary>
  /// Session Provider implementation that uses the session management of NiceCore itself
  /// </summary>
  public sealed class NcManagedSessionProvider : BaseSessionProvider
  {
    private readonly INcConnectionManagementService m_ManagementService;

    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_SessionFactory"></param>
    /// <param name="i_ManagementService"></param>
    public NcManagedSessionProvider(ISessionFactory i_SessionFactory, INcConnectionManagementService i_ManagementService): base(i_SessionFactory)
    {
      m_ManagementService = i_ManagementService;
    }

    /// <summary>
    /// Create Session
    /// </summary>
    /// <param name="i_Interceptor"></param>
    /// <param name="i_Connection"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    public override async ValueTask<ISession> CreateSession(IInterceptor i_Interceptor, IConnectionInfo i_Connection, CancellationToken i_Token)
    {
      var con = await m_ManagementService.GetConnection(i_Connection, null, i_Token).ConfigureAwait(false);
      if (i_Interceptor == null)
      {
        return m_SessionFactory.WithOptions().NoInterceptor().Connection(con).OpenSession();
      }

      return m_SessionFactory.WithOptions().Interceptor(i_Interceptor).Connection(con).OpenSession();
    }

    /// <summary>
    /// Create Stateless Session
    /// </summary>
    /// <param name="i_Connection"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    public override async ValueTask<IStatelessSession> CreateStatelessSession(IConnectionInfo i_Connection, CancellationToken i_Token)
    {
      var con = await m_ManagementService.GetConnection(i_Connection, null, i_Token).ConfigureAwait(false);
      return m_SessionFactory.OpenStatelessSession(con);
    }
  }
}