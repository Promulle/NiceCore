using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using NHibernate;
using NiceCore.Storage.Connections;

namespace NiceCore.Storage.NH.Conversation.Sessions
{
  /// <summary>
  /// Session provider that uses the user given connection for a new Session
  /// </summary>
  public sealed class UserProvidedConnectionSessionProvider : INcSessionProvider
  {
    private readonly ISessionFactory m_SessionFactory;
    private readonly DbConnection m_Connection;

    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_Factory"></param>
    /// <param name="i_Connection"></param>
    public UserProvidedConnectionSessionProvider(ISessionFactory i_Factory, DbConnection i_Connection)
    {
      m_SessionFactory = i_Factory;
      m_Connection = i_Connection;
    }

    /// <summary>
    /// Create Session
    /// </summary>
    /// <param name="i_Interceptor"></param>
    /// <param name="i_Connection"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    public ValueTask<ISession> CreateSession(IInterceptor i_Interceptor, IConnectionInfo i_Connection, CancellationToken i_Token)
    {
      if (i_Interceptor == null)
      {
        return ValueTask.FromResult(m_SessionFactory.WithOptions().NoInterceptor().Connection(m_Connection).OpenSession());
      }

      return ValueTask.FromResult(m_SessionFactory.WithOptions().Interceptor(i_Interceptor).Connection(m_Connection).OpenSession());
    }

    /// <summary>
    /// Create a new Stateless session
    /// </summary>
    /// <param name="i_Connection"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    public ValueTask<IStatelessSession> CreateStatelessSession(IConnectionInfo i_Connection, CancellationToken i_Token)
    {
      return ValueTask.FromResult(m_SessionFactory.OpenStatelessSession(m_Connection));
    }
  }
}