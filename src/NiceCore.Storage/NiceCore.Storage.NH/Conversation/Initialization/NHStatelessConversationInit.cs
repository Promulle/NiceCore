using System.Collections.Generic;
using NiceCore.Storage.Connections;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.NH.Conversation.Sessions;

namespace NiceCore.Storage.NH.Conversation.Initialization
{
  /// <summary>
  /// Stateless Conversation init
  /// </summary>
  public struct NHStatelessConversationInit
  {
    /// <summary>
    /// Session factory
    /// </summary>
    public required INcSessionProvider SessionProvider { get; init; }

    /// <summary>
    /// Context
    /// </summary>
    public required IConversationContext Context { get; init; }

    /// <summary>
    /// Query limiters
    /// </summary>
    public required IEnumerable<IQueryLimiter> QueryLimiters { get; init; }
    
    /// <summary>
    /// Connection Info
    /// </summary>
    public required IConnectionInfo ConnectionInfo { get; init; }

    /// <summary>
    /// Conversation Name
    /// </summary>
    public required string ConversationName { get; init; }
    
    /// <summary>
    /// Batch Size
    /// </summary>
    public required int? BatchSize { get; init; }
  }
}