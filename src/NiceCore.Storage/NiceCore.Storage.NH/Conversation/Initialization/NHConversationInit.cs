using System.Collections.Generic;
using NHibernate;
using NiceCore.Eventing.Messaging.Parameters;
using NiceCore.Storage.Connections;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Conversations.EventHooks;
using NiceCore.Storage.Conversations.Queries;
using NiceCore.Storage.Messages;
using NiceCore.Storage.Messages.Providers;
using NiceCore.Storage.NH.Conversation.Sessions;
using NiceCore.Storage.NH.Diffing;

namespace NiceCore.Storage.NH.Conversation.Initialization
{
  /// <summary>
  /// Data used to init a conversation
  /// </summary>
  public struct NHConversationInit
  {
    /// <summary>
    /// Session factory
    /// </summary>
    public INcSessionProvider SessionProvider { get; init; }

    /// <summary>
    /// Strict mode
    /// </summary>
    public bool StrictMode { get; init; }
    
    /// <summary>
    /// Retry strategy
    /// </summary>
    public IDefaultConnectionRetryStrategy RetryStrategy { get; init; }
    
    /// <summary>
    /// Context
    /// </summary>
    public IConversationContext Context { get; init; }
    
    /// <summary>
    /// Message conversation context
    /// </summary>
    public IMessageSendingParameters MessageSendingParameters { get; init; }
    
    /// <summary>
    /// Use transaction
    /// </summary>
    public TransactionHandlingMode TransactionHandlingMode { get; init; }
    
    /// <summary>
    /// Query limiters
    /// </summary>
    public IEnumerable<IQueryLimiter> QueryLimiters { get; init; }
    
    /// <summary>
    /// Entity modifiers
    /// </summary>
    public IDictionary<EntityModifyOperations, List<IEntityOperationModifier>> EntityModifiers { get; init; }
    
    /// <summary>
    /// Loaded Entity modifiers
    /// </summary>
    public List<ILoadedEntityModifier> LoadedEntityModifiers { get; init; }

    /// <summary>
    /// Conversation Name
    /// </summary>
    public string ConversationName { get; init; }
    
    /// <summary>
    /// Message Providers
    /// </summary>
    public IEnumerable<IStorageMessageProvider> MessageProviders { get; init; }
    
    /// <summary>
    /// Differ Service
    /// </summary>
    public INHDifferService DifferService { get; init; }
    
    /// <summary>
    /// Connection Info
    /// </summary>
    public IConnectionInfo ConnectionInfo { get; init; }
    
    /// <summary>
    /// Storage Message Sender
    /// </summary>
    public IStorageMessageSender StorageMessageSender { get; init; }
  }
}
