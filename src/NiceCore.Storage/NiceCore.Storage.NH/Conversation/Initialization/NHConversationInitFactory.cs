using System.Collections.Generic;
using System.Linq;
using NiceCore.Eventing.Messaging.Parameters;
using NiceCore.Extensions;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using NiceCore.Storage.Connections;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Conversations.EventHooks;
using NiceCore.Storage.Conversations.Factories;
using NiceCore.Storage.Conversations.Queries;
using NiceCore.Storage.Messages;
using NiceCore.Storage.Messages.Providers;
using NiceCore.Storage.NH.Config.AvailableSettings;
using NiceCore.Storage.NH.Conversation.Sessions;
using NiceCore.Storage.NH.Diffing;

namespace NiceCore.Storage.NH.Conversation.Initialization
{
  /// <summary>
  /// Class used to create init configurations for a nh conversation
  /// </summary>
  [Register(InterfaceType = typeof(INHConversationInitFactory))]
  public class NHConversationInitFactory : INHConversationInitFactory
  {
    /// <summary>
    /// Container
    /// </summary>
    public IServiceContainer Container { get; set; }

    /// <summary>
    /// Configuration
    /// </summary>
    public INcConfiguration Configuration { get; set; }

    /// <summary>
    /// Differ Service
    /// </summary>
    public INHDifferService DifferService { get; set; }

    NHConversationInit IInitProvider<NHConversationInit>.ProvideInit(INcSessionProvider i_NcSessionProvider, IConnectionInfo i_Info, ConversationCreationParameters i_Parameters)
    {
      return NHConversationInitFactory.CreateDefaultInit(i_NcSessionProvider,
        i_Parameters.Name,
        i_Info,
        i_Parameters.Context,
        i_Parameters.MessageSendingParameters,
        i_Parameters.ConversationConnectionRetryStrategy,
        Container,
        Configuration,
        DifferService,
        i_Parameters.StorageMessageSender,
        i_Parameters.TransactionHandlingMode);
    }

    NHStatelessConversationInit IInitProvider<NHStatelessConversationInit>.ProvideInit(INcSessionProvider i_NcSessionProvider, IConnectionInfo i_Info, ConversationCreationParameters i_Parameters)
    {
      return NHConversationInitFactory.CreateStatelessInit(i_NcSessionProvider, i_Parameters.Name, i_Info, i_Parameters.Context, Container, i_Parameters.BatchSizeForStateless);
    }

    /// <summary>
    /// Create the default init
    /// </summary>
    /// <param name="i_SessionProvider"></param>
    /// <param name="i_Name"></param>
    /// <param name="i_ConnectionInfo"></param>
    /// <param name="i_ConversationContext"></param>
    /// <param name="i_MessageSendingParameters"></param>
    /// <param name="i_RetryStrategy"></param>
    /// <param name="i_Container"></param>
    /// <param name="i_Configuration"></param>
    /// <param name="i_DifferService"></param>
    /// <param name="i_StorageMessageSender"></param>
    /// <param name="i_TransactionHandlingMode"></param>
    /// <returns></returns>
    public static NHConversationInit CreateDefaultInit(INcSessionProvider i_SessionProvider,
      string i_Name,
      IConnectionInfo i_ConnectionInfo,
      IConversationContext i_ConversationContext,
      IMessageSendingParameters i_MessageSendingParameters,
      IDefaultConnectionRetryStrategy i_RetryStrategy,
      IServiceContainer i_Container,
      INcConfiguration i_Configuration,
      INHDifferService i_DifferService,
      IStorageMessageSender i_StorageMessageSender,
      TransactionHandlingMode i_TransactionHandlingMode)
    {
      return new()
      {
        QueryLimiters = GetLimiters(i_Container),
        ConversationName = i_Name,
        SessionProvider = i_SessionProvider,
        ConnectionInfo = i_ConnectionInfo,
        EntityModifiers = GetEntityModifiers(i_Container),
        LoadedEntityModifiers = GetLoadedEntityModifiers(i_Container),
        StrictMode = GetStrictModeSetting(i_Configuration),
        RetryStrategy = i_RetryStrategy,
        TransactionHandlingMode = i_TransactionHandlingMode,
        Context = i_ConversationContext,
        MessageSendingParameters = i_MessageSendingParameters,
        MessageProviders = i_Container.ResolveAll<IStorageMessageProvider>(),
        DifferService = i_DifferService,
        StorageMessageSender = i_StorageMessageSender
      };
    }

    /// <summary>
    /// Creates an init instance for a stateless Conversation
    /// </summary>
    /// <param name="i_SessionProvider"></param>
    /// <param name="i_Name"></param>
    /// <param name="i_ConnectionInfo"></param>
    /// <param name="i_Context"></param>
    /// <param name="i_Container"></param>
    /// <param name="i_BatchSize"></param>
    /// <returns></returns>
    public static NHStatelessConversationInit CreateStatelessInit(INcSessionProvider i_SessionProvider,
      string i_Name,
      IConnectionInfo i_ConnectionInfo,
      IConversationContext i_Context,
      IServiceContainer i_Container,
      int? i_BatchSize)
    {
      return new NHStatelessConversationInit()
      {
        Context = i_Context,
        ConversationName = i_Name,
        SessionProvider = i_SessionProvider,
        ConnectionInfo = i_ConnectionInfo,
        QueryLimiters = GetLimiters(i_Container),
        BatchSize = i_BatchSize
      };
    }

    private static List<ILoadedEntityModifier> GetLoadedEntityModifiers(IServiceContainer i_Container)
    {
      return i_Container.ResolveAll<ILoadedEntityModifier>().ToList();
    }

    private static bool GetStrictModeSetting(INcConfiguration i_Configuration)
    {
      if (i_Configuration != null)
      {
        return i_Configuration.GetValue(ConversationStrictModeSetting.KEY, false);
      }

      return false;
    }

    private static IDictionary<EntityModifyOperations, List<IEntityOperationModifier>> GetEntityModifiers(IServiceContainer i_Container)
    {
      var resolvedModifiers = i_Container.ResolveAll<IEntityOperationModifier>();
      var dict = new Dictionary<EntityModifyOperations, List<IEntityOperationModifier>>();
      foreach (var modifier in resolvedModifiers)
      {
        modifier.Operations.ForEachItem(operation =>
        {
          if (!dict.TryGetValue(operation, out var list))
          {
            list = new List<IEntityOperationModifier>();
            dict[operation] = list;
          }

          list.Add(modifier);
        });
      }

      return dict;
    }

    private static IEnumerable<IQueryLimiter> GetLimiters(IServiceContainer i_Container)
    {
      var resolvedLimiters = i_Container.ResolveAll<IQueryLimiter>();
      var allLimiters = new List<IQueryLimiter>();
      foreach (var limiter in resolvedLimiters)
      {
        if (!allLimiters.Contains(limiter))
        {
          allLimiters.Add(limiter);
        }

        //release limiter so only the conversation will have a reference to it
        //conversation disposes -> limiter is collected (hopefully)
        i_Container.Release(limiter);
      }

      return allLimiters;
    }
  }
}