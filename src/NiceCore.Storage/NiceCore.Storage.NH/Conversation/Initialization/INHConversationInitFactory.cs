namespace NiceCore.Storage.NH.Conversation.Initialization
{
  /// <summary>
  /// Interface for NH Conversation Init Factory
  /// </summary>
  public interface INHConversationInitFactory : IInitProvider<NHConversationInit>, IInitProvider<NHStatelessConversationInit>
  {
    
  }
}