using NiceCore.Storage.Connections;
using NiceCore.Storage.Conversations.Factories;
using NiceCore.Storage.NH.Conversation.Sessions;

namespace NiceCore.Storage.NH.Conversation.Initialization
{
  /// <summary>
  /// Init Provider
  /// </summary>
  /// <typeparam name="TInit"></typeparam>
  public interface IInitProvider<TInit>
  {
    /// <summary>
    /// Provide Init
    /// </summary>
    /// <param name="i_NcSessionProvider"></param>
    /// <param name="i_Info"></param>
    /// <param name="i_Parameters"></param>
    /// <returns></returns>
    TInit ProvideInit(INcSessionProvider i_NcSessionProvider,
      IConnectionInfo i_Info,
      ConversationCreationParameters i_Parameters);
  }
}