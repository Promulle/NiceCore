﻿using System;
using System.Data.Common;
using NiceCore.Base.Services;
using NiceCore.ServiceLocation;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Fetching;
using NiceCore.Storage.NH.Fetching;
using NiceCore.Storage.NH.Queries;
using NiceCore.Storage.NH.SessionFactories;
using NiceCore.Storage.Queries;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NHibernate;
using NiceCore.Eventing.Messaging.Parameters;
using NiceCore.Logging.Extensions;
using NiceCore.Storage.Connections;
using NiceCore.Storage.Conversations.Factories;
using NiceCore.Storage.Conversations.Factories.Extensions;
using NiceCore.Storage.NH.Config.Services;
using NiceCore.Storage.NH.Connections;
using NiceCore.Storage.NH.Logging;
using NiceCore.Storage.Services;
using ILoggerFactory = Microsoft.Extensions.Logging.ILoggerFactory;

namespace NiceCore.Storage.NH
{
  /// <summary>
  /// NH based implementation for IStorageService
  /// </summary>
  [Register(InterfaceType = typeof(IStorageService), LifeStyle = LifeStyles.Singleton)]
  public class NHStorageService : BaseService, IStorageService
  {
    private readonly SemaphoreSlim m_LockObject = new(1, 1);
    private bool m_StorageServiceInitialized = false; //whether the storage service is initialized or not (needed to guarantee thread safety)
    
    /// <summary>
    /// Conversation Factory
    /// </summary>
    public IConversationFactory ConversationFactory { get; set; }

    /// <summary>
    /// Configservice for reading of appconfig
    /// </summary>
    public INHConfigService NhConfigService { get; set; }

    /// <summary>
    /// Whether Initialize initializes all session factories. Default: false
    /// </summary>
    public bool EnableFullInitialize { get; set; }

    /// <summary>
    /// Container for managing registrations
    /// </summary>
    public IServiceContainer Container { get; set; }

    /// <summary>
    /// Session Factory service
    /// </summary>
    public ISessionFactoriesService SessionFactoryService { get; set; }

    /// <summary>
    /// Strategy that should be used when creating a connection errors out
    /// </summary>
    public IDefaultConnectionRetryStrategy ConnectionRetryStrategy { get; set; }

    /// <summary>
    /// Connection definition management service
    /// </summary>
    public IConnectionDefinitionManagementService ConnectionDefinitionManagementService { get; set; }
    
    /// <summary>
    /// Connection Management Service
    /// </summary>
    public INcConnectionManagementService NcConnectionManagementService { get; set; }
    
    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<NHStorageService> Logger { get; set; }
    
    /// <summary>
    /// Logger Factory
    /// </summary>
    public ILoggerFactory LoggerFactory { get; set; }

    /// <summary>
    /// returns a conversation, if there is a configured Connection for i_Name
    /// </summary>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    [Obsolete("Use ConversationFactory.Conversation instead", false)]
    public IConversation Conversation(string i_Name)
    {
      return Conversation(i_Name, null);
    }

    /// <summary>
    /// returns a conversation, if there is a configured Connection for i_Name
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Context">Context to create new conversation under. may be null</param>
    /// <returns></returns>
    [Obsolete("Use ConversationFactory.Conversation instead", false)]
    public IConversation Conversation(string i_Name, IConversationContext i_Context)
    {
      return ConversationFactory.Conversation(i_Name, i_Context: i_Context).GetAwaiter().GetResult();
    }

    /// <summary>
    /// returns a conversation, if there is a configured Connection for i_Name
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Context">Context to create new conversation under. may be null</param>
    /// <param name="i_MessageContext"></param>
    /// <returns></returns>
    [Obsolete("Use ConversationFactory.Conversation instead", false)]
    public IConversation Conversation(string i_Name, IConversationContext i_Context, IMessageSendingParameters i_MessageContext)
    {
      return ConversationFactory.Conversation(i_Name, i_Context: i_Context, i_SendingParameters: i_MessageContext).GetAwaiter().GetResult();
    }
    
    /// <summary>
    /// Returns a connection for the definition found at i_Name
    /// </summary>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    public Task<DbConnection> GetConnection(string i_Name)
    {
      return GetConnection(i_Name, CancellationToken.None);
    }

    /// <summary>
    /// Get Connection Definition Name For Entity
    /// </summary>
    /// <param name="i_Type"></param>
    /// <returns></returns>
    public async ValueTask<string> GetConnectionDefinitionNameForEntity(Type i_Type)
    {
      //ensure all available factories are initialized
      await SessionFactoryService.InitializeAllFactories().ConfigureAwait(false);
      return await SessionFactoryService.GetConnectionForEntityType(i_Type).ConfigureAwait(false);
    }

    /// <summary>
    /// check whether i_Type is mapped for i_ConnectionDefinition
    /// </summary>
    /// <param name="i_Type"></param>
    /// <param name="i_ConnectionDefinitionName"></param>
    /// <returns></returns>
    public async ValueTask<bool> IsEntityTypeMappedFor(Type i_Type, string i_ConnectionDefinitionName)
    {
      var factory = await SessionFactoryService.GetOrInitializeFactory(i_ConnectionDefinitionName, null, false).ConfigureAwait(false);
      return factory.SessionFactory.GetClassMetadata(i_Type) != null;
    }

    /// <summary>
    /// Returns a connection for the definition found at i_Name
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    public async Task<DbConnection> GetConnection(string i_Name, CancellationToken i_Token)
    {
      //TODO: reintroduce retry strategy
      var sessionInfo = await SessionFactoryService.GetOrInitializeFactory(i_Name, null, false).ConfigureAwait(false);
      return await NcConnectionManagementService.GetConnection(sessionInfo.ConnectionInfo, null, i_Token).ConfigureAwait(false);
    }

    /// <summary>
    /// Initializes storage service
    /// </summary>
    protected override void InternalInitialize()
    {
      m_LockObject.Wait();
      try
      {
        if (!m_StorageServiceInitialized)
        {
          Logger.Information("Initializing storage service.");
          base.InternalInitialize();
          NHibernateLogger.SetLoggersFactory(new NHNcLoggerFactory(LoggerFactory));
          LoadConnections();
          //if needed, initialize all factories
          if (EnableFullInitialize)
          {
            //Initialize does not yet support async so we need this for now.
            SessionFactoryService.InitializeAllFactories().GetAwaiter().GetResult();
          }

          FetchRequests.Provider = new NHFetchProviderImpl();
          AsyncQueries.AsyncQueryProvider = new NHAsyncQueryProvider();
          m_StorageServiceInitialized = true;
          Logger.Information("Storage service successfully initialized.");
        }
      }
      finally
      {
        m_LockObject.Release();
      }
    }

    /// <summary>
    /// Loads all Connections
    /// </summary>
    public void LoadConnections()
    {
      foreach (var conn in NhConfigService.GetConfiguredConnections())
      {
        Logger.Information($"Found connection {conn.Name}");
        ConnectionDefinitionManagementService.AddConnectionDefinition(conn, false);
      }
    }
    
    /// <summary>
    /// Returns the connection string for i_ConnectionName
    /// </summary>
    /// <param name="i_ConnectionName"></param>
    /// <returns></returns>
    public async Task<string> GetConnectionStringForConnection(string i_ConnectionName)
    {
      var conInfo = await GetConnectionInfo(i_ConnectionName).ConfigureAwait(false);
      return conInfo.ConnectionString;
    }

    /// <summary>
    /// Returns the driver used to create connections for the specified connection. Might not be supported on all implementations
    /// </summary>
    /// <param name="i_ConnectionName"></param>
    /// <returns></returns>
    public async Task<string> GetConnectionDriverForConnection(string i_ConnectionName)
    {
      var conInfo = await GetConnectionInfo(i_ConnectionName).ConfigureAwait(false);
      if (conInfo is NHConnectionInfo NhConInfo)
      {
        return NhConInfo.DriverClass;
      }

      throw new InvalidOperationException($"ConnectionInfo of type {conInfo.GetType()} was expected to be of type {typeof(NHConnectionInfo).FullName}");
    }

    /// <summary>
    /// Get the connection info
    /// </summary>
    /// <param name="i_ConnectionName"></param>
    /// <returns></returns>
    public async Task<IConnectionInfo> GetConnectionInfo(string i_ConnectionName)
    {
      var res = await SessionFactoryService.GetOrInitializeFactory(i_ConnectionName, null, false).ConfigureAwait(false);
      return res.ConnectionInfo;
    }
    
    
  }
}