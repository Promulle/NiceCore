using System;
using NHibernate;
using ILoggerFactory = Microsoft.Extensions.Logging.ILoggerFactory;

namespace NiceCore.Storage.NH.Logging
{
  /// <summary>
  /// NH nc Logger Factory
  /// </summary>
  public sealed class NHNcLoggerFactory : INHibernateLoggerFactory
  {
    private readonly ILoggerFactory m_Factory;
    
    /// <summary>
    /// Create a nh Logger factory that passes arguments to microsoft.extensions.logging factory
    /// </summary>
    /// <param name="i_Factory"></param>
    public NHNcLoggerFactory(ILoggerFactory i_Factory)
    {
      m_Factory = i_Factory;
    }
    
    /// <summary>
    /// Logger For Name
    /// </summary>
    /// <param name="keyName"></param>
    /// <returns></returns>
    public INHibernateLogger LoggerFor(string keyName)
    {
      return new NHNcLogger(m_Factory.CreateLogger(keyName));
    }

    /// <summary>
    /// Logger for type
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public INHibernateLogger LoggerFor(Type type)
    {
      return new NHNcLogger(m_Factory.CreateLogger(type?.FullName ?? string.Empty));
    }
  }
}