using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using NHibernate;

namespace NiceCore.Storage.NH.Logging
{
  /// <summary>
  /// NH Nc Logger
  /// </summary>
  public sealed class NHNcLogger : INHibernateLogger
  {
    private readonly ILogger m_Logger;

    private static readonly Dictionary<NHibernateLogLevel, LogLevel> m_LogLevelMap = new()
    {
      {NHibernateLogLevel.Debug, LogLevel.Debug},
      {NHibernateLogLevel.Error, LogLevel.Error},
      {NHibernateLogLevel.Fatal, LogLevel.Critical},
      {NHibernateLogLevel.Info, LogLevel.Information},
      {NHibernateLogLevel.Trace, LogLevel.Trace},
      {NHibernateLogLevel.Warn, LogLevel.Warning},
      {NHibernateLogLevel.None, LogLevel.None}
    };

    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_Logger"></param>
    public NHNcLogger(ILogger i_Logger)
    {
      m_Logger = i_Logger;
    }
    
    /// <summary>
    /// Log
    /// </summary>
    /// <param name="logLevel"></param>
    /// <param name="state"></param>
    /// <param name="exception"></param>
    public void Log(NHibernateLogLevel logLevel, NHibernateLogValues state, Exception exception)
    {
      m_Logger.Log(m_LogLevelMap[logLevel], exception, state.Format, state.Args);
    }

    /// <summary>
    /// is Enabled
    /// </summary>
    /// <param name="logLevel"></param>
    /// <returns></returns>
    public bool IsEnabled(NHibernateLogLevel logLevel)
    {
      return m_Logger.IsEnabled(m_LogLevelMap[logLevel]);
    }
  }
}