using NiceCore.Storage.NH.Conversation;

namespace NiceCore.Storage.NH.Interceptors
{
  /// <summary>
  /// A single Background Task in the Interceptor
  /// </summary>
  public struct InterceptorBackgroundTask
  {
    /// <summary>
    /// Instance
    /// </summary>
    public object Entity { get; init; }
    
    /// <summary>
    /// Properties as Array, new values
    /// </summary>
    public object[] NewState { get; init; }
    
    /// <summary>
    /// Properties as Array, old Values 
    /// </summary>
    public object[] OldState { get; init; }
    
    /// <summary>
    /// Names of properties
    /// </summary>
    public string[] PropertyNames { get; init; }
    
    /// <summary>
    /// Operation
    /// </summary>
    public ConversationOperations Operation { get; init; }
  }
}