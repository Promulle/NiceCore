using NiceCore.Storage.Connections;

namespace NiceCore.Storage.NH.Connections
{
  /// <summary>
  /// NH Connection Info
  /// </summary>
  public sealed class NHConnectionInfo : IConnectionInfo 
  {
    /// <summary>
    /// Enable Auditing
    /// </summary>
    public bool EnableAuditing { get; init; }
    
    /// <summary>
    /// Connection Name
    /// </summary>
    public string ConnectionName { get; init; }
    
    /// <summary>
    /// Connection Type
    /// </summary>
    public string ConnectionType { get; init; }
    
    /// <summary>
    /// Connection String
    /// </summary>
    public string ConnectionString { get; init; }
    
    /// <summary>
    /// Driver class
    /// </summary>
    public string DriverClass { get; init; }
  }
}