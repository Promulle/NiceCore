﻿using NHibernate.Linq;
using Remotion.Linq;
using System.Linq;

namespace NiceCore.Storage.NH.Fetching
{
  /// <summary>
  /// NH impl of Fetch request
  /// </summary>
  internal class NHFetchRequest<TQueried, TFetched> : QueryableBase<TQueried>, IFetchRequest<TQueried, TFetched>
  {
    /// <summary>
    /// Nh-request
    /// </summary>
    public INhFetchRequest<TQueried, TFetched> InnerRequest { get; set; }

    /// <summary>
    /// Default cosntructor
    /// </summary>
    public NHFetchRequest(IQueryable<TQueried> i_Query) : base(i_Query.Provider, i_Query.Expression)
    {
    }
  }
}
