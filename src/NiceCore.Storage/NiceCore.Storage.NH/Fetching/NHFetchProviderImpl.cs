﻿using NHibernate.Linq;
using NiceCore.Storage.Fetching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace NiceCore.Storage.NH.Fetching
{
  /// <summary>
  /// Implementation for IFetchProvider
  /// </summary>
  public class NHFetchProviderImpl : IFetchRequestProvider
  {
    /// <summary>
    /// Returns a new Fetch request
    /// </summary>
    /// <typeparam name="TQueried"></typeparam>
    /// <typeparam name="TFetched"></typeparam>
    /// <returns></returns>
    public IFetchRequest<TQueried, TFetched> GetFetchRequest<TQueried, TFetched>(IQueryable<TQueried> i_Query, Expression<Func<TQueried, TFetched>> i_Selector)
    {
      var nhInternalFetch = EagerFetchingExtensionMethods.Fetch(i_Query, i_Selector);
      return new NHFetchRequest<TQueried, TFetched>(nhInternalFetch) { InnerRequest = nhInternalFetch };
    }

    /// <summary>
    /// Returns a new request for fetching a collection
    /// </summary>
    /// <typeparam name="TQueried"></typeparam>
    /// <typeparam name="TFetched"></typeparam>
    /// <param name="i_Query"></param>
    /// <param name="i_Selector"></param>
    /// <returns></returns>
    public IFetchRequest<TQueried, TFetched> GetFetchManyRequest<TQueried, TFetched>(IQueryable<TQueried> i_Query, Expression<Func<TQueried, IEnumerable<TFetched>>> i_Selector)
    {
      var nhInternalFetch = EagerFetchingExtensionMethods.FetchMany(i_Query, i_Selector);
      return new NHFetchRequest<TQueried, TFetched>(nhInternalFetch) { InnerRequest = nhInternalFetch };
    }

    /// <summary>
    /// Returns a ThenFetch - Request
    /// </summary>
    /// <typeparam name="TQueried"></typeparam>
    /// <typeparam name="TAlreadyFetched"></typeparam>
    /// <typeparam name="TFetched"></typeparam>
    /// <param name="i_Query"></param>
    /// <param name="i_Selector"></param>
    /// <returns></returns>
    public IFetchRequest<TQueried, TFetched> GetThenFetchRequest<TQueried, TAlreadyFetched, TFetched>(IFetchRequest<TQueried, TAlreadyFetched> i_Query, Expression<Func<TAlreadyFetched, TFetched>> i_Selector)
    {
      //if you go to a "ThenFetch" which is implemented here but i_Query is not of type INhFetchRequest something is wrong
      //so i assume that i_Query is that
      var nhInternalFetch = EagerFetchingExtensionMethods.ThenFetch((i_Query as NHFetchRequest<TQueried, TAlreadyFetched>).InnerRequest, i_Selector);
      return new NHFetchRequest<TQueried, TFetched>(nhInternalFetch) { InnerRequest = nhInternalFetch };
    }

    /// <summary>
    /// Returns a fetch request for ThenFetchMany
    /// </summary>
    /// <typeparam name="TQueried"></typeparam>
    /// <typeparam name="TAlreadyFetched"></typeparam>
    /// <typeparam name="TFetched"></typeparam>
    /// <param name="i_Query"></param>
    /// <param name="i_Selector"></param>
    /// <returns></returns>
    public IFetchRequest<TQueried, TFetched> GetThenFetchManyRequest<TQueried, TAlreadyFetched, TFetched>(IFetchRequest<TQueried, TAlreadyFetched> i_Query, Expression<Func<TAlreadyFetched, IEnumerable<TFetched>>> i_Selector)
    {
      //if you go to a "ThenFetchMany" which is implemented here but i_Query is not of type INhFetchRequest something is wrong
      //so i assume that i_Query is that
      var nhInternalFetch = EagerFetchingExtensionMethods.ThenFetchMany((i_Query as NHFetchRequest<TQueried, TAlreadyFetched>).InnerRequest, i_Selector);
      return new NHFetchRequest<TQueried, TFetched>(nhInternalFetch) { InnerRequest = nhInternalFetch };
    }
  }
}
