using NHibernate.Linq.Functions;

namespace NiceCore.Storage.NH
{
  /// <summary>
  /// Generators registry for nc linq functions
  /// </summary>
  public class NcLinqToHqlGeneratorsRegistry :DefaultLinqToHqlGeneratorsRegistry
  {
    /// <summary>
    /// Ctor
    /// </summary>
    public NcLinqToHqlGeneratorsRegistry()
    {
      // ReSharper disable once VirtualMemberCallInConstructor - NH Does it the same way
      RegisterGenerator(typeof(NcNhLinqExtensions).GetMethod(nameof(NcNhLinqExtensions.AsDateTime)), new  MethodParameterAsNodeGenerator());
    }
  }
}