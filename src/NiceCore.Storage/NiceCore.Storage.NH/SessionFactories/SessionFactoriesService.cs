using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Base;
using NiceCore.Exceptions;
using NiceCore.Extensions;
using NiceCore.Logging;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation;
using NiceCore.ServiceLocation.Helpers;
using NiceCore.Storage.DomainBuilder;
using NiceCore.Storage.NH.Exceptions;
using NiceCore.Storage.NH.SessionFactories.Creation;
using NiceCore.Storage.NH.SessionFactories.Model;
using NiceCore.Storage.Services;

namespace NiceCore.Storage.NH.SessionFactories
{
  /// <summary>
  /// Session Factories Service
  /// </summary>
  [Register(InterfaceType = typeof(ISessionFactoriesService), LifeStyle = LifeStyles.Singleton)]
  public class SessionFactoriesService : BaseDisposable, ISessionFactoriesService
  {
    private readonly SemaphoreSlim m_LockObject = new(1, 1);
    /// <summary>
    /// Sessionfactories mapped by ConnectionName
    /// </summary>
    protected Dictionary<string, SessionCreationInfo> m_SessionFactories = new();
    
    /// <summary>
    /// Container
    /// </summary>
    public IServiceContainer Container { get; set; }
    
    /// <summary>
    /// Connection Definition Management Service
    /// </summary>
    public IConnectionDefinitionManagementService ConnectionDefinitionManagementService { get; set; }
    
    /// <summary>
    /// Create Session Factories Service
    /// </summary>
    public ICreateSessionFactoriesService CreateSessionFactoriesService { get; set; }
    
    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<SessionFactoriesService> Logger { get; set; }

    /// <summary>
    /// Uses m_FoundConnections to build corresponing Nhibernate SessionFactories
    /// </summary>
    public Task InitializeAllFactories()
    {
      return ConnectionDefinitionManagementService.GetAllConnectionDefinitions<ConnectionDefinition>()
        .ForEachItem(r => GetOrInitializeFactory(r.Name, null, true));
    }

    /// <summary>
    /// Builds single factory according to i_ConnectionName. Only if not already built
    /// </summary>
    /// <param name="i_ConnectionName"></param>
    /// <param name="i_RetryStrategy"></param>
    /// <param name="i_AllowSkip"></param>
    public async Task<SessionCreationInfo> GetOrInitializeFactory(string i_ConnectionName, IDefaultConnectionRetryStrategy i_RetryStrategy, bool i_AllowSkip)
    {
      await m_LockObject.WaitAsync().ConfigureAwait(false);
      try
      {
        if (!m_SessionFactories.ContainsKey(i_ConnectionName))
        {
          var definition = ConnectionDefinitionManagementService.GetConnectionDefinition<ConnectionDefinition>(i_ConnectionName);
          if (definition != null)
          {
            Logger.Information($"No Sessionfactory found for {i_ConnectionName}. Creating/loading new.");
            //CreateOrLoad either loads serialized sessionfactory or creates a new one and decides to run schema update or not
            using (var domainBuilder = Container.LocateAll<IDomainBuilder>())
            {
              try
              {
                var sessionFactoryInfo = await CreateSessionFactoriesService.CreateOrLoad(definition, domainBuilder.Instances().Where(r => r.IsApplicable(i_ConnectionName)), i_RetryStrategy, null).ConfigureAwait(false);
                m_SessionFactories.Add(i_ConnectionName, sessionFactoryInfo);
                return sessionFactoryInfo;
              }
              catch (Exception ex)
              {
                if (!i_AllowSkip)
                {
                  throw;
                }
                Logger.Information($"An exception occurred building the SessionFactory for connection {i_ConnectionName}: {ExceptionUtils.FormatException(ex)}. But {nameof(i_AllowSkip)} was true, so returning null");
                return null;
              }
              
            }
          }
          else
          {
            throw new ConnectionNotDefinedException(i_ConnectionName);
          }
        }
        else
        {
          Logger.Information($"Sessionfactory for connection {i_ConnectionName} already exists. Doing nothing.");
        }
      }
      finally
      {
        m_LockObject.Release();
      }

      return m_SessionFactories[i_ConnectionName];
    }

    /// <summary>
    /// Get Connection For Entity Type
    /// </summary>
    /// <param name="i_Type"></param>
    /// <returns></returns>
    public async ValueTask<string> GetConnectionForEntityType(Type i_Type)
    {
      await m_LockObject.WaitAsync().ConfigureAwait(false);
      try
      {
        if (m_SessionFactories == null)
        {
          return null;
        }
        foreach (var (key, sessionFactoryInfo) in m_SessionFactories)
        {
          var metadata = sessionFactoryInfo.SessionFactory.GetClassMetadata(i_Type);
          if (metadata != null)
          {
            return key;
          }
        }
      }
      finally
      {
        m_LockObject.Release();
      }
      return null;
    }

    /// <summary>
    /// Cleanup of resources
    /// </summary>
    public void Cleanup()
    {
      m_LockObject.Wait();
      try
      {
        InternalCleanup();
      }
      finally
      {
        m_LockObject.Release();
      }
    }

    private void InternalCleanup()
    {
      if (m_SessionFactories != null)
      {
        Logger.Information($"Clearing {m_SessionFactories.Count} session factory instances.");
        m_SessionFactories.ToList().ForEach(r => r.Value.SessionFactory.Dispose());
        m_SessionFactories.Clear();
      }
    }

    /// <summary>
    /// Dispose
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    protected override void Dispose(bool i_IsDisposing)
    {
      if (i_IsDisposing)
      {
        Cleanup();
      }
    }
  }
}