using System;
using System.Threading.Tasks;
using NiceCore.Storage.NH.SessionFactories.Model;

namespace NiceCore.Storage.NH.SessionFactories
{
  /// <summary>
  /// Session Factories Service
  /// </summary>
  public interface ISessionFactoriesService
  {
    /// <summary>
    /// Uses m_FoundConnections to build corresponing Nhibernate SessionFactories
    /// </summary>
    Task InitializeAllFactories();

    /// <summary>
    /// Builds single factory according to i_ConnectionName. Only if not already built
    /// </summary>
    /// <param name="i_ConnectionName"></param>
    /// <param name="i_RetryStrategy"></param>
    /// <param name="i_AllowSkip"></param>
    Task<SessionCreationInfo> GetOrInitializeFactory(string i_ConnectionName, IDefaultConnectionRetryStrategy i_RetryStrategy, bool i_AllowSkip);

    /// <summary>
    /// Get Connection For EntityType
    /// </summary>
    /// <param name="i_Type"></param>
    /// <returns></returns>
    ValueTask<string> GetConnectionForEntityType(Type i_Type);
  }
}