﻿using NHibernate.Envers.Configuration;
using NHibernate.Mapping;

namespace NiceCore.Storage.NH.SessionFactories
{
  /// <summary>
  /// Custom envers naming strategy, stripping \" from the nhconfig registered name
  /// </summary>
  public class CustomEnversNamingStrategy : DefaultNamingStrategy
  {
    private const string SUFFIX = "_AUD";

    /// <summary>
    /// Audit that fixes the table name
    /// </summary>
    /// <param name="i_PersistentClass"></param>
    /// <returns></returns>
    public override string AuditTableName(PersistentClass i_PersistentClass)
    {
      return GetAuditTableName(i_PersistentClass.Table.Name);
    }

    /// <summary>
    /// Audit that fixes table name for join
    /// </summary>
    /// <param name="i_OriginalJoin"></param>
    /// <returns></returns>
    public override string JoinTableName(Join i_OriginalJoin)
    {
      return GetAuditTableName(i_OriginalJoin.Table.Name);
    }

    /// <summary>
    /// Name of the collection table for audit fixed
    /// </summary>
    /// <param name="i_OriginalCollection"></param>
    /// <returns></returns>
    public override string CollectionTableName(Collection i_OriginalCollection)
    {
      return GetAuditTableName(i_OriginalCollection.Table.Name);
    }

    private static string GetAuditTableName(string i_TableName)
    {
      return $"\"{FixTableName(i_TableName)}{SUFFIX}\"";
    }

    private static string FixTableName(string i_TableName)
    {
      return i_TableName.Replace("\"", "");
    }
  }
}
