namespace NiceCore.Storage.NH.SessionFactories
{
  /// <summary>
  /// Interface for service that maps driver classes to connection types for nh connection infos created by
  /// ISessionFactoriesService
  /// </summary>
  public interface ISessionFactoriesConnectionTypeService
  {
    /// <summary>
    /// Get Connection Type that should be used for i_Driver Class
    /// </summary>
    /// <param name="i_DriverClass"></param>
    /// <returns></returns>
    string GetConnectionType(string i_DriverClass);
  }
}