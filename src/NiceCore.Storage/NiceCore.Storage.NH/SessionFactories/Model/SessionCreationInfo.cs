﻿using NHibernate;
using NiceCore.Storage.NH.Connections;
using NiceCore.Storage.NH.Conversation.Sessions;

namespace NiceCore.Storage.NH.SessionFactories.Model
{
  /// <summary>
  /// Class tuple for ISessionFactory and additionally needed info
  /// </summary>
  public sealed class SessionCreationInfo
  {
    /// <summary>
    /// The session factory to create sessions with
    /// </summary>
    public ISessionFactory SessionFactory { get; init; }
    
    /// <summary>
    /// Connection Info
    /// </summary>
    public NHConnectionInfo ConnectionInfo { get; init; }
    
    /// <summary>
    /// Native Session Provider
    /// </summary>
    public NcNativeSessionProvider NativeSessionProvider { get; init; }
  }
}
