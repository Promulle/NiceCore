namespace NiceCore.Storage.NH.SessionFactories
{
  /// <summary>
  /// NH Config Properties
  /// </summary>
  public static class NHConfigProperties
  {
    /// <summary>
    /// Connection
    /// </summary>
    public static class Connection
    {
      /// <summary>
      /// Connection Prefix
      /// </summary>
      public const string CONNECTION_PREFIX = "connection.";
      
      /// <summary>
      /// Property for Connection String
      /// </summary>
      public const string CONNECTION_STRING = CONNECTION_PREFIX + "connection_string";
      
      /// <summary>
      /// Property for Driver Class
      /// </summary>
      public const string CONNECTION_DRIVER_CLASS = CONNECTION_PREFIX + "driver_class";
    }
  }
}
