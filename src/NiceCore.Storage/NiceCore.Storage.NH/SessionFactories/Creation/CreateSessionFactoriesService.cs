﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NHibernate.Cfg;
using NiceCore.Logging;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation;
using NiceCore.Storage.DomainBuilder;
using NiceCore.Storage.Events;
using NiceCore.Storage.NH.Config.Services;
using NiceCore.Storage.NH.SchemaUpdates;
using NiceCore.Storage.NH.SessionFactories.Model;
using NiceCore.Storage.Services;

namespace NiceCore.Storage.NH.SessionFactories.Creation
{
  /// <summary>
  /// Service for session factories
  /// </summary>
  [Register(InterfaceType = typeof(ICreateSessionFactoriesService))]
  public class CreateSessionFactoriesService : ICreateSessionFactoriesService
  {
    /// <summary>
    /// Service for configs
    /// </summary>
    public INHConfigService NHConfigService { get; set; }

    /// <summary>
    /// Container
    /// </summary>
    public IServiceContainer Container { get; set; }

    /// <summary>
    /// Session Factories Connection Type Service
    /// </summary>
    public ISessionFactoriesConnectionTypeService SessionFactoriesConnectionTypeService { get; set; }
    
    /// <summary>
    /// Runner for schema update
    /// </summary>
    public ISchemaUpdateRunner SchemaUpdateRunner { get; set; }

    /// <summary>
    /// Service for reading/creating nh configs
    /// </summary>
    public INHConnectionConfigurationFileService NHConnectionConfigurationFileService { get; set; }

    /// <summary>
    /// Connection configuration management service
    /// </summary>
    public IConnectionConfigurationManagementService<ConnectionDefinition, Configuration> ConnectionConfigurationManagementService { get; set; }
    
    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<CreateSessionFactoriesService> Logger { get; set; }

    /// <summary>
    /// Creates or loads a session factory for connection i_ConnectionName
    /// </summary>
    /// <param name="i_Connection"></param>
    /// <param name="i_DomainBuilders"></param>
    /// <param name="i_ConnectionRetryStrategy"></param>
    /// <param name="i_SchemaUpdateCallBack"></param>
    /// <returns></returns>
    public Task<SessionCreationInfo> CreateOrLoad(ConnectionDefinition i_Connection, IEnumerable<IDomainBuilder> i_DomainBuilders, IDefaultConnectionRetryStrategy i_ConnectionRetryStrategy, Action<string> i_SchemaUpdateCallBack)
    {
      if (i_ConnectionRetryStrategy != null)
      {
        Logger.Information($"Creating a sessionfactory for connection {i_Connection.Name} with retry policy {i_ConnectionRetryStrategy.GetType().FullName}");
        return i_ConnectionRetryStrategy.Execute(() => CreateOrLoadInternal(i_Connection, i_DomainBuilders, i_SchemaUpdateCallBack));
      }
      Logger.Information($"Creating a sessionfactory for connection {i_Connection.Name}");
      return CreateOrLoadInternal(i_Connection, i_DomainBuilders, i_SchemaUpdateCallBack);
    }

    private async Task<SessionCreationInfo> CreateOrLoadInternal(ConnectionDefinition i_Connection, IEnumerable<IDomainBuilder> i_DomainBuilders, Action<string> i_SchemaUpdateCallBack)
    {
      Logger.Information($"Serialisation is disabled, creating config and sessionfactory for connection {i_Connection.Name}");
      var newCfg = await CreateConfigAndRunSchemaUpdate(i_Connection, i_DomainBuilders, i_SchemaUpdateCallBack).ConfigureAwait(false);
      newCfg.Properties.TryGetValue(NHConfigProperties.Connection.CONNECTION_DRIVER_CLASS, out var driverCls);
      var sessionFactory = newCfg.BuildSessionFactory();
      return new()
      {
        ConnectionInfo = new()
        {
          ConnectionName = i_Connection.Name,
          ConnectionString = newCfg.GetProperty(NHConfigProperties.Connection.CONNECTION_STRING),
          EnableAuditing = i_Connection.AllowAuditing,
          DriverClass = driverCls,
          ConnectionType = SessionFactoriesConnectionTypeService?.GetConnectionType(driverCls)
        },
        SessionFactory = sessionFactory,
        NativeSessionProvider = new (sessionFactory)
      };
    }

    private async Task<Configuration> CreateConfigAndRunSchemaUpdate(ConnectionDefinition i_Connection, IEnumerable<IDomainBuilder> i_DomainBuilders, Action<string> i_SchemaUpdateCallBack)
    {
      var domainBuilderList = i_DomainBuilders.ToList();
      var conf = await ConnectionConfigurationManagementService.GetConfiguration(i_Connection.Name, i_Connection, domainBuilderList).ConfigureAwait(false);
      if (i_Connection.SchemaUpdate)
      {
        SchemaUpdateRunner.DoSchemaUpdate(conf, domainBuilderList, i_SchemaUpdateCallBack);
      }
      return conf;
    }
  }
}
