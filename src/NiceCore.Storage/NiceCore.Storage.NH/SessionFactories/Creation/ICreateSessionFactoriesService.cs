﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NiceCore.Storage.DomainBuilder;
using NiceCore.Storage.NH.SessionFactories.Model;

namespace NiceCore.Storage.NH.SessionFactories.Creation
{
  /// <summary>
  /// Service for session factories
  /// </summary>
  public interface ICreateSessionFactoriesService
  {
    /// <summary>
    /// Create or load the SessionFactory for i_Connection
    /// </summary>
    /// <param name="i_Connection"></param>
    /// <param name="i_DomainBuilders"></param>
    /// <param name="i_ConnectionRetryStrategy">Strategy used for retrying when creating a session factory fails</param>
    /// <param name="i_SchemaUpdateCallBack"></param>
    /// <returns></returns>
    Task<SessionCreationInfo> CreateOrLoad(ConnectionDefinition i_Connection, IEnumerable<IDomainBuilder> i_DomainBuilders, IDefaultConnectionRetryStrategy i_ConnectionRetryStrategy, Action<string> i_SchemaUpdateCallBack);
  }
}