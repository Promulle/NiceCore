﻿using NHibernate.Linq;
using NiceCore.Filtering;
using NiceCore.Reflection;
using NiceCore.ServiceLocation;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace NiceCore.Storage.NH.AdditionalFilterProvider
{
  /// <summary>
  /// Filter Op
  /// </summary>
  [Register(InterfaceType = typeof(IAdditionalFilterOperationProvider), LifeStyle = LifeStyles.Singleton)]
  public class NHFilterOperationLikeProvider : IAdditionalFilterOperationProvider
  {
    private readonly MethodInfo m_LikeFunction = GetLikeFunction();

    /// <summary>
    /// LIKE
    /// </summary>
    public FilterOperations FilterOperation { get; } = FilterOperations.Like;

    /// <summary>
    /// Provide like expression
    /// </summary>
    /// <param name="i_PropertyExpression"></param>
    /// <param name="i_ValueExpression"></param>
    /// <returns></returns>
    public Expression ProvideExpression(Expression i_PropertyExpression, Expression i_ValueExpression)
    {
      return Expression.Call(null, m_LikeFunction, i_PropertyExpression, i_ValueExpression);
    }

    private static MethodInfo GetLikeFunction()
    {
      return ReflectionUtils.GetMethodEx(typeof(SqlMethods), nameof(SqlMethods.Like), new List<MethodParameterType>()
      {
        MethodParameterType.Of(typeof(string)),
        MethodParameterType.Of(typeof(string))
      });
    }
  }
}
