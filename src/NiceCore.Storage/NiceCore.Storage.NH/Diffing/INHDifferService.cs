using System.Collections.Generic;
using NiceCore.Storage.Diffing;

namespace NiceCore.Storage.NH.Diffing
{
  /// <summary>
  /// NH Differ Service
  /// </summary>
  public interface INHDifferService
  {
    /// <summary>
    /// Diffs state of nh format by the correct differ for i_Entity
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_OldState"></param>
    /// <param name="i_NewState"></param>
    /// <param name="i_PropertyNames"></param>
    /// <returns></returns>
    StorageDiffResult DiffState(object i_Entity, object[] i_OldState, object[] i_NewState, string[] i_PropertyNames);
    
    /// <summary>
    /// Diffs state of nh format by the correct differ for i_Entity
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_OldState"></param>
    /// <param name="i_NewState"></param>
    /// <param name="i_PropertyNames"></param>
    /// <returns></returns>
    IReadOnlySet<string> GetChangedProperties(object i_Entity, object[] i_OldState, object[] i_NewState, string[] i_PropertyNames);
  }
}