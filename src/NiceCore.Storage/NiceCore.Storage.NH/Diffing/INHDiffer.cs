using System;
using System.Collections.Generic;
using NiceCore.Storage.Diffing;

namespace NiceCore.Storage.NH.Diffing
{
  /// <summary>
  /// NH Differ
  /// </summary>
  public interface INHDiffer
  {
    /// <summary>
    /// Checks if a differ can be used for an instance
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <returns></returns>
    bool CanBeUsedFor(object i_Instance);
    
    /// <summary>
    /// Create diff from old to new state
    /// </summary>
    /// <param name="i_EntityType"></param>
    /// <param name="i_OldState"></param>
    /// <param name="i_NewState"></param>
    /// <param name="i_PropertyNames"></param>
    /// <returns></returns>
    StorageDiffResult DiffState(Type i_EntityType, object[] i_OldState, object[] i_NewState, string[] i_PropertyNames);

    /// <summary>
    /// Returns the names of properties that have changed from i_OldState to i_NewState. For full info use <see cref="DiffState"/>
    /// </summary>
    /// <param name="i_EntityType"></param>
    /// <param name="i_OldState"></param>
    /// <param name="i_NewState"></param>
    /// <param name="i_PropertyNames"></param>
    /// <returns></returns>
    IReadOnlySet<string> GetChangedProperties(Type i_EntityType, object[] i_OldState, object[] i_NewState, string[] i_PropertyNames);
  }
}