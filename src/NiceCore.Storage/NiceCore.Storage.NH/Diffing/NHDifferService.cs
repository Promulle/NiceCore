using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using NiceCore.Collections;
using NiceCore.Diagnostics;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation;
using NiceCore.Storage.Diffing;

namespace NiceCore.Storage.NH.Diffing
{
  /// <summary>
  /// NH Differ Repository
  /// </summary>
  [Register(InterfaceType = typeof(INHDifferService), LifeStyle = LifeStyles.Singleton)]
  public sealed class NHDifferService : INHDifferService
  {
    /// <summary>
    /// All Differs
    /// </summary>
    public IList<INHDiffer> AllDiffers { get; set; } = new List<INHDiffer>();

    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<NHDifferService> Logger { get; set; } = DiagnosticsCollector.Instance.GetLogger<NHDifferService>();

    /// <summary>
    /// Diffs state of nh format by the correct differ for i_Entity
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_OldState"></param>
    /// <param name="i_NewState"></param>
    /// <param name="i_PropertyNames"></param>
    /// <returns></returns>
    public StorageDiffResult DiffState(object i_Entity, object[] i_OldState, object[] i_NewState, string[] i_PropertyNames)
    {
      try
      {
        if (AllDiffers == null || AllDiffers.Count == 0)
        {
          Logger.Warning("No differs were injected into NHDifferService.");
          return new();
        }
        if (i_Entity == null)
        {
          return new();
        }
        foreach (var differ in AllDiffers)
        {
          if (differ.CanBeUsedFor(i_Entity))
          {
            return differ.DiffState(i_Entity.GetType(), i_OldState, i_NewState, i_PropertyNames);
          }
        }
      }
      catch (Exception e)
      {
        Logger.Error(e, $"An error occurred during execution of differ for entitytype: {i_Entity!.GetType()}"); 
      }
      return new();
    }
    
    /// <summary>
    /// Diffs state of nh format by the correct differ for i_Entity
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_OldState"></param>
    /// <param name="i_NewState"></param>
    /// <param name="i_PropertyNames"></param>
    /// <returns></returns>
    public IReadOnlySet<string> GetChangedProperties(object i_Entity, object[] i_OldState, object[] i_NewState, string[] i_PropertyNames)
    {
      try
      {
        if (AllDiffers == null || AllDiffers.Count == 0)
        {
          Logger.Warning("No differs were injected into NHDifferService.");
          return NcDefaultSets<string>.Empty;
        }
        if (i_Entity == null)
        {
          return NcDefaultSets<string>.Empty;
        }
        foreach (var differ in AllDiffers)
        {
          if (differ.CanBeUsedFor(i_Entity))
          {
            return differ.GetChangedProperties(i_Entity.GetType(), i_OldState, i_NewState, i_PropertyNames);
          }
        }
      }
      catch (Exception e)
      {
        Logger.Error(e, $"An error occurred during execution of differ (propertyNames only) for entitytype: {i_Entity!.GetType()}"); 
      }
      return NcDefaultSets<string>.Empty;
    }
  }
}