namespace NiceCore.Storage.NH.Diffing
{
  /// <summary>
  /// Diff Values
  /// </summary>
  public struct DiffResult
  {
    private static readonly DiffResult NO_CHANGE_RESULT = new()
    {
      PropertyWasChanged = false,
      New = null,
      Old = null
    };
    
    /// <summary>
    /// OldValue
    /// </summary>
    public required object Old { get; init; }
    
    /// <summary>
    /// New
    /// </summary>
    public required object New { get; init; }
    
    /// <summary>
    /// Whether the property was changed or not
    /// </summary>
    public required bool PropertyWasChanged { get; init; }

    /// <summary>
    /// Success
    /// </summary>
    /// <param name="i_OldValue"></param>
    /// <param name="i_NewValue"></param>
    /// <returns></returns>
    public static DiffResult Changed(object i_OldValue, object i_NewValue)
    {
      return new()
      {
        New = i_NewValue,
        Old = i_OldValue,
        PropertyWasChanged = true
      };
    }

    /// <summary>
    /// No Change
    /// </summary>
    /// <returns></returns>
    public static DiffResult NoChange()
    {
      return NO_CHANGE_RESULT;
    }
  }
}