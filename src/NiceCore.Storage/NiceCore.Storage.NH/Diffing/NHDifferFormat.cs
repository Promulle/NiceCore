namespace NiceCore.Storage.NH.Diffing
{
  /// <summary>
  /// Format
  /// </summary>
  public struct NHDifferFormat
  {
    /// <summary>
    /// Old State
    /// </summary>
    public object[] OldState { get; set; }
    
    /// <summary>
    /// New State
    /// </summary>
    public object[] NewState { get; set; }
    
    /// <summary>
    /// Property Names
    /// </summary>
    public string[] PropertyNames { get; set; }

    /// <summary>
    /// Convert to objects to NH Format
    /// </summary>
    /// <param name="i_Old"></param>
    /// <param name="i_New"></param>
    /// <returns></returns>
    public static NHDifferFormat ToNHFormat(object i_Old, object i_New)
    {
      var properties = i_Old.GetType().GetProperties();
      var oldState = new object[properties.Length];
      var newState = new object[properties.Length];
      var propertyNames = new string[properties.Length];
      for (var i = 0; i < properties.Length; i++)
      {
        var property = properties[i];
        var oldVal = property.GetValue(i_Old);
        var newVal = property.GetValue(i_New);
        propertyNames[i] = property.Name;
        oldState[i] = oldVal;
        newState[i] = newVal;
      }

      return new()
      {
        NewState = newState,
        OldState = oldState,
        PropertyNames = propertyNames 
      };
    }
  }
}