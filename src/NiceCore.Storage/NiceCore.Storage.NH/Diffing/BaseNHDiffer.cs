﻿using System;
using System.Collections.Generic;
using NiceCore.Caches;
using NiceCore.Collections;
using NiceCore.Entities;
using NiceCore.Reflection;
using NiceCore.Storage.Diffing;

namespace NiceCore.Storage.NH.Diffing
{
  /// <summary>
  /// Diffing impl used by NHConv,
  /// using format provided by interceptor
  /// </summary>
  public abstract class BaseNHDiffer : INHDiffer
  {
    /// <summary>
    /// Create diff from old to new state
    /// </summary>
    /// <param name="i_EntityType"></param>
    /// <param name="i_OldState"></param>
    /// <param name="i_NewState"></param>
    /// <param name="i_PropertyNames"></param>
    /// <returns></returns>
    public StorageDiffResult DiffState(Type i_EntityType, object[] i_OldState, object[] i_NewState, string[] i_PropertyNames)
    {
      if (i_EntityType == null)
      {
        return new();
      }

      Dictionary<string, StorageDiffResultEntry> entries = new();
      for (var i = 0; i < i_PropertyNames.Length; i++)
      {
        var propertyName = i_PropertyNames[i];
        var newValue = i_NewState[i];
        var oldValue = i_OldState[i];
        var valueChangeResult = ValueChanged(i_EntityType, propertyName, oldValue, newValue);
        if (valueChangeResult.PropertyWasChanged)
        {
          entries[propertyName] = new()
          {
            NewValue = valueChangeResult.New,
            OldValue = valueChangeResult.Old,
            PropertyName = propertyName
          };
        }
      }

      return new()
      {
        Entries = entries
      };
    }

    /// <summary>
    /// Returns the names of properties that have changed from i_OldState to i_NewState. For full info use <see cref="DiffState"/>
    /// </summary>
    /// <param name="i_EntityType"></param>
    /// <param name="i_OldState"></param>
    /// <param name="i_NewState"></param>
    /// <param name="i_PropertyNames"></param>
    /// <returns></returns>
    public IReadOnlySet<string> GetChangedProperties(Type i_EntityType, object[] i_OldState, object[] i_NewState, string[] i_PropertyNames)
    {
      if (i_EntityType == null)
      {
        return NcDefaultSets<string>.Empty;
      }

      var set = new HashSet<string>();
      for (var i = 0; i < i_PropertyNames.Length; i++)
      {
        var propertyName = i_PropertyNames[i];
        var newValue = i_NewState[i];
        var oldValue = i_OldState[i];
        var valueChangeResult = ValueChanged(i_EntityType, propertyName, oldValue, newValue);
        if (valueChangeResult.PropertyWasChanged)
        {
          set.Add(propertyName);
        }
      }

      return set;
    }

    /// <summary>
    /// Checks if a differ can be used for an instance
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <returns></returns>
    public abstract bool CanBeUsedFor(object i_Instance);

    /// <summary>
    /// Gets the entity type that this differ will use to determine whether something is an entity
    /// </summary>
    /// <returns></returns>
    protected abstract Type GetEntityType();

    private DiffResult ValueChanged(Type i_EntityType, string i_PropertyName, object i_OldValue, object i_NewValue)
    {
      if (i_OldValue == null && i_NewValue == null)
      {
        return DiffResult.NoChange();
      }

      // var propertyType = ExpressionCaches.PropertyTypeCache.Get(new()
      // {
      //   DeclaringType = i_EntityType,
      //   PropertyName = i_PropertyName
      // });
      var entityValue = i_OldValue ?? i_NewValue;
      var isCollectionTypes = ReflectionUtils.OpenGenericTypeIsBaseFor(typeof(ICollection<>), entityValue.GetType());
      if (isCollectionTypes.Success)
      {
        //Collections are not yet supported
        return DiffResult.NoChange();
      }
      var entityType = GetEntityType();
      if (entityType.IsInstanceOfType(entityValue))
      {
        return CheckEntityValues(null, i_OldValue, i_NewValue);
      }
      return CheckNonEntityValues(i_OldValue, i_NewValue);
    }

    private static DiffResult CheckNonEntityValues(object i_OldValue, object i_NewValue)
    {
      if (i_OldValue == null || i_NewValue == null)
      {
        if (i_OldValue != null)
        {
          return DiffResult.Changed(i_OldValue, null);
        }

        return DiffResult.Changed(null, i_NewValue);
      }
      if (!object.Equals(i_NewValue, i_OldValue))
      {
        return DiffResult.Changed(i_OldValue, i_NewValue);
      }

      return DiffResult.NoChange();
    }

    private DiffResult CheckEntityValues(Type i_EntityType, object i_OldValue, object i_NewValue)
    {
      // var idAccessor = ExpressionCaches.PropertyGetterCache.Get(new()
      // {
      //   DeclaringType = i_EntityType,
      //   PropertyName = nameof(IDomainEntity<object>.ID)
      // });
      if (i_OldValue == null || i_NewValue == null)
      {
        if (i_OldValue != null)
        {
          var actualOldId = GetID(i_OldValue);
          return DiffResult.Changed(actualOldId, null);
        }

        var actualNewId = GetID(i_NewValue);
        return DiffResult.Changed(null, actualNewId);
      }
      var oldId = GetID(i_OldValue);
      var newId = GetID(i_NewValue);
      if (!object.Equals(oldId, newId))
      {
        return DiffResult.Changed(oldId, newId);
      }
      return DiffResult.NoChange();
    }

    /// <summary>
    /// Retrieves the ID from i_Value
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    protected abstract object GetID(object i_Value);
  }
}