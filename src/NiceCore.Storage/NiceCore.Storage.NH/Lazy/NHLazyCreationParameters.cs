﻿using NHibernate.Engine;
using NiceCore.Storage.LazyLoading;

namespace NiceCore.Storage.NH.Lazy
{
  /// <summary>
  /// Lazy Parameters for NHibernate
  /// </summary>
  public class NHLazyCreationParameters<TId> : ILazyProxyCreationParameters<TId>
  {
    /// <summary>
    /// Id
    /// </summary>
    public TId ID { get; init; }

    /// <summary>
    /// Session
    /// </summary>
    public ISessionImplementor Session { get; init; }
  }
}
