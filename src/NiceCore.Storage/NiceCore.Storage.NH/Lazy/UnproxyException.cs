using System;
using NHibernate.Proxy;

namespace NiceCore.Storage.NH.Lazy
{
  /// <summary>
  /// Exception that is thrown if something goes wrong during initialization/unproxying of a nh proxy
  /// </summary>
  public class UnproxyException : Exception
  {
    /// <summary>
    /// Entity Name
    /// </summary>
    public string EntityName { get; }
    
    /// <summary>
    /// Identifier
    /// </summary>
    public object Identifier { get; }
    
    /// <summary>
    /// Persistent Class
    /// </summary>
    public Type PersistentClass { get; }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="i_InnerException"></param>
    /// <param name="i_Initializer"></param>
    public UnproxyException(Exception i_InnerException, ILazyInitializer i_Initializer) : base(GetMessage(i_Initializer), i_InnerException)
    {
      EntityName = i_Initializer.EntityName;
      Identifier = i_Initializer.Identifier;
      PersistentClass = i_Initializer.PersistentClass;
    }

    private static string GetMessage(ILazyInitializer i_Initializer)
    {
      return $"Could not unproxy proxy for type {i_Initializer.PersistentClass.FullName} for id {i_Initializer.Identifier}";
    }
  }
}