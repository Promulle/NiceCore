using NHibernate.Engine;
using NiceCore.Storage.LazyLoading;

namespace NiceCore.Storage.NH.Lazy
{
  /// <summary>
  /// Unproxy parameters
  /// </summary>
  public class NHUnproxyParameters : IUnproxyParameters
  {
    /// <summary>
    /// Possible proxy
    /// </summary>
    public object PossibleProxy { get; set; }
    
    /// <summary>
    /// Session
    /// </summary>
    public ISessionImplementor Session { get; set; }
  }
}
