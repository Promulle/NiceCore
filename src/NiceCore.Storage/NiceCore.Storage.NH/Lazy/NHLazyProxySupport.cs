﻿using NHibernate.Proxy;
using NiceCore.Base.Caching;
using NiceCore.Entities;
using NiceCore.ServiceLocation;
using NiceCore.Storage.LazyLoading;
using System;
using System.Collections.Generic;
using System.Reflection;
using NHibernate;
using NHibernate.Engine;

namespace NiceCore.Storage.NH.Lazy
{
  /// <summary>
  /// lazy Proxy support for NH
  /// </summary>
  [Register(InterfaceType = typeof(ILazyProxySupport), LifeStyle = LifeStyles.Singleton)]
  public class NHLazyProxySupport : ILazyProxySupport
  {
    private readonly SimpleCache<Type, StaticProxyFactory> m_FactoryCache = new();

    /// <summary>
    /// Creates a lazy proxy
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <param name="i_Parameter"></param>
    /// <returns></returns>
    public T CreateLazyProxy<T, TId>(ILazyProxyCreationParameters<TId> i_Parameter) where T : class, IDomainEntity<TId>
    {
      if (i_Parameter is NHLazyCreationParameters<TId> nhParameter)
      {
        var factory = m_FactoryCache.GetOrCreate(typeof(T), GetFactoryFor<T, TId>);
        return factory.GetProxy(nhParameter.ID, nhParameter.Session) as T;
      }
      throw new InvalidOperationException($"Proxy Support of type {GetType()} was called with incorrect parameter class {i_Parameter.GetType()}. Expected parameter class: {typeof(NHLazyCreationParameters<TId>)}");
    }

    /// <summary>
    /// Unproxy
    /// </summary>
    /// <param name="i_Parameter"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    public T Unproxy<T, TId>(IUnproxyParameters i_Parameter) where T : IDomainEntity<TId>
    {
      if (i_Parameter is NHUnproxyParameters nhParameter)
      {
        return (T)nhParameter.Session.PersistenceContext.Unproxy(nhParameter.PossibleProxy);
      }
      throw new InvalidOperationException($"Proxy Support of type {GetType()} was called with incorrect parameter class {i_Parameter.GetType()}. Expected parameter class: {typeof(NHUnproxyParameters)}");
    }
    
    /// <summary>
    /// Unproxies the parameter given, initializes the proxy if it is not initialized
    /// </summary>
    /// <param name="i_Parameter"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    public T UnproxyWithInitialize<T, TId>(IUnproxyParameters i_Parameter) where T : IDomainEntity<TId>
    {
      if (i_Parameter is NHUnproxyParameters parameters && parameters.PossibleProxy is INHibernateProxy proxy)
      {
        try
        {
          if (proxy.HibernateLazyInitializer.Session == null)
          {
            proxy.HibernateLazyInitializer.SetSession(parameters.Session);
          }
          else if (proxy.HibernateLazyInitializer.Session.IsClosed || !proxy.HibernateLazyInitializer.Session.IsConnected)
          {
            proxy.HibernateLazyInitializer.UnsetSession();
            proxy.HibernateLazyInitializer.SetSession(parameters.Session);
          }
          var res = proxy.HibernateLazyInitializer.GetImplementation();
          return (T) res;
        }
        catch (Exception ex)
        {
          throw new UnproxyException(ex, proxy.HibernateLazyInitializer);
        }
      }
      return (T)i_Parameter.PossibleProxy;
    }

    /// <summary>
    /// Checks if an instance is a proxy
    /// </summary>
    /// <param name="i_PossibleProxy"></param>
    /// <returns></returns>
    public bool IsProxy(object i_PossibleProxy)
    {
      return i_PossibleProxy is INHibernateProxy;
    }

    private static StaticProxyFactory GetFactoryFor<T, TId>() where T : class, IDomainEntity<TId>
    {
      var type = typeof(T);
      var idGettter = GetIDGetter<T, TId>();
      var idSetter = GetIDSetter<T, TId>();
      var factory = new StaticProxyFactory();
      factory.PostInstantiate(type.FullName, type, new HashSet<Type> { typeof(INHibernateProxy) }, idGettter, idSetter, null, true);
      return factory;
    }

    private static MethodInfo GetIDGetter<T, TId>() where T : class, IDomainEntity<TId>
    {
      var property = typeof(T).GetProperty(nameof(IDomainEntity<TId>.ID));
      if (property == null)
      {
        throw new InvalidOperationException($"Type: {typeof(T).FullName} does not contain the property: {nameof(IDomainEntity<TId>.ID)}. Could not retrieve getter.");
      }
      if (property.GetMethod == null)
      {
        throw new NotSupportedException($"Type: {typeof(T).FullName} does contain the property {nameof(IDomainEntity<TId>.ID)} but this property does not contain a Getter. Can not create lazy proxy.");
      }
      return property.GetMethod;
    }
    
    private static MethodInfo GetIDSetter<T, TId>() where T : class, IDomainEntity<TId>
    {
      var property = typeof(T).GetProperty(nameof(IDomainEntity<TId>.ID));
      if (property == null)
      {
        throw new InvalidOperationException($"Type: {typeof(T).FullName} does not contain the property: {nameof(IDomainEntity<TId>.ID)}. Could not retrieve setter.");
      }
      if (property.SetMethod == null)
      {
        throw new NotSupportedException($"Type: {typeof(T).FullName} does contain the property {nameof(IDomainEntity<TId>.ID)} but this property does not contain a Setter. Can not create lazy proxy.");
      }
      return property.SetMethod;
    }
  }
}
