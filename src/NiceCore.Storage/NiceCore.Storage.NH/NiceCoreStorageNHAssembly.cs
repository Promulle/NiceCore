﻿namespace NiceCore.Storage.NH
{
  /// <summary>
  /// Storage Assembly Constants
  /// </summary>
  internal static class NiceCoreStorageNHAssembly
  {
    /// <summary>
    /// Name of Assembly
    /// </summary>
    public const string ASSEMBLY_NAME = "NiceCore.Storage.NH";
  }
}
