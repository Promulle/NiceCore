﻿using System;
using System.Collections.Generic;

namespace NiceCore.Storage.NH.Mapping
{
  /// <summary>
  /// Interface for classes that are mappings for storage
  /// </summary>
  public interface IFluentlyMapped
  {
    /// <summary>
    /// Type of entity to be mapped
    /// </summary>
    /// <returns></returns>
    Type GetMappedType();

    /// <summary>
    /// Returns a set of connections this mapping can be applied to.
    /// Null or Empty = ALL Connections
    /// </summary>
    /// <returns></returns>
    IReadOnlySet<string> GetAllowedConnections();
  }
}
