using System.Threading.Tasks;

namespace NiceCore.Storage.NH.Mapping
{
  /// <summary>
  /// Interfadce for mappings that are executed delayed (after Constructor).
  /// This enables use of injected Properties
  /// </summary>
  public interface IDelayedFluentlyMapped : IFluentlyMapped
  {
    /// <summary>
    /// Execute method that is implemented to enable delayed mapping execution
    /// </summary>
    /// <returns></returns>
    Task ExecuteMappingDelayed();
  }
}
