﻿using FluentNHibernate.Conventions;

namespace NiceCore.Storage.NH.Mapping
{
  /// <summary>
  /// Convention for FluentNHibernate
  /// </summary>
  public interface IFluentConvention
  {
    /// <summary>
    /// Convention that is returned
    /// </summary>
    /// <returns></returns>
    IConvention[] GetConventions();
  }
}
