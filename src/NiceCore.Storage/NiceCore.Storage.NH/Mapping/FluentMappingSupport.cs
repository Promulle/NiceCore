﻿using System;
using System.Collections.Generic;
using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using NiceCore.Extensions;
using NiceCore.ServiceLocation;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Logging.Extensions;

namespace NiceCore.Storage.NH.Mapping
{
  /// <summary>
  /// Class that loads and applies fluent mappings
  /// </summary>
  public class FluentMappingSupport
  {
    /// <summary>
    /// Applies mappings found in t_Container to t_Conf
    /// </summary>
    /// <param name="t_Conf"></param>
    /// <param name="i_Definition"></param>
    /// <param name="t_Container"></param>
    /// <param name="i_ExcludedTypes">Types whose mappings will be ignored</param>
    /// <param name="i_Logger"></param>
    public static async Task AddFluentMappingsToConfig(NHibernate.Cfg.Configuration t_Conf,
      ConnectionDefinition i_Definition,
      IServiceContainer t_Container,
      HashSet<Type> i_ExcludedTypes,
      ILogger<FluentMappingSupport> i_Logger)
    {
      if (t_Container != null)
      {
        var fluentMappings = t_Container.ResolveAll<IFluentlyMapped>().ToArray();
        foreach (var mapping in fluentMappings)
        {
          if (mapping is IDelayedFluentlyMapped delayedMapping)
          {
            await delayedMapping.ExecuteMappingDelayed().ConfigureAwait(false);
          }
        }

        i_Logger.Information($"Resolved {fluentMappings.Length} fluent mappings.");
        var fluentConventions = t_Container.ResolveAll<IFluentConvention>();
        i_Logger.Information($"Resolved {fluentConventions.Count()} fluent conventions.");
        var mappings = new MappingConfiguration();
        fluentConventions.ForEachItem(convention =>
        {
          i_Logger.Information($"Applying convention {convention.GetType().FullName}");
          mappings.AutoMappings.Add(new AutoPersistenceModel().Conventions.Add(convention.GetConventions()));
          mappings.FluentMappings.Conventions.Add(convention.GetConventions());
          t_Container.Release(convention);
        });
        ApplyFluentMappings(mappings, i_Definition.Name, fluentMappings, t_Container, i_ExcludedTypes, i_Logger);
        i_Logger.Information("Applying all found mappings and conventions ");
        mappings.Apply(t_Conf);
      }
      else
      {
        i_Logger.Warning("Container property not set for fluent mapping support. FluentMappings will not be applied.");
      }
    }

    private static void ApplyFluentMappings(MappingConfiguration t_MappingConfiguration,
      string i_ConnectionName,
      IEnumerable<IFluentlyMapped> i_Mappings,
      IServiceContainer t_Container,
      IReadOnlySet<Type> i_ExcludedTypes,
      ILogger i_Logger)
    {
      foreach (var mapping in i_Mappings)
      {
        i_Logger.Information($"Applying fluent mapping of type {mapping.GetType().FullName}");
        if (IsConnectionAllowedForMapping(mapping, i_ConnectionName, i_Logger))
        {
          var entityType = mapping.GetMappedType();
          if (i_ExcludedTypes == null || i_ExcludedTypes.Count == 0 || !i_ExcludedTypes.Contains(entityType))
          {
            i_Logger.Information($"Fluent Mapping of type {mapping.GetType()} that maps entities of type {entityType} will be mapped.");
            t_MappingConfiguration.FluentMappings.Add(mapping.GetType());
          }
          else
          {
            i_Logger.Information($"Fluent Mapping of type {mapping.GetType()} that maps entities of type {entityType} will not be mapped because the entity type is in the excluded set.");
          }  
        }
        t_Container.Release(mapping);
      }
    }

    private static bool IsConnectionAllowedForMapping(IFluentlyMapped i_Mapping, string i_ConnectionName, ILogger i_Logger)
    {
      var allowedConnections = i_Mapping.GetAllowedConnections();
      if (allowedConnections == null || allowedConnections.Count == 0)
      {
        i_Logger.Information($"Mapping {i_Mapping.GetType().Name} does not define allowed connections.");
        return true;
      }

      var containsDefinition = allowedConnections.Contains(i_ConnectionName);
      i_Logger.Information($"Mapping {i_Mapping.GetType().Name} defines {allowedConnections.Count} allowed Connections. These connections contain {i_ConnectionName}: {containsDefinition}");
      return containsDefinition;
    }
  }
}
