﻿using NHibernate.Linq;
using NiceCore.Entities;
using NiceCore.Storage.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Polly;

namespace NiceCore.Storage.NH.Queries
{
  /// <summary>
  /// Provider used for executing queries async
  /// </summary>
  public class NHAsyncQueryProvider : IAsyncQueryProvider
  {
    /// <summary>
    /// Call ToListAsync
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="t_Query"></param>
    /// <param name="i_Policy"></param>
    /// <returns></returns>
    public Task<List<T>> QueryToListAsync<T, Tid>(IQueryable<T> t_Query, IAsyncPolicy i_Policy) where T : IDomainEntity<Tid>
    {
      return ToListAsync(t_Query, i_Policy);
    }

    /// <summary>
    /// Call ToListAsync
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Query"></param>
    /// <param name="i_Policy"></param>
    /// <returns></returns>
    public Task<List<T>> QueryToListAsync<T>(IQueryable<T> t_Query, IAsyncPolicy i_Policy)
    {
      return ToListAsync(t_Query, i_Policy);
    }

    private static Task<List<T>> ToListAsync<T>(IQueryable<T> t_Query, IAsyncPolicy i_Policy)
    {
      return Execute(static (qry,_) => LinqExtensionMethods.ToListAsync(qry), t_Query, (object)null, i_Policy);
    }

    /// <summary>
    /// Call CountAsync
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="t_Query"></param>
    /// <param name="i_Policy"></param>
    /// <returns></returns>
    public Task<int> CountAsync<T, Tid>(IQueryable<T> t_Query, IAsyncPolicy i_Policy) where T : IDomainEntity<Tid>
    {
      return CountAsyncInternal(t_Query, i_Policy);
    }

    /// <summary>
    /// Call CountAsync
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Query"></param>
    /// <param name="i_Policy"></param>
    /// <returns></returns>
    public Task<int> CountAsync<T>(IQueryable<T> t_Query, IAsyncPolicy i_Policy)
    {
      return CountAsyncInternal(t_Query, i_Policy);
    }

    private static Task<int> CountAsyncInternal<T>(IQueryable<T> t_Query, IAsyncPolicy i_Policy)
    {
      return Execute(static (qry,_) => LinqExtensionMethods.CountAsync(qry), t_Query, (object)null, i_Policy);
    }

    /// <summary>
    /// Call CountAsync
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="t_Query"></param>
    /// <param name="i_WhereExpression"></param>
    /// <param name="i_Policy"></param>
    /// <returns></returns>
    public Task<int> CountAsync<T, Tid>(IQueryable<T> t_Query, Expression<Func<T, bool>> i_WhereExpression, IAsyncPolicy i_Policy) where T : IDomainEntity<Tid>
    {
      return CountAsyncInternal(t_Query, i_WhereExpression, i_Policy);
    }

    /// <summary>
    /// Call CountAsync
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Query"></param>
    /// <param name="i_WhereExpression"></param>
    /// <param name="i_Policy"></param>
    /// <returns></returns>
    public Task<int> CountAsync<T>(IQueryable<T> t_Query, Expression<Func<T, bool>> i_WhereExpression, IAsyncPolicy i_Policy)
    {
      return CountAsyncInternal(t_Query, i_WhereExpression, i_Policy);
    }
    
    private static Task<int> CountAsyncInternal<T>(IQueryable<T> t_Query, Expression<Func<T, bool>> i_WhereExpression, IAsyncPolicy i_Policy)
    {
      return Execute(static (qry,expr) => LinqExtensionMethods.CountAsync(qry, expr), t_Query, i_WhereExpression, i_Policy);
    }

    /// <summary>
    /// Any Async
    /// </summary>
    /// <param name="t_Query"></param>
    /// <param name="i_Policy"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public Task<bool> AnyAsync<T>(IQueryable<T> t_Query, IAsyncPolicy i_Policy)
    {
      return Execute(static (qry, _) => LinqExtensionMethods.AnyAsync(qry), t_Query, (object)null, i_Policy);
    }

    /// <summary>
    /// First or default async
    /// </summary>
    /// <param name="t_Query"></param>
    /// <param name="i_Policy"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <returns></returns>
    public Task<T> QueryFirstOrDefaultAsync<T, Tid>(IQueryable<T> t_Query, IAsyncPolicy i_Policy) where T : IDomainEntity<Tid>
    {
      return FirstOrDefaultAsync(t_Query, i_Policy);
    }

    /// <summary>
    /// First or default async
    /// </summary>
    /// <param name="t_Query"></param>
    /// <param name="i_Policy"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public Task<T> QueryFirstOrDefaultAsync<T>(IQueryable<T> t_Query, IAsyncPolicy i_Policy)
    {
      return FirstOrDefaultAsync(t_Query, i_Policy);
    }
    
    private static Task<T> FirstOrDefaultAsync<T>(IQueryable<T> t_Query, IAsyncPolicy i_Policy)
    {
      return Execute(static (qry, _) => LinqExtensionMethods.FirstOrDefaultAsync(qry), t_Query, (object)null, i_Policy);
    }

    /// <summary>
    /// First or default async
    /// </summary>
    /// <param name="t_Query"></param>
    /// <param name="i_WhereExpr"></param>
    /// <param name="i_Policy"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <returns></returns>
    public Task<T> QueryFirstOrDefaultAsync<T, Tid>(IQueryable<T> t_Query, Expression<Func<T, bool>> i_WhereExpr, IAsyncPolicy i_Policy) where T : IDomainEntity<Tid>
    {
      return Execute(static (qry, expr) => LinqExtensionMethods.FirstOrDefaultAsync(qry, expr), t_Query, i_WhereExpr, i_Policy);
    }
    
    private static Task<TResult> Execute<T, TAdditional, TResult>(Func<IQueryable<T>, TAdditional, Task<TResult>> i_Func, IQueryable<T> t_Query, TAdditional i_Additional, IAsyncPolicy i_Policy)
    {
      if (i_Policy != null)
      {
        return i_Policy.ExecuteAsync(() => i_Func.Invoke(t_Query, i_Additional));
      }
      if (t_Query.Provider is INcQueryProvider<T> ncProvider && ncProvider.Options != null && ncProvider.Options.Policy != null)
      {
        return ncProvider.Options.Policy.ExecuteAsync(() => i_Func.Invoke(t_Query, i_Additional));
      }

      return i_Func.Invoke(t_Query, i_Additional);
    }
  }
}
