﻿using NiceCore.Storage.Diffing;
using NiceCore.Storage.NH.Conversation;

namespace NiceCore.Storage.NH
{
  /// <summary>
  /// Event thrown by nh interceptor
  /// </summary>
  public class NHInterceptorEvent
  {
    /// <summary>
    /// Entity that is operated on
    /// </summary>
    public object Entity { get; init; }

    /// <summary>
    /// Diff, only filled in update events
    /// </summary>
    public StorageDiffResult Diff { get; init; }

    /// <summary>
    /// Operation of the event
    /// </summary>
    public ConversationOperations Operation { get; init; }
  }
}
