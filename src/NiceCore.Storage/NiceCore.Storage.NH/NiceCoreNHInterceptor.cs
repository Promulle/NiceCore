﻿using NHibernate;
using NHibernate.Type;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.NH.Conversation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Logging.Extensions;
using NiceCore.Storage.Conversations.Flags;
using NiceCore.Storage.Conversations.Queries;
using NiceCore.Storage.Events;
using NiceCore.Storage.Events.LostEvents;
using NiceCore.Storage.Helpers;
using NiceCore.Storage.NH.Diffing;
using NiceCore.Storage.NH.Interceptors;

namespace NiceCore.Storage.NH
{
  /// <summary>
  /// Entity Modifier NHInterceptor
  /// </summary>
  public sealed class NiceCoreNHInterceptor : EmptyInterceptor, IDisposable
  {
    private readonly IDictionary<EntityModifyOperations, List<IEntityOperationModifier>> m_Modifier;
    private readonly List<ILoadedEntityModifier>                                         m_ReadModifier;
    private readonly IConversationContext                                                m_Context;
    private readonly ConversationOperationFlags                                          m_Flags;
    private readonly INHDifferService                                                    m_DifferService;
    private readonly ILostStorageEventCollector                                          m_LostEventCollector;
    private readonly ILogger                                                             m_Logger;

    /// <summary>
    /// Event for listening to events happening in the interceptor
    /// </summary>
    public event EventHandler<NHInterceptorEvent> InterceptorEvent;

    /// <summary>
    /// ctor filling entity modifier
    /// </summary>
    /// <param name="i_Modifier"></param>
    /// <param name="i_ReadModifier"></param>
    /// <param name="i_Flags"></param>
    /// <param name="i_Context"></param>
    /// <param name="i_DifferService"></param>
    /// <param name="i_LostEventCollector"></param>
    /// <param name="i_StorageEventModel"></param>
    /// <param name="i_Logger"></param>
    public NiceCoreNHInterceptor(IDictionary<EntityModifyOperations, List<IEntityOperationModifier>> i_Modifier,
      List<ILoadedEntityModifier> i_ReadModifier,
      ConversationOperationFlags i_Flags,
      IConversationContext i_Context,
      INHDifferService i_DifferService,
      ILostStorageEventCollector i_LostEventCollector,
      StorageEventModel i_StorageEventModel,
      ILogger i_Logger)
    {
      m_Modifier                = i_Modifier;
      m_Context                 = i_Context;
      m_ReadModifier            = i_ReadModifier;
      m_Flags                   = i_Flags;
      m_DifferService           = i_DifferService;
      m_LostEventCollector      = i_LostEventCollector;
      m_Logger                  = i_Logger;
    }

    /// <summary>
    /// Get Current Context
    /// </summary>
    /// <returns></returns>
    public IConversationContext GetCurrentContext()
    {
      return m_Context;
    }

    /// <summary>
    /// on load
    /// </summary>
    /// <param name="entity"></param>
    /// <param name="id"></param>
    /// <param name="state"></param>
    /// <param name="propertyNames"></param>
    /// <param name="types"></param>
    /// <returns></returns>
    public override bool OnLoad(object entity, object id, object[] state, string[] propertyNames, IType[] types)
    {
      try
      {
        var res = base.OnLoad(entity, id, state, propertyNames, types);
        if (m_ReadModifier != null && m_ReadModifier.Count > 0)
        {
          var entityState = GenericEntityState<object>.Of(entity, propertyNames, state);
          var changed     = m_ReadModifier.Select(modifier => modifier.ModifyNonGenericEntity(entityState, m_Flags, m_Context)).ToList().Any(r => r);
          return res || changed;
        }

        return res;
      }
      catch (Exception e)
      {
        m_Logger.Error(e, $"Error in Interceptor {nameof(OnLoad)}");
        throw;
      }
    }

    /// <summary>
    /// Update
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_Id"></param>
    /// <param name="i_CurrentState"></param>
    /// <param name="i_PreviousState"></param>
    /// <param name="i_PropertyNames"></param>
    /// <param name="i_Types"></param>
    /// <returns></returns>
    public override bool OnFlushDirty(object i_Entity, object i_Id, object[] i_CurrentState, object[] i_PreviousState, string[] i_PropertyNames, IType[] i_Types)
    {
      try
      {
        var res           = base.OnFlushDirty(i_Entity, i_Id, i_CurrentState, i_PreviousState, i_PropertyNames, i_Types);
        var entityState   = GenericEntityState<object>.Of(i_Entity, i_PropertyNames, i_CurrentState);
        var canRaiseEvent = CanRaiseUpdateEvent(i_Entity, i_PreviousState, i_CurrentState, i_PropertyNames);
        var changed       = ApplyModifyOperators(entityState, EntityModifyOperations.Update);
        i_CurrentState = entityState.State;
        if (canRaiseEvent)
        {
          RaiseEvent(i_Entity, i_CurrentState, i_PreviousState, i_PropertyNames, ConversationOperations.Update);
        }

        return changed || res;
      }
      catch (Exception e)
      {
        m_Logger.Error(e, $"Error in Interceptor {nameof(OnFlushDirty)}");
        throw;
      }
    }

    /// <summary>
    /// Insert
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_Id"></param>
    /// <param name="i_State"></param>
    /// <param name="i_PropertyNames"></param>
    /// <param name="i_Types"></param>
    /// <returns></returns>
    public override bool OnSave(object i_Entity, object i_Id, object[] i_State, string[] i_PropertyNames, IType[] i_Types)
    {
      try
      {
        var res         = base.OnSave(i_Entity, i_Id, i_State, i_PropertyNames, i_Types);
        var entityState = GenericEntityState<object>.Of(i_Entity, i_PropertyNames, i_State);
        var changed     = ApplyModifyOperators(entityState, EntityModifyOperations.Insert);
        i_State = entityState.State;
        RaiseEvent(i_Entity, i_State, null, i_PropertyNames, ConversationOperations.Save);
        return changed || res;
      }
      catch (Exception e)
      {
        m_Logger.Error(e, $"Error in Interceptor {nameof(OnSave)}");
        throw;
      }
    }

    /// <summary>
    /// Delete
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_Id"></param>
    /// <param name="i_State"></param>
    /// <param name="i_PropertyNames"></param>
    /// <param name="i_Types"></param>
    public override void OnDelete(object i_Entity, object i_Id, object[] i_State, string[] i_PropertyNames, IType[] i_Types)
    {
      try
      {
        RaiseEvent(i_Entity, i_State, null, i_PropertyNames, ConversationOperations.Delete);
        base.OnDelete(i_Entity, i_Id, i_State, i_PropertyNames, i_Types);
      }
      catch (Exception e)
      {
        m_Logger.Error(e, $"Error in Interceptor {nameof(OnDelete)}");
        throw;
      }
    }

    /// <summary>
    /// Applies the all modifiers for operation
    /// </summary>
    /// <param name="t_Entity"></param>
    /// <param name="i_Operation"></param>
    /// <returns></returns>
    private bool ApplyModifyOperators(IGenericEntityState<object> t_Entity, EntityModifyOperations i_Operation)
    {
      if (m_Modifier != null && m_Modifier.TryGetValue(i_Operation, out var modifiers))
      {
        var modifyResults = new List<bool>();
        foreach (var modifier in modifiers)
        {
          try
          {
            modifyResults.Add(modifier.ModifyNonGenericEntity(t_Entity, m_Flags, m_Context));
          }
          catch (Exception ex)
          {
            m_Logger.Error(ex, $"An error occured during execution of modifier {modifier.GetType().FullName}");
            if (modifier.ThrowOnError)
            {
              throw;
            }
          }
        }

        return modifyResults.Any(r => r);
      }

      return false;
    }

    private void RaiseEvent(object i_Entity, object[] i_NewState, object[] i_OldState, string[] i_PropertyNames, ConversationOperations i_Operation)
    {
      InterceptorEvent?.Invoke(this, new()
      {
        Entity    = i_Entity,
        Operation = i_Operation,
        Diff      = i_OldState != null ? m_DifferService?.DiffState(i_Entity, i_OldState, i_NewState, i_PropertyNames) : null
      });
    }

    private ValueTask OnNewBackgroundTask(IReadOnlyCollection<InterceptorBackgroundTask> i_BackgroundTasks, NiceCoreNHInterceptor i_State)
    {
      foreach (var i_BackgroundTask in i_BackgroundTasks)
      {
        m_Logger.Trace($"New Interceptor BackgroundTask for Operation {i_BackgroundTask.Operation} for entity of type {i_BackgroundTask.Entity.GetType()}");
        m_Logger.Trace($"Raising {i_BackgroundTask.Operation} Event for entity of type {i_BackgroundTask.Entity.GetType()}");

        i_State.InterceptorEvent?.Invoke(this, new()
        {
          Entity    = i_BackgroundTask.Entity,
          Operation = i_BackgroundTask.Operation,
          Diff      = i_BackgroundTask.OldState != null ? m_DifferService?.DiffState(i_BackgroundTask.Entity, i_BackgroundTask.OldState, i_BackgroundTask.NewState, i_BackgroundTask.PropertyNames) : null
        });
      }
      
      return ValueTask.CompletedTask;
    }

    private static bool CanRaiseUpdateEvent(object i_Entity, object[] i_OldState, object[] i_NewState, string[] i_PropertyNames)
    {
      return true;
    }

    /// <summary>
    /// Dispose
    /// </summary>
    public void Dispose()
    {
      InterceptorEvent = null;
    }
  }
}
