﻿using System;

namespace NiceCore.Storage.NH.Exceptions
{
  /// <summary>
  /// Exception thrown when 
  /// </summary>
#pragma warning disable RCS1194 // Implement exception constructors.
  public class ConnectionNotDefinedException : Exception
#pragma warning restore RCS1194 // Implement exception constructors.
  {
    private const string PARAM_CONNECTION = "%con";
    private const string CONNECTION_NOT_DEFINED = "Connection " + PARAM_CONNECTION + " is not defined in any config.";

    /// <summary>
    /// Constructor inserting Connection name into error message;
    /// </summary>
    /// <param name="i_ConnectionName"></param>
    public ConnectionNotDefinedException(string i_ConnectionName) : base(CreateErrorMessage(i_ConnectionName))
    {
    }

    private static string CreateErrorMessage(string i_ConnectionName)
    {
      return CONNECTION_NOT_DEFINED.Replace(PARAM_CONNECTION, i_ConnectionName);
    }
  }
}
