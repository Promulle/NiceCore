using System;
using NHibernate.Envers.Configuration.Fluent;

namespace NiceCore.Storage.NH.Auditing
{
  /// <summary>
  /// INcFluentAuditMapping
  /// </summary>
  public interface INcFluentAuditMapping
  {
    /// <summary>
    /// Mapped Type
    /// </summary>
    Type MappedType { get; }
    
    /// <summary>
    /// Apply
    /// </summary>
    /// <param name="i_Audit"></param>
    void Apply(IFluentAudit i_Audit);
  }
}
