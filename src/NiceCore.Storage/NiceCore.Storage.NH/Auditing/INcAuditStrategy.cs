using Microsoft.Extensions.Logging;
using NiceCore.Storage.Auditing;
using NiceCore.Storage.Conversations;

namespace NiceCore.Storage.NH.Auditing
{
  /// <summary>
  /// Nc Audit Strategy
  /// </summary>
  public interface INcAuditStrategy
  {
    /// <summary>
    /// Integrates the listener with niceCore
    /// </summary>
    /// <param name="i_Listener"></param>
    /// <param name="i_Logger"></param>
    void IntegrateWithNiceCore(IAuditOnPerformListener i_Listener, ILogger i_Logger);
  }
}