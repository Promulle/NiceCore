using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NHibernate.Envers.Query;
using NHibernate.Envers.Query.Criteria;
using NiceCore.Entities;
using NiceCore.Storage.Auditing;
using NiceCore.Storage.NH.Auditing.ResultExtraction;

namespace NiceCore.Storage.NH.Auditing
{
  /// <summary>
  /// Nh impl for INcAuditQuery
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <typeparam name="TId"></typeparam>
  public class NcNHAuditQuery<T, TId> : INcAuditQuery<T, TId>
    where T : IDomainEntity<TId>
  {
    private readonly IAuditQuery m_ActualQuery;
    private readonly IAuditQueryResultExtractor m_QueryResultExtractor;

    /// <summary>
    /// Ctor
    /// </summary>
    public NcNHAuditQuery(IAuditQuery i_Query, IAuditQueryResultExtractor i_AuditQueryResultExtractor)
    {
      if (i_Query == null)
      {
        throw new ArgumentException("can not create instance of NcNHAuditQuery with null query.");
      }
      m_ActualQuery = i_Query;
      m_QueryResultExtractor = i_AuditQueryResultExtractor ?? new AuditQueryResultExtractor();
    }

    /// <summary>
    /// Where ID is
    /// </summary>
    /// <param name="i_ID"></param>
    /// <returns></returns>
    public INcAuditQuery<T, TId> WhereIDIs(TId i_ID)
    {
      m_ActualQuery.Add(AuditEntity.Id().Eq(i_ID));
      return this;
    }

    /// <summary>
    /// Where Revision is
    /// </summary>
    /// <param name="i_RevisionNumber"></param>
    /// <returns></returns>
    public INcAuditQuery<T, TId> WhereRevisionIs(long i_RevisionNumber)
    {
      m_ActualQuery.Add(AuditEntity.RevisionNumber().Eq(i_RevisionNumber));
      return this;
    }

    /// <summary>
    /// Where Revisions are in i_RevisionNumbers
    /// </summary>
    /// <param name="i_RevisionNumbers"></param>
    /// <returns></returns>
    public INcAuditQuery<T, TId> WhereRevisionIsIn(IEnumerable<long> i_RevisionNumbers)
    {
      return GenerateWheresForIn(i_RevisionNumbers, val => WhereRevisionIs(val), val => AuditEntity.RevisionNumber().Eq(val));
    }

    /// <summary>
    /// adds conditions for all ids in i_Ids. Basically does a i_Ids.Contains(r.ID) with a lot of wheres
    /// </summary>
    /// <param name="i_Ids"></param>
    /// <returns></returns>
    public INcAuditQuery<T, TId> WhereIDIsIn(IEnumerable<TId> i_Ids)
    {
      return GenerateWheresForIn(i_Ids, val => WhereIDIs(val), val => AuditEntity.Id().Eq(val));
    }

    private INcAuditQuery<T, TId> GenerateWheresForIn<TValue>(IEnumerable<TValue> i_Values, Func<TValue, INcAuditQuery<T, TId>> i_SingleValueFunc, Func<TValue, IAuditCriterion> i_ConditionFunc)
    {
      var ids = i_Values.ToArray();
      if (ids.Length > 0)
      {
        if (ids.Length == 1)
        {
          return i_SingleValueFunc.Invoke(ids[0]);
        }
        var condition = i_ConditionFunc.Invoke(ids[0]);
        for (var i = 1; i < ids.Length; i++)
        {
          condition = AuditEntity.Or(condition, i_ConditionFunc.Invoke(ids[i]));
        }
        m_ActualQuery.Add(condition);
      }
      return this;
    }

    /// <summary>
    /// Executes the query async
    /// </summary>
    /// <returns></returns>
    public Task<NcAuditResult<T, TId>> GetResult()
    {
      var resultTask = Task.Run(() =>
      {
        var results = m_ActualQuery.GetResultList();
        return m_QueryResultExtractor.ExtractResults<T, TId>(results);
      });
      return resultTask;
    }
  }
}