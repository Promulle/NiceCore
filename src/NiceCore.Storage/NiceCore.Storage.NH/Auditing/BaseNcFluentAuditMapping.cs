using System;
using NHibernate.Envers.Configuration.Fluent;

namespace NiceCore.Storage.NH.Auditing
{
  /// <summary>
  /// BaseNcFluentAuditMapping
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public abstract class BaseNcFluentAuditMapping<T> : INcFluentAuditMapping
  {
    /// <summary>
    /// Mapped Type
    /// </summary>
    public Type MappedType { get; } = typeof(T);
    
    /// <summary>
    /// Apply
    /// </summary>
    /// <param name="i_Audit"></param>
    public abstract void Apply(IFluentAudit i_Audit);
  }
}