using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NHibernate.Envers;
using NiceCore.Entities;
using NiceCore.Storage.Auditing;

namespace NiceCore.Storage.NH.Auditing.ResultExtraction
{
  /// <summary>
  /// Result extractor used to get results from an auditQuery
  /// </summary>
  public class AuditQueryResultExtractor : IAuditQueryResultExtractor
  {
    /// <summary>
    /// Extract the results
    /// </summary>
    /// <param name="i_ResultList"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    public NcAuditResult<T, TId> ExtractResults<T, TId>(IList i_ResultList) where T : IDomainEntity<TId>
    {
      var actualResults = new List<NcSingleAuditResult<T, TId>>();
      foreach (var singleResult in i_ResultList)
      {
        if (singleResult is object[] revisionEntryArray)
        {
          if (revisionEntryArray.Length != 3)
          {
            throw new InvalidOperationException($"expected the entry of the result list of the audit query to have a length of 3. found: {revisionEntryArray.Length}");
          }
          var inst = ExtractEntity<T>(revisionEntryArray);
          var revEntity = ExtractRevisionEntity(revisionEntryArray);
          var revType = ExtractRevisionType(revisionEntryArray);
          var actualResult = new NcSingleAuditResult<T, TId>()
          {
            Entity = inst,
            RevisionID = revEntity.Id,
            AuditedOperation = GetAuditOperation(revType)
          };
          actualResults.Add(actualResult);
        }
        else
        {
          throw new InvalidOperationException("Result of audit query was not of type: object[]. Something might have changed with the envers implementation");
        }
      }
      return new ()
      {
        Results = actualResults.OrderBy(r => r.RevisionID).ToList()
      };
    }

    private static NcAuditedOperations GetAuditOperation(RevisionType i_Type)
    {
      return i_Type switch
      {
        RevisionType.Added => NcAuditedOperations.Create,
        RevisionType.Modified => NcAuditedOperations.Update,
        RevisionType.Deleted => NcAuditedOperations.Deleted,
        _ => NcAuditedOperations.None
      };
    }

    private static DefaultRevisionEntity ExtractRevisionEntity(object[] i_RevisionEntryArray)
    {
      if (i_RevisionEntryArray[1] is DefaultRevisionEntity type)
      {
        return type;
      }
      throw new InvalidOperationException($"Could not extract entity from audit result. Expected entity of type {typeof(DefaultRevisionEntity).FullName} at position 1 of array. Found instance of type {i_RevisionEntryArray[0].GetType().FullName}");
    }

    private static RevisionType ExtractRevisionType(object[] i_RevisionEntryArray)
    {
      if (i_RevisionEntryArray[2] is RevisionType type)
      {
        return type;
      }
      throw new InvalidOperationException($"Could not extract entity from audit result. Expected entity of type {typeof(RevisionType).FullName} at position 2 of array. Found instance of type {i_RevisionEntryArray[0].GetType().FullName}");
    }

    private static T ExtractEntity<T>(object[] i_RevisionEntryArray)
    {
      if (i_RevisionEntryArray[0] is T entity)
      {
        return entity;
      }

      throw new InvalidOperationException($"Could not extract entity from audit result. Expected entity of type {typeof(T).FullName} at position 0 of array. Found instance of type {i_RevisionEntryArray[0].GetType().FullName}");
    }
  }
}