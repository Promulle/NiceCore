using System.Collections;
using NiceCore.Entities;
using NiceCore.Storage.Auditing;

namespace NiceCore.Storage.NH.Auditing.ResultExtraction
{
  /// <summary>
  /// Result extractor used to get results from an auditQuery
  /// </summary>
  public interface IAuditQueryResultExtractor
  {
    /// <summary>
    /// Extract the results
    /// </summary>
    /// <param name="i_ResultList"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    NcAuditResult<T, TId> ExtractResults<T, TId>(IList i_ResultList) where T : IDomainEntity<TId>;
  }
}