using System;
using System.Collections.Generic;
using NHibernate.Envers;
using NiceCore.Storage.Auditing;
using NiceCore.Storage.Conversations;

namespace NiceCore.Storage.NH.Auditing.Impl
{
  /// <summary>
  /// Base Audit On Perform Listener
  /// </summary>
  public abstract class BaseAuditOnPerformListener : IAuditOnPerformListener
  {
    private const string REV_TYPE_NAME = "REVTYPE";

    /// <summary>
    /// On Audit Strategy Perform
    /// </summary>
    /// <param name="i_Context"></param>
    /// <param name="i_EntityName"></param>
    /// <param name="i_EntityType"></param>
    /// <param name="i_ID"></param>
    /// <param name="t_State"></param>
    public abstract void OnAuditStrategyPerform(IConversationContext i_Context, string i_EntityName, Type i_EntityType, object i_ID, IDictionary<string, object> t_State);

    /// <summary>
    /// Get Revision Type
    /// </summary>
    /// <param name="i_Data"></param>
    /// <returns></returns>
    protected RevisionType? GetRevisionType(IDictionary<string, object> i_Data)
    {
      if (i_Data.TryGetValue(REV_TYPE_NAME, out var revType) && revType is RevisionType revisionType)
      {
        return revisionType;
      }

      return null;
    }
  }
}