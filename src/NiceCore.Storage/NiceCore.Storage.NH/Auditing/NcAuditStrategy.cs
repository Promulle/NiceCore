using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Microsoft.Extensions.Logging;
using NHibernate;
using NHibernate.Envers.Configuration;
using NHibernate.Envers.Entities.Mapper;
using NHibernate.Envers.Entities.Mapper.Relation;
using NHibernate.Envers.Strategy;
using NHibernate.Envers.Tools.Query;
using NiceCore.Logging.Extensions;
using NiceCore.Storage.Auditing;
using NiceCore.Storage.Conversations;

namespace NiceCore.Storage.NH.Auditing
{
  /// <summary>
  /// Nc Audit Strategy
  /// </summary>
  public class NcAuditStrategy : IAuditStrategy, INcAuditStrategy
  {
    private readonly DefaultAuditStrategy m_Strategy = new();
    private IAuditOnPerformListener m_AuditOnPerformListener;
    private ILogger m_Logger;

    /// <summary>
    /// Initialize
    /// </summary>
    /// <param name="auditConfiguration"></param>
    public void Initialize(AuditConfiguration auditConfiguration)
    {
      m_Strategy.Initialize(auditConfiguration);
    }

    /// <summary>
    /// Perform
    /// </summary>
    /// <param name="session"></param>
    /// <param name="entityName"></param>
    /// <param name="id"></param>
    /// <param name="data"></param>
    /// <param name="revision"></param>
    public void Perform(ISession session, string entityName, object id, object data, object revision)
    {
      InvokeOnPerformListener(session, entityName, id, data, revision);
      m_Strategy.Perform(session, entityName, id, data, revision);
    }

    /// <summary>
    /// Integrate With NiceCore
    /// </summary>
    /// <param name="i_Listener"></param>
    /// <param name="i_Logger"></param>
    public void IntegrateWithNiceCore(IAuditOnPerformListener i_Listener, ILogger i_Logger)
    {
      m_AuditOnPerformListener = i_Listener;
      m_Logger = i_Logger;
    }

    private void InvokeOnPerformListener(ISession session, string entityName, object id, object data, object revision)
    {
      if (m_AuditOnPerformListener != null && data is Dictionary<string, object> dict)
      {
        var context = GetContextFromSession(session);
        if (context != null)
        {
          m_Logger.Trace($"Invoking {m_AuditOnPerformListener.GetType()} for entityName {entityName}");
          try
          {
            var metadata = session.SessionFactory.GetClassMetadata(entityName);
            m_AuditOnPerformListener.OnAuditStrategyPerform(context, entityName, metadata?.MappedClass, id, dict);
          }
          catch (Exception e)
          {
            m_Logger.Error(e, $"An error occurred during invocation of listener of type {m_AuditOnPerformListener.GetType()} for entity {entityName} with id {id}");
          }
        }
        else
        {
          m_Logger.Warning($"Tried invoking {m_AuditOnPerformListener.GetType()} but: given session did not have any conversationContext");
        }
      }
    }

    private static IConversationContext GetContextFromSession(ISession i_Session)
    {
      var interceptor = i_Session.GetSessionImplementation()?.Interceptor;
      if (interceptor is NiceCoreNHInterceptor ncInterceptor)
      {
        return ncInterceptor.GetCurrentContext();
      }
      return null;
    }

    /// <summary>
    /// PerformCollectionChange
    /// </summary>
    /// <param name="session"></param>
    /// <param name="entityName"></param>
    /// <param name="propertyName"></param>
    /// <param name="auditCfg"></param>
    /// <param name="persistentCollectionChangeData"></param>
    /// <param name="revision"></param>
    public void PerformCollectionChange(ISession session, string entityName, string propertyName, AuditConfiguration auditCfg, PersistentCollectionChangeData persistentCollectionChangeData, object revision)
    {
      m_Strategy.PerformCollectionChange(session, entityName, propertyName, auditCfg, persistentCollectionChangeData, revision);
    }

    /// <summary>
    /// AddEntityAtRevisionRestriction
    /// </summary>
    /// <param name="rootQueryBuilder"></param>
    /// <param name="parameters"></param>
    /// <param name="revisionProperty"></param>
    /// <param name="revisionEndProperty"></param>
    /// <param name="addAlias"></param>
    /// <param name="idData"></param>
    /// <param name="revisionPropertyPath"></param>
    /// <param name="originalIdPropertyName"></param>
    /// <param name="alias1"></param>
    /// <param name="alias2"></param>
    public void AddEntityAtRevisionRestriction(QueryBuilder rootQueryBuilder, Parameters parameters, string revisionProperty, string revisionEndProperty, bool addAlias, MiddleIdData idData, string revisionPropertyPath, string originalIdPropertyName, string alias1, string alias2)
    {
      m_Strategy.AddEntityAtRevisionRestriction(rootQueryBuilder, parameters, revisionProperty, revisionEndProperty, addAlias, idData, revisionPropertyPath, originalIdPropertyName, alias1, alias2);
    }

    /// <summary>
    /// AddAssociationAtRevisionRestriction
    /// </summary>
    /// <param name="rootQueryBuilder"></param>
    /// <param name="parameters"></param>
    /// <param name="revisionProperty"></param>
    /// <param name="revisionEndProperty"></param>
    /// <param name="addAlias"></param>
    /// <param name="referencingIdData"></param>
    /// <param name="versionsMiddleEntityName"></param>
    /// <param name="eeOriginalIdPropertyPath"></param>
    /// <param name="revisionPropertyPath"></param>
    /// <param name="originalIdPropertyName"></param>
    /// <param name="alias1"></param>
    /// <param name="inclusive"></param>
    /// <param name="componentDatas"></param>
    public void AddAssociationAtRevisionRestriction(QueryBuilder rootQueryBuilder, Parameters parameters, string revisionProperty, string revisionEndProperty, bool addAlias, MiddleIdData referencingIdData, string versionsMiddleEntityName, string eeOriginalIdPropertyPath, string revisionPropertyPath, string originalIdPropertyName, string alias1, bool inclusive, params MiddleComponentData[] componentDatas)
    {
      m_Strategy.AddAssociationAtRevisionRestriction(rootQueryBuilder, parameters, revisionProperty, revisionEndProperty, addAlias, referencingIdData, versionsMiddleEntityName, eeOriginalIdPropertyPath, revisionPropertyPath, originalIdPropertyName, alias1, inclusive, componentDatas);
    }

    /// <summary>
    /// AddExtraRevisionMapping
    /// </summary>
    /// <param name="classMapping"></param>
    /// <param name="revisionInfoRelationMapping"></param>
    public void AddExtraRevisionMapping(XElement classMapping, XElement revisionInfoRelationMapping)
    {
      m_Strategy.AddExtraRevisionMapping(classMapping, revisionInfoRelationMapping);
    }
  }
}