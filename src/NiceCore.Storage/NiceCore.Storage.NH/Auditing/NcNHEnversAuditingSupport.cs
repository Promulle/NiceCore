using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using NHibernate;
using NHibernate.Envers;
using NiceCore.Entities;
using NiceCore.Logging.Extensions;
using NiceCore.Storage.Auditing;
using NiceCore.Storage.NH.Conversation.Utils;

namespace NiceCore.Storage.NH.Auditing
{
  /// <summary>
  /// NH Envers Auditing Support
  /// </summary>
  public class NcNHEnversAuditingSupport : INcAuditingSupport
  {
    private readonly IAuditReader m_AuditReader;
    private readonly ISession m_ConvSession;
    private readonly string m_ConvName;
    
    /// <summary>
    /// Logger
    /// </summary>
    public ILogger Logger { get; }

    /// <summary>
    /// Constructor for audit support
    /// </summary>
    /// <param name="i_Session"></param>
    /// <param name="i_ConversationName"></param>
    /// <param name="i_Logger"></param>
    public NcNHEnversAuditingSupport(ISession i_Session, string i_ConversationName, ILogger i_Logger)
    {
      m_AuditReader = AuditReaderFactory.Get(i_Session);
      m_ConvSession = i_Session;
      m_ConvName = i_ConversationName;
      Logger = i_Logger;
    }

    /// <summary>
    /// Queries the audit system
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    public INcAuditQuery<T, TId> Query<T, TId>() where T : IDomainEntity<TId>
    {
      var qry = m_AuditReader.CreateQuery().ForRevisionsOfEntity(typeof(T), false, true);
      return new NcNHAuditQuery<T, TId>(qry, null);
    }
    
    /// <summary>
    /// Queries the audit system
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    public INcAuditQuery<T, TId> Query<T, TId>(int i_RevisionID) where T : IDomainEntity<TId>
    {
      var qry = m_AuditReader.CreateQuery().ForEntitiesAtRevision(typeof(T), i_RevisionID);
      return new NcNHAuditQuery<T, TId>(qry, null);
    }

     /// <summary>
    /// Finds the revision for i_ID and i_Revision
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_ID"></param>
    /// <param name="i_Revision"></param>
    /// <returns></returns>
    public T FindRevision<T>(long i_ID, long i_Revision) where T : IBaseDomainEntity
    {
      return FindRevision<T, long>(i_ID, i_Revision);
    }

    /// <summary>
    /// Finds the revision for i_ID and i_Revision
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <param name="i_ID"></param>
    /// <param name="i_Revision"></param>
    /// <returns></returns>
    public T FindRevision<T, TId>(TId i_ID, long i_Revision) where T : IDomainEntity<TId>
    {
      if (SessionReadyChecker.IsSessionReady(m_ConvSession, nameof(FindRevision), Logger))
      {
        if (m_AuditReader != null)
        {
          Logger.Information($"AuditReader is enabled, FindRevision called for entity of type {typeof(T).FullName} with id {i_ID} in revision {i_Revision}.");
          return m_AuditReader.Find<T>(i_ID, i_Revision);
        }
        var msg = $"FindRevision called but auditreader is not set for this conversation for connection {m_ConvName}. Returning current entity for id {i_ID} instead.";
        Logger.LogError(msg);
        throw new InvalidOperationException(msg);
      }
      return default;
    }

    /// <summary>
    /// Returns all possible revisions of an entity of type T with id specified by i_Id
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <param name="i_Id"></param>
    /// <returns></returns>
    public IEnumerable<long> GetRevisions<T, TId>(TId i_Id) where T : IDomainEntity<TId>
    {
      if (m_AuditReader == null)
      {
        Logger.Warning("GetRevisions was called but auditing is not enabled. Returning empty list");
        return new List<long>();
      }
      return m_AuditReader.GetRevisions(typeof(T), i_Id);
    }
    
    /// <summary>
    /// Checks if T is audited
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    public bool IsEntityClassAudited<T, TId>() where T : IDomainEntity<TId>
    {
      if (m_AuditReader == null)
      {
        Logger.Warning($"IsEntityClassAudited was called but auditing is not enabled for conversation of connection {m_ConvName}. Returning false.");
        return false;
      }
      return m_AuditReader.IsEntityClassAudited(typeof(T));
    }

    /// <summary>
    /// Return the revision number for date
    /// </summary>
    /// <param name="i_Date"></param>
    /// <returns></returns>
    public long GetRevisionNumberForDate(DateTime i_Date)
    {
      if (m_AuditReader == null)
      {
        Logger.Warning($"GetRevisionNumberForDate has been called but auditreader was not set for conversation of connection {m_ConvName}. Returning -1");
        return -1;
      }
      return m_AuditReader.GetRevisionNumberForDate(i_Date);
    }

    /// <summary>
    /// Returns the date for a revision
    /// </summary>
    /// <param name="i_Revision"></param>
    /// <returns></returns>
    public DateTime GetRevisionDate(long i_Revision)
    {
      if (m_AuditReader == null)
      {
        Logger.Warning($"GetRevisionDate has been called but auditreader was not set for conversation of connection {m_ConvName}. Returning now.");
        return DateTime.Now;
      }
      return m_AuditReader.GetRevisionDate(i_Revision);
    }
  }
}