using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using NHibernate.Proxy;
using NiceCore.Storage.Conversations.Factories.Extensions;
using NiceCore.Storage.NH.Tests.Impls;
using NUnit.Framework;

namespace NiceCore.Storage.NH.Tests.Proxies
{
  [TestFixture]
  public class LazyProxyTests: BaseStorageTest
  {
    /// <summary>
    /// Test fetch reference name
    /// </summary>
    public const string TEST_FETCH_REFERENCE_NAME = "fetched";
    
    /// <summary>
    /// Test Fetch then name
    /// </summary>
    public const string TEST_FETCH_THEN_NAME = "ThenFetched";
    
    [Test]
    public async Task TestUnproxyWithSecondSession()
    {
      using (var conv = await StorageService.ConversationFactory.Conversation(ConnectionName).ConfigureAwait(false))
      {
        var thenEnt = new ThenEntity() {Name = TEST_FETCH_THEN_NAME};
        var newEnt = new ReferencedEntity() {Name = TEST_FETCH_REFERENCE_NAME, ThenFetchEntity = thenEnt};
        var entTest = new TestEntity() {Name = nameof(ReferencedEntity.ThenFetchEntity), OtherEntity = newEnt};
        conv.SaveOrUpdate(thenEnt);
        conv.SaveOrUpdate(newEnt);
        conv.SaveOrUpdate(entTest);
        await conv.ApplyAsync();
        await conv.ApplyAsync().ConfigureAwait(false);
      }

      using var loadConv = await StorageService.ConversationFactory.Conversation(ConnectionName).ConfigureAwait(false);
      var loaded = await loadConv.Query<ReferencedEntity, long>().ToListAsync().ConfigureAwait(false);
      loaded.Should().NotBeNullOrEmpty();
      var first = loaded.First();
      using var unproxyConv = await StorageService.ConversationFactory.Conversation(ConnectionName).ConfigureAwait(false);
      var unproxied = unproxyConv.UnproxyWithInitialize<ThenEntity, long>(first.ThenFetchEntity);
      unproxied.Should().NotBeNull().And.NotBeAssignableTo<INHibernateProxy>();
    }
    
    [Test]
    public async Task TestUnproxyWithSecondSessionWhileLoadSessionClosed()
    {
      using (var conv = await StorageService.ConversationFactory.Conversation(ConnectionName).ConfigureAwait(false))
      {
        var thenEnt = new ThenEntity() {Name = TEST_FETCH_THEN_NAME};
        var newEnt = new ReferencedEntity() {Name = TEST_FETCH_REFERENCE_NAME, ThenFetchEntity = thenEnt};
        var entTest = new TestEntity() {Name = nameof(ReferencedEntity.ThenFetchEntity), OtherEntity = newEnt};
        conv.SaveOrUpdate(thenEnt);
        conv.SaveOrUpdate(newEnt);
        conv.SaveOrUpdate(entTest);
        await conv.ApplyAsync();
        await conv.ApplyAsync().ConfigureAwait(false);
      }

      ReferencedEntity first = null;
      using (var loadConv = await StorageService.ConversationFactory.Conversation(ConnectionName).ConfigureAwait(false))
      {
        var loaded = await loadConv.Query<ReferencedEntity, long>().ToListAsync().ConfigureAwait(false);
        loaded.Should().NotBeNullOrEmpty();
        first = loaded.First();
      }
      
      using var unproxyConv = await StorageService.ConversationFactory.Conversation(ConnectionName).ConfigureAwait(false);
      var unproxied = unproxyConv.UnproxyWithInitialize<ThenEntity, long>(first.ThenFetchEntity);
      unproxied.Should().NotBeNull().And.NotBeAssignableTo<INHibernateProxy>();
    }
  }
}