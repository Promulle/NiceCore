using System;
using System.Threading.Tasks;
using FluentAssertions;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Conversations.Factories;
using NiceCore.Storage.Conversations.Factories.Extensions;
using NUnit.Framework;

namespace NiceCore.Storage.NH.Tests
{
  [TestFixture]
  public class ActiveConversationStoreTests : BaseStorageTest
  {
    [Test]
    public async Task TestGetActiveConversation()
    {
      var factory = Container.Resolve<IConversationFactory>();
      factory.Should().NotBeNull();
      var activeConversationStore = Container.Resolve<IActiveConversationStore>();
      activeConversationStore.Should().NotBeNull();
      Guid? convId = null;
      using (var conv = await factory.Conversation(ConnectionName).ConfigureAwait(false))
      {
        convId = conv.GetIdentifier();
        convId.Should().NotBeNull();
        var retrieved = activeConversationStore.GetActiveConversation(convId!.Value);
        retrieved.Should().NotBeNull().And.Be(conv);
      }

      var retrievedAfterDispose = activeConversationStore.GetActiveConversation(convId.Value);
      retrievedAfterDispose.Should().BeNull();
    }
  }
}