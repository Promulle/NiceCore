using System;
using NiceCore.Storage.Services;

namespace NiceCore.Storage.NH.Tests.ConnectionDefinitions.Model
{
  public class TestConnectionDefinitionIntegrityCheck : IConnectionDefinitionIntegrityCheck
  {
    public const string CORRECT_NAME = "Correct";
    public const string WRONG_NAME = "Wrong";
    
    public void EnsureIntegrityOfConnectionDefinition(IConnectionDefinition i_Definition)
    {
      if (i_Definition.Name == WRONG_NAME)
      {
        throw new InvalidOperationException("Wrong Connection definition detected");
      }
    }
  }
}