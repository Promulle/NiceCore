using System;
using System.Collections.Generic;
using FluentAssertions;
using NiceCore.Storage.NH.Tests.ConnectionDefinitions.Model;
using NiceCore.Storage.Services;
using NiceCore.Storage.Services.Impl;
using NUnit.Framework;

namespace NiceCore.Storage.NH.Tests.ConnectionDefinitions
{
  [TestFixture]
  public class ConnectionDefinitionManagementServiceTests
  {
    [Test]
    public void TestIntegrityChecks()
    {
      var service = new ConnectionDefinitionManagementService
      {
        IntegrityChecks = new List<IConnectionDefinitionIntegrityCheck>()
        {
          new TestConnectionDefinitionIntegrityCheck()
        }
      };
      var correctDefinition = new ConnectionDefinition()
      {
        Name = TestConnectionDefinitionIntegrityCheck.CORRECT_NAME
      };
      service.AddConnectionDefinition(correctDefinition, false);
      service.GetConnectionDefinition(TestConnectionDefinitionIntegrityCheck.CORRECT_NAME).Should().NotBeNull();
      var wrongDefinition = new ConnectionDefinition()
      {
        Name = TestConnectionDefinitionIntegrityCheck.WRONG_NAME
      };
      var func = () => service.AddConnectionDefinition(wrongDefinition, false);
      func.Should().Throw<InvalidOperationException>();
      service.GetConnectionDefinition(wrongDefinition.Name).Should().BeNull();
    }
  }
}