namespace NiceCore.Storage.NH.Tests.Diffing.Model
{
  /// <summary>
  /// Entity to be diffed
  /// </summary>
  public sealed class DiffEntity
  {
    /// <summary>
    /// Number
    /// </summary>
    public int Number { get; set; }
    
    /// <summary>
    /// Word
    /// </summary>
    public string Word { get; set; }
    
    /// <summary>
    /// Ref
    /// </summary>
    public DiffSubEntity Ref { get; set; }
  }
}