using System;
using NiceCore.Entities;
using NiceCore.ServiceLocation;
using NiceCore.Storage.NH.Diffing;

namespace NiceCore.Storage.NH.Tests.Diffing.Model
{
  /// <summary>
  /// Test Differ
  /// </summary>
  [Register(InterfaceType = typeof(INHDiffer))]
  public sealed class TestDiffer : BaseNHDiffer
  {
    /// <summary>
    /// Always use
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <returns></returns>
    public override bool CanBeUsedFor(object i_Instance)
    {
      return true;
    }

    /// <summary>
    /// Get entity type
    /// </summary>
    /// <returns></returns>
    protected override Type GetEntityType()
    {
      return typeof(IBaseDomainEntity);
    }

    protected override object GetID(object i_Value)
    {
      if (i_Value is IBaseDomainEntity casted)
      {
        return casted.ID;
      }

      return null;
    }
  }
}