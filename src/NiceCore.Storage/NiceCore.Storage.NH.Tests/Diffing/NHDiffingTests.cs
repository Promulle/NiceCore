using System.Collections.Generic;
using FluentAssertions;
using NiceCore.Storage.Diffing;
using NiceCore.Storage.NH.Diffing;
using NiceCore.Storage.NH.Tests.Diffing.Model;
using NUnit.Framework;

namespace NiceCore.Storage.NH.Tests.Diffing
{
  /// <summary>
  /// NH Diffing tests
  /// </summary>
  [TestFixture]
  public sealed class NHDiffingTests
  {
    /// <summary>
    /// Test Differ
    /// </summary>
    [Test]
    public void TestNHDiffer()
    {
      var firstDiff = new DiffEntity()
      {
        Number = 10,
        Word = "Test"
      };
      var secondDiff = new DiffEntity()
      {
        Number = 10,
        Word = "Test2",
        Ref = new()
        {
          ID = 1
        }
      };
      var format = NHFormat.ToNHFormat(firstDiff, secondDiff);
      var differ = new TestDiffer();
      var res = differ.DiffState(firstDiff.GetType(), format.OldState, format.NewState, format.PropertyNames);
      res.Should().NotBeNull();
      res.Entries.Should().NotBeNull().And.NotBeEmpty();
      res.Entries.Should().Contain(r => CheckDiffResult(r, nameof(DiffEntity.Word), "Test", "Test2"))
        .And.Contain(r => CheckDiffResult(r, nameof(DiffEntity.Ref), null, 1L));
    }
    
    /// <summary>
    /// Test Differ
    /// </summary>
    [Test]
    public void TestNHDifferPropertyNamesOnly()
    {
      var firstDiff = new DiffEntity()
      {
        Number = 10,
        Word = "Test"
      };
      var secondDiff = new DiffEntity()
      {
        Number = 10,
        Word = "Test2",
        Ref = new()
        {
          ID = 1
        }
      };
      var format = NHFormat.ToNHFormat(firstDiff, secondDiff);
      var differ = new TestDiffer();
      var res = differ.GetChangedProperties(firstDiff.GetType(), format.OldState, format.NewState, format.PropertyNames);
      res.Should().NotBeNull();
      res.Should().Contain(nameof(DiffEntity.Word))
        .And.Contain(nameof(DiffEntity.Ref))
        .And.NotContain(nameof(DiffEntity.Number));
    }

    private static bool CheckDiffResult(KeyValuePair<string, StorageDiffResultEntry> i_KeyValuePair, string i_ExpectedName, object i_ExpectedOldValue, object i_ExpectedNewValue)
    {
      return i_KeyValuePair.Key == i_ExpectedName &&
             i_KeyValuePair.Value != null &&
             i_KeyValuePair.Value.PropertyName == i_ExpectedName &&
             i_KeyValuePair.Value.OldValue == i_ExpectedOldValue &&
             object.Equals(i_KeyValuePair.Value.NewValue, i_ExpectedNewValue);
    }
  }
}