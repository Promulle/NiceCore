﻿using NiceCore.ServiceLocation;
using NiceCore.Storage.DomainBuilder;

namespace NiceCore.Storage.NH.Tests
{
  /// <summary>
  /// Domain builder for testing
  /// </summary>
  [Register(InterfaceType = typeof(IDomainBuilder), LifeStyle = LifeStyles.Singleton)]
  public class TestDomainBuilder : BaseDomainBuilder
  {
    /// <summary>
    /// default ctor
    /// </summary>
    public TestDomainBuilder()
    {
      AllowSchemaUpdate = true;
      //m_AllowedConversations.Add("test");
    }
  }
}
