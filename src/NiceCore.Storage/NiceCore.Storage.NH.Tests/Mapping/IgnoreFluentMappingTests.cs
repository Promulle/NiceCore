using System;
using System.Collections.Generic;
using FluentAssertions;
using NiceCore.Storage.NH.Tests.Impls.Fluent;
using NUnit.Framework;

namespace NiceCore.Storage.NH.Tests.Mapping
{
  /// <summary>
  /// Ignore Fluent Mapping Tests
  /// </summary>
  [TestFixture]
  public class IgnoreFluentMappingTests : BaseStorageTest
  {
    /// <summary>
    /// Get Configuration Dictionary
    /// </summary>
    /// <returns></returns>
    protected override IDictionary<string, string> GetConfigurationDictionary()
    {
      return new Dictionary<string, string>()
      {
        {"NcTest:ShowSql", "true"},
        {"NcTest:DriverClass", "NHibernate.Driver.NpgsqlDriver"},
        {"NcTest:Dialect", "NHibernate.Dialect.PostgreSQL83Dialect"},
        {"NcTest:ConnectionString", $"{NiceCoreStorageNHTestsConstants.GetConnectionString()}"},
        {"NiceCore:Storage:NH:ExcludedTypes:One", $"{typeof(FluentlyMappedEntity).FullName}, {typeof(FluentlyMappedEntity).Assembly.GetName().Name}"},
        {"NiceCore:Storage:NH:ExcludedTypes:Two", $"{typeof(SecondFluentlyMappedEntity).FullName}, {typeof(FluentlyMappedEntity).Assembly.GetName().Name}"}
      };
    }

    /// <summary>
    /// Tests ignoring of fluently mapped entities
    /// </summary>
    [Test]
    public void IgnoreFluentlyMappedEntity()
    {
      var actionForTypeOne = async () =>
      {
        using var conv = StorageService.Conversation(ConnectionName);
        var res = await conv.Query<FluentlyMappedEntity>()
          .ToListAsync();
      };
      actionForTypeOne.Should().ThrowAsync<Exception>("This proves that fluentlymappedEntity is not mapped");
      var actionForTypeTwo = async () =>
      {
        using var conv = StorageService.Conversation(ConnectionName);
        var res = await conv.Query<SecondFluentlyMappedEntity>()
          .ToListAsync();
      };
      actionForTypeTwo.Should().ThrowAsync<Exception>("This proves that secondfluentlymappedEntity is not mapped");
    }
  }
}