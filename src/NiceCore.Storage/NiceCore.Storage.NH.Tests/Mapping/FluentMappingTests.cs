using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using NiceCore.Storage.NH.Tests.Impls;
using NiceCore.Storage.NH.Tests.Impls.Fluent;
using NUnit.Framework;

namespace NiceCore.Storage.NH.Tests.Mapping
{
  /// <summary>
  /// Fluenty mapping tests
  /// </summary>
  [TestFixture]
  public sealed class FluentMappingTests : BaseStorageTest
  {
    private const string TEST_FLUENT_NAME = "flent";
    private const string TEST_FLUENT_ENTITY_NAME = "entName";
    
    /// <summary>
    /// Entity that is fluently mapped
    /// </summary>
    [Test]
    public async Task FluentMappedEntity()
    {
      var refer = new ReferencedEntity()
      {
        Name = TEST_FLUENT_ENTITY_NAME,
      };
      var flEnt = new FluentlyMappedEntity()
      {
        Entity = refer,
        Name = TEST_FLUENT_NAME,
      };
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        conv.SaveOrUpdate(refer);
        conv.SaveOrUpdate(flEnt);
        await conv.ApplyAsync();
      }

      FluentlyMappedEntity loaded = null;
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        loaded = conv.Query<FluentlyMappedEntity>()
          .Fetch(r => r.Entity)
          .ToList()
          .FirstOrDefault();
      }

      loaded.Should().NotBeNull();
      loaded!.Name.Should().Be(TEST_FLUENT_NAME);
      loaded.Entity.Should().NotBeNull();
      loaded.Entity.Name.Should().Be(TEST_FLUENT_ENTITY_NAME);
    }
  }
}