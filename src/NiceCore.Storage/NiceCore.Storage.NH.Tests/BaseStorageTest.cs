﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using NiceCore.Rapid;
using NiceCore.Rapid.ApplicationBuilding;
using NiceCore.ServiceLocation;
using NiceCore.ServiceLocation.CastleWindsor;
using NiceCore.Services.Config.Impl;
using NiceCore.Storage.NH.Tests.Utils;
using NiceCore.Storage.Services;
using NUnit.Framework;
using NiceCore.Rapid.Storage;
using NiceCore.ServiceLocation.Discovery.ServiceDescriptors;
using NiceCore.Storage.Conversations.Factories;

namespace NiceCore.Storage.NH.Tests
{
  /// <summary>
  /// Base class for storage tests
  /// </summary>
  public class BaseStorageTest
  {
    private const string CONNECTION_NAME = "unit_test";

    private PostgresRespawner m_Respawner;

    /// <summary>
    /// Connection Name
    /// </summary>
    public string ConnectionName { get; } = CONNECTION_NAME;

    /// <summary>
    /// Storage Service
    /// </summary>
    public IStorageService StorageService { get; private set; }
    
    public IConversationFactory ConversationFactory { get; private set; }

    /// <summary>
    /// Container
    /// </summary>
    public IServiceContainer Container { get; private set; }

    /// <summary>
    /// Setup
    /// </summary>
    [OneTimeSetUp]
    public virtual async Task SetupApp()
    {
      var extensionStore = new DefaultAppBuilderExtensionStore();
      var app = await NcApplicationBuilder
        .Create<NcStorageApplication>()
        .ServiceLocation.UseContainerType<CastleWindsorServiceContainer>()
        .ServiceLocation.DefaultPlugins()
        .BeforeServicesRegistered.Run(collection => BeforeServicesRegistered(collection))
        .Configuration.Default()
        .Configuration.ConfigurationSources.AddInstance(new DefaultInMemoryNcConfigurationSource(GetConfigurationDictionary()))
        .StorageService(extensionStore).Default()
        .Build().ConfigureAwait(false);
      m_Respawner = await PostgresRespawner.Create();
      Container = app.ServiceContainer;
      StorageService = Container.Resolve<IStorageService>();
      StorageService.Should().NotBeNull();

      ConversationFactory = Container.Resolve<IConversationFactory>();
      ConversationFactory.Should().NotBeNull();
      
      var conDefManager = Container.Resolve<IConnectionDefinitionManagementService>();
      conDefManager.AddConnectionDefinition(new ConnectionDefinition()
      {
        Name = CONNECTION_NAME,
        AllowAuditing = true,
        SchemaUpdate = true,
        UseSerialization = false,
        ConfigFile = "NcTest"
      }, true);
      BeforeStorageServiceInitialize();
      //initialize storage service
      StorageService.Initialize();
    }

    protected virtual ValueTask BeforeServicesRegistered(NcServiceCollection t_Collection)
    {
      return ValueTask.CompletedTask;
    }

    protected virtual void BeforeStorageServiceInitialize()
    {
      
    }

    /// <summary>
    /// Get Configuration Dictionary
    /// </summary>
    /// <returns></returns>
    protected virtual IDictionary<string, string> GetConfigurationDictionary()
    {
      return new Dictionary<string, string>()
      {
        {"NcTest:ShowSql", "true"},
        {"NcTest:DriverClass", "NHibernate.Driver.NpgsqlDriver"},
        {"NcTest:Dialect", "NHibernate.Dialect.PostgreSQL83Dialect"},
        {"NcTest:ConnectionString", $"{NiceCoreStorageNHTestsConstants.GetConnectionString()}"}
      };
    }

    /// <summary>
    /// Tear Down
    /// </summary>
    [OneTimeTearDown]
    public async Task TearDown()
    {
      await m_Respawner.Reset();
    }
  }
}