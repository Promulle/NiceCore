using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using NiceCore.ServiceLocation;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Conversations.Factories;
using NiceCore.Storage.Conversations.Factories.Extensions;
using NiceCore.Storage.NH.Tests.Impls;
using NiceCore.Storage.NH.Tests.Modifers.Impls;
using NUnit.Framework;

namespace NiceCore.Storage.NH.Tests.Modifers
{
  [TestFixture]
  public class ModifierTests : BaseStorageTest
  {
    public override async Task SetupApp()
    {
      await base.SetupApp();
      Container.Register<IEntityOperationModifier, TestInsertModifier>(LifeStyles.Singleton, null);
      Container.Register<IEntityOperationModifier, TestUpdateModifier>(LifeStyles.Singleton, null);
    }

    [Test]
    public async Task TestInsertModifiers()
    {
      using var conv = await StorageService.ConversationFactory.Conversation(ConnectionName);
      await conv.SaveOrUpdateAsync(new TestEntity()
      {
        Name = "something"
      });
      await conv.ApplyAsync();
      await Task.Delay(1000);
      var modifier = Container.ResolveAll<IEntityOperationModifier>().First(r => r.Operations.Contains(EntityModifyOperations.Insert));
      modifier.Should().BeOfType<TestInsertModifier>();
      (modifier as TestInsertModifier)!.WasRun.Should().BeTrue();
    }
    
    [Test]
    public async Task TestUpdateModifiers()
    {
      var id = -1L;
      using (var conv = await StorageService.ConversationFactory.Conversation(ConnectionName))
      {
        var ent = new TestEntity()
        {
          Name = "something"
        };
        await conv.SaveOrUpdateAsync(ent);
        id = ent.ID;
        await conv.ApplyAsync();
      }

      using (var conv = await StorageService.ConversationFactory.Conversation(ConnectionName))
      {
        var ent = await conv.Query<TestEntity>()
          .Where(r => r.ID == id)
          .FirstOrDefaultAsync();
        ent.Name = "something changed";
        await conv.ApplyAsync();
      }

      await Task.Delay(1000);

      var modifier = Container.ResolveAll<IEntityOperationModifier>().First(r => r.Operations.Contains(EntityModifyOperations.Update));
      modifier.Should().BeOfType<TestUpdateModifier>();
      (modifier as TestUpdateModifier)!.WasRun.Should().BeTrue();
    }
  }
}