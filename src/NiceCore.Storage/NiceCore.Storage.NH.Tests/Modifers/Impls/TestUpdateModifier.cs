using System.Collections.Generic;
using NiceCore.Storage.Conversations;

namespace NiceCore.Storage.NH.Tests.Modifers.Impls
{
  public sealed class TestUpdateModifier : BaseTestModifier
  {
    public override IEnumerable<EntityModifyOperations> Operations { get; } = new[]
    {
      EntityModifyOperations.Update
    };
  }
}