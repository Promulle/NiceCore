using System.Collections.Generic;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Conversations.Flags;
using NiceCore.Storage.Helpers;

namespace NiceCore.Storage.NH.Tests.Modifers.Impls
{
  public abstract class BaseTestModifier : IEntityOperationModifier
  {
    public bool WasRun { get; private set; }
    
    public bool ThrowOnError { get; } = false;

    public abstract IEnumerable<EntityModifyOperations> Operations { get; }

    public bool ModifyEntity<TEntity, TId>(IGenericEntityState<TEntity> i_Entity, ConversationOperationFlags i_Flags, IConversationContext i_Context) where TEntity : class
    {
      return true;
    }

    public bool ModifyNonGenericEntity(IGenericEntityState<object> i_Entity, ConversationOperationFlags i_Flags, IConversationContext i_Context)
    {
      WasRun = true;
      return true;
    } 
  }
}