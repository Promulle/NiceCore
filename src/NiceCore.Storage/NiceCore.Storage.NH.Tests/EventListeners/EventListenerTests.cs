using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using NiceCore.ServiceLocation.Discovery.ServiceDescriptors;
using NiceCore.Storage.Conversations.EventListeners;
using NiceCore.Storage.Conversations.Factories.Extensions;
using NiceCore.Storage.NH.Tests.EventListeners.Model;
using NiceCore.Storage.NH.Tests.Impls;
using NUnit.Framework;

namespace NiceCore.Storage.NH.Tests.EventListeners
{
  [TestFixture]
  public class EventListenerTests : BaseStorageTest
  {
    protected override ValueTask BeforeServicesRegistered(NcServiceCollection t_Collection)
    {
      t_Collection.AddTransient<INcConversationPostSaveOrUpdateEventListener, TestSaveOrUpdateListener>();
      t_Collection.AddTransient<INcConversationPostDeleteEventListener, TestDeleteListener>();
      t_Collection.AddTransient<INcConversationPostSaveOrUpdateEventListener, TestNotRunSaveOrUpdateListener>();
      return ValueTask.CompletedTask;
    }

    //[Test]
    public async Task TestSaveOrUpdateListener()
    {
      using (var conv = await ConversationFactory.Conversation(ConnectionName).ConfigureAwait(false))
      {
        var newEntity = new TestEntity()
        {
          Name = nameof(TestSaveOrUpdateListener)
        };
        await conv.SaveOrUpdateAsync<TestEntity>(newEntity).ConfigureAwait(false);
        await conv.ApplyAsync().ConfigureAwait(false);
      }

      using var readConv = await ConversationFactory.Conversation(ConnectionName).ConfigureAwait(false);
      var entities = await readConv.Query<ReferencedEntity>()
        .Where(r => r.Name == Model.TestSaveOrUpdateListener.TEST_ENTITY_NAME)
        .ToListAsync().ConfigureAwait(false);
      entities.Should().HaveCount(0);
    }
    
    [Test]
    public async Task TestDeleteListener()
    {
      var names = new string[]
      {
        nameof(TestDeleteListener),
        Model.TestDeleteListener.TEST_ENTITY_NAME
      };
      using (var conv = await ConversationFactory.Conversation(ConnectionName).ConfigureAwait(false))
      {
        var newEntity = new TestEntity()
        {
          Name = nameof(TestDeleteListener)
        };
        var newEntity2 = new ReferencedEntity()
        {
          Name = Model.TestDeleteListener.TEST_ENTITY_NAME
        };
        await conv.SaveOrUpdateAsync<TestEntity>(newEntity).ConfigureAwait(false);
        await conv.SaveOrUpdateAsync<ReferencedEntity>(newEntity2).ConfigureAwait(false);
        await conv.ApplyAsync().ConfigureAwait(false);
      }

      using (var conv2 = await ConversationFactory.Conversation(ConnectionName).ConfigureAwait(false))
      {
        var loadedEntities = await conv2.Query<TestEntity>()
          .Where(r => r.Name == nameof(TestDeleteListener))
          .ToListAsync().ConfigureAwait(false);
        loadedEntities.Should().HaveCount(1);
        var entityToDelete = loadedEntities.First(r => r.Name == nameof(TestDeleteListener));
        await conv2.DeleteAsync<TestEntity>(entityToDelete).ConfigureAwait(false);
        await conv2.ApplyAsync().ConfigureAwait(false);
      }

      using var readConv = await ConversationFactory.Conversation(ConnectionName).ConfigureAwait(false);
      var entities = await readConv.Query<ReferencedEntity>()
        .Where(r => r.Name == Model.TestDeleteListener.TEST_ENTITY_NAME)
        .ToListAsync().ConfigureAwait(false);
      entities.Should().HaveCount(0);
    }
  }
}