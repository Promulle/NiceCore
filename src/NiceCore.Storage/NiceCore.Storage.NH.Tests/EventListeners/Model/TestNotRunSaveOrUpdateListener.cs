using System;
using System.Threading;
using System.Threading.Tasks;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Conversations.EventListeners;
using NiceCore.Storage.NH.Tests.Impls;

namespace NiceCore.Storage.NH.Tests.EventListeners.Model
{
  public class TestNotRunSaveOrUpdateListener : BaseNcConversationPostSaveOrUpdateEventListener<TestEntity>
  {
    public override bool CanBeUsedForConnection(string i_ConnectionName)
    {
      return false;
    }

    protected override ValueTask PostSaveOrUpdate(TestEntity i_Entity, TestEntity i_ResultEntity, INotApplyingConversation t_Conversation, CancellationToken i_CancellationToken)
    {
      throw new Exception("unlucky");
    }
  }
}