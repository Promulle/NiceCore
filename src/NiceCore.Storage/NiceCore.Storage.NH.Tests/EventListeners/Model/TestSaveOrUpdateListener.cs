using System.Threading;
using System.Threading.Tasks;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Conversations.EventListeners;
using NiceCore.Storage.NH.Tests.Impls;

namespace NiceCore.Storage.NH.Tests.EventListeners.Model
{
  public class TestSaveOrUpdateListener : BaseNcConversationPostSaveOrUpdateEventListener<TestEntity>
  {
    public const string TEST_ENTITY_NAME = "Save-Entity-Name";
    
    protected override async ValueTask PostSaveOrUpdate(TestEntity i_Entity, TestEntity i_ResultEntity, INotApplyingConversation t_Conversation, CancellationToken i_CancellationToken)
    {
      var secondEntity = new ReferencedEntity()
      {
        Name = TEST_ENTITY_NAME
      };
      await t_Conversation.SaveOrUpdateAsync(secondEntity).ConfigureAwait(false);
    }
  }
}