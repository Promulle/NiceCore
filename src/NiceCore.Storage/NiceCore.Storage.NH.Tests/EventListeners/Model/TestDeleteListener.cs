using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Conversations.EventListeners;
using NiceCore.Storage.NH.Tests.Impls;

namespace NiceCore.Storage.NH.Tests.EventListeners.Model
{
  public class TestDeleteListener : BaseNcConversationPostDeleteListener<TestEntity>
  {
    public const string TEST_ENTITY_NAME = "Delete-Entity-Name";
    
    protected override async ValueTask PostDelete(TestEntity i_Entity, INotApplyingConversation t_Conversation, CancellationToken i_CancellationToken)
    {
      var entities = await t_Conversation.Query<ReferencedEntity>()
        .Where(r => r.Name == TEST_ENTITY_NAME)
        .ToListAsync().ConfigureAwait(false);
      if (entities.Count > 0)
      {
        await t_Conversation.DeleteAsync(entities[0]).ConfigureAwait(false);
      }
    }
  }
}