using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using NiceCore.Eventing.Messaging;
using NiceCore.Eventing.Messaging.Extensions;
using NiceCore.Eventing.Messaging.Impl;
using NiceCore.Storage.Conversations.Factories.Extensions;
using NiceCore.Storage.Messages;
using NiceCore.Storage.Messages.Factories;
using NiceCore.Storage.NH.Tests.Impls;
using NUnit.Framework;

namespace NiceCore.Storage.NH.Tests.Messages.Sending
{
  /// <summary>
  /// Tests for message sending
  /// </summary>
  [TestFixture]
  public class StorageMessageSendingTests : BaseStorageTest
  {
    private static bool m_MessageEntityDeletedCalled = false;
    private static bool m_MessageEntityUpdatedCalled = false;
    private static bool m_MessageEntityUpdatedForMessageCalled = false;
    private static bool m_MessageEntityCreatedCalled = false;

    /// <summary>
    /// Test External Storage Message Sender
    /// </summary>
    [Test]
    public async Task TestExternalStorageMessageSender()
    {
      m_MessageEntityCreatedCalled = false;
      m_MessageEntityDeletedCalled = false;
      var messageAwaiter = new DefaultMessageAwaiter();
      var msgService = Container.Resolve<IMessageService>();
      var createSub = msgService.RegisterDirect<EntitiesCreatedStorageMessage<TestEntity, long>, IEntitiesMessageWrapper<long>>(EntityCreated);
      var deleteSub = msgService.RegisterDirect<EntitiesDeletedStorageMessage<TestEntity, long>, IEntitiesMessageWrapper<long>>(EntityDeleted);
      var senderFactory = Container.Resolve<IStorageMessageSenderFactory>();
      var sender = senderFactory.CreateNew(new DefaultTestContext(), null);
      var entityId = 0L;
      var createConvAwaiter = new DefaultMessageAwaiter();
      using (var conv = await StorageService.ConversationFactory.Conversation(ConnectionName, i_MessageSender: sender))
      {
        conv.EnableMessageAwaiting(createConvAwaiter);
        var newTest = new TestEntity()
        {
          Name = "sfada"
        };
        await conv.SaveOrUpdateAsync<TestEntity, long>(newTest).ConfigureAwait(false);
        await conv.ApplyAsync().ConfigureAwait(false);
        entityId = newTest.ID;
      }

      await createConvAwaiter.AwaitMessageCompletion().ConfigureAwait(false);
      m_MessageEntityCreatedCalled.Should().BeFalse();
      m_MessageEntityDeletedCalled.Should().BeFalse();
      var deleteConvAwaiter = new DefaultMessageAwaiter();
      using (var conv = await StorageService.ConversationFactory.Conversation(ConnectionName, i_MessageSender: sender))
      {
        conv.EnableMessageAwaiting(deleteConvAwaiter);
        var res = await conv.Query<TestEntity, long>()
          .Where(r => r.ID == entityId)
          .ToListAsync().ConfigureAwait(false);
        await conv.DeleteAsync<TestEntity, long>(res.FirstOrDefault()).ConfigureAwait(false);
        await conv.ApplyAsync().ConfigureAwait(false);
      }

      await deleteConvAwaiter.AwaitMessageCompletion().ConfigureAwait(false);
      m_MessageEntityCreatedCalled.Should().BeFalse();
      m_MessageEntityDeletedCalled.Should().BeFalse();
      sender.SendMessages(messageAwaiter);
      await messageAwaiter.AwaitMessageCompletion().ConfigureAwait(false);
      m_MessageEntityCreatedCalled.Should().BeTrue();
      m_MessageEntityDeletedCalled.Should().BeTrue();
      createSub.Unsubscribe();
      deleteSub.Unsubscribe();
    }

    /// <summary>
    /// Tests that an update message should contain a diff result
    /// </summary>
    [Test]
    public async Task TestDiffInUpdateMessage()
    {
      m_MessageEntityUpdatedForMessageCalled = false;
      var id = 0L;
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var test = new TestEntity()
        {
          Name = "First"
        };
        await conv.SaveOrUpdateAsync<TestEntity>(test).ConfigureAwait(false);
        await conv.ApplyAsync().ConfigureAwait(false);
        id = test.ID;
      }

      var msgService = Container.Resolve<IMessageService>();
      var updateSub = msgService.RegisterDirect<EntitiesUpdatedStorageMessage<TestEntity, long>, IEntitiesMessageWrapper<long>>(EntityUpdatedForMessage);
      var messageAwaiter = new DefaultMessageAwaiter();
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        conv.EnableMessageAwaiting(messageAwaiter);
        var ent = conv.Query<TestEntity>()
          .Where(r => r.ID == id)
          .ToList().FirstOrDefault();
        ent.Name = "First-Modified";
        await conv.ApplyAsync().ConfigureAwait(false);
      }

      await messageAwaiter.AwaitMessageCompletion().ConfigureAwait(false);
      updateSub.Unsubscribe();
      m_MessageEntityUpdatedForMessageCalled.Should().BeTrue();
    }

    /// <summary>
    /// Tests Sending of storage messages
    /// </summary>
    [Test]
    public async Task Messages()
    {
      m_MessageEntityUpdatedCalled = false;
      m_MessageEntityDeletedCalled = false;
      m_MessageEntityCreatedCalled = false;
      var msgService = Container.Resolve<IMessageService>();
      if (msgService is DefaultMessageService defaultService)
      {
        defaultService.MessageServiceToEventingAdapterBridge = null;
      }
      var createSub = msgService.RegisterDirect<EntitiesDeletedStorageMessage<TestEntity, long>, IEntitiesMessageWrapper<long>>(EntityDeleted);
      var updateSub = msgService.RegisterDirect<EntitiesCreatedStorageMessage<TestEntity, long>, IEntitiesMessageWrapper<long>>(EntityCreated);
      var deleteSub = msgService.RegisterDirect<EntitiesUpdatedStorageMessage<TestEntity, long>, IEntitiesMessageWrapper<long>>(EntityUpdated);
      var messageAwaiter = new DefaultMessageAwaiter();
      long id = 0;
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        conv.EnableMessageAwaiting(messageAwaiter);
        var ent = new TestEntity()
        {
          Name = "Hans Paul",
          OtherEntities = new HashSet<ReferencedEntity>()
          {
            new ReferencedEntity()
            {
              Name = "ref"
            }
          }
        };
        await conv.SaveOrUpdateAsync(ent).ConfigureAwait(false);
        await conv.ApplyAsync().ConfigureAwait(false);
        id = ent.ID;
      }

      using (var conv = StorageService.Conversation(ConnectionName))
      {
        conv.EnableMessageAwaiting(messageAwaiter);
        var ent = conv
          .Query<TestEntity>()
          .FirstOrDefault(r => r.ID == id);
        ent.Name = "Hans Paul II";
        conv.SaveOrUpdate(ent);
        await conv.ApplyAsync();
      }

      using (var conv = StorageService.Conversation(ConnectionName))
      {
        conv.EnableMessageAwaiting(messageAwaiter);
        var ent = conv
          .Query<TestEntity>()
          .FirstOrDefault(r => r.ID == id);
        conv.Delete(ent);
        await conv.ApplyAsync();
      }

      await messageAwaiter.AwaitMessageCompletion();
      m_MessageEntityCreatedCalled.Should().BeTrue();
      m_MessageEntityUpdatedCalled.Should().BeTrue();
      m_MessageEntityDeletedCalled.Should().BeTrue();
      createSub.Unsubscribe();
      updateSub.Unsubscribe();
      deleteSub.Unsubscribe();
    }

    private Task EntityDeleted(EntitiesDeletedStorageMessage<TestEntity, long> i_Message)
    {
      m_MessageEntityDeletedCalled = i_Message != null && i_Message.GetEntitiesFromMessage().Count > 0;
      return Task.CompletedTask;
    }

    private async Task EntityCreated(EntitiesCreatedStorageMessage<TestEntity, long> i_Message)
    {
      var entities = i_Message.GetEntitiesFromMessage();
      m_MessageEntityCreatedCalled = entities.Count > 0;
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var ent = entities.FirstOrDefault();
        ent.Name += "_Copy";
        await conv.SaveOrUpdateAsync(ent).ConfigureAwait(false);
        await conv.ApplyAsync().ConfigureAwait(false);
      }
    }

    private Task EntityUpdated(EntitiesUpdatedStorageMessage<TestEntity, long> i_Message)
    {
      m_MessageEntityUpdatedCalled = i_Message != null && i_Message.GetEntitiesFromMessage().Count > 0;
      return Task.CompletedTask;
    }

    private Task EntityUpdatedForMessage(EntitiesUpdatedStorageMessage<TestEntity, long> i_Message)
    {
      var val = i_Message.GetActualValue();
      if (val.Success && val.ResultValue.DiffResult != null && val.ResultValue.DiffResult != null && val.ResultValue.DiffResult.Count > 0)
      {
        if (val.ResultValue.DiffResult.TryGetValue(i_Message.GetEntitiesFromMessage().First().ID, out var diffResult) && diffResult.Entries.Count > 0)
        {
          m_MessageEntityUpdatedForMessageCalled = true;
        }
      }

      return Task.CompletedTask;
    }
  }
}