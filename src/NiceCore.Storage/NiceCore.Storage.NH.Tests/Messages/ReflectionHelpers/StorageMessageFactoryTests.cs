using System.Collections.Generic;
using FluentAssertions;
using NiceCore.Storage.Diffing;
using NiceCore.Storage.Messages;
using NiceCore.Storage.Messages.Helpers;
using NiceCore.Storage.NH.Tests.Impls;
using NUnit.Framework;

namespace NiceCore.Storage.NH.Tests.Messages.ReflectionHelpers
{
  /// <summary>
  /// Storage Message factory
  /// </summary>
  [TestFixture]
  public class StorageMessageFactoryTests
  {
    /// <summary>
    /// Test create message
    /// </summary>
    [Test]
    public void TestCreateNotUpdatedMessage()
    {
      var values = new List<TestEntity>()
      {
        new TestEntity()
      };
      var newMessage = StorageMessageFactory.CreateNewNotUpdatedMessage(values, new DefaultTestContext(), typeof(EntitiesCreatedStorageMessage<TestEntity, long>));
      newMessage.Should().NotBeNull().And.BeOfType(typeof(EntitiesCreatedStorageMessage<TestEntity, long>));
      (newMessage as EntitiesCreatedStorageMessage<TestEntity, long>)!.GetEntitiesFromMessage().Should().NotBeNull().And.HaveCount(1);
    }
    
    /// <summary>
    /// Test create message
    /// </summary>
    [Test]
    public void TestCreateUpdatedMessage()
    {
      var values = new List<TestEntity>()
      {
        new TestEntity()
      };
      var dictionary = new Dictionary<long, StorageDiffResult>()
      {
        {0, new StorageDiffResult()}
      };
      var newMessage = StorageMessageFactory.CreateNewUpdatedMessage(values, dictionary, new DefaultTestContext(), typeof(EntitiesUpdatedStorageMessage<TestEntity, long>));
      newMessage.Should().NotBeNull().And.BeOfType(typeof(EntitiesUpdatedStorageMessage<TestEntity, long>));
      (newMessage as EntitiesUpdatedStorageMessage<TestEntity, long>)!.GetEntitiesFromMessage().Should().NotBeNull().And.HaveCount(1);
      (newMessage as EntitiesUpdatedStorageMessage<TestEntity, long>)!.GetActualValue().ResultValue.DiffResult.Should().NotBeNull().And.HaveCount(1);
    }
  }
}