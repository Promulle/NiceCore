using System.Collections.Generic;
using FluentAssertions;
using NiceCore.Storage.Messages;
using NiceCore.Storage.Messages.Helpers;
using NiceCore.Storage.NH.Tests.Impls;
using NUnit.Framework;

namespace NiceCore.Storage.NH.Tests.Messages.ReflectionHelpers
{
  /// <summary>
  /// Test for message value accessor
  /// </summary>
  [TestFixture]
  public class StorageMessageValueAccessorTests
  {
    /// <summary>
    /// Test get entities
    /// </summary>
    [Test]
    public void TestGetEntitiesFromMessage()
    {
      var message = EntitiesCreatedStorageMessage<TestEntity, long>.Create(new List<TestEntity>()
      {
        new TestEntity(),
        new TestEntity()
      }, null);
      var res = StorageMessageValueAccessor.GetEntities(message, typeof(TestEntity), typeof(long));
      res.Should().NotBeNull().And.BeOfType(typeof(List<TestEntity>));
      (res as List<TestEntity>).Should().HaveCount(2);
    }
    
    /// <summary>
    /// Test get entities
    /// </summary>
    [Test]
    public void TestGetContextFromMessage()
    {
      var message = EntitiesCreatedStorageMessage<TestEntity, long>.Create(new List<TestEntity>()
      {
        new TestEntity(),
        new TestEntity()
      }, new DefaultTestContext()
      {
        Number = 10
      });
      var res = StorageMessageValueAccessor.GetContext(message, typeof(TestEntity), typeof(long));
      res.Should().NotBeNull().And.BeOfType(typeof(DefaultTestContext));
      (res as DefaultTestContext)!.Number.Should().Be(10);
    }
  }
}