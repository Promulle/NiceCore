﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using NHibernate.Cfg;
using NiceCore.Storage.DomainBuilder;
using NiceCore.Storage.NH.Config;
using NiceCore.Storage.NH.Tests.Impls;
using NiceCore.Storage.NH.Tests.Utils;
using NiceCore.Storage.Services;
using NUnit.Framework;

namespace NiceCore.Storage.NH.Tests.Dynamic
{
  /// <summary>
  /// Tests for dynamic connection
  /// </summary>
  [TestFixture]
  public class DynamicConnectionTests : BaseStorageTest
  {
    private const string DYNAMIC_DB = "NC-Tests-NH-Dynamic";
    /// <summary>
    /// test dynamic addition of connections
    /// </summary>
    /// <returns></returns>
    [Test]
    public async Task AddDynamicConnection()
    {
      var respawner = await PostgresRespawner.Create(DYNAMIC_DB);
      var service = Container.Resolve<IConnectionConfigurationManagementService<ConnectionDefinition, Configuration>>();
      var newConnectionName = Guid.NewGuid().ToString();
      var nhModel = new NHConfigModel()
      {
        ConnectionString = NiceCoreStorageNHTestsConstants.GetConnectionString(DYNAMIC_DB),
        Dialect = "NHibernate.Dialect.PostgreSQL83Dialect",
        DriverClass = "NHibernate.Driver.NpgsqlDriver",
        ShowSql = "false"
      };
      var connDefinition = new ConnectionDefinition()
      {
        Name = newConnectionName,
        AllowAuditing = true,
        ConfigFile = null,
        SchemaUpdate = true,
        UseSerialization = false
      };
      await service.AddConfigurationFromConfigurationModel(newConnectionName, nhModel, new List<IDomainBuilder>() { new TestDomainBuilder() }, connDefinition).ConfigureAwait(false);

      using (var conv = StorageService.Conversation(newConnectionName))
      {
        var ent = new TestEntity()
        {
          Name = nameof(AddDynamicConnection)
        };
        await conv.SaveOrUpdateAsync(ent).ConfigureAwait(false);
        await conv.ApplyAsync().ConfigureAwait(false);
        var loaded = await conv.Query<TestEntity>()
          .ToListAsync().ConfigureAwait(false);
        loaded.Should().NotBeNull();
        loaded.Should().NotBeEmpty();
      }

      await respawner.Reset();
    }
  }
}