using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using NHibernate.Cfg;
using NiceCore.Services.Encryption.Providers;
using NiceCore.Storage.DomainBuilder;
using NiceCore.Storage.NH.Config.Services;
using NiceCore.Storage.NH.Tests.Impls;
using NiceCore.Storage.NH.Tests.Utils;
using NiceCore.Storage.Services;
using NUnit.Framework;

namespace NiceCore.Storage.NH.Tests.Dynamic
{
  /// <summary>
  /// Tests for loading an encrypted config file
  /// </summary>
  [TestFixture]
  public class EncryptedConfigurationTests : BaseStorageTest
  {
    private const string DYNAMIC_DB = "NC-Tests-NH-Dynamic";

    private static readonly string NH_CFG_TEXT = @$"<?xml version=""1.0"" encoding=""utf-8""?>
    <hibernate-configuration  xmlns=""urn:nhibernate-configuration-2.2"" >
      <session-factory>
    <property name=""connection.driver_class"">NHibernate.Driver.NpgsqlDriver</property>
    <property name=""connection.connection_string"">{NiceCoreStorageNHTestsConstants.GetConnectionString(DYNAMIC_DB)}</property>
    <property name=""dialect"">NHibernate.Dialect.PostgreSQL83Dialect</property>
    <property name=""show_sql"">false</property>
    </session-factory>
    </hibernate-configuration>";

    private static readonly string CERT_PATH = Path.Join(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "test-cert.pfx");
    private static readonly string CFG_PATH = Path.Join(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "nh.enc.cfg");

    /// <summary>
    /// Teardown
    /// </summary>
    [OneTimeTearDown]
    public void Teardown()
    {
      if (File.Exists(CERT_PATH))
      {
        File.Delete(CERT_PATH);
      }

      if (File.Exists(CFG_PATH))
      {
        File.Delete(CFG_PATH);
      }
    }

    /// <summary>
    /// Setup
    /// </summary>
    [OneTimeSetUp]
    public void Setup()
    {
      if (File.Exists(CERT_PATH))
      {
        File.Delete(CERT_PATH);
      }

      if (File.Exists(CFG_PATH))
      {
        File.Delete(CFG_PATH);
      }

      CreateCertFile();
      CreateCFGFile();
    }

    private static void CreateCertFile()
    {
      var rsaKey = RSA.Create();
      var certRequest = new CertificateRequest("CN=Self-Signed-Cert-Test", rsaKey, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
      var cert = certRequest.CreateSelfSigned(DateTimeOffset.Now.AddDays(-1), DateTimeOffset.Now.AddYears(10));
      var certData = cert.Export(X509ContentType.Pfx);
      if (File.Exists(CERT_PATH))
      {
        File.Delete(CERT_PATH);
      }

      File.WriteAllBytes(CERT_PATH, certData);
    }

    private static void CreateCFGFile()
    {
      if (File.Exists(CFG_PATH))
      {
        File.Delete(CFG_PATH);
      }

      var cfgAsBytes = Encoding.UTF8.GetBytes(NH_CFG_TEXT);
      var data = File.ReadAllBytes(CERT_PATH);
      var cert = new X509Certificate2(data);
      var encrypted = Encrypt(cert, cfgAsBytes);
      var base64Cfg = Convert.ToBase64String(encrypted);
      File.WriteAllText(CFG_PATH, base64Cfg);
    }

    private static byte[] Encrypt(X509Certificate2 i_Cert, byte[] i_Data)
    {
      var cms = new EnvelopedCms(new ContentInfo(i_Data));
      cms.Encrypt(new CmsRecipient(i_Cert));
      return cms.Encode();
      // using var rsa = i_Cert.GetRSAPrivateKey();
      // if (rsa == null)
      // {
      //   throw new InvalidOperationException("Can not encrypt data with given certificate: could not get private key from certificate.");
      // }
      // return rsa.Encrypt(i_Data, RSAEncryptionPadding.OaepSHA512);
    }

    /// <summary>
    /// Get Configuration Dictionary
    /// </summary>
    /// <returns></returns>
    protected override IDictionary<string, string> GetConfigurationDictionary()
    {
      var baseDict = base.GetConfigurationDictionary();
      return new Dictionary<string, string>(baseDict)
      {
        {"NiceCore:Encryption:Providers:NcCertProvider:CertificateFilePath", CERT_PATH},
        {"NiceCore:Storage:AdditionalEncryptionProviderMappings:One:ConnectionName", "enc"},
        {"NiceCore:Storage:AdditionalEncryptionProviderMappings:One:EncryptionProvider", NcEncryptionProviders.CERT_PROVIDER}
      };
    }

    /// <summary>
    /// test dynamic addition of connections
    /// </summary>
    /// <returns></returns>
    [Test]
    public async Task TestLoadEncryptedConnection()
    {
      var respawner = await PostgresRespawner.Create(DYNAMIC_DB);
      var service = Container.Resolve<IConnectionConfigurationManagementService<ConnectionDefinition, Configuration>>();
      var conDefService = Container.Resolve<IConnectionDefinitionManagementService>();
      var newConnectionName = Guid.NewGuid().ToString();

      var connDefinition = new ConnectionDefinition()
      {
        Name = newConnectionName,
        AllowAuditing = true,
        ConfigFile = CFG_PATH,
        SchemaUpdate = true,
        UseSerialization = false,
        EncryptionProvider = NcEncryptionProviders.CERT_PROVIDER
      };
      await service.AddConfigurationFromDefinition(connDefinition, new List<IDomainBuilder>() {new TestDomainBuilder()}).ConfigureAwait(false);
      conDefService.AddConnectionDefinition(connDefinition, false);
      using (var conv = StorageService.Conversation(newConnectionName))
      {
        var ent = new TestEntity()
        {
          Name = nameof(TestLoadEncryptedConnection)
        };
        await conv.SaveOrUpdateAsync(ent).ConfigureAwait(false);
        await conv.ApplyAsync().ConfigureAwait(false);
        var loaded = await conv.Query<TestEntity>()
          .ToListAsync().ConfigureAwait(false);
        loaded.Should().NotBeNull();
        loaded.Should().NotBeEmpty();
      }

      await respawner.Reset().ConfigureAwait(false);
    }

    /// <summary>
    /// test dynamic addition of connections
    /// </summary>
    /// <returns></returns>
    [Test]
    public async Task TestLoadEncryptedConnectionAdditionalProviders()
    {
      var respawner = await PostgresRespawner.Create(DYNAMIC_DB);
      var service = Container.Resolve<IConnectionConfigurationManagementService<ConnectionDefinition, Configuration>>();
      var conDefService = Container.Resolve<IConnectionDefinitionManagementService>();
      var newConnectionName = "enc";

      var connDefinition = new ConnectionDefinition()
      {
        Name = newConnectionName,
        AllowAuditing = true,
        ConfigFile = CFG_PATH,
        SchemaUpdate = true,
        UseSerialization = false
      };
      await service.AddConfigurationFromDefinition(connDefinition, new List<IDomainBuilder>() {new TestDomainBuilder()}).ConfigureAwait(false);
      conDefService.AddConnectionDefinition(connDefinition, false);
      using (var conv = StorageService.Conversation(newConnectionName))
      {
        var ent = new TestEntity()
        {
          Name = nameof(TestLoadEncryptedConnection)
        };
        await conv.SaveOrUpdateAsync(ent).ConfigureAwait(false);
        await conv.ApplyAsync().ConfigureAwait(false);
        var loaded = await conv.Query<TestEntity>()
          .ToListAsync().ConfigureAwait(false);
        loaded.Should().NotBeNull();
        loaded.Should().NotBeEmpty();
      }

      await respawner.Reset().ConfigureAwait(false);
    }
  }
}