﻿using FluentAssertions;
using NiceCore.Extensions;
using NiceCore.Filtering;
using NiceCore.Services.Validation.Exceptions;
using NiceCore.Storage.Cascading;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Fetching;
using NiceCore.Storage.NH.AdditionalFilterProvider;
using NiceCore.Storage.NH.Tests.Impls;
using NiceCore.Storage.NH.Tests.Impls.Limiting;
using Polly;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NiceCore.Storage.Connections;
using NiceCore.Storage.Conversations.Factories.Extensions;
using NiceCore.Storage.Conversations.Flags.Impls;
using NiceCore.Storage.NH.Connections;
using NiceCore.Storage.NH.Conversation.Factories;
using NiceCore.Storage.NH.Tests.Impls.ConnectionCreation;
using NUnit.Framework;

namespace NiceCore.Storage.NH.Tests
{
  /// <summary>
  /// Class for testing Storage
  /// </summary>
  [TestFixture]
  public class StorageTests : BaseStorageTest
  {
    /// <summary>
    /// Test SQL
    /// </summary>
    public const string TEST_SQL = "My sql statement";
    
    /// <summary>
    /// Test fetch reference name
    /// </summary>
    public const string TEST_FETCH_REFERENCE_NAME = "fetched";
    
    /// <summary>
    /// Test Fetch name
    /// </summary>
    public const string TEST_FETCH_NAME = "NotFetched";
    
    /// <summary>
    /// Test Fetch then name
    /// </summary>
    public const string TEST_FETCH_THEN_NAME = "ThenFetched";

    /// <summary>
    /// Gets the connection string
    /// </summary>
    [Test]
    public async Task GetConnectionString()
    {
      var conStr = await StorageService.GetConnectionStringForConnection(ConnectionName).ConfigureAwait(false);
      conStr.Should().NotBeEmpty();
    }

    /// <summary>
    /// Gets the connection string
    /// </summary>
    [Test]
    public async Task GetDriverClass()
    {
      var driverCls = await StorageService.GetConnectionDriverForConnection(ConnectionName).ConfigureAwait(false);
      driverCls.Should().NotBeEmpty();
    }

    [Test]
    public async Task TestGetConnectionFor()
    {
      var resNull = await StorageService.GetConnectionDefinitionNameForEntity(typeof(NotMappedEntity));
      resNull.Should().BeNull();
      var resName = await StorageService.GetConnectionDefinitionNameForEntity(typeof(TestEntity));
      resName.Should().Be(ConnectionName);
    }
    
    [Test]
    public async Task TestIsEntityMappedFor()
    {
      var resNull = await StorageService.IsEntityTypeMappedFor(typeof(NotMappedEntity), ConnectionName);
      resNull.Should().BeFalse();
      var resName = await StorageService.IsEntityTypeMappedFor(typeof(TestEntity), ConnectionName);
      resName.Should().BeTrue();
    }

    /// <summary>
    /// Test for limiting
    /// </summary>
    [Test]
    public async Task Limiting()
    {
      var ids = await PrepareLimiting();
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        conv.QueryLimiterSupport.AddLimiter(new TestLimiter());
        var limitEntities = conv.Query<LimitableEntity>()
          .Where(r => ids.Contains(r.ID))
          .ToList();
        limitEntities.Should().HaveCount(1);
      }
    }

    /// <summary>
    /// Test Create Connection
    /// </summary>
    [Test]
    public async Task TestCreateConnection()
    {
      var service = new NcConnectionManagementService()
      {
        Creators = new List<INcConnectionCreator>()
        {
          new TestCreator()
        }
      };
      service.Initialize();
      var conn = await service.GetConnection(new NHConnectionInfo()
      {
        ConnectionName = "test",
        ConnectionString = NiceCoreStorageNHTestsConstants.GetConnectionString(),
        ConnectionType = "Test",
        DriverClass = "Something",
        EnableAuditing = false
      }, null, CancellationToken.None).ConfigureAwait(false);
      conn.Should().NotBeNull();
      conn.State.Should().Be(ConnectionState.Open);
      conn.Dispose();
    }

    /// <summary>
    /// Test Connection Management
    /// </summary>
    [Test]
    public async Task TestConnectionManagement()
    {
      var conInfo = new NHConnectionInfo()
      {
        ConnectionString = NiceCoreStorageNHTestsConstants.GetConnectionString()
      };
      var con = await new TestCreator().CreateNewConnection(conInfo, null).ConfigureAwait(false);
      using (var conv = await StorageService.ConversationFactory.Conversation(ConnectionName, null, con).ConfigureAwait(false))
      {
        var res = await conv.Query<TestEntity>()
          .ToListAsync().ConfigureAwait(false);
      }

      con.Should().NotBeNull();
      con.State.Should().Be(ConnectionState.Open);
      con.Dispose();
    }

    /// <summary>
    /// Test if IgnoreLimiter is working
    /// </summary>
    [Test]
    public async Task IgnoreLimiting()
    {
      var ids = await PrepareLimiting();
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        conv.QueryLimiterSupport.DisableLimiters();
        var limitEntities = conv.Query<LimitableEntity>()
          .Where(r => ids.Contains(r.ID))
          .ToList();
        limitEntities.Should().HaveCount(2);
      }
    }

    private async Task<List<long>> PrepareLimiting()
    {
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var limitEntity = new LimitableEntity()
        {
          ShouldBeFiltered = false,
        };
        var limitEntity2 = new LimitableEntity()
        {
          ShouldBeFiltered = true,
        };
        var list = new List<long>();
        conv.SaveOrUpdate(limitEntity);
        conv.SaveOrUpdate(limitEntity2);
        list.Add(limitEntity.ID);
        list.Add(limitEntity2.ID);
        await conv.ApplyAsync();
        return list;
      }
    }

    /// <summary>
    /// Tests filering using NcFilters
    /// </summary>
    [Test]
    public async Task Filtering()
    {
      var ent1 = new TestEntity()
      {
        Name = "Name_0"
      };
      var ent2 = new TestEntity()
      {
        Name = "Name_1"
      };
      var ids = new List<long>();
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        conv.SaveOrUpdate(ent1);
        conv.SaveOrUpdate(ent2);
        ids.Add(ent1.ID);
        ids.Add(ent2.ID);
        await conv.ApplyAsync();
      }

      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var filter = NcFilters.CreateAnd(NcFilterConditions.CreateEquals(nameof(TestEntity.Name), "Name_0"));
        var list = conv.Query<TestEntity>()
          .Where(r => ids.Contains(r.ID))
          .FilterBy(filter, new() {AdditionalProviders = new Dictionary<FilterOperations, IAdditionalFilterOperationProvider>()})
          .ToList();
        list.Should().HaveCount(1);
      }
    }

    /// <summary>
    /// Tests filering using NcFilters
    /// </summary>
    [Test]
    public async Task FilteringWithLike()
    {
      var searchedName = "Heinz Gustav";
      var ent1 = new TestEntity()
      {
        Name = "Hans Paul"
      };
      var ent2 = new TestEntity()
      {
        Name = searchedName
      };
      var ids = new List<long>();
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        conv.SaveOrUpdate(ent1);
        conv.SaveOrUpdate(ent2);
        ids.Add(ent1.ID);
        ids.Add(ent2.ID);
        await conv.ApplyAsync();
      }

      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var filter = NcFilters.CreateAnd(NcFilterConditions.CreateLike(nameof(TestEntity.Name), "%usta%"));
        var options = new NcFilterTranslationOptions()
        {
          AdditionalProviders = new Dictionary<FilterOperations, IAdditionalFilterOperationProvider>()
          {
            {FilterOperations.Like, new NHFilterOperationLikeProvider()}
          }
        };
        var list = conv.Query<TestEntity>()
          .Where(r => ids.Contains(r.ID))
          .FilterBy(filter, options)
          .ToList();
        list.Should().HaveCount(1);
        list[0].Name.Should().Be(searchedName);
      }
    }

    

    /// <summary>
    /// tests saving and loading
    /// </summary>
    [Test]
    public async Task TestSaveLoad()
    {
      long id = 0;
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var test = new TestEntity
        {
          Name = "Test",
          IsActive = true
        };
        conv.SaveOrUpdate(test);
        id = test.ID;
        await conv.ApplyAsync();
      }

      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var test = conv
          .Query<TestEntity>()
          .Where(r => r.ID == id)
          .ToList()
          .FirstOrDefault();
        TestInstance(test, "Test");
      }
    }
    
    /// <summary>
    /// tests saving and loading
    /// </summary>
    [Test]
    public async Task TestSaveOrUpdateAsyncRaw()
    {
      long id = 0;
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var test = new TestEntity
        {
          Name = "TestSaveOrUpdateAsyncRaw",
          IsActive = true
        };
        await conv.SaveOrUpdateAsyncRaw<TestEntity, long>(test).ConfigureAwait(false);
        id = test.ID;
        await conv.ApplyAsync();
      }

      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var test = conv
          .Query<TestEntity>()
          .Where(r => r.ID == id)
          .ToList()
          .FirstOrDefault();
        TestInstance(test, "TestSaveOrUpdateAsyncRaw");
      }
    }

    /// <summary>
    /// tests saving and loading
    /// </summary>
    [Test]
    public async Task TestAuditing()
    {
      long entityId = 0;
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var test = new TestEntity
        {
          Name = "Test",
          IsActive = true
        };
        conv.SaveOrUpdate(test);
        entityId = test.ID;
        await conv.ApplyAsync();
      }

      var id = 0L;
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var test = conv
          .Query<TestEntity>()
          .Where(r => r.ID == entityId)
          .ToList()
          .FirstOrDefault();
        TestInstance(test, "Test");
        test.Name = "Test2";
        id = test.ID;
        conv.SaveOrUpdate(test);
        await conv.ApplyAsync();
      }

      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var newInst = conv.Query<TestEntity>()
          .Where(r => r.ID == entityId)
          .ToList()
          .FirstOrDefault();
        TestInstance(newInst, "Test2");
        var revision = conv.Audit.GetRevisions<TestEntity, long>(entityId).FirstOrDefault();
        var oldInstance = conv.Audit.FindRevision<TestEntity>(id, revision);
        TestInstance(oldInstance, "Test");
      }
    }

    /// <summary>
    /// Tests fetch many
    /// </summary>
    [Test]
    public async Task FetchManyProperty()
    {
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var newEnt = new ReferencedEntity() {Name = TEST_FETCH_REFERENCE_NAME};
        var entTest = new TestEntity() {Name = TEST_FETCH_NAME, OtherEntities = new HashSet<ReferencedEntity>() {newEnt}};
        conv.SaveOrUpdate(newEnt);
        conv.SaveOrUpdate(entTest);
        await conv.ApplyAsync();
      }

      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var ent = conv.Query<TestEntity>()
          .Where(r => r.Name == TEST_FETCH_NAME)
          .FetchMany(r => r.OtherEntities)
          .ToList()
          .FirstOrDefault();
        ent.Should().NotBeNull();
        ent.OtherEntities.Should()
          .NotBeNull()
          .And.NotBeEmpty();
      }
    }

    /// <summary>
    /// Test a specific fetch scenario when dealing with collections
    /// </summary>
    [Test]
    public async Task FetchTraverseOverCollection()
    {
      var ids = new List<long>();
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var thenEnt = new ThenEntity() {Name = "ThenEntity"};
        conv.SaveOrUpdate(thenEnt);
        var newEnt = new ReferencedEntity() {Name = TEST_FETCH_REFERENCE_NAME, ThenFetchEntity = thenEnt};
        conv.SaveOrUpdate(newEnt);
        var entTest = new TestEntity() {Name = nameof(FetchTraverseOverCollection), OtherEntities = new HashSet<ReferencedEntity>() {newEnt}};
        conv.SaveOrUpdate(entTest);
        ids.Add(thenEnt.ID);
        ids.Add(newEnt.ID);
        ids.Add(entTest.ID);
        await conv.ApplyAsync();
      }

      var fetchRelation = FetchStrategies.Create("OtherEntities.ThenFetchEntity");
      List<TestEntity> resList = null;
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        resList = conv.Query<TestEntity>()
          .Where(r => r.Name == nameof(FetchTraverseOverCollection))
          .ApplyFetchStrategy(fetchRelation)
          .ToList();
      }

      resList.Should().NotBeNull();
      resList.Should().NotBeEmpty();
      resList[0].Should().NotBeNull();
      resList[0].OtherEntities.Should().NotBeNull();
      resList[0].OtherEntities.Should().NotBeEmpty();
      resList[0].OtherEntities.FirstOrDefault().Should().NotBeNull();
      resList[0].OtherEntities.FirstOrDefault().ThenFetchEntity.Should().NotBeNull();
      resList[0].OtherEntities.FirstOrDefault().ThenFetchEntity.Name.Should().Be("ThenEntity");
    }

    /// <summary>
    /// Test save relation cascade
    /// </summary>
    [Test]
    public async Task CascadeSaveRelation()
    {
      var cascades = new List<ICascadingRelation>()
      {
        CascadingRelations.CreateManyToOne(nameof(TestEntity.OtherEntity), new List<DefaultCascadingRelation>()
        {
          CascadingRelations.CreateManyToOne(nameof(ReferencedEntity.ThenFetchEntity))
        }),
        CascadingRelations.CreateOneToMany(nameof(TestEntity.OtherEntities), nameof(ReferencedEntity.ParentId))
      };
      var ent = new TestEntity()
      {
        Name = "EntityManyToOne",
        OtherEntity = new ReferencedEntity()
        {
          Name = "CascadedEntity",
          ThenFetchEntity = new ThenEntity()
          {
            Name = "DeepCascadedEntity"
          }
        },
        OtherEntities = new HashSet<ReferencedEntity>()
        {
          new ReferencedEntity()
          {
            Name = "ManyToOneEntityOne"
          },
          new ReferencedEntity()
          {
            Name = "ManyToOneEntityTwo"
          }
        }
      };
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        await conv.SaveOrUpdateAsync<TestEntity, long>(ent, Policy.Handle<Exception>().RetryAsync(1), cascades, null).ConfigureAwait(false);
        await conv.ApplyAsync().ConfigureAwait(false);
      }

      TestEntity retEntity = null;
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        retEntity = conv.Query<TestEntity>()
          .Where(r => r.Name == "EntityManyToOne")
          .Fetch(r => r.OtherEntity)
          .ThenFetch(r => r.ThenFetchEntity)
          .FetchMany(r => r.OtherEntities)
          .ToList()
          .FirstOrDefault();
      }

      retEntity.Should().NotBeNull();
      retEntity.OtherEntity.Should().NotBeNull();
      retEntity.OtherEntity.ThenFetchEntity.Should().NotBeNull();
      retEntity.OtherEntities.Should().NotBeNull();
      retEntity.OtherEntities.Should().NotBeEmpty();
    }

    /// <summary>
    /// Test save relation cascade
    /// </summary>
    [Test]
    public async Task CascadeDeleteRelation()
    {
      var cascades = new List<ICascadingRelation>()
      {
        CascadingRelations.CreateManyToOne(nameof(TestEntity.OtherEntity), new List<DefaultCascadingRelation>()
        {
          CascadingRelations.CreateManyToOne(nameof(ReferencedEntity.ThenFetchEntity))
        }),
        CascadingRelations.CreateOneToMany(nameof(TestEntity.OtherEntities), nameof(ReferencedEntity.ParentId))
      };
      var ent = new TestEntity()
      {
        Name = "EntityManyToOne",
        OtherEntity = new ReferencedEntity()
        {
          Name = "CascadedEntity",
          ThenFetchEntity = new ThenEntity()
          {
            Name = "DeepCascadedEntity"
          }
        },
        OtherEntities = new HashSet<ReferencedEntity>()
        {
          new ReferencedEntity()
          {
            Name = "ManyToOneEntityOne"
          },
          new ReferencedEntity()
          {
            Name = "ManyToOneEntityTwo"
          }
        }
      };

      using (var conv = StorageService.Conversation(ConnectionName))
      {
        await conv.SaveOrUpdateAsync(ent).ConfigureAwait(false);
        await conv.SaveOrUpdateAsync(ent.OtherEntity).ConfigureAwait(false);
        await conv.SaveOrUpdateAsync(ent.OtherEntity.ThenFetchEntity).ConfigureAwait(false);
        await ent.OtherEntities.ForEachItem(async r =>
        {
          r.ParentId = ent.ID;
          await conv.SaveOrUpdateAsync(r).ConfigureAwait(false);
        }).ConfigureAwait(false);
        await conv.ApplyAsync().ConfigureAwait(false);
      }

      var entId = ent.ID;
      var otherEntId = ent.OtherEntity.ID;
      var thenEntityId = ent.OtherEntity.ThenFetchEntity.ID;
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        await conv.DeleteAsync<TestEntity, long>(ent, Policy.Handle<Exception>().RetryAsync(1), cascades, null).ConfigureAwait(false);
        await conv.ApplyAsync().ConfigureAwait(false);
      }

      TestEntity retEntity = null;
      ReferencedEntity otherEntity = null;
      ThenEntity thenEntity = null;
      List<ReferencedEntity> refEnts = null;
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        retEntity = conv
          .Query<TestEntity>()
          .Where(r => r.ID == entId)
          .FirstOrDefault(r => r.Name == "EntityManyToOne");
        otherEntity = conv
          .Query<ReferencedEntity>()
          .Where(r => r.ID == otherEntId)
          .FirstOrDefault(r => r.Name == "CascadedEntity");
        thenEntity = conv
          .Query<ThenEntity>()
          .Where(r => r.ID == thenEntityId)
          .FirstOrDefault(r => r.Name == "DeepCascadedEntity");

        refEnts = conv.Query<ReferencedEntity>()
          .Where(r => r.ParentId == entId)
          .ToList();
        retEntity.Should().BeNull();
        otherEntity.Should().BeNull();
        thenEntity.Should().BeNull();
        refEnts.Should().NotBeNull();
        refEnts.Should().BeEmpty();
      }
    }

    /// <summary>
    /// Test for ThenFetchMany
    /// </summary>
    [Test]
    public async Task ThenFetchManyProperties()
    {
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var thenEnt = new ThenEntity() {Name = TEST_FETCH_THEN_NAME};
        var newEnt = new ReferencedEntity() {Name = TEST_FETCH_REFERENCE_NAME, ThenFetchManyEntities = new HashSet<ThenEntity>() {thenEnt}};
        var entTest = new TestEntity() {Name = nameof(ThenFetchManyProperties), OtherEntity = newEnt};
        conv.SaveOrUpdate(thenEnt);
        conv.SaveOrUpdate(newEnt);
        conv.SaveOrUpdate(entTest);
        await conv.ApplyAsync();
      }

      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var ent = conv.Query<TestEntity>()
          .Where(r => r.Name == nameof(ThenFetchManyProperties))
          .Fetch(r => r.OtherEntity)
          .ThenFetchMany(r => r.ThenFetchManyEntities)
          .ToList()
          .FirstOrDefault();
        ent.Should().NotBeNull();
        ent.OtherEntity.Should().NotBeNull();
        TEST_FETCH_REFERENCE_NAME.Should().Be(ent.OtherEntity.Name);
        ent.OtherEntity.ThenFetchManyEntities.Should().NotBeNull();
        ent.OtherEntity.ThenFetchManyEntities.Should().NotBeNull();
      }
    }

    /// <summary>
    /// Test for ThenFetch
    /// </summary>
    [Test]
    public async Task ThenFetchedProperty()
    {
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var thenEnt = new ThenEntity() {Name = TEST_FETCH_THEN_NAME};
        var newEnt = new ReferencedEntity() {Name = TEST_FETCH_REFERENCE_NAME, ThenFetchEntity = thenEnt};
        var entTest = new TestEntity() {Name = nameof(ThenFetchedProperty), OtherEntity = newEnt};
        conv.SaveOrUpdate(thenEnt);
        conv.SaveOrUpdate(newEnt);
        conv.SaveOrUpdate(entTest);
        await conv.ApplyAsync();
      }

      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var ent = conv.Query<TestEntity>()
          .Where(r => r.Name == nameof(ThenFetchedProperty))
          .Fetch(r => r.OtherEntity)
          .ThenFetch(r => r.ThenFetchEntity)
          .ToList()
          .FirstOrDefault();
        ent.Should().NotBeNull();
        ent.OtherEntity.Should().NotBeNull();
        TEST_FETCH_REFERENCE_NAME.Should().Be(ent.OtherEntity.Name);
        ent.OtherEntity.ThenFetchEntity.Should().NotBeNull();
        TEST_FETCH_THEN_NAME.Should().Be(ent.OtherEntity.ThenFetchEntity.Name);
      }
    }

    /// <summary>
    /// Test for ThenFetch
    /// </summary>
    [Test]
    public async Task FetchRelations()
    {
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var thenEnt = new ThenEntity() {Name = TEST_FETCH_THEN_NAME};
        var newEnt = new ReferencedEntity() {Name = TEST_FETCH_REFERENCE_NAME, ThenFetchEntity = thenEnt};
        var entTest = new TestEntity() {Name = nameof(FetchRelations), OtherEntity = newEnt};
        conv.SaveOrUpdate(thenEnt);
        conv.SaveOrUpdate(newEnt);
        conv.SaveOrUpdate(entTest);
        await conv.ApplyAsync();
      }

      TestEntity ent;
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var fetchStrategy = FetchStrategies.Create("OtherEntity.ThenFetchEntity");
        ent = conv.Query<TestEntity>()
          .Where(r => r.Name == nameof(FetchRelations))
          .ApplyFetchStrategy(fetchStrategy)
          .ToList()
          .FirstOrDefault();
      }

      ent.Should().NotBeNull();
      ent.OtherEntity.Should().NotBeNull();
      TEST_FETCH_REFERENCE_NAME.Should().Be(ent.OtherEntity.Name);
      ent.OtherEntity.ThenFetchEntity.Should().NotBeNull();
      TEST_FETCH_THEN_NAME.Should().Be(ent.OtherEntity.ThenFetchEntity.Name);
    }

    /// <summary>
    /// Test for ThenFetch
    /// </summary>
    [Test]
    public async Task FetchRelationsWithCachedCall()
    {
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var thenEnt = new ThenEntity() {Name = TEST_FETCH_THEN_NAME};
        var newEnt = new ReferencedEntity() {Name = TEST_FETCH_REFERENCE_NAME, ThenFetchEntity = thenEnt};
        var entTest = new TestEntity() {Name = nameof(FetchRelationsWithCachedCall), OtherEntity = newEnt};
        conv.SaveOrUpdate(thenEnt);
        conv.SaveOrUpdate(newEnt);
        conv.SaveOrUpdate(entTest);
        await conv.ApplyAsync();
      }

      TestEntity ent;
      var fetchStrategy = FetchStrategies.Create("OtherEntity.ThenFetchEntity");
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        ent = conv.Query<TestEntity>()
          .Where(r => r.Name == nameof(FetchRelationsWithCachedCall))
          .ApplyFetchStrategy(fetchStrategy)
          .ToList()
          .FirstOrDefault();
      }

      ent.Should().NotBeNull();
      ent.OtherEntity.Should().NotBeNull();
      TEST_FETCH_REFERENCE_NAME.Should().Be(ent.OtherEntity.Name);
      ent.OtherEntity.ThenFetchEntity.Should().NotBeNull();
      TEST_FETCH_THEN_NAME.Should().Be(ent.OtherEntity.ThenFetchEntity.Name);
      TestEntity ent2;
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        ent2 = conv.Query<TestEntity>()
          .Where(r => r.Name == nameof(FetchRelationsWithCachedCall))
          .ApplyFetchStrategy(fetchStrategy)
          .ToList()
          .FirstOrDefault();
      }

      ent2.Should().NotBeNull();
      ent2.OtherEntity.Should().NotBeNull();
      TEST_FETCH_REFERENCE_NAME.Should().Be(ent2.OtherEntity.Name);
      ent2.OtherEntity.ThenFetchEntity.Should().NotBeNull();
      TEST_FETCH_THEN_NAME.Should().Be(ent2.OtherEntity.ThenFetchEntity.Name);
    }

    /// <summary>
    /// Tests if fetching lazy loaded properties works
    /// </summary>
    [Test]
    public async Task FetchedProperty()
    {
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var newEnt = new ReferencedEntity() {Name = TEST_FETCH_REFERENCE_NAME};
        var entTest = new TestEntity() {Name = nameof(FetchedProperty), OtherEntity = newEnt};
        conv.SaveOrUpdate(newEnt);
        conv.SaveOrUpdate(entTest);
        await conv.ApplyAsync();
      }

      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var ent = conv.Query<TestEntity>()
          .Where(r => r.Name == nameof(FetchedProperty))
          .Fetch(r => r.OtherEntity)
          .ToList()
          .FirstOrDefault();
        ent.Should().NotBeNull();
        ent.OtherEntity.Should().NotBeNull();
        TEST_FETCH_REFERENCE_NAME.Should().Be(ent.OtherEntity.Name);
      }
    }

    /// <summary>
    /// tests restart of conversation
    /// </summary>
    [Test]
    public async Task TestRestart()
    {
      long testId = 0;
      long test2Id = 0;
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var test = new TestEntity
        {
          Name = "Test",
          IsActive = true
        };
        conv.SaveOrUpdate(test);
        testId = test.ID;
        await conv.ApplyAsync();
        conv.Restart();

        var test2 = new TestEntity
        {
          Name = "Test2",
          IsActive = true
        };
        conv.SaveOrUpdate(test2);
        test2Id = test2.ID;
        await conv.ApplyAsync();

        var testLoaded = conv
          .Query<TestEntity>()
          .Where(r => r.ID == testId)
          .ToList()
          .FirstOrDefault();
        var testLoaded2 = conv
          .Query<TestEntity>()
          .Where(r => r.Name == "Test2" && r.ID == test2Id)
          .ToList()
          .FirstOrDefault();
        TestInstance(testLoaded, "Test");
        TestInstance(testLoaded2, "Test2");
      }
    }

    private static void TestInstance(TestEntity i_Instance, string i_Name)
    {
      i_Instance.Should().NotBeNull();
      i_Name.Should().Be(i_Instance.Name);
      i_Instance.IsActive.Should().BeTrue();
    }

    /// <summary>
    /// Tests IDatabaseConversation for getcommand function. IF the registered Conversation is NOT an IDatabaseConversation
    /// This test will NOT fail
    /// </summary>
    [Test]
    public void TestGetCommand()
    {
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        if (conv is IDatabaseConversation dbConv)
        {
          var command1 = dbConv.GetDbCommand();
          command1.Should().NotBeNull();
          var command2 = dbConv.GetDbCommand(TEST_SQL);
          command2.Should().NotBeNull();
          TEST_SQL.Should().Be(command2.CommandText);
        }
      }
    }

    /// <summary>
    /// Tests validation
    /// </summary>
    [Test]
    [Ignore("For now there never was a validator registered for this so I do not know when this last worked.")]
    public async Task Validate()
    {
      var validatableEntity = new ValidatableTestEntity()
      {
        Name = "Hans"
      };
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        conv.SaveOrUpdate(validatableEntity);
        await conv.ApplyAsync();
      }

      var wrongEntity = new ValidatableTestEntity()
      {
        Name = "1"
      };
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        Func<Task> act = async () => await conv.SaveOrUpdateAsync(wrongEntity);
        var exception = (await act.Should().ThrowAsync<NcValidationException>()).Which;
        exception.Instance.Should().Be(wrongEntity);
        exception.Entries.Should().NotBeEmpty();
      }
    }

    /// <summary>
    /// Tests disabling of validation
    /// </summary>
    [Test]
    public async Task DoNotValidate()
    {
      var wrongEntity = new ValidatableTestEntity()
      {
        Name = "1"
      };
      var conf = ConversationOperationConfig.CreateNoValidationConfig();
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        conv.SaveOrUpdate(wrongEntity, conf);
        await conv.ApplyAsync();
      }
    }
    
    /// <summary>
    /// Tests disabling of validation
    /// </summary>
    [Test]
    public async Task DoNotValidateByFlag()
    {
      var wrongEntity = new ValidatableTestEntity()
      {
        Name = "1"
      };
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        conv.Flags.AddFlag(DefaultConversationFlags.FLAG_VALIDATE_INSTANCES, false);
        conv.SaveOrUpdate(wrongEntity);
        await conv.ApplyAsync();
      }
    }

    /// <summary>
    /// Tests use of .ID if session is not open
    /// </summary>
    /// <returns></returns>
    [Test]
    public async Task AccessIDOfProxy()
    {
      long ID = 0;
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var otherInst = new ReferencedEntity()
        {
          Name = "Ref",
        };
        var inst = new TestEntity()
        {
          Name = "anotha one",
          OtherEntity = otherInst,
          IsActive = true
        };
        await conv.SaveOrUpdateAsync(otherInst).ConfigureAwait(false);
        await conv.SaveOrUpdateAsync(inst).ConfigureAwait(false);
        ID = inst.ID;
        await conv.ApplyAsync().ConfigureAwait(false);
      }

      TestEntity loaded = null;
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var entities = await conv.Query<TestEntity>()
          .Where(r => r.ID == ID)
          .ToListAsync().ConfigureAwait(false);
        loaded = entities[0];
      }

      loaded.OtherEntity.ID.Should().NotBe(0);
    }
  }
}