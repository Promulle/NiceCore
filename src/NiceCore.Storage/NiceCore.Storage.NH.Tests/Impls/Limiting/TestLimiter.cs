﻿using NiceCore.ServiceLocation;
using NiceCore.Storage.Conversations;
using System.Linq;
using NiceCore.Storage.Conversations.Flags;

namespace NiceCore.Storage.NH.Tests.Impls.Limiting
{
  /// <summary>
  /// limiter for testing
  /// </summary>
  [Register(InterfaceType = typeof(IQueryLimiter))]
  public class TestLimiter : IQueryLimiter
  {
    /// <summary>
    /// Limit qry test
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Query"></param>
    /// <param name="i_Flags"></param>
    /// <param name="i_Context"></param>
    /// <returns></returns>
    public IQueryable<T> LimitQuery<T>(IQueryable<T> i_Query, ConversationOperationFlags i_Flags, IConversationContext i_Context)
    {
      if (typeof(ILimitable).IsAssignableFrom(typeof(T)))
      {
        return i_Query.Where(r => !(r as ILimitable).ShouldBeFiltered);
      }
      return i_Query;
    }
  }
}
