﻿namespace NiceCore.Storage.NH.Tests.Impls.Limiting
{
  /// <summary>
  /// test interface
  /// </summary>
  public interface ILimitable
  {
    /// <summary>
    /// if it should be thrown away by limiter
    /// </summary>
    bool ShouldBeFiltered { get; set; }
  }
}
