﻿using NiceCore.Entities;

namespace NiceCore.Storage.NH.Tests.Impls.Limiting
{
  /// <summary>
  /// Entity that will be filtered by limiter
  /// </summary>
  public class LimitableEntity : BaseDomainEntity, ILimitable
  {
    /// <summary>
    /// ShouldBeFiltered
    /// </summary>
    public virtual bool ShouldBeFiltered { get; set; }
  }
}
