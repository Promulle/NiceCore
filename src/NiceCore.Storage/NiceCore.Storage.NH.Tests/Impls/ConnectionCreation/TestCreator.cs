using System.Data.Common;
using NiceCore.Storage.Connections;
using NiceCore.Storage.Connections.Impl;
using Npgsql;

namespace NiceCore.Storage.NH.Tests.Impls.ConnectionCreation
{
  /// <summary>
  /// Test Creator
  /// </summary>
  public sealed class TestCreator: BaseNcConnectionCreator
  {
    /// <summary>
    /// Connection Type
    /// </summary>
    public override string ConnectionType { get; } = "Test";
    
    /// <summary>
    /// Create Connection Instance
    /// </summary>
    /// <param name="i_ConnectionString"></param>
    /// <param name="i_Parameters"></param>
    /// <returns></returns>
    protected override DbConnection CreateConnectionInstance(string i_ConnectionString, IConnectionInstanceCreationParameters i_Parameters)
    {
      return new NpgsqlConnection(i_ConnectionString);
    }
  }
}