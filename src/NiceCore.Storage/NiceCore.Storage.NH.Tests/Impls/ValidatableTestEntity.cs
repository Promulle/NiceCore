﻿using NiceCore.Entities;

namespace NiceCore.Storage.NH.Tests.Impls
{
  /// <summary>
  /// entity fpr validation tests
  /// </summary>
  public class ValidatableTestEntity : BaseDomainEntity
  {
    /// <summary>
    /// Name
    /// </summary>
    public virtual string Name { get; set; }
  }
}
