﻿using System;
using System.Collections.Generic;
using FluentNHibernate.Mapping;
using NiceCore.Collections;
using NiceCore.ServiceLocation;
using NiceCore.Storage.NH.Mapping;

namespace NiceCore.Storage.NH.Tests.Impls.Fluent
{
  /// <summary>
  /// Mapping for fluently mapped entity
  /// </summary>
  [Register(InterfaceType = typeof(IFluentlyMapped))]
  public class FluentlyMappedEntityMapping : ClassMap<FluentlyMappedEntity>, IFluentlyMapped
  {
    /// <summary>
    /// Default constructor
    /// </summary>
    public FluentlyMappedEntityMapping()
    {
      Id(r => r.ID).GeneratedBy.Native();
      Table(nameof(FluentlyMappedEntity));
      Map(r => r.Name);
      References(r => r.Entity);
    }

    /// <summary>
    /// Get Mapped type
    /// </summary>
    /// <returns></returns>
    public Type GetMappedType()
    {
      return typeof(FluentlyMappedEntity);
    }

    /// <summary>
    /// Get allowed Connections
    /// </summary>
    /// <returns></returns>
    public IReadOnlySet<string> GetAllowedConnections()
    {
      return NcDefaultSets<string>.Empty;
    }
  }
}
