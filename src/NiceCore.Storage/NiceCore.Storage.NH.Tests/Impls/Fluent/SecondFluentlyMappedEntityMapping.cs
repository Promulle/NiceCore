using System;
using System.Collections.Generic;
using FluentNHibernate.Mapping;
using NiceCore.Collections;
using NiceCore.ServiceLocation;
using NiceCore.Storage.NH.Mapping;

namespace NiceCore.Storage.NH.Tests.Impls.Fluent
{
  /// <summary>
  /// Second Fluently Mapped Entity Mapping
  /// </summary>
  [Register(InterfaceType = typeof(IFluentlyMapped))]
  public class SecondFluentlyMappedEntityMapping : ClassMap<SecondFluentlyMappedEntity>, IFluentlyMapped
  {
    /// <summary>
    /// Ctor
    /// </summary>
    public SecondFluentlyMappedEntityMapping()
    {
      Id(r => r.ID).GeneratedBy.Native();
      Table(nameof(SecondFluentlyMappedEntity));
      Map(r => r.Name);
    }
    
    /// <summary>
    /// Get Mapped Type
    /// </summary>
    /// <returns></returns>
    public Type GetMappedType()
    {
      return typeof(SecondFluentlyMappedEntity);
    }
    
    /// <summary>
    /// Get allowed Connections
    /// </summary>
    /// <returns></returns>
    public IReadOnlySet<string> GetAllowedConnections()
    {
      return NcDefaultSets<string>.Empty;
    }
  }
}