using NiceCore.Entities;

namespace NiceCore.Storage.NH.Tests.Impls.Fluent
{
  /// <summary>
  /// Second Fluently Mapped Entity
  /// </summary>
  public class SecondFluentlyMappedEntity : BaseDomainEntity
  {
    /// <summary>
    /// Name
    /// </summary>
    public virtual string Name { get; set; }
  }
}