﻿using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Helpers;
using NiceCore.ServiceLocation;
using NiceCore.Storage.NH.Mapping;

namespace NiceCore.Storage.NH.Tests.Impls.Fluent
{
  /// <summary>
  /// Example fluent convention
  /// </summary>
  [Register(InterfaceType = typeof(IFluentConvention))]
  public class FluentConventions : IFluentConvention
  {
    /// <summary>
    /// Returns conventions
    /// </summary>
    /// <returns></returns>
    public IConvention[] GetConventions()
    {
      return new IConvention[]
      {
        ConventionBuilder.Id.Always(r => r.Column("ID")),
        ConventionBuilder.Class.Always(r => r.Table(r.EntityType.Name)),
        ConventionBuilder.Class.Always(r => r.SchemaAction.All()),
      };
    }
  }
}
