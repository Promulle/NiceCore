﻿using NiceCore.Entities;

namespace NiceCore.Storage.NH.Tests.Impls.Fluent
{
  /// <summary>
  /// Entity that is fluently mapped for testing
  /// </summary>
  public class FluentlyMappedEntity : BaseDomainEntity
  {
    /// <summary>
    /// Name
    /// </summary>
    public virtual string Name { get; set; }
    /// <summary>
    /// many to one example
    /// </summary>
    public virtual ReferencedEntity Entity { get; set; }
  }
}
