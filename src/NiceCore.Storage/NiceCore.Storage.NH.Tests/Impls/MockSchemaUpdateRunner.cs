﻿using NHibernate.Cfg;
using NiceCore.Storage.DomainBuilder;
using NiceCore.Storage.NH.SchemaUpdates;
using System;
using System.Collections.Generic;

namespace NiceCore.Storage.NH.Tests.Impls
{
  /// <summary>
  /// Mock impl for schema update runner
  /// </summary>
  public class MockSchemaUpdateRunner : ISchemaUpdateRunner
  {
    /// <summary>
    /// Do nothing
    /// </summary>
    /// <param name="i_Config"></param>
    /// <param name="i_DomainBuilders"></param>
    /// <param name="i_SchemaUpdateCallBack"></param>
    /// <returns></returns>
    public bool DoSchemaUpdate(Configuration i_Config, IEnumerable<IDomainBuilder> i_DomainBuilders, Action<string> i_SchemaUpdateCallBack)
    {
      return true;
    }
  }
}
