using NiceCore.ServiceLocation;
using NiceCore.Services.Validation.Contexts;
using NiceCore.Storage.Conversations;

namespace NiceCore.Storage.NH.Tests.Impls
{
  /// <summary>
  /// Default test context
  /// </summary>
  public class DefaultTestContext : IConversationContext
  {
    /// <summary>
    /// Number
    /// </summary>
    public int Number { get; init; }

    /// <summary>
    /// To valid context
    /// </summary>
    /// <param name="i_Conversation"></param>
    /// <param name="i_Container"></param>
    /// <returns></returns>
    public INcCustomValidationProcessContext ToValidationContext(IConversation i_Conversation, IServiceContainer i_Container)
    {
      return null;
    }
  }
}