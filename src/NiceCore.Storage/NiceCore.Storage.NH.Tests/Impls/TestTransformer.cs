﻿using NiceCore.Base;
using NiceCore.Storage.Config;

namespace NiceCore.Storage.NH.Tests.Impls
{
  /// <summary>
  /// Transformer for tests
  /// </summary>
  public class TestTransformer : IConnectionStringTransformer
  {
    /// <summary>
    /// Out
    /// </summary>
    public static readonly string OUT = "out";

    /// <summary>
    /// Transform
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public string Transform(string i_Value)
    {
      return OUT;
    }
  }
}
