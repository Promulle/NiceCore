﻿using NiceCore.Entities;

namespace NiceCore.Storage.NH.Tests.Impls
{
  /// <summary>
  /// Enttity to be then fetched
  /// </summary>
  public class ThenEntity : BaseDomainEntity
  {
    /// <summary>
    /// Name
    /// </summary>
    public virtual string Name { get; set; }
  }
}
