﻿using NiceCore.Entities;
using System.Collections.Generic;

namespace NiceCore.Storage.NH.Tests.Impls
{
  /// <summary>
  /// Entity to be referenced
  /// </summary>
  public class ReferencedEntity : BaseDomainEntity
  {
    /// <summary>
    /// Name
    /// </summary>
    public virtual string Name { get; set; }

    /// <summary>
    /// parentId
    /// </summary>
    public virtual long ParentId { get; set; }

    /// <summary>
    /// Entity
    /// </summary>
    public virtual ThenEntity ThenFetchEntity { get; set; }

    /// <summary>
    /// Set to be fetched
    /// </summary>
    public virtual ISet<ThenEntity> ThenFetchManyEntities { get; set; }
  }
}
