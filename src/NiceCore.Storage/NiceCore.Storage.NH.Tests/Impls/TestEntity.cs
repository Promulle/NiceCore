﻿using NiceCore.Entities;
using System.Collections.Generic;

namespace NiceCore.Storage.NH.Tests.Impls
{
  /// <summary>
  /// Test entity to save/load
  /// </summary>
  public class TestEntity : BaseDomainEntity
  {
    /// <summary>
    /// name
    /// </summary>
    public virtual string Name { get; set; }
    /// <summary>
    /// IsActive
    /// </summary>
    public virtual bool IsActive { get; set; }
    /// <summary>
    /// The other entity
    /// </summary>
    public virtual ReferencedEntity OtherEntity { get; set; }
    /// <summary>
    /// Set to be fetched
    /// </summary>
    public virtual ISet<ReferencedEntity> OtherEntities { get; set; }
  }
}
