using System.Threading.Tasks;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Conversations.EventHooks;

namespace NiceCore.Storage.NH.Tests.StorageHooks
{
  public class TestBeforeFalseHook : IBeforeAddToConversationHook
  {
    public bool IsDestructive { get; } = false;
    public ValueTask<bool> BeforeAdd(object t_Object, IConversation t_Conversation)
    {
      return ValueTask.FromResult(false);
    }
  }
}