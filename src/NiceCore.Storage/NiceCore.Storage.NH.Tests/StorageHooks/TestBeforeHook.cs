﻿using System.Threading.Tasks;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Conversations.EventHooks;
using NiceCore.Storage.NH.Tests.Impls;

namespace NiceCore.Storage.NH.Tests.StorageHooks
{
  /// <summary>
  /// Test before hook
  /// </summary>
  public class TestBeforeHook : IBeforeAddToConversationHook
  {
    public bool IsDestructive { get; } = false;
    
    /// <summary>
    /// On operation
    /// </summary>
    /// <param name="t_Object"></param>
    /// <param name="t_Conversation"></param>
    /// <returns></returns>
    public ValueTask<bool> BeforeAdd(object t_Object, IConversation t_Conversation)
    {
      var ent = new ThenEntity()
      {
        Name = "Before"
      };
      t_Conversation.SaveOrUpdate(ent);
      return ValueTask.FromResult(true);
    }
  }
}