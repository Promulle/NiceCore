﻿using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using NiceCore.Storage.NH.Conversation;
using NiceCore.Storage.NH.Tests.Impls;
using NUnit.Framework;

namespace NiceCore.Storage.NH.Tests.StorageHooks
{
  /// <summary>
  /// Storage Hook tests
  /// </summary>
  [TestFixture]
  public class StorageHookTests : BaseStorageTest
  {
    private const string CONNECTION_NAME = "unit_test";

    /// <summary>
    /// Test hooks
    /// </summary>
    [Test]
    public async Task Hooks()
    {
      var testEntity = new TestEntity()
      {
        Name = "Default"
      };
      using (var conv = StorageService.Conversation(CONNECTION_NAME))
      {
        (conv as NHConversation)!.BeforeAddToConversationHooks = new()
        {
          new TestBeforeHook()
        };
        await conv.SaveOrUpdateAsync(testEntity).ConfigureAwait(false);
        await conv.ApplyAsync().ConfigureAwait(false);
      }

      using (var conv = StorageService.Conversation(CONNECTION_NAME))
      {
        var allTestEntities = conv.Query<TestEntity>().ToList();
        allTestEntities.Count.Should().Be(1);
        
        allTestEntities.Find(r => r.Name == "Default").Should().NotBeNull();
        var allThenEntities = conv.Query<ThenEntity>().ToList();
        allThenEntities.Count.Should().BeGreaterThan(0);
        allThenEntities.Find(r => r.Name == "Before").Should().NotBeNull();
      }
    }
    
    /// <summary>
    /// Test hooks
    /// </summary>
    [Test]
    public async Task HooksDontContinue()
    {
      var testEntity = new TestEntity()
      {
        Name = "Default"
      };
      using var conv = StorageService.Conversation(CONNECTION_NAME);
      (conv as NHConversation)!.BeforeAddToConversationHooks = new()
      {
        new TestBeforeFalseHook()
      };
      await conv.SaveOrUpdateAsync(testEntity).ConfigureAwait(false);
      await conv.ApplyAsync().ConfigureAwait(false);
      testEntity.ID.Should().Be(default);
    }
  }
}