﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NHibernate.Cfg;
using NiceCore.Storage.DomainBuilder;
using NiceCore.Storage.NH.Config;
using NiceCore.Storage.NH.Config.Services;

namespace NiceCore.Storage.NH.Tests.ConnectionRetry;

public class MockConfigurationFileService : INHConnectionConfigurationFileService
{
  private int m_ThrowCounter = 0;
  private NHibernate.Cfg.Configuration m_FakeConfig;

  public MockConfigurationFileService(NHibernate.Cfg.Configuration i_FakeConfig)
  {
    m_FakeConfig = i_FakeConfig;
  }
  
  public bool IsInitialized { get; }
  public void Initialize()
  {
  }

  public void Dispose()
  {
  }

  public event EventHandler OnDisposeStart;
  public event EventHandler OnDisposeDone;
  public Task<Configuration> CreateFreshConfiguration(ConnectionDefinition i_Definition, IEnumerable<IDomainBuilder> i_DomainBuilders)
  {
    if (m_ThrowCounter < 2)
    {
      m_ThrowCounter++;
      throw new Exception();
    }

    return Task.FromResult(m_FakeConfig);
  }

  public Task<Configuration> CreateFreshConfiguration(NHConfigModel i_ConfigurationModel, ConnectionDefinition i_Definition, IEnumerable<IDomainBuilder> i_ApplicableDomainBuilders)
  {
    throw new Exception();
  }
}