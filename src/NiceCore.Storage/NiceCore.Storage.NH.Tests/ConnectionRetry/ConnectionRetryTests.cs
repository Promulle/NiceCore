﻿using NiceCore.Storage.DomainBuilder;
using NiceCore.Storage.NH.Tests.Impls;
using Polly;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Microsoft.Extensions.Logging.Abstractions;
using NiceCore.Storage.NH.Config.Services;
using NiceCore.Storage.NH.SessionFactories.Creation;
using NUnit.Framework;

namespace NiceCore.Storage.NH.Tests.ConnectionRetry
{
  /// <summary>
  /// Tests for Retrying connection attempts
  /// </summary>
  [TestFixture]
  public class ConnectionRetryTests : BaseStorageTest
  {
    private string CFG_TEXT = $@"<?xml version=""1.0"" encoding=""utf-8"" ?>
    <hibernate-configuration xmlns = ""urn:nhibernate-configuration-2.2"">
      <session-factory>
        <property name = ""connection.driver_class""> NHibernate.Driver.NpgsqlDriver </property>
        <property name = ""dialect"">NHibernate.Dialect.PostgreSQL83Dialect</property>
        <property name = ""connection.connection_string"">{NiceCoreStorageNHTestsConstants.GetConnectionString()}</property>
        <property name = ""show_sql"">false</property>
      </session-factory>
    </hibernate-configuration>";

    /// <summary>
    /// Retry
    /// </summary>
    [Test]
    public void RetrySuccessfully()
    {
      var sessionFactoriesService = GetSessionFactoryService();
      var retryPolicy = Policy.Handle<Exception>().Retry(3);
      var defaultStrategy = new DefaultConnectionRetryStrategy(NullLogger.Instance)
      {
        SyncPolicy = retryPolicy,
      };
      sessionFactoriesService.CreateOrLoad(CreateConnectionDefinition(), new List<IDomainBuilder>(), defaultStrategy, null);
    }

    private CreateSessionFactoriesService GetSessionFactoryService()
    {
      return new()
      {
        Logger = NullLogger<CreateSessionFactoriesService>.Instance,
        NHConnectionConfigurationFileService = MockConnectionConfigurationFileService(),
        SchemaUpdateRunner = new MockSchemaUpdateRunner()
      };
    }

    private INHConnectionConfigurationFileService MockConnectionConfigurationFileService()
    {
      return new MockConfigurationFileService(CreateConfigFromXml());
    }

    private static ConnectionDefinition CreateConnectionDefinition()
    {
      return new ConnectionDefinition()
      {
        UseSerialization = false,
        ConfigFile = "",
      };
    }

    private NHibernate.Cfg.Configuration CreateConfigFromXml()
    {
      try
      {
        using (var stringReader = new StringReader(CFG_TEXT))
        {
          using (var xmlReader = new XmlTextReader(stringReader))
          {
            return new NHibernate.Cfg.Configuration()
              .Configure(xmlReader);
          }
        }
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex);
        throw;
      }
    }
  }
}
