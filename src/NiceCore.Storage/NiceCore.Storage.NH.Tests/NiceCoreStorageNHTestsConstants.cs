﻿using System;

namespace NiceCore.Storage.NH.Tests
{
  /// <summary>
  /// Test Constant class
  /// </summary>
  internal static class NiceCoreStorageNHTestsConstants
  {
    internal static string GetConnectionString()
    {
      return GetConnectionString("NC-Tests-NH");
    }
    
    internal static string GetConnectionString(string i_DB)
    {
      return $"Server=localhost;Database={i_DB};User ID=TestUser;Password=Test;Enlist=true;";
    }
    
    internal static string GetConfigText()
    {
      return $@"<?xml version=""1.0"" encoding=""utf-8"" ?>
<hibernate-configuration xmlns = ""urn:nhibernate-configuration-2.2"">
  <session-factory>
    <property name = ""connection.driver_class"">NHibernate.Driver.NpgsqlDriver</property>
    <property name = ""dialect"">NHibernate.Dialect.PostgreSQL83Dialect</property>
    <property name = ""connection.connection_string"">{GetConnectionString()}</property>
    <property name = ""show_sql"">false</property>
  </session-factory>
</hibernate-configuration>";
    }
  }
}
