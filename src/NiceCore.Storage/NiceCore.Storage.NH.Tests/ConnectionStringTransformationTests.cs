﻿using System.Collections.Generic;
using FluentAssertions;
using Microsoft.Extensions.Logging.Abstractions;
using NiceCore.Base;
using NiceCore.ServiceLocation.CastleWindsor;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using NiceCore.Services.Config.Impl;
using NiceCore.Services.Default.Config;
using NiceCore.Storage.Config;
using NiceCore.Storage.NH.Config.Services.Impl;
using NiceCore.Storage.NH.Tests.Impls;
using NUnit.Framework;

namespace NiceCore.Storage.NH.Tests
{
  /// <summary>
  /// Tests for Connection-String transformer
  /// </summary>
  [TestFixture]
  public class ConnectionStringTransformationTests
  {
    private static readonly string TEST_KEY = "Test";
    private static readonly string TEST_KEY_CON_STR = "Test";
    private static readonly string TEST_NOT_TRANSFORMED = "This is untransformed";

    private static NHConfigService GetService(bool i_RegisterTransformer)
    {
      var config = new DefaultNcConfiguration()
      {
        ConfigurationSources = new List<INcConfigurationSource>()
      };
      config.ConfigurationSources.Add(new DefaultInMemoryNcConfigurationSource(new Dictionary<string, string>()
      {
        {$"{TEST_KEY_CON_STR}", TEST_NOT_TRANSFORMED},
        {$"{TEST_KEY}:ConnectionStringKey", TEST_KEY_CON_STR}
      }));
      config.Initialize();
      var container = new CastleWindsorServiceContainer();
      if (i_RegisterTransformer)
      {
        container.Register<IConnectionStringTransformer, TestTransformer>();
      }
      return new NHConfigService()
      {
        Logger = NullLogger<NHConfigService>.Instance,
        Config = config,
        Container = container,
      };
    }

    /// <summary>
    /// Tests transform
    /// </summary>
    [Test]
    public void Transform()
    {
      var service = GetService(true);
      var inst = service.GetConfigFromConfiguration(TEST_KEY);
      inst.ConnectionString.Should().Be(TestTransformer.OUT);
    }

    /// <summary>
    /// Tests transform
    /// </summary>
    [Test]
    public void DontTransform()
    {
      var service = GetService(false);
      var inst = service.GetConfigFromConfiguration(TEST_KEY);
      inst.ConnectionString.Should().Be(TEST_NOT_TRANSFORMED);
    }
  }
}
