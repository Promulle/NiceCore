﻿using System.Data.Common;
using System.Threading.Tasks;
using Npgsql;
using Respawn;

namespace NiceCore.Storage.NH.Tests.Utils
{
  /// <summary>
  /// Respawner with postgres connection
  /// </summary>
  public sealed class PostgresRespawner
  {
    /// <summary>
    /// Respawner
    /// </summary>
    public Respawner Respawner { get; private init; }
    
    /// <summary>
    /// Connection
    /// </summary>
    public DbConnection Connection { get; private init; }

    /// <summary>
    /// Reset
    /// </summary>
    public async Task Reset()
    {
      await Respawner.ResetAsync(Connection);
      await Connection.CloseAsync();
      await Connection.DisposeAsync();
    }
    
    /// <summary>
    /// Create
    /// </summary>
    /// <param name="i_DatabaseName"></param>
    /// <returns></returns>
    public static Task<PostgresRespawner> Create(string i_DatabaseName)
    {
      return Create(new NpgsqlConnection(NiceCoreStorageNHTestsConstants.GetConnectionString(i_DatabaseName)));
    }
    
    /// <summary>
    /// Create
    /// </summary>
    /// <returns></returns>
    public static Task<PostgresRespawner> Create()
    {
      return Create(new NpgsqlConnection(NiceCoreStorageNHTestsConstants.GetConnectionString()));
    }

    private static async Task<PostgresRespawner> Create(DbConnection i_Connection)
    {
      await i_Connection.OpenAsync();
      var respawner = await Respawner.CreateAsync(i_Connection, new RespawnerOptions()
      {
        DbAdapter = DbAdapter.Postgres,
        SchemasToInclude = new[] {"public"}
      });
      return new PostgresRespawner()
      {
        Connection = i_Connection,
        Respawner = respawner
      };
    }
  }
}