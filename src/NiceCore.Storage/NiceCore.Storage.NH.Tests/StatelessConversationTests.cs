using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using NiceCore.Filtering;
using NiceCore.Storage.Conversations.Factories;
using NiceCore.Storage.Conversations.Factories.Extensions;
using NiceCore.Storage.Fetching;
using NiceCore.Storage.NH.AdditionalFilterProvider;
using NiceCore.Storage.NH.Tests.Impls;
using NiceCore.Storage.NH.Tests.Impls.Limiting;
using NUnit.Framework;

namespace NiceCore.Storage.NH.Tests
{
  /// <summary>
  /// Stateless Conversation Tests
  /// </summary>
  [TestFixture]
  public sealed class StatelessConversationTests : BaseStorageTest
  {
    /// <summary>
    /// Test for limiting
    /// </summary>
    [Test]
    public async Task Limiting()
    {
      var ids = await PrepareLimiting();
      using var conv = await StorageService.ConversationFactory.StatelessReadOnlyConversation(ConnectionName).ConfigureAwait(false);
      conv.QueryLimiterSupport.AddLimiter(new TestLimiter());
      var limitEntities = conv.Query<LimitableEntity>()
        .Where(r => ids.Contains(r.ID))
        .ToList();
      limitEntities.Should().HaveCount(1);
    }

    private async Task<List<long>> PrepareLimiting()
    {
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var limitEntity = new LimitableEntity()
        {
          ShouldBeFiltered = false,
        };
        var limitEntity2 = new LimitableEntity()
        {
          ShouldBeFiltered = true,
        };
        var list = new List<long>();
        conv.SaveOrUpdate(limitEntity);
        conv.SaveOrUpdate(limitEntity2);
        list.Add(limitEntity.ID);
        list.Add(limitEntity2.ID);
        await conv.ApplyAsync();
        return list;
      }
    }

    /// <summary>
    /// Tests filering using NcFilters
    /// </summary>
    [Test]
    public async Task Filtering()
    {
      var ent1 = new TestEntity()
      {
        Name = "Name_0"
      };
      var ent2 = new TestEntity()
      {
        Name = "Name_1"
      };
      var ids = new List<long>();
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        conv.SaveOrUpdate(ent1);
        conv.SaveOrUpdate(ent2);
        ids.Add(ent1.ID);
        ids.Add(ent2.ID);
        await conv.ApplyAsync();
      }

      using (var conv = await StorageService.ConversationFactory.StatelessReadOnlyConversation(ConnectionName).ConfigureAwait(false))
      {
        var filter = NcFilters.CreateAnd(NcFilterConditions.CreateEquals(nameof(TestEntity.Name), "Name_0"));
        var list = conv.Query<TestEntity>()
          .Where(r => ids.Contains(r.ID))
          .FilterBy(filter, new() {AdditionalProviders = new Dictionary<FilterOperations, IAdditionalFilterOperationProvider>()})
          .ToList();
        list.Should().HaveCount(1);
      }
    }

    /// <summary>
    /// Tests filering using NcFilters
    /// </summary>
    [Test]
    public async Task FilteringWithLike()
    {
      var searchedName = "Heinz Gustav";
      var ent1 = new TestEntity()
      {
        Name = "Hans Paul"
      };
      var ent2 = new TestEntity()
      {
        Name = searchedName
      };
      var ids = new List<long>();
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        conv.SaveOrUpdate(ent1);
        conv.SaveOrUpdate(ent2);
        ids.Add(ent1.ID);
        ids.Add(ent2.ID);
        await conv.ApplyAsync();
      }

      using (var conv = await StorageService.ConversationFactory.StatelessReadOnlyConversation(ConnectionName).ConfigureAwait(false))
      {
        var filter = NcFilters.CreateAnd(NcFilterConditions.CreateLike(nameof(TestEntity.Name), "%usta%"));
        var options = new NcFilterTranslationOptions()
        {
          AdditionalProviders = new Dictionary<FilterOperations, IAdditionalFilterOperationProvider>()
          {
            {FilterOperations.Like, new NHFilterOperationLikeProvider()}
          }
        };
        var list = conv.Query<TestEntity>()
          .Where(r => ids.Contains(r.ID))
          .FilterBy(filter, options)
          .ToList();
        list.Should().HaveCount(1);
        list[0].Name.Should().Be(searchedName);
      }
    }

    /// <summary>
    /// Tests fetch many
    /// </summary>
    [Test]
    public async Task FetchManyProperty()
    {
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var newEnt = new ReferencedEntity() {Name = StorageTests.TEST_FETCH_REFERENCE_NAME};
        var entTest = new TestEntity() {Name = StorageTests.TEST_FETCH_NAME, OtherEntities = new HashSet<ReferencedEntity>() {newEnt}};
        conv.SaveOrUpdate(newEnt);
        conv.SaveOrUpdate(entTest);
        await conv.ApplyAsync();
      }

      using (var conv = await StorageService.ConversationFactory.StatelessReadOnlyConversation(ConnectionName).ConfigureAwait(false))
      {
        var ent = conv.Query<TestEntity>()
          .Where(r => r.Name == StorageTests.TEST_FETCH_NAME)
          .FetchMany(r => r.OtherEntities)
          .ToList()
          .FirstOrDefault();
        ent.Should().NotBeNull();
        ent.OtherEntities.Should()
          .NotBeNull()
          .And.NotBeEmpty();
      }
    }

    /// <summary>
    /// Test a specific fetch scenario when dealing with collections
    /// </summary>
    [Test]
    public async Task FetchTraverseOverCollection()
    {
      var ids = new List<long>();
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var thenEnt = new ThenEntity() {Name = "ThenEntity"};
        conv.SaveOrUpdate(thenEnt);
        var newEnt = new ReferencedEntity() {Name = StorageTests.TEST_FETCH_REFERENCE_NAME, ThenFetchEntity = thenEnt};
        conv.SaveOrUpdate(newEnt);
        var entTest = new TestEntity() {Name = nameof(FetchTraverseOverCollection), OtherEntities = new HashSet<ReferencedEntity>() {newEnt}};
        conv.SaveOrUpdate(entTest);
        ids.Add(thenEnt.ID);
        ids.Add(newEnt.ID);
        ids.Add(entTest.ID);
        await conv.ApplyAsync();
      }

      var fetchRelation = FetchStrategies.Create("OtherEntities.ThenFetchEntity");
      List<TestEntity> resList = null;
      using (var conv = await StorageService.ConversationFactory.StatelessReadOnlyConversation(ConnectionName).ConfigureAwait(false))
      {
        resList = conv.Query<TestEntity>()
          .Where(r => r.Name == nameof(FetchTraverseOverCollection))
          .ApplyFetchStrategy(fetchRelation)
          .ToList();
      }

      resList.Should().NotBeNull();
      resList.Should().NotBeEmpty();
      resList[0].Should().NotBeNull();
      resList[0].OtherEntities.Should().NotBeNull();
      resList[0].OtherEntities.Should().NotBeEmpty();
      resList[0].OtherEntities.FirstOrDefault().Should().NotBeNull();
      resList[0].OtherEntities.FirstOrDefault().ThenFetchEntity.Should().NotBeNull();
      resList[0].OtherEntities.FirstOrDefault().ThenFetchEntity.Name.Should().Be("ThenEntity");
    }

    /// <summary>
    /// Test for ThenFetchMany
    /// </summary>
    [Test]
    public async Task ThenFetchManyProperties()
    {
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var thenEnt = new ThenEntity() {Name = StorageTests.TEST_FETCH_THEN_NAME};
        var newEnt = new ReferencedEntity() {Name = StorageTests.TEST_FETCH_REFERENCE_NAME, ThenFetchManyEntities = new HashSet<ThenEntity>() {thenEnt}};
        var entTest = new TestEntity() {Name = nameof(ThenFetchManyProperties), OtherEntity = newEnt};
        conv.SaveOrUpdate(thenEnt);
        conv.SaveOrUpdate(newEnt);
        conv.SaveOrUpdate(entTest);
        await conv.ApplyAsync();
      }

      using (var conv = await StorageService.ConversationFactory.StatelessReadOnlyConversation(ConnectionName).ConfigureAwait(false))
      {
        var ent = conv.Query<TestEntity>()
          .Where(r => r.Name == nameof(ThenFetchManyProperties))
          .Fetch(r => r.OtherEntity)
          .ThenFetchMany(r => r.ThenFetchManyEntities)
          .ToList()
          .FirstOrDefault();
        ent.Should().NotBeNull();
        ent.OtherEntity.Should().NotBeNull();
        StorageTests.TEST_FETCH_REFERENCE_NAME.Should().Be(ent.OtherEntity.Name);
        ent.OtherEntity.ThenFetchManyEntities.Should().NotBeNull();
        ent.OtherEntity.ThenFetchManyEntities.Should().NotBeNull();
      }
    }

    /// <summary>
    /// Test for ThenFetch
    /// </summary>
    [Test]
    public async Task ThenFetchedProperty()
    {
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var thenEnt = new ThenEntity() {Name = StorageTests.TEST_FETCH_THEN_NAME};
        var newEnt = new ReferencedEntity() {Name = StorageTests.TEST_FETCH_REFERENCE_NAME, ThenFetchEntity = thenEnt};
        var entTest = new TestEntity() {Name = nameof(ThenFetchedProperty), OtherEntity = newEnt};
        conv.SaveOrUpdate(thenEnt);
        conv.SaveOrUpdate(newEnt);
        conv.SaveOrUpdate(entTest);
        await conv.ApplyAsync();
      }

      using (var conv = await StorageService.ConversationFactory.StatelessReadOnlyConversation(ConnectionName).ConfigureAwait(false))
      {
        var ent = conv.Query<TestEntity>()
          .Where(r => r.Name == nameof(ThenFetchedProperty))
          .Fetch(r => r.OtherEntity)
          .ThenFetch(r => r.ThenFetchEntity)
          .ToList()
          .FirstOrDefault();
        ent.Should().NotBeNull();
        ent.OtherEntity.Should().NotBeNull();
        StorageTests.TEST_FETCH_REFERENCE_NAME.Should().Be(ent.OtherEntity.Name);
        ent.OtherEntity.ThenFetchEntity.Should().NotBeNull();
        StorageTests.TEST_FETCH_THEN_NAME.Should().Be(ent.OtherEntity.ThenFetchEntity.Name);
      }
    }

    /// <summary>
    /// Test for ThenFetch
    /// </summary>
    [Test]
    public async Task FetchRelations()
    {
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var thenEnt = new ThenEntity() {Name = StorageTests.TEST_FETCH_THEN_NAME};
        var newEnt = new ReferencedEntity() {Name = StorageTests.TEST_FETCH_REFERENCE_NAME, ThenFetchEntity = thenEnt};
        var entTest = new TestEntity() {Name = nameof(FetchRelations), OtherEntity = newEnt};
        conv.SaveOrUpdate(thenEnt);
        conv.SaveOrUpdate(newEnt);
        conv.SaveOrUpdate(entTest);
        await conv.ApplyAsync();
      }

      TestEntity ent;
      using (var conv = await StorageService.ConversationFactory.StatelessReadOnlyConversation(ConnectionName).ConfigureAwait(false))
      {
        var fetchStrategy = FetchStrategies.Create("OtherEntity.ThenFetchEntity");
        ent = conv.Query<TestEntity>()
          .Where(r => r.Name == nameof(FetchRelations))
          .ApplyFetchStrategy(fetchStrategy)
          .ToList()
          .FirstOrDefault();
      }

      ent.Should().NotBeNull();
      ent.OtherEntity.Should().NotBeNull();
      StorageTests.TEST_FETCH_REFERENCE_NAME.Should().Be(ent.OtherEntity.Name);
      ent.OtherEntity.ThenFetchEntity.Should().NotBeNull();
      StorageTests.TEST_FETCH_THEN_NAME.Should().Be(ent.OtherEntity.ThenFetchEntity.Name);
    }

    /// <summary>
    /// Test for ThenFetch
    /// </summary>
    [Test]
    public async Task FetchRelationsWithCachedCall()
    {
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var thenEnt = new ThenEntity() {Name = StorageTests.TEST_FETCH_THEN_NAME};
        var newEnt = new ReferencedEntity() {Name = StorageTests.TEST_FETCH_REFERENCE_NAME, ThenFetchEntity = thenEnt};
        var entTest = new TestEntity() {Name = nameof(FetchRelationsWithCachedCall), OtherEntity = newEnt};
        conv.SaveOrUpdate(thenEnt);
        conv.SaveOrUpdate(newEnt);
        conv.SaveOrUpdate(entTest);
        await conv.ApplyAsync();
      }

      TestEntity ent;
      var fetchStrategy = FetchStrategies.Create("OtherEntity.ThenFetchEntity");
      using (var conv = await StorageService.ConversationFactory.StatelessReadOnlyConversation(ConnectionName).ConfigureAwait(false))
      {
        ent = conv.Query<TestEntity>()
          .Where(r => r.Name == nameof(FetchRelationsWithCachedCall))
          .ApplyFetchStrategy(fetchStrategy)
          .ToList()
          .FirstOrDefault();
      }

      ent.Should().NotBeNull();
      ent.OtherEntity.Should().NotBeNull();
      StorageTests.TEST_FETCH_REFERENCE_NAME.Should().Be(ent.OtherEntity.Name);
      ent.OtherEntity.ThenFetchEntity.Should().NotBeNull();
      StorageTests.TEST_FETCH_THEN_NAME.Should().Be(ent.OtherEntity.ThenFetchEntity.Name);
      TestEntity ent2;
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        ent2 = conv.Query<TestEntity>()
          .Where(r => r.Name == nameof(FetchRelationsWithCachedCall))
          .ApplyFetchStrategy(fetchStrategy)
          .ToList()
          .FirstOrDefault();
      }

      ent2.Should().NotBeNull();
      ent2.OtherEntity.Should().NotBeNull();
      StorageTests.TEST_FETCH_REFERENCE_NAME.Should().Be(ent2.OtherEntity.Name);
      ent2.OtherEntity.ThenFetchEntity.Should().NotBeNull();
      StorageTests.TEST_FETCH_THEN_NAME.Should().Be(ent2.OtherEntity.ThenFetchEntity.Name);
    }

    /// <summary>
    /// Tests if fetching lazy loaded properties works
    /// </summary>
    [Test]
    public async Task FetchedProperty()
    {
      using (var conv = StorageService.Conversation(ConnectionName))
      {
        var newEnt = new ReferencedEntity() {Name = StorageTests.TEST_FETCH_REFERENCE_NAME};
        var entTest = new TestEntity() {Name = nameof(FetchedProperty), OtherEntity = newEnt};
        conv.SaveOrUpdate(newEnt);
        conv.SaveOrUpdate(entTest);
        await conv.ApplyAsync();
      }

      using (var conv = await StorageService.ConversationFactory.StatelessReadOnlyConversation(ConnectionName).ConfigureAwait(false))
      {
        var ent = conv.Query<TestEntity>()
          .Where(r => r.Name == nameof(FetchedProperty))
          .Fetch(r => r.OtherEntity)
          .ToList()
          .FirstOrDefault();
        ent.Should().NotBeNull();
        ent.OtherEntity.Should().NotBeNull();
        StorageTests.TEST_FETCH_REFERENCE_NAME.Should().Be(ent.OtherEntity.Name);
      }
    }

    [Test]
    public async Task TestInsert()
    {
      using (var conv = await StorageService.ConversationFactory.CreateNewStatelessConversation(ConversationCreationParameters.ForName(ConnectionName), CancellationToken.None))
      {
        await conv.SaveOrUpdateAsync<TestEntity, long>(new TestEntity()
        {
          Name = "Hans Paul"
        }).ConfigureAwait(false);
        await conv.ApplyAsync().ConfigureAwait(false);
      }

      TestEntity ent = null;
      using (var conv = await StorageService.ConversationFactory.CreateNewStatelessConversation(ConversationCreationParameters.ForName(ConnectionName), CancellationToken.None))
      {
        ent = (await conv.Query<TestEntity, long>()
          .Where(r => r.Name == "Hans Paul")
          .ToListAsync().ConfigureAwait(false)).FirstOrDefault();
      }

      ent.Should().NotBeNull();
    }

    [Test]
    public async Task TestUpdate()
    {
      using (var conv = await StorageService.ConversationFactory.CreateNewStatelessConversation(ConversationCreationParameters.ForName(ConnectionName), CancellationToken.None))
      {
        await conv.SaveOrUpdateAsync<TestEntity, long>(new TestEntity()
        {
          Name = "Hans Paul"
        }).ConfigureAwait(false);
        await conv.ApplyAsync().ConfigureAwait(false);
      }

      
      using (var conv = await StorageService.ConversationFactory.CreateNewStatelessConversation(ConversationCreationParameters.ForName(ConnectionName), CancellationToken.None))
      {
        var ent = (await conv.Query<TestEntity, long>()
          .Where(r => r.Name == "Hans Paul")
          .ToListAsync().ConfigureAwait(false)).FirstOrDefault();
        ent.Should().NotBeNull();
        ent!.Name = "Hans Paul 2";
        await conv.SaveOrUpdateAsync<TestEntity, long>(ent).ConfigureAwait(false);
        await conv.ApplyAsync().ConfigureAwait(false);
      }
      
      TestEntity finalEnt = null;
      using (var conv = await StorageService.ConversationFactory.CreateNewStatelessConversation(ConversationCreationParameters.ForName(ConnectionName), CancellationToken.None))
      {
        finalEnt = (await conv.Query<TestEntity, long>()
          .Where(r => r.Name == "Hans Paul 2")
          .ToListAsync().ConfigureAwait(false)).FirstOrDefault();
      }
      finalEnt.Should().NotBeNull();
    }
  }
}