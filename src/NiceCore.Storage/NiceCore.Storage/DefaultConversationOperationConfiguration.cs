﻿namespace NiceCore.Storage
{
  /// <summary>
  /// Default impl for IConversationOperationConfiguration
  /// </summary>
  public class DefaultConversationOperationConfiguration : IConversationOperationConfiguration
  {
    /// <summary>
    /// Whether to validate instances before operation
    /// </summary>
    public required bool ValidateInstance { get; init; }

    /// <summary>
    /// Is Default Instance
    /// </summary>
    public required bool IsDefaultInstance { get; init; }
  }
}
