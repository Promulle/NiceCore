﻿using System.Collections.Generic;

namespace NiceCore.Storage.Cascading
{
  /// <summary>
  /// Relations for cascading. Default impl for instances
  /// </summary>
  public class DefaultCascadingRelation : ICascadingRelation
  {
    /// <summary>
    /// PropertyName
    /// </summary>
    public string PropertyName { get; set; }

    /// <summary>
    /// Name of property that references the relation
    /// </summary>
    public string ReferencingPropertyName { get; set; }

    /// <summary>
    /// Relation Kind
    /// </summary>
    public CascadeRelationKinds RelationKind { get; set; }

    /// <summary>
    /// Sub Relations to be executed too.
    /// </summary>
    public List<DefaultCascadingRelation> SubRelations { get; set; }
  }
}
