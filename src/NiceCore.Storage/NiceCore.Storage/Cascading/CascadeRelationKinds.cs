﻿namespace NiceCore.Storage.Cascading
{
  /// <summary>
  /// Relation kinds for cascading
  /// </summary>
  public enum CascadeRelationKinds
  {
    /// <summary>
    /// Many To One
    /// </summary>
    ManyToOne,

    /// <summary>
    /// One To One
    /// </summary>
    OneToOne,

    /// <summary>
    /// One To Many
    /// </summary>
    OneToMany
  }
}
