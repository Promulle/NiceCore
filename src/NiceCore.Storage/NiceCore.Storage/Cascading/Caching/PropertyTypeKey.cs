﻿using System;
using System.Collections.Generic;

namespace NiceCore.Storage.Cascading.Caching
{
  /// <summary>
  /// Key for property type cached
  /// </summary>
  public class PropertyTypeKey
  {
    /// <summary>
    /// Property name
    /// </summary>
    public string PropertyName { get; init; }

    /// <summary>
    /// ObjectType
    /// </summary>
    public Type ObjectType { get; init; }

    /// <summary>
    /// Equals
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj)
    {
      return obj is PropertyTypeKey key &&
             PropertyName == key.PropertyName &&
             EqualityComparer<Type>.Default.Equals(ObjectType, key.ObjectType);
    }

    /// <summary>
    /// GetHashcode
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode()
    {
      return HashCode.Combine(PropertyName, ObjectType);
    }
  }
}
