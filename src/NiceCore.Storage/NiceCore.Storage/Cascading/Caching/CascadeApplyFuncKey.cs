﻿using System;
using System.Collections.Generic;

namespace NiceCore.Storage.Cascading.Caching
{
  /// <summary>
  /// Key that is used for chaches that hold cascade apply funcs
  /// </summary>
  public class CascadeApplyFuncKey
  {
    /// <summary>
    /// Property name
    /// </summary>
    public string PropertyName { get; init; }

    /// <summary>
    /// ObjectType
    /// </summary>
    public Type ObjectType { get; init; }

    /// <summary>
    /// Id Type
    /// </summary>
    public Type IdType { get; init; }

    /// <summary>
    /// Property Type
    /// </summary>
    public Type PropertyType { get; init; }

    /// <summary>
    /// MethodName
    /// </summary>
    public string MethodName { get; init; }

    /// <summary>
    /// Equals
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj)
    {
      return obj is CascadeApplyFuncKey key &&
             PropertyName == key.PropertyName &&
             EqualityComparer<Type>.Default.Equals(ObjectType, key.ObjectType) &&
             EqualityComparer<Type>.Default.Equals(IdType, key.IdType) &&
             EqualityComparer<Type>.Default.Equals(PropertyType, key.PropertyType) &&
             MethodName == key.MethodName;
    }

    /// <summary>
    /// GetHashCode
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode()
    {
      return HashCode.Combine(PropertyName, ObjectType, IdType, MethodName, PropertyType);
    }
  }
}
