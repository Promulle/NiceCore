﻿using System;
using System.Collections.Generic;

namespace NiceCore.Storage.Cascading.Caching
{
  /// <summary>
  /// Cache Key for 2 types
  /// </summary>
  public class DoubleTypeKey
  {
    /// <summary>
    /// First Type
    /// </summary>
    public Type FirstType { get; init; }

    /// <summary>
    /// Second Type
    /// </summary>
    public Type SecondType { get; init; }

    /// <summary>
    /// Equals
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj)
    {
      return obj is DoubleTypeKey key &&
             EqualityComparer<Type>.Default.Equals(FirstType, key.FirstType) &&
             EqualityComparer<Type>.Default.Equals(SecondType, key.SecondType);
    }

    /// <summary>
    /// GetHashCode
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode()
    {
      return HashCode.Combine(FirstType, SecondType);
    }
  }
}
