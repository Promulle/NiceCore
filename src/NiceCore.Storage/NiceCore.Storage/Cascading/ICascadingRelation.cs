﻿using System.Collections.Generic;

namespace NiceCore.Storage.Cascading
{
  /// <summary>
  /// Strategy for cascading saves
  /// </summary>
  public interface ICascadingRelation
  {
    /// <summary>
    /// Name of the property
    /// </summary>
    string PropertyName { get; }

    /// <summary>
    /// Name of the property that references the object that was persisted
    /// </summary>
    string ReferencingPropertyName { get; }

    /// <summary>
    /// Kind of the relation
    /// </summary>
    CascadeRelationKinds RelationKind { get; }

    /// <summary>
    /// Sub Relations
    /// </summary>
    List<DefaultCascadingRelation> SubRelations { get; }
  }
}
