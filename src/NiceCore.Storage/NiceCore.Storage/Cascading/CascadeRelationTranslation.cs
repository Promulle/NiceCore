﻿using NiceCore.Base.Caching;
using NiceCore.Caches;
using NiceCore.Entities;
using NiceCore.Extensions;
using NiceCore.Reflection;
using NiceCore.Services.Validation.Model;
using NiceCore.Storage.Cascading.Caching;
using NiceCore.Storage.Cascading.Utils;
using NiceCore.Storage.Conversations;
using Polly;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace NiceCore.Storage.Cascading
{
  /// <summary>
  /// Class that translates ICascadeRelation into actual functions
  /// </summary>
  public static class CascadeRelationTranslation
  {
    private static readonly SimpleCache<PropertyTypeKey, Type> m_PropTypeCache = new();
    private static readonly SimpleCache<PropertyTypeKey, Func<object, object>> m_PropAccessorCache = new();
    private static readonly SimpleCache<CascadeApplyFuncKey, Func<IConversation, object, IAsyncPolicy, IEnumerable<ICascadingRelation>, Task>> m_CascadeApplyFuncCache = new();

    /// <summary>
    /// Applies the save relation and executes any subcalls using t_Conversation
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Object"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Relations"></param>
    /// <returns></returns>
    public static Task ApplySaveCascadeRelation<T, TId>(IConversation t_Conversation, T i_Object, IAsyncPolicy i_Policy, IEnumerable<ICascadingRelation> i_Relations) where T : IDomainEntity<TId>
    {
      return ApplyRelations(nameof(IConversation.SaveOrUpdateAsync), true, i_Relations, typeof(TId), t_Conversation, i_Policy, i_Object);
    }

    /// <summary>
    /// Applies the delete relation and executes any subcalls using t_Conversation
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Object"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Relations"></param>
    /// <returns></returns>
    public static Task ApplyDeleteCascadeRelation<T, TId>(IConversation t_Conversation, T i_Object, IAsyncPolicy i_Policy, IEnumerable<ICascadingRelation> i_Relations) where T : IDomainEntity<TId>
    {
      return ApplyRelations(nameof(IConversation.DeleteAsync), false, i_Relations, typeof(TId), t_Conversation, i_Policy, i_Object);
    }

    private static Task ApplyRelations(
      string i_MethodName,
      bool i_IsSave,
      IEnumerable<ICascadingRelation> i_Relations,
      Type i_IdType,
      IConversation t_Conversation,
      IAsyncPolicy i_Policy,
      object i_Object)
    {
      if (i_Relations != null)
      {
        var objectType = i_Object.GetType();
        return i_Relations.ForEachItem(async relation =>
        {
          var propTypeKey = new PropertyTypeKey()
          {
            ObjectType = objectType,
            PropertyName = relation.PropertyName
          };
          var propType = m_PropTypeCache.GetOrCreate(propTypeKey, () => objectType.GetProperty(relation.PropertyName).PropertyType);
          var propAccessor = m_PropAccessorCache.GetOrCreate(propTypeKey, () => GetPropertyAccessor(objectType, relation.PropertyName));
          var propInstance = propAccessor.Invoke(i_Object);
          if (ReflectionUtils.IsGenericCollectionType(propInstance.GetType()))
          {
            foreach (var inst in (propInstance as IEnumerable))
            {
              await ApplySingleRelation(i_IsSave, i_MethodName, relation, objectType, inst.GetType(), i_IdType, i_Object, inst, t_Conversation, i_Policy).ConfigureAwait(false);
            }
          }
          else
          {
            await ApplySingleRelation(i_IsSave, i_MethodName, relation, objectType, propType, i_IdType, i_Object, propInstance, t_Conversation, i_Policy).ConfigureAwait(false);
          }
        });
      }
      return Task.CompletedTask;
    }

    private static async Task ApplySingleRelation(
      bool i_IsSave,
      string i_MethodName,
      ICascadingRelation i_Relation,
      Type i_ObjectType,
      Type i_PropertyType,
      Type i_IdType,
      object i_Object,
      object i_PropInstance,
      IConversation t_Conversation,
      IAsyncPolicy i_Policy)
    {
      if (i_IsSave && i_Relation.RelationKind == CascadeRelationKinds.OneToMany && i_Relation.ReferencingPropertyName != null)
      {
        SetRefId(i_ObjectType, i_Object, i_PropertyType, i_PropInstance, i_IdType, i_Relation);
      }
      await ApplyRelation(i_Relation, i_ObjectType, i_IdType, i_MethodName, t_Conversation, i_Policy, i_PropInstance).ConfigureAwait(false);
      if (i_IsSave && i_Relation.ReferencingPropertyName != null && (i_Relation.RelationKind == CascadeRelationKinds.ManyToOne || i_Relation.RelationKind == CascadeRelationKinds.OneToOne))
      {
        SetRefId(i_PropertyType, i_PropInstance, i_ObjectType, i_Object, i_IdType, i_Relation);
      }
    }

    /// <summary>
    /// This functions sets the id from i_IdSource into the property defined by ReferencingPropertyName in i_IdTarget
    /// </summary>
    /// <param name="i_IdSourceType"></param>
    /// <param name="i_IdSource">Source for ID value, using ID Property</param>
    /// <param name="i_IdTargetType"></param>
    /// <param name="t_IdTarget">Instance whichs property identified by ReferencingPropertyName will be set to the vlaue of "ID" from i_IdSource</param>
    /// <param name="i_IdType"></param>
    /// <param name="i_Relation"></param>
    private static void SetRefId(Type i_IdSourceType, object i_IdSource, Type i_IdTargetType, object t_IdTarget, Type i_IdType, ICascadingRelation i_Relation)
    {
      const string idPropertyName = nameof(IDomainEntity<object>.ID);
      var parentIdKey = new PropertyTypeKey() { ObjectType = i_IdSourceType, PropertyName = idPropertyName };
      var parentIdFunc = m_PropAccessorCache.GetOrCreate(parentIdKey, () => GetPropertyAccessor(i_IdSourceType, idPropertyName));
      var id = parentIdFunc.Invoke(i_IdSource);
      var key = new PropertySetterCacheKey() { DeclaringType = i_IdTargetType, PropertyName = i_Relation.ReferencingPropertyName, PropertyType = i_IdType };
      var refIdPropSetter = GetCorrectPropertySetter(key, i_IdType);
      refIdPropSetter.Invoke(t_IdTarget, id);
    }

    private static Action<object, object> GetCorrectPropertySetter(PropertySetterCacheKey i_Key, Type i_idType)
    {
      var typeKey = new PropertyTypeCacheKey()
      {
        DeclaringType = i_Key.DeclaringType,
        PropertyName = i_Key.PropertyName
      };
      var typeOfReferencingProperty = ExpressionCaches.PropertyTypeCache.Get(typeKey);
      if (i_idType != typeOfReferencingProperty)
      {
        var domainType = ExpressionCaches.DomainEntityInterfaceForParameterTypeCache.Get(i_idType);
        if (!domainType.IsAssignableFrom(typeOfReferencingProperty))
        {
          throw new InvalidOperationException($"Cannot create wrapper object setter expression for type {typeOfReferencingProperty}. Type must implement {domainType.FullName}");
        }
        var key = new ObjectIDWrapperSetterCacheKey()
        {
          HostType = i_Key.DeclaringType,
          IdType = i_idType,
          PropertyName = i_Key.PropertyName,
          PropertyType = typeOfReferencingProperty
        };
        return CascadingTranslationCaches.ObjectIDWrapperSetterCache.Get(key);
      }
      return ExpressionCaches.PropertySetterCache.Get(i_Key);
    }

    private static Task ApplyRelation(ICascadingRelation i_Relation,
      Type i_ObjectType,
      Type i_IdType,
      string i_MethodName,
      IConversation t_Conversation,
      IAsyncPolicy i_Policy,
      object i_PropertyValue)
    {
      var key = new CascadeApplyFuncKey()
      {
        IdType = i_IdType,
        ObjectType = i_ObjectType,
        PropertyType = i_PropertyValue.GetType(),
        MethodName = i_MethodName,
        PropertyName = i_Relation.PropertyName
      };
      var applyFunc = m_CascadeApplyFuncCache.GetOrCreate(key, () => GetCascadeApplyMethod(i_PropertyValue.GetType(), i_IdType, i_MethodName));
      return applyFunc.Invoke(t_Conversation, i_PropertyValue, i_Policy, i_Relation.SubRelations);
    }

    private static Func<object, object> GetPropertyAccessor(Type i_ObjectType, string i_PropertyName) //TODO: move into general thing
    {
      var objParamExpr = Expression.Parameter(typeof(object));
      var castedparamExpr = Expression.Convert(objParamExpr, i_ObjectType);
      var propExpr = Expression.Property(castedparamExpr, i_PropertyName);
      var propExprObject = Expression.Convert(propExpr, typeof(object));
      return Expression.Lambda<Func<object, object>>(propExprObject, objParamExpr).Compile();
    }

    private static Func<IConversation, object, IAsyncPolicy, IEnumerable<ICascadingRelation>, Task> GetCascadeApplyMethod(Type i_ObjectType, Type i_IdType, string i_MethodName)
    {
      var objectParamExpr = Expression.Parameter(typeof(object));
      var castedParam = Expression.Convert(objectParamExpr, i_ObjectType);
      var convParamExpr = Expression.Parameter(typeof(INotApplyingConversation));
      var subRelationParamExpr = Expression.Parameter(typeof(IEnumerable<ICascadingRelation>));
      var pollyPolicyParamExpr = Expression.Parameter(typeof(IAsyncPolicy));
      var nodeExpr = Expression.Constant(null, typeof(SubValidationNode));
      var typeParams = new List<MethodParameterType>()
      {
        MethodParameterType.GenericPlaceHolder(),
        MethodParameterType.Of(typeof(IAsyncPolicy)),
        MethodParameterType.Of(typeof(IEnumerable<ICascadingRelation>)),
        MethodParameterType.Of(typeof(SubValidationNode))
      };
      var method = ReflectionUtils.GetMethodEx(typeof(INotApplyingConversation), i_MethodName, typeParams);
      var genericMethod = method.MakeGenericMethod(i_ObjectType, i_IdType);
      var methodCallExpr = Expression.Call(convParamExpr, genericMethod, castedParam, pollyPolicyParamExpr, subRelationParamExpr, nodeExpr);
      return Expression.Lambda<Func<IConversation, object, IAsyncPolicy, IEnumerable<ICascadingRelation>, Task>>(methodCallExpr, convParamExpr, objectParamExpr, pollyPolicyParamExpr, subRelationParamExpr)
        .Compile();
    }
  }
}
