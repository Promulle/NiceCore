﻿using System.Collections.Generic;
using System.Linq;

namespace NiceCore.Storage.Cascading
{
  /// <summary>
  /// static clas for creating relations
  /// </summary>
  public static class CascadingRelations
  {
    /// <summary>
    /// Creates a ManyToOne relation for Property i_PropertyName that is referenced in the ParentObject in the property i_ReferencingPropertyName
    /// </summary>
    /// <param name="i_PropertyName"></param>
    /// <returns></returns>
    public static DefaultCascadingRelation CreateManyToOne(string i_PropertyName)
    {
      return Create(CascadeRelationKinds.ManyToOne, i_PropertyName, null, new List<DefaultCascadingRelation>());
    }

    /// <summary>
    /// Creates a ManyToOne relation for Property i_PropertyName that is referenced in the ParentObject in the property i_ReferencingPropertyName
    /// </summary>
    /// <param name="i_PropertyName"></param>
    /// <returns></returns>
    public static DefaultCascadingRelation CreateOneToOne(string i_PropertyName)
    {
      return Create(CascadeRelationKinds.OneToOne, i_PropertyName, null, new List<DefaultCascadingRelation>());
    }

    /// <summary>
    /// Creates a ManyToOne relation for Property i_PropertyName that is referenced in the SubObject in the property i_ReferencingPropertyName
    /// </summary>
    /// <param name="i_PropertyName"></param>
    /// <param name="i_ReferencingPropertyName"></param>
    /// <returns></returns>
    public static DefaultCascadingRelation CreateOneToMany(string i_PropertyName, string i_ReferencingPropertyName)
    {
      return Create(CascadeRelationKinds.OneToMany, i_PropertyName, i_ReferencingPropertyName, new List<DefaultCascadingRelation>());
    }

    /// <summary>
    /// Creates a ManyToOne relation for Property i_PropertyName that is referenced in the SubObject in the property i_ReferencingPropertyName
    /// </summary>
    /// <param name="i_PropertyName"></param>
    /// <param name="i_ReferencingPropertyName"></param>
    /// <param name="i_SubRelations"></param>
    /// <returns></returns>
    public static DefaultCascadingRelation CreateOneToMany(string i_PropertyName, string i_ReferencingPropertyName, IEnumerable<DefaultCascadingRelation> i_SubRelations)
    {
      return Create(CascadeRelationKinds.OneToMany, i_PropertyName, i_ReferencingPropertyName, i_SubRelations);
    }

    /// <summary>
    /// Creates a ManyToOne relation for Property i_PropertyName that is referenced in the ParentObject in the property i_ReferencingPropertyName
    /// </summary>
    /// <param name="i_PropertyName"></param>
    /// <param name="i_SubRelations"></param>
    /// <returns></returns>
    public static DefaultCascadingRelation CreateManyToOne(string i_PropertyName, IEnumerable<DefaultCascadingRelation> i_SubRelations)
    {
      return Create(CascadeRelationKinds.ManyToOne, i_PropertyName, null, i_SubRelations);
    }

    /// <summary>
    /// Creates a ManyToOne relation for Property i_PropertyName that is referenced in the ParentObject in the property i_ReferencingPropertyName
    /// </summary>
    /// <param name="i_PropertyName"></param>
    /// <param name="i_SubRelations"></param>
    /// <returns></returns>
    public static DefaultCascadingRelation CreateOneToOne(string i_PropertyName, IEnumerable<DefaultCascadingRelation> i_SubRelations)
    {
      return Create(CascadeRelationKinds.OneToOne, i_PropertyName, null, i_SubRelations);
    }

    /// <summary>
    /// Creates a cascading relation with all info filled
    /// </summary>
    /// <param name="i_Kind"></param>
    /// <param name="i_PropertyName"></param>
    /// <param name="i_ReferencingPropertyName"></param>
    /// <param name="i_SubRelations"></param>
    /// <returns></returns>
    public static DefaultCascadingRelation Create(CascadeRelationKinds i_Kind, string i_PropertyName, string i_ReferencingPropertyName, IEnumerable<DefaultCascadingRelation> i_SubRelations)
    {
      return new DefaultCascadingRelation()
      {
        PropertyName = i_PropertyName,
        ReferencingPropertyName = i_ReferencingPropertyName,
        RelationKind = i_Kind,
        SubRelations = i_SubRelations.ToList(),
      };
    }
  }
}
