﻿using NiceCore.Base.Caching;
using NiceCore.Entities;
using System;
using System.Linq.Expressions;

namespace NiceCore.Storage.Cascading.Utils
{
  /// <summary>
  /// Base Specialized Cache for ObjectWrapper Setter
  /// </summary>
  public class ObjectIDWrapperSetterCache : BaseSpecializedSimpleCache<ObjectIDWrapperSetterCacheKey, Action<object, object>>
  {
    /// <summary>
    /// Create Func
    /// </summary>
    /// <param name="i_Key"></param>
    /// <returns></returns>
    protected override Action<object, object> CreateFunc(ObjectIDWrapperSetterCacheKey i_Key)
    {
      return GetSetter(i_Key.HostType, i_Key.PropertyType, i_Key.IdType, i_Key.PropertyName);
    }

    /// <summary>
    /// Returns the setter for the parameters
    /// </summary>
    /// <param name="i_HostType"></param>
    /// <param name="i_PropertyType"></param>
    /// <param name="i_IdType"></param>
    /// <param name="i_PropertyName"></param>
    /// <returns></returns>
    private static Action<object, object> GetSetter(Type i_HostType, Type i_PropertyType, Type i_IdType, string i_PropertyName)
    {
      var hostParamExpr = Expression.Parameter(typeof(object));
      var castedHostParamExpr = Expression.Convert(hostParamExpr, i_HostType);
      var valueParamExpr = Expression.Parameter(typeof(object));
      var castedValueParamExpr = Expression.Convert(valueParamExpr, i_IdType);

      var newExpr = Expression.New(i_PropertyType);
      var property = i_PropertyType.GetProperty(nameof(IDomainEntity<object>.ID));
      var idAssignmentExpression = Expression.Bind(property, castedValueParamExpr);
      var initExpr = Expression.MemberInit(newExpr, idAssignmentExpression);

      var hostPropertyExpr = Expression.Property(castedHostParamExpr, i_PropertyName);
      var assignExpr = Expression.Assign(hostPropertyExpr, initExpr);
      var actionExpr = Expression.Lambda<Action<object, object>>(assignExpr, hostParamExpr, valueParamExpr);
      return actionExpr.Compile();
    }
  }
}
