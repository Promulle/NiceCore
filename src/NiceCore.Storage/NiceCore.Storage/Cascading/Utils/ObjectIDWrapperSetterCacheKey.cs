﻿using NiceCore.Base.Caching;
using System;
using System.Collections.Generic;

namespace NiceCore.Storage.Cascading.Utils
{
  /// <summary>
  /// Cache Key for ObjectIDWrapperSetterCache
  /// </summary>
  public class ObjectIDWrapperSetterCacheKey : BaseCacheKey
  {
    /// <summary>
    /// Type that thas the wrapper property
    /// </summary>
    public Type HostType { get; init; }

    /// <summary>
    /// Type of the id wrapper
    /// </summary>
    public Type PropertyType { get; init; }

    /// <summary>
    /// Type of id to set
    /// </summary>
    public Type IdType { get; init; }

    /// <summary>
    /// Name of idwrapper property
    /// </summary>
    public string PropertyName { get; init; }

    /// <summary>
    /// Equals
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj)
    {
      return obj is ObjectIDWrapperSetterCacheKey key &&
             EqualityComparer<Type>.Default.Equals(HostType, key.HostType) &&
             EqualityComparer<Type>.Default.Equals(PropertyType, key.PropertyType) &&
             EqualityComparer<Type>.Default.Equals(IdType, key.IdType) &&
             PropertyName == key.PropertyName;
    }

    /// <summary>
    /// GetHashCode
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode()
    {
      return HashCode.Combine(HostType, PropertyType, IdType, PropertyName);
    }
  }
}
