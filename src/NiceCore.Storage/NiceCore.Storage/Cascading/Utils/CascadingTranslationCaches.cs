﻿namespace NiceCore.Storage.Cascading.Utils
{
  /// <summary>
  /// Static class holding caches for cascade relation translations
  /// </summary>
  public static class CascadingTranslationCaches
  {
    /// <summary>
    /// Cache holding setters, which set a new instance of the wrapper type (with filled ID) into the first parameter
    /// </summary>
    public static ObjectIDWrapperSetterCache ObjectIDWrapperSetterCache { get; } = new();
  }
}
