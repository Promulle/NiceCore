using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using NiceCore.Base.Caching;
using NiceCore.Entities;
using NiceCore.Eventing.Messages;
using NiceCore.Reflection;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Diffing;

namespace NiceCore.Storage.Messages.Helpers
{
  /// <summary>
  /// Class for creating storage messages when no generics are involved
  /// </summary>
  public static class StorageMessageFactory
  {
    //cache that caches type of message to the func used to create a new instance of it that has value and context from function filled
    private static readonly SimpleCache<Type, Func<object, IConversationContext, IObjectMessage>> m_CreateNotUpdatedMessageFuncCache = new();

    //cache that caches type of message to the func used to create a new instance of it that has value and context from function filled
    private static readonly SimpleCache<Type, Func<object, object, IConversationContext, IObjectMessage>> m_CreateUpdatedMessageFuncCache = new();

    /// <summary>
    /// Create a new message of type i_MessageType
    /// </summary>
    /// <param name="i_NewValues"></param>
    /// <param name="i_Context"></param>
    /// <param name="i_MessageType"></param>
    /// <returns></returns>
    public static IObjectMessage CreateNewNotUpdatedMessage(object i_NewValues, IConversationContext i_Context, Type i_MessageType)
    {
      var entityType = i_MessageType.GetGenericArguments().First();
      var func = m_CreateNotUpdatedMessageFuncCache.GetOrCreate(i_MessageType, () =>
      {
        var parameters = new List<MethodParameterType>()
        {
          MethodParameterType.Of(typeof(IList<>), new()
          {
            MethodParameterType.Of(entityType)
          }),
          MethodParameterType.Of(typeof(IConversationContext))
        };
        var createMethod         = ReflectionUtils.GetMethodEx(i_MessageType, nameof(EntitiesCreatedStorageMessage<IDomainEntity<Guid>, Guid>.Create), parameters);
        var ctxParamExpr         = Expression.Parameter(typeof(IConversationContext));
        var valueParamExpr       = Expression.Parameter(typeof(object));
        var valueAsListParamExpr = Expression.Convert(valueParamExpr, typeof(IList<>).MakeGenericType(entityType));
        var callExpr             = Expression.Call(createMethod, valueAsListParamExpr, ctxParamExpr);
        var funcExpr             = Expression.Lambda<Func<object, IConversationContext, IObjectMessage>>(callExpr, valueParamExpr, ctxParamExpr);
        return funcExpr.Compile();
      });
      return func.Invoke(i_NewValues, i_Context);
    }

    /// <summary>
    /// Create New updated message
    /// </summary>
    /// <param name="i_NewValues"></param>
    /// <param name="i_DiffResultDictionary"></param>
    /// <param name="i_Context"></param>
    /// <param name="i_MessageType"></param>
    /// <returns></returns>
    public static IObjectMessage CreateNewUpdatedMessage(object i_NewValues, object i_DiffResultDictionary, IConversationContext i_Context, Type i_MessageType)
    {
      var entityType = i_MessageType.GetGenericArguments().First();
      var idType     = i_MessageType.GetGenericArguments()[^1];
      var func = m_CreateUpdatedMessageFuncCache.GetOrCreate(i_MessageType, () =>
      {
        var parameters = new List<MethodParameterType>()
        {
          MethodParameterType.Of(typeof(IList<>), new()
          {
            MethodParameterType.Of(entityType)
          }),
          MethodParameterType.Of(typeof(Dictionary<,>), new()
          {
            MethodParameterType.Of(idType),
            MethodParameterType.Of(typeof(StorageDiffResult))
          }),
          MethodParameterType.Of(typeof(IConversationContext))
        };
        var createMethod               = ReflectionUtils.GetMethodEx(i_MessageType, nameof(EntitiesUpdatedStorageMessage<IDomainEntity<Guid>, Guid>.Create), parameters);
        var ctxParamExpr               = Expression.Parameter(typeof(IConversationContext));
        var valueParamExpr             = Expression.Parameter(typeof(object));
        var diffResultParamExpr        = Expression.Parameter(typeof(object));
        var valueAsListParamExpr       = Expression.Convert(valueParamExpr, typeof(IList<>).MakeGenericType(entityType));
        var diffResultCorrectParamExpr = Expression.Convert(diffResultParamExpr, typeof(Dictionary<,>).MakeGenericType(idType, typeof(StorageDiffResult)));
        var callExpr                   = Expression.Call(createMethod, valueAsListParamExpr, diffResultCorrectParamExpr, ctxParamExpr);
        var funcExpr                   = Expression.Lambda<Func<object, object, IConversationContext, IObjectMessage>>(callExpr, valueParamExpr, diffResultParamExpr, ctxParamExpr);
        return funcExpr.Compile();
      });
      return func.Invoke(i_NewValues, i_DiffResultDictionary, i_Context);
    }
  }
}
