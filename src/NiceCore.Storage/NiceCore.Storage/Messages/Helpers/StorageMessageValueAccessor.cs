using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using NiceCore.Base.Caching;
using NiceCore.Entities;
using NiceCore.Eventing.Messages;
using NiceCore.Reflection;
using NiceCore.Storage.Conversations;

namespace NiceCore.Storage.Messages.Helpers
{
  /// <summary>
  /// Static class providing untyped access to storage messages
  /// </summary>
  public static class StorageMessageValueAccessor
  {
    //cache that caches the message type to a function that can be used to extract the entities list as T
    private static readonly SimpleCache<Type, Func<IObjectMessage, object>> m_GetValueFuncCache = new();
    
    //cache that caches the message type to a function that can be used to extract the diffresult of a message
    private static readonly SimpleCache<Type, Func<IObjectMessage, object>> m_GetDiffResultFuncCache = new();

    //cache that caches the message type to a function that can be used to extract the entities list as T
    private static readonly SimpleCache<Type, Func<IObjectMessage, IConversationContext>> m_GetContextFuncCache = new();

    /// <summary>
    /// get Value of i_Message. This function returns "object" which should be a IList of T. It stays an object so reusing it for setting is easier
    /// </summary>
    /// <param name="i_Message"></param>
    /// <param name="i_EntityType"></param>
    /// <param name="i_IdType"></param>
    /// <returns></returns>
    public static object GetEntities(IObjectMessage i_Message, Type i_EntityType, Type i_IdType)
    {
      var func = m_GetValueFuncCache.GetOrCreate(i_Message.GetType(), () =>
      {
        var paramExpr       = Expression.Parameter(typeof(IObjectMessage));
        var convExpr = Expression.Convert(paramExpr, i_Message.GetType());
        var getEntitiesFunc = GetGetEntitiesFunc(i_EntityType, i_IdType);
        var callExpr        = Expression.Call(convExpr, getEntitiesFunc);
        var funcExpr        = Expression.Lambda<Func<IObjectMessage, object>>(callExpr, paramExpr);
        return funcExpr.Compile();
      });
      return func.Invoke(i_Message);
    }

    /// <summary>
    /// Retrieves the diff result from i_Message
    /// </summary>
    /// <param name="i_Message"></param>
    /// <param name="i_EntityType"></param>
    /// <param name="i_IdType"></param>
    /// <returns></returns>
    public static object GetDiffResult(IObjectMessage i_Message, Type i_EntityType, Type i_IdType)
    {
      var func = m_GetDiffResultFuncCache.GetOrCreate(i_Message.GetType(), () =>
      {
        var paramExpr       = Expression.Parameter(typeof(IObjectMessage));
        var convExpr        = Expression.Convert(paramExpr, i_Message.GetType());
        var getEntitiesFunc = GetGetDiffResultFunc(i_EntityType, i_IdType);
        var callExpr        = Expression.Call(convExpr, getEntitiesFunc);
        var funcExpr        = Expression.Lambda<Func<IObjectMessage, object>>(callExpr, paramExpr);
        return funcExpr.Compile();
      });
      return func.Invoke(i_Message);
    }

    /// <summary>
    /// get context of i_Message.
    /// </summary>
    /// <param name="i_Message"></param>
    /// <param name="i_EntityType"></param>
    /// <param name="i_IdType"></param>
    /// <returns></returns>
    public static IConversationContext GetContext(IObjectMessage i_Message, Type i_EntityType, Type i_IdType)
    {
      var func = m_GetContextFuncCache.GetOrCreate(i_Message.GetType(), () =>
      {
        var paramExpr       = Expression.Parameter(typeof(IObjectMessage));
        var convExpr        = Expression.Convert(paramExpr, i_Message.GetType());
        var getEntitiesFunc = GetGetContextFunc(i_EntityType, i_IdType);
        var callExpr        = Expression.Call(convExpr, getEntitiesFunc);
        var funcExpr        = Expression.Lambda<Func<IObjectMessage, IConversationContext>>(callExpr, paramExpr);
        return funcExpr.Compile();
      });
      return func.Invoke(i_Message);
    }

    private static MethodInfo GetGetContextFunc(Type i_EntityType, Type i_IdType)
    {
      var actualType = typeof(BaseStorageMessage<,>).MakeGenericType(i_EntityType, i_IdType);
      return ReflectionUtils.GetMethodEx(actualType, nameof(BaseStorageMessage<IDomainEntity<Guid>, Guid>.GetContextFromMessage), 0, 0, Array.Empty<MethodParameterType>());
    }

    private static MethodInfo GetGetEntitiesFunc(Type i_EntityType, Type i_IdType)
    {
      var type = typeof(BaseStorageMessage<,>).MakeGenericType(i_EntityType, i_IdType);
      return ReflectionUtils.GetMethodEx(type, nameof(BaseStorageMessage<IDomainEntity<Guid>, Guid>.GetEntitiesFromMessage), 0, 0, Array.Empty<MethodParameterType>());
    }
    
    private static MethodInfo GetGetDiffResultFunc(Type i_EntityType, Type i_IdType)
    {
      var type = typeof(BaseStorageMessage<,>).MakeGenericType(i_EntityType, i_IdType);
      return ReflectionUtils.GetMethodEx(type, nameof(BaseStorageMessage<IDomainEntity<Guid>, Guid>.GetDiffResultFromMessage), 0, 0, Array.Empty<MethodParameterType>());
    }
  }
}
