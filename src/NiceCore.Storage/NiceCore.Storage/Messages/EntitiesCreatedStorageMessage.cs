﻿using System;
using System.Collections.Generic;
using NiceCore.Entities;
using NiceCore.Eventing.Messages;
using NiceCore.Reflection;
using NiceCore.Storage.Conversations;

namespace NiceCore.Storage.Messages
{
  /// <summary>
  /// Storage message for entity creation
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <typeparam name="TId"></typeparam>
  public class EntitiesCreatedStorageMessage<T, TId> : BaseStorageMessage<T, TId>,  IEntitiesCreatedStorageMessage<T, TId>
    where T : IDomainEntity<TId>
  {
    /// <summary>
    /// Message Topic
    /// </summary>
    public override string MessageTopic { get; } = StorageMessages.Topics.ENTITY_CREATED + "_" + typeof(T).Name;
    
    /// <summary>
    /// Creates a new EntityCreatedMessage
    /// </summary>
    /// <param name="i_Entities"></param>
    /// <param name="i_Context"></param>
    /// <returns></returns>
    public static EntitiesCreatedStorageMessage<T, TId> Create(IList<T> i_Entities, IConversationContext i_Context)
    {
      return new ()
      {
        Value = new MessageValue<IEntitiesMessageWrapper<TId>>()
        {
          Value = new EntitiesMessageWrapper<T,TId>()
          {
            TimeAsTicks = Environment.TickCount64,
            EntityTypeName = ReflectionUtils.GetMinimalAssemblyQualifiedName(typeof(T)),
            ConversationContext = i_Context,
            Entities = i_Entities
          },
        }
      };
    }
  }
}
