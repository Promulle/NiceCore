﻿using System;
using System.Collections.Generic;
using NiceCore.Entities;
using NiceCore.Eventing.Messages;
using NiceCore.Reflection;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Diffing;

namespace NiceCore.Storage.Messages
{
  /// <summary>
  /// Message for entity updates
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <typeparam name="TId"></typeparam>
  public class EntitiesUpdatedStorageMessage<T, TId> : BaseStorageMessage<T, TId>, IEntitiesUpdatedStorageMessage<T, TId>
    where T : IDomainEntity<TId>
  {
    /// <summary>
    /// Topic
    /// </summary>
    public override string MessageTopic { get; } = StorageMessages.Topics.ENTITY_UPDATED + "_" + typeof(T).Name;

    /// <summary>
    /// Create
    /// </summary>
    /// <param name="i_Entities"></param>
    /// <param name="i_DiffResults"></param>
    /// <param name="i_Context"></param>
    /// <returns></returns>
    public static EntitiesUpdatedStorageMessage<T, TId> Create(IList<T> i_Entities, Dictionary<TId, StorageDiffResult> i_DiffResults, IConversationContext i_Context)
    {
      var res = new EntitiesUpdatedStorageMessage<T, TId>()
      {
        Value = new MessageValue<EntitiesMessageWrapper<T, TId>>()
        {
          Value = new()
          {
            TimeAsTicks = Environment.TickCount64,
            EntityTypeName = ReflectionUtils.GetMinimalAssemblyQualifiedName(typeof(T)),
            Entities = i_Entities,
            ConversationContext = i_Context
          }
        }
      };
      foreach (var entity in i_Entities)
      {
        if (i_DiffResults.TryGetValue(entity.ID, out var diffResult))
        {
          res.Value.Value.DiffResult[entity.ID] = diffResult;  
        }
      }
      return res;
    }
  }
};

