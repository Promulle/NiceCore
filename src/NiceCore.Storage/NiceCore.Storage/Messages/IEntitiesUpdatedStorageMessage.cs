namespace NiceCore.Storage.Messages
{
  /// <summary>
  /// Entities Updated Storage Message
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <typeparam name="TId"></typeparam>
  public interface IEntitiesUpdatedStorageMessage<out T, TId> : IStorageMessage<T,TId>
  {
    
  }
}