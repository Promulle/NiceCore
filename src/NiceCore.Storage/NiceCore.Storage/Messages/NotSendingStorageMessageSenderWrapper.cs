using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NiceCore.Entities;
using NiceCore.Eventing.Messaging.Parameters;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Messages.Providers;

namespace NiceCore.Storage.Messages
{
  /// <summary>
  /// Not Sending Storage Message Sender Wrapper
  /// </summary>
  public sealed class NotSendingStorageMessageSenderWrapper : IStorageMessageSender
  {
    private readonly IStorageMessageSender m_ActualSender;
    
    /// <summary>
    /// Context
    /// </summary>
    public IConversationContext Context => m_ActualSender.Context;

    /// <summary>
    /// Storage message sender wrapper
    /// </summary>
    /// <param name="i_ActualSender"></param>
    public NotSendingStorageMessageSenderWrapper(IStorageMessageSender i_ActualSender)
    {
      if (i_ActualSender == null)
      {
        throw new ArgumentException($"Could not create a new instance of {typeof(NotSendingStorageMessageSenderWrapper).FullName}. Provided actual sender was null");
      }
      m_ActualSender = i_ActualSender;
    }
    
    /// <summary>
    /// Entity Deleted
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_Parameters"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    public void EntityDeleted<T, TId>(T i_Entity, MessageProviderCreateMessageParameters i_Parameters) where T : IDomainEntity<TId>
    {
      m_ActualSender.EntityDeleted<T, TId>(i_Entity, i_Parameters);
    }

    /// <summary>
    /// Entity Updated
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_Parameters"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    public void EntityUpdated<T, TId>(T i_Entity, MessageProviderCreateMessageParameters i_Parameters) where T : IDomainEntity<TId>
    {
      m_ActualSender.EntityUpdated<T, TId>(i_Entity, i_Parameters);
    }

    /// <summary>
    /// Entity Created
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_Parameters"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    public void EntityCreated<T, TId>(T i_Entity, MessageProviderCreateMessageParameters i_Parameters) where T : IDomainEntity<TId>
    {
      m_ActualSender.EntityCreated<T, TId>(i_Entity, i_Parameters);
    }

    /// <summary>
    /// Send, Does nothing
    /// </summary>
    /// <param name="i_Awaiter"></param>
    public Task SendMessages(IMessageAwaiter i_Awaiter)
    {
      //Do Nothing
      return Task.CompletedTask;
    }

    /// <summary>
    /// Clear, does nothing
    /// </summary>
    public void ClearMessages()
    {
      //Do Nothing
    }

    
    /// <summary>
    /// Set Context, does nothing
    /// </summary>
    /// <param name="i_Context"></param>
    /// <param name="i_MessageContext"></param>
    /// <param name="i_MessageProviders"></param>
    public void SetContext(IConversationContext i_Context, IMessageSendingParameters i_MessageContext, IEnumerable<IStorageMessageProvider> i_MessageProviders)
    {
      //Do Nothing
    }
  }
}