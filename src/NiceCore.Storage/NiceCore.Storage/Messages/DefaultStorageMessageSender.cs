﻿using NiceCore.Entities;
using NiceCore.Eventing.Messaging;
using NiceCore.Logging;
using NiceCore.ServiceLocation;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Diffing;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Diagnostics;
using NiceCore.Eventing.Messaging.Parameters;
using NiceCore.Extensions;
using NiceCore.Logging.Extensions;
using NiceCore.Storage.Messages.Providers;
using NiceCore.Storage.Messages.Store;

namespace NiceCore.Storage.Messages
{
  /// <summary>
  /// Default impl for IStorageMessageSender
  /// </summary>
  [Register(InterfaceType = typeof(IStorageMessageSender))]
  public class DefaultStorageMessageSender : IStorageMessageSender
  {
    private readonly object m_LockObject = new();
    private StorageMessageStore m_MessageStore;

    /// <summary>
    /// Message Service
    /// </summary>
    public IMessageService MessageService { get; set; }

    /// <summary>
    /// Context for this message sender, can only be set once
    /// </summary>
    public IConversationContext Context { get; private set; }
    
    /// <summary>
    /// Message Conversation Context
    /// </summary>
    public IMessageSendingParameters MessageSendingParameters { get; private set; }

    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<DefaultStorageMessageSender> Logger { get; set; } = DiagnosticsCollector.Instance.GetLogger<DefaultStorageMessageSender>();

    /// <summary>
    /// Clears messages
    /// </summary>
    public void ClearMessages()
    {
      lock (m_LockObject)
      {
        Logger.Trace("Clearing Storage Message Sender");
        m_MessageStore?.Clear();
      }
    }

    /// <summary>
    /// Sets context.
    /// </summary>
    /// <param name="i_Context"></param>
    /// <param name="i_MessageSendingParameters"></param>
    /// <param name="i_MessageProviders"></param>
    /// <exception cref="InvalidOperationException">Throws an exception if it is called a second time, because altering the context is not wanted.</exception>
    public void SetContext(IConversationContext i_Context, IMessageSendingParameters i_MessageSendingParameters, IEnumerable<IStorageMessageProvider> i_MessageProviders)
    {
      if (Context == null)
      {
        MessageSendingParameters = i_MessageSendingParameters;
        Context = i_Context;
        m_MessageStore = new (i_Context, i_MessageProviders, Logger);
      }
      else
      {
        throw new InvalidOperationException("Context for StorageMessageSender was set more than one time. This is not allowed because all messages that are send by the conversation should be under the same context.");
      }
    }

    /// <summary>
    /// Entity created
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <param name="i_Entity"></param>
    /// <param name="i_Parameters"></param>
    public void EntityCreated<T, TId>(T i_Entity, MessageProviderCreateMessageParameters i_Parameters) where T : IDomainEntity<TId>
    {
      if (i_Entity != null)
      {
        Logger.Trace($"Possible new message for EntityCreated for type {typeof(T).FullName}");
        m_MessageStore.EntityCreated<T, TId>(i_Entity, i_Parameters);
      }
      else
      {
        Logger.Warning("Null was passed as value for EntityCreatedMessage. Skipping message.");
      }
    }

    /// <summary>
    /// Entity deleted
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <param name="i_Entity"></param>
    /// <param name="i_Parameters"></param>
    public void EntityDeleted<T, TId>(T i_Entity, MessageProviderCreateMessageParameters i_Parameters) where T : IDomainEntity<TId>
    {
      if (i_Entity != null)
      {
        Logger.Trace($"Possible new message for EntityDeleted for type {typeof(T).FullName}");
        m_MessageStore.EntityDeleted<T, TId>(i_Entity, i_Parameters);
      }
      else
      {
        Logger.Warning("Null was passed as value for EntityDeletedMessage. Skipping message.");
      }
    }

    /// <summary>
    /// Entity updated
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <param name="i_Entity"></param>
    /// <param name="i_Parameters"></param>
    public void EntityUpdated<T, TId>(T i_Entity, MessageProviderCreateMessageParameters i_Parameters) where T : IDomainEntity<TId>
    {
      if (i_Entity != null)
      {
        Logger.Trace($"Possible new message for EntityUpdated for type {typeof(T).FullName}");
        m_MessageStore.EntityUpdated<T, TId>(i_Entity, i_Parameters);
      }
      else
      {
        Logger.Warning("Null was passed as value for EntityUpdatedMessage. Skipping message.");
      }
    }

    /// <summary>
    /// Send messages
    /// </summary>
    public Task SendMessages(IMessageAwaiter i_Awaiter)
    {
      lock (m_LockObject)
      {
        Logger.Trace("Sending Messages...");
        var messages = m_MessageStore.GetAllMessages();
        var taskList = new List<Task>(messages.Count);
        foreach (var message in messages)
        {
          Logger.Information($"Sending message of type {message.GetType().FullName} for topic {message.MessageTopic}");
          var task = MessageService.SendMessage(message, MessageSendingParameters, true);
          i_Awaiter?.AddMessageTask(task);
          taskList.Add(task);
        }
        ClearMessages();
        Context = null;
        return Task.WhenAll(taskList);
      }
    }
  }
}
