namespace NiceCore.Storage.Messages
{
  /// <summary>
  /// Entities Created Storage Message
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <typeparam name="TId"></typeparam>
  public interface IEntitiesCreatedStorageMessage<out T, TId> : IStorageMessage<T, TId>
  {
    
  }
}