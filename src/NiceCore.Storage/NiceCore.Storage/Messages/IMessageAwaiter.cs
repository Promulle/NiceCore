using System.Threading.Tasks;

namespace NiceCore.Storage.Messages
{
  /// <summary>
  /// Interface for instances that can be used to await the completion of sent messages from inside of a Conversation
  /// </summary>
  public interface IMessageAwaiter
  {
    /// <summary>
    /// Tasks
    /// </summary>
    /// <param name="i_Task"></param>
    void AddMessageTask(Task i_Task);
    
    /// <summary>
    /// Awaits the completion of messages
    /// </summary>
    /// <returns></returns>
    ValueTask AwaitMessageCompletion();
  }
}
