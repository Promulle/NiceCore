﻿namespace NiceCore.Storage.Messages
{
  /// <summary>
  /// Constant class for Messages
  /// </summary>
  public static class StorageMessages
  {
    /// <summary>
    /// Topics used in storage messages
    /// </summary>
    public static class Topics
    {
      /// <summary>
      /// Constant for message topic
      /// </summary>
      public static readonly string ENTITY_CREATED = "ENTITY_CREATED";

      /// <summary>
      /// Constant for message topic
      /// </summary>
      public static readonly string ENTITY_UPDATED = "ENTITY_UPDATED";

      /// <summary>
      /// Constant for message topic
      /// </summary>
      public static readonly string ENTITY_DELETED = "ENTITY_DELETED";
    }
  }
}
