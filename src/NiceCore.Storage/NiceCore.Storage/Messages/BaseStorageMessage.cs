﻿using System;
using System.Collections.Generic;
using System.Linq;
using NiceCore.Entities;
using NiceCore.Eventing.Messages;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Diffing;

namespace NiceCore.Storage.Messages
{
  /// <summary>
  /// Base message impl for storage events
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <typeparam name="TId"></typeparam>
  public abstract class BaseStorageMessage<T, TId> : BaseMessage<IEntitiesMessageWrapper<TId>>, IStorageMessage<T, TId>
    where T : IDomainEntity<TId>
  {
    /// <summary>
    /// Adds i_Entity to the message
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <exception cref="InvalidOperationException"></exception>
    public void AddEntityToMessage(T i_Entity)
    {
      var val = GetActualValue();
      if (val.Success)
      {
        if (val.ResultValue is EntitiesMessageWrapper<T, TId> casted)
        {
          casted.Entities.Add(i_Entity);
        }
      }
      else
      {
        throw new InvalidOperationException($"Could not add entity of type {i_Entity.GetType()} with id {i_Entity.ID} to message: messageValue was set incorrectly");  
      }
    }

    /// <summary>
    /// Returns the entities of this message
    /// </summary>
    /// <returns></returns>
    public IReadOnlyCollection<T> GetEntitiesFromMessage()
    {
      var val = GetActualValue();
      if (val.Success)
      {
        if (val.ResultValue is EntitiesMessageWrapper<T, TId> casted)
        {
          return casted.Entities.ToList();
        }

        throw new NotSupportedException($"The value of a {nameof(BaseStorageMessage<T, TId>)} must be of type {nameof(EntitiesMessageWrapper<T,TId>)}. Other values are currently not supported.");
      }
      return new List<T>();
    }
    
    /// <summary>
    /// Returns the entities of this message
    /// </summary>
    /// <returns></returns>
    public Dictionary<TId, StorageDiffResult> GetDiffResultFromMessage()
    {
      var val = GetActualValue();
      if (val.Success)
      {
        return val.ResultValue.DiffResult;
      }
      return new();
    }

    /// <summary>
    /// Returns the context from the message
    /// </summary>
    /// <returns></returns>
    public IConversationContext GetContextFromMessage()
    {
      var val = GetActualValue();
      if (val.Success)
      {
        return val.ResultValue.ConversationContext;
      }
      return null;
    }
  }
}
