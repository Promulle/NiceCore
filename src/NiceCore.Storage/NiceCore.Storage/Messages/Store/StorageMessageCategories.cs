namespace NiceCore.Storage.Messages.Store
{
  /// <summary>
  /// Category a message will be put into
  /// </summary>
  public enum StorageMessageCategories
  {
    /// <summary>
    /// Create
    /// </summary>
    Create = 0,
    
    /// <summary>
    /// Update
    /// </summary>
    Update = 1,
    
    /// <summary>
    /// Delete
    /// </summary>
    Delete = 2,
  }
}