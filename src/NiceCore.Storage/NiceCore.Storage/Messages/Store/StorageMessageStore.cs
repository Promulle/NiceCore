using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using NiceCore.Diagnostics;
using NiceCore.Entities;
using NiceCore.Eventing.Messages;
using NiceCore.Logging.Extensions;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Messages.Providers;

namespace NiceCore.Storage.Messages.Store
{
  /// <summary>
  /// Store that keeps messages to be sent by the storage message sender and sorts entities into the correct 
  /// </summary>
  internal class StorageMessageStore
  {
    private readonly object m_Lock = new();
    private readonly ILogger m_Logger;
    private readonly IConversationContext m_Context;
    private readonly Dictionary<StorageMessageCategories, Dictionary<Type, List<IObjectMessage>>> m_Messages = new()
    {
      {StorageMessageCategories.Create, new()},
      {StorageMessageCategories.Delete, new()},
      {StorageMessageCategories.Update, new()}
    };
    private readonly Dictionary<StorageMessageCategories, List<IStorageMessageProvider>> m_MessageProviders = new();

    public StorageMessageStore(IConversationContext i_Context, IEnumerable<IStorageMessageProvider> i_MessageProviders, ILogger i_Logger)
    {
      m_Logger = i_Logger ?? DiagnosticsCollector.Instance.GetLogger<StorageMessageStore>();
      m_Context = i_Context;
      foreach (var messageProvider in i_MessageProviders)
      {
        m_Logger.Information($"Adding message provider of type {messageProvider.GetType().FullName}");
        messageProvider.Categories.ForEach(messageProviderCategory =>
        {
          m_Logger.Information($"Adding message provider of type {messageProvider.GetType().FullName} for Category {messageProviderCategory}");
          if (!m_MessageProviders.TryGetValue(messageProviderCategory, out var list))
          {
            list = new ();
            m_MessageProviders[messageProviderCategory] = list;
          }
          list.Add(messageProvider);
        });
      }
    }

    internal void EntityCreated<T, TId>(T i_Entity, MessageProviderCreateMessageParameters i_Parameters) where T : IDomainEntity<TId>
    {
      AddEntity<T, TId>(i_Entity, i_Parameters, StorageMessageCategories.Create);
    }

    internal void EntityUpdated<T, TId>(T i_Entity, MessageProviderCreateMessageParameters i_Parameters) where T : IDomainEntity<TId>
    {
      AddEntity<T, TId>(i_Entity, i_Parameters, StorageMessageCategories.Update);
    }

    internal void EntityDeleted<T, TId>(T i_Entity, MessageProviderCreateMessageParameters i_Parameters) where T : IDomainEntity<TId>
    {
      AddEntity<T, TId>(i_Entity, i_Parameters, StorageMessageCategories.Delete);
    }

    internal void Clear()
    {
      lock (m_Lock)
      {
        if (m_Messages.TryGetValue(StorageMessageCategories.Create, out var createByType))
        {
          createByType.Clear();
        }
        if (m_Messages.TryGetValue(StorageMessageCategories.Update, out var updateByType))
        {
          updateByType.Clear();
        }
        if (m_Messages.TryGetValue(StorageMessageCategories.Delete, out var deleteByType))
        {
          deleteByType.Clear();
        }
      }
    }

    internal IList<IObjectMessage> GetAllMessages()
    {
      lock (m_Lock)
      {
       return m_Messages.Values
          .SelectMany(byCat => byCat.Values)
          .SelectMany(r => r)
          .ToList();
      }
    }

    private void AddEntity<T, TId>(T i_Entity, MessageProviderCreateMessageParameters i_Parameters, StorageMessageCategories i_Category) where T : IDomainEntity<TId>
    {
      lock (m_Lock)
      {
        AssignEntityIntoCorrectMessageForEntityType<T, TId>(i_Entity, i_Parameters, i_Category);
      }
    }
    
    private List<IObjectMessage> GetCorrectCollection<T>(StorageMessageCategories i_Category)
    {
      m_Logger.Trace($"Determining collection to sort value with category {i_Category} into.");
      //the by category dicts should always be filled
      if (m_Messages.TryGetValue(i_Category, out var byTypeDict))
      {
        //create the collection if needed
        if (!byTypeDict.TryGetValue(typeof(T), out var messageCollection))
        {
          m_Logger.Trace($"Creating collection for message of type {typeof(T).FullName} for category {i_Category}");
          messageCollection = new();
          byTypeDict[typeof(T)] = messageCollection;
        }

        return messageCollection;
      }
      m_Logger.Warning($"No dictionary defined for category {i_Category}. This should not happen. If it does, check that {i_Category} is defined in the static initializer for {nameof(m_Messages)}.");
      return null;
    }

    private void AssignEntityIntoCorrectMessageForEntityType<T, TId>(T i_Entity, MessageProviderCreateMessageParameters i_Parameters, StorageMessageCategories i_Category) where T : IDomainEntity<TId>
    {
      var messagesForCategory = GetCorrectCollection<T>(i_Category);
      if (m_MessageProviders.TryGetValue(i_Category, out var providers))
      {
        m_Logger.Trace($"Message providers found for category {i_Category}");
        foreach (var messageProvider in providers)
        {
          if (messageProvider.ProviderIsApplicable<T, TId>(i_Entity))
          {
            m_Logger.Trace($"Can use message provider of type {messageProvider.GetType().FullName} for entity of type {typeof(T).FullName}");
            if (!messageProvider.ContainsMessage<T, TId>(messagesForCategory))
            {
              m_Logger.Trace($"Creating new message for type {typeof(T)} with entity {i_Entity.ID} as first item.");
              messagesForCategory.Add(messageProvider.CreateNewMessage<T, TId>(m_Context, i_Parameters, i_Entity));
            }
            else
            {
              m_Logger.Trace($"Sorting entity {i_Entity.ID} into message for type {typeof(T)}.");
              messageProvider.SortEntityIntoCorrectMessages<T, TId>(i_Entity, i_Parameters, messagesForCategory);
            }
          }
        }
      }
      else
      {
        m_Logger.Warning($"Could not assign entity {i_Entity.ID} of type {typeof(T).FullName} to correct collection, no message provider defined for {i_Category}");
      }
    }
  }
}