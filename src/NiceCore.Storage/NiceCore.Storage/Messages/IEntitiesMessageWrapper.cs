using System;
using System.Collections.Generic;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Diffing;

namespace NiceCore.Storage.Messages
{
  /// <summary>
  /// Entities Message Wrapper Interface
  /// </summary>
  /// <typeparam name="TId"></typeparam>
  public interface IEntitiesMessageWrapper<TId>
  {
    /// <summary>
    /// The time the message was created as value from <see cref="Environment.TickCount64"/>
    /// </summary>
    long TimeAsTicks { get; }

    /// <summary>
    /// Type Name of entities. This is the minimal qualified assembly name
    /// </summary>
    string EntityTypeName { get; }

    /// <summary>
    /// DiffResult
    /// </summary>
    Dictionary<TId, StorageDiffResult> DiffResult { get; }

    /// <summary>
    /// Conversation Context
    /// </summary>
    IConversationContext ConversationContext { get; }

    /// <summary>
    /// Get EntityList not generic
    /// </summary>
    /// <returns></returns>
    object GetEntityListNotGeneric();
  }
}