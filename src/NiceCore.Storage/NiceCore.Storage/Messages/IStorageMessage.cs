using System.Collections.Generic;
using NiceCore.Eventing.Messages;

namespace NiceCore.Storage.Messages
{
  /// <summary>
  /// IStorage MEssage
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <typeparam name="TId"></typeparam>
  public interface IStorageMessage<out T, TId> : IMessage<IEntitiesMessageWrapper<TId>>
  {
    /// <summary>
    /// Returns the entities of this message
    /// </summary>
    /// <returns></returns>
    IReadOnlyCollection<T> GetEntitiesFromMessage();
  }
}