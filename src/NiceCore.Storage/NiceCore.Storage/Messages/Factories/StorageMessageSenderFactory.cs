using NiceCore.Eventing.Messaging.Parameters;
using NiceCore.ServiceLocation;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Messages.Providers;

namespace NiceCore.Storage.Messages.Factories
{
  /// <summary>
  /// Factory that creates ready to use StorageMessageSender Instances
  /// </summary>
  [Register(InterfaceType = typeof(IStorageMessageSenderFactory))]
  public sealed class StorageMessageSenderFactory : IStorageMessageSenderFactory
  {
    /// <summary>
    /// Container
    /// </summary>
    public IServiceContainer Container { get; init; }
    
    /// <summary>
    /// Create a new instance of IStorage Message sender
    /// </summary>
    /// <param name="i_Context"></param>
    /// <param name="i_MessageSendingParameters"></param>
    /// <returns></returns>
    public IStorageMessageSender CreateNew(IConversationContext i_Context, IMessageSendingParameters i_MessageSendingParameters)
    {
      var inst = Container.Resolve<IStorageMessageSender>();
      inst.SetContext(i_Context, i_MessageSendingParameters, Container.ResolveAll<IStorageMessageProvider>());
      return inst;
    }

    /// <summary>
    /// Create New
    /// </summary>
    /// <param name="i_Context"></param>
    /// <returns></returns>
    public IStorageMessageSender CreateNew(IConversationContext i_Context)
    {
      return CreateNew(i_Context, null);
    }

    /// <summary>
    /// Create New
    /// </summary>
    /// <returns></returns>
    public IStorageMessageSender CreateNew()
    {
      return CreateNew(null, null);
    }
  }
}