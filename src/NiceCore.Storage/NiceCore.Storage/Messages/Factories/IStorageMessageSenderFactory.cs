using NiceCore.Eventing.Messaging.Parameters;
using NiceCore.Storage.Conversations;

namespace NiceCore.Storage.Messages.Factories
{
  /// <summary>
  /// Factory that creates ready to use instance of IStorageMessageSender
  /// </summary>
  public interface IStorageMessageSenderFactory
  {
    /// <summary>
    /// Create a new instance of IStorage Message sender
    /// </summary>
    /// <param name="i_Context"></param>
    /// <param name="i_MessageContext"></param>
    /// <returns></returns>
    IStorageMessageSender CreateNew(IConversationContext i_Context, IMessageSendingParameters i_MessageContext);
    
    /// <summary>
    /// Create a new instance of IStorage Message sender
    /// </summary>
    /// <param name="i_Context"></param>
    /// <returns></returns>
    IStorageMessageSender CreateNew(IConversationContext i_Context);
    
    /// <summary>
    /// Create a new instance of IStorage Message sender
    /// </summary>
    /// <returns></returns>
    IStorageMessageSender CreateNew();
  }
}