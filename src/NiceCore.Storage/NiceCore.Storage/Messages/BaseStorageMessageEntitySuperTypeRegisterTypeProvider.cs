using System;
using System.Collections.Generic;
using System.Reflection;
using NiceCore.Eventing.Messaging.Receiver;
using NiceCore.Reflection;

namespace NiceCore.Storage.Messages
{
  /// <summary>
  /// Storage Message Entity SuperType Register Type Provider
  /// </summary>
  public abstract class BaseStorageMessageEntitySuperTypeRegisterTypeProvider : IReceiverRegistrationTypeProvider
  {
    /// <summary>
    /// Get Types to register receiver
    /// </summary>
    /// <param name="i_Descriptor"></param>
    /// <returns></returns>
    public IReadOnlyCollection<Type> GetTypesToRegisterReceiver(MessageInvocationReceiverDescriptor i_Descriptor)
    {
      var registerType = i_Descriptor.RegisterMessageType;
      if (registerType.GenericTypeArguments.Length == 0)
      {
        return Array.Empty<Type>();
      }
      
      var baseType = GetMessageBaseType(i_Descriptor.RegisterMessageType);
      var allTypes = new List<Type>();
      foreach (var entityType in GetAllApplicableTypes(i_Descriptor.RegisterMessageType))
      {
        var genericMessageType = baseType.MakeGenericType(entityType, typeof(Guid));
        allTypes.Add(genericMessageType);
      }

      return allTypes;
    }

    private static Type GetMessageBaseType(Type i_RegisterType)
    {
      var isCreated = ReflectionUtils.OpenGenericTypeIsBaseFor(typeof(IEntitiesCreatedStorageMessage<,>), i_RegisterType);
      if (isCreated.Success)
      {
        return typeof(EntitiesCreatedStorageMessage<,>);
      }
      var isUpdated = ReflectionUtils.OpenGenericTypeIsBaseFor(typeof(IEntitiesUpdatedStorageMessage<,>), i_RegisterType);
      if (isUpdated.Success)
      {
        return typeof(EntitiesUpdatedStorageMessage<,>);
      }
      var isDeleted = ReflectionUtils.OpenGenericTypeIsBaseFor(typeof(IEntitiesDeletedStorageMessage<,>), i_RegisterType);
      if (isDeleted.Success)
      {
        return typeof(EntitiesDeletedStorageMessage<,>);
      }

      throw new InvalidOperationException($"MessageType for {i_RegisterType} does not fit any base-type");
    }

    /// <summary>
    /// Can Use Assembly check
    /// </summary>
    /// <param name="i_Assembly"></param>
    /// <returns></returns>
    protected abstract bool CanUseAssembly(Assembly i_Assembly);

    private HashSet<Type> GetAllApplicableTypes(Type i_RegisterType)
    {
      var registerEntityType = i_RegisterType.GenericTypeArguments[0];
      var res = new HashSet<Type>()
      {
        registerEntityType
      };
     
      foreach (var loadedAssembly in AppDomain.CurrentDomain.GetAssemblies())
      {
        if (CanUseAssembly(loadedAssembly))
        {
          foreach (var exportedType in loadedAssembly.GetExportedTypes())
          {
            if (exportedType.IsAssignableTo(registerEntityType))
            {
              res.Add(exportedType);
            }
          }  
        }
      }

      return res;
    }
  }
}