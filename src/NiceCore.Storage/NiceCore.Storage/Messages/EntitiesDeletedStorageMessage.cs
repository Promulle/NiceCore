﻿using System;
using System.Collections.Generic;
using NiceCore.Entities;
using NiceCore.Eventing.Messages;
using NiceCore.Reflection;
using NiceCore.Storage.Conversations;

namespace NiceCore.Storage.Messages
{
  /// <summary>
  /// Storage message for deletion events
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <typeparam name="TId"></typeparam>
  public class EntitiesDeletedStorageMessage<T, TId> : BaseStorageMessage<T, TId>, IEntitiesDeletedStorageMessage<T, TId>
    where T : IDomainEntity<TId>
  {
    /// <summary>
    /// Deleted message topic
    /// </summary>
    public override string MessageTopic { get; } = StorageMessages.Topics.ENTITY_DELETED + "_" + typeof(T).Name;

    /// <summary>
    /// Creates a new message
    /// </summary>
    /// <param name="i_Entities"></param>
    /// <param name="i_Context"></param>
    /// <returns></returns>
    public static EntitiesDeletedStorageMessage<T, TId> Create(IList<T> i_Entities, IConversationContext i_Context)
    {
      return new EntitiesDeletedStorageMessage<T, TId>()
      {
        Value = new MessageValue<IEntitiesMessageWrapper<TId>>()
        {
          Value = new EntitiesMessageWrapper<T, TId>()
          {
            TimeAsTicks = Environment.TickCount64,
            EntityTypeName = ReflectionUtils.GetMinimalAssemblyQualifiedName(typeof(T)),
            ConversationContext = i_Context,
            Entities = i_Entities
          }
        }
      };
    }
  }
}
