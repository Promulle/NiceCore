namespace NiceCore.Storage.Messages
{
  /// <summary>
  /// Entities Deleted Storage Message
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <typeparam name="TId"></typeparam>
  public interface IEntitiesDeletedStorageMessage<out T, TId> : IStorageMessage<T, TId>
  {
    
  }
}