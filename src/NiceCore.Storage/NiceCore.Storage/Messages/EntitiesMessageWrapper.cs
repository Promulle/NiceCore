﻿using System;
using System.Collections.Generic;
using NiceCore.Entities;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Diffing;

namespace NiceCore.Storage.Messages
{
  /// <summary>
  /// Wrapper class used as values for storage messages containing the entity and possibly more info
  /// </summary>
  public class EntitiesMessageWrapper<T, TId> : IEntitiesMessageWrapper<TId>
    where T : IDomainEntity<TId>
  {
    /// <summary>
    /// Entity
    /// </summary>
    public IList<T> Entities { get; init; }
    
    /// <summary>
    /// The time the message was created as value from <see cref="Environment.TickCount64"/>
    /// </summary>
    public required long TimeAsTicks { get; init; }
    
    /// <summary>
    /// Type Name of entities. This is the minimal qualified assembly name
    /// </summary>
    public string EntityTypeName { get; init; }

    /// <summary>
    /// DiffResult
    /// </summary>
    public Dictionary<TId, StorageDiffResult> DiffResult { get; init; } = new();

    /// <summary>
    /// Conversation Context
    /// </summary>
    public IConversationContext ConversationContext { get; init; }

    /// <summary>
    /// Get Entity List NotGeneric
    /// </summary>
    /// <returns></returns>
    public object GetEntityListNotGeneric()
    {
      return Entities;
    }
  }
}
