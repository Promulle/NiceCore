using NiceCore.Storage.Diffing;

namespace NiceCore.Storage.Messages.Providers
{
  /// <summary>
  /// Message Provider Create Message Parameters
  /// </summary>
  public sealed class MessageProviderCreateMessageParameters
  {
    /// <summary>
    /// Diff Result
    /// </summary>
    public StorageDiffResult DiffResult { get; init; }
  }
}