using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using NiceCore.Diagnostics;
using NiceCore.Entities;
using NiceCore.Eventing.Messages;
using NiceCore.ServiceLocation;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Messages.Store;

namespace NiceCore.Storage.Messages.Providers.Default
{
  /// <summary>
  /// Delete Message provider
  /// </summary>
  [Register(InterfaceType = typeof(IStorageMessageProvider))]
  public class DeleteMessageProvider: BaseDefaultStorageMessageProvider
  {
    /// <summary>
    /// Actual Logger
    /// </summary>
    public ILogger<DeleteMessageProvider> ActualLogger { get; set; } = DiagnosticsCollector.Instance.GetLogger<DeleteMessageProvider>();
    
    /// <summary>
    /// Logger override
    /// </summary>
    public override ILogger Logger => ActualLogger;

    /// <summary>
    /// Categories
    /// </summary>
    public override List<StorageMessageCategories> Categories { get; } = new()
    {
      StorageMessageCategories.Delete
    };
    
    /// <summary>
    /// Create new Message
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    public override IObjectMessage CreateNewMessage<T, TId>(IConversationContext i_Context, MessageProviderCreateMessageParameters i_ProvidedParameters, T i_Entity)
    {
      return EntitiesDeletedStorageMessage<T, TId>.Create(new List<T>()
      {
        i_Entity
      }, i_Context);
    }

    /// <summary>
    /// Checks if i_Messages contains the message
    /// </summary>
    /// <param name="i_Messages"></param>
    /// <returns></returns>
    public override bool ContainsMessage<T, TId>(List<IObjectMessage> i_Messages)
    {
      return i_Messages.OfType<EntitiesDeletedStorageMessage<T, TId>>().Any();
    }
  }
}