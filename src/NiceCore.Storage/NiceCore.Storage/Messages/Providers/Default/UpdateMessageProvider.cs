using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using NiceCore.Diagnostics;
using NiceCore.Eventing.Messages;
using NiceCore.ServiceLocation;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Diffing;
using NiceCore.Storage.Messages.Store;

namespace NiceCore.Storage.Messages.Providers.Default
{
  /// <summary>
  /// Updated Message Provider
  /// </summary>
  [Register(InterfaceType = typeof(IStorageMessageProvider))]
  public class UpdateMessageProvider : BaseDefaultStorageMessageProvider
  {
    /// <summary>
    /// Actual Logger
    /// </summary>
    public ILogger<UpdateMessageProvider> ActualLogger { get; set; } = DiagnosticsCollector.Instance.GetLogger<UpdateMessageProvider>();

    /// <summary>
    /// Logger
    /// </summary>
    public override ILogger Logger => ActualLogger;

    /// <summary>
    /// Categories
    /// </summary>
    public override List<StorageMessageCategories> Categories { get; } = new()
    {
      StorageMessageCategories.Update
    };

    /// <summary>
    /// Create new Message
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    public override IObjectMessage CreateNewMessage<T, TId>(IConversationContext i_Context, MessageProviderCreateMessageParameters i_ProvidedParameters, T i_Entity)
    {
      var diffResultDict = new Dictionary<TId, StorageDiffResult>
      {
        [i_Entity.ID] = i_ProvidedParameters.DiffResult
      };
      return EntitiesUpdatedStorageMessage<T, TId>.Create(new List<T>()
      {
        i_Entity
      }, diffResultDict, i_Context);
    }

    /// <summary>
    /// Checks if i_Messages contains the message
    /// </summary>
    /// <param name="i_Messages"></param>
    /// <returns></returns>
    public override bool ContainsMessage<T, TId>(List<IObjectMessage> i_Messages)
    {
      return i_Messages.OfType<EntitiesUpdatedStorageMessage<T, TId>>().Any();
    }
  }
}