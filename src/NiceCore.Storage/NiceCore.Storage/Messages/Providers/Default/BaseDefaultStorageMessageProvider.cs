using System.Collections.Generic;
using System.Linq;
using NiceCore.Eventing.Messages;
using NiceCore.Logging.Extensions;

namespace NiceCore.Storage.Messages.Providers.Default
{
  /// <summary>
  /// Base Default Storage Message Provider
  /// </summary>
  public abstract class BaseDefaultStorageMessageProvider : BaseStorageMessageProvider
  {
    /// <summary>
    /// Sort Entity into correct message
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_Parameters"></param>
    /// <param name="i_Messages"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    public override void SortEntityIntoCorrectMessages<T, TId>(T i_Entity, MessageProviderCreateMessageParameters i_Parameters, IEnumerable<IObjectMessage> i_Messages)
    {
      foreach (var message in i_Messages.OfType<BaseStorageMessage<T, TId>>())
      {
        Logger.Trace($"Adding entity {i_Entity.ID} of type {typeof(T).FullName} to message of type {message.GetType().FullName}");
        message.AddEntityToMessage(i_Entity);
        if (i_Parameters != null)
        {
          message.Value.Value.DiffResult[i_Entity.ID] = i_Parameters.DiffResult;  
        }
      }
    }
  }
}