using System.Collections.Generic;
using NiceCore.Entities;
using NiceCore.Eventing.Messages;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Messages.Store;

namespace NiceCore.Storage.Messages.Providers
{
  /// <summary>
  /// Interface for providers for custom messages
  /// </summary>
  public interface IStorageMessageProvider
  {
    /// <summary>
    /// Categories the provider should be used for
    /// </summary>
    List<StorageMessageCategories> Categories { get; }

    /// <summary>
    /// Create New Message by this custom message provider
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    IObjectMessage CreateNewMessage<T, TId>(IConversationContext i_Context, MessageProviderCreateMessageParameters i_ProvidedParameters, T i_Entity) where T: IDomainEntity<TId>;

    /// <summary>
    /// Sort the entity into the corrrect message
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_Parameters"></param>
    /// <param name="i_Messages"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    void SortEntityIntoCorrectMessages<T, TId>(T i_Entity, MessageProviderCreateMessageParameters i_Parameters, IEnumerable<IObjectMessage> i_Messages) where T : IDomainEntity<TId>;

    /// <summary>
    /// Contains Method
    /// </summary>
    /// <param name="i_Messages"></param>
    /// <returns></returns>
    bool ContainsMessage<T, TId>(List<IObjectMessage> i_Messages)  where T : IDomainEntity<TId>;

    /// <summary>
    /// Checks if this provider is applicable for an entity
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    bool ProviderIsApplicable<T, TId>(T i_Entity) where T : IDomainEntity<TId>;
  }
}