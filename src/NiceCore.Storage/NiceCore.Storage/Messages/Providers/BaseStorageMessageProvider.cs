using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using NiceCore.Entities;
using NiceCore.Eventing.Messages;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Messages.Store;

namespace NiceCore.Storage.Messages.Providers
{
  /// <summary>
  /// Base for storage Message Provider
  /// </summary>
  public abstract class BaseStorageMessageProvider : IStorageMessageProvider
  {
    /// <summary>
    /// Logger
    /// </summary>
    public abstract ILogger Logger { get; } 
    
    /// <summary>
    /// Categories
    /// </summary>
    public abstract List<StorageMessageCategories> Categories { get; }

    /// <summary>
    /// Create a new Message
    /// </summary>
    /// <param name="i_Context"></param>
    /// <param name="i_ProvidedParameters"></param>
    /// <param name="i_Entity"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    public abstract IObjectMessage CreateNewMessage<T, TId>(IConversationContext i_Context, MessageProviderCreateMessageParameters i_ProvidedParameters, T i_Entity) where T : IDomainEntity<TId>;

    /// <summary>
    /// Sort the entity into the corrrect message
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_Parameters"></param>
    /// <param name="i_Messages"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    public abstract void SortEntityIntoCorrectMessages<T, TId>(T i_Entity, MessageProviderCreateMessageParameters i_Parameters, IEnumerable<IObjectMessage> i_Messages) where T : IDomainEntity<TId>;

    /// <summary>
    /// Contains a message this provider has to create a new one for the given collection
    /// </summary>
    /// <param name="i_Messages"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    public abstract bool ContainsMessage<T, TId>(List<IObjectMessage> i_Messages) where T : IDomainEntity<TId>;

    /// <summary>
    /// Check if provider can be used
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    public virtual bool ProviderIsApplicable<T, TId>(T i_Entity) where T : IDomainEntity<TId>
    {
      return true;
    }
  }
}