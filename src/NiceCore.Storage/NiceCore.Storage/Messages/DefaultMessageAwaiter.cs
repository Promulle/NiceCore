using System.Collections.Generic;
using System.Threading.Tasks;

namespace NiceCore.Storage.Messages
{
  /// <summary>
  /// Default Message Awaiter
  /// </summary>
  public class DefaultMessageAwaiter : IMessageAwaiter
  {
    private readonly List<Task> m_Tasks = new(5);
    
    /// <summary>
    /// Add message task
    /// </summary>
    /// <param name="i_Task"></param>
    public void AddMessageTask(Task i_Task)
    {
      m_Tasks.Add(i_Task);
    }

    /// <summary>
    /// Await Message Completion
    /// </summary>
    /// <returns></returns>
    public async ValueTask AwaitMessageCompletion()
    {
      await Task.WhenAll(m_Tasks);
      m_Tasks.Clear();
    }
  }
}
