﻿using NiceCore.Entities;
using NiceCore.Storage.Fetching;
using NiceCore.Storage.Fetching.Impl;
using NiceCore.Storage.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using NiceCore.Storage.Conversations;
using Polly;

namespace NiceCore.Storage
{
  /// <summary>
  /// Extension for fetching
  /// </summary>
  public static class QueryExtensions
  {
    private const string MSG_FETCH_NOT_SUPPORTED = "FetchRequests are not supported with this implementation of storage. (The FetchProvider might not be set)";
    private const string MSG_ASYNC_NOT_SUPPORTED = "Async queries are not supported with this implementation of storage. (The AsyncQueryProvider might not be set)";
    /// <summary>
    /// Creates a fetch request for a query
    /// </summary>
    /// <typeparam name="TQueried"></typeparam>
    /// <typeparam name="TFetched"></typeparam>
    /// <param name="i_Query"></param>
    /// <param name="i_Selector"></param>
    /// <returns></returns>
    public static IFetchRequest<TQueried, TFetched> Fetch<TQueried, TFetched>(this IQueryable<TQueried> i_Query, Expression<Func<TQueried, TFetched>> i_Selector)
    {
      if (FetchRequests.AreSupported)
      {
        return FetchRequests.Provider.GetFetchRequest(i_Query, i_Selector);
      }
      throw new NotSupportedException(MSG_FETCH_NOT_SUPPORTED);
    }
    /// <summary>
    /// Creates a fetchmany request for a query
    /// </summary>
    /// <typeparam name="TQueried"></typeparam>
    /// <typeparam name="TFetched"></typeparam>
    /// <param name="i_Query"></param>
    /// <param name="i_Selector"></param>
    /// <returns></returns>
    public static IFetchRequest<TQueried, TFetched> FetchMany<TQueried, TFetched>(this IQueryable<TQueried> i_Query, Expression<Func<TQueried, IEnumerable<TFetched>>> i_Selector)
    {
      if (FetchRequests.AreSupported)
      {
        return FetchRequests.Provider.GetFetchManyRequest(i_Query, i_Selector);
      }
      throw new NotSupportedException(MSG_FETCH_NOT_SUPPORTED);
    }
    /// <summary>
    /// Creates a thenfetch request for a query
    /// </summary>
    /// <typeparam name="TQueried"></typeparam>
    /// <typeparam name="TAlreadyFetched"></typeparam>
    /// <typeparam name="TFetched"></typeparam>
    /// <param name="i_Query"></param>
    /// <param name="i_Selector"></param>
    /// <returns></returns>
    public static IFetchRequest<TQueried, TFetched> ThenFetch<TQueried, TAlreadyFetched, TFetched>(this IFetchRequest<TQueried, TAlreadyFetched> i_Query, Expression<Func<TAlreadyFetched, TFetched>> i_Selector)
    {
      if (FetchRequests.AreSupported)
      {
        return FetchRequests.Provider.GetThenFetchRequest(i_Query, i_Selector);
      }
      throw new NotSupportedException(MSG_FETCH_NOT_SUPPORTED);
    }
    /// <summary>
    /// Creates a thenfetchmany request for a query
    /// </summary>
    /// <typeparam name="TQueried"></typeparam>
    /// <typeparam name="TAlreadyFetched"></typeparam>
    /// <typeparam name="TFetched"></typeparam>
    /// <param name="i_Query"></param>
    /// <param name="i_Selector"></param>
    /// <returns></returns>
    public static IFetchRequest<TQueried, TFetched> ThenFetchMany<TQueried, TAlreadyFetched, TFetched>(this IFetchRequest<TQueried, TAlreadyFetched> i_Query, Expression<Func<TAlreadyFetched, IEnumerable<TFetched>>> i_Selector)
    {
      if (FetchRequests.AreSupported)
      {
        return FetchRequests.Provider.GetThenFetchManyRequest(i_Query, i_Selector);
      }
      throw new NotSupportedException(MSG_FETCH_NOT_SUPPORTED);
    }

    /// <summary>
    /// Applies the provided fetch strategy to the query. This eager fetches properties defined in the strategy
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Query"></param>
    /// <param name="i_Strategy"></param>
    /// <returns></returns>
    public static IQueryable<T> ApplyFetchStrategy<T>(this IQueryable<T> i_Query, IFetchStrategy i_Strategy)
    {
      return FetchStrategyTranslation.ApplyStrategy(i_Query, i_Strategy);
    }

    /// <summary>
    /// Executes the query async
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Query"></param>
    /// <returns></returns>
    public static Task<List<T>> ToListAsync<T, Tid>(this IQueryable<T> i_Query) where T : IDomainEntity<Tid>
    {
      return ToListAsync<T, Tid>(i_Query, null);
    }

    /// <summary>
    /// Executes the query async
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Query"></param>
    /// <param name="i_Policy"></param>
    /// <returns></returns>
    public static Task<List<T>> ToListAsync<T, Tid>(this IQueryable<T> i_Query, IAsyncPolicy i_Policy) where T : IDomainEntity<Tid>
    {
      if (AsyncQueries.AreSupported)
      {
        return AsyncQueries.AsyncQueryProvider.QueryToListAsync<T, Tid>(i_Query, i_Policy);
      }
      throw new NotSupportedException(MSG_ASYNC_NOT_SUPPORTED);
    }

    /// <summary>
    /// Executes the query async
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="t_Query"></param>
    /// <returns></returns>
    public static Task<T> FirstOrDefaultAsync<T, Tid>(this IQueryable<T> t_Query) where T : IDomainEntity<Tid>
    {
      return FirstOrDefaultAsync<T, Tid>(t_Query, (IAsyncPolicy)null);
    }
    
    /// <summary>
    /// Executes the query async
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="t_Query"></param>
    /// <param name="i_Policy"></param>
    /// <returns></returns>
    public static Task<T> FirstOrDefaultAsync<T, Tid>(this IQueryable<T> t_Query, IAsyncPolicy i_Policy) where T : IDomainEntity<Tid>
    {
      if (AsyncQueries.AreSupported)
      {
        return AsyncQueries.AsyncQueryProvider.QueryFirstOrDefaultAsync<T, Tid>(t_Query, i_Policy);
      }
      throw new NotSupportedException(MSG_ASYNC_NOT_SUPPORTED);
    }

    /// <summary>
    /// Executes the query async
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Query"></param>
    /// <returns></returns>
    public static Task<T> FirstOrDefaultAsync<T>(this IQueryable<T> t_Query)
    {
      return FirstOrDefaultAsync<T>(t_Query, null);
    }
    
    /// <summary>
    /// Executes the query async
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Query"></param>
    /// <param name="i_Policy"></param>
    /// <returns></returns>
    public static Task<T> FirstOrDefaultAsync<T>(this IQueryable<T> t_Query, IAsyncPolicy i_Policy)
    {
      if (AsyncQueries.AreSupported)
      {
        return AsyncQueries.AsyncQueryProvider.QueryFirstOrDefaultAsync<T>(t_Query, i_Policy);
      }
      throw new NotSupportedException(MSG_ASYNC_NOT_SUPPORTED);
    }

    /// <summary>
    /// Executes the query async
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="t_Query"></param>
    /// <param name="i_WhereExpression"></param>
    /// <returns></returns>
    public static Task<T> FirstOrDefaultAsync<T, Tid>(this IQueryable<T> t_Query, Expression<Func<T, bool>> i_WhereExpression) where T : IDomainEntity<Tid>
    {
      return FirstOrDefaultAsync<T, Tid>(t_Query, i_WhereExpression, null);
    }

    /// <summary>
    /// Executes the query async
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="t_Query"></param>
    /// <param name="i_WhereExpression"></param>
    /// <param name="i_Policy"></param>
    /// <returns></returns>
    public static Task<T> FirstOrDefaultAsync<T, Tid>(this IQueryable<T> t_Query, Expression<Func<T, bool>> i_WhereExpression, IAsyncPolicy i_Policy) where T : IDomainEntity<Tid>
    {
      if (AsyncQueries.AreSupported)
      {
        return AsyncQueries.AsyncQueryProvider.QueryFirstOrDefaultAsync<T, Tid>(t_Query, i_WhereExpression, i_Policy);
      }
      throw new NotSupportedException(MSG_ASYNC_NOT_SUPPORTED);
    }

    /// <summary>
    /// Executes the query async
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Query"></param>
    /// <returns></returns>
    public static Task<List<T>> ToListAsync<T>(this IQueryable<T> i_Query)
    {
      return ToListAsync<T>(i_Query, null);
    }
    
    /// <summary>
    /// Executes the query async
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Query"></param>
    /// <param name="i_Policy"></param>
    /// <returns></returns>
    public static Task<List<T>> ToListAsync<T>(this IQueryable<T> i_Query, IAsyncPolicy i_Policy)
    {
      if (AsyncQueries.AreSupported)
      {
        return AsyncQueries.AsyncQueryProvider.QueryToListAsync(i_Query, i_Policy);
      }
      throw new NotSupportedException(MSG_ASYNC_NOT_SUPPORTED);
    }

    /// <summary>
    /// Executes the query async
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Query"></param>
    /// <returns></returns>
    public static Task<int> CountAsync<T, Tid>(this IQueryable<T> i_Query) where T : IDomainEntity<Tid>
    {
      return CountAsync<T, Tid>(i_Query, (IAsyncPolicy)null);
    }
    
    /// <summary>
    /// Executes the query async
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Query"></param>
    /// <param name="i_Policy"></param>
    /// <returns></returns>
    public static Task<int> CountAsync<T, Tid>(this IQueryable<T> i_Query, IAsyncPolicy i_Policy) where T : IDomainEntity<Tid>
    {
      if (AsyncQueries.AreSupported)
      {
        return AsyncQueries.AsyncQueryProvider.CountAsync<T, Tid>(i_Query, i_Policy);
      }
      throw new NotSupportedException(MSG_ASYNC_NOT_SUPPORTED);
    }

    /// <summary>
    /// Execute the query but returns only if this query has atleast one matching element
    /// </summary>
    /// <param name="i_Query"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static Task<bool> AnyAsync<T>(this IQueryable<T> i_Query)
    {
      return AnyAsync<T>(i_Query, null);
    }
    
    /// <summary>
    /// Execute the query but returns only if this query has atleast one matching element
    /// </summary>
    /// <param name="i_Query"></param>
    /// <param name="i_Policy"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    /// <exception cref="NotSupportedException"></exception>
    public static Task<bool> AnyAsync<T>(this IQueryable<T> i_Query, IAsyncPolicy i_Policy)
    {
      if (AsyncQueries.AreSupported)
      {
        return AsyncQueries.AsyncQueryProvider.AnyAsync<T>(i_Query, i_Policy);
      }
      throw new NotSupportedException(MSG_ASYNC_NOT_SUPPORTED);
    }

    /// <summary>
    /// Executes the query async
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Query"></param>
    /// <param name="i_WhereExpression"></param>
    /// <returns></returns>
    public static Task<int> CountAsync<T, Tid>(this IQueryable<T> i_Query, Expression<Func<T, bool>> i_WhereExpression) where T : IDomainEntity<Tid>
    {
      return CountAsync<T, Tid>(i_Query, i_WhereExpression, null);
    }
    
    /// <summary>
    /// Executes the query async
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Query"></param>
    /// <param name="i_WhereExpression"></param>
    /// <param name="i_Policy"></param>
    /// <returns></returns>
    public static Task<int> CountAsync<T, Tid>(this IQueryable<T> i_Query, Expression<Func<T, bool>> i_WhereExpression, IAsyncPolicy i_Policy) where T : IDomainEntity<Tid>
    {
      if (AsyncQueries.AreSupported)
      {
        return AsyncQueries.AsyncQueryProvider.CountAsync<T, Tid>(i_Query, i_WhereExpression, i_Policy);
      }
      throw new NotSupportedException(MSG_ASYNC_NOT_SUPPORTED);
    }

    /// <summary>
    /// Executes the query async
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Query"></param>
    /// <returns></returns>
    public static Task<int> CountAsync<T>(this IQueryable<T> i_Query)
    {
      return CountAsync<T>(i_Query, (IAsyncPolicy)null);
    }
    
    /// <summary>
    /// Executes the query async
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Query"></param>
    /// <param name="i_Policy"></param>
    /// <returns></returns>
    public static Task<int> CountAsync<T>(this IQueryable<T> i_Query, IAsyncPolicy i_Policy)
    {
      if (AsyncQueries.AreSupported)
      {
        return AsyncQueries.AsyncQueryProvider.CountAsync(i_Query, i_Policy);
      }
      throw new NotSupportedException(MSG_ASYNC_NOT_SUPPORTED);
    }

    /// <summary>
    /// Executes the query async
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Query"></param>
    /// <param name="i_WhereExpression"></param>
    /// <returns></returns>
    public static Task<int> CountAsync<T>(this IQueryable<T> i_Query, Expression<Func<T, bool>> i_WhereExpression)
    {
      return CountAsync<T>(i_Query, i_WhereExpression, null);
    }
    
    /// <summary>
    /// Executes the query async
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Query"></param>
    /// <param name="i_WhereExpression"></param>
    /// <param name="i_Policy"></param>
    /// <returns></returns>
    public static Task<int> CountAsync<T>(this IQueryable<T> i_Query, Expression<Func<T, bool>> i_WhereExpression, IAsyncPolicy i_Policy)
    {
      if (AsyncQueries.AreSupported)
      {
        return AsyncQueries.AsyncQueryProvider.CountAsync(i_Query, i_WhereExpression, i_Policy);
      }
      throw new NotSupportedException(MSG_ASYNC_NOT_SUPPORTED);
    }

    /// <summary>
    /// Creates a .Join Without limiters, needed since limiters on Inner expression Queries usually 
    /// </summary>
    /// <param name="i_Query"></param>
    /// <param name="i_Conversation"></param>
    /// <param name="i_OuterKeySelector"></param>
    /// <param name="i_InnerKeySelector"></param>
    /// <param name="i_ResultSelector"></param>
    /// <typeparam name="TOuter"></typeparam>
    /// <typeparam name="TInner"></typeparam>
    /// <typeparam name="TInnerID"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    /// <returns></returns>
    public static IQueryable<TResult> LimiterLessJoin<TOuter, TInner, TInnerID, TKey, TResult>(this IQueryable<TOuter> i_Query,
      IReadonlyConversation i_Conversation,
      Expression<Func<TOuter, TKey>> i_OuterKeySelector,
      Expression<Func<TInner, TKey>> i_InnerKeySelector,
      Expression<Func<TOuter, TInner, TResult>> i_ResultSelector) where TInner: IDomainEntity<TInnerID>
    {
      return i_Query.Join(i_Conversation.Query<TInner, TInnerID>(false), i_OuterKeySelector, i_InnerKeySelector, i_ResultSelector);
    }
  }
}
