using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using NiceCore.Reflection;
using NiceCore.Storage.Fetching;

namespace NiceCore.Storage.Extensions.Expressions
{
  public static class QueryExtensionExpressionMethods
  {
    public static MethodInfo GetApplyFetchStrategyMethod(Type i_Type)
    {
      var types = new List<MethodParameterType>()
      {
        MethodParameterType.Of(typeof(IQueryable<>), new List<MethodParameterType>()
        {
          MethodParameterType.GenericPlaceHolder()
        }),
        MethodParameterType.Of(typeof(IFetchStrategy))
      };
      var methodInfo = ReflectionUtils.GetMethodEx(typeof(QueryExtensions), nameof(QueryExtensions.ApplyFetchStrategy), 2, 1, types);
      return methodInfo.MakeGenericMethod(i_Type);
    }
  }
}