using System;
using System.Linq.Expressions;

namespace NiceCore.Storage.Extensions.Expressions
{
  /// <summary>
  /// QueryExtensionExpressionUtils
  /// </summary>
  public static class QueryExtensionExpressionUtils
  {
    /// <summary>
    /// CallApplyFetchStrategyOnQueryable
    /// </summary>
    /// <param name="i_QueryableExpression"></param>
    /// <param name="i_FetchStrategyExpression"></param>
    /// <param name="i_EntityType"></param>
    /// <returns></returns>
    public static MethodCallExpression CallApplyFetchStrategyOnQueryable(Expression i_QueryableExpression,
      Expression i_FetchStrategyExpression,
      Type i_EntityType)
    {
      var method = QueryExtensionExpressionMethods.GetApplyFetchStrategyMethod(i_EntityType);
      return Expression.Call(null, method, i_QueryableExpression, i_FetchStrategyExpression);
    }
  }
}