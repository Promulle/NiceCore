﻿using System;
using System.Data;

namespace NiceCore.Storage.Extensions
{
  /// <summary>
  /// Class for extension methods for IDbCommand
  /// </summary>
  public static class DbCommandExtensions
  {
    /// <summary>
    /// Extension method, that sets a parameter or creates a new one, if a command with equal name is found 
    /// </summary>
    /// <typeparam name="T">Type of value</typeparam>
    /// <param name="i_Command">this</param>
    /// <param name="i_ParameterName">Name of parameter to set</param>
    /// <param name="i_Value">value to use for parameter, if type is Nullable of T use .Value</param>
    /// <param name="i_Type">type of parameter to use</param>
    public static void SetParameter<T>(this IDbCommand i_Command, string i_ParameterName, T i_Value, DbType i_Type = DbType.Object)
    {
      IDbDataParameter parameter = null;
      if (i_Command.Parameters.Contains(i_ParameterName))
      {
        parameter = i_Command.Parameters[i_ParameterName] as IDbDataParameter;
      }
      else
      {
        parameter = i_Command.CreateParameter();
      }
      if (parameter != null)
      {
        parameter.ParameterName = i_ParameterName;
        parameter.Value = i_Value;
        parameter.DbType = i_Type;
        i_Command.Parameters.Add(parameter);
      }
    }
    /// <summary>
    /// Extension method, that sets a parameter or creates a new one, if a command with equal name is found 
    /// </summary>
    /// <typeparam name="T">Type of value</typeparam>
    /// <param name="i_Command">this</param>
    /// <param name="i_ParameterName">Name of parameter to set</param>
    /// <param name="i_Value">value to use for parameter, if type is Nullable of T use .Value</param>
    /// <param name="i_UseTypeOfValue">Whether the type of i_Value should be used as DbType, if false, no DbType will be set</param>
    public static void SetParameter<T>(this IDbCommand i_Command, string i_ParameterName, T i_Value, bool i_UseTypeOfValue = false)
    {
      IDbDataParameter parameter = null;
      if (i_Command.Parameters.Contains(i_ParameterName))
      {
        parameter = i_Command.Parameters[i_ParameterName] as IDbDataParameter;
      }
      else
      {
        parameter = i_Command.CreateParameter();
      }
      if (parameter != null)
      {
        parameter.ParameterName = i_ParameterName;
        parameter.Value = i_Value;
        if (i_UseTypeOfValue)
        {
          parameter.DbType = ConvertToDbType(i_Value);
        }
        i_Command.Parameters.Add(parameter);
      }
    }
    private static DbType ConvertToDbType<T>(T i_Instance)
    {
      var typeCode = Type.GetTypeCode(typeof(T));
      if (typeCode == TypeCode.Int32)
      {
        return DbType.Int32;
      }
      else if (typeCode == TypeCode.Int64)
      {
        return DbType.Int64;
      }
      else if (typeCode == TypeCode.Int16)
      {
        return DbType.Int16;
      }
      else if (typeCode == TypeCode.String)
      {
        return DbType.String;
      }
      else if (typeCode == TypeCode.Boolean)
      {
        return DbType.Boolean;
      }
      else if (typeCode == TypeCode.Decimal)
      {
        return DbType.Decimal;
      }
      else if (typeCode == TypeCode.Double)
      {
        return DbType.Double;
      }
      else
      {
        return DbType.String;
      }
    }
  }
}
