﻿using System;
using System.Collections.Generic;

namespace NiceCore.Storage.DefaultImplementations
{
  /// <summary>
  /// Default impl for IRetryStatus
  /// </summary>
  public class DefaultRetryStatus : IRetryStatus
  {
    /// <summary>
    /// Count of current retries
    /// </summary>
    public int Retries { get; set; } = 0;

    /// <summary>
    /// All encountered Exceptions
    /// </summary>
    public List<Exception> EncounteredExceptions { get; set; } = new List<Exception>();

    /// <summary>
    /// Latest Exception
    /// </summary>
    public Exception LatestException { get; set; }
  }
}
