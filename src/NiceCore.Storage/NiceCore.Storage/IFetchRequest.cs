﻿using System.Linq;

namespace NiceCore.Storage
{
  /// <summary>
  /// Fetch request interface returned by query extension
  /// </summary>
  public interface IFetchRequest<TQueried, TFetched> : IQueryable<TQueried>
  {
  }
}
