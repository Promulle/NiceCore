﻿using System;

namespace NiceCore.Storage.Diffing
{
  /// <summary>
  /// Entry for a single property of a diff result
  /// </summary>
  public class StorageDiffResultEntry
  {
    /// <summary>
    /// PropertyName
    /// </summary>
    public string PropertyName { get; init; }

    /// <summary>
    /// Old Value
    /// </summary>
    public object OldValue { get; init; }

    /// <summary>
    /// New Value
    /// </summary>
    public object NewValue { get; init; }
  }
}
