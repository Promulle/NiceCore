﻿using System.Collections.Generic;

namespace NiceCore.Storage.Diffing
{
  /// <summary>
  /// Result of a diffing
  /// </summary>
  public sealed class StorageDiffResult
  {
    /// <summary>
    /// Entries that have actually changed
    /// </summary>
    public Dictionary<string, StorageDiffResultEntry> Entries { get; init; } = new();
  }
}
