﻿using System.Collections.Generic;

namespace NiceCore.Storage.Fetching
{
  /// <summary>
  /// Fetch strategy that can be used to fetch for a query
  /// </summary>
  public interface IFetchStrategy
  {
    /// <summary>
    /// Relations used for fetching. Each relation represents a Fetch/ThenFetch "Tree"
    /// </summary>
    List<string> Relations { get; }
  }
}
