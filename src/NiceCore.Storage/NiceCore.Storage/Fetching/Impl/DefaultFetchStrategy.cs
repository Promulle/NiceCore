﻿using System.Collections.Generic;

namespace NiceCore.Storage.Fetching.Impl
{
  /// <summary>
  /// Default impl of IFetchStrategy
  /// </summary>
  public class DefaultFetchStrategy : IFetchStrategy
  {
    /// <summary>
    /// Relations
    /// </summary>
    public List<string> Relations { get; set; }
  }
}
