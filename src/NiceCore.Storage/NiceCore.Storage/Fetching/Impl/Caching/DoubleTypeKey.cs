﻿using System;
using System.Collections.Generic;

namespace NiceCore.Storage.Fetching.Impl.Caching
{
  /// <summary>
  /// Key for two type values
  /// </summary>
  public class DoubleTypeKey
  {
    /// <summary>
    /// ObjectType
    /// </summary>
    public Type ObjectType { get; init; }

    /// <summary>
    /// PropertyType
    /// </summary>
    public Type PropertyType { get; init; }

    /// <summary>
    /// Equals
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj)
    {
      return obj is DoubleTypeKey key &&
             EqualityComparer<Type>.Default.Equals(ObjectType, key.ObjectType) &&
             EqualityComparer<Type>.Default.Equals(PropertyType, key.PropertyType);
    }

    /// <summary>
    /// GetHashcode
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode()
    {
      return HashCode.Combine(ObjectType, PropertyType);
    }
  }
}
