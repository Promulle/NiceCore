﻿using System;
using System.Collections.Generic;

namespace NiceCore.Storage.Fetching.Impl.Caching
{
  /// <summary>
  /// Class that is a key for type and property name of that type
  /// </summary>
  public class TypeAndPropertyNameKey
  {
    /// <summary>
    /// Type
    /// </summary>
    public Type Type { get; init; }

    /// <summary>
    /// PropertyName
    /// </summary>
    public string PropertyName { get; init; }

    /// <summary>
    /// Equals
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj)
    {
      return obj is TypeAndPropertyNameKey key &&
             EqualityComparer<Type>.Default.Equals(Type, key.Type) &&
             PropertyName == key.PropertyName;
    }

    /// <summary>
    /// GetHashCode
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode()
    {
      return HashCode.Combine(Type, PropertyName);
    }
  }
}
