﻿using System;
using System.Collections.Generic;

namespace NiceCore.Storage.Fetching.Impl.Caching
{
  /// <summary>
  /// key for cache that holds apply funcs for queries
  /// </summary>
  public class QueryApplyFuncKey
  {
    /// <summary>
    /// Query Type
    /// </summary>
    public Type QueryType { get; init; }

    /// <summary>
    /// ObjectType
    /// </summary>
    public Type ObjectType { get; init; }

    /// <summary>
    /// PropertyName
    /// </summary>
    public string PropertyName { get; init; }

    /// <summary>
    /// Name of fetch function
    /// </summary>
    public string FunctionName { get; init; }

    /// <summary>
    /// Equals
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj)
    {
      return obj is QueryApplyFuncKey key &&
             EqualityComparer<Type>.Default.Equals(QueryType, key.QueryType) &&
             EqualityComparer<Type>.Default.Equals(ObjectType, key.ObjectType) &&
             PropertyName == key.PropertyName &&
             FunctionName == key.FunctionName;
    }

    /// <summary>
    /// GetHashCode
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode()
    {
      return HashCode.Combine(QueryType, ObjectType, PropertyName, FunctionName);
    }
  }
}
