﻿using NiceCore.Base.Caching;
using NiceCore.Reflection;
using NiceCore.Storage.Fetching.Impl.Caching;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace NiceCore.Storage.Fetching.Impl
{
  /// <summary>
  /// Class that translates and applies a fetchstrategy
  /// </summary>
  public static class FetchStrategyTranslation
  {
    /// <summary>
    /// Cache that holds the types of properties of a certain name for a certain type
    /// </summary>
    private static readonly SimpleCache<TypeAndPropertyNameKey, Type> m_PropertyTypeCache = new();

    /// <summary>
    /// Cache that caches the lambda creation method for Object and property type
    /// </summary>
    private static readonly SimpleCache<DoubleTypeKey, MethodInfo> m_LambdaCreationMethodCache = new();

    /// <summary>
    /// Caches the apply func for a specific type of query
    /// </summary>
    private static readonly SimpleCache<QueryApplyFuncKey, Func<object, object>> m_QueryTypeToApplyFuncCache = new();

    /// <summary>
    /// Applies i_Strategy to i_Query
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Query">Query that is changed by i_Strategy</param>
    /// <param name="i_Strategy"></param>
    /// <returns></returns>
    public static IQueryable<T> ApplyStrategy<T>(IQueryable<T> t_Query, IFetchStrategy i_Strategy)
    {
      if (i_Strategy == null)
      {
        return t_Query;
      }
      var qry = t_Query;
      foreach (var relation in i_Strategy.Relations)
      {
        qry = ApplyRelation(qry, relation);
      }
      return qry;
    }

    private static IQueryable<T> ApplyRelation<T>(IQueryable<T> t_Query, string i_Relation)
    {
      if (string.IsNullOrEmpty(i_Relation) || string.IsNullOrWhiteSpace(i_Relation))
      {
        return t_Query;
      }
      var queryObjType = typeof(T);
      var steps = i_Relation.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
      var stepCount = steps.Length;
      if (stepCount == 1)
      {
        return ApplyNormalStep(t_Query, steps[0]).request as IQueryable<T>;
      }
      var (request, fetchedType) = ApplyNormalStep(t_Query, steps[0]);
      for (var i = 1; i < stepCount; i++)
      {
        var (newRequest, nextFetchedType) = ApplyThenStep(request, fetchedType, queryObjType, steps[i]);
        request = newRequest;
        fetchedType = nextFetchedType;
      }
      return request as IQueryable<T>;
    }

    /// <summary>
    /// Returns a INhFetchRequest of objectType, propertyType
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Query"></param>
    /// <param name="i_PropertyName"></param>
    /// <returns></returns>
    private static (object request, Type propType) ApplyNormalStep<T>(IQueryable<T> t_Query, string i_PropertyName)
    {
      var objType = typeof(T);
      var propType = GetPropertyTypeFor(objType, i_PropertyName);
      var isCollection = ReflectionUtils.IsGenericCollectionType(propType);
      var methodName = isCollection ? "FetchMany" : "Fetch";
      return (ApplyStep(t_Query, objType, propType, meth => TransformFetchMethod(meth, objType, propType), i_PropertyName, methodName), propType);
    }

    /// <summary>
    /// Returns a INhFetchRequest of objectType, propertyType
    /// </summary>
    /// <param name="i_FetchRequest"></param>
    /// <param name="i_ObjectType"></param>
    /// <param name="i_QueryObjType"></param>
    /// <param name="i_PropertyName"></param>
    /// <returns></returns>
    private static (object request, Type propType) ApplyThenStep(object i_FetchRequest, Type i_ObjectType, Type i_QueryObjType, string i_PropertyName)
    {
      var objType = GetCorrectObjectTypeForThenStep(i_ObjectType);
      var propType = GetPropertyTypeFor(objType, i_PropertyName);
      var isCollection = ReflectionUtils.IsGenericCollectionType(propType);
      var methodName = isCollection ? "ThenFetchMany" : "ThenFetch";
      return (ApplyStep(i_FetchRequest, objType, propType, meth => TransformFetchMethodThenStep(meth, i_QueryObjType, objType, propType), i_PropertyName, methodName), propType);
    }

    private static MethodInfo TransformFetchMethod(MethodInfo i_Method, Type i_ObjectType, Type i_PropertyType)
    {
      var propType = i_PropertyType;
      if (ReflectionUtils.IsGenericCollectionType(i_PropertyType))
      {
        propType = i_PropertyType.GetGenericArguments()[0];
      }
      return i_Method.MakeGenericMethod(i_ObjectType, propType);
    }

    private static MethodInfo TransformFetchMethodThenStep(MethodInfo i_Method, Type i_QueryType, Type i_ObjectType, Type i_PropertyType)
    {
      var propType = i_PropertyType;
      if (ReflectionUtils.IsGenericCollectionType(i_PropertyType))
      {
        propType = i_PropertyType.GetGenericArguments()[0];
      }
      return i_Method.MakeGenericMethod(i_QueryType, i_ObjectType, propType);
    }

    /// <summary>
    /// Used when creating functions for thenSteps, needed to traverse collections.
    /// Checks whether i_ObjectType is a generic collection (IEnumerable of T is assignable from i_ObjectType) and takes first parameter as actual type to use
    /// Should enable following fetch relation:
    /// Example: MyInstance.CollectionProperty.SomePropertyThatExistsOnEachInstanceInCollectionProperty
    /// </summary>
    /// <param name="i_ObjectType"></param>
    /// <returns></returns>
    private static Type GetCorrectObjectTypeForThenStep(Type i_ObjectType)
    {
      if (ReflectionUtils.IsGenericCollectionType(i_ObjectType))
      {
        return i_ObjectType.GetGenericArguments()[0];
      }
      return i_ObjectType;
    }

    private static object ApplyStep(object t_Query, Type i_ObjectType, Type i_PropertyType, Func<MethodInfo, MethodInfo> i_MakeGenericFunc, string i_PropertyName, string i_FetchMethodName)
    {
      var key = new QueryApplyFuncKey()
      {
        FunctionName = i_FetchMethodName,
        PropertyName = i_PropertyName,
        ObjectType = i_ObjectType,
        QueryType = t_Query.GetType()
      };
      var applyFunc = m_QueryTypeToApplyFuncCache.GetOrCreate(key, () => GetApplyFunc(t_Query.GetType(), i_ObjectType, i_PropertyType, i_MakeGenericFunc, i_PropertyName, i_FetchMethodName));
      return applyFunc.Invoke(t_Query);
    }

    private static Func<object, object> GetApplyFunc(Type i_QueryType, Type i_ObjectType, Type i_PropertyType, Func<MethodInfo, MethodInfo> i_MakeGenericFunc, string i_PropertyName, string i_FetchMethodName)
    {
      var paramExpr = Expression.Parameter(i_ObjectType);
      var propExpr = GetPropertyExpressionFor(paramExpr, i_PropertyName);
      var lambdaMethod = GetLambdaCreationMethod(i_ObjectType, i_PropertyType);
      var paramsAr = new object[] { propExpr, new ParameterExpression[] { paramExpr } };
      var lambdaExprCasted = lambdaMethod.Invoke(null, paramsAr);

      var method = i_MakeGenericFunc.Invoke(typeof(QueryExtensions).GetMethod(i_FetchMethodName));
      var queryParamExpr = Expression.Parameter(typeof(object));
      var queryParamCasted = Expression.Convert(queryParamExpr, i_QueryType);
      var invokeExpr = Expression.Call(method, queryParamCasted, Expression.Constant(lambdaExprCasted));
      var queryApplyLambdaExpr = Expression.Lambda<Func<object, object>>(invokeExpr, queryParamExpr);
      return queryApplyLambdaExpr.Compile();
    }

    private static MemberExpression GetPropertyExpressionFor(ParameterExpression i_ParameterExpression, string i_PropertyPath)
    {
      var propertiesInPath = i_PropertyPath.Split(new string[] { "->" }, StringSplitOptions.RemoveEmptyEntries);
      var initialPropertyExpression = Expression.Property(i_ParameterExpression, propertiesInPath[0]);
      if (propertiesInPath.Length == 1)
      {
        return initialPropertyExpression;
      }
      var propertyExpr = initialPropertyExpression;
      for (var i = 1; i < propertiesInPath.Length; i++)
      {
        propertyExpr = Expression.Property(propertyExpr, propertiesInPath[i]);
      }
      return propertyExpr;
    }

    private static MethodInfo GetLambdaCreationMethod(Type i_ObjectType, Type i_PropertyType)
    {
      var key = new DoubleTypeKey()
      {
        ObjectType = i_ObjectType,
        PropertyType = i_PropertyType
      };
      return m_LambdaCreationMethodCache.GetOrCreate(key, () => CreateLambdaCreationMethod(i_ObjectType, i_PropertyType));
    }

    private static MethodInfo CreateLambdaCreationMethod(Type i_ObjectType, Type i_PropertyType)
    {
      var propType = i_PropertyType;
      if (ReflectionUtils.IsGenericCollectionType(propType))
      {
        propType = ReflectionUtils.ConstructGenericEnumerableType(propType);
      }
      var funcType = typeof(Func<,>).MakeGenericType(i_ObjectType, propType);
      var typesForLambdaMethod = new Type[] { typeof(Expression), typeof(ParameterExpression[]) };
      return typeof(Expression).GetMethod("Lambda", 1, typesForLambdaMethod).MakeGenericMethod(funcType);
    }

    private static Type GetPropertyTypeFor(Type i_ParentType, string i_PropertyName)
    {
      var propPathParts = i_PropertyName.Split(new string[] { "->" }, 2, StringSplitOptions.RemoveEmptyEntries);
      var firstPropertyName = propPathParts[0];
      var key = new TypeAndPropertyNameKey()
      {
        Type = i_ParentType,
        PropertyName = firstPropertyName,
      };
      var type = m_PropertyTypeCache.GetOrCreate(key, () =>
      {
        var propertyInfo = i_ParentType.GetProperty(firstPropertyName);
        if (propertyInfo == null)
        {
          throw new InvalidOperationException($"Can not create a value for the property type cache for FetchStrategyTranslation. Type {i_ParentType.FullName} does not define a property with name {firstPropertyName}.");
        }
        return propertyInfo.PropertyType;
      });
      if (propPathParts.Length == 2)
      {
        return GetPropertyTypeFor(type, propPathParts[1]);
      }
      return type;
    }
  }
}
