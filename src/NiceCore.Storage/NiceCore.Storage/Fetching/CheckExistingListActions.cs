namespace NiceCore.Storage.Fetching
{
  /// <summary>
  /// Actions for the checking of newlist vs existinglist
  /// </summary>
  public enum CheckExistingListActions
  {
    /// <summary>
    /// Check has shown that existing and new list are equal or subset. So do nothing for new path
    /// </summary>
    ChangeNothing,
    
    /// <summary>
    /// Check has shown that existing and new list diverge before end of existing. This is not the list to change
    /// </summary>
    Diverge,
    
    /// <summary>
    /// Check has shown that newlist is an extension of existing list 
    /// </summary>
    CanBeAppended,
  }
}
