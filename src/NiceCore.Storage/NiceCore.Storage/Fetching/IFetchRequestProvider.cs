﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace NiceCore.Storage.Fetching
{
  /// <summary>
  /// Provider for fetch requests
  /// </summary>
  public interface IFetchRequestProvider
  {
    /// <summary>
    /// Returns a request for i_Query
    /// </summary>
    /// <typeparam name="TQueried"></typeparam>
    /// <typeparam name="TFetched"></typeparam>
    /// <param name="i_Query"></param>
    /// <param name="i_Selector"></param>
    /// <returns></returns>
    IFetchRequest<TQueried, TFetched> GetFetchRequest<TQueried, TFetched>(IQueryable<TQueried> i_Query, Expression<Func<TQueried, TFetched>> i_Selector);
    /// <summary>
    /// Returns a new request for fetching a collection
    /// </summary>
    /// <typeparam name="TQueried"></typeparam>
    /// <typeparam name="TFetched"></typeparam>
    /// <param name="i_Query"></param>
    /// <param name="i_Selector"></param>
    /// <returns></returns>
    IFetchRequest<TQueried, TFetched> GetFetchManyRequest<TQueried, TFetched>(IQueryable<TQueried> i_Query, Expression<Func<TQueried, IEnumerable<TFetched>>> i_Selector);
    /// <summary>
    /// Returns a ThenFetch - Request
    /// </summary>
    /// <typeparam name="TQueried"></typeparam>
    /// <typeparam name="TAlreadyFetched"></typeparam>
    /// <typeparam name="TFetched"></typeparam>
    /// <param name="i_Query"></param>
    /// <param name="i_Selector"></param>
    /// <returns></returns>
    IFetchRequest<TQueried, TFetched> GetThenFetchRequest<TQueried, TAlreadyFetched, TFetched>(IFetchRequest<TQueried, TAlreadyFetched> i_Query, Expression<Func<TAlreadyFetched, TFetched>> i_Selector);
    /// <summary>
    /// Returns a fetch request for ThenFetchMany
    /// </summary>
    /// <typeparam name="TQueried"></typeparam>
    /// <typeparam name="TAlreadyFetched"></typeparam>
    /// <typeparam name="TFetched"></typeparam>
    /// <param name="i_Query"></param>
    /// <param name="i_Selector"></param>
    /// <returns></returns>
    IFetchRequest<TQueried, TFetched> GetThenFetchManyRequest<TQueried, TAlreadyFetched, TFetched>(IFetchRequest<TQueried, TAlreadyFetched> i_Query, Expression<Func<TAlreadyFetched, IEnumerable<TFetched>>> i_Selector);
  }
}
