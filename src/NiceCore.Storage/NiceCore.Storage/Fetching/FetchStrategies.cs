﻿using System;
using NiceCore.Storage.Fetching.Impl;
using System.Linq;

namespace NiceCore.Storage.Fetching
{
  /// <summary>
  /// Helper Class for creating FetchRelations
  /// </summary>
  public static class FetchStrategies
  {
    /// <summary>
    /// Creates a new FetchStrategy from all passed relations
    /// </summary>
    /// <param name="i_Relations"></param>
    /// <returns></returns>
    public static IFetchStrategy Create(params string[] i_Relations)
    {
      return new DefaultFetchStrategy()
      {
        Relations = i_Relations.Where(r => !string.IsNullOrEmpty(r) && !string.IsNullOrWhiteSpace(r)).ToList()
      };
    }
    
    /// <summary>
    /// Adapts all relations from i_Original to a new IFetchStrategy whiches relations start with "i_NewStart."
    /// </summary>
    /// <param name="i_Original"></param>
    /// <param name="i_NewStart"></param>
    /// <returns></returns>
    /// <exception cref="ArgumentException"></exception>
    public static IFetchStrategy AdaptToNewStart(IFetchStrategy i_Original, string i_NewStart)
    {
      if (i_Original == null)
      {
        throw new ArgumentException($"Could not execute {nameof(AdaptToNewStart)} because i_Original was passed as null.");
      }
      if (i_NewStart == null)
      {
        throw new ArgumentException("Could not execute {nameof(AdaptToNewStart)} because i_NewStart was passed as null.");
      }
      var defStrat = new DefaultFetchStrategy()
      {
        Relations = new()
      };
      foreach (var relation in i_Original.Relations)
      {
        defStrat.Relations.Add(i_NewStart + "." + relation);
      }

      return defStrat;
    }
  }
}
