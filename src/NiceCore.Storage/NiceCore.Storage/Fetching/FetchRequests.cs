﻿namespace NiceCore.Storage.Fetching
{
  /// <summary>
  /// Class returning FetchProvider for fetching.
  /// TODO: using multiple storage impls is not possible with this approach
  /// </summary>
  public static class FetchRequests
  {
    /// <summary>
    /// Provider for fetch requests
    /// </summary>
    public static IFetchRequestProvider Provider { get; set; }

    /// <summary>
    /// Whether fetching is supported in current context
    /// </summary>
    public static bool AreSupported
    {
      get { return Provider != null; }
    }
  }
}
