using System.Collections.Generic;

namespace NiceCore.Storage.Fetching
{
  /// <summary>
  /// Result for checking a newpath against an existing list
  /// </summary>
  internal class CheckExistingListResult
  {
    /// <summary>
    /// Action that was filled by check
    /// </summary>
    public CheckExistingListActions Action { get; init; }
    
    /// <summary>
    /// Entries that are used if Action is CanBe Appended
    /// </summary>
    public List<string> EntriesForAppend { get; init; }
  }
}
