using System;
using System.Collections.Generic;
using System.Linq;
using NiceCore.Entities;
using NiceCore.Reflection;

namespace NiceCore.Storage.Fetching
{
  /// <summary>
  /// Class that deals with creating fetch relations strings from filter strings
  /// </summary>
  public class FetchingFromFilterBlocks
  {
    private readonly List<List<string>> m_RelationBlocks = new();
    private readonly Type m_EntityType;

    /// <summary>
    /// Ctor filling type that is used as a starting point for checking whether a property is a domain entity
    /// </summary>
    /// <param name="i_EntityType"></param>
    public FetchingFromFilterBlocks(Type i_EntityType)
    {
      m_EntityType = i_EntityType;
    }
    
    /// <summary>
    /// Adds a filter string to the existing blocks
    /// </summary>
    /// <param name="i_FilterString"></param>
    public void AddRelationFromFilterString(string i_FilterString)
    {
      var split = i_FilterString.Split(new[] {'.'}, StringSplitOptions.RemoveEmptyEntries).ToList();
      //a fetch does not need to be added if the filter just is one level or is one-level.ID
      if (split.Count == 1 || (split.Count == 2 && split[1] == nameof(IDomainEntity<Guid>.ID)))
      {
        return;
      }
      if (split.Count > 0)
      {
        //remove last part if it is not an entity
        if (!IsPathToDomainEntity(i_FilterString))
        {
          split.RemoveAt(split.Count - 1);
        }
      }
      
      if (m_RelationBlocks.Count == 0)
      {
        m_RelationBlocks.Add(split);
      }
      else
      {
        var allDiverge = true;
        for (var i = 0; i < m_RelationBlocks.Count; i++)
        {
          var list = m_RelationBlocks[i];
          var check = CheckListForPath(list, split);
          var continueWithLoop = HandleCheckResult(list, check);
          if (!continueWithLoop)
          {
            allDiverge = false;
            break;
          }
        }
        if (allDiverge)
        {
          m_RelationBlocks.Add(split);
        }
      }
    }

    private static bool HandleCheckResult(List<string> t_ExistingResult, CheckExistingListResult i_Result)
    {
      return i_Result.Action switch
      {
        CheckExistingListActions.Diverge => true,
        CheckExistingListActions.ChangeNothing => false,
        CheckExistingListActions.CanBeAppended => AppendEntries(t_ExistingResult, i_Result),
        _ => throw new NotSupportedException($"Could not {nameof(HandleCheckResult)} because enum of value {i_Result.Action} is not supported.")
      };
    }

    private static bool AppendEntries(List<string> t_ExistingResult, CheckExistingListResult i_Result)
    {
      t_ExistingResult.AddRange(i_Result.EntriesForAppend);
      return false;
    }

    private static CheckExistingListResult CheckListForPath(IReadOnlyList<string> i_ExistingList, IReadOnlyList<string> i_NewPath)
    {
      for (var i = 0; i < i_ExistingList.Count; i++)
      {
        var existingValue = i_ExistingList[i];
        if (i < i_NewPath.Count)
        {
          var newValue = i_NewPath[i];
          if (newValue != existingValue)
          {
            //if we hit this, then the paths diverge so this is not the list to add from newpath
            return new()
            {
              Action = CheckExistingListActions.Diverge
            };
          }
        }
        else
        {
          //if we hit this, than i_ExistingList is encompassing i_NewPath so we do need to change anything
          return new()
          {
            Action = CheckExistingListActions.ChangeNothing
          };
        }
      }
      //this is hit when i_NewPath and i_ExistingList are equal until we hit the end of i_ExistingList. If i_NewPath is longer -> anything above
      //i_ExistingList.Count should be added to the existing entry
      if (i_NewPath.Count > i_ExistingList.Count)
      {
        var entriesToAppend = new List<string>();
        for (var i = i_ExistingList.Count; i < i_NewPath.Count; i++)
        {
          entriesToAppend.Add(i_NewPath[i]);
        }

        return new()
        {
          Action = CheckExistingListActions.CanBeAppended,
          EntriesForAppend = entriesToAppend
        };
      }
      //if we hit this, then both new and existing are equal so change nothing
      return new()
      {
        Action = CheckExistingListActions.ChangeNothing
      };
    }

    private bool IsPathToDomainEntity(string i_Path)
    {
      var typeOfLast = ReflectionUtils.GetTypeOfLastPropertyInPath(i_Path, m_EntityType);
      return ReflectionUtils.OpenGenericTypeIsBaseFor(typeof(IDomainEntity<>), typeOfLast).Success;
    }

    /// <summary>
    /// Returns the relations from the blocks this instance holds
    /// </summary>
    /// <returns></returns>
    public List<string> ToRelations()
    {
      return m_RelationBlocks.Select(relationBlock => string.Join('.', relationBlock)).ToList();
    }
  }
}
