using NiceCore.Base;

namespace NiceCore.Storage.Config
{
  /// <summary>
  /// Connection String Transformer
  /// </summary>
  public interface IConnectionStringTransformer : IOneWayValueTransformer<string, string>
  {
    
  }
}