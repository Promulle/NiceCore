using System;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Storage.Config.AvailableSettings
{
  /// <summary>
  /// Enable Lost Storage Events Setting
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public sealed class EnableLostStorageEventsSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// KEY
    /// </summary>
    public const string KEY = "NiceCore.Storage.Events.CollectLostEvents";

    /// <summary>
    /// Key
    /// </summary>
    public string Key { get; } = KEY;

    /// <summary>
    /// Setting Type
    /// </summary>
    public Type SettingType { get; } = typeof(bool);
  }
}