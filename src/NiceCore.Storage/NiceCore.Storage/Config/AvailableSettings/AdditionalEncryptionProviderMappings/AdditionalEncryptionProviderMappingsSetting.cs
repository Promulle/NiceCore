using System;
using System.Collections.Generic;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Storage.Config.AvailableSettings.AdditionalEncryptionProviderMappings
{
  /// <summary>
  /// Additional Encryption Provider Mappings Setting
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public sealed class AdditionalEncryptionProviderMappingsSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// KEY
    /// </summary>
    public const string KEY = "NiceCore.Storage.AdditionalEncryptionProviderMappings";

    /// <summary>
    /// KEY
    /// </summary>
    public string Key { get; } = KEY;

    /// <summary>
    /// Setting Type
    /// </summary>
    public Type SettingType { get; } = typeof(List<AdditionalEncryptionProviderMapping>);
  }
}