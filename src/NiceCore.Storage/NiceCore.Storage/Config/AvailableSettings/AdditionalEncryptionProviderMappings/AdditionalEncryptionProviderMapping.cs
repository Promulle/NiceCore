namespace NiceCore.Storage.Config.AvailableSettings.AdditionalEncryptionProviderMappings
{
  /// <summary>
  /// Model for Setting defining additional EncryptionProviders
  /// </summary>
  public class AdditionalEncryptionProviderMapping
  {
    /// <summary>
    /// Connection to apply the encryption provider to
    /// </summary>
    public string ConnectionName { get; set; }
    
    /// <summary>
    /// Encryption Provider
    /// </summary>
    public string EncryptionProvider { get; set; }
  }
}