﻿namespace NiceCore.Storage
{
  /// <summary>
  /// Constatns for NiceCore.Storage
  /// </summary>
  public static class NiceCoreStorageConstants
  {
    /// <summary>
    /// Assembly name
    /// </summary>
    public static readonly string ASSEMBLY_NAME = "NiceCore.Storage";
  }
}
