using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using NiceCore.Storage.Connections.Exceptions;

namespace NiceCore.Storage.Connections
{
  /// <summary>
  /// Interface for class that can instantiate open connections
  /// </summary>
  public interface INcConnectionCreator
  {
    /// <summary>
    /// Connection Type this creator can create
    /// </summary>
    string ConnectionType { get; }

    /// <summary>
    /// Create new Connection
    /// </summary>
    /// <param name="i_ConnectionInfo"></param>
    /// <param name="i_Parameters"></param>
    /// <returns></returns>
    Task<DbConnection> CreateNewConnection(IConnectionInfo i_ConnectionInfo, IConnectionInstanceCreationParameters i_Parameters);

    /// <summary>
    /// Create new Connection
    /// </summary>
    /// <param name="i_ConnectionInfo"></param>
    /// <param name="i_Parameters"></param>
    /// <param name="i_CancellationToken"></param>
    /// <returns></returns>
    /// <exception cref="NcConnectionCreationException">Thrown if something goes wrong during creation of connection</exception>
    Task<DbConnection> CreateNewConnection(IConnectionInfo i_ConnectionInfo, IConnectionInstanceCreationParameters i_Parameters, CancellationToken i_CancellationToken);
  }
}