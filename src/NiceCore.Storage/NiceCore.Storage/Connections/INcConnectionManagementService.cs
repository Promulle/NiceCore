using System;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace NiceCore.Storage.Connections
{
  /// <summary>
  /// Service for NC sided connection management
  /// </summary>
  public interface INcConnectionManagementService
  {
    /// <summary>
    /// Get new connection
    /// </summary>
    /// <param name="i_ConnectionInfo"></param>
    /// <param name="i_Parameters"></param>
    /// <returns></returns>
    Task<DbConnection> GetNewConnection(IConnectionInfo i_ConnectionInfo, IConnectionInstanceCreationParameters i_Parameters);

    /// <summary>
    /// Get New Connection
    /// </summary>
    /// <param name="i_ConnectionInfo"></param>
    /// <param name="i_Parameters"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    /// <exception cref="InvalidOperationException"></exception>
    Task<DbConnection> GetNewConnection(IConnectionInfo i_ConnectionInfo, IConnectionInstanceCreationParameters i_Parameters, CancellationToken i_Token);

    /// <summary>
    /// Get Connection
    /// </summary>
    /// <param name="i_ConnectionInfo"></param>
    /// <param name="i_Parameters"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    Task<DbConnection> GetConnection(IConnectionInfo i_ConnectionInfo, IConnectionInstanceCreationParameters i_Parameters, CancellationToken i_Token);

    /// <summary>
    /// Gets the count of Open connections
    /// </summary>
    /// <returns></returns>
    public int GetOpenConnections();
  }
}