using System;

namespace NiceCore.Storage.Connections.Exceptions
{
  /// <summary>
  /// Connection creation exception
  /// </summary>
  public sealed class NcConnectionCreationException : Exception
  {
    /// <summary>
    /// Connection Info that failed to open a connection
    /// </summary>
    public IConnectionInfo ConnectionInfo { get; }
    
    /// <summary>
    /// Ctor filling fields
    /// </summary>
    /// <param name="i_ConnectionInfo"></param>
    /// <param name="i_InnerException"></param>
    public NcConnectionCreationException(IConnectionInfo i_ConnectionInfo, Exception i_InnerException): base(GetErrorMessage(i_ConnectionInfo), i_InnerException) 
    {
      ConnectionInfo = i_ConnectionInfo;
    }

    private static string GetErrorMessage(IConnectionInfo i_Info)
    {
      return $"Something went wrong during creation or opening of connection for {i_Info.ConnectionName}. Check inner exception for details.";
    }
  }
}