using System;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using NiceCore.Logging;
using NiceCore.Storage.Connections.Exceptions;

namespace NiceCore.Storage.Connections.Impl
{
  /// <summary>
  /// base impl for nc connection creators
  /// </summary>
  public abstract class BaseNcConnectionCreator : INcConnectionCreator
  {
    /// <summary>
    /// Connection Type this creator should be used for
    /// </summary>
    public abstract string ConnectionType { get; }

    /// <summary>
    /// Whether a null passed connection string is allowed
    /// </summary>
    public virtual bool AllowNullConnectionString { get; } = false;

    /// <summary>
    /// Create new Connection
    /// </summary>
    /// <param name="i_ConnectionInfo"></param>
    /// <param name="i_Parameters"></param>
    /// <returns></returns>
    public Task<DbConnection> CreateNewConnection(IConnectionInfo i_ConnectionInfo, IConnectionInstanceCreationParameters i_Parameters)
    {
      return CreateNewConnection(i_ConnectionInfo, i_Parameters, CancellationToken.None);
    }

    /// <summary>
    /// Create new Connection
    /// </summary>
    /// <param name="i_ConnectionInfo"></param>
    /// <param name="i_Parameters"></param>
    /// <param name="i_CancellationToken"></param>
    /// <returns></returns>
    /// <exception cref="NcConnectionCreationException">Thrown if something goes wrong during creation of connection</exception>
    public async Task<DbConnection> CreateNewConnection(IConnectionInfo i_ConnectionInfo, IConnectionInstanceCreationParameters i_Parameters, CancellationToken i_CancellationToken)
    {
      if (i_ConnectionInfo == null)
      {
        throw new ArgumentException($"Could not create Connection for type {ConnectionType}. ConnectionInfo was passed as null.");
      }
      EnsureCorrectConnectionString(i_ConnectionInfo);
      try
      {
        var instance = CreateConnectionInstance(i_ConnectionInfo.ConnectionString, i_Parameters);
        await instance.OpenAsync(i_CancellationToken).ConfigureAwait(false);
        return instance;
      }
      catch (Exception e)
      {
        throw new NcConnectionCreationException(i_ConnectionInfo, e);
      }
    }

    private void EnsureCorrectConnectionString(IConnectionInfo i_ConnectionString)
    {
      if (i_ConnectionString?.ConnectionString == null)
      {
        if (!AllowNullConnectionString)
        {
          throw new ArgumentException("Could not open connection. Connection string was null.");
        }
      }
    }

    /// <summary>
    /// Creates the instance of the connection
    /// </summary>
    /// <returns></returns>
    protected abstract DbConnection CreateConnectionInstance(string i_ConnectionString, IConnectionInstanceCreationParameters i_Parameters);
  }
}