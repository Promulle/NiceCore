namespace NiceCore.Storage.Connections
{
  /// <summary>
  /// Interface for information about a specific connection used by the storage service
  /// </summary>
  public interface IConnectionInfo
  {
    /// <summary>
    /// Enable Auditing
    /// </summary>
    bool EnableAuditing { get; }

    /// <summary>
    /// Connection Name
    /// </summary>
    string ConnectionName { get; }
    
    /// <summary>
    /// Broader Type of connection. Multiple connectionInfos might be of the same type but have different names
    /// </summary>
    string ConnectionType { get; }
    
    /// <summary>
    /// Connection String
    /// </summary>
    string ConnectionString { get; }
  }
}
