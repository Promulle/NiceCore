using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Base.Services;
using NiceCore.Logging;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation;

namespace NiceCore.Storage.Connections
{
  /// <summary>
  /// Connections management service impl
  /// </summary>
  [Register(InterfaceType = typeof(INcConnectionManagementService), LifeStyle = LifeStyles.Singleton)]
  [InitializeOnResolve]
  public sealed class NcConnectionManagementService : BaseService, INcConnectionManagementService
  {
    private readonly object m_Lock = new();
    private readonly Dictionary<string, INcConnectionCreator> m_Creators = new();
    private readonly List<DbConnection> m_OpenConnections = new();

    /// <summary>
    /// Creators
    /// </summary>
    public IList<INcConnectionCreator> Creators { get; set; }
    
    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<NcConnectionManagementService> Logger { get; set; }

    /// <summary>
    /// Internal initialize
    /// </summary>
    protected override void InternalInitialize()
    {
      if (Creators == null || Creators.Count == 0)
      {
        Logger.Warning("NcConnectionManagementService: No creators registered. You will not be able to create Connections on NiceCore side.");
        return;
      }

      foreach (var connectionCreator in Creators)
      {
        m_Creators[connectionCreator.ConnectionType] = connectionCreator;
      }
    }

    /// <summary>
    /// Dispose
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    protected override void Dispose(bool i_IsDisposing)
    {
      m_Creators.Clear();
      lock (m_Lock)
      {
        foreach (var openConnection in m_OpenConnections)
        {
          try
          {
            openConnection?.Dispose();
          }
          catch (Exception e)
          {
            Logger.Error(e, $"Error when disposing connection of Database ({openConnection?.Database}). If there is not databaseName, the connection was null.");
          }
        }  
      }
    }

    /// <summary>
    /// Get new connection
    /// </summary>
    /// <param name="i_ConnectionInfo"></param>
    /// <param name="i_Parameters"></param>
    /// <returns></returns>
    public Task<DbConnection> GetNewConnection(IConnectionInfo i_ConnectionInfo, IConnectionInstanceCreationParameters i_Parameters)
    {
      return GetNewConnection(i_ConnectionInfo, i_Parameters, CancellationToken.None);
    }

    /// <summary>
    /// Get New Connection
    /// </summary>
    /// <param name="i_ConnectionInfo"></param>
    /// <param name="i_Parameters"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    /// <exception cref="InvalidOperationException"></exception>
    public async Task<DbConnection> GetNewConnection(IConnectionInfo i_ConnectionInfo, IConnectionInstanceCreationParameters i_Parameters, CancellationToken i_Token)
    {
      if (m_Creators.TryGetValue(i_ConnectionInfo.ConnectionType, out var creator))
      {
        var connection = await creator.CreateNewConnection(i_ConnectionInfo, i_Parameters, i_Token).ConfigureAwait(false);
        lock (m_Lock)
        {
          m_OpenConnections.Add(connection);  
        }
        connection.Disposed += ConnectionOnDisposed;
        return connection;
      }

      throw new InvalidOperationException($"Could not get connection for {i_ConnectionInfo.ConnectionName}. No Instance creator was found for it.");
    }

    /// <summary>
    /// Get Connection
    /// </summary>
    /// <param name="i_ConnectionInfo"></param>
    /// <param name="i_Parameters"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    public Task<DbConnection> GetConnection(IConnectionInfo i_ConnectionInfo, IConnectionInstanceCreationParameters i_Parameters, CancellationToken i_Token)
    {
      return GetNewConnection(i_ConnectionInfo, i_Parameters, i_Token);
    }

    /// <summary>
    /// Gets the count of 
    /// </summary>
    /// <returns></returns>
    public int GetOpenConnections()
    {
      lock (m_Lock)
      {
        return m_OpenConnections.Count;  
      }
    }

    private void ConnectionOnDisposed(object sender, EventArgs e)
    {
      //this should always be true, but we go save here
      if (sender is DbConnection connection)
      {
        connection.Disposed -= ConnectionOnDisposed;
        lock (m_Lock)
        {
          m_OpenConnections.Remove(connection);  
        }
      }
    }
  }
}