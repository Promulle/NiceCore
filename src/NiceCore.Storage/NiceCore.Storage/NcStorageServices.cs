﻿namespace NiceCore.Storage
{
  /// <summary>
  /// Names for services that can be registered for storage usage
  /// </summary>
  public static class NcStorageServices
  {
    /// <summary>
    /// Connection string transformer
    /// const because otherwise it would not be useable in Register-Attribute as Name parameter
    /// </summary>
    public const string CONNECTION_STRING_TRANSFORMER = "ConnectionStringTransformer";
  }
}
