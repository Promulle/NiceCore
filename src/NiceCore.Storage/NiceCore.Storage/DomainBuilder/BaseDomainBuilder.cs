﻿using NiceCore.Base;
using System.Collections.Generic;

namespace NiceCore.Storage.DomainBuilder
{
  /// <summary>
  /// Base implementation for IDomainBuilder
  /// </summary>
  public abstract class BaseDomainBuilder : BaseDisposable, IDomainBuilder
  {
    /// <summary>
    /// list of all allowed connection names
    /// </summary>
    protected readonly List<string> m_AllowedConversations = new();

    /// <summary>
    /// Whether the assembly this domainbuilder is in allows schemaupdate of it's mappings
    /// if one builder disallows it, there will be no schemaupdate done
    /// </summary>
    public bool AllowSchemaUpdate { get; protected set; }

    /// <summary>
    /// Checks whether this DoaminBuilder is applicable for the connection described by i_ConversationName
    /// </summary>
    /// <param name="i_ConversationName">ConversationName</param>
    /// <returns></returns>
    public virtual bool IsApplicable(string i_ConversationName)
    {
      if (m_AllowedConversations.Count > 0)
      {
        return m_AllowedConversations.Contains(i_ConversationName);
      }
      return true;
    }
  }
}
