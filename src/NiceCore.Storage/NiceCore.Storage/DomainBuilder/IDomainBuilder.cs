﻿using NiceCore.Base;

namespace NiceCore.Storage.DomainBuilder
{
  /// <summary>
  /// Domain builder to mark Assemblies with mappings for storage
  /// </summary>
  public interface IDomainBuilder : IBaseDisposable
  {
    /// <summary>
    /// Whether the assembly this domainbuilder is in allows schemaupdate of it's mappings
    /// if one builder disallows it, there will be no schemaupdate done
    /// </summary>
    bool AllowSchemaUpdate { get; }
    /// <summary>
    /// Checks whether this domainBuilder is applicable for the Connection described by i_ConnectioName
    /// </summary>
    /// <param name="i_ConnectionName"></param>
    /// <returns></returns>
    bool IsApplicable(string i_ConnectionName);
  }
}
