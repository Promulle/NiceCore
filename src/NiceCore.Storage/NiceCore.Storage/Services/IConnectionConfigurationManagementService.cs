﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NiceCore.Storage.ConfigurationModel;
using NiceCore.Storage.DomainBuilder;

namespace NiceCore.Storage.Services
{
  /// <summary>
  /// Service for managing existing configurations based on connection definitions
  /// </summary>
  public interface IConnectionConfigurationManagementService<TDefinition, TConfig> where TDefinition: IConnectionDefinition
  {
    /// <summary>
    /// Adds a configuration from definition (for example reading config file that is specified in connection definition)
    /// </summary>
    /// <param name="i_Definition"></param>
    /// <param name="i_DomainBuilder"></param>
    /// <returns></returns>
    Task AddConfigurationFromDefinition(TDefinition i_Definition, IEnumerable<IDomainBuilder> i_DomainBuilder);

    /// <summary>
    /// Adds a configuration from definition (for example reading config file that is specified in connection definition)
    /// </summary>
    /// <param name="i_DefinitionName"></param>
    /// <param name="i_DomainBuilder"></param>
    /// <returns></returns>
    Task AddConfigurationFromDefinition(string i_DefinitionName, IEnumerable<IDomainBuilder> i_DomainBuilder);

    /// <summary>
    /// Adds a new config from IConnectionConfiguration
    /// </summary>
    /// <param name="i_Name">Name of connection the configuration is used in</param>
    /// <param name="i_Configuration"></param>
    /// <param name="i_DomainBuilder"></param>
    /// <param name="i_Definition"></param>
    /// <returns></returns>
    Task AddConfigurationFromConfigurationModel(string i_Name, IConnectionConfiguration i_Configuration, IEnumerable<IDomainBuilder> i_DomainBuilder, TDefinition i_Definition = default);

    /// <summary>
    /// Returns the config for the given name
    /// </summary>
    /// <param name="i_Name">Name the config should be found under</param>
    /// <returns></returns>
    TConfig GetConfiguration(string i_Name);

    /// <summary>
    /// Returns the config for the given name or creates a new one for the name based on the configuration model passed
    /// </summary>
    /// <param name="i_Name">Name the config should be found under</param>
    /// <param name="i_Configuration">ConfigurationModel used to create the config</param>
    /// <param name="i_Builder"></param>
    /// <param name="i_Definition"></param>
    /// <returns></returns>
    Task<TConfig> GetConfiguration(string i_Name, IConnectionConfiguration i_Configuration, IEnumerable<IDomainBuilder> i_Builder, TDefinition i_Definition = default);

    /// <summary>
    /// Returns the config for the given name, or creates a new one from the definition
    /// </summary>
    /// <param name="i_Name">Name the config should be found under</param>
    /// <param name="i_Definition">Connection used to create the config if none found</param>
    /// <param name="i_Builder"></param>
    /// <returns></returns>
    Task<TConfig> GetConfiguration(string i_Name, TDefinition i_Definition, IEnumerable<IDomainBuilder> i_Builder);
  }
}