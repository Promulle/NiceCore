﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using NiceCore.Storage.ConfigurationModel;
using NiceCore.Storage.DomainBuilder;

namespace NiceCore.Storage.Services.Impl
{
  /// <summary>
  /// Base impl for IConnectionConfigurationManagementService
  /// </summary>
  /// <typeparam name="TDefinition"></typeparam>
  /// <typeparam name="TConfig"></typeparam>
  public abstract class BaseConnectionConfigurationManagementService<TDefinition, TConfig> : IConnectionConfigurationManagementService<TDefinition, TConfig>
    where TDefinition: IConnectionDefinition
  {
    private readonly ConcurrentDictionary<string, TConfig> m_Configs = new();
    
    /// <summary>
    /// Connection definition managment service
    /// </summary>
    public IConnectionDefinitionManagementService ConnectionDefinitionManagementService { get; set; }

    /// <summary>
    /// Add a configuration from definition
    /// </summary>
    /// <param name="i_Definition"></param>
    /// <param name="i_DomainBuilder"></param>
    /// <returns></returns>
    public async Task AddConfigurationFromDefinition(TDefinition i_Definition, IEnumerable<IDomainBuilder> i_DomainBuilder)
    {
      var config = await CreateConfigFromDefinition(i_Definition, i_DomainBuilder).ConfigureAwait(false);
      m_Configs[i_Definition.Name] = config;
    }

    /// <summary>
    /// Add a configuration from definition by name
    /// </summary>
    /// <param name="i_DefinitionName"></param>
    /// <param name="i_DomainBuilder"></param>
    /// <returns></returns>
    public Task AddConfigurationFromDefinition(string i_DefinitionName, IEnumerable<IDomainBuilder> i_DomainBuilder)
    {
      if (ConnectionDefinitionManagementService == null)
      {
        throw new NotSupportedException($"Adding a configuration from definition name is not supported: Property {nameof(ConnectionDefinitionManagementService)} is not filled.");
      }
      var def = ConnectionDefinitionManagementService.GetConnectionDefinition<TDefinition>(i_DefinitionName);
      if (def != null)
      {
        return AddConfigurationFromDefinition(def, i_DomainBuilder);
      }
      throw new InvalidOperationException($"Could not create a configuration from definition name: {i_DefinitionName} does not have a correlating definition.");
    }

    /// <summary>
    /// Add configuration from ConnectionConfiguration
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Configuration"></param>
    /// <param name="i_DomainBuilder"></param>
    /// <param name="i_Definition"></param>
    /// <returns></returns>
    public async Task AddConfigurationFromConfigurationModel(string i_Name, IConnectionConfiguration i_Configuration, IEnumerable<IDomainBuilder> i_DomainBuilder, TDefinition i_Definition = default )
    {
      m_Configs[i_Name] = await CreateConfigFromModelInternal(i_Name, i_Configuration, i_Definition, i_DomainBuilder).ConfigureAwait(false);
    }

    private Task<TConfig> CreateConfigFromModelInternal(string i_Name, IConnectionConfiguration i_Configuration, TDefinition i_Definition, IEnumerable<IDomainBuilder> i_DomainBuilder)
    {
      var def = i_Definition != null ? i_Definition : GetReasonableDefaultDefinition(i_Name);
      ConnectionDefinitionManagementService.AddConnectionDefinition(def, false);
      return CreateConfigFromModel(i_Configuration, def, i_DomainBuilder);
    }

    /// <summary>
    /// Creates the config from Definition
    /// </summary>
    /// <param name="i_Definition"></param>
    /// <param name="i_DomainBuilder"></param>
    /// <returns></returns>
    protected abstract Task<TConfig> CreateConfigFromDefinition(TDefinition i_Definition, IEnumerable<IDomainBuilder> i_DomainBuilder);

    /// <summary>
    /// Creates a config from the model
    /// </summary>
    /// <param name="i_Model"></param>
    /// <param name="i_Definition"></param>
    /// <param name="i_DomainBuilder"></param>
    /// <returns></returns>
    protected abstract Task<TConfig> CreateConfigFromModel(IConnectionConfiguration i_Model, TDefinition i_Definition, IEnumerable<IDomainBuilder> i_DomainBuilder);

    /// <summary>
    /// Returns a reasonable default definition that is used when a configuration should be created from model but no definition was passed
    /// </summary>
    /// <returns></returns>
    protected abstract TDefinition GetReasonableDefaultDefinition(string i_Name);

    /// <summary>
    /// Get configuration
    /// </summary>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    public TConfig GetConfiguration(string i_Name)
    {
      if (!m_Configs.TryGetValue(i_Name, out var val))
      {
        return val;
      }
      return default;
    }

    /// <summary>
    /// Get Configuration
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Configuration"></param>
    /// <param name="i_DomainBuilder"></param>
    /// <param name="i_Definition"></param>
    /// <returns></returns>
    public async Task<TConfig> GetConfiguration(string i_Name, IConnectionConfiguration i_Configuration, IEnumerable<IDomainBuilder> i_DomainBuilder, TDefinition i_Definition = default)
    {
      if (!m_Configs.TryGetValue(i_Name, out var cfg))
      {
        cfg = await CreateConfigFromModelInternal(i_Name, i_Configuration, i_Definition, i_DomainBuilder).ConfigureAwait(false);
        m_Configs[i_Name] = cfg;
      }
      return cfg;
    }

    /// <summary>
    /// get Configuration
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Definition"></param>
    /// <param name="i_DomainBuilder"></param>
    /// <returns></returns>
    public async Task<TConfig> GetConfiguration(string i_Name, TDefinition i_Definition, IEnumerable<IDomainBuilder> i_DomainBuilder)
    {
      if (!m_Configs.TryGetValue(i_Name, out var cfg))
      {
        cfg = await CreateConfigFromDefinition(i_Definition, i_DomainBuilder).ConfigureAwait(false);
        m_Configs[i_Name] = cfg;
      }
      return cfg;
    }
  }
}