﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using NiceCore.ServiceLocation;

namespace NiceCore.Storage.Services.Impl
{
  /// <summary>
  /// Service for managing connection definitions
  /// </summary>
  [Register(InterfaceType = typeof(IConnectionDefinitionManagementService), LifeStyle = LifeStyles.Singleton)]
  public class ConnectionDefinitionManagementService : IConnectionDefinitionManagementService
  {
    private readonly ConcurrentDictionary<string, IConnectionDefinition> m_ConnectionDefinitions = new();
    
    /// <summary>
    /// List of registered integrity Checks
    /// </summary>
    public IList<IConnectionDefinitionIntegrityCheck> IntegrityChecks { get; set; }
    
    /// <summary>
    /// Add
    /// </summary>
    /// <param name="i_Definition"></param>
    /// <param name="i_AllowOverwrite"></param>
    public void AddConnectionDefinition(IConnectionDefinition i_Definition, bool i_AllowOverwrite)
    {
      EnsureIntegrityOfConnectionDefinition(i_Definition);
      if (i_AllowOverwrite)
      {
        m_ConnectionDefinitions[i_Definition.Name] = i_Definition;
      }
      else
      {
        if (!m_ConnectionDefinitions.ContainsKey(i_Definition.Name))
        {
          m_ConnectionDefinitions[i_Definition.Name] = i_Definition;
        }
      }
    }

    private void EnsureIntegrityOfConnectionDefinition(IConnectionDefinition i_Definition)
    {
      if (IntegrityChecks != null)
      {
        foreach (var integrityCheck in IntegrityChecks)
        {
          integrityCheck.EnsureIntegrityOfConnectionDefinition(i_Definition);
        }
      }
    }

    /// <summary>
    /// Remove
    /// </summary>
    /// <param name="i_Name"></param>
    public void RemoveConnectionDefinition(string i_Name)
    {
      m_ConnectionDefinitions.Remove(i_Name, out var _);
    }

    /// <summary>
    /// gets the connection 
    /// </summary>
    /// <param name="i_Name"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public T GetConnectionDefinition<T>(string i_Name) where T : IConnectionDefinition
    {
      var connDef = GetConnectionDefinition(i_Name);
      if (connDef != default)
      {
        if (connDef is T casted)
        {
          return casted;
        }

        throw new InvalidCastException($"Could not retrieve a connection definition for name {i_Name}: ConnectionDefinitionInstance of type {connDef.GetType().FullName} could not be cast to {typeof(T).FullName}");
      }

      return default;
    }

    /// <summary>
    /// gets the definition
    /// </summary>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    public IConnectionDefinition GetConnectionDefinition(string i_Name)
    {
      if (m_ConnectionDefinitions.TryGetValue(i_Name, out var val))
      {
        return val;
      }

      return default;
    }

    /// <summary>
    /// Get all connections
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public ICollection<T> GetAllConnectionDefinitions<T>() where T : IConnectionDefinition
    {
      return GetAllConnectionDefinitions().Cast<T>().ToList();
    }

    /// <summary>
    /// Get all conections
    /// </summary>
    /// <returns></returns>
    public ICollection<IConnectionDefinition> GetAllConnectionDefinitions()
    {
      return m_ConnectionDefinitions.Values;
    }
  }
}