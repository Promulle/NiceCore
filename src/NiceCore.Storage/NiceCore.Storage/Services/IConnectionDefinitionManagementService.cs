﻿using System.Collections.Generic;

namespace NiceCore.Storage.Services
{
  /// <summary>
  /// Service that manages existing connection definitions
  /// </summary>
  public interface IConnectionDefinitionManagementService
  {
    /// <summary>
    /// Adds a connection. If i_AllowOverwrite is true, an existing definition with the same name will be replaced
    /// </summary>
    /// <param name="i_Definition"></param>
    /// <param name="i_AllowOverwrite"></param>
    void AddConnectionDefinition(IConnectionDefinition i_Definition, bool i_AllowOverwrite);

    /// <summary>
    /// Removes the connection definition for i_Name
    /// </summary>
    /// <param name="i_Name"></param>
    void RemoveConnectionDefinition(string i_Name);
    
    /// <summary>
    /// Returns the connection definition that is registered for name i_Name.
    /// This directly casts the connection definition to T. Will throw an InvalidCastException if that is not possible
    /// </summary>
    /// <param name="i_Name"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    T GetConnectionDefinition<T>(string i_Name) where T : IConnectionDefinition;

    /// <summary>
    /// Returns the connection definition that is registered for name i_Name.
    /// Returns null if not found
    /// </summary>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    IConnectionDefinition GetConnectionDefinition(string i_Name);
    
    /// <summary>
    /// Returns the connection definition that is registered for name i_Name.
    /// This directly casts the connection definition to T. Will throw an InvalidCastException if that is not possible
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    ICollection<T> GetAllConnectionDefinitions<T>() where T : IConnectionDefinition;

    /// <summary>
    /// Returns the connection definition that is registered for name i_Name.
    /// Returns null if not found
    /// </summary>
    /// <returns></returns>
    ICollection<IConnectionDefinition> GetAllConnectionDefinitions();
  }
}