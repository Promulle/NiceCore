namespace NiceCore.Storage.Services
{
  /// <summary>
  /// Connection Definition Integrity Check
  /// </summary>
  public interface IConnectionDefinitionIntegrityCheck
  {
    /// <summary>
    /// Function that checks whether i_Definition does correspond to the expectation of it
    /// </summary>
    /// <param name="i_Definition"></param>
    void EnsureIntegrityOfConnectionDefinition(IConnectionDefinition i_Definition);
  }
}