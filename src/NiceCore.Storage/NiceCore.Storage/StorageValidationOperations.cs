﻿namespace NiceCore.Storage
{
  /// <summary>
  /// Class holding constants for validation operations used by storage services, for example the conversation
  /// </summary>
  public static class StorageValidationOperations
  {
    /// <summary>
    /// Operation that saves or updates
    /// </summary>
    public static readonly string SAVE_OR_UPDATE = "SaveOrUpdate";

    /// <summary>
    /// Operation that deletes
    /// </summary>
    public static readonly string DELETE = "Delete";
  }
}
