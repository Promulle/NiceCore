﻿using System;
using System.Collections.Generic;

namespace NiceCore.Storage
{
  /// <summary>
  /// Status of current retries
  /// </summary>
  public interface IRetryStatus
  {
    /// <summary>
    /// Retries
    /// </summary>
    int Retries { get; set; }

    /// <summary>
    /// All exceptions encountered
    /// </summary>
    List<Exception> EncounteredExceptions { get; set; }

    /// <summary>
    /// Exception that prompted to call to retry
    /// </summary>
    Exception LatestException { get; set; }
  }
}
