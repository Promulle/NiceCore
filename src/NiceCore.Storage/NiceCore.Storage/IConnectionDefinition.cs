﻿namespace NiceCore.Storage
{
  /// <summary>
  /// Definition of a connection used by nicecore
  /// </summary>
  public interface IConnectionDefinition
  {
    /// <summary>
    /// Name of the connection
    /// </summary>
    string Name { get; }
    
    /// <summary>
    /// Whether auditing is allowed or enabled for the connection
    /// </summary>
    bool AllowAuditing { get; }
  }
}