﻿using NiceCore.Entities;

namespace NiceCore.Storage.LazyLoading
{
  /// <summary>
  /// Service providing support for creation of lazy proxies
  /// </summary>
  public interface ILazyProxySupport
  {
    /// <summary>
    /// Creates a lazy load proxy according to ILazyProxyCreationParameters
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <param name="i_Parameter"></param>
    /// <returns></returns>
    T CreateLazyProxy<T, TId>(ILazyProxyCreationParameters<TId> i_Parameter) where T : class, IDomainEntity<TId>;

    /// <summary>
    /// Unproxy
    /// </summary>
    /// <param name="i_Parameter"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    T Unproxy<T, TId>(IUnproxyParameters i_Parameter)  where T : IDomainEntity<TId>;

    /// <summary>
    /// Checks if an instance is a proxy
    /// </summary>
    /// <param name="i_PossibleProxy"></param>
    /// <returns></returns>
    bool IsProxy(object i_PossibleProxy);
    
    /// <summary>
    /// Unproxy
    /// </summary>
    /// <param name="i_Parameter"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    T UnproxyWithInitialize<T, TId>(IUnproxyParameters i_Parameter)  where T : IDomainEntity<TId>;
  }
}
