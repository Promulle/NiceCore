﻿namespace NiceCore.Storage.LazyLoading
{
  /// <summary>
  /// Parameters used for creation of lazy proxies
  /// </summary>
  /// <typeparam name="TId"></typeparam>
  public interface ILazyProxyCreationParameters<TId>
  {
    /// <summary>
    /// ID to use for proxy
    /// </summary>
    TId ID { get; }
  }
}
