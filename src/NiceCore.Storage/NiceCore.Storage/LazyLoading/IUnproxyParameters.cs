namespace NiceCore.Storage.LazyLoading
{
  /// <summary>
  /// Parameter used for unproxying
  /// </summary>
  public interface IUnproxyParameters
  {
    /// <summary>
    /// POssible proxy
    /// </summary>
    object PossibleProxy { get; set; }
  }
}
