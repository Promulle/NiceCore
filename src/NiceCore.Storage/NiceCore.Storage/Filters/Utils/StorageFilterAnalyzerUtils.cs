using System;
using NiceCore.Entities;
using NiceCore.Filtering;

namespace NiceCore.Storage.Filters.Utils
{
  /// <summary>
  /// Utils for analyzing Storage filters
  /// </summary>
  public class StorageFilterAnalyzerUtils
  {
    /// <summary>
    /// Determines whether i_Filter has at least one Path that contains more than 2 instances of '.' 
    /// or at least one path that contains a '.' not does not conform to "PropertyName.ID"
    /// </summary>
    /// <param name="i_Filter"></param>
    /// <returns></returns>
    public static bool AnyComplexPropertyPath(INcFilter i_Filter)
    {
      if (i_Filter == null)
      {
        return false;
      }

      foreach (var filterCondition in i_Filter.Conditions)
      {
        var propNameSplit = filterCondition.PropertyName.Split('.', StringSplitOptions.RemoveEmptyEntries);
        if (propNameSplit.Length > 2)
        {
          return true;
        }

        if (propNameSplit.Length == 2 && propNameSplit[1] != nameof(IDomainEntity<int>.ID))
        {
          return true;
        }
      }

      if (i_Filter.SubFilters?.Count > 0)
      {
        foreach (var subFilter in i_Filter.SubFilters)
        {
          var anyComplex = AnyComplexPropertyPath(subFilter);
          if (anyComplex)
          {
            return true;
          }
        }
      }

      return false;
    }
  }
}
