namespace NiceCore.Storage.Queries
{
  /// <summary>
  /// Nc Query Provider
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public interface INcQueryProvider<T>
  {
    /// <summary>
    /// Options
    /// </summary>
    public INcQueryOptions Options { get; }
  }
}