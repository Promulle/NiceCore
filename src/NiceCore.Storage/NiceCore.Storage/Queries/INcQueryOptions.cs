using Polly;

namespace NiceCore.Storage.Queries
{
  /// <summary>
  /// Query Options
  /// </summary>
  public interface INcQueryOptions
  {
    /// <summary>
    /// Policy
    /// </summary>
    IAsyncPolicy Policy { get; }
  }
}