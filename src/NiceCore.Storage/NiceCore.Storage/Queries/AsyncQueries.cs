﻿namespace NiceCore.Storage.Queries
{
  /// <summary>
  /// Async queries
  /// </summary>
  public static class AsyncQueries
  {
    /// <summary>
    /// Provider that enables async execution of queries
    /// </summary>
    public static IAsyncQueryProvider AsyncQueryProvider { get; set; }

    /// <summary>
    /// Whether fetching is supported in current context
    /// </summary>
    public static bool AreSupported
    {
      get { return AsyncQueryProvider != null; }
    }
  }
}
