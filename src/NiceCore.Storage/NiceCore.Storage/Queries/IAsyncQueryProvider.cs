﻿using NiceCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Polly;

namespace NiceCore.Storage.Queries
{
  /// <summary>
  /// Provider that provides functionality for executing queries async
  /// </summary>
  public interface IAsyncQueryProvider
  {
    /// <summary>
    /// returns the async execution of t_Query
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="t_Query"></param>
    /// <param name="i_Policy"></param>
    /// <returns></returns>
    Task<List<T>> QueryToListAsync<T, Tid>(IQueryable<T> t_Query, IAsyncPolicy i_Policy) where T : IDomainEntity<Tid>;

    /// <summary>
    /// Call CountAsync
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="t_Query"></param>
    /// <param name="i_Policy"></param>
    /// <returns></returns>
    Task<int> CountAsync<T, Tid>(IQueryable<T> t_Query, IAsyncPolicy i_Policy) where T : IDomainEntity<Tid>;

    /// <summary>
    /// Call CountAsync
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="t_Query"></param>
    /// <param name="i_WhereExpression"></param>
    /// <param name="i_Policy"></param>
    /// <returns></returns>
    Task<int> CountAsync<T, Tid>(IQueryable<T> t_Query, Expression<Func<T, bool>> i_WhereExpression, IAsyncPolicy i_Policy) where T : IDomainEntity<Tid>;

    /// <summary>
    /// returns the async execution of t_Query
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Query"></param>
    /// <param name="i_Policy"></param>
    /// <returns></returns>
    Task<List<T>> QueryToListAsync<T>(IQueryable<T> t_Query, IAsyncPolicy i_Policy);

    /// <summary>
    /// Call CountAsync
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Query"></param>
    /// <param name="i_Policy"></param>
    /// <returns></returns>
    Task<int> CountAsync<T>(IQueryable<T> t_Query, IAsyncPolicy i_Policy);

    /// <summary>
    /// Call CountAsync
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Query"></param>
    /// <param name="i_WhereExpression"></param>
    /// <param name="i_Policy"></param>
    /// <returns></returns>
    Task<int> CountAsync<T>(IQueryable<T> t_Query, Expression<Func<T, bool>> i_WhereExpression, IAsyncPolicy i_Policy);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="t_Query"></param>
    /// <param name="i_Policy"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    Task<bool> AnyAsync<T>(IQueryable<T> t_Query, IAsyncPolicy i_Policy);

    /// <summary>
    /// First Or Default Async
    /// </summary>
    /// <param name="t_Query"></param>
    /// <param name="i_Policy"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <returns></returns>
    Task<T> QueryFirstOrDefaultAsync<T, Tid>(IQueryable<T> t_Query, IAsyncPolicy i_Policy) where T : IDomainEntity<Tid>;

    /// <summary>
    /// First or default async
    /// </summary>
    /// <param name="t_Query"></param>
    /// <param name="i_Policy"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    Task<T> QueryFirstOrDefaultAsync<T>(IQueryable<T> t_Query, IAsyncPolicy i_Policy);

    /// <summary>
    /// First Or Default Async
    /// </summary>
    /// <param name="t_Query"></param>
    /// <param name="i_WhereExpr"></param>
    /// <param name="i_Policy"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <returns></returns>
    Task<T> QueryFirstOrDefaultAsync<T, Tid>(IQueryable<T> t_Query, Expression<Func<T, bool>> i_WhereExpr, IAsyncPolicy i_Policy) where T : IDomainEntity<Tid>;
  }
}
