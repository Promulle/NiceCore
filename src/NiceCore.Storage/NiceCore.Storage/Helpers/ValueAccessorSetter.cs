using System;

namespace NiceCore.Storage.Helpers
{
  /// <summary>
  /// Value Acessor setter used for state class
  /// </summary>
  public class ValueAccessorSetter<T>
  {
    /// <summary>
    /// State
    /// </summary>
    private readonly GenericEntityState<T> m_State;
    
    /// <summary>
    /// Index
    /// </summary>
    public int Index { get; init; }

    private ValueAccessorSetter(GenericEntityState<T> i_State)
    {
      m_State = i_State;
    }

    /// <summary>
    /// Set Value
    /// </summary>
    public void SetValue(object i_Value)
    {
      m_State.State[Index] = i_Value;
    }
    
    /// <summary>
    /// Get Value
    /// </summary>
    /// <returns></returns>
    public object GetValue()
    {
      return m_State.State[Index];
    }

    /// <summary>
    /// Creates a new value accessor setter
    /// </summary>
    /// <param name="i_State"></param>
    /// <param name="i_Index"></param>
    /// <returns></returns>
    public static ValueAccessorSetter<T> Create(GenericEntityState<T> i_State, int i_Index)
    {
      return new (i_State)
      {
        Index = i_Index
      };
    }
  }
}