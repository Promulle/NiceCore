using System;
using System.Collections.Generic;

namespace NiceCore.Storage.Helpers
{
  /// <summary>
  /// State encapsulating propertynames and their respective values for a certain entity
  /// </summary>
  public class GenericEntityState<T> : IGenericEntityState<T>
  {
    /// <summary>
    /// Entityy
    /// </summary>
    public T Entity { get; private set; }
    
    /// <summary>
    /// State of the properties
    /// </summary>
    public object[] State { get; private set; }

    /// <summary>
    /// Properties
    /// </summary>
    public Dictionary<string, ValueAccessorSetter<T>> Properties { get; init; }

    /// <summary>
    /// Indexer
    /// </summary>
    /// <param name="i_PropertyName"></param>
    public object this[string i_PropertyName]
    {
      get
      {
        if (Properties.TryGetValue(i_PropertyName, out var property))
        {
          return property.GetValue();
        }

        return null;
      }
      set
      {
        if (Properties.TryGetValue(i_PropertyName, out var property))
        {
           property.SetValue(value);
        }
      }
    }
    
    /// <summary>
    /// ctor
    /// </summary>
    private GenericEntityState()
    {
    }

    /// <summary>
    /// Constructs a new instance using the entity, propertynames and propertyvalues. The arrays must match their order
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_PropertyNames"></param>
    /// <param name="i_PropertyValues"></param>
    /// <returns></returns>
    public static GenericEntityState<T> Of(T i_Entity, string[] i_PropertyNames, object[] i_PropertyValues)
    {
      if (i_PropertyNames.Length != i_PropertyValues.Length)
      {
        throw new InvalidOperationException($"Arrays {nameof(i_PropertyNames)} and {nameof(i_PropertyValues)} do not match. Cannot create new instance of {nameof(GenericEntityState<T>)}");
      }
      var state = new GenericEntityState<T>()
      {
        Entity = i_Entity,
        State = i_PropertyValues,
        Properties = new(i_PropertyNames.Length)
      };
      for (var i = 0; i < i_PropertyNames.Length; i++)
      {
        var propertyName = i_PropertyNames[i];
        var valueProperty = ValueAccessorSetter<T>.Create(state, i);
        state.Properties[propertyName] = valueProperty;
      }

      return state;
    }
  }
}