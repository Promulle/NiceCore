using System.Collections.Generic;

namespace NiceCore.Storage.Helpers
{
  /// <summary>
  /// Generic entity state
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public interface IGenericEntityState<out T>
  {
    /// <summary>
    /// Entity
    /// </summary>
    T Entity { get; }

    /// <summary>
    /// Indexer
    /// </summary>
    /// <param name="i_PropertyName"></param>
    object this[string i_PropertyName]
    {
      get;
      set;
    }
  }
}