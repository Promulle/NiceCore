using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using NiceCore.Logging.Extensions;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Conversations.Flags;

namespace NiceCore.Storage.Limiters
{
  /// <summary>
  /// Support instance for applying limiters inside a conversation
  /// </summary>
  public sealed class QueryLimiterSupport
  {
    private readonly List<IQueryLimiter>        m_Limiters;
    private          bool                       m_Ignore;
    private readonly ILogger                    m_Logger;
    private readonly ConversationOperationFlags m_Flags;
    private readonly IConversationContext       m_Context;

    /// <summary>
    /// Query Limiter support
    /// </summary>
    /// <param name="i_Limiters"></param>
    /// <param name="i_IgnoreLimiters"></param>
    /// <param name="i_Logger"></param>
    /// <param name="i_Context"></param>
    /// <param name="i_Flags"></param>
    public QueryLimiterSupport(IEnumerable<IQueryLimiter> i_Limiters,
      bool i_IgnoreLimiters,
      ILogger i_Logger,
      IConversationContext i_Context,
      ConversationOperationFlags i_Flags)
    {
      m_Ignore   = i_IgnoreLimiters;
      m_Logger   = i_Logger;
      m_Context  = i_Context;
      m_Flags    = i_Flags;
      m_Limiters = i_Limiters.ToList();
    }

    /// <summary>
    /// Disable limiters
    /// </summary>
    public void DisableLimiters()
    {
      m_Ignore = true;
    }

    /// <summary>
    /// Enable limiters
    /// </summary>
    public void EnableLimiters()
    {
      m_Ignore = false;
    }

    /// <summary>
    /// Adds a limiter to be used
    /// </summary>
    /// <param name="i_Limiter"></param>
    public void AddLimiter(IQueryLimiter i_Limiter)
    {
      m_Limiters.Add(i_Limiter);
    }

    /// <summary>
    /// Applies the fitting limiters to i_Query
    /// </summary>
    /// <param name="i_Query"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public IQueryable<T> ApplyLimiter<T>(IQueryable<T> i_Query)
    {
      return ApplyLimiters(i_Query, m_Limiters, m_Ignore, m_Flags, m_Context, m_Logger);
    }

    /// <summary>
    /// Applies custom limiters to i_Query
    /// </summary>
    /// <param name="i_Query"></param>
    /// <param name="i_CustomLimiters"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public IQueryable<T> ApplyCustomLimiter<T>(IQueryable<T> i_Query, IReadOnlyCollection<IQueryLimiter> i_CustomLimiters)
    {
      return ApplyLimiters(i_Query, i_CustomLimiters, m_Ignore, m_Flags, m_Context, m_Logger);
    }

    /// <summary>
    /// Applies limiters
    /// </summary>
    /// <param name="i_Query"></param>
    /// <param name="i_Limiters"></param>
    /// <param name="i_Ignore"></param>
    /// <param name="i_Flags"></param>
    /// <param name="i_Context"></param>
    /// <param name="i_Logger"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static IQueryable<T> ApplyLimiters<T>(IQueryable<T> i_Query,
      IReadOnlyCollection<IQueryLimiter> i_Limiters,
      bool i_Ignore,
      ConversationOperationFlags i_Flags,
      IConversationContext i_Context,
      ILogger i_Logger)
    {
      var qry = i_Query;
      if (!i_Ignore)
      {
        foreach (var limiter in i_Limiters)
        {
          i_Logger.Information($"Applying limiter of type {limiter.GetType().FullName}");
          qry = limiter.LimitQuery(qry, i_Flags, i_Context);
        }
      }

      return qry;
    }
  }
}
