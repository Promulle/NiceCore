﻿namespace NiceCore.Storage
{
  /// <summary>
  /// Static helper class for IConversationOperationConfiguration
  /// </summary>
  public static class ConversationOperationConfig
  {
    /// <summary>
    /// Default Instance
    /// </summary>
    public static IConversationOperationConfiguration DEFAULT_INSTANCE = CreateDefault();
    
    /// <summary>
    /// Creates a default config
    /// </summary>
    /// <returns></returns>
    private static IConversationOperationConfiguration CreateDefault()
    {
      return new DefaultConversationOperationConfiguration()
      {
        ValidateInstance = true,
        IsDefaultInstance = true
      };
    }

    /// <summary>
    /// CreateNoValidationConfig
    /// </summary>
    /// <returns></returns>
    public static IConversationOperationConfiguration CreateNoValidationConfig()
    {
      return new DefaultConversationOperationConfiguration()
      {
        ValidateInstance = false,
        IsDefaultInstance = false,
      };
    }
    
    /// <summary>
    /// CreateNoValidationConfig
    /// </summary>
    /// <returns></returns>
    public static IConversationOperationConfiguration CreateValidationConfig()
    {
      return new DefaultConversationOperationConfiguration()
      {
        ValidateInstance = true,
        IsDefaultInstance = false,
      };
    }
  }
}
