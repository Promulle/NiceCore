using System;
using System.Collections.Concurrent;
using Microsoft.Extensions.Logging;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation;

namespace NiceCore.Storage.Conversations
{
  /// <summary>
  /// Default impl for IActiveConversationStore
  /// </summary>
  [Register(InterfaceType = typeof(IActiveConversationStore), LifeStyle = LifeStyles.Singleton)]
  public class DefaultActiveConversationStore : IActiveConversationStore
  {
    private readonly ConcurrentDictionary<Guid, IConversation> m_ActiveConversations = new();
    
    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<DefaultActiveConversationStore> Logger { get; set; }

    /// <summary>
    /// Adds i_Conversation to active conversations
    /// </summary>
    /// <param name="i_Conversation"></param>
    public void AddActiveConversation(IConversation i_Conversation)
    {
      var id = i_Conversation.GetIdentifier();
      if (id == null || id == Guid.Empty)
      {
        Logger.Warning($"Could not add conversation of type {i_Conversation.GetType()} to active conversations: The conversation returned null or an empty guid as id.");
        return;
      }

      m_ActiveConversations[id.Value] = i_Conversation;
    }

    /// <summary>
    /// Get Active Conversation for the given id
    /// </summary>
    /// <param name="i_ConversationId"></param>
    /// <returns></returns>
    public IConversation GetActiveConversation(Guid i_ConversationId)
    {
      if (m_ActiveConversations.TryGetValue(i_ConversationId, out var conv))
      {
        return conv;
      }
      Logger.Warning($"An active conversation was requested for id {i_ConversationId} but none was found.");
      return null;
    }

    /// <summary>
    /// Removes the conversation for the given id
    /// </summary>
    /// <param name="i_ConversationId"></param>
    /// <returns></returns>
    public bool RemoveConversation(Guid i_ConversationId)
    {
      if (i_ConversationId == Guid.Empty)
      {
        Logger.Warning($"Could not remove conversation for id, since an empty guid was passed.");
      }
      var result = m_ActiveConversations.TryRemove(i_ConversationId, out _);
      if (!result)
      {
        Logger.Warning($"Conversation for id {i_ConversationId} could not be removed since it was not found as active conversation");
      }

      return result;
    }

    /// <summary>
    /// Remove Conversation by instance
    /// </summary>
    /// <param name="i_Conversation"></param>
    /// <returns></returns>
    public bool RemoveConversation(IConversation i_Conversation)
    {
      var id = i_Conversation.GetIdentifier();
      if (id != null)
      {
        return RemoveConversation(id.Value);
      }

      return false;
    }
  }
}