﻿using NiceCore.Entities;
using NiceCore.Filtering;
using NiceCore.Storage.Fetching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace NiceCore.Storage.Conversations.Queries
{
  /// <summary>
  /// A query that is dependent on using a pre selection
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <typeparam name="TKey"></typeparam>
  public interface IPreSelectDependentQuery<T, TKey> where T : IDomainEntity<TKey>
  {
    /// <summary>
    /// executing the pre select
    /// </summary>
    /// <param name="i_Conversation">Conversation to create/execute actual query</param>
    /// <param name="i_Context">Context for pre selection</param>
    /// <param name="i_PreSelectors">Pre Selectors to use</param>
    /// <param name="i_QueryModifier"></param>
    Task ApplyPreSelection(IReadonlyConversation i_Conversation, IConversationContext i_Context, IEnumerable<IQueryPreSelector> i_PreSelectors, Func<IQueryable<T>, ValueTask<IQueryable<T>>> i_QueryModifier);

    /// <summary>
    /// Count
    /// </summary>
    /// <returns></returns>
    int Count();

    /// <summary>
    /// Count
    /// </summary>
    /// <returns></returns>
    Task<int> CountAsync();

    /// <summary>
    /// To List
    /// </summary>
    /// <returns></returns>
    List<T> ToList();

    /// <summary>
    /// To List
    /// </summary>
    /// <returns></returns>
    Task<List<T>> ToListAsync();

    /// <summary>
    /// Applies a where condition to the query
    /// </summary>
    /// <param name="i_Where"></param>
    /// <returns></returns>
    PreSelectDependentQuery<T, TKey> Where(Expression<Func<T, bool>> i_Where);

    /// <summary>
    /// Applies a filter to the query
    /// </summary>
    /// <param name="i_Filter"></param>
    /// <param name="i_Options"></param>
    /// <returns></returns>
    PreSelectDependentQuery<T, TKey> FilterBy(INcFilter i_Filter, NcFilterTranslationOptions i_Options);

    /// <summary>
    /// Applies a fetch strategy to the query
    /// </summary>
    /// <param name="i_FetchStrategy"></param>
    /// <returns></returns>
    PreSelectDependentQuery<T, TKey> ApplyFetchStrategy(IFetchStrategy i_FetchStrategy);
  }
}