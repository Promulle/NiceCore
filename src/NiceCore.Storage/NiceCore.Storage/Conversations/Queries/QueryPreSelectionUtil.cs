﻿using NiceCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NiceCore.Storage.Conversations.Queries
{
  /// <summary>
  /// Static Helper class for Query pre selection
  /// </summary>
  public static class QueryPreSelectionUtil
  {
    /// <summary>
    /// Apply pre selections
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    /// <param name="i_QueryPreSelectors"></param>
    /// <param name="i_Conversation"></param>
    /// <param name="i_Context"></param>
    /// <param name="i_QueryModifier"></param>
    /// <returns></returns>
    public static async Task<IEnumerable<TKey>> PreSelect<TEntity, TKey>(IEnumerable<IQueryPreSelector> i_QueryPreSelectors,
      IReadonlyConversation i_Conversation,
      IConversationContext i_Context,
      Func<IQueryable<TEntity>, ValueTask<IQueryable<TEntity>>> i_QueryModifier) where TEntity : IDomainEntity<TKey>
    {
      IEnumerable<TKey> res = await GetKeys<TEntity, TKey>(i_Conversation, i_QueryModifier).ConfigureAwait(false);
      foreach (var preSelector in i_QueryPreSelectors.OrderBy(r => r.Priority).ToList())
      {
        res = await preSelector.PreSelect<TEntity, TKey>(res, i_Conversation, i_Context).ConfigureAwait(false);
      }
      return res;
    }

    private static async Task<IEnumerable<TKey>> GetKeys<TEntity, TKey>(IReadonlyConversation i_Conversation, Func<IQueryable<TEntity>, ValueTask<IQueryable<TEntity>>> i_QueryModifier) where TEntity : IDomainEntity<TKey>
    {
      var query = i_Conversation.Query<TEntity, TKey>();
      var modified = await i_QueryModifier.Invoke(query).ConfigureAwait(false);
      return await modified.Select(r => r.ID).ToListAsync()
        .ConfigureAwait(false);
    }
  }
}
