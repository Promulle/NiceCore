﻿using NiceCore.Entities;
using NiceCore.Filtering;
using NiceCore.Ordering;
using NiceCore.Storage.Fetching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace NiceCore.Storage.Conversations.Queries
{
  /// <summary>
  /// A query that is dependent on using a pre selection
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <typeparam name="TKey"></typeparam>
  public class PreSelectDependentQuery<T, TKey> : IPreSelectDependentQuery<T, TKey> where T : IDomainEntity<TKey>
  {
    private IQueryable<T> m_Query;

    /// <summary>
    /// AllowedIDs
    /// </summary>
    private HashSet<TKey> m_AllowedIDs;

    /// <summary>
    /// executing the pre select
    /// </summary>
    /// <param name="i_Conversation">Conversation to create/execute actual query</param>
    /// <param name="i_Context">Context for pre selection</param>
    /// <param name="i_PreSelectors">Pre Selectors to use</param>
    /// <param name="i_QueryModifier">Modifier applied to queries used by the dependent query</param>
    public async Task ApplyPreSelection(IReadonlyConversation i_Conversation, IConversationContext i_Context, IEnumerable<IQueryPreSelector> i_PreSelectors, Func<IQueryable<T>, ValueTask<IQueryable<T>>> i_QueryModifier)
    {
      var modifier = i_QueryModifier ?? (qry => ValueTask.FromResult(qry));
      m_AllowedIDs = (await QueryPreSelectionUtil.PreSelect<T, TKey>(i_PreSelectors, i_Conversation, i_Context, modifier).ConfigureAwait(false)).ToHashSet();
      m_Query = await modifier.Invoke(i_Conversation.Query<T, TKey>()).ConfigureAwait(false);
    }

    /// <summary>
    /// Applies a where condition to the query
    /// </summary>
    /// <param name="i_Where"></param>
    /// <returns></returns>
    public PreSelectDependentQuery<T, TKey> Where(Expression<Func<T, bool>> i_Where)
    {
      return ApplyOperationToQueries(qry => qry.Where(i_Where));
    }

    private PreSelectDependentQuery<T, TKey> ApplyOperationToQueries(Func<IQueryable<T>, IQueryable<T>> i_Func)
    {
      m_Query = i_Func(m_Query);
      return this;
    }

    /// <summary>
    /// Count
    /// </summary>
    /// <returns></returns>
    public int Count()
    {
      return ToList().Count(r => m_AllowedIDs.Contains(r.ID));
    }

    /// <summary>
    /// Count
    /// </summary>
    /// <returns></returns>
    public async Task<int> CountAsync()
    {
      var result = await ToListAsync().ConfigureAwait(false);
      return result.Count(r => m_AllowedIDs.Contains(r.ID));
    }

    /// <summary>
    /// To List
    /// </summary>
    /// <returns></returns>
    public List<T> ToList()
    {
      return m_Query.ToList().Where(r => m_AllowedIDs.Contains(r.ID)).ToList();
    }

    /// <summary>
    /// To List
    /// </summary>
    /// <returns></returns>
    public async Task<List<T>> ToListAsync()
    {
      var res = await m_Query.ToListAsync().ConfigureAwait(false);
      return res.Where(r => m_AllowedIDs.Contains(r.ID)).ToList();
    }

    /// <summary>
    /// Filter by
    /// </summary>
    /// <param name="i_Filter"></param>
    /// <param name="i_Options"></param>
    /// <returns></returns>
    public PreSelectDependentQuery<T, TKey> FilterBy(INcFilter i_Filter, NcFilterTranslationOptions i_Options)
    {
      ApplyOperationToQueries(qry => qry.FilterBy(i_Filter, i_Options));
      return this;
    }

    /// <summary>
    /// Apply Fetch Strategy
    /// </summary>
    /// <param name="i_FetchStrategy"></param>
    /// <returns></returns>
    public PreSelectDependentQuery<T, TKey> ApplyFetchStrategy(IFetchStrategy i_FetchStrategy)
    {
      ApplyOperationToQueries(qry => qry.ApplyFetchStrategy(i_FetchStrategy));
      return this;
    }

    /// <summary>
    /// Applies ordering
    /// </summary>
    /// <param name="i_Descriptor"></param>
    /// <returns></returns>
    public PreSelectDependentQuery<T, TKey> ApplySortBy(params NcOrderDescriptor[] i_Descriptor)
    {
      ApplyOperationToQueries(qry => qry.SortBy(i_Descriptor));
      return this;
    }
  }
}
