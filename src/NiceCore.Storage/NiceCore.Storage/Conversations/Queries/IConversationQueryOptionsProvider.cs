using System;
using NiceCore.Storage.Queries;

namespace NiceCore.Storage.Conversations.Queries
{
  /// <summary>
  /// Conversation Query Options Provider
  /// </summary>
  public interface IConversationQueryOptionsProvider
  {
    /// <summary>
    /// Get Options
    /// </summary>
    /// <returns></returns>
    INcQueryOptions GetOptions(Type i_QueryType);
  }
}