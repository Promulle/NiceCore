﻿using NiceCore.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NiceCore.Storage.Conversations.Queries
{
  /// <summary>
  /// Selector for pre filtering queries
  /// </summary>
  public interface IQueryPreSelector
  {
    /// <summary>
    /// Priority of the pre selector. Important if multiple selectors should be run in a specific order
    /// </summary>
    int Priority { get; }

    /// <summary>
    /// Pre select. Can use i_Keys if it should use an already filtered enumerable
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    /// <param name="i_Keys"></param>
    /// <param name="i_Conversation"></param>
    /// <param name="i_Context"></param>
    /// <returns></returns>
    Task<IEnumerable<TKey>> PreSelect<TEntity, TKey>(IEnumerable<TKey> i_Keys, IReadonlyConversation i_Conversation, IConversationContext i_Context) where TEntity : IDomainEntity<TKey>;
  }
}
