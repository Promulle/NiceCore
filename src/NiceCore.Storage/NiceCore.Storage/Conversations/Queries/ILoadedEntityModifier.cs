using NiceCore.Storage.Conversations.Flags;
using NiceCore.Storage.Helpers;

namespace NiceCore.Storage.Conversations.Queries
{
  /// <summary>
  /// Modifier for entities that were loaded
  /// </summary>
  public interface ILoadedEntityModifier
  {
    /// <summary>
    /// Whether an error thrown by this should be rethrown
    /// </summary>
    bool ThrowOnError { get; }

    /// <summary>
    /// Modifies i_Entity before an operation occurs
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <param name="i_Entity"></param>
    /// <param name="i_Flags"></param>
    /// <param name="i_Context"></param>
    /// <returns></returns>
    bool ModifyEntity<TEntity, TId>(IGenericEntityState<TEntity> i_Entity, ConversationOperationFlags i_Flags, IConversationContext i_Context) where TEntity : class;

    /// <summary>
    /// Modify but for non generic
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_Flags"></param>
    /// <param name="i_Context"></param>
    /// <returns></returns>
    bool ModifyNonGenericEntity(IGenericEntityState<object> i_Entity, ConversationOperationFlags i_Flags, IConversationContext i_Context);
  }
}