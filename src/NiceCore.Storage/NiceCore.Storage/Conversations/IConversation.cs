﻿using NiceCore.Base;
using System.Threading.Tasks;

namespace NiceCore.Storage.Conversations
{
  /// <summary>
  /// Conversation to save/query/change/delete data
  /// </summary>
  public interface IConversation : INotApplyingConversation, IBaseDisposable
  {
    /// <summary>
    /// Applies changes, flushes session
    /// </summary>
    ValueTask ApplyAsync();

    /// <summary>
    /// Restarts the conversation
    /// </summary>
    void Restart();

    /// <summary>
    /// Cancels conversation, does rollback
    /// </summary>
    ValueTask CancelAsync();
  }
}
