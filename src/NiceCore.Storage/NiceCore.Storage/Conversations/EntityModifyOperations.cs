﻿namespace NiceCore.Storage.Conversations
{
  /// <summary>
  /// Possible Modify Operations
  /// </summary>
  public enum EntityModifyOperations
  {
    /// <summary>
    /// Insert
    /// </summary>
    Insert = 0,

    /// <summary>
    /// Update
    /// </summary>
    Update = 1,
  }
}
