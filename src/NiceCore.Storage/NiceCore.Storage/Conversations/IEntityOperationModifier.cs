﻿using System.Collections.Generic;
using NiceCore.Storage.Conversations.Flags;
using NiceCore.Storage.Helpers;

namespace NiceCore.Storage.Conversations
{
  /// <summary>
  /// Modifier that is used to modify an entity before a certain operation
  /// </summary>
  public interface IEntityOperationModifier
  {
    /// <summary>
    /// Whether an error thrown by this should be rethrown
    /// </summary>
    bool ThrowOnError { get; }
    
    /// <summary>
    /// Operations this modifier should be used for
    /// </summary>
    IEnumerable<EntityModifyOperations> Operations { get; }

    /// <summary>
    /// Modify but for non generic
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_Flags"></param>
    /// <param name="i_Context"></param>
    /// <returns></returns>
    bool ModifyNonGenericEntity(IGenericEntityState<object> i_Entity, ConversationOperationFlags i_Flags, IConversationContext i_Context);
  }
}
