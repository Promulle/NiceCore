using System;
using System.Collections.Generic;
using System.Linq;
using NiceCore.Entities;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Queries;

namespace NiceCore.Storage
{
  /// <summary>
  /// Readonly Extensions Queries
  /// </summary>
  public static class IReadonlyConversationExtensions
  {
    /// <summary>
    /// Queries instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static IQueryable<T> Query<T>(this IReadonlyConversation i_Conversation) where T : IBaseDomainEntity
    {
      return Query<T>(i_Conversation, true);
    }

    /// <summary>
    /// Queries instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <returns></returns>
    public static IQueryable<T> Query<T, Tid>(this IReadonlyConversation i_Conversation) where T : IDomainEntity<Tid>
    {
      return i_Conversation.Query<T, Tid>(true, Array.Empty<IQueryLimiter>(), null);
    }
    
    /// <summary>
    /// Queries instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static IQueryable<T> Query<T>(this IReadonlyConversation i_Conversation, INcQueryOptions i_Options) where T : IBaseDomainEntity
    {
      return Query<T>(i_Conversation, true, i_Options);
    }

    /// <summary>
    /// Queries instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <returns></returns>
    public static IQueryable<T> Query<T, Tid>(this IReadonlyConversation i_Conversation, INcQueryOptions i_Options) where T : IDomainEntity<Tid>
    {
      return i_Conversation.Query<T, Tid>(true, Array.Empty<IQueryLimiter>(), i_Options);
    }

    /// <summary>
    /// Queries instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <returns></returns>
    public static IQueryable<T> Query<T, Tid>(this IReadonlyConversation i_Conversation, IReadOnlyCollection<IQueryLimiter> i_QueryLimiters) where T : IDomainEntity<Tid>
    {
      return i_Conversation.Query<T, Tid>(true, i_QueryLimiters, null);
    }

    /// <summary>
    /// Queries instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static IQueryable<T> Query<T>(this IReadonlyConversation i_Conversation, IReadOnlyCollection<IQueryLimiter> i_QueryLimiters) where T : IBaseDomainEntity
    {
      return i_Conversation.Query<T, long>(true, i_QueryLimiters, null);
    }
    
    /// <summary>
    /// Queries instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <returns></returns>
    public static IQueryable<T> Query<T, Tid>(this IReadonlyConversation i_Conversation, IReadOnlyCollection<IQueryLimiter> i_QueryLimiters, INcQueryOptions i_Options) where T : IDomainEntity<Tid>
    {
      return i_Conversation.Query<T, Tid>(true, i_QueryLimiters, i_Options);
    }

    /// <summary>
    /// Queries instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static IQueryable<T> Query<T>(this IReadonlyConversation i_Conversation, IReadOnlyCollection<IQueryLimiter> i_QueryLimiters, INcQueryOptions i_Options) where T : IBaseDomainEntity
    {
      return i_Conversation.Query<T, long>(true, i_QueryLimiters, i_Options);
    }

    /// <summary>
    /// Queries instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static IQueryable<T> Query<T>(this IReadonlyConversation i_Conversation, bool i_ApplyDefaultLimiter) where T : IBaseDomainEntity
    {
      return i_Conversation.Query<T, long>(i_ApplyDefaultLimiter, Array.Empty<IQueryLimiter>(), null);
    }
    
    /// <summary>
    /// Queries instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static IQueryable<T> Query<T>(this IReadonlyConversation i_Conversation, bool i_ApplyDefaultLimiter, INcQueryOptions i_Options) where T : IBaseDomainEntity
    {
      return i_Conversation.Query<T, long>(i_ApplyDefaultLimiter, Array.Empty<IQueryLimiter>(), i_Options);
    }

    /// <summary>
    /// Queries instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    public static IQueryable<T> Query<T, TId>(this IReadonlyConversation i_Conversation, bool i_ApplyDefaultLimiter) where T : IDomainEntity<TId>
    {
      return i_Conversation.Query<T, TId>(i_ApplyDefaultLimiter, Array.Empty<IQueryLimiter>(), null);
    }
    
    /// <summary>
    /// Queries instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    public static IQueryable<T> Query<T, TId>(this IReadonlyConversation i_Conversation, bool i_ApplyDefaultLimiter, INcQueryOptions i_Options) where T : IDomainEntity<TId>
    {
      return i_Conversation.Query<T, TId>(i_ApplyDefaultLimiter, Array.Empty<IQueryLimiter>(), i_Options);
    }

    /// <summary>
    /// Queries instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static IQueryable<T> Query<T>(this IReadonlyConversation i_Conversation, bool i_ApplyDefaultLimiter, IReadOnlyCollection<IQueryLimiter> i_QueryLimiters) where T : IBaseDomainEntity
    {
      return i_Conversation.Query<T, long>(i_ApplyDefaultLimiter, i_QueryLimiters, null);
    }
    
    /// <summary>
    /// Queries instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static IQueryable<T> Query<T>(this IReadonlyConversation i_Conversation, bool i_ApplyDefaultLimiter, IReadOnlyCollection<IQueryLimiter> i_QueryLimiters, INcQueryOptions i_Options) where T : IBaseDomainEntity
    {
      return i_Conversation.Query<T, long>(i_ApplyDefaultLimiter, i_QueryLimiters, i_Options);
    }
  }
}