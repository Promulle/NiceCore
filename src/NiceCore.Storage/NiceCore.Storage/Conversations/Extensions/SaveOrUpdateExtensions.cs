﻿using NiceCore.Entities;
using NiceCore.Storage.Conversations;
using Polly;

namespace NiceCore.Storage
{
  /// <summary>
  /// Extensions for save or update
  /// </summary>
  public static class SaveOrUpdateExtensions
  {
    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    public static void SaveOrUpdate<T>(this IConversation t_Conversation, T i_Instance) where T : IBaseDomainEntity
    {
      t_Conversation.SaveOrUpdate<T, long>(i_Instance, null, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Config"></param>
    public static void SaveOrUpdate<T>(this IConversation t_Conversation, T i_Instance, IConversationOperationConfiguration i_Config) where T : IBaseDomainEntity
    {
      t_Conversation.SaveOrUpdate<T, long>(i_Instance, null, i_Config, null);
    }

    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    public static void SaveOrUpdate<T>(this IConversation t_Conversation, T i_Instance, ISyncPolicy i_Policy) where T : IBaseDomainEntity
    {
      t_Conversation.SaveOrUpdate<T, long>(i_Instance, i_Policy, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Config"></param>
    public static void SaveOrUpdate<T>(this IConversation t_Conversation, T i_Instance, ISyncPolicy i_Policy, IConversationOperationConfiguration i_Config) where T : IBaseDomainEntity
    {
      t_Conversation.SaveOrUpdate<T, long>(i_Instance, i_Policy, i_Config, null);
    }

    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    public static void SaveOrUpdate<T, Tid>(this IConversation t_Conversation, T i_Instance) where T : IDomainEntity<Tid>
    {
      t_Conversation.SaveOrUpdate<T, Tid>(i_Instance, null, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    public static void SaveOrUpdate<T, Tid>(this IConversation t_Conversation, T i_Instance, IConversationOperationConfiguration i_Config) where T : IDomainEntity<Tid>
    {
      t_Conversation.SaveOrUpdate<T, Tid>(i_Instance, null, i_Config, null);
    }
    
    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    public static void SaveOrUpdate<T>(this INotApplyingConversation t_Conversation, T i_Instance) where T : IBaseDomainEntity
    {
      t_Conversation.SaveOrUpdate<T, long>(i_Instance, null, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Config"></param>
    public static void SaveOrUpdate<T>(this INotApplyingConversation t_Conversation, T i_Instance, IConversationOperationConfiguration i_Config) where T : IBaseDomainEntity
    {
      t_Conversation.SaveOrUpdate<T, long>(i_Instance, null, i_Config, null);
    }

    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    public static void SaveOrUpdate<T>(this INotApplyingConversation t_Conversation, T i_Instance, ISyncPolicy i_Policy) where T : IBaseDomainEntity
    {
      t_Conversation.SaveOrUpdate<T, long>(i_Instance, i_Policy, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Config"></param>
    public static void SaveOrUpdate<T>(this INotApplyingConversation t_Conversation, T i_Instance, ISyncPolicy i_Policy, IConversationOperationConfiguration i_Config) where T : IBaseDomainEntity
    {
      t_Conversation.SaveOrUpdate<T, long>(i_Instance, i_Policy, i_Config, null);
    }

    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    public static void SaveOrUpdate<T, Tid>(this INotApplyingConversation t_Conversation, T i_Instance) where T : IDomainEntity<Tid>
    {
      t_Conversation.SaveOrUpdate<T, Tid>(i_Instance, null, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    public static void SaveOrUpdate<T, Tid>(this INotApplyingConversation t_Conversation, T i_Instance, IConversationOperationConfiguration i_Config) where T : IDomainEntity<Tid>
    {
      t_Conversation.SaveOrUpdate<T, Tid>(i_Instance, null, i_Config, null);
    }
  }
}
