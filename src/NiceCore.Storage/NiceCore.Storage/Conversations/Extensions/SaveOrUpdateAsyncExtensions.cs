﻿using NiceCore.Entities;
using NiceCore.Storage.Conversations;
using Polly;
using System.Threading.Tasks;

namespace NiceCore.Storage
{
  /// <summary>
  /// Extensions for save or update async
  /// </summary>
  public static class SaveOrUpdateAsyncExtensions
  {
    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    public static Task SaveOrUpdateAsync<T>(this IConversation t_Conversation, T i_Instance) where T : IBaseDomainEntity
    {
      return t_Conversation.SaveOrUpdateAsync<T, long>(i_Instance, null, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Config"></param>
    public static Task SaveOrUpdateAsync<T>(this IConversation t_Conversation, T i_Instance, IConversationOperationConfiguration i_Config) where T : IBaseDomainEntity
    {
      return t_Conversation.SaveOrUpdateAsync<T, long>(i_Instance, null, i_Config, null);
    }

    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    public static Task SaveOrUpdateAsync<T>(this IConversation t_Conversation, T i_Instance, IAsyncPolicy i_Policy) where T : IBaseDomainEntity
    {
      return t_Conversation.SaveOrUpdateAsync<T, long>(i_Instance, i_Policy, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Config"></param>
    public static Task SaveOrUpdateAsync<T>(this IConversation t_Conversation, T i_Instance, IAsyncPolicy i_Policy, IConversationOperationConfiguration i_Config) where T : IBaseDomainEntity
    {
      return t_Conversation.SaveOrUpdateAsync<T, long>(i_Instance, i_Policy, i_Config, null);
    }

    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    public static Task SaveOrUpdateAsync<T, Tid>(this IConversation t_Conversation, T i_Instance) where T : IDomainEntity<Tid>
    {
      return t_Conversation.SaveOrUpdateAsync<T, Tid>(i_Instance, null, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Config"></param>
    public static Task SaveOrUpdateAsync<T, Tid>(this IConversation t_Conversation, T i_Instance, IConversationOperationConfiguration i_Config) where T : IDomainEntity<Tid>
    {
      return t_Conversation.SaveOrUpdateAsync<T, Tid>(i_Instance, null, i_Config, null);
    }
    
    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    public static Task SaveOrUpdateAsync<T>(this INotApplyingConversation t_Conversation, T i_Instance) where T : IBaseDomainEntity
    {
      return t_Conversation.SaveOrUpdateAsync<T, long>(i_Instance, null, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Config"></param>
    public static Task SaveOrUpdateAsync<T>(this INotApplyingConversation t_Conversation, T i_Instance, IConversationOperationConfiguration i_Config) where T : IBaseDomainEntity
    {
      return t_Conversation.SaveOrUpdateAsync<T, long>(i_Instance, null, i_Config, null);
    }

    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    public static Task SaveOrUpdateAsync<T>(this INotApplyingConversation t_Conversation, T i_Instance, IAsyncPolicy i_Policy) where T : IBaseDomainEntity
    {
      return t_Conversation.SaveOrUpdateAsync<T, long>(i_Instance, i_Policy, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Config"></param>
    public static Task SaveOrUpdateAsync<T>(this INotApplyingConversation t_Conversation, T i_Instance, IAsyncPolicy i_Policy, IConversationOperationConfiguration i_Config) where T : IBaseDomainEntity
    {
      return t_Conversation.SaveOrUpdateAsync<T, long>(i_Instance, i_Policy, i_Config, null);
    }

    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    public static Task SaveOrUpdateAsync<T, Tid>(this INotApplyingConversation t_Conversation, T i_Instance) where T : IDomainEntity<Tid>
    {
      return t_Conversation.SaveOrUpdateAsync<T, Tid>(i_Instance, null, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Saves or Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Config"></param>
    public static Task SaveOrUpdateAsync<T, Tid>(this INotApplyingConversation t_Conversation, T i_Instance, IConversationOperationConfiguration i_Config) where T : IDomainEntity<Tid>
    {
      return t_Conversation.SaveOrUpdateAsync<T, Tid>(i_Instance, null, i_Config, null);
    }

    /// <summary>
    /// Save Or Update Async but without policy support. Hopefully higher performance than normal
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="t_Conversation"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    public static ValueTask SaveOrUpdateAsyncRaw<T, TId>(this INotApplyingConversation t_Conversation, T i_Instance) where T : IDomainEntity<TId>
    {
      return t_Conversation.SaveOrUpdateAsyncRaw<T, TId>(i_Instance, null);
    }
  }
}
