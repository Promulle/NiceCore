﻿using NiceCore.Entities;
using NiceCore.Storage.Conversations;
using Polly;

namespace NiceCore.Storage
{
  /// <summary>
  /// Extensions for delete
  /// </summary>
  public static class DeleteExtensions
  {
    /// <summary>
    /// Deletes an Instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    public static void Delete<T>(this IConversation t_Conversation, T i_Instance) where T : IBaseDomainEntity
    {
      t_Conversation.Delete<T, long>(i_Instance, null, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Deletes an Instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Config"></param>
    public static void Delete<T>(this IConversation t_Conversation, T i_Instance, IConversationOperationConfiguration i_Config) where T : IBaseDomainEntity
    {
      t_Conversation.Delete<T, long>(i_Instance, null, i_Config, null);
    }

    /// <summary>
    /// Deletes an Instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    public static void Delete<T>(this IConversation t_Conversation, T i_Instance, ISyncPolicy i_Policy) where T : IBaseDomainEntity
    {
      t_Conversation.Delete<T, long>(i_Instance, i_Policy, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Deletes an Instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Config"></param>
    public static void Delete<T>(this IConversation t_Conversation, T i_Instance, ISyncPolicy i_Policy, IConversationOperationConfiguration i_Config) where T : IBaseDomainEntity
    {
      t_Conversation.Delete<T, long>(i_Instance, i_Policy, i_Config, null);
    }

    /// <summary>
    /// Deletes an Instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    public static void Delete<T, Tid>(this IConversation t_Conversation, T i_Instance) where T : IDomainEntity<Tid>
    {
      t_Conversation.Delete<T, Tid>(i_Instance, null, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Deletes an Instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Config"></param>
    public static void Delete<T, Tid>(this IConversation t_Conversation, T i_Instance, IConversationOperationConfiguration i_Config) where T : IDomainEntity<Tid>
    {
      t_Conversation.Delete<T, Tid>(i_Instance, null, i_Config, null);
    }

    /// <summary>
    /// Deletes every entry for T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public static void Delete<T>(this IConversation t_Conversation) where T : IBaseDomainEntity
    {
      t_Conversation.Delete<T, long>(ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Deletes every entry for T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public static void Delete<T>(this IConversation t_Conversation, IConversationOperationConfiguration i_Config) where T : IBaseDomainEntity
    {
      t_Conversation.Delete<T, long>(i_Config, null);
    }
  }
}
