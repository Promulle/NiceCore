﻿using NiceCore.Entities;
using NiceCore.Storage.Conversations;
using Polly;
using System.Threading.Tasks;

namespace NiceCore.Storage
{
  /// <summary>
  /// Extensions for delete async
  /// </summary>
  public static class DeleteAsyncExtensions
  {
    /// <summary>
    /// Deletes an Instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    public static Task DeleteAsync<T>(this IConversation t_Conversation, T i_Instance) where T : IBaseDomainEntity
    {
      return t_Conversation.DeleteAsync<T, long>(i_Instance, null, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Deletes an Instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Config"></param>
    public static Task DeleteAsync<T>(this IConversation t_Conversation, T i_Instance, IConversationOperationConfiguration i_Config) where T : IBaseDomainEntity
    {
      return t_Conversation.DeleteAsync<T, long>(i_Instance, null, i_Config, null);
    }

    /// <summary>
    /// Deletes an Instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    public static Task DeleteAsync<T>(this IConversation t_Conversation, T i_Instance, IAsyncPolicy i_Policy) where T : IBaseDomainEntity
    {
      return t_Conversation.DeleteAsync<T, long>(i_Instance, i_Policy, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Deletes an Instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Config"></param>
    public static Task DeleteAsync<T>(this IConversation t_Conversation, T i_Instance, IAsyncPolicy i_Policy, IConversationOperationConfiguration i_Config) where T : IBaseDomainEntity
    {
      return t_Conversation.DeleteAsync<T, long>(i_Instance, i_Policy, i_Config, null);
    }

    /// <summary>
    /// Deletes an Instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    public static Task DeleteAsync<T, Tid>(this IConversation t_Conversation, T i_Instance) where T : IDomainEntity<Tid>
    {
      return t_Conversation.DeleteAsync<T, Tid>(i_Instance, null, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Deletes an Instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Config"></param>
    public static Task DeleteAsync<T, Tid>(this IConversation t_Conversation, T i_Instance, IConversationOperationConfiguration i_Config) where T : IDomainEntity<Tid>
    {
      return t_Conversation.DeleteAsync<T, Tid>(i_Instance, null, i_Config, null);
    }

    /// <summary>
    /// Deletes every entry for T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public static Task DeleteAsync<T>(this IConversation t_Conversation) where T : IBaseDomainEntity
    {
      return t_Conversation.DeleteAsync<T, long>(ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Deletes every entry for T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Config"></param>
    public static Task DeleteAsync<T>(this IConversation t_Conversation, IConversationOperationConfiguration i_Config) where T : IBaseDomainEntity
    {
      return t_Conversation.DeleteAsync<T, long>(i_Config, null);
    }
    
    /// <summary>
    /// Deletes an Instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    public static Task DeleteAsync<T>(this INotApplyingConversation t_Conversation, T i_Instance) where T : IBaseDomainEntity
    {
      return t_Conversation.DeleteAsync<T, long>(i_Instance, null, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Deletes an Instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Config"></param>
    public static Task DeleteAsync<T>(this INotApplyingConversation t_Conversation, T i_Instance, IConversationOperationConfiguration i_Config) where T : IBaseDomainEntity
    {
      return t_Conversation.DeleteAsync<T, long>(i_Instance, null, i_Config, null);
    }

    /// <summary>
    /// Deletes an Instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    public static Task DeleteAsync<T>(this INotApplyingConversation t_Conversation, T i_Instance, IAsyncPolicy i_Policy) where T : IBaseDomainEntity
    {
      return t_Conversation.DeleteAsync<T, long>(i_Instance, i_Policy, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Deletes an Instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Config"></param>
    public static Task DeleteAsync<T>(this INotApplyingConversation t_Conversation, T i_Instance, IAsyncPolicy i_Policy, IConversationOperationConfiguration i_Config) where T : IBaseDomainEntity
    {
      return t_Conversation.DeleteAsync<T, long>(i_Instance, i_Policy, i_Config, null);
    }

    /// <summary>
    /// Deletes an Instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    public static Task DeleteAsync<T, Tid>(this INotApplyingConversation t_Conversation, T i_Instance) where T : IDomainEntity<Tid>
    {
      return t_Conversation.DeleteAsync<T, Tid>(i_Instance, null, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Deletes an Instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Config"></param>
    public static Task DeleteAsync<T, Tid>(this INotApplyingConversation t_Conversation, T i_Instance, IConversationOperationConfiguration i_Config) where T : IDomainEntity<Tid>
    {
      return t_Conversation.DeleteAsync<T, Tid>(i_Instance, null, i_Config, null);
    }

    /// <summary>
    /// Deletes every entry for T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public static Task DeleteAsync<T>(this INotApplyingConversation t_Conversation) where T : IBaseDomainEntity
    {
      return t_Conversation.DeleteAsync<T, long>(ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Deletes every entry for T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Config"></param>
    public static Task DeleteAsync<T>(this INotApplyingConversation t_Conversation, IConversationOperationConfiguration i_Config) where T : IBaseDomainEntity
    {
      return t_Conversation.DeleteAsync<T, long>(i_Config, null);
    }
  }
}
