﻿using NiceCore.Entities;
using NiceCore.Storage.Conversations;
using Polly;
using System.Threading.Tasks;

namespace NiceCore.Storage
{
  /// <summary>
  /// Extensions for update async
  /// </summary>
  public static class UpdateAsyncExtensions
  {
    /// <summary>
    /// Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    public static Task UpdateAsync<T>(this IConversation t_Conversation, T i_Instance) where T : IBaseDomainEntity
    {
      return t_Conversation.UpdateAsync<T, long>(i_Instance, null, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Config"></param>
    public static Task UpdateAsync<T>(this IConversation t_Conversation, T i_Instance, IConversationOperationConfiguration i_Config) where T : IBaseDomainEntity
    {
      return t_Conversation.UpdateAsync<T, long>(i_Instance, null, i_Config, null);
    }

    /// <summary>
    /// Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    public static Task UpdateAsync<T>(this IConversation t_Conversation, T i_Instance, IAsyncPolicy i_Policy) where T : IBaseDomainEntity
    {
      return t_Conversation.UpdateAsync<T, long>(i_Instance, i_Policy, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Config"></param>
    public static Task UpdateAsync<T>(this IConversation t_Conversation, T i_Instance, IAsyncPolicy i_Policy, IConversationOperationConfiguration i_Config) where T : IBaseDomainEntity
    {
      return t_Conversation.UpdateAsync<T, long>(i_Instance, i_Policy, i_Config, null);
    }

    /// <summary>
    /// Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    public static Task UpdateAsync<T, Tid>(this IConversation t_Conversation, T i_Instance) where T : IDomainEntity<Tid>
    {
      return t_Conversation.UpdateAsync<T, Tid>(i_Instance, null, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Config"></param>
    public static Task UpdateAsync<T, Tid>(this IConversation t_Conversation, T i_Instance, IConversationOperationConfiguration i_Config) where T : IDomainEntity<Tid>
    {
      return t_Conversation.UpdateAsync<T, Tid>(i_Instance, null, i_Config, null);
    }
  }
}
