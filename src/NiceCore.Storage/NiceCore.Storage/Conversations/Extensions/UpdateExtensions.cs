﻿using NiceCore.Entities;
using NiceCore.Storage.Conversations;
using Polly;

namespace NiceCore.Storage
{
  /// <summary>
  /// Update extension methods for IConversation
  /// </summary>
  public static class UpdateExtensions
  {
    /// <summary>
    /// Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    public static void Update<T>(this IConversation t_Conversation, T i_Instance) where T : IBaseDomainEntity
    {
      t_Conversation.Update<T, long>(i_Instance, null, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Config"></param>
    public static void Update<T>(this IConversation t_Conversation, T i_Instance, IConversationOperationConfiguration i_Config) where T : IBaseDomainEntity
    {
      t_Conversation.Update<T, long>(i_Instance, null, i_Config, null);
    }

    /// <summary>
    /// Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    public static void Update<T>(this IConversation t_Conversation, T i_Instance, ISyncPolicy i_Policy) where T : IBaseDomainEntity
    {
      t_Conversation.Update<T, long>(i_Instance, i_Policy, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Config"></param>
    public static void Update<T>(this IConversation t_Conversation, T i_Instance, ISyncPolicy i_Policy, IConversationOperationConfiguration i_Config) where T : IBaseDomainEntity
    {
      t_Conversation.Update<T, long>(i_Instance, i_Policy, i_Config, null);
    }

    /// <summary>
    /// Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    public static void Update<T, Tid>(this IConversation t_Conversation, T i_Instance) where T : IDomainEntity<Tid>
    {
      t_Conversation.Update<T, Tid>(i_Instance, null, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }

    /// <summary>
    /// Updates instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="t_Conversation"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_Config"></param>
    public static void Update<T, Tid>(this IConversation t_Conversation, T i_Instance, IConversationOperationConfiguration i_Config) where T : IDomainEntity<Tid>
    {
      t_Conversation.Update<T, Tid>(i_Instance, null, i_Config, null);
    }
  }
}
