using System.Threading.Tasks;
using NiceCore.Entities;
using NiceCore.Storage.Conversations;

namespace NiceCore.Storage
{
  /// <summary>
  /// Save Or Update Async Extensions
  /// </summary>
  public static class StatelessSaveOrUpdateAsyncExtensions
  {
    /// <summary>
    /// Update Async
    /// </summary>
    /// <param name="i_StatelessConversation"></param>
    /// <param name="i_Entity"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    public static ValueTask SaveOrUpdateAsync<T, TId>(this IStatelessConversation i_StatelessConversation, T i_Entity)
      where T : IDomainEntity<TId>
    {
      return i_StatelessConversation.SaveOrUpdateAsync<T, TId>(i_Entity, ConversationOperationConfig.DEFAULT_INSTANCE, null);
    }
    
    /// <summary>
    /// Update Async
    /// </summary>
    /// <param name="i_StatelessConversation"></param>
    /// <param name="i_Entity"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    public static ValueTask SaveOrUpdateNoValidationAsync<T, TId>(this IStatelessConversation i_StatelessConversation, T i_Entity)
      where T : IDomainEntity<TId>
    {
      return i_StatelessConversation.SaveOrUpdateAsync<T, TId>(i_Entity, ConversationOperationConfig.CreateNoValidationConfig(), null);
    }
  }
}