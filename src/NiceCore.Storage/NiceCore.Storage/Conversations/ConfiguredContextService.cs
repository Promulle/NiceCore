using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using NiceCore.Storage.Conversations.Config;

namespace NiceCore.Storage.Conversations
{
  /// <summary>
  /// Configured Context Service
  /// </summary>
  [Register(InterfaceType = typeof(IConfiguredContextService))]
  public class ConfiguredContextService : IConfiguredContextService
  {
    /// <summary>
    /// Configuration
    /// </summary>
    public INcConfiguration Configuration { get; set; }

    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<ConfiguredContextService> Logger { get; set; }

    /// <summary>
    /// Get Configured Context
    /// </summary>
    /// <param name="i_Key"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public T GetConfiguredContext<T>(string i_Key)
      where T : class
    {
      var typeKey = i_Key + ".Type";
      var valueKey = i_Key + ".Value";
      var typeIdent = Configuration.GetValue<string>(typeKey, null);
      if (typeIdent == null)
      {
        return null;
      }

      var type = Type.GetType(typeIdent);
      if (type == null)
      {
        Logger.Error($"Default Conversation Context was defined but type {typeIdent} could not be instantiated. Returning default parameter instead");
        return null;
      }

      var context = Configuration.GetValue<T>(valueKey, type, null);
      if (context == null)
      {
        Logger.Error("Default Conversation Context was defined but reading value returned null. Returning default parameter instead");
        return null;
      }

      return context;
    }

    /// <summary>
    /// Get Configured Context
    /// </summary>
    /// <returns></returns>
    public IConversationContext GetConfiguredDefaultContext()
    {
      return GetConfiguredContext<IConversationContext>(DefaultConversationContextSetting.KEY);
    }

    /// <summary>
    /// Get Available Configured Contexts
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public IReadOnlyCollection<T> GetAvailableConfiguredContexts<T>()
    {
      return Configuration.GetCollection<T>(AvailableConversationContextsSetting.KEY);
    }
  }
}