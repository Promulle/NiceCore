﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NiceCore.Entities;
using NiceCore.Storage.Connections;
using NiceCore.Storage.Conversations.Flags;
using NiceCore.Storage.Conversations.Queries;
using NiceCore.Storage.Limiters;
using NiceCore.Storage.Queries;

namespace NiceCore.Storage.Conversations
{
  /// <summary>
  /// Interface for 
  /// </summary>
  public interface IReadonlyConversation: IDisposable
  {
    /// <summary>
    /// Flags to configure operations in this conversation
    /// </summary>
    ConversationOperationFlags Flags { get; }
    
    /// <summary>
    /// Query Limiter Support
    /// </summary>
    QueryLimiterSupport QueryLimiterSupport { get; }

    /// <summary>
    /// Context with additional data for this conversation
    /// </summary>
    IConversationContext Context { get; }

    /// <summary>
    /// Support enabling easier queries when not in a generic context
    /// </summary>
    IUntypedConversationSupport Untyped { get; }
    
    /// <summary>
    /// Connection Name
    /// </summary>
    string ConnectionName { get; }
    
    /// <summary>
    /// Connection Info
    /// </summary>
    IConnectionInfo ConnectionInfo { get; }

    /// <summary>
    /// gets the identifier for the conversation
    /// </summary>
    /// <returns></returns>
    Guid? GetIdentifier();

    /// <summary>
    /// Sets options for queries of this conversation
    /// </summary>
    /// <param name="i_Options"></param>
    void UseOptions(INcQueryOptions i_Options);

    /// <summary>
    /// Queries instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <returns></returns>
    IQueryable<T> Query<T, Tid>(bool i_ApplyDefaultLimiter, IReadOnlyCollection<IQueryLimiter> i_CustomLimiter, INcQueryOptions i_Options) where T : IDomainEntity<Tid>;

    /// <summary>
    /// Creates a query that depends on preselected data and executes the preselection. (hence this method returning a task)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <param name="i_PreSelectors"></param>
    /// <param name="i_Context"></param>
    /// <param name="i_QueryModifier">Query Modifier used for internal queries of the dependent query. May be null</param>
    /// <returns></returns>
    Task<IPreSelectDependentQuery<T, TId>> DependentQuery<T, TId>(IEnumerable<IQueryPreSelector> i_PreSelectors, IConversationContext i_Context, Func<IQueryable<T>, ValueTask<IQueryable<T>>> i_QueryModifier) where T : IDomainEntity<TId>;
  }
}