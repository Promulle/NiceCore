using System.Threading;
using System.Threading.Tasks;
using NiceCore.Eventing.Messaging.Parameters;
using NiceCore.Storage.Messages;

namespace NiceCore.Storage.Conversations.Factories.Extensions
{
  /// <summary>
  /// Extensions for Creating Conversations from ConversationFactory
  /// </summary>
  public static class ConversationFactoryConversationExtensions
  {
    /// <summary>
    /// Create a new Conversation
    /// </summary>
    /// <param name="i_Factory"></param>
    /// <param name="i_Name"></param>
    /// <param name="i_MessageSender"></param>
    /// <param name="i_SendingParameters"></param>
    /// <param name="i_CreationRetryStrategy"></param>
    /// <param name="i_Context"></param>
    /// <param name="i_TransactionHandlingMode"></param>
    /// <param name="i_ConversationRetryStrategy"></param>
    /// <returns></returns>
    public static ValueTask<IConversation> Conversation(this IConversationFactory i_Factory,
      string i_Name,
      IStorageMessageSender i_MessageSender = null,
      IMessageSendingParameters i_SendingParameters = null,
      IDefaultConnectionRetryStrategy i_ConversationRetryStrategy = null,
      IDefaultConnectionRetryStrategy i_CreationRetryStrategy = null,
      IConversationContext i_Context = null,
      TransactionHandlingMode i_TransactionHandlingMode = TransactionHandlingMode.Explicit
    )
    {
      var parameters = new ConversationCreationParameters()
      {
        Name = i_Name,
        StorageMessageSender = i_MessageSender,
        MessageSendingParameters = i_SendingParameters,
        BatchSizeForStateless = null,
        ConversationConnectionRetryStrategy = i_ConversationRetryStrategy,
        CreationRetryStrategy = i_CreationRetryStrategy,
        Context = i_Context,
        TransactionHandlingMode = i_TransactionHandlingMode
      };
      return i_Factory.CreateNewConversation(parameters, CancellationToken.None);
    }
  }
}