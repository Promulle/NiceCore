using NiceCore.Eventing.Messaging.Parameters;
using NiceCore.Storage.Messages;

namespace NiceCore.Storage.Conversations.Factories
{
  /// <summary>
  /// Conversation Creation Parameters
  /// </summary>
  public class ConversationCreationParameters
  {
    /// <summary>
    /// Name
    /// </summary>
    public required string Name { get; init; }

    /// <summary>
    /// Context
    /// </summary>
    public IConversationContext Context { get; init; }
    
    /// <summary>
    /// Transaction Handling Mode
    /// </summary>
    public TransactionHandlingMode TransactionHandlingMode { get; init; }

    /// <summary>
    /// Message Context
    /// </summary>
    public IMessageSendingParameters MessageSendingParameters { get; init; }
    
    /// <summary>
    /// Default Connection Retry Strategy
    /// </summary>
    public IDefaultConnectionRetryStrategy ConversationConnectionRetryStrategy { get; init; }
    
    /// <summary>
    /// Retry Strategy used to create conversation
    /// </summary>
    public IDefaultConnectionRetryStrategy CreationRetryStrategy { get; init; }
    
    /// <summary>
    /// Storage Message Sender
    /// </summary>
    public IStorageMessageSender StorageMessageSender { get; init; }
    
    /// <summary>
    /// Batch size set when this object is used to create stateless conversations
    /// </summary>
    public int? BatchSizeForStateless { get; init; }
    
    /// <summary>
    /// For Name
    /// </summary>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    public static ConversationCreationParameters ForName(string i_Name)
    {
      return new()
      {
        Name = i_Name
      };
    }

    /// <summary>
    /// For Name
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Parameters"></param>
    /// <returns></returns>
    public static ConversationCreationParameters ForNameAndMessageParameters(string i_Name, IMessageSendingParameters i_Parameters)
    {
      return new()
      {
        Name = i_Name,
        MessageSendingParameters = i_Parameters
      };
    }

/// <summary>
    /// For Name
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_BatchSize">BatchSize used for Stateless Conversations</param>
    /// <returns></returns>
    public static ConversationCreationParameters ForNameAndBatchSize(string i_Name, int i_BatchSize)
    {
      return new()
      {
        Name = i_Name,
        BatchSizeForStateless = i_BatchSize
      };
    }
    
    /// <summary>
    /// For Name
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_StorageMessageSender"></param>
    /// <returns></returns>
    public static ConversationCreationParameters ForNameAndMessageSender(string i_Name, IStorageMessageSender i_StorageMessageSender)
    {
      return new()
      {
        Name = i_Name,
        StorageMessageSender = i_StorageMessageSender
      };
    }

    /// <summary>
    /// For Name
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Context"></param>
    /// <returns></returns>
    public static ConversationCreationParameters ForNameAndContext(string i_Name, IConversationContext i_Context)
    {
      return new()
      {
        Name = i_Name,
        Context = i_Context
      };
    }

    /// <summary>
    /// For Name And Context And BatchSize
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Context"></param>
    /// <param name="i_BatchSize">BatchSize used for Stateless Conversations</param>
    /// <returns></returns>
    public static ConversationCreationParameters ForNameAndContextAndBatchSize(string i_Name, IConversationContext i_Context, int i_BatchSize)
    {
      return new()
      {
        Name = i_Name,
        Context = i_Context,
        BatchSizeForStateless = i_BatchSize
      };
    }

    /// <summary>
    /// For Name
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Context"></param>
    /// <param name="i_MessageSender"></param>
    /// <returns></returns>
    public static ConversationCreationParameters ForNameAndContextAndMessageSender(string i_Name, IConversationContext i_Context, IStorageMessageSender i_MessageSender)
    {
      return new()
      {
        Name = i_Name,
        Context = i_Context,
        StorageMessageSender = i_MessageSender
      };
    }

    /// <summary>
    /// For Name
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Context"></param>
    /// <param name="i_MessageContext"></param>
    /// <returns></returns>
    public static ConversationCreationParameters ForNameAndContextAndMessageParameters(string i_Name, IConversationContext i_Context, IMessageSendingParameters i_MessageContext)
    {
      return new()
      {
        Name = i_Name,
        Context = i_Context,
        MessageSendingParameters = i_MessageContext
      };
    }

    /// <summary>
    /// Copy With Context
    /// </summary>
    /// <param name="i_Original"></param>
    /// <param name="i_Context"></param>
    /// <returns></returns>
    public static ConversationCreationParameters CopyWithContext(ConversationCreationParameters i_Original, IConversationContext i_Context)
    {
      return new ConversationCreationParameters()
      {
        Name = i_Original.Name,
        TransactionHandlingMode = i_Original.TransactionHandlingMode,
        MessageSendingParameters = i_Original.MessageSendingParameters,
        ConversationConnectionRetryStrategy = i_Original.ConversationConnectionRetryStrategy,
        CreationRetryStrategy = i_Original.CreationRetryStrategy,
        StorageMessageSender = i_Original.StorageMessageSender,
        BatchSizeForStateless = i_Original.BatchSizeForStateless,
        Context = i_Context
      };
    }
  }
}