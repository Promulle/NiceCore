using System.Threading;
using System.Threading.Tasks;

namespace NiceCore.Storage.Conversations.Factories
{
  /// <summary>
  /// Conversation Factory
  /// </summary>
  public interface IConversationFactory
  { 
    /// <summary>
    /// Creates a new Conversation
    /// </summary>
    /// <param name="i_CreationParameters"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    ValueTask<IConversation> CreateNewConversation(ConversationCreationParameters i_CreationParameters, CancellationToken i_Token);

    /// <summary>
    /// Creates a new Conversation
    /// </summary>
    /// <param name="i_Parameters"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    ValueTask<IStatelessConversation> CreateNewStatelessConversation(ConversationCreationParameters i_Parameters, CancellationToken i_Token);

    /// <summary>
    /// Creates a new Conversation
    /// </summary>
    /// <param name="i_Parameters"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    ValueTask<IReadonlyConversation> CreateNewStatelessReadOnlyConversation(ConversationCreationParameters i_Parameters, CancellationToken i_Token);
  }
}