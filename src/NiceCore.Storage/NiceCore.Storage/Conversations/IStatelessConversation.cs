using System.Threading.Tasks;
using NiceCore.Entities;
using NiceCore.Services.Validation.Model;

namespace NiceCore.Storage.Conversations
{
  /// <summary>
  /// Interface for stateless conversation
  /// </summary>
  public interface IStatelessConversation : IReadonlyConversation
  {
    /// <summary>
    /// Save Or Update Async
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_Config"></param>
    /// <param name="i_Node"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    ValueTask SaveOrUpdateAsync<T, TId>(T i_Entity, IConversationOperationConfiguration i_Config, SubValidationNode i_Node) where T : IDomainEntity<TId>;

    /// <summary>
    /// Save Or Update Async
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_Config"></param>
    /// <param name="i_Node"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    ValueTask DeleteAsync<T, TId>(T i_Entity, IConversationOperationConfiguration i_Config, SubValidationNode i_Node) where T : IDomainEntity<TId>;

    /// <summary>
    /// Applies changes from conversation
    /// </summary>
    /// <returns></returns>
    ValueTask ApplyAsync();
  }
}