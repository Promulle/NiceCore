﻿namespace NiceCore.Storage.Conversations
{
  /// <summary>
  /// interface marking classes that have a conversation property
  /// </summary>
  public interface IHasConversation
  {
    /// <summary>
    /// Conversation
    /// </summary>
    IConversation Conversation { get; }
  }
}
