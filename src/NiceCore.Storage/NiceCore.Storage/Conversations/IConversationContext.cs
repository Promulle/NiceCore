﻿using NiceCore.ServiceLocation;
using NiceCore.Services.Validation;
using NiceCore.Services.Validation.Contexts;

namespace NiceCore.Storage.Conversations
{
  /// <summary>
  /// Context for data used by conversation that is passed to limiter/modifiers (later validators too.)
  /// </summary>
  public interface IConversationContext
  {
    /// <summary>
    /// Converts this conversation context to an instance of INcValidationContext. This is needed if you want to be able to validate based off the current conversation context
    /// when executing an operation in a conversation
    /// </summary>
    /// <param name="i_Conversation">The Conversation this conversion started from. Might be null.</param>
    /// <param name="i_ServiceContainer"></param>
    /// <returns></returns>
    INcCustomValidationProcessContext ToValidationContext(IConversation i_Conversation, IServiceContainer i_ServiceContainer);
  }
}
