using System.Collections.Generic;

namespace NiceCore.Storage.Conversations.Flags
{
  /// <summary>
  /// Store managing flags for a single conversation
  /// </summary>
  public sealed class ConversationOperationFlags
  {
    private readonly Dictionary<string, ConversationOperationFlag> m_Flags = new();
    
    /// <summary>
    /// Add a new flag. If a flag with the same name already exists, it will be overriden
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Value"></param>
    public void AddFlag(string i_Name, object i_Value)
    {
      m_Flags[i_Name] = new(i_Name, i_Value);
    }

    /// <summary>
    /// gets the value of type T for flag with name i_Name. Throws exceptions if flag is not found or type
    /// did not match (is operator returns nothing)
    /// </summary>
    /// <param name="i_Name"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public T GetFlagValue<T>(string i_Name)
    {
      if (m_Flags.TryGetValue(i_Name, out var flag))
      {
        if (flag.Value is T casted)
        {
          return casted;
        }

        if (flag.Value is null)
        {
          if (default(T) == null)
          {
            return default(T);
          }
          throw new FlagValueExpectedTypeNotNullableException(i_Name, typeof(T));
        }
        throw new FlagMismatchedTypeException(i_Name, typeof(T), flag.Value.GetType());
      }
      throw new FlagNotFoundException(i_Name, typeof(T));
    }

    /// <summary>
    /// gets the value of type T for Flag with name i_Name. Returns default(T) if flag is not found types do not match (is operator returns nothing) 
    /// </summary>
    /// <param name="i_Name"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public ConversationFlagResult<T> TryGetFlagValue<T>(string i_Name)
    {
      if (m_Flags.TryGetValue(i_Name, out var flag))
      {
        if (flag.Value is T casted)
        {
          return new()
          {
            Value = casted,
            Found = true
          };
        }
        return new()
        {
          Value = default,
          Found = true
        };
      }
      return new()
      {
        Value = default,
        Found = false
      };
    }
  }
}