using System;

namespace NiceCore.Storage.Conversations.Flags
{
  /// <summary>
  /// Exception thrown if value is null but the expected type is not nullable
  /// </summary>
  public sealed class FlagValueExpectedTypeNotNullableException : Exception
  {
    /// <summary>
    /// Flag name
    /// </summary>
    public string FlagName { get; }
    
    /// <summary>
    /// Expected Type
    /// </summary>
    public Type ExpectedType { get; }
    
    /// <summary>
    /// ctor filling values
    /// </summary>
    /// <param name="i_FlagName"></param>
    /// <param name="i_ExpectedType"></param>
    public FlagValueExpectedTypeNotNullableException(string i_FlagName, Type i_ExpectedType): base(GetMessage(i_FlagName, i_ExpectedType))
    {
      FlagName = i_FlagName;
      ExpectedType = i_ExpectedType;
    }
    
    private static string GetMessage(string i_Name, Type i_ExpectedType)
    {
      return $"Could not get value for flag: {i_Name}: Flag found but value is null and expected type {i_ExpectedType.FullName} is not nullable.";
    }
  }
}