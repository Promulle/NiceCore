namespace NiceCore.Storage.Conversations.Flags
{
  /// <summary>
  /// Result of the search for a Conversation Flag
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public struct ConversationFlagResult<T>
  {
    /// <summary>
    /// Value
    /// </summary>
    public T Value { get; init; }
    
    /// <summary>
    /// Whether the value was found or not
    /// </summary>
    public bool Found { get; init; }
  }
}