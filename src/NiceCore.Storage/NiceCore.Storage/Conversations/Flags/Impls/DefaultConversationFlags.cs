namespace NiceCore.Storage.Conversations.Flags.Impls
{
  /// <summary>
  /// Conversation Flags provided by NiceCore
  /// </summary>
  public static class DefaultConversationFlags
  {
    /// <summary>
    /// Flag that enables/disables Validation of Instances. Expected Value Type: bool
    /// </summary>
    public const string FLAG_VALIDATE_INSTANCES = "ValidateInstances";
  }
}