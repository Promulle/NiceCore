namespace NiceCore.Storage.Conversations.Flags
{
  /// <summary>
  /// Flag used to customize execution of conversation operations on a per conversation basis
  /// </summary>
  public sealed class ConversationOperationFlag
  {
    /// <summary>
    /// Name Identifying the flag. Used as Key, so should be Unique
    /// </summary>
    public string Name { get; }
    
    /// <summary>
    /// Value of the flag
    /// </summary>
    public object Value { get; }

    /// <summary>
    /// Constructor filling both needed values
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Value"></param>
    public ConversationOperationFlag(string i_Name, object i_Value)
    {
      Name = i_Name;
      Value = i_Value;
    }
  }
}