using System;

namespace NiceCore.Storage.Conversations.Flags
{
  /// <summary>
  /// Exception thrown if a flag was not found.
  /// </summary>
  public sealed class FlagNotFoundException : Exception
  {
    /// <summary>
    /// Expected Flag Name
    /// </summary>
    public string ExpectedFlagName { get; }

    /// <summary>
    /// ctor setting name
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_ExpectedType"></param>
    public FlagNotFoundException(string i_Name, Type i_ExpectedType): base(GetMessage(i_Name, i_ExpectedType))
    {
      ExpectedFlagName = i_Name;
    }

    private static string GetMessage(string i_Name, Type i_ExpectedType)
    {
      return $"Could not get value for flag: {i_Name} of type {i_ExpectedType.FullName}.";
    }
  }
}