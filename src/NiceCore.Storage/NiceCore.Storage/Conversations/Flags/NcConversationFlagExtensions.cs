using NiceCore.Storage.Conversations.Flags.Impls;

namespace NiceCore.Storage.Conversations.Flags
{
  /// <summary>
  /// NcConversationFlagExtensions
  /// </summary>
  public static class NcConversationFlagExtensions
  {
    /// <summary>
    /// Disable Validation
    /// </summary>
    /// <param name="i_Conversation"></param>
    public static void DisableValidation(this IReadonlyConversation i_Conversation)
    {
      i_Conversation.Flags.AddFlag(DefaultConversationFlags.FLAG_VALIDATE_INSTANCES, false);
    }

    /// <summary>
    /// Enable Validation
    /// </summary>
    /// <param name="i_Conversation"></param>
    public static void EnableValidation(this IReadonlyConversation i_Conversation)
    {
      i_Conversation.Flags.AddFlag(DefaultConversationFlags.FLAG_VALIDATE_INSTANCES, true);
    }
  }
}