using System;

namespace NiceCore.Storage.Conversations.Flags
{
  /// <summary>
  /// Exception thrown if the value for a flag was found but did not correspond to the expected type
  /// </summary>
  public sealed class FlagMismatchedTypeException : Exception
  {
    /// <summary>
    /// Name of flag
    /// </summary>
    public string FlagName { get; }
    
    /// <summary>
    /// Expected type
    /// </summary>
    public Type ExpectedFlagType { get; }
    
    /// <summary>
    /// Actual flag type
    /// </summary>
    public Type ActualFlagType { get; }
    
    /// <summary>
    /// Ctor setting values
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_ExpectedType"></param>
    /// <param name="i_ActualType"></param>
    public FlagMismatchedTypeException(string i_Name, Type i_ExpectedType, Type i_ActualType): base(GetMessage(i_Name, i_ExpectedType, i_ActualType))
    {
      FlagName = i_Name;
      ExpectedFlagType = i_ExpectedType;
      ActualFlagType = i_ActualType;
    }
    
    private static string GetMessage(string i_Name, Type i_ExpectedType, Type i_ActualType)
    {
      return $"Found flag: {i_Name} but its type ({i_ActualType.FullName}) did not match the expected type {i_ExpectedType.FullName}.";
    }
  }
}