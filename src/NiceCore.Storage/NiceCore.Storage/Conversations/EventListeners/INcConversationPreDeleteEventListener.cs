using System.Threading;
using System.Threading.Tasks;

namespace NiceCore.Storage.Conversations.EventListeners
{
  /// <summary>
  /// Nc Conversation EventListener
  /// </summary>
  public interface INcConversationPreDeleteEventListener : IConnectionNameBasedNcConversationEventListener
  {
    /// <summary>
    /// On Delete
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="t_Conversation"></param>
    /// <param name="i_CancellationToken"></param>
    /// <returns></returns>
    ValueTask PreDelete(object i_Entity, INotApplyingConversation t_Conversation, CancellationToken i_CancellationToken);
  }
}