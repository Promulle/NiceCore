using System.Threading;
using System.Threading.Tasks;

namespace NiceCore.Storage.Conversations.EventListeners
{
  /// <summary>
  /// Nc Conversation SaveOrUpdate Event Listener
  /// </summary>
  public interface INcConversationPreSaveOrUpdateEventListener : IConnectionNameBasedNcConversationEventListener
  {
    /// <summary>
    /// On SaveOrUpdate
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_ResultEntity"></param>
    /// <param name="t_Conversation"></param>
    /// <param name="i_CancellationToken"></param>
    /// <returns></returns>
    ValueTask PreSaveOrUpdate(object i_Entity, object i_ResultEntity, INotApplyingConversation t_Conversation, CancellationToken i_CancellationToken);
  }
}