using System.Threading;
using System.Threading.Tasks;

namespace NiceCore.Storage.Conversations.EventListeners
{
  /// <summary>
  /// Nc Conversation Delete Listener
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public abstract class BaseNcConversationPostDeleteListener<T> : BaseConnectionNameBasedNcConversationEventListener, INcConversationPostDeleteEventListener
  {
    /// <summary>
    /// On Delete
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="t_Conversation"></param>
    /// <param name="i_CancellationToken"></param>
    /// <returns></returns>
    public ValueTask PostDelete(object i_Entity, INotApplyingConversation t_Conversation, CancellationToken i_CancellationToken)
    {
      if (i_Entity is T casted)
      {
        return PostDelete(casted, t_Conversation, i_CancellationToken);
      }

      return ValueTask.CompletedTask;
    }

    /// <summary>
    /// On Delete
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="t_Conversation"></param>
    /// <param name="i_CancellationToken"></param>
    /// <returns></returns>
    protected abstract ValueTask PostDelete(T i_Entity, INotApplyingConversation t_Conversation, CancellationToken i_CancellationToken);
  }
}