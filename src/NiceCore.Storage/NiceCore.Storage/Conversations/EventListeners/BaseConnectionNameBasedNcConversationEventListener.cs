namespace NiceCore.Storage.Conversations.EventListeners
{
  /// <summary>
  /// Base Connection Name Based Conversation Event Listener
  /// </summary>
  public abstract class BaseConnectionNameBasedNcConversationEventListener : IConnectionNameBasedNcConversationEventListener
  {
    /// <summary>
    /// Can be used for connection
    /// </summary>
    /// <param name="i_ConnectionName"></param>
    /// <returns></returns>
    public virtual bool CanBeUsedForConnection(string i_ConnectionName)
    {
      return true;
    }
  }
}