using System.Threading;
using System.Threading.Tasks;

namespace NiceCore.Storage.Conversations.EventListeners
{
  /// <summary>
  /// Listener for Post-SaveOrUpdate Events
  /// </summary>
  public interface INcConversationPostSaveOrUpdateEventListener: IConnectionNameBasedNcConversationEventListener
  {
    /// <summary>
    /// On SaveOrUpdate
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_ResultEntity"></param>
    /// <param name="t_Conversation"></param>
    /// <param name="i_CancellationToken"></param>
    /// <returns></returns>
    ValueTask PostSaveOrUpdate(object i_Entity, object i_ResultEntity, INotApplyingConversation t_Conversation, CancellationToken i_CancellationToken);
  }
}