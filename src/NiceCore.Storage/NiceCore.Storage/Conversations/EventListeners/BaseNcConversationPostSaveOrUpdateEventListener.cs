using System.Threading;
using System.Threading.Tasks;

namespace NiceCore.Storage.Conversations.EventListeners
{
  /// <summary>
  /// Base Nc Conversation SaveOrUpdate
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public abstract class BaseNcConversationPostSaveOrUpdateEventListener<T> : BaseConnectionNameBasedNcConversationEventListener, INcConversationPostSaveOrUpdateEventListener
  {
    /// <summary>
    /// On SaveOrUpdate
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_ResultEntity"></param>
    /// <param name="t_Conversation"></param>
    /// <param name="i_CancellationToken"></param>
    /// <returns></returns>
    public ValueTask PostSaveOrUpdate(object i_Entity, object i_ResultEntity, INotApplyingConversation t_Conversation, CancellationToken i_CancellationToken)
    {
      if (i_Entity is T castedEntity)
      {
        return PostSaveOrUpdate(castedEntity, GetResultEntity(i_ResultEntity), t_Conversation, i_CancellationToken);
      }
      return ValueTask.CompletedTask;
    }

    private static T GetResultEntity(object i_ResultEntity)
    {
      if (i_ResultEntity != null && i_ResultEntity is T casted)
      {
        return casted;
      }
      return default;
    }

    /// <summary>
    /// On Save Or Update
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_ResultEntity"></param>
    /// <param name="t_Conversation"></param>
    /// <param name="i_CancellationToken"></param>
    /// <returns></returns>
    protected abstract ValueTask PostSaveOrUpdate(T i_Entity, T i_ResultEntity, INotApplyingConversation t_Conversation, CancellationToken i_CancellationToken);
  }
}