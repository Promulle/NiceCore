using System.Threading;
using System.Threading.Tasks;

namespace NiceCore.Storage.Conversations.EventListeners
{
  /// <summary>
  /// Base Nc Conversation SaveOrUpdate
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public abstract class BaseNcConversationPreSaveOrUpdateEventListener<T> : BaseConnectionNameBasedNcConversationEventListener, INcConversationPreSaveOrUpdateEventListener
  {
    /// <summary>
    /// On SaveOrUpdate
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_ResultEntity"></param>
    /// <param name="t_Conversation"></param>
    /// <param name="i_CancellationToken"></param>
    /// <returns></returns>
    public ValueTask PreSaveOrUpdate(object i_Entity, object i_ResultEntity, INotApplyingConversation t_Conversation, CancellationToken i_CancellationToken)
    {
      if (i_Entity is T castedEntity && i_ResultEntity is T castedResultEntity)
      {
        return PreSaveOrUpdate(castedEntity, castedResultEntity, t_Conversation, i_CancellationToken);
      }
      return ValueTask.CompletedTask;
    }

    /// <summary>
    /// On Save Or Update
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_ResultEntity"></param>
    /// <param name="t_Conversation"></param>
    /// <param name="i_CancellationToken"></param>
    /// <returns></returns>
    protected abstract ValueTask PreSaveOrUpdate(T i_Entity, T i_ResultEntity, INotApplyingConversation t_Conversation, CancellationToken i_CancellationToken);
  }
}