namespace NiceCore.Storage.Conversations.EventListeners
{
  /// <summary>
  /// Connection Name Based Conversation Event Listener
  /// </summary>
  public interface IConnectionNameBasedNcConversationEventListener
  {
    /// <summary>
    /// Whether the EventListener can be used for the given connection name
    /// </summary>
    /// <param name="i_ConnectionName"></param>
    /// <returns></returns>
    bool CanBeUsedForConnection(string i_ConnectionName);
  }
}