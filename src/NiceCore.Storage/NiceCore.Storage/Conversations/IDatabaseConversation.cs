﻿using Polly;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace NiceCore.Storage.Conversations
{
  /// <summary>
  /// Interface for conversations that load/save objects to databases
  /// </summary>
  public interface IDatabaseConversation : IConversation
  {
    /// <summary>
    /// gets the connection string of this IDatabaseConversation
    /// </summary>
    string ConnectionString { get; }

    /// <summary>
    /// Returns a command, useable for the connection of this conversation
    /// </summary>
    /// <param name="i_CommandText">Optional pre-fill of command text</param>
    /// <returns></returns>
    DbCommand GetDbCommand(string i_CommandText = null);

    /// <summary>
    /// Get Connection
    /// </summary>
    /// <returns></returns>
    IDbConnection GetConnection();

    /// <summary>
    /// Executes i_SqlStatement, as non-Query
    /// </summary>
    /// <param name="i_SqlStatement"></param>
    /// <returns></returns>
    int ExecSqlNonQuery(string i_SqlStatement);

    /// <summary>
    /// Executes i_SqlStatement, as non-Query
    /// </summary>
    /// <param name="i_SqlStatement"></param>
    /// <returns></returns>
    Task<int> ExecSqlNonQueryAsync(string i_SqlStatement);

    /// <summary>
    /// Executes i_SqlStatement, as non-Query
    /// </summary>
    /// <param name="i_SqlStatement"></param>
    /// <param name="i_Policy"></param>
    /// <returns></returns>
    int ExecSqlNonQuery(string i_SqlStatement, ISyncPolicy i_Policy);

    /// <summary>
    /// Executes i_SqlStatement, as non-Query
    /// </summary>
    /// <param name="i_SqlStatement"></param>
    /// <param name="i_Policy"></param>
    /// <returns></returns>
    Task<int> ExecSqlNonQueryAsync(string i_SqlStatement, IAsyncPolicy i_Policy);

    /// <summary>
    /// Execute an sql query
    /// </summary>
    /// <param name="i_SqlQueryStatement"></param>
    /// <returns></returns>
    Task<DbDataReader> ExecSqlQueryAsync(string i_SqlQueryStatement);
  }
}
