using System.Threading;
using System.Threading.Tasks;

namespace NiceCore.Storage.Conversations
{
  /// <summary>
  /// Interface for Conversations that are initialized by an initialize function
  /// </summary>
  /// <typeparam name="TInit"></typeparam>
  public interface IInitializableConversation<TInit>
  {
    /// <summary>
    /// Initialize
    /// </summary>
    /// <param name="i_Init"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    ValueTask Initialize(TInit i_Init, CancellationToken i_Token);
  }
}