using System.Collections.Generic;
using System.Threading.Tasks;

namespace NiceCore.Storage.Conversations.EventHooks
{
  /// <summary>
  /// Util for executing the BeforeAddToConversation Hooks
  /// </summary>
  public static class ConversationHooks
  {
    /// <summary>
    /// Runs all Hooks from i_Hooks for t_Instance with given conversation. Given Conversation should
    /// </summary>
    /// <param name="t_Instance"></param>
    /// <param name="i_Hooks"></param>
    /// <param name="i_Conversation"></param>
    /// <returns></returns>
    public static async ValueTask<bool> ExecuteAllBeforeAdd(object t_Instance, IEnumerable<IBeforeAddToConversationHook> i_Hooks, IConversation i_Conversation)
    {
      if (i_Hooks == null)
      {
        return true;
      }
      foreach (var hook in i_Hooks)
      {
        var canContinue = await hook.BeforeAdd(t_Instance, i_Conversation).ConfigureAwait(false);
        if (!canContinue)
        {
          return false;
        }
      }
      return true;
    }
  }
}