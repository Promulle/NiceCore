﻿using System.Threading.Tasks;

namespace NiceCore.Storage.Conversations.EventHooks
{
  /// <summary>
  /// Storage event hook
  /// </summary>
  public interface IBeforeAddToConversationHook
  {
    /// <summary>
    /// Whether this hook changes something about instances. Destructive Hooks are run later than non-destructive hooks
    /// </summary>
    bool IsDestructive { get; }
    
    /// <summary>
    /// Run the hook
    /// </summary>
    /// <param name="t_Object">object for what the operation was executed</param>
    /// <param name="t_Conversation">conversation which was used for the operation</param>
    /// <returns>Whether to continue with adding t_Object to the conversation</returns>
    ValueTask<bool> BeforeAdd(object t_Object, IConversation t_Conversation);
  }
}