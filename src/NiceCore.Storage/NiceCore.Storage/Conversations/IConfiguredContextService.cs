using System.Collections.Generic;

namespace NiceCore.Storage.Conversations
{
  /// <summary>
  /// Configured Context Service
  /// </summary>
  public interface IConfiguredContextService
  {
    /// <summary>
    /// Get Configured Context
    /// </summary>
    /// <param name="i_Key"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    T GetConfiguredContext<T>(string i_Key) where T : class;

    /// <summary>
    /// Get Configured Context
    /// </summary>
    /// <returns></returns>
    IConversationContext GetConfiguredDefaultContext();

    /// <summary>
    /// Get Available Configured Contexts
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    IReadOnlyCollection<T> GetAvailableConfiguredContexts<T>();
  }
}