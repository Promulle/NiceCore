namespace NiceCore.Storage.Conversations
{
  /// <summary>
  /// Mode for Handling transactions
  /// </summary>
  public enum TransactionHandlingMode
  {
    /// <summary>
    /// The system which is wrapped by the conversation is handling the transaction
    /// Supported: NHibernate
    /// </summary>
    Implicit = 0,
    
    /// <summary>
    /// The Conversation (NiceCore) handles the transaction
    /// Supported: NHibernate
    /// </summary>
    Explicit = 1,
    
    /// <summary>
    /// None is not supported with all providers
    /// NotSupported: NHibernate -> will pick Explicit
    /// </summary>
    None = 2
  }
}