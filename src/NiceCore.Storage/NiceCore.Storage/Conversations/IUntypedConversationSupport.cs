using System;
using System.Threading.Tasks;

namespace NiceCore.Storage.Conversations
{
  /// <summary>
  /// Interface for classes enabling untyped queries
  /// </summary>
  public interface IUntypedConversationSupport
  {
    /// <summary>
    /// Queries an entity of i_Type for i_Id
    /// </summary>
    /// <param name="i_Type"></param>
    /// <param name="i_Id"></param>
    /// <returns></returns>
    Task<object> GetById(Type i_Type, object i_Id);
  }
}
