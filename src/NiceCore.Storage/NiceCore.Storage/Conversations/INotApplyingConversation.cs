﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NiceCore.Entities;
using NiceCore.Services.Validation.Model;
using NiceCore.Storage.Cascading;
using NiceCore.Storage.Conversations.Queries;
using NiceCore.Storage.Messages;
using Polly;

namespace NiceCore.Storage.Conversations
{
  /// <summary>
  /// Conversation that implements all operations but can not call apply -> this usually means that the conversation object was passed to
  /// a service that manipulates data but is not responsible for calling apply  
  /// </summary>
  public interface INotApplyingConversation : IReadOnlyAuditableConversation
  {
    /// <summary>
    /// Enables awaiting messages from this conversation using i_Awaiter
    /// </summary>
    /// <param name="i_Awaiter"></param>
    void EnableMessageAwaiting(IMessageAwaiter i_Awaiter);
    
    /// <summary>
    /// Associates i_Entity with the conversation. Only associates does not reload.
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    Task Attach<T, TId>(T i_Entity) where T: IDomainEntity<TId>;
    
    /// <summary>
    /// Deletes EVERY entry for T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Configuration"></param>
    /// <param name="i_Node"></param>
    void Delete<T, Tid>(IConversationOperationConfiguration i_Configuration, SubValidationNode i_Node) where T : IDomainEntity<Tid>;

    /// <summary>
    /// Deletes EVERY entry for T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Configuration"></param>
    /// <param name="i_Node"></param>
    Task DeleteAsync<T, Tid>(IConversationOperationConfiguration i_Configuration, SubValidationNode i_Node) where T : IDomainEntity<Tid>;

    /// <summary>
    /// Creates a proxy used for lazy loading for an object of type T and i_ID.
    /// Might not be supported on all conversation implementations
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    T CreateLazyProxy<T, TId>(TId i_ID) where T : class, IDomainEntity<TId>;

    /// <summary>
    /// Unproxy
    /// </summary>
    /// <param name="i_PossibleProxy"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    T Unproxy<T, TId>(object i_PossibleProxy) where T : IDomainEntity<TId>;
    
    /// <summary>
    /// Unproxy
    /// </summary>
    /// <param name="i_PossibleProxy"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    T UnproxyWithInitialize<T, TId>(object i_PossibleProxy) where T : IDomainEntity<TId>;

    /// <summary>
    /// Checks if i_Instance is a proxy
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <returns></returns>
    bool IsProxy(object i_Instance);
    
    /// <summary>
    /// Update with policy
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Configuration"></param>
    /// <param name="i_Node"></param>
    void Update<T, Tid>(T i_Instance, ISyncPolicy i_Policy, IConversationOperationConfiguration i_Configuration, SubValidationNode i_Node) where T : IDomainEntity<Tid>;

    /// <summary>
    /// Update with policy
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Configuration"></param>
    /// <param name="i_Node"></param>
    /// <returns></returns>
    Task UpdateAsync<T, Tid>(T i_Instance, IAsyncPolicy i_Policy, IConversationOperationConfiguration i_Configuration, SubValidationNode i_Node) where T : IDomainEntity<Tid>;

    /// <summary>
    /// Save with policy
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Configuration"></param>
    /// <param name="i_Node"></param>
    void Save<T, Tid>(T i_Instance, ISyncPolicy i_Policy, IConversationOperationConfiguration i_Configuration, SubValidationNode i_Node) where T : IDomainEntity<Tid>;

    /// <summary>
    /// Save with policy
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Configuration"></param>
    /// <param name="i_Node"></param>
    /// <returns></returns>
    Task SaveAsync<T, Tid>(T i_Instance, IAsyncPolicy i_Policy, IConversationOperationConfiguration i_Configuration, SubValidationNode i_Node) where T : IDomainEntity<Tid>;

    /// <summary>
    /// SaveOrUpdate with policy
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Configuration"></param>
    /// <param name="i_Node"></param>
    void SaveOrUpdate<T, Tid>(T i_Instance, ISyncPolicy i_Policy, IConversationOperationConfiguration i_Configuration, SubValidationNode i_Node) where T : IDomainEntity<Tid>;

    /// <summary>
    /// SaveOrUpdate with policy
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Configuration"></param>
    /// <param name="i_Node"></param>
    /// <returns></returns>
    Task SaveOrUpdateAsync<T, Tid>(T i_Instance, IAsyncPolicy i_Policy, IConversationOperationConfiguration i_Configuration, SubValidationNode i_Node) where T : IDomainEntity<Tid>;

    /// <summary>
    /// Save Or Update Async but without policy support. Hopefully higher performance than normal
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_Config"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    ValueTask SaveOrUpdateAsyncRaw<T, Tid>(T i_Instance, IConversationOperationConfiguration i_Config) where T : IDomainEntity<Tid>;

    /// <summary>
    /// Delete with policy
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Configuration"></param>
    /// <param name="i_Node"></param>
    void Delete<T, Tid>(T i_Instance, ISyncPolicy i_Policy, IConversationOperationConfiguration i_Configuration, SubValidationNode i_Node) where T : IDomainEntity<Tid>;

    /// <summary>
    /// Delete with policy
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Configuration"></param>
    /// <param name="i_Node"></param>
    /// <returns></returns>
    Task DeleteAsync<T, Tid>(T i_Instance, IAsyncPolicy i_Policy, IConversationOperationConfiguration i_Configuration, SubValidationNode i_Node) where T : IDomainEntity<Tid>;

    /// <summary>
    /// Save or update async with policy and relations
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Relations"></param>
    /// <param name="i_Node"></param>
    /// <returns></returns>
    Task SaveOrUpdateAsync<T, Tid>(T i_Instance, IAsyncPolicy i_Policy, IEnumerable<ICascadingRelation> i_Relations, SubValidationNode i_Node) where T : IDomainEntity<Tid>;

    /// <summary>
    /// Delete async with policy and relations
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tid"></typeparam>
    /// <param name="i_Instance"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Relations"></param>
    /// <param name="i_Node"></param>
    /// <returns></returns>
    Task DeleteAsync<T, Tid>(T i_Instance, IAsyncPolicy i_Policy, IEnumerable<ICascadingRelation> i_Relations, SubValidationNode i_Node) where T : IDomainEntity<Tid>;

    /// <summary>
    /// Calling this method prevents this conversation from sending any messages
    /// </summary>
    void DisableMessages();
  }
}