using System;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Storage.Conversations.Config
{
  /// <summary>
  /// Default Conversation Context Setting
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class DefaultConversationContextSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// KEY
    /// </summary>
    public const string KEY = "NiceCore.Storage.Conversation.Contexts.Default";
    /// <summary>
    /// KEY
    /// </summary>
    public const string TYPE_KEY = KEY + ".Type";
    /// <summary>
    /// KEY
    /// </summary>
    public const string TYPE_VALUE = KEY + ".Value";

    /// <summary>
    /// Key
    /// </summary>
    public string Key { get; } = KEY;

    /// <summary>
    /// Setting Type
    /// </summary>
    public Type SettingType { get; } = typeof(object);
  }
}