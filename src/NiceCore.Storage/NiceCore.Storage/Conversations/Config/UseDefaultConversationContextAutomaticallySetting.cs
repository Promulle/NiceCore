using System;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Storage.Conversations.Config
{
  /// <summary>
  /// Use Default ConversationContext Automatically
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class UseDefaultConversationContextAutomaticallySetting : IConfigurableSettingModel
  {
    /// <summary>
    /// KEY
    /// </summary>
    public const string KEY = "NiceCore.Storage.Conversation.Contexts.AutomaticallyApplyDefaultContext";

    /// <summary>
    /// KEy
    /// </summary>
    public string Key { get; } = KEY;

    /// <summary>
    /// Setting Type
    /// </summary>
    public Type SettingType { get; } = typeof(bool);
  }
}