using System;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Storage.Conversations.Config
{
  /// <summary>
  /// Available Conversation Context Setting
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class AvailableConversationContextsSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// KEY
    /// </summary>
    public const string KEY = "NiceCore.Storage.Conversation.Contexts.Available";

    /// <summary>
    /// KEy
    /// </summary>
    public string Key { get; } = KEY;
    
    /// <summary>
    /// Setting Type
    /// </summary>
    public Type SettingType { get; } = typeof(object);
  }
}