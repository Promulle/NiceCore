using NiceCore.Storage.Auditing;

namespace NiceCore.Storage.Conversations
{
  /// <summary>
  /// Interface for ReadOnlyConversations that have access to the auditing support
  /// </summary>
  public interface IReadOnlyAuditableConversation : IReadonlyConversation
  {
    /// <summary>
    /// Audit -> Gateway to audit functionality. Only filled if the auditing of the conversation is enabled
    /// </summary>
    INcAuditingSupport Audit { get; }
  }
}