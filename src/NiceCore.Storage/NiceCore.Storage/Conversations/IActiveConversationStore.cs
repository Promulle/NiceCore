using System;

namespace NiceCore.Storage.Conversations
{
  /// <summary>
  /// Active Conversation Store
  /// </summary>
  public interface IActiveConversationStore
  {
    /// <summary>
    /// Adds i_Conversation to active conversations
    /// </summary>
    /// <param name="i_Conversation"></param>
    void AddActiveConversation(IConversation i_Conversation);

    /// <summary>
    /// Get Active Conversation for the given id
    /// </summary>
    /// <param name="i_ConversationId"></param>
    /// <returns></returns>
    IConversation GetActiveConversation(Guid i_ConversationId);

    /// <summary>
    /// Removes the conversation for the given id
    /// </summary>
    /// <param name="i_ConversationId"></param>
    /// <returns></returns>
    bool RemoveConversation(Guid i_ConversationId);
    
    /// <summary>
    /// Removes the conversation for the given id
    /// </summary>
    /// <param name="i_Conversation"></param>
    /// <returns></returns>
    bool RemoveConversation(IConversation i_Conversation);
  }
}