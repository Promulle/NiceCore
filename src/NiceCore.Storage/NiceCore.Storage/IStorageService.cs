﻿using System;
using NiceCore.Base.Services;
using NiceCore.ServiceLocation;
using NiceCore.Storage.Conversations;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using NiceCore.Eventing.Messaging.Parameters;
using NiceCore.Storage.Connections;
using NiceCore.Storage.Conversations.Factories;

namespace NiceCore.Storage
{
  /// <summary>
  /// Service for interacting with defined storage Connections/Conversations
  /// </summary>
  public interface IStorageService : IService
  {
    /// <summary>
    /// Conversation Factory
    /// </summary>
    IConversationFactory ConversationFactory { get; }
    
    /// <summary>
    /// Strategy that should be used when creating a connection errors out
    /// </summary>
    IDefaultConnectionRetryStrategy ConnectionRetryStrategy { get; set; }

    /// <summary>
    /// Whether Initialize initializes the service completely or if service should initialize parts if needed
    /// </summary>
    bool EnableFullInitialize { get; set; }
    /// <summary>
    /// Container managing registrations
    /// </summary>
    IServiceContainer Container { get; set; }
    /// <summary>
    /// returns a new Conversation which is identified by name
    /// </summary>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    IConversation Conversation(string i_Name);

    /// <summary>
    /// returns a conversation, if there is a configured Connection for i_Name under the context of i_Context
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Context">Context to create new conversation under. may be null</param>
    /// <returns></returns>
    IConversation Conversation(string i_Name, IConversationContext i_Context);

    /// <summary>
    /// returns a conversation, if there is a configured Connection for i_Name under the context of i_Context
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Context">Context to create new conversation under. may be null</param>
    /// <param name="i_MessageSendingParameters">Parameters for messages, may be null</param>
    /// <returns></returns>
    IConversation Conversation(string i_Name, IConversationContext i_Context, IMessageSendingParameters i_MessageSendingParameters);
    
    /// <summary>
    /// Returns the connection string for i_ConnectionName
    /// </summary>
    /// <param name="i_ConnectionName"></param>
    /// <returns></returns>
    Task<string> GetConnectionStringForConnection(string i_ConnectionName);

    /// <summary>
    /// Returns the driver used to create connections for the specified connection. Might not be supported on all implementations
    /// </summary>
    /// <param name="i_ConnectionName"></param>
    /// <returns></returns>
    Task<string> GetConnectionDriverForConnection(string i_ConnectionName);

    /// <summary>
    /// Returns the connectionInfo for a given connection name, if found.
    /// </summary>
    /// <param name="i_ConnectionName"></param>
    /// <returns></returns>
    Task<IConnectionInfo> GetConnectionInfo(string i_ConnectionName);

    /// <summary>
    /// Returns a connection for the definition found at i_Name.
    /// Only implemented if the Storage implementation requires sql-connections
    /// </summary>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    Task<DbConnection> GetConnection(string i_Name);

    /// <summary>
    /// check whether i_Type is mapped for i_ConnectionDefinition
    /// </summary>
    /// <param name="i_Type"></param>
    /// <param name="i_ConnectionDefinitionName"></param>
    /// <returns></returns>
    ValueTask<bool> IsEntityTypeMappedFor(Type i_Type, string i_ConnectionDefinitionName);

    /// <summary>
    /// Get Connection Definition Name for Entity
    /// </summary>
    /// <param name="i_Type"></param>
    /// <returns></returns>
    ValueTask<string> GetConnectionDefinitionNameForEntity(Type i_Type);
  }
}
