﻿using Polly;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Logging.Extensions;

namespace NiceCore.Storage
{
  /// <summary>
  /// Default impl for IDefaultConnectionRetryStrategy
  /// </summary>
  public class DefaultConnectionRetryStrategy : IDefaultConnectionRetryStrategy
  {
    private readonly ILogger m_Logger;
    
    /// <summary>
    /// Policy for sync
    /// </summary>
    public ISyncPolicy SyncPolicy { get; init; }

    /// <summary>
    /// Policy for async
    /// </summary>
    public IAsyncPolicy AsyncPolicy { get; init; }

    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_Logger"></param>
    public DefaultConnectionRetryStrategy(ILogger i_Logger)
    {
      m_Logger = i_Logger;
    }
    
    /// <summary>
    /// Execute
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Func"></param>
    /// <returns></returns>
    public T Execute<T>(Func<T> i_Func)
    {
      if (SyncPolicy != null)
      {
        return SyncPolicy.Execute(i_Func);
      }
      m_Logger.Debug($"Sync-Execute called but {nameof(SyncPolicy)} is not set. Executing function normally.");
      return i_Func.Invoke();
    }

    /// <summary>
    /// Execute
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Func"></param>
    /// <returns></returns>
    public Task<T> Execute<T>(Func<Task<T>> i_Func)
    {
      if (AsyncPolicy != null)
      {
        return AsyncPolicy.ExecuteAsync(i_Func);
      }
      m_Logger.Debug($"Async-Execute called but {nameof(AsyncPolicy)} is not set. Executing function normally.");
      return i_Func.Invoke();
    }

    /// <summary>
    /// Execute
    /// </summary>
    /// <param name="i_Func"></param>
    /// <returns></returns>
    public Task Execute(Func<Task> i_Func)
    {
      if (AsyncPolicy != null)
      {
        return AsyncPolicy.ExecuteAsync(i_Func);
      }
      m_Logger.Debug($"Async-Execute called but {nameof(AsyncPolicy)} is not set. Executing function normally.");
      return i_Func.Invoke();
    }

    /// <summary>
    /// Execute
    /// </summary>
    /// <param name="i_Func"></param>
    public void Execute(Action i_Func)
    {
      if (SyncPolicy != null)
      {
        SyncPolicy.Execute(i_Func);
      }
      else
      {
        m_Logger.Debug($"Sync-Execute called but {nameof(SyncPolicy)} is not set. Executing function normally.");
        i_Func.Invoke();
      }
    }
  }
}
