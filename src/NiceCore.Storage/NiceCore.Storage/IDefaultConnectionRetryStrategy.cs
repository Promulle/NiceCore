﻿using Polly;
using System;
using System.Threading.Tasks;

namespace NiceCore.Storage
{
  /// <summary>
  /// Strategy to be used when creating or accessing a connection fails
  /// </summary>
  public interface IDefaultConnectionRetryStrategy
  {
    /// <summary>
    /// Sync Policy to use
    /// </summary>
    ISyncPolicy SyncPolicy { get; }

    /// <summary>
    /// Async policy to use
    /// </summary>
    IAsyncPolicy AsyncPolicy { get; }

    /// <summary>
    /// Retries i_Func using the correct retry policy. If T implements/inherits Task, the async policy is used
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Func"></param>
    /// <returns></returns>
    T Execute<T>(Func<T> i_Func);

    /// <summary>
    /// Retries i_Func using the correct retry policy. If T implements/inherits Task, the async policy is used
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Func"></param>
    /// <returns></returns>
    Task<T> Execute<T>(Func<Task<T>> i_Func);

    /// <summary>
    /// Retries i_Func using the correct retry policy. If T implements/inherits Task, the async policy is used
    /// </summary>
    /// <param name="i_Func"></param>
    /// <returns></returns>
    Task Execute(Func<Task> i_Func);

    /// <summary>
    /// Retries i_Func using the correct retry policy. Sync policy is used if found
    /// </summary>
    /// <param name="i_Func"></param>
    /// <returns></returns>
    void Execute(Action i_Func);
  }
}
