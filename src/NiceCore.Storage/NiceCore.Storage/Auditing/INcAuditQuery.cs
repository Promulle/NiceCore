using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using NiceCore.Entities;

namespace NiceCore.Storage.Auditing
{
  /// <summary>
  /// Audit Query interface
  /// </summary>
  public interface INcAuditQuery<T, TId> where T: IDomainEntity<TId>
  {
    /// <summary>
    /// Where
    /// </summary>
    /// <returns></returns>
    INcAuditQuery<T, TId> WhereIDIs(TId i_ID);

    /// <summary>
    /// Where Revision is
    /// </summary>
    /// <param name="i_RevisionNumber"></param>
    /// <returns></returns>
    INcAuditQuery<T, TId> WhereRevisionIs(long i_RevisionNumber);

    /// <summary>
    /// Where Revisions are in i_RevisionNumbers
    /// </summary>
    /// <param name="i_RevisionNumbers"></param>
    /// <returns></returns>
    INcAuditQuery<T, TId> WhereRevisionIsIn(IEnumerable<long> i_RevisionNumbers);

    /// <summary>
    /// adds conditions for all ids in i_Ids. Basically does a i_Ids.Contains(r.ID) with a lot of wheres
    /// </summary>
    /// <param name="i_Ids"></param>
    /// <returns></returns>
    INcAuditQuery<T, TId> WhereIDIsIn(IEnumerable<TId> i_Ids);

    /// <summary>
    /// Get result
    /// </summary>
    /// <returns></returns>
    Task<NcAuditResult<T, TId>> GetResult();
  }
}