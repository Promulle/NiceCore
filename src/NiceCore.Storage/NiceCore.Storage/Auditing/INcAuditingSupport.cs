using System;
using System.Collections.Generic;
using NiceCore.Entities;

namespace NiceCore.Storage.Auditing
{
  /// <summary>
  /// Auditing Support, enables audit related functions for a conversation
  /// </summary>
  public interface INcAuditingSupport
  {
    /// <summary>
    /// Queries the audit system
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    INcAuditQuery<T, TId> Query<T, TId>() where T : IDomainEntity<TId>;

    /// <summary>
    /// Queries the audit system
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    INcAuditQuery<T, TId> Query<T, TId>(int i_RevisionID) where T : IDomainEntity<TId>;

    /// <summary>
    /// Finds the revision for i_ID and i_Revision
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_ID"></param>
    /// <param name="i_Revision"></param>
    /// <returns></returns>
    T FindRevision<T>(long i_ID, long i_Revision) where T : IBaseDomainEntity;

    /// <summary>
    /// Finds the revision for i_ID and i_Revision
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <param name="i_ID"></param>
    /// <param name="i_Revision"></param>
    /// <returns></returns>
    T FindRevision<T, TId>(TId i_ID, long i_Revision) where T : IDomainEntity<TId>;

    /// <summary>
    /// Returns all possible revisions of an entity of type T with id specified by i_Id
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <param name="i_Id"></param>
    /// <returns></returns>
    public IEnumerable<long> GetRevisions<T, TId>(TId i_Id) where T : IDomainEntity<TId>;

    /// <summary>
    /// Checks if T is audited
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId"></typeparam>
    /// <returns></returns>
    public bool IsEntityClassAudited<T, TId>() where T : IDomainEntity<TId>;

    /// <summary>
    /// Return the revision number for date
    /// </summary>
    /// <param name="i_Date"></param>
    /// <returns></returns>
    public long GetRevisionNumberForDate(DateTime i_Date);

    /// <summary>
    /// Returns the date for a revision
    /// </summary>
    /// <param name="i_Revision"></param>
    /// <returns></returns>
    public DateTime GetRevisionDate(long i_Revision);
  }
}