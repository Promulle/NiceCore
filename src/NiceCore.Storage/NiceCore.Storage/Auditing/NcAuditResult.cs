using System.Collections.Generic;
using NiceCore.Entities;

namespace NiceCore.Storage.Auditing
{
  /// <summary>
  /// Result of a nice core audit query
  /// </summary>
  public class NcAuditResult<T, TId> where T: IDomainEntity<TId>
  {
    /// <summary>
    /// Results 
    /// </summary>
    public List<NcSingleAuditResult<T, TId>> Results { get; init; } = new();
  }
}