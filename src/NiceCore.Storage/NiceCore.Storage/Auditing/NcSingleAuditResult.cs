using NiceCore.Entities;

namespace NiceCore.Storage.Auditing
{
  /// <summary>
  /// Nc Single Audit Result
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <typeparam name="TId"></typeparam>
  public class NcSingleAuditResult<T, TId> where T: IDomainEntity<TId>
  {
    /// <summary>
    /// Entity 
    /// </summary>
    public T Entity { get; init; }
    
    /// <summary>
    /// Id of revision
    /// </summary>
    public int RevisionID { get; init; }
    
    /// <summary>
    /// Audit operation
    /// </summary>
    public NcAuditedOperations AuditedOperation { get; init; }
  }
}