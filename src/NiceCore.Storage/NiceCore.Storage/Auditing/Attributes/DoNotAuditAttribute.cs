using System;

namespace NiceCore.Storage.Auditing.Attributes
{
  /// <summary>
  /// This attribute is used to mark a class as "do not use in auditing"
  /// </summary>
  [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
  public class DoNotAuditAttribute : Attribute
  {
  }
}
