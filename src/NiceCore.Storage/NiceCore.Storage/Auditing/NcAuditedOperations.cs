namespace NiceCore.Storage.Auditing
{
  /// <summary>
  /// Operation that caused an entity to be audited
  /// </summary>
  public enum NcAuditedOperations
  {
    /// <summary>
    /// Value that is used when actual operation could not be retrieved
    /// </summary>
    None,
    
    /// <summary>
    /// Instance was created
    /// </summary>
    Create,
    
    /// <summary>
    /// Instance was updated
    /// </summary>
    Update,
    
    /// <summary>
    /// Instance was deleted
    /// </summary>
    Deleted
  }
}