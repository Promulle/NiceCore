using System;
using System.Collections.Generic;
using NiceCore.Storage.Conversations;

namespace NiceCore.Storage.Auditing
{
  /// <summary>
  /// IAudit On Perform Listener
  /// </summary>
  public interface IAuditOnPerformListener
  {
    /// <summary>
    /// On Audit Strategy Perform
    /// </summary>
    /// <param name="i_Context"></param>
    /// <param name="i_EntityName"></param>
    /// <param name="i_EntityType"></param>
    /// <param name="i_ID"></param>
    /// <param name="t_State"></param>
    void OnAuditStrategyPerform(IConversationContext i_Context, string i_EntityName, Type i_EntityType, object i_ID, IDictionary<string, object> t_State);
  }
}