﻿namespace NiceCore.Storage
{
  /// <summary>
  /// Interface, that a config section, that stores storage information, should implement
  /// </summary>
  public interface IStorageConfigSection
  {
  }
}
