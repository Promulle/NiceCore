﻿using NiceCore.Storage.Conversations;
using System.Linq;
using NiceCore.Storage.Conversations.Flags;

namespace NiceCore.Storage
{
  /// <summary>
  /// Interface for limiters that can be applied to queries
  /// </summary>
  public interface IQueryLimiter
  {
    /// <summary>
    /// return a new limited query
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Query"></param>
    /// <param name="i_Flags"></param>
    /// <param name="i_Context"></param>
    /// <returns></returns>
    IQueryable<T> LimitQuery<T>(IQueryable<T> i_Query, ConversationOperationFlags i_Flags, IConversationContext i_Context);
  }
}
