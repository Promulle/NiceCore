﻿namespace NiceCore.Storage
{
  /// <summary>
  /// Interface for objects that define configuration for operations done by a conversation
  /// </summary>
  public interface IConversationOperationConfiguration
  {
    /// <summary>
    /// Flag that determines whether the operation should validate the instance before actually executing the operation
    /// </summary>
    bool ValidateInstance { get; }
    
    /// <summary>
    /// If the config is the default instance it will be overruled by 
    /// </summary>
    bool IsDefaultInstance { get; }
  }
}
