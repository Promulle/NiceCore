using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using NiceCore.Diagnostics;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation;

namespace NiceCore.Storage.Events
{
  /// <summary>
  /// Storage Event Model Service
  /// </summary>
  [Register(InterfaceType = typeof(IStorageEventModelService), LifeStyle = LifeStyles.Singleton)]
  public class StorageEventModelService : IStorageEventModelService
  {
    private readonly Dictionary<string, StorageEventModel> m_GlobalEventModelsByConnection = new();

    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<StorageEventModelService> Logger { get; set; } = DiagnosticsCollector.Instance.GetLogger<StorageEventModelService>();

    /// <summary>
    /// Configures the StorageEventModelService
    /// </summary>
    /// <param name="i_ConnectionName"></param>
    /// <param name="i_Model"></param>
    /// <exception cref="InvalidOperationException">If Service instance was already configured.</exception>
    public void Configure(string i_ConnectionName, StorageEventModel i_Model)
    {
      Logger.Information($"Configuring Storage event model for connection name {i_ConnectionName}");
      m_GlobalEventModelsByConnection[i_ConnectionName] = i_Model;
    }

    /// <summary>
    /// Get a copied storageEventModel instance
    /// </summary>
    /// <returns></returns>
    public StorageEventModel GetTransientStorageEventModel(string i_ConnectionName)
    {
      if (m_GlobalEventModelsByConnection.TryGetValue(i_ConnectionName, out var sourceModel))
      {
        Logger.Debug($"Storage event model is configured for {i_ConnectionName}, returning copied instance.");
        return StorageEventModel.Copy(sourceModel);
      }

      Logger.Warning($"Storage event model was not configured for connection {i_ConnectionName}, returning new default instance.");
      return StorageEventModel.CreateNew();
    }
  }
}