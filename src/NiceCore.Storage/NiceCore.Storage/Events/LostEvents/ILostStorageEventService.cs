using System.Collections.Generic;

namespace NiceCore.Storage.Events.LostEvents
{
  /// <summary>
  /// Lost Storage Event Service
  /// </summary>
  public interface ILostStorageEventService
  {
    /// <summary>
    /// Add a lost event
    /// </summary>
    /// <param name="i_Event"></param>
    void AddLostStorageEvent(LostStorageEvent i_Event);

    /// <summary>
    /// Returns the currently tracked lost events
    /// </summary>
    /// <returns></returns>
    IEnumerable<LostStorageEvent> GetCurrentlyLostEvents();

    /// <summary>
    /// Clear
    /// </summary>
    void Clear();
  }
}