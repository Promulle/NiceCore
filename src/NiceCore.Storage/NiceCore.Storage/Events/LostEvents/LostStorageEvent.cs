namespace NiceCore.Storage.Events.LostEvents
{
  /// <summary>
  /// Class for a single lost event
  /// </summary>
  public sealed class LostStorageEvent
  {
    /// <summary>
    /// Entity
    /// </summary>
    public required object Entity { get; init; }
    
    /// <summary>
    /// Operation
    /// </summary>
    public required string Operation { get; init; }
  }
}