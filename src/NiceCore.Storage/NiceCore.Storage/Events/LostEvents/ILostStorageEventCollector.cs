namespace NiceCore.Storage.Events.LostEvents
{
  /// <summary>
  /// lost Storage Event Collector
  /// </summary>
  public interface ILostStorageEventCollector
  {
    /// <summary>
    /// Queues a lost event for i_Entity at i_Operation if lostEvents is enabled
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_Operation"></param>
    void EventLost(object i_Entity, string i_Operation);
  }
}