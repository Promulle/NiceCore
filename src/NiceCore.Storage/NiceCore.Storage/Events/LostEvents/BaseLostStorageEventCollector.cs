using NiceCore.Base.Services;
using NiceCore.Services.Config;
using NiceCore.Storage.Config.AvailableSettings;

namespace NiceCore.Storage.Events.LostEvents
{
  /// <summary>
  /// Lost Storage Event Collector
  /// </summary>
  public abstract class BaseLostStorageEventCollector : BaseService, ILostStorageEventCollector
  {
    private bool m_IsEnabled = false;
    
    /// <summary>
    /// Configuration
    /// </summary>
    public INcConfiguration Configuration { get; set; }
    
    /// <summary>
    /// Lost Storage Event Service
    /// </summary>
    public ILostStorageEventService LostStorageEventService { get; set; }

    /// <summary>
    /// Initialize
    /// </summary>
    protected override void InternalInitialize()
    {
      base.InternalInitialize();
      m_IsEnabled = Configuration.GetValue(EnableLostStorageEventsSetting.KEY, false);
    }

    /// <summary>
    /// Checks if the event collector can be used for the given entity
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <returns></returns>
    protected abstract bool CanBeUsedForEntity(object i_Entity);

    /// <summary>
    /// Queues a lost event for i_Entity at i_Operation if lostEvents is enabled
    /// </summary>
    /// <param name="i_Entity"></param>
    /// <param name="i_Operation"></param>
    public void EventLost(object i_Entity, string i_Operation)
    {
      if (!m_IsEnabled)
      {
        return;
      }

      if (CanBeUsedForEntity(i_Entity))
      {
        LostStorageEventService.AddLostStorageEvent(new()
        {
          Entity = i_Entity,
          Operation = i_Operation
        });
      }
    }
  }
}