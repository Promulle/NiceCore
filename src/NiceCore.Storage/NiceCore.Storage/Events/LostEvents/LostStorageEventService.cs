using System.Collections.Generic;
using NiceCore.ServiceLocation;

namespace NiceCore.Storage.Events.LostEvents
{
  /// <summary>
  /// Lost Storage Event Service
  /// </summary>
  [Register(InterfaceType = typeof(ILostStorageEventService), LifeStyle = LifeStyles.Singleton)]
  public class LostStorageEventService : ILostStorageEventService
  {
    private readonly object m_Lock = new();
    private readonly List<LostStorageEvent> m_Events = new();

    /// <summary>
    /// Add a lost event
    /// </summary>
    /// <param name="i_Event"></param>
    public void AddLostStorageEvent(LostStorageEvent i_Event)
    {
      lock (m_Lock)
      {
        m_Events.Add(i_Event);
      }
    }

    /// <summary>
    /// Returns the currently tracked lost events
    /// </summary>
    /// <returns></returns>
    public IEnumerable<LostStorageEvent> GetCurrentlyLostEvents()
    {
      lock (m_Lock)
      {
        return m_Events.ToArray();
      }
    }

    /// <summary>
    /// Clear
    /// </summary>
    public void Clear()
    {
      lock (m_Lock)
      {
        m_Events.Clear();
      }
    }
  }
}