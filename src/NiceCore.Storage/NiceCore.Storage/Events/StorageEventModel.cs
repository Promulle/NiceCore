using System;
using System.Collections.Generic;
using NiceCore.Collections;

namespace NiceCore.Storage.Events
{
  /// <summary>
  /// Class that describes the behavior of storage components like conversations when it comes to things that happened
  /// </summary>
  public sealed class StorageEventModel
  {
    private readonly Dictionary<Type, HashSet<string>> m_IgnoredProperties = new();

    private StorageEventModel()
    {
      
    }
    
    /// <summary>
    /// Checks if a property should be ignored for update events
    /// </summary>
    /// <param name="i_EntityType"></param>
    /// <param name="i_PropertyName"></param>
    /// <returns></returns>
    public bool IsPropertyIgnoredForUpdate(Type i_EntityType, string i_PropertyName)
    {
      if (i_EntityType == null)
      {
        throw new ArgumentException($"Could not check for ignored property. Argument {nameof(i_EntityType)} was null");
      }
      if (i_PropertyName == null)
      {
        return false;
      }
      if (m_IgnoredProperties.TryGetValue(i_EntityType, out var ignoredProperties))
      {
        return ignoredProperties.Contains(i_PropertyName);
      }
      return false;
    }

    /// <summary>
    /// get all properties that are ignored for type typeof(T)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public IReadOnlySet<string> GetUpdateIgnoredPropertiesForType<T>()
    {
      return GetUpdateIgnoredPropertiesForType(typeof(T));
    }

    /// <summary>
    /// vget all properties that are ignored for type i_Type
    /// </summary>
    /// <param name="i_Type"></param>
    /// <returns></returns>
    public IReadOnlySet<string> GetUpdateIgnoredPropertiesForType(Type i_Type)
    {
      if (i_Type == null)
      {
        throw new ArgumentException($"Could not get update ignored properties for type. Argument {nameof(i_Type)} was null.");
      }
      if (m_IgnoredProperties.TryGetValue(i_Type, out var set))
      {
        return set;
      }

      return NcDefaultSets<string>.Empty;
    }

    /// <summary>
    /// Ignore an additional property in the scope of this event model
    /// </summary>
    /// <param name="i_PropertyName"></param>
    /// <typeparam name="T"></typeparam>
    public void IgnorePropertyForUpdate<T>(string i_PropertyName)
    {
      IgnorePropertyForUpdate(typeof(T), i_PropertyName);
    }
    
    /// <summary>
    /// Ignore an additional property in the scope of this event model
    /// </summary>
    /// <param name="i_Type"></param>
    /// <param name="i_PropertyName"></param>
    /// <exception cref="ArgumentException">If i_Type or i_Property are null.</exception>
    public void IgnorePropertyForUpdate(Type i_Type, string i_PropertyName)
    {
      if (i_Type == null)
      {
        throw new ArgumentException($"Could not ignore additional property {i_PropertyName}. Argument {nameof(i_Type)} was null.");
      }

      if (i_PropertyName == null)
      {
        throw new ArgumentException($"Could not ignore additional property for type {i_Type.FullName}. Argument {nameof(i_PropertyName)} was null.");
      }

      if (!m_IgnoredProperties.TryGetValue(i_Type, out var set))
      {
        set = new();
        m_IgnoredProperties[i_Type] = set;
      }

      set.Add(i_PropertyName);
    }

    /// <summary>
    /// Creates a new Storage EventModel
    /// </summary>
    /// <returns></returns>
    public static StorageEventModel CreateNew()
    {
      return new();
    }

    /// <summary>
    /// Creates a new instance that copies settings from i_Model
    /// </summary>
    /// <param name="i_Model"></param>
    /// <returns></returns>
    public static StorageEventModel Copy(StorageEventModel i_Model)
    {
      var newInst = new StorageEventModel();
      foreach (var (key, value) in i_Model.m_IgnoredProperties)
      {
        var newIgnoredSet = new HashSet<string>(value);
        newInst.m_IgnoredProperties[key] = newIgnoredSet;
      }
      return newInst;
    }
  }
}