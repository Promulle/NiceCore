using System;

namespace NiceCore.Storage.Events
{
  /// <summary>
  /// Storage Event Model Service
  /// </summary>
  public interface IStorageEventModelService
  {
    /// <summary>
    /// Configures the StorageEventModelService
    /// </summary>
    /// <param name="i_ConnectionName"></param>
    /// <param name="i_Model"></param>
    /// <exception cref="InvalidOperationException">If Service instance was already configured.</exception>
    void Configure(string i_ConnectionName, StorageEventModel i_Model);
    
    /// <summary>
    /// Get a copied storageEventModel instance
    /// </summary>
    /// <returns></returns>
    StorageEventModel GetTransientStorageEventModel(string i_ConnectionName);
  }
}