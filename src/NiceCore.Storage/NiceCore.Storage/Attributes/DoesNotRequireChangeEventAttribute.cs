using System;

namespace NiceCore.Storage.Attributes
{
  /// <summary>
  /// Attribute that marks properties where a change should not lead to any raised event
  /// </summary>
  [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
  public class DoesNotRequireChangeEventAttribute : Attribute
  {
  }
}