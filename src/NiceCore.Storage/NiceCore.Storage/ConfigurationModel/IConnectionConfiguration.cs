﻿namespace NiceCore.Storage.ConfigurationModel
{
  /// <summary>
  /// Interface for models that contain the configuration for a connection
  /// </summary>
  public interface IConnectionConfiguration
  {
  }
}