using System;
using NiceCore.Entities;

namespace NiceCore.Storage.NH.Benchmarks.Models
{
  public class BenchmarkEntity : DomainEntity<Guid>
  {
    public virtual string Name { get; set; }
    public virtual string Description { get; set; }
    public virtual int Number { get; set; }
  }
}