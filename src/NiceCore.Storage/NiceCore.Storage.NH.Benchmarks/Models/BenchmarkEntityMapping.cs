using System;
using System.Collections.Generic;
using FluentNHibernate.Mapping;
using NiceCore.Collections;
using NiceCore.ServiceLocation;
using NiceCore.Storage.NH.Mapping;

namespace NiceCore.Storage.NH.Benchmarks.Models
{
  [Register(InterfaceType = typeof(IFluentlyMapped))]
  public class BenchmarkEntityMapping : ClassMap<BenchmarkEntity>, IFluentlyMapped
  {
    public BenchmarkEntityMapping()
    {
      Id(r => r.ID).GeneratedBy.Guid();
      Map(r => r.Name, "name").Length(200);
      Map(r => r.Description, "description").Length(8000);
      Map(r => r.Number, "number");
      Table("benchmark_entity");
    }
    
    public Type GetMappedType()
    {
      return typeof(BenchmarkEntity);
    }

    public IReadOnlySet<string> GetAllowedConnections()
    {
      return NcDefaultSets<string>.Empty;
    }
  }
}