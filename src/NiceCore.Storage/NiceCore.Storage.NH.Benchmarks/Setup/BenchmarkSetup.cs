using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using NiceCore.Rapid;
using NiceCore.Rapid.ApplicationBuilding;
using NiceCore.Rapid.Storage;
using NiceCore.ServiceLocation;
using NiceCore.ServiceLocation.CastleWindsor;
using NiceCore.Services.Config.Impl;
using NiceCore.Storage.Conversations.Factories;
using NiceCore.Storage.Conversations.Factories.Extensions;
using NiceCore.Storage.NH.Benchmarks.Models;
using NiceCore.Storage.Services;

namespace NiceCore.Storage.NH.Benchmarks.Setup
{
  public class BenchmarkSetup
  {
    private const string RANDOM_SET = "abcdefghijklmnoprstuvwxyzABCDEFGHIJKLMNOPRSTUVWXYZ0123456789";
    
    public const string CONNECTION_NAME = "bechmarks";
    
    private const string CONNECTION_STRING = $"Server=localhost;Database=NC-Benchmarks-NH;User ID=TestUser;Password=Test;Enlist=true;";

    public IServiceContainer Container { get; private set; }
    public IStorageService StorageService { get; private set; }
    public IConversationFactory ConversationFactory { get; private set; }

    /// <summary>
    /// Get Configuration Dictionary
    /// </summary>
    /// <returns></returns>
    protected virtual IDictionary<string, string> GetConfigurationDictionary()
    {
      return new Dictionary<string, string>()
      {
        {"NcTest:ShowSql", "false"},
        {"NcTest:DriverClass", "NHibernate.Driver.NpgsqlDriver"},
        {"NcTest:Dialect", "NHibernate.Dialect.PostgreSQL83Dialect"},
        {"NcTest:ConnectionString", $"{CONNECTION_STRING}"}
      };
    }

    public async Task Setup()
    {
      var extensionStore = new DefaultAppBuilderExtensionStore();
      var app = await NcApplicationBuilder
        .Create<NcStorageApplication>()
        .ServiceLocation.UseContainerType<CastleWindsorServiceContainer>()
        .ServiceLocation.DefaultPlugins()
        .Configuration.Default()
        .Configuration.ConfigurationSources.AddInstance(new DefaultInMemoryNcConfigurationSource(GetConfigurationDictionary()))
        .StorageService(extensionStore).Default()
        .Build().ConfigureAwait(false);

      Container = app.ServiceContainer;
      StorageService = Container.Resolve<IStorageService>();

      ConversationFactory = Container.Resolve<IConversationFactory>();

      var conDefManager = Container.Resolve<IConnectionDefinitionManagementService>();
      conDefManager.AddConnectionDefinition(new ConnectionDefinition()
      {
        Name = CONNECTION_NAME,
        AllowAuditing = true,
        SchemaUpdate = true,
        UseSerialization = false,
        ConfigFile = "NcTest"
      }, true);
      //initialize storage service
      StorageService.Initialize();
    }

    public async Task GenerateEntities(int i_Start, int i_Count)
    {
      using var conv = await ConversationFactory.Conversation(CONNECTION_NAME).ConfigureAwait(false);
      for (var i = i_Start; i < i_Start + i_Count; i++)
      {
        var inst = GetNewInstance(i);
        await conv.SaveOrUpdateAsync<BenchmarkEntity, Guid>(inst).ConfigureAwait(false);
      }
      await conv.ApplyAsync().ConfigureAwait(false);
    }

    private static BenchmarkEntity GetNewInstance(int i_Number)
    {
      return new()
      {
        Name = GenerateString(100),
        Description = GenerateString(2000),
        Number = i_Number
      };
    }

    private static string GenerateString(int i_Count)
    {
      var builder = new StringBuilder();
      for (var i = 0; i < i_Count; i++)
      {
        var index = Random.Shared.Next(0, RANDOM_SET.Length - 1);
        var atPos = RANDOM_SET[index];
        builder.Append(atPos);
      }
      return builder.ToString();
    }
  }
}