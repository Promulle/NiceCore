using System;
using System.Linq;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using NiceCore.Storage.Conversations.Factories.Extensions;
using NiceCore.Storage.NH.Benchmarks.Models;
using NiceCore.Storage.NH.Benchmarks.Setup;

namespace NiceCore.Storage.NH.Benchmarks.Benchmarks
{
  [MemoryDiagnoser(false)]
  public class ConversationQuerySingleBenchmark
  {
    private readonly BenchmarkSetup m_Setup = new();
    
    [GlobalSetup]
    public Task Setup()
    {
      return m_Setup.Setup();
    }
    
    [Benchmark]
    public async Task QuerySingleDefaultConversation()
    {
      using var conv = await m_Setup.ConversationFactory.Conversation(BenchmarkSetup.CONNECTION_NAME);
      var entities = await conv.Query<BenchmarkEntity, Guid>()
        .OrderBy(r => r.Number)
        .FirstOrDefaultAsync().ConfigureAwait(false);
    }
    
    [Benchmark]
    public async Task QuerySingleStatelessConversation()
    {
      using var conv = await m_Setup.ConversationFactory.StatelessReadOnlyConversation(BenchmarkSetup.CONNECTION_NAME);
      var entities = await conv.Query<BenchmarkEntity, Guid>()
        .OrderBy(r => r.Number)
        .FirstOrDefaultAsync().ConfigureAwait(false);
    }
  }
}