using System;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using NHibernate.Linq;
using NiceCore.Storage.Conversations.Factories.Extensions;
using NiceCore.Storage.NH.Benchmarks.Models;
using NiceCore.Storage.NH.Benchmarks.Setup;

namespace NiceCore.Storage.NH.Benchmarks.Benchmarks
{
  [MemoryDiagnoser(false)]
  public class ConversationQueryBenchmarks
  {
    private readonly BenchmarkSetup m_Setup = new();
    
    [GlobalSetup]
    public Task Setup()
    {
      return m_Setup.Setup();
    }
    
    [Benchmark]
    public async Task QueryDefaultConversation()
    {
      using var conv = await m_Setup.ConversationFactory.Conversation(BenchmarkSetup.CONNECTION_NAME);
      var entities = await conv.Query<BenchmarkEntity, Guid>()
        .ToListAsync().ConfigureAwait(false);
    }
    
    [Benchmark]
    public async Task QueryStatelessConversation()
    {
      using var conv = await m_Setup.ConversationFactory.StatelessReadOnlyConversation(BenchmarkSetup.CONNECTION_NAME);
      var entities = await conv.Query<BenchmarkEntity, Guid>()
        .ToListAsync().ConfigureAwait(false);
    }
    
    [Benchmark]
    public async Task QueryStatelessConversationSetReadonlyInQuery()
    {
      using var conv = await m_Setup.ConversationFactory.StatelessReadOnlyConversation(BenchmarkSetup.CONNECTION_NAME);
      var entities = await conv.Query<BenchmarkEntity, Guid>()
        .WithOptions(r => r.SetReadOnly(true))
        .ToListAsync().ConfigureAwait(false);
    }
  }
}