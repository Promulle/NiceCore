using System;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using NiceCore.Storage.Conversations;
using NiceCore.Storage.Conversations.Factories.Extensions;
using NiceCore.Storage.NH.Benchmarks.Models;
using NiceCore.Storage.NH.Benchmarks.Setup;
using NiceCore.Storage.NH.Conversation;

namespace NiceCore.Storage.NH.Benchmarks.Benchmarks
{
  [MemoryDiagnoser(false)]
  public class ConversationOneConversationQueryBenchmarks
  {
    private readonly BenchmarkSetup m_Setup = new();
    private IReadonlyConversation m_ReadonlyConversation;
    private IReadonlyConversation m_StatelessReadOnlyConversation;
    private IConversation m_Conversation;
    
    [GlobalSetup]
    public async Task Setup()
    {
      await m_Setup.Setup();
      m_Conversation = await m_Setup.ConversationFactory.Conversation(BenchmarkSetup.CONNECTION_NAME).ConfigureAwait(false);
      m_StatelessReadOnlyConversation = await m_Setup.ConversationFactory.StatelessReadOnlyConversation(BenchmarkSetup.CONNECTION_NAME).ConfigureAwait(false);
    }
    
    [Benchmark]
    public async Task QueryDefaultConversation()
    {
      var entities = await m_Conversation.Query<BenchmarkEntity, Guid>()
        .ToListAsync().ConfigureAwait(false);
    }
    
    [Benchmark]
    public async Task QueryStatelessConversation()
    {
      var entities = await m_StatelessReadOnlyConversation.Query<BenchmarkEntity, Guid>()
        .ToListAsync().ConfigureAwait(false);
    }
    
    [Benchmark]
    public async Task QueryReadonlyConversation()
    {
      var entities = await m_ReadonlyConversation.Query<BenchmarkEntity, Guid>()
        .ToListAsync().ConfigureAwait(false);
    }

    [GlobalCleanup]
    public void Cleanup()
    {
      m_Conversation.Dispose();
      m_ReadonlyConversation.Dispose();
      m_StatelessReadOnlyConversation.Dispose();
    }
  }
}