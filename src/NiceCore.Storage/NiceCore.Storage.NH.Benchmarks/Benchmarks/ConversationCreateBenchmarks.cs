using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using NiceCore.Storage.Conversations.Factories.Extensions;
using NiceCore.Storage.NH.Benchmarks.Setup;

namespace NiceCore.Storage.NH.Benchmarks.Benchmarks
{
  [MemoryDiagnoser(false)]
  public class ConversationCreateBenchmarks
  {
    private readonly BenchmarkSetup m_Setup = new();
    
    [GlobalSetup]
    public Task Setup()
    {
      return m_Setup.Setup();
    }
    
    [Benchmark]
    public async Task CreateDefaultConversation()
    {
      using var conv = await m_Setup.ConversationFactory.Conversation(BenchmarkSetup.CONNECTION_NAME);
    }
    
    [Benchmark]
    public async Task CreateStatelessConversation()
    {
      using var conv = await m_Setup.ConversationFactory.StatelessReadOnlyConversation(BenchmarkSetup.CONNECTION_NAME);
    }
  }
}