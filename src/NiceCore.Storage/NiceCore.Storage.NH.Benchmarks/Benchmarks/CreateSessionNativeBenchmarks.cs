using System.Threading;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using NiceCore.Storage.Connections;
using NiceCore.Storage.NH.Benchmarks.Setup;
using NiceCore.Storage.NH.Conversation.Sessions;
using NiceCore.Storage.NH.SessionFactories;

namespace NiceCore.Storage.NH.Benchmarks.Benchmarks
{
  [MemoryDiagnoser(false)]
  public class CreateSessionNativeBenchmarks
  {
    private readonly BenchmarkSetup m_Setup = new();
    private NcNativeSessionProvider m_Provider;
    private IConnectionInfo m_ConnectionInfo;
    
    [GlobalSetup]
    public async Task Setup()
    {
      await m_Setup.Setup();
      var sessionFactoriesService = m_Setup.Container.Resolve<ISessionFactoriesService>();
      var sessionCreationInfo = await sessionFactoriesService.GetOrInitializeFactory(BenchmarkSetup.CONNECTION_NAME, null, false).ConfigureAwait(false);
      m_Provider = sessionCreationInfo.NativeSessionProvider;
      m_ConnectionInfo = sessionCreationInfo.ConnectionInfo;
    }

    [Benchmark]
    public async Task CreateSession()
    {
      using var session = await m_Provider.CreateSession(null, m_ConnectionInfo, CancellationToken.None).ConfigureAwait(false);
    }
    
    [Benchmark]
    public async Task CreateStatelessSession()
    {
      using var session = await m_Provider.CreateStatelessSession(m_ConnectionInfo, CancellationToken.None).ConfigureAwait(false);
    }
  }
}