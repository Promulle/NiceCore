﻿using NiceCore.Hosting.Services;
using NiceCore.Rapid.Storage;

namespace NiceCore.Rapid.Hosting
{
  /// <summary>
  /// Interface for application used for hosting
  /// </summary>
  public interface INcHostingApplication : INcStorageApplication
  {
    /// <summary>
    /// HostService
    /// </summary>
    IHostService HostService { get; set; }
  }
}