﻿using NiceCore.Hosting.Services;
using NiceCore.Rapid.Storage;
using System.Threading.Tasks;

namespace NiceCore.Rapid.Hosting
{
  /// <summary>
  /// Application class for projects using hosting
  /// </summary>
  public class NcHostingApplication : NcStorageApplication, INcHostingApplication
  {
    /// <summary>
    /// HostService
    /// </summary>
    public IHostService HostService { get; set; }

    /// <summary>
    /// Runs hostservice
    /// </summary>
    protected override async Task InternalRunBlocking(TaskCompletionSource i_Source)
    {
      base.InternalRun();
      if (HostService != null)
      {
        await HostService.Run(i_Source).ConfigureAwait(false);
      }
    }
  }
}
