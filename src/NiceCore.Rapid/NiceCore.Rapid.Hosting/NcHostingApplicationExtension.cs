﻿using System.Linq;
using NiceCore.Hosting.Services;
using NiceCore.Rapid.Application;
using NiceCore.Rapid.ApplicationBuilding;
using NiceCore.Rapid.PropertyBuilding;
using NiceCore.ServiceLocation.Discovery.ServiceDescriptors;

namespace NiceCore.Rapid.Hosting
{
  /// <summary>
  /// Extension for hosting application
  /// </summary>
  public class NcHostingApplicationExtension<TApp> : IAppBuilderExtension<TApp>
     where TApp : INcHostingApplication
  {
    /// <summary>
    /// HostService
    /// </summary>
    public IPropertyBuilder<IHostService, TApp> HostService { get; set; }

    /// <summary>
    /// Debug
    /// </summary>
    public bool Debug { get; set; }

    /// <summary>
    /// Default constructor
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_Extensions"></param>
    public NcHostingApplicationExtension(IApplicationBuilder<TApp> i_Instance, IAppBuilderExtensionStore i_Extensions)
    {
      HostService = i_Extensions.GetOrCreate(() => new DefaultPropertyBuilder<IHostService, TApp>(i_Instance));
    }

    /// <summary>
    /// Registers the services for this extension
    /// </summary>
    /// <param name="t_Collection"></param>
    public void RegisterServices(NcServiceCollection t_Collection)
    {
      HostService.Register(t_Collection);
    }

    /// <summary>
    /// BuildStarted EventHandler
    /// </summary>
    /// <param name="i_Builder"></param>
    public void OnBuildStarted(IApplicationBuilder<TApp> i_Builder)
    {
    }

    /// <summary>
    /// properties initialized EventHandler
    /// </summary>
    /// <param name="i_Builder"></param>
    public void OnPropertiesInitialized(IApplicationBuilder<TApp> i_Builder)
    {
      var hostService = HostService.BuildInstance(i_Builder.ServiceLocation.GetContainer());
      hostService.Initialize();
    }

    /// <summary>
    /// PropertiesSet EventHandler
    /// </summary>
    /// <param name="i_App"></param>
    public void OnPropertiesSet(TApp i_App)
    {
      i_App.HostService = HostService.GetInstance();
    }

    /// <summary>
    /// Event fired when an app has been registered with the container
    /// </summary>
    public void OnApplicationRegistered(TApp i_App, NcServiceCollection t_Collection)
    {
      var existingDescriptor = t_Collection.FirstOrDefault(r => r.InterfaceType == typeof(INcApplication));
      if (existingDescriptor != null)
      {
        t_Collection.Remove(existingDescriptor);
        var newDescriptor = existingDescriptor.WithNewAlias(new(existingDescriptor.InterfaceType, typeof(INcHostingApplication)));
        t_Collection.Add(newDescriptor);
      }
    }
  }
}
