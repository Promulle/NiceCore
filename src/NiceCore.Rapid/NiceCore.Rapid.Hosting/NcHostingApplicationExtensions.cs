﻿using NiceCore.Hosting.Services;
using NiceCore.Rapid.ApplicationBuilding;
using NiceCore.Rapid.PropertyBuilding;

namespace NiceCore.Rapid.Hosting
{
  /// <summary>
  /// Extension class holding extension methods
  /// </summary>
  public static class NcHostingApplicationExtensions
  {
    /// <summary>
    /// Enables set of HostService property for HostApps
    /// </summary>
    /// <typeparam name="TApp"></typeparam>
    /// <param name="i_AppBuilder"></param>
    /// <param name="i_Extensions"></param>
    /// <returns></returns>
    public static IPropertyBuilder<IHostService, TApp> HostService<TApp>(this IApplicationBuilder<TApp> i_AppBuilder, IAppBuilderExtensionStore i_Extensions)
      where TApp : INcHostingApplication
    {
      var ext = new NcHostingApplicationExtension<TApp>(i_AppBuilder, i_Extensions);
      i_AppBuilder.AddExtension(ext);
      return ext.HostService;
    }
  }
}
