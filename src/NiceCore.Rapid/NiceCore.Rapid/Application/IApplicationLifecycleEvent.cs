﻿using System.Threading.Tasks;

namespace NiceCore.Rapid.Application
{
  /// <summary>
  /// Interface for events in the lifecycle of an application instance
  /// </summary>
  public interface IApplicationLifecycleEvent
  {
    /// <summary>
    /// Priority by which events of the same stage are sorted.
    /// Lower number will be executed before higher numbers
    /// </summary>
    int Priority { get; }

    /// <summary>
    /// Stage this event is defined for
    /// </summary>
    ApplicationLifecycleStages Stage { get; }

    /// <summary>
    /// Flag, that defines whether to wait for the end of this execution or not
    /// </summary>
    bool BlocksCurrentStage { get; }

    /// <summary>
    /// Executes the event, could be async
    /// </summary>
    /// <returns></returns>
    Task Execute();
  }
}
