﻿using System.Threading.Tasks;

namespace NiceCore.Rapid.Application
{
  /// <summary>
  /// Class returned by App.Run
  /// </summary>
  public class NcAppBootupInfo
  {
    /// <summary>
    /// Task that is completed once the blocking execution is not over but has entered a "ready" state
    /// </summary>
    public Task AppStartedBlocking { get; set; }

    /// <summary>
    /// Task that can be awaited to keep the app alive as long as any blocking execution is running
    /// </summary>
    public Task AppRunning { get; set; }
  }
}
