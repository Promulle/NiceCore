﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NiceCore.Rapid.Application
{
  /// <summary>
  /// Helper class for running lifecycle events
  /// </summary>
  public static class LifecycleEventsRunner
  {
    /// <summary>
    /// Run lifecycle events
    /// </summary>
    /// <param name="i_Stage"></param>
    /// <param name="i_LifecycleEvents"></param>
    /// <returns></returns>
    public static Task RunLifecycleEvents(ApplicationLifecycleStages i_Stage, IDictionary<ApplicationLifecycleStages, List<IApplicationLifecycleEvent>> i_LifecycleEvents)
    {
      return RunLifecycleEvents(i_Stage, i_LifecycleEvents, r => true);
    }

    /// <summary>
    /// Run lifecycle events
    /// </summary>
    /// <param name="i_Stage"></param>
    /// <param name="i_LifecycleEvents"></param>
    /// <param name="i_Predicate"></param>
    /// <returns></returns>
    public static async Task RunLifecycleEvents(ApplicationLifecycleStages i_Stage, IDictionary<ApplicationLifecycleStages, List<IApplicationLifecycleEvent>> i_LifecycleEvents, Func<IApplicationLifecycleEvent, bool> i_Predicate)
    {
      if (i_LifecycleEvents.TryGetValue(i_Stage, out var events))
      {
        foreach (var evt in events.Where(r => i_Predicate.Invoke(r)).OrderBy(r => r.Priority))
        {
          if (evt.BlocksCurrentStage)
          {
            await evt.Execute().ConfigureAwait(false);
          }
          else
          {
#pragma warning disable CS4014 // await here is not needed since this is already done if BlocksCurrentStage is true
            evt.Execute().ConfigureAwait(false);
#pragma warning restore CS4014 // await here is not needed since this is already done if BlocksCurrentStage is true
          }
        }
      }
    }
  }
}
