﻿using NiceCore.Eventing;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using NiceCore.Services.Scheduling;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NiceCore.Rapid.Application.Content;

namespace NiceCore.Rapid.Application
{
  /// <summary>
  /// Default impl of INcApplication
  /// </summary>
  public class DefaultNcApplication : INcApplication
  {
    private readonly List<Func<INcApplication, ValueTask>> m_StartFunctions = new();
    private readonly List<Func<INcApplication, ValueTask>> m_StopFunctions = new();
    
    /// <summary>
    /// ServiceContainer
    /// </summary>
    public IServiceContainer ServiceContainer { get; set; }

    /// <summary>
    /// Configuration of application
    /// </summary>
    public INcConfiguration Configuration { get; set; }

    /// <summary>
    /// Version
    /// </summary>
    public Version ApplicationVersion { get; set; }

    /// <summary>
    /// Whether this app is running or not
    /// </summary>
    public bool IsRunning { get; protected set; }

    /// <summary>
    /// Is TestMode
    /// </summary>
    public bool IsTestMode { get; set; }

    /// <summary>
    /// Host for scheduled services
    /// </summary>
    public IScheduleServiceHost ScheduleServiceHost { get; set; }

    /// <summary>
    /// Debug Mode
    /// </summary>
    public bool DebugMode { get; }
    
    /// <summary>
    /// Adapter for eventing
    /// </summary>
    public IEventingAdapter EventingAdapter { get; set; }

    /// <summary>
    /// Application files
    /// </summary>
    public IApplicationFileResolver ApplicationFiles { get; set; }

    /// <summary>
    /// All lifecycle events
    /// </summary>
    public Dictionary<ApplicationLifecycleStages, List<IApplicationLifecycleEvent>> LifecycleEvents { get; set; }

    /// <summary>
    /// Runs app
    /// </summary>
    public async Task<NcAppBootupInfo> Run()
    {
      var startedBlockingSource = new TaskCompletionSource();
      await LifecycleEventsRunner.RunLifecycleEvents(ApplicationLifecycleStages.Starting, LifecycleEvents).ConfigureAwait(false);
      if (!IsRunning && InternalRun())
      {
        IsRunning = true;
        await RunInternalSystemsOnStart().ConfigureAwait(false);
        await LifecycleEventsRunner.RunLifecycleEvents(ApplicationLifecycleStages.Started, LifecycleEvents).ConfigureAwait(false);
      }
      //Start things that block only after the app has been declared as started
      var keepAliveTask = InternalRunBlocking(startedBlockingSource);
      return new NcAppBootupInfo()
      {
        AppRunning = keepAliveTask,
        AppStartedBlocking = startedBlockingSource.Task,
      };
    }

    private async ValueTask RunInternalSystemsOnStart()
    {
      foreach (var startFunction in m_StartFunctions)
      {
        try
        {
          await startFunction.Invoke(this).ConfigureAwait(false);
        }
        catch (Exception )
        {
          
        }
      }
      m_StartFunctions.Clear();
    }
    
    private async ValueTask RunInternalSystemsOnShutdown()
    {
      foreach (var stopFunction in m_StopFunctions)
      {
        try
        {
          await stopFunction.Invoke(this).ConfigureAwait(false);
        }
        catch (Exception )
        {
          
        }
      }
      m_StopFunctions.Clear();
    }
    
    /// <summary>
    /// Queue Run On Start
    /// </summary>
    /// <param name="i_Func"></param>
    public void QueueRunOnStart(Func<INcApplication, ValueTask> i_Func)
    {
      m_StartFunctions.Add(i_Func);
    }

    /// <summary>
    /// Queue Run On Shutdown
    /// </summary>
    /// <param name="i_Func"></param>
    public void QueueRunOnShutDown(Func<INcApplication, ValueTask> i_Func)
    {
      m_StopFunctions.Add(i_Func);
    }

    /// <summary>
    /// Shuts app down
    /// </summary>
    public async Task Shutdown()
    {
      await LifecycleEventsRunner.RunLifecycleEvents(ApplicationLifecycleStages.Shuttingdown, LifecycleEvents).ConfigureAwait(false);
      if (IsRunning && InternalShutdown())
      {
        IsRunning = false;
        await RunInternalSystemsOnShutdown().ConfigureAwait(false);
        await LifecycleEventsRunner.RunLifecycleEvents(ApplicationLifecycleStages.Shutdown, LifecycleEvents).ConfigureAwait(false);
      }
    }

    /// <summary>
    /// Run function to override
    /// </summary>
    protected virtual bool InternalRun()
    {
      return true;
    }

    /// <summary>
    /// Shutdown to override
    /// </summary>
    protected virtual bool InternalShutdown()
    {
      Configuration?.Dispose();
      return true;
    }

    /// <summary>
    /// Start things that are blocking
    /// </summary>
    /// <returns></returns>
    protected virtual Task InternalRunBlocking(TaskCompletionSource i_Source)
    {
      return Task.CompletedTask;
    }
  }
}
