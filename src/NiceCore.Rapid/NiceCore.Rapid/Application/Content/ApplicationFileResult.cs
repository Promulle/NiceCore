namespace NiceCore.Rapid.Application.Content
{
  /// <summary>
  /// Result of a file resolve from application directories
  /// </summary>
  public class ApplicationFileResult
  {
    /// <summary>
    /// Found
    /// </summary>
    public bool Found => FullFilePath != null;

    /// <summary>
    /// The relative path that was searched for
    /// </summary>
    public string RelativeFilePath { get; init; }
    
    /// <summary>
    /// Directory the file was found in
    /// </summary>
    public IApplicationContentDirectory ResultDirectory { get; init; }
    
    /// <summary>
    /// Full file path that was resolved
    /// </summary>
    public string FullFilePath { get; init; }
  }
}