namespace NiceCore.Rapid.Application.Content
{
  /// <summary>
  /// A single directory used to store application content (dlls, resources)
  /// </summary>
  public class ApplicationContentDirectory : IApplicationContentDirectory
  {
    /// <summary>
    /// The directory hosting the content
    /// </summary>
    public string Directory { get; set; }
    
    /// <summary>
    /// Whether this can be used if a file does not fit all specific wildcards. (this is seen as true if Wildcard == null)
    /// </summary>
    public bool UseForGenericWildcard { get; set; }

    /// <summary>
    /// An optional name for this content directory
    /// </summary>
    public string Name { get; set; }
    
    /// <summary>
    /// Wildcard that is preferred to be replaced by this directory.
    /// If this is null, the default wildcard "content" will be used
    /// </summary>
    public string Wildcard { get; set; }
    
    /// <summary>
    /// Whether this directory can be used to search for registrations in dll/exe files
    /// </summary>
    public bool CanBeUsedAsRegistrationDiscoveryDirectory { get; set; }
  }
}