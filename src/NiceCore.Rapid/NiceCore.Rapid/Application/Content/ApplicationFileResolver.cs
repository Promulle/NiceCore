using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using NiceCore.ServiceLocation;

namespace NiceCore.Rapid.Application.Content
{
  /// <summary>
  /// Impl of IApplicationFileResolver
  /// </summary>
  [Register(InterfaceType = typeof(IApplicationFileResolver), LifeStyle = LifeStyles.Singleton)]
  public class ApplicationFileResolver : IApplicationFileResolver
  {
    /// <summary>
    /// Content Directories
    /// </summary>
    public IList<IApplicationContentDirectory> ContentDirectories { get; set; }

    /// <summary>
    /// Resolve file
    /// </summary>
    /// <param name="i_RelativeFile"></param>
    /// <returns></returns>
    public ApplicationFileResult ResolveRelativeFile(string i_RelativeFile)
    {
      if (ContentDirectories != null && ContentDirectories.Count > 0)
      {
        var wildCard = GetWildcardInPath(i_RelativeFile);
        if (wildCard != null)
        {
          var directory = ContentDirectories.FirstOrDefault(r => r.Wildcard != null && r.Wildcard == wildCard);
          if (directory != null)
          {
            var resultFile = ResolveWithDirectory(i_RelativeFile, wildCard, directory);
            if (resultFile != null)
            {
              return resultFile;
            }
          }
          foreach (var dir in ContentDirectories.Where(r => CanBeUsedForGenericWildcard(r)))
          {
            var file = ResolveWithDirectory(i_RelativeFile, wildCard, dir);
            if (file != null)
            {
              return file;
            }
          }
        }
      }

      return new()
      {
        RelativeFilePath = i_RelativeFile,
        FullFilePath = i_RelativeFile
      };
    }

    private static bool CanBeUsedForGenericWildcard(IApplicationContentDirectory i_Directory)
    {
      return i_Directory.Wildcard == null || i_Directory.UseForGenericWildcard;
    }

    private static ApplicationFileResult ResolveWithDirectory(string i_RelativeFile, string i_Wildcard, IApplicationContentDirectory i_Directory)
    {
      var file = ResolveFilePathWithDirectory(i_RelativeFile, i_Wildcard, i_Directory);
      if (File.Exists(file))
      {
        return new ApplicationFileResult
        {
          ResultDirectory = i_Directory,
          FullFilePath = file,
          RelativeFilePath = i_RelativeFile
        };
      }

      return null;
    }

    private static string ResolveFilePathWithDirectory(string i_RelativePath, string i_Wildcard, IApplicationContentDirectory i_Directory)
    {
      var directory = i_Directory.Directory;
      if (directory.EndsWith(Path.DirectorySeparatorChar))
      {
        directory = directory[..^1];
      }

      return i_RelativePath.Replace(i_Wildcard, directory);
    }

    private static string GetWildcardInPath(string i_RelativePath)
    {
      var match = Regex.Match(i_RelativePath, "<\\w*>");
      if (match.Success)
      {
        return match.Value;
      }

      return i_RelativePath.StartsWith(".") ? "." : null;
    }
  }
}