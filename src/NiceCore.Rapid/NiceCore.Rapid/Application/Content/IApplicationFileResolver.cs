namespace NiceCore.Rapid.Application.Content
{
  /// <summary>
  /// Interface for service used by app to resolve relative files
  /// </summary>
  public interface IApplicationFileResolver
  {
    /// <summary>
    /// Resolves the relative file using available application content directories
    /// </summary>
    /// <param name="i_RelativeFile"></param>
    /// <returns></returns>
    ApplicationFileResult ResolveRelativeFile(string i_RelativeFile);
  }
}