namespace NiceCore.Rapid.Application.Content
{
  /// <summary>
  /// Static constant class for application files
  /// </summary>
  public static class AppFile
  {
    /// <summary>
    /// The default wildcard to use
    /// </summary>
    public static readonly string DEFAULT_WILDCARD = "<content>";
  }
}