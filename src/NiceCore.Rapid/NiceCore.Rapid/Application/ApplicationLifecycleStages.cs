﻿namespace NiceCore.Rapid.Application
{
  /// <summary>
  /// Available stages of an application lifecycle
  /// </summary>
  public enum ApplicationLifecycleStages
  {
    /// <summary>
    /// App.Run has just been called
    /// </summary>
    Starting,

    /// <summary>
    /// All things that were executed in App.Run have finished (possibly excluding async event executions)
    /// </summary>
    Started,

    /// <summary>
    /// App.Shutdown has just been called
    /// </summary>
    Shuttingdown,

    /// <summary>
    /// All things that were executed in App.Shutdown have finished (possibly excluding async event executions)
    /// </summary>
    Shutdown
  }
}
