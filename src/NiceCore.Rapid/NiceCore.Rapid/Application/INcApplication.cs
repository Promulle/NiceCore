﻿using NiceCore.Eventing;
using NiceCore.Rapid.Application.Content;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using NiceCore.Services.Scheduling;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NiceCore.Rapid.Application
{
  /// <summary>
  /// Interface for application
  /// </summary>
  public interface INcApplication
  {
    /// <summary>
    /// Container for services
    /// </summary>
    IServiceContainer ServiceContainer { get; set; }

    /// <summary>
    /// Configuration of app
    /// </summary>
    INcConfiguration Configuration { get; set; }

    /// <summary>
    /// Host of scheduled services
    /// </summary>
    IScheduleServiceHost ScheduleServiceHost { get; set; }

    /// <summary>
    /// Adapter for eventing
    /// </summary>
    IEventingAdapter EventingAdapter { get; set; }

    /// <summary>
    /// Version
    /// </summary>
    Version ApplicationVersion { get; set; }

    /// <summary>
    /// Application files used for resolving files in app content roots
    /// </summary>
    IApplicationFileResolver ApplicationFiles { get; set; }

    /// <summary>
    /// All lifecycle events
    /// </summary>
    Dictionary<ApplicationLifecycleStages, List<IApplicationLifecycleEvent>> LifecycleEvents { get; set; }

    /// <summary>
    /// Whether the app is running or not
    /// </summary>
    bool IsRunning { get; }
    
    /// <summary>
    /// Whether this app is running in a test-mode. Only use to keep things from running in tests
    /// </summary>
    bool IsTestMode { get; set; }

    /// <summary>
    /// Whether the app has been build in debug mode
    /// </summary>
    bool DebugMode { get; }

    /// <summary>
    /// Run on Start
    /// </summary>
    void QueueRunOnStart(Func<INcApplication, ValueTask> i_Func);
    
    /// <summary>
    /// Run On Shutdown
    /// </summary>
    void QueueRunOnShutDown(Func<INcApplication, ValueTask> i_Func);

    /// <summary>
    /// Runs all components that should be run
    /// </summary>
    Task<NcAppBootupInfo> Run();

    /// <summary>
    /// Meant to be called when the app stops or is going to be restarted
    /// </summary>
    Task Shutdown();
  }
}
