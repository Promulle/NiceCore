﻿using NiceCore.Rapid.Application;
using NiceCore.Rapid.ApplicationBuilding;
using NiceCore.Rapid.ApplicationBuilding.Impl;
using System;

namespace NiceCore.Rapid
{
  /// <summary>
  /// static nicecore application class used for initializing
  /// </summary>
  public static class NcApplicationBuilder
  {
    /// <summary>
    /// Creates an application usable for configuring nicecore
    /// </summary>
    /// <returns></returns>
    public static DefaultApplicationBuilder Create ()
    {
      var builder = new DefaultApplicationBuilder();
      return builder;
    }
    /// <summary>
    /// Generic ApplicationBuilder Create method so fluent can be used with custom builder classes
    /// </summary>
    /// <typeparam name="TApp"></typeparam>
    /// <returns></returns>
    public static IApplicationBuilder<TApp> Create<TApp> ()
      where TApp : INcApplication
    {
      var builder = Activator.CreateInstance<NcApplicationBuilder<TApp>>();
      return builder;
    }
  }
}
