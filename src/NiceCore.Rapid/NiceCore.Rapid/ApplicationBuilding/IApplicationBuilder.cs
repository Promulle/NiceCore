﻿using System.Threading.Tasks;
using NiceCore.Eventing;
using NiceCore.Rapid.Application;
using NiceCore.Rapid.Application.Content;
using NiceCore.Rapid.ApplicationBuilding.Extensions;
using NiceCore.Rapid.Logging;
using NiceCore.Rapid.PropertyBuilding;
using NiceCore.Rapid.PropertyBuilding.Specific;
using NiceCore.ServiceLocation;
using NiceCore.ServiceLocation.Discovery.ServiceDescriptors;
using NiceCore.Services.Scheduling;

namespace NiceCore.Rapid.ApplicationBuilding
{
  /// <summary>
  /// Base interface for application builder only delivers InitAndConfigure Method
  /// </summary>
  /// <typeparam name="TApp"></typeparam>
  public interface IApplicationBuilder<TApp> where TApp : INcApplication
  {
    /// <summary>
    /// Version of application
    /// </summary>
    VersionPropertyBuilder<TApp> ApplicationVersion { get; }

    /// <summary>
    /// Configuration
    /// </summary>
    ConfigurationPropertyBuilder<TApp> Configuration { get; }

    /// <summary>
    /// Container
    /// </summary>
    ServiceLocationPropertyBuilder<TApp> ServiceLocation { get; }

    /// <summary>
    /// Logger
    /// </summary>
    ApplicationLoggingSetup<TApp> Logging { get; }

    /// <summary>
    /// Service host for scheduled services
    /// </summary>
    IPropertyBuilder<IScheduleServiceHost, TApp> ScheduleServiceHost { get; }

    /// <summary>
    /// Eventing adapter property builder
    /// </summary>
    IPropertyBuilder<IEventingAdapter, TApp> EventingAdapter { get; }

    /// <summary>
    /// Lifecycle events
    /// </summary>
    ICollectionPropertyBuilder<IApplicationLifecycleEvent, TApp> LifecycleEvents { get; }

    /// <summary>
    /// Before Services Registered
    /// </summary>
    ExecuteFuncPropertyBuilder<TApp, NcServiceCollection> BeforeServicesRegistered { get; }

    /// <summary>
    /// After Services Registered (logging will not be registered at this point)
    /// </summary>
    ExecuteFuncPropertyBuilder<TApp, NcServiceCollection> AfterServicesRegistered { get; }

    /// <summary>
    /// Application Content Directories
    /// </summary>
    IPropertyBuilder<IApplicationContentDirectory, TApp> ApplicationContentDirectories { get; }

    /// <summary>
    /// Adds an extension to the appbuilder
    /// </summary>
    /// <param name="i_Extension"></param>
    void AddExtension(IAppBuilderExtension<TApp> i_Extension);

    /// <summary>
    /// Add Post Build Extension
    /// </summary>
    /// <param name="i_Extension"></param>
    void AddPostBuildExtension(IPostBuildAppBuilderExtension<TApp> i_Extension);

    /// <summary>
    /// Creates the app and returns it
    /// </summary>
    /// <returns></returns>
    ValueTask<TApp> Build();

    /// <summary>
    /// Enable debugging of this app builder
    /// </summary>
    /// <returns></returns>
    IApplicationBuilder<TApp> EnableDebug();

    /// <summary>
    /// Enable debugging of this app builder
    /// </summary>
    /// <returns></returns>
    IApplicationBuilder<TApp> EnableTestMode();

    /// <summary>
    /// Disable debgging of this app builder
    /// </summary>
    /// <returns></returns>
    IApplicationBuilder<TApp> DisableDebug();
  }
}