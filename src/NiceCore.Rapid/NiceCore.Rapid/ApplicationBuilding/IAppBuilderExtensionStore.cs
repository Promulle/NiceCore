﻿using NiceCore.Rapid.Application;
using NiceCore.Rapid.PropertyBuilding;
using System;

namespace NiceCore.Rapid.ApplicationBuilding
{
  /// <summary>
  /// Store for keeping the instances for extension properties. One Instance of this should be used per application build -> new application build = new Extension store
  /// </summary>
  public interface IAppBuilderExtensionStore
  {
    /// <summary>
    /// Retruns the property builder for TPropertyValue and the app
    /// </summary>
    /// <typeparam name="TPropertyValue"></typeparam>
    /// <typeparam name="TApp"></typeparam>
    /// <returns></returns>
    IPropertyBuilder<TPropertyValue, TApp> GetOrCreate<TPropertyValue, TApp>(Func<IPropertyBuilder<TPropertyValue, TApp>> i_Supplier) where TApp : INcApplication;
  }
}
