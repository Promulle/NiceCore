﻿using NiceCore.Rapid.Application;
using NiceCore.ServiceLocation.Discovery.ServiceDescriptors;

namespace NiceCore.Rapid.ApplicationBuilding
{
  /// <summary>
  /// Extension that is used by the appbuilder
  /// </summary>
  public interface IAppBuilderExtension<TApp>
    where TApp : INcApplication
  {
    /// <summary>
    /// Whether the extension should be debugged
    /// </summary>
    bool Debug { get; set; }

    /// <summary>
    /// Started build of app
    /// </summary>
    void OnBuildStarted(IApplicationBuilder<TApp> i_Builder);

    /// <summary>
    /// Registers all services the extension might need
    /// </summary>
    /// <param name="t_Collection"></param>
    void RegisterServices(NcServiceCollection t_Collection);

    /// <summary>
    /// properties have been initialized
    /// </summary>
    /// <param name="i_Builder"></param>
    void OnPropertiesInitialized(IApplicationBuilder<TApp> i_Builder);

    /// <summary>
    /// Properties have been set
    /// </summary>
    /// <param name="i_App"></param>
    void OnPropertiesSet(TApp i_App);

    /// <summary>
    /// Event fired when an app has been registered with the container
    /// </summary>
    /// <param name="i_App"></param>
    /// <param name="t_Collection"></param>
    void OnApplicationRegistered(TApp i_App, NcServiceCollection t_Collection);
  }
}
