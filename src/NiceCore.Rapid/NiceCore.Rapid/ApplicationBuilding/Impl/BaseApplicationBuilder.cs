﻿using NiceCore.Eventing;
using NiceCore.Rapid.Application;
using NiceCore.Rapid.PropertyBuilding;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using NiceCore.Services.Scheduling;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Logging.Factories.Default;
using NiceCore.Rapid.Application.Content;
using NiceCore.Rapid.ApplicationBuilding.Extensions;
using NiceCore.Rapid.Logging;
using NiceCore.Rapid.PropertyBuilding.Specific;
using NiceCore.ServiceLocation.Discovery;
using NiceCore.ServiceLocation.Discovery.ServiceDescriptors;
using NiceCore.ServiceLocation.Extensions;

namespace NiceCore.Rapid.ApplicationBuilding.Impl
{
  /// <summary>
  /// AppBuilder
  /// </summary>
  public abstract class BaseApplicationBuilder<TApp> : IApplicationBuilder<TApp>
    where TApp : INcApplication
  {
    /// <summary>
    /// Appbuilder extensions
    /// </summary>
    protected List<IAppBuilderExtension<TApp>> Extensions { get; set; }

    /// <summary>
    /// Extensions to be run after build
    /// </summary>
    protected List<IPostBuildAppBuilderExtension<TApp>> PostBuildExtensions { get; set; }

    /// <summary>
    /// Whether the debug messages should be output if there are errors during app building
    /// </summary>
    protected bool Debug { get; set; }

    /// <summary>
    /// Whether the debug messages should be output if there are errors during app building
    /// </summary>
    protected bool TestMode { get; set; }

    /// <summary>
    /// Container for service
    /// </summary>
    public ServiceLocationPropertyBuilder<TApp> ServiceLocation { get; protected set; }

    /// <summary>
    /// Configuration property
    /// </summary>
    public ConfigurationPropertyBuilder<TApp> Configuration { get; protected set; }

    /// <summary>
    /// Service host for scheduled services
    /// </summary>
    public IPropertyBuilder<IScheduleServiceHost, TApp> ScheduleServiceHost { get; protected set; }

    /// <summary>
    /// Version of application
    /// </summary>
    public VersionPropertyBuilder<TApp> ApplicationVersion { get; protected set; }

    /// <summary>
    /// Used loggers
    /// </summary>
    public ApplicationLoggingSetup<TApp> Logging { get; protected set; }

    /// <summary>
    /// Eventing adapter
    /// </summary>
    public IPropertyBuilder<IEventingAdapter, TApp> EventingAdapter { get; }

    /// <summary>
    /// Lifecycle events
    /// </summary>
    public ICollectionPropertyBuilder<IApplicationLifecycleEvent, TApp> LifecycleEvents { get; }
    
    /// <summary>
    /// Before Services Registered
    /// </summary>
    public ExecuteFuncPropertyBuilder<TApp, NcServiceCollection> BeforeServicesRegistered { get; }
    
    /// <summary>
    /// After Services Registered (logging will not be registered at this point)
    /// </summary>
    public ExecuteFuncPropertyBuilder<TApp, NcServiceCollection> AfterServicesRegistered { get; }
    
    /// <summary>
    /// Application Content Directories
    /// </summary>
    public IPropertyBuilder<IApplicationContentDirectory, TApp> ApplicationContentDirectories { get; }

    /// <summary>
    /// Default constructor
    /// </summary>
    protected BaseApplicationBuilder()
    {
      ServiceLocation = new(this);
      ApplicationContentDirectories = new DefaultPropertyBuilder<IApplicationContentDirectory, TApp>(this);
      Configuration = new(this);
      ApplicationVersion = new(this);
      Logging = new(this);
      ScheduleServiceHost = new DefaultPropertyBuilder<IScheduleServiceHost, TApp>(this);
      EventingAdapter = new DefaultPropertyBuilder<IEventingAdapter, TApp>(this);
      LifecycleEvents = new DefaultCollectionPropertyBuilder<IApplicationLifecycleEvent, TApp>(this);
      Extensions = new();
      PostBuildExtensions = new();
      BeforeServicesRegistered = new(this);
      AfterServicesRegistered = new(this);
    }

    /// <summary>
    /// Method that returns the app as instance which will get properties added to it
    /// </summary>
    /// <returns></returns>
    protected TApp CreateApp()
    {
      return Activator.CreateInstance<TApp>();
    }

    /// <summary>
    /// Enable debugging of this app builder
    /// </summary>
    /// <returns></returns>
    public IApplicationBuilder<TApp> EnableDebug()
    {
      Debug = true;
      Extensions.ForEach(r => r.Debug = true);
      return this;
    }

    /// <summary>
    /// Enable debugging of this app builder
    /// </summary>
    /// <returns></returns>
    public IApplicationBuilder<TApp> EnableTestMode()
    {
      TestMode = true;
      return this;
    }

    /// <summary>
    /// Disable debgging of this app builder
    /// </summary>
    /// <returns></returns>
    public IApplicationBuilder<TApp> DisableDebug()
    {
      Debug = false;
      Extensions.ForEach(r => r.Debug = false);
      return this;
    }

    /// <summary>
    /// Method that builds the app. Must be overriden
    /// </summary>
    /// <returns></returns>
    public virtual async ValueTask<TApp> Build()
    {
      var app = CreateApp();
      RunExtensionsUtil.RunBuildStarted(this, Extensions);
      var factory = BuildLoggers();
      var services = DiscoverRegistrations();
      await RegisterServices(app, services, factory).ConfigureAwait(false);
      BuildContainerProperty(services, factory);
      await InitProperties(app, factory).ConfigureAwait(false);
      AddProperties(app);
      RunExtensionsUtil.RunSetProperties(app, Extensions);
      await RunPostBuildExtensions(app).ConfigureAwait(false);
      return app;
    }

    private NcServiceCollection DiscoverRegistrations()
    {
      ServiceLocation.InitializeDiscovery();
      var discovery = ServiceLocation.GetDiscovery();
      
      var runConfiguration = ServiceLocation.GetDiscoveryRunConfiguration();
      var collection = discovery.Discover(runConfiguration);
      collection.AddInstance<IDiscoveredNamespaceStorage>(runConfiguration.DiscoveredNamespaceStorage);
      return collection;
    }

    private async ValueTask RegisterServices(TApp i_App, NcServiceCollection t_Collection, ILoggerFactory i_LoggerFactory)
    {
      await BeforeServicesRegistered.Execute(t_Collection).ConfigureAwait(false);
      RegisterApplication(i_App, t_Collection);
      Configuration.Register(t_Collection);
      EventingAdapter.Register(t_Collection);
      LifecycleEvents.Register(t_Collection);
      ScheduleServiceHost.Register(t_Collection);
      ApplicationContentDirectories.Register(t_Collection);
      t_Collection.RegisterDefaultLoggingProviders();
      //in case logging should not be created by configuration, register it here
      if (i_LoggerFactory != null)
      {
        t_Collection.RegisterLogging(i_LoggerFactory);
      }
      RunExtensionsUtil.RunRegisterServices(t_Collection, Extensions);
      RunExtensionsUtil.RunRegisterApplication(i_App, t_Collection, Extensions);
      await AfterServicesRegistered.Execute(t_Collection).ConfigureAwait(false);
    }

    private async ValueTask RunPostBuildExtensions(TApp i_App)
    {
      if (PostBuildExtensions != null)
      {
        foreach (var postBuildExtension in PostBuildExtensions)
        {
          await postBuildExtension.Execute(i_App).ConfigureAwait(false);
        }
      }
    }

    /// <summary>
    /// Adds an extension to the appbuilder
    /// </summary>
    /// <param name="i_Extension"></param>
    public void AddExtension(IAppBuilderExtension<TApp> i_Extension)
    {
      Extensions.Add(i_Extension);
      i_Extension.Debug = Debug;
    }

    /// <summary>
    /// Add Post Build Extension
    /// </summary>
    /// <param name="i_Extension"></param>
    public void AddPostBuildExtension(IPostBuildAppBuilderExtension<TApp> i_Extension)
    {
      PostBuildExtensions.Add(i_Extension);
    }

    /// <summary>
    /// Initializes properties so that they can be set into the app
    /// </summary>
    /// <param name="i_App"></param>
    /// <param name="i_Factory"></param>
    protected virtual async ValueTask InitProperties(TApp i_App, ILoggerFactory i_Factory)
    {
      var container = ServiceLocation.GetContainer();
      var config = Configuration.BuildInstance(container);
      if (config != null)
      {
        if (i_Factory != null)
        {
          var logger = i_Factory.CreateLogger(config.GetType());
          config.SetLogger(logger);
        }

        config.Initialize();
      }

      BuildLoggersAfterContainer(container, config);
      EnsureAtleastDummyFactoryIsRegistered(container);
      var host = ScheduleServiceHost.BuildInstance(container);
      if (host != null)
      {
        await host.Initialize().ConfigureAwait(false);
        i_App.QueueRunOnStart(static async app => { await app.ScheduleServiceHost.StartScheduling().ConfigureAwait(false); });
        i_App.QueueRunOnShutDown(static app =>
        {
          app.ScheduleServiceHost.StopScheduling();
          return ValueTask.CompletedTask;
        });
      }

      ApplicationContentDirectories.BuildInstance(container);
      EventingAdapter.BuildInstance(container);
      LifecycleEvents.BuildInstance(container);
      RunExtensionsUtil.RunInitialize(this, Extensions);
    }

    private void BuildLoggersAfterContainer(IServiceContainer i_Container, INcConfiguration i_Configuration)
    {
      if (Logging.Stage == ApplicationLoggingSetupStage.AfterContainer)
      {
        var factory = Logging.BuildFactory(i_Container, i_Configuration);
        var containerLogger = factory.CreateLogger(i_Container.GetType());
        i_Container.SetLogger(containerLogger);
        i_Container.RegisterLogging(factory);
        var configLogger = factory.CreateLogger(i_Configuration.GetType());
        i_Configuration.SetLogger(configLogger);
      }
    }

    private static void EnsureAtleastDummyFactoryIsRegistered(IServiceContainer t_Container)
    {
      if (!t_Container.HasImplementationOf<ILoggerFactory>())
      {
        t_Container.RegisterLogging(new DummyLoggerFactory());
      }
    }

    private ILoggerFactory BuildLoggers()
    {
      if (Logging.Stage == ApplicationLoggingSetupStage.BeforeContainer)
      {
        return Logging.BuildFactory(null, null);
      }

      return null;
    }

    private void BuildContainerProperty(NcServiceCollection t_Collection, ILoggerFactory i_Factory)
    {
      ServiceLocation.InitializeContainer(t_Collection);
      var container = ServiceLocation.GetContainer();

      if (container != null)
      {
        var containerLogger = i_Factory?.CreateLogger(container.GetType());
        if (containerLogger != null)
        {
          container.SetLogger(containerLogger);
        }
      }
    }

    /// <summary>
    /// Registers i_App into i_Container
    /// override is needed because the interface it should be registered under can vary
    /// </summary>
    /// <param name="i_App"></param>
    /// <param name="t_ServiceCollection"></param>
    protected abstract void RegisterApplication(TApp i_App, NcServiceCollection t_ServiceCollection);

    /// <summary>
    /// Adds properties to an application
    /// </summary>
    /// <param name="i_App"></param>
    protected abstract void AddProperties(TApp i_App);
  }
}