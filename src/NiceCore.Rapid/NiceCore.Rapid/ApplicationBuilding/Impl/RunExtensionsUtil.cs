using System.Collections.Generic;
using NiceCore.Rapid.Application;
using NiceCore.ServiceLocation;
using NiceCore.ServiceLocation.Discovery.ServiceDescriptors;

namespace NiceCore.Rapid.ApplicationBuilding.Impl
{
  /// <summary>
  /// Util to run Extensions
  /// </summary>
  public static class RunExtensionsUtil
  {
    /// <summary>
    /// Run Build Started
    /// </summary>
    /// <param name="i_Builder"></param>
    /// <param name="i_Extensions"></param>
    /// <typeparam name="TApp"></typeparam>
    public static void RunBuildStarted<TApp>(IApplicationBuilder<TApp> i_Builder, IEnumerable<IAppBuilderExtension<TApp>> i_Extensions) where TApp : INcApplication
    {
      foreach (var extension in i_Extensions)
      {
        extension.OnBuildStarted(i_Builder);  
      }
    }

    /// <summary>
    /// Run RegisterServices
    /// </summary>
    /// <param name="t_Collection"></param>
    /// <param name="i_Extensions"></param>
    /// <typeparam name="TApp"></typeparam>
    public static void RunRegisterServices<TApp>(NcServiceCollection t_Collection, IEnumerable<IAppBuilderExtension<TApp>> i_Extensions) where TApp : INcApplication
    {
      foreach (var extension in i_Extensions)
      {
        extension.RegisterServices(t_Collection);
      }
    }

    /// <summary>
    /// Run Initialize
    /// </summary>
    /// <param name="i_Builder"></param>
    /// <param name="i_Extensions"></param>
    /// <typeparam name="TApp"></typeparam>
    public static void RunInitialize<TApp>(IApplicationBuilder<TApp> i_Builder, IEnumerable<IAppBuilderExtension<TApp>> i_Extensions) where TApp : INcApplication
    {
      foreach (var extension in i_Extensions)
      {
        extension.OnPropertiesInitialized(i_Builder);
      }
    }

    /// <summary>
    /// Run SetProperties
    /// </summary>
    /// <param name="i_App"></param>
    /// <param name="i_Extensions"></param>
    /// <typeparam name="TApp"></typeparam>
    public static void RunSetProperties<TApp>(TApp i_App, IEnumerable<IAppBuilderExtension<TApp>> i_Extensions) where TApp : INcApplication
    {
      foreach (var extension in i_Extensions)
      {
        extension.OnPropertiesSet(i_App);
      }
    }

    /// <summary>
    /// Run RegisterApplication
    /// </summary>
    /// <param name="i_App"></param>
    /// <param name="t_Collection"></param>
    /// <param name="i_Extensions"></param>
    /// <typeparam name="TApp"></typeparam>
    public static void RunRegisterApplication<TApp>(TApp i_App, NcServiceCollection t_Collection, IEnumerable<IAppBuilderExtension<TApp>> i_Extensions) where TApp : INcApplication
    {
      foreach (var extension in i_Extensions)
      {
        extension.OnApplicationRegistered(i_App, t_Collection);
      }
    }
  }
}