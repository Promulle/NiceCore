﻿using NiceCore.Extensions;
using NiceCore.Rapid.Application;
using NiceCore.Rapid.Application.Content;
using NiceCore.ServiceLocation;
using System.Collections.Generic;
using System.Linq;
using NiceCore.ServiceLocation.Discovery;
using NiceCore.ServiceLocation.Discovery.ServiceDescriptors;

namespace NiceCore.Rapid.ApplicationBuilding.Impl
{
  /// <summary>
  /// Default impl of IAppBootstrapper
  /// </summary>
  public class NcApplicationBuilder<TApp> : BaseApplicationBuilder<TApp>
    where TApp : INcApplication
  {
    /// <summary>
    /// Adding of properties needed by default app
    /// </summary>
    /// <param name="t_App"></param>
    protected override void AddProperties(TApp t_App)
    {
      t_App.ServiceContainer = ServiceLocation.GetContainer();
      t_App.ApplicationVersion = ApplicationVersion.GetVersion();
      t_App.Configuration = Configuration.GetInstance();
      t_App.ScheduleServiceHost = ScheduleServiceHost.GetInstance();
      t_App.EventingAdapter = EventingAdapter.GetInstance();
      t_App.LifecycleEvents = GroupEvents(LifecycleEvents.GetInstance());
      t_App.IsTestMode = TestMode;
      SetFileResolver(t_App, ServiceLocation.GetContainer());
    }

    private void SetFileResolver(TApp t_App, IServiceContainer i_Container)
    {
      if (i_Container != null)
      {
        t_App.ApplicationFiles = i_Container.Resolve<IApplicationFileResolver>();
      }
    }
    
    /// <summary>
    /// Registers app
    /// </summary>
    /// <param name="i_App"></param>
    /// <param name="t_Collection"></param>
    protected override void RegisterApplication(TApp i_App, NcServiceCollection t_Collection)
    {
      t_Collection.AddInstance<INcApplication>(i_App);
    }

    private static Dictionary<ApplicationLifecycleStages, List<IApplicationLifecycleEvent>> GroupEvents(IEnumerable<IApplicationLifecycleEvent> i_Events)
    {
      return i_Events.GroupBy(r => r.Stage).ToDictionary(g => g.Key, r => r.ToList());
    }
  }
}
