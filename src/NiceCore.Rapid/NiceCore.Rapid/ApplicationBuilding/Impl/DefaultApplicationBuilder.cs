﻿using NiceCore.Rapid.Application;

namespace NiceCore.Rapid.ApplicationBuilding.Impl
{
  /// <summary>
  /// Default impl of IAppBootstrapper
  /// </summary>
  public class DefaultApplicationBuilder : NcApplicationBuilder<DefaultNcApplication>
  {
  }
}
