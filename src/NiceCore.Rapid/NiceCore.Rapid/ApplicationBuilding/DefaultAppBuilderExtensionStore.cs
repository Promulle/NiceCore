﻿using NiceCore.Base.Caching;
using NiceCore.Rapid.Application;
using NiceCore.Rapid.PropertyBuilding;
using System;

namespace NiceCore.Rapid.ApplicationBuilding
{
  /// <summary>
  /// AppBuilder Extension Store
  /// </summary>
  public class DefaultAppBuilderExtensionStore : IAppBuilderExtensionStore
  {
    private readonly SimpleCache<Type, object> m_InternalCache = new();

    /// <summary>
    /// GetOrCreate
    /// </summary>
    /// <typeparam name="TPropertyValue"></typeparam>
    /// <typeparam name="TApp"></typeparam>
    /// <param name="i_Supplier"></param>
    /// <returns></returns>
    public IPropertyBuilder<TPropertyValue, TApp> GetOrCreate<TPropertyValue, TApp>(Func<IPropertyBuilder<TPropertyValue, TApp>> i_Supplier) where TApp : INcApplication
    {
      return m_InternalCache.GetOrCreate(typeof(TPropertyValue), () => i_Supplier()) as IPropertyBuilder<TPropertyValue, TApp>;
    }
  }
}
