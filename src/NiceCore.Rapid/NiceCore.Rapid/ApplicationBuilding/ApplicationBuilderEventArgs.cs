﻿using NiceCore.Rapid.Application;
using NiceCore.ServiceLocation;
using System;

namespace NiceCore.Rapid.ApplicationBuilding
{
  /// <summary>
  /// EventArgs that are used in Events of an ApplicationBuilder
  /// </summary>
  public class ApplicationBuilderEventArgs<TApp> : EventArgs
    where TApp : INcApplication
  {
    /// <summary>
    /// Application
    /// </summary>
    public TApp Application { get; protected set; }
    /// <summary>
    /// The service container that was used
    /// </summary>
    public IServiceContainer ServiceContainer { get; protected set; }
    /// <summary>
    /// Fill constructor
    /// </summary>
    public ApplicationBuilderEventArgs(TApp i_Application, IServiceContainer i_Container)
    {
      Application = i_Application;
      ServiceContainer = i_Container;
    }
  }
}
