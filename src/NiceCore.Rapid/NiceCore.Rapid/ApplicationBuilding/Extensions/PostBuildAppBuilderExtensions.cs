using NiceCore.Rapid.Application;
using NiceCore.Rapid.ApplicationBuilding.Extensions.Impl;

namespace NiceCore.Rapid.ApplicationBuilding.Extensions
{
  /// <summary>
  /// Extension Methods to add post build extensions
  /// </summary>
  public static class PostBuildAppBuilderExtensions
  {
    /// <summary>
    /// Adds the ExecuteonScheduled PostBuildExtension
    /// </summary>
    /// <param name="t_Builder"></param>
    /// <typeparam name="TApp"></typeparam>
    public static IApplicationBuilder<TApp> ExecuteOnScheduledAfterBuild<TApp>(this IApplicationBuilder<TApp> t_Builder)
      where TApp : INcApplication
    {
      t_Builder.AddPostBuildExtension(new ScheduleServiceHostExecutePostBuildExtension<TApp>());
      return t_Builder;
    }
  }
}