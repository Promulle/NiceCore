using System.Threading.Tasks;
using NiceCore.Rapid.Application;
using NiceCore.Services.Scheduling;

namespace NiceCore.Rapid.ApplicationBuilding.Extensions.Impl
{
  /// <summary>
  /// Schedule Service Host Execute PostBuild Extension
  /// </summary>
  public sealed class ScheduleServiceHostExecutePostBuildExtension<TApp> : IPostBuildAppBuilderExtension<TApp>
  where TApp: INcApplication
  {
    /// <summary>
    /// Schedule Entry Metrics
    /// </summary>
    public IScheduleEntryMetrics ScheduleEntryMetrics { get; set; }
    
    /// <summary>
    /// Execute all entries that are ExecuteOnScheduled
    /// </summary>
    /// <param name="i_App"></param>
    public ValueTask Execute(TApp i_App)
    {
      return ExecuteExecuteOnScheduledServices(i_App, ScheduleEntryMetrics);
    }

    /// <summary>
    /// Execute all entries that are ExecuteOnScheduled
    /// </summary>
    /// <param name="i_App"></param>
    /// <param name="i_Metrics"></param>
    public static async ValueTask ExecuteExecuteOnScheduledServices(TApp i_App, IScheduleEntryMetrics i_Metrics)
    {
      if (i_App.ScheduleServiceHost != null)
      {
        foreach (var schedule in i_App.ScheduleServiceHost.GetAllSchedules())
        {
          foreach (var entry in schedule.GetAllScheduledEntries())
          {
            if (entry.ExecuteOnScheduled)
            {
              await ScheduleEntryExecutor.ExecuteEntry(entry, i_Metrics).ConfigureAwait(false);
            } 
          }
        }
      }
    }
  }
}