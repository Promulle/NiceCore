using System.Threading.Tasks;
using NiceCore.Rapid.Application;

namespace NiceCore.Rapid.ApplicationBuilding.Extensions
{
  /// <summary>
  /// Post Build AppBuilder Extension
  /// </summary>
  public interface IPostBuildAppBuilderExtension<TApp>
  where TApp: INcApplication
  {
    /// <summary>
    /// Executes the extension
    /// </summary>
    /// <param name="i_App"></param>
    /// <returns></returns>
    ValueTask Execute(TApp i_App);
  }
}