using System;
using System.Threading.Tasks;
using NiceCore.Rapid.Application;
using NiceCore.Rapid.ApplicationBuilding;

namespace NiceCore.Rapid.PropertyBuilding
{
  /// <summary>
  /// Execute Func Property Builder
  /// </summary>
  /// <typeparam name="TApp"></typeparam>
  /// <typeparam name="TParameter"></typeparam>
  public class ExecuteFuncPropertyBuilder<TApp, TParameter> : BasePropertyBuilder<TApp>
    where TApp : INcApplication
  {
    private Func<TParameter, ValueTask> m_Func;

    /// <summary>
    /// ctor
    /// </summary>
    /// <param name="i_Builder"></param>
    public ExecuteFuncPropertyBuilder(IApplicationBuilder<TApp> i_Builder) : base(i_Builder)
    {
    }

    /// <summary>
    /// Set i_Func as Func to be run
    /// </summary>
    /// <param name="i_Func"></param>
    /// <returns></returns>
    public IApplicationBuilder<TApp> Run(Func<TParameter, ValueTask> i_Func)
    {
      m_Func = i_Func;
      return Builder;
    }

    /// <summary>
    /// Execute the given func with i_Parameter
    /// </summary>
    /// <param name="i_Parameter"></param>
    /// <returns></returns>
    public ValueTask Execute(TParameter i_Parameter)
    {
      if (m_Func != null)
      {
        return m_Func.Invoke(i_Parameter);
      }

      return ValueTask.CompletedTask;
    }
  }
}