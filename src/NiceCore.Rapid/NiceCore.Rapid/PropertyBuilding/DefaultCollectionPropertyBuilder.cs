using System;
using System.Collections.Generic;
using NiceCore.Rapid.Application;
using NiceCore.Rapid.ApplicationBuilding;
using NiceCore.ServiceLocation;
using NiceCore.ServiceLocation.Discovery.ServiceDescriptors;

namespace NiceCore.Rapid.PropertyBuilding
{
  /// <summary>
  /// Default Collection Property Builder
  /// </summary>
  /// <typeparam name="TPropertyType"></typeparam>
  /// <typeparam name="TApp"></typeparam>
  public class DefaultCollectionPropertyBuilder<TPropertyType, TApp> : BasePropertyBuilder<TApp>, ICollectionPropertyBuilder<TPropertyType, TApp>
  where TApp: INcApplication
  {
    private readonly List<Type> m_Types = new();
    private readonly List<TPropertyType> m_Instances = new();
    private readonly List<TPropertyType> m_ResultInstances = new();

    /// <summary>
    /// Default Collection Property Builder
    /// </summary>
    /// <param name="i_Builder"></param>
    public DefaultCollectionPropertyBuilder(IApplicationBuilder<TApp> i_Builder) : base(i_Builder)
    {
    }
    
    /// <summary>
    /// Register
    /// </summary>
    /// <param name="t_Collection"></param>
    public void Register(NcServiceCollection t_Collection)
    {
      foreach (var type in m_Types)
      {
        if (type != typeof(TPropertyType))
        {
          t_Collection.Add(new()
          {
            ImplementorType = type,
            ServiceLifestyle = LifeStyles.Singleton,
            InterfaceType = typeof(TPropertyType)
          });  
        }
      }

      foreach (var instance in m_Instances)
      {
        t_Collection.AddInstance<TPropertyType>(instance);
      }
    }

    /// <summary>
    /// Build Instance
    /// </summary>
    /// <param name="i_Container"></param>
    /// <returns></returns>
    public List<TPropertyType> BuildInstance(IServiceContainer i_Container)
    {
      foreach (var propertyInstance in i_Container.ResolveAll<TPropertyType>())
      {
        m_ResultInstances.Add(propertyInstance); 
      }

      return GetInstance();
    }

    /// <summary>
    /// Get Instances
    /// </summary>
    /// <returns></returns>
    public List<TPropertyType> GetInstance()
    {
      return m_ResultInstances;
    }

    /// <summary>
    /// Add Instance by type
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public IApplicationBuilder<TApp> Add<T>() where T : TPropertyType
    {
      m_Types.Add(typeof(T));
      return Builder;
    }

    /// <summary>
    /// Add Instance
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public IApplicationBuilder<TApp> AddInstance<T>(T i_Instance) where T : TPropertyType
    {
      m_Instances.Add(i_Instance);
      return Builder;
    }
  }
}