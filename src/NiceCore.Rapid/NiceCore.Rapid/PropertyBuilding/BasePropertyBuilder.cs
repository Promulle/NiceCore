using NiceCore.Rapid.Application;
using NiceCore.Rapid.ApplicationBuilding;

namespace NiceCore.Rapid.PropertyBuilding
{
  /// <summary>
  /// Base Property Builder
  /// </summary>
  /// <typeparam name="TApp"></typeparam>
  public abstract class BasePropertyBuilder<TApp>
    where TApp : INcApplication
  {
    /// <summary>
    /// Builder
    /// </summary>
    public IApplicationBuilder<TApp> Builder { get; }

    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_Builder"></param>
    protected BasePropertyBuilder(IApplicationBuilder<TApp> i_Builder)
    {
      Builder = i_Builder;
    }
  }
}