using System.Collections.Generic;
using NiceCore.Rapid.Application;
using NiceCore.ServiceLocation;
using NiceCore.ServiceLocation.Discovery.ServiceDescriptors;

namespace NiceCore.Rapid.PropertyBuilding
{
  /// <summary>
  /// Property Builder Base
  /// </summary>
  /// <typeparam name="TPropertyType"></typeparam>
  /// <typeparam name="TApp"></typeparam>
  public interface IPropertyBuilderBase<TPropertyType, TApp> where TApp: INcApplication
  {
    /// <summary>
    /// Register
    /// </summary>
    /// <param name="t_Collection"></param>
    void Register(NcServiceCollection t_Collection);
    
    /// <summary>
    /// Get Instance
    /// </summary>
    /// <returns></returns>
    TPropertyType GetInstance();
    
    /// <summary>
    /// Build Instance
    /// </summary>
    /// <param name="i_Container"></param>
    /// <returns></returns>
    TPropertyType BuildInstance(IServiceContainer i_Container);
  }
}