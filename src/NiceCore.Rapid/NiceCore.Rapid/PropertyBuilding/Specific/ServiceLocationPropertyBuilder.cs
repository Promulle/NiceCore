using System;
using System.Collections.Generic;
using NiceCore.Rapid.Application;
using NiceCore.Rapid.ApplicationBuilding;
using NiceCore.ServiceLocation;
using NiceCore.ServiceLocation.Discovery;
using NiceCore.ServiceLocation.Discovery.DefaultDirectoryProviders;
using NiceCore.ServiceLocation.Discovery.ErrorHandlers;
using NiceCore.ServiceLocation.Discovery.Files;
using NiceCore.ServiceLocation.Discovery.ServiceDescriptors;
using NiceCore.ServiceLocation.Plugins;

namespace NiceCore.Rapid.PropertyBuilding.Specific
{
  /// <summary>
  /// Service Location Builder
  /// </summary>
  /// <typeparam name="TApp"></typeparam>
  public class ServiceLocationPropertyBuilder<TApp> : BasePropertyBuilder<TApp>
  where TApp: INcApplication
  {
    private readonly List<Action<IServiceContainer>> m_Setters = new();
    private List<Type> m_PluginTypes = new();
    private Type m_ContainerType;
    private IServiceContainer m_ContainerInstance;
    private Type m_DiscoveryType = typeof(DefaultRegistrationDiscovery);
    private IRegistrationDiscovery m_DiscoveryInstance;

    /// <summary>
    /// Discovery Run Configuration used
    /// </summary>
    private DiscoveryRunConfiguration m_DiscoveryRunConfiguration  = new()
    {
      TargetMode = DiscoveryModes.AssembliesAndFiles,
      AssemblyTargetMode = AssemblyProvidingModes.All,
      DiscoveredNamespaceStorage = new PublicTypeDiscoveredNamespaceStorage(),
      DiscoveryErrorHandler = new LoggingDiscoveryErrorHandler(),
      DiscoveryDefaultDirectoryProvider = new ExecutingAssemblyDefaultDirectoryProvider(),
      AllowSearchOfLoadedAssemblies = false,
      DependencyFileAdapter = new NiceCoreFileAdapter()
    };
    
    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_Builder"></param>
    public ServiceLocationPropertyBuilder(IApplicationBuilder<TApp> i_Builder) : base(i_Builder)
    {
    }

    /// <summary>
    /// Depends on
    /// </summary>
    /// <param name="i_Setter"></param>
    /// <returns></returns>
    public IApplicationBuilder<TApp> DependsOn(Action<IServiceContainer> i_Setter)
    {
      m_Setters.Add(i_Setter);
      return Builder;
    }

    
    /// <summary>
    /// Add Plugin
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public IApplicationBuilder<TApp> AddPlugin<T>() where T: IDependencyInjectionLifecyclePlugin
    {
      m_PluginTypes.Add(typeof(T));
      return Builder;
    }

    /// <summary>
    /// Default Plugins
    /// </summary>
    /// <returns></returns>
    public IApplicationBuilder<TApp> DefaultPlugins()
    {
      m_PluginTypes = new()
      {
        typeof(InitializePlugin),
        typeof(ReleaseOnDisposePlugin)
      };
      return Builder;
    }

    /// <summary>
    /// Use Container Type
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public IApplicationBuilder<TApp> UseContainerType<T>() where T: IServiceContainer
    {
      m_ContainerType = typeof(T);
      return Builder;
    }

    /// <summary>
    /// Use Container Instance
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public IApplicationBuilder<TApp> UseContainerInstance<T>(T i_Instance) where T: IServiceContainer
    {
      m_ContainerInstance = i_Instance;
      return Builder;
    }

    /// <summary>
    /// Use Discovery Run Configuration
    /// </summary>
    /// <param name="i_Configuration"></param>
    /// <returns></returns>
    public IApplicationBuilder<TApp> UseDiscoveryRunConfiguration(DiscoveryRunConfiguration i_Configuration)
    {
      m_DiscoveryRunConfiguration = i_Configuration;
      return Builder;
    }

    /// <summary>
    /// Get The used discovery run configuration
    /// </summary>
    /// <returns></returns>
    public DiscoveryRunConfiguration GetDiscoveryRunConfiguration()
    {
      return m_DiscoveryRunConfiguration;
    }

    /// <summary>
    /// Initialize the discovery
    /// </summary>
    /// <exception cref="Exception"></exception>
    public void InitializeDiscovery()
    {
      if (m_DiscoveryInstance == null)
      {
        m_DiscoveryInstance = Activator.CreateInstance(m_DiscoveryType) as IRegistrationDiscovery;
        if (m_DiscoveryInstance == null)
        {
          throw new Exception();
        }
      }
    }

    /// <summary>
    /// Get the discovery
    /// </summary>
    /// <returns></returns>
    public IRegistrationDiscovery GetDiscovery()
    {
      return m_DiscoveryInstance;
    }
    
    /// <summary>
    /// Initialize the container
    /// </summary>
    /// <param name="i_Collection"></param>
    /// <exception cref="InvalidOperationException"></exception>
    /// <exception cref="Exception"></exception>
    public void InitializeContainer(NcServiceCollection i_Collection)
    {
      if (m_ContainerInstance == null)
      {
        if (m_ContainerType == null)
        {
          throw new InvalidOperationException("Could not build NiceCore App - No Container type or instance defined. Call ServiceLocation.UseContainerType or ServiceLocation.UseContainerInstane on the app builder to change that.");
        }
        m_ContainerInstance = Activator.CreateInstance(m_ContainerType) as IServiceContainer;
        if (m_ContainerInstance == null)
        {
          throw new Exception();
        }
      }

      foreach (var pluginType in m_PluginTypes)
      {
        var pluginInst = Activator.CreateInstance(pluginType) as IDependencyInjectionLifecyclePlugin;
        if (pluginInst == null)
        {
          throw new Exception();
        }
        m_ContainerInstance.Plugins.Add(pluginInst);
      }
      m_ContainerInstance.Initialize(i_Collection);
    }

    /// <summary>
    /// Get Container
    /// </summary>
    /// <returns></returns>
    public IServiceContainer GetContainer()
    {
      return m_ContainerInstance;
    }
  }
}