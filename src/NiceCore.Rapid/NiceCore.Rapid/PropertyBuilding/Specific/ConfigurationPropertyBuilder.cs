using NiceCore.Rapid.Application;
using NiceCore.Rapid.ApplicationBuilding;
using NiceCore.ServiceLocation.Discovery.ServiceDescriptors;
using NiceCore.Services.Config;

namespace NiceCore.Rapid.PropertyBuilding.Specific
{
  /// <summary>
  /// Property for ConfigurationBuilder
  /// </summary>
  /// <typeparam name="TApp"></typeparam>
  public class ConfigurationPropertyBuilder<TApp> : DefaultPropertyBuilder<INcConfiguration, TApp>
    where TApp: INcApplication
  {
    /// <summary>
    /// Configuration Sources
    /// </summary>
    public ICollectionPropertyBuilder<INcConfigurationSource, TApp> ConfigurationSources { get; } 
    
    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_Builder"></param>
    public ConfigurationPropertyBuilder(IApplicationBuilder<TApp> i_Builder) : base(i_Builder)
    {
      ConfigurationSources = new DefaultCollectionPropertyBuilder<INcConfigurationSource, TApp>(i_Builder);
    }

    /// <summary>
    /// Register
    /// </summary>
    /// <param name="t_Collection"></param>
    public override void Register(NcServiceCollection t_Collection)
    {
      base.Register(t_Collection);
      ConfigurationSources.Register(t_Collection);
    }
  }
}