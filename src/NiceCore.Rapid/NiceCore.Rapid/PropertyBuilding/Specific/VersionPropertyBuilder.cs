using System;
using System.Reflection;
using NiceCore.Rapid.Application;
using NiceCore.Rapid.ApplicationBuilding;

namespace NiceCore.Rapid.PropertyBuilding.Specific
{
  /// <summary>
  /// Version Property Builder
  /// </summary>
  /// <typeparam name="TApp"></typeparam>
  public class VersionPropertyBuilder<TApp> : BasePropertyBuilder<TApp>
  where TApp: INcApplication
  {
    private Version m_Version;

    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_Builder"></param>
    public VersionPropertyBuilder(IApplicationBuilder<TApp> i_Builder) : base(i_Builder)
    {
    }
    
    /// <summary>
    /// use version of entry assembly
    /// </summary>
    /// <returns></returns>
    public IApplicationBuilder<TApp> UseEntryAssemblyVersion()
    {
      var entryAssembly = Assembly.GetEntryAssembly();
      if (entryAssembly == null)
      {
        throw new NotSupportedException($"{nameof(UseEntryAssemblyVersion)} is only supported in environments, where Assembly.GetEntryAssembly does not return null.");
      }
      m_Version = entryAssembly.GetName().Version;
      return Builder;
    }
    
    /// <summary>
    /// Get Version
    /// </summary>
    /// <returns></returns>
    public Version GetVersion()
    {
      return m_Version;
    }
  }
}