using System;
using NiceCore.Rapid.Application;
using NiceCore.Rapid.ApplicationBuilding;
using NiceCore.ServiceLocation;

namespace NiceCore.Rapid.PropertyBuilding
{ 
  /// <summary>
  /// Property Builder
  /// </summary>
  /// <typeparam name="TPropertyType"></typeparam>
  /// <typeparam name="TApp"></typeparam>
  public interface IPropertyBuilder<TPropertyType, TApp> : IPropertyBuilderBase<TPropertyType, TApp> where TApp: INcApplication
  {
    /// <summary>
    /// Use
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    IApplicationBuilder<TApp> Use<T>() where T: TPropertyType;
    
    /// <summary>
    /// Use Instance
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    IApplicationBuilder<TApp> UseInstance<T>(T i_Instance) where T: TPropertyType;

    /// <summary>
    /// Default
    /// </summary>
    /// <returns></returns>
    IApplicationBuilder<TApp> Default();

    /// <summary>
    /// After getting the result, this runs i_Setter with the Instance as parameter
    /// </summary>
    /// <param name="i_Setter"></param>
    /// <returns></returns>
    IApplicationBuilder<TApp> DependsOn(Action<TPropertyType> i_Setter);
  }
}