using System;
using System.Collections.Generic;
using NiceCore.Rapid.Application;
using NiceCore.Rapid.ApplicationBuilding;
using NiceCore.ServiceLocation;
using NiceCore.ServiceLocation.Discovery.ServiceDescriptors;

namespace NiceCore.Rapid.PropertyBuilding
{
  /// <summary>
  /// Default Property Builder
  /// </summary>
  /// <typeparam name="TPropertyType"></typeparam>
  /// <typeparam name="TApp"></typeparam>
  public class DefaultPropertyBuilder<TPropertyType, TApp> : BasePropertyBuilder<TApp>, IPropertyBuilder<TPropertyType, TApp>
  where TApp: INcApplication
  {
    private readonly List<Action<TPropertyType>> m_Setters = new();
    private Type m_Type;
    private TPropertyType m_Instance;
    
    /// <summary>
    /// Default Property Builder
    /// </summary>
    /// <param name="i_Builder"></param>
    public DefaultPropertyBuilder(IApplicationBuilder<TApp> i_Builder): base(i_Builder)
    {
      
    }

    /// <summary>
    /// Default
    /// </summary>
    /// <returns></returns>
    public IApplicationBuilder<TApp> Default()
    {
      m_Type = typeof(TPropertyType);
      return Builder;
    }

    /// <summary>
    /// Register With t_Collection
    /// </summary>
    /// <param name="t_Collection"></param>
    public virtual void Register(NcServiceCollection t_Collection)
    {
      if (m_Instance != null || (m_Type != null && m_Type != typeof(TPropertyType)))
      {
        t_Collection.RemoveByInterfaceType(typeof(TPropertyType));
      }
      
      if (m_Type != null && m_Type != typeof(TPropertyType))
      {
        t_Collection.Add(new()
        {
          Instance = null,
          ImplementorType = m_Type,
          InterfaceType = typeof(TPropertyType),
          ServiceLifestyle = LifeStyles.Singleton
        });
      }

      if (m_Instance != null)
      {
        t_Collection.Add(new()
        {
          Instance = m_Instance,
          ImplementorType = m_Instance.GetType(),
          InterfaceType = typeof(TPropertyType),
          ServiceLifestyle = LifeStyles.Singleton
        });
      }
    }

    /// <summary>
    /// Build Instance
    /// </summary>
    /// <param name="i_Container"></param>
    /// <returns></returns>
    public virtual TPropertyType BuildInstance(IServiceContainer i_Container)
    {
      if (m_Instance == null && m_Type != null)
      {
        m_Instance = i_Container.Resolve<TPropertyType>(m_Type);
      }

      if (m_Instance != null)
      {
        foreach (var setter in m_Setters)
        {
          setter.Invoke(m_Instance);
        }
      }
      return GetInstance();
    }

    /// <summary>
    /// Get Instance
    /// </summary>
    /// <returns></returns>
    public TPropertyType GetInstance()
    {
      return m_Instance;
    }

    /// <summary>
    /// Use instance created by type
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public IApplicationBuilder<TApp> Use<T>() where T : TPropertyType
    {
      m_Instance = default;
      m_Type = typeof(T);
      return Builder;
    }

    /// <summary>
    /// Use Instance
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public IApplicationBuilder<TApp> UseInstance<T>(T i_Instance) where T : TPropertyType
    {
      m_Type = null;
      m_Instance = i_Instance;
      return Builder;
    }

    /// <summary>
    /// Depends On
    /// </summary>
    /// <param name="i_Setter"></param>
    /// <returns></returns>
    public IApplicationBuilder<TApp> DependsOn(Action<TPropertyType> i_Setter)
    {
      m_Setters.Add(i_Setter);
      return Builder;
    }
  }
}