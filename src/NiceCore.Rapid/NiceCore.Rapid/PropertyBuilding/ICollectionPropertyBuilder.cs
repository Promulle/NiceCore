using System.Collections.Generic;
using NiceCore.Rapid.Application;
using NiceCore.Rapid.ApplicationBuilding;

namespace NiceCore.Rapid.PropertyBuilding
{
  /// <summary>
  /// Interface for collection property builder
  /// </summary>
  /// <typeparam name="TProperty"></typeparam>
  /// <typeparam name="TApp"></typeparam>
  public interface ICollectionPropertyBuilder<TProperty, TApp> : IPropertyBuilderBase<List<TProperty>, TApp> where TApp: INcApplication
  {
    /// <summary>
    /// Add
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    IApplicationBuilder<TApp> Add<T>() where T: TProperty;
    
    /// <summary>
    /// Add Instance
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    IApplicationBuilder<TApp> AddInstance<T>(T i_Instance) where T: TProperty;
    
    
  }
}