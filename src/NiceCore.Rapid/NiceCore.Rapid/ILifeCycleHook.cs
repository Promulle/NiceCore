﻿using NiceCore.Rapid.Application;
using System.Threading.Tasks;

namespace NiceCore.Rapid
{
  /// <summary>
  /// Interface for hooks that are called by the nc application
  /// </summary>
  public interface ILifeCycleHook
  {
    /// <summary>
    /// Execute the lifecycle hook
    /// </summary>
    /// <param name="i_Application"></param>
    void Execute(INcApplication i_Application);

    /// <summary>
    /// Execute the lifecycle hook async
    /// </summary>
    /// <param name="i_Application"></param>
    /// <returns></returns>
    Task ExecuteAsync(INcApplication i_Application);
  }
}
