﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using NiceCore.Logging;
using NiceCore.Rapid.Application;
using NiceCore.Rapid.ApplicationBuilding;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using NiceCore.Services.Config.Logging;

namespace NiceCore.Rapid.Logging
{
  /// <summary>
  /// Application Logging setup
  /// </summary>
  public sealed class ApplicationLoggingSetup<TApp>
    where TApp : INcApplication
  {
    /// <summary>
    /// Builder instance wrapped by a wrapper using this propertyBuilder
    /// </summary>
    private IApplicationBuilder<TApp> m_Parent;

    /// <summary>
    /// Logging Providers
    /// </summary>
    private IEnumerable<INcLoggingProvider> LoggingProviders { get; set; } = Enumerable.Empty<INcLoggingProvider>();

    /// <summary>
    /// Level
    /// </summary>
    private LogLevel Level { get; set; } = LogLevel.Information;

    /// <summary>
    /// Stage
    /// </summary>
    public ApplicationLoggingSetupStage Stage { get; private set; } = ApplicationLoggingSetupStage.None;
    
    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_Parent"></param>
    public ApplicationLoggingSetup(IApplicationBuilder<TApp> i_Parent)
    {
      m_Parent = i_Parent;
    }

    /// <summary>
    /// Direct
    /// </summary>
    /// <param name="i_Providers"></param>
    /// <param name="i_Level"></param>
    /// <returns></returns>
    public IApplicationBuilder<TApp> Direct(IEnumerable<INcLoggingProvider> i_Providers, LogLevel i_Level)
    {
      LoggingProviders = i_Providers;
      Level = i_Level;
      Stage = ApplicationLoggingSetupStage.BeforeContainer;
      return m_Parent;
    }
    
    /// <summary>
    /// Configured
    /// </summary>
    /// <returns></returns>
    public IApplicationBuilder<TApp> Configured()
    {
      Stage = ApplicationLoggingSetupStage.AfterContainer;
      return m_Parent;
    }

    internal ILoggerFactory BuildFactory(IServiceContainer i_Container, INcConfiguration i_Configuration)
    {
      if (Stage == ApplicationLoggingSetupStage.BeforeContainer)
      {
        return DirectLoggerFactorySetup.SetupLoggerFactoryFor(LoggingProviders, Level);
      }
      if (Stage == ApplicationLoggingSetupStage.AfterContainer)
      {
        return ConfigurableLoggerFactorySetup.SetupLoggerFactoryFor(i_Container, i_Configuration);
      }

      throw new InvalidOperationException($"Could not build a logger factory, unsupported stage: {Stage}");
    }
  }
}