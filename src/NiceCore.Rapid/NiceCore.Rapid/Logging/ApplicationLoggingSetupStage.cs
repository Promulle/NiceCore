﻿namespace NiceCore.Rapid.Logging
{
  /// <summary>
  /// Applicatin Logging Setup Stage
  /// </summary>
  public enum ApplicationLoggingSetupStage
  {
    /// <summary>
    /// None
    /// </summary>
    None = 0,
    
    /// <summary>
    /// Before Container
    /// </summary>
    BeforeContainer = 1,
    
    /// <summary>
    /// After Container
    /// </summary>
    AfterContainer = 2,
  }
}