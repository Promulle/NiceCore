﻿using System.Linq;
using NiceCore.Rapid.Application;
using NiceCore.Rapid.ApplicationBuilding;
using NiceCore.Rapid.PropertyBuilding;
using NiceCore.ServiceLocation.Attributes;
using NiceCore.ServiceLocation.Discovery.ServiceDescriptors;
using NiceCore.Storage;

namespace NiceCore.Rapid.Storage
{
  /// <summary>
  /// Extension that enables usage of a StorageService
  /// </summary>
  public class NcStorageApplicationExtension<TApp> : IAppBuilderExtension<TApp>
     where TApp : INcStorageApplication
  {
    /// <summary>
    /// property builder for storage service
    /// </summary>
    public IPropertyBuilder<IStorageService, TApp> StorageService { get; set; }

    /// <summary>
    /// Debug
    /// </summary>
    public bool Debug { get; set; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public NcStorageApplicationExtension(IApplicationBuilder<TApp> i_Instance, IAppBuilderExtensionStore i_Extensions)
    {
      StorageService = i_Extensions.GetOrCreate(() => new DefaultPropertyBuilder<IStorageService, TApp>(i_Instance));
    }

    /// <summary>
    /// BuildStarted EventHandler
    /// </summary>
    /// <param name="i_Builder"></param>
    public void OnBuildStarted(IApplicationBuilder<TApp> i_Builder)
    {
    }

    /// <summary>
    /// Register Services
    /// </summary>
    /// <param name="t_Collection"></param>
    public void RegisterServices(NcServiceCollection t_Collection)
    {
      StorageService.Register(t_Collection);
    }

    /// <summary>
    /// PropertiesInitialized EventHandler
    /// </summary>
    /// <param name="i_Builder"></param>
    public void OnPropertiesInitialized(IApplicationBuilder<TApp> i_Builder)
    {
      var storageService = StorageService.BuildInstance(i_Builder.ServiceLocation.GetContainer());
      if (storageService != null)
      {
        storageService.Initialize();
      }
    }

    /// <summary>
    /// Properties set event handler
    /// </summary>
    /// <param name="i_App"></param>
    public void OnPropertiesSet(TApp i_App)
    {
      i_App.StorageService = StorageService.GetInstance();
    }
    /// <summary>
    /// Event fired when an app has been registered with the container
    /// </summary>
    public void OnApplicationRegistered(TApp i_App, NcServiceCollection t_Collection)
    {
      var existingDescriptor = t_Collection.FirstOrDefault(r => r.InterfaceType == typeof(INcApplication));
      if (existingDescriptor != null)
      {
        t_Collection.Remove(existingDescriptor);
        var newDescriptor = existingDescriptor.WithNewAlias(new(existingDescriptor.InterfaceType, typeof(INcStorageApplication)));
        t_Collection.Add(newDescriptor);
      }
    }
  }
}
