﻿using NiceCore.Rapid.Application;
using NiceCore.Storage;

namespace NiceCore.Rapid.Storage
{
  /// <summary>
  /// Impl of app for INcApplication with storage
  /// </summary>
  public class NcStorageApplication : DefaultNcApplication, INcStorageApplication
  {
    /// <summary>
    /// Service for storage interaction
    /// </summary>
    public IStorageService StorageService { get; set; }
  }
}
