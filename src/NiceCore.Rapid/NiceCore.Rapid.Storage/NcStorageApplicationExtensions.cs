﻿using NiceCore.Rapid.ApplicationBuilding;
using NiceCore.Rapid.PropertyBuilding;
using NiceCore.Storage;

namespace NiceCore.Rapid.Storage
{
  /// <summary>
  /// Class for Extension methods
  /// </summary>
  public static class NcStorageApplicationExtensions
  {
    /// <summary>
    /// Sets StorageService PropertyBuilder
    /// </summary>
    /// <typeparam name="TApp"></typeparam>
    /// <param name="i_AppBuilder"></param>
    /// <param name="i_Extensions"></param>
    /// <returns></returns>
    public static IPropertyBuilder<IStorageService, TApp> StorageService<TApp>(this IApplicationBuilder<TApp> i_AppBuilder, IAppBuilderExtensionStore i_Extensions)
      where TApp : INcStorageApplication
    {
      var ext = new NcStorageApplicationExtension<TApp>(i_AppBuilder, i_Extensions);
      i_AppBuilder.AddExtension(ext);
      return ext.StorageService;
    }
  }
}
