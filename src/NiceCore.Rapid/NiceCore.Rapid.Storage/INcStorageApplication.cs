﻿using NiceCore.Rapid.Application;
using NiceCore.Storage;

namespace NiceCore.Rapid.Storage
{
  /// <summary>
  /// Application with storage service
  /// </summary>
  public interface INcStorageApplication : INcApplication
  {
    /// <summary>
    /// StorageService
    /// </summary>
    IStorageService StorageService { get; set; }
  }
}