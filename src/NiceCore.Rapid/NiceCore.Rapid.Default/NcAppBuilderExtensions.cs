﻿using NiceCore.Rapid.Application;
using NiceCore.Rapid.ApplicationBuilding;
using NiceCore.ServiceLocation.CastleWindsor;
using NiceCore.ServiceLocation.Discovery;
using NiceCore.ServiceLocation.Discovery.Files;
using NiceCore.ServiceLocation.Plugins;
using NiceCore.Services.Config.Impl;
using NiceCore.Services.Config.Impl.AssemblyBased;
using NiceCore.Services.Default.Config;

namespace NiceCore.Rapid.Default
{
  /// <summary>
  /// Extension methods for bootstrapping
  /// </summary>
  public static class NcAppBuilderExtensions
  {
    /// <summary>
    /// Configures appbootstrapper to use default values for components
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <returns></returns>
    public static IApplicationBuilder<TApp> UseDefaults<TApp>(this IApplicationBuilder<TApp> i_Instance)
      where TApp : INcApplication
    {
      return i_Instance
        .ServiceLocation.UseContainerType<CastleWindsorServiceContainer>()
        .ServiceLocation.DefaultPlugins()
        .Configuration.Default()
        .Configuration.ConfigurationSources.Add<DefaultEnvironmentNcConfigurationSource>()
        .Configuration.ConfigurationSources.Add<EntryAssemblyJSONNcConfigurationSource>()
        .ApplicationVersion.UseEntryAssemblyVersion()
        .Logging.Configured();
    }
  }
}
