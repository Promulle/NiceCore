﻿using NiceCore.Base.Caching;
using NiceCore.Entities;
using System;

namespace NiceCore.Caches
{
  /// <summary>
  /// Cache that holds the exact IDomainEntity Type for a specific ID Type
  /// </summary>
  public class DomainEntityInterfaceForParameterTypeCache : BaseSpecializedSimpleCache<Type, Type>
  {
    /// <summary>
    /// Create Func
    /// </summary>
    /// <param name="i_Key"></param>
    /// <returns></returns>
    protected override Type CreateFunc(Type i_Key)
    {
      return typeof(IDomainEntity<>).MakeGenericType(i_Key);
    }
  }
}
