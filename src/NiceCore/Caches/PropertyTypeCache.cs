﻿using NiceCore.Base.Caching;
using System;

namespace NiceCore.Caches
{
  /// <summary>
  /// Specialized Cache for types of properties
  /// </summary>
  public class PropertyTypeCache : BaseSpecializedSimpleCache<PropertyTypeCacheKey, Type>
  {
    /// <summary>
    /// Create Func
    /// </summary>
    /// <param name="i_Key"></param>
    /// <returns></returns>
    protected override Type CreateFunc(PropertyTypeCacheKey i_Key)
    {
      var property = i_Key.DeclaringType.GetProperty(i_Key.PropertyName);
      if (property == null)
      {
        throw new InvalidOperationException($"Could not cache property value: type {i_Key.DeclaringType.FullName} does not declare a property called {i_Key.PropertyName}");
      }
      return property.PropertyType;
    }
  }
}
