using System;
using System.Linq.Expressions;
using NiceCore.Base.Caching;

namespace NiceCore.Caches
{
  /// <summary>
  /// Property getter cache
  /// </summary>
  public class PropertyGetterCache : BaseSpecializedSimpleCache<PropertyGetterCacheKey, Func<object, object>>
  {
    /// <summary>
    /// Create Func
    /// </summary>
    /// <param name="i_Key"></param>
    /// <returns></returns>
    protected override Func<object, object> CreateFunc(PropertyGetterCacheKey i_Key)
    {
      return CreateGetter(i_Key);
    }

    /// <summary>
    /// Create Getter Func
    /// </summary>
    /// <param name="i_Key"></param>
    /// <returns></returns>
    public static Func<object, object> CreateGetter(PropertyGetterCacheKey i_Key)
    {
      var objectParam      = Expression.Parameter(typeof(object));
      var genericInstParam = Expression.Convert(objectParam, i_Key.DeclaringType);
      var propExpr         = Expression.Property(genericInstParam, i_Key.PropertyName);
      var convPropExpr     = Expression.Convert(propExpr, typeof(object));
      var lambda           = Expression.Lambda<Func<object, object>>(convPropExpr, objectParam);
      return lambda.Compile();
    }
  }
}
