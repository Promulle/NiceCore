﻿using NiceCore.Base.Caching;
using System;
using System.Collections.Generic;

namespace NiceCore.Caches
{
  /// <summary>
  /// Key used for PropertySetter Cache. PropertyType is only for casting and is not used for equals
  /// </summary>
  public class PropertySetterCacheKey : BaseCacheKey
  {
    /// <summary>
    /// Type that declares the Property
    /// </summary>
    public Type DeclaringType { get; init; }

    /// <summary>
    /// Type of the property, needed for casting the param expr of the value
    /// </summary>
    public Type PropertyType { get; init; }

    /// <summary>
    /// Name of the property to set
    /// </summary>
    public string PropertyName { get; init; }

    /// <summary>
    /// Equals
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj)
    {
      return obj is PropertySetterCacheKey key &&
             EqualityComparer<Type>.Default.Equals(DeclaringType, key.DeclaringType) &&
             PropertyName == key.PropertyName;
    }

    /// <summary>
    /// GetHashCode
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode()
    {
      return HashCode.Combine(DeclaringType, PropertyName);
    }
  }
}
