﻿namespace NiceCore.Caches
{
  /// <summary>
  /// Caches that hold cached compiled/non-compiled expressions
  /// </summary>
  public static class ExpressionCaches
  {
    /// <summary>
    /// Cache for retrieving functions for setting property values
    /// </summary>
    public static PropertySetterCache PropertySetterCache { get; } = new();

    /// <summary>
    /// Cache for property types
    /// </summary>
    public static PropertyTypeCache PropertyTypeCache { get; } = new();

    /// <summary>
    /// Property Getter cache
    /// </summary>
    public static PropertyGetterCache PropertyGetterCache { get; } = new();

    /// <summary>
    /// Cache that holds concrete interface of IDomainEntity for the given type parameter
    /// </summary>
    public static DomainEntityInterfaceForParameterTypeCache DomainEntityInterfaceForParameterTypeCache { get; } = new();
  }
}
