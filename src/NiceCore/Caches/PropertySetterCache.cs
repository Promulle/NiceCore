﻿using NiceCore.Base.Caching;
using System;
using System.Linq.Expressions;

namespace NiceCore.Caches
{
  /// <summary>
  /// Cache that holds property setter functions
  /// </summary>
  public class PropertySetterCache : BaseSpecializedSimpleCache<PropertySetterCacheKey, Action<object, object>>
  {
    /// <summary>
    /// Create Func that creates the setter func
    /// </summary>
    /// <param name="i_Key"></param>
    /// <returns></returns>
    protected override Action<object, object> CreateFunc(PropertySetterCacheKey i_Key)
    {
      return GetPropertySetter(i_Key.DeclaringType, i_Key.PropertyName, i_Key.PropertyType);
    }

    private static Action<object, object> GetPropertySetter(Type i_ObjectType, string i_PropertyName, Type i_ValueType)
    {
      var propertyParamExpr = Expression.Parameter(typeof(object));
      var castedPropertyParamExpr = Expression.Convert(propertyParamExpr, i_ObjectType);
      var propertyExpr = Expression.Property(castedPropertyParamExpr, i_PropertyName);
      var valueParamExpr = Expression.Parameter(typeof(object));
      var valueCastedParamExpr = Expression.Convert(valueParamExpr, i_ValueType);
      var assignExpr = Expression.Assign(propertyExpr, valueCastedParamExpr);
      return Expression.Lambda<Action<object, object>>(assignExpr, propertyParamExpr, valueParamExpr).Compile();
    }
  }
}
