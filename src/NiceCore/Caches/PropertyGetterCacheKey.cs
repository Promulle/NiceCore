using System;
using System.Collections.Generic;

namespace NiceCore.Caches
{
  /// <summary>
  /// property getter cache key
  /// </summary>
  public class PropertyGetterCacheKey
  {
    /// <summary>
    /// Type that declares the Property
    /// </summary>
    public Type DeclaringType { get; init; }

    /// <summary>
    /// Name of the property to set
    /// </summary>
    public string PropertyName { get; init; }

    /// <summary>
    /// Equals
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj)
    {
      return obj is PropertySetterCacheKey key &&
             EqualityComparer<Type>.Default.Equals(DeclaringType, key.DeclaringType) &&
             PropertyName == key.PropertyName;
    }

    /// <summary>
    /// GetHashCode
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode()
    {
      return HashCode.Combine(DeclaringType, PropertyName);
    }
  }
}
