﻿using NiceCore.Base.Caching;
using System;
using System.Collections.Generic;

namespace NiceCore.Caches
{
  /// <summary>
  /// Cache Key for PropertyType Cache
  /// </summary>
  public class PropertyTypeCacheKey : BaseCacheKey
  {
    /// <summary>
    /// Type declaring the property
    /// </summary>
    public Type DeclaringType { get; init; }

    /// <summary>
    /// PropertyName
    /// </summary>
    public string PropertyName { get; init; }

    /// <summary>
    /// Equals
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj)
    {
      return obj is PropertyTypeCacheKey key &&
             EqualityComparer<Type>.Default.Equals(DeclaringType, key.DeclaringType) &&
             PropertyName == key.PropertyName;
    }

    /// <summary>
    /// Gethashcode
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode()
    {
      return HashCode.Combine(DeclaringType, PropertyName);
    }
  }
}
