﻿namespace NiceCore.Permissions.Impl
{
  /// <summary>
  /// Impl for IPermissionKey that just defines the name of a permission
  /// </summary>
  public class SimplePermissionKey : IPermissionKey
  {
    /// <summary>
    /// Name of the permission to check for
    /// </summary>
    public string PermissionKeyName { get; set; }

    /// <summary>
    /// Factory constructor that takes the name of the permission as parameter
    /// </summary>
    /// <param name="i_Key"></param>
    public static SimplePermissionKey Of(string i_Key)
    {
      return new SimplePermissionKey()
      {
        PermissionKeyName = i_Key
      };
    }
  }
}
