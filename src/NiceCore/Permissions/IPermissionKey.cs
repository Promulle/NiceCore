﻿namespace NiceCore.Permissions
{
  /// <summary>
  /// Interface for PermissionKeys, A permission key is an abstraction over the multiple ways to define what a user should have to be allowed to execute an action
  /// </summary>
  public interface IPermissionKey
  {
  }
}
