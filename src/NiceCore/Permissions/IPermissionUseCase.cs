﻿namespace NiceCore.Permissions
{
  /// <summary>
  /// A use case that might hold information that can be used to identify the user that is about to execute the action
  /// </summary>
  public interface IPermissionUseCase
  {
  }
}
