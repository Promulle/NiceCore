﻿using NiceCore.Discovery.Impl;
using System;

namespace NiceCore.Discovery
{
  /// <summary>
  /// Static helper class
  /// </summary>
  public static class AttributeDiscovery
  {
    /// <summary>
    /// Create a new Behavior For Attributes of Type TAttribute
    /// </summary>
    /// <typeparam name="TAttribute"></typeparam>
    /// <param name="i_Behavior"></param>
    /// <returns></returns>
    public static IAttributeDiscoveredBehavior NewBehaviorFor<TAttribute>(Action<object, Type> i_Behavior)
    {
      return new DefaultAttributeDiscoveredBehavior(typeof(TAttribute), i_Behavior);
    }
  }
}
