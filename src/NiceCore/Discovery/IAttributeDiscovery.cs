﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace NiceCore.Discovery
{
  /// <summary>
  /// Generic attribute discovery
  /// </summary>
  public interface IAttributeDiscovery
  {
    /// <summary>
    /// Discover all attributes on i_Type and react with correct behavior
    /// </summary>
    /// <param name="i_Type"></param>
    /// <param name="i_Behaviors"></param>
    void Discover(Type i_Type, IEnumerable<IAttributeDiscoveredBehavior> i_Behaviors);

    /// <summary>
    /// Discover attributes on all types in i_Assembly and react with correct behavior
    /// </summary>
    /// <param name="i_Assembly"></param>
    /// <param name="i_Behaviors"></param>
    void Discover(Assembly i_Assembly, IEnumerable<IAttributeDiscoveredBehavior> i_Behaviors);
  }
}
