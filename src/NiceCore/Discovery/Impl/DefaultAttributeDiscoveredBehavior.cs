﻿using System;

namespace NiceCore.Discovery.Impl
{
  /// <summary>
  /// Default impl for IAttributeDiscoveredBehavior
  /// </summary>
  public class DefaultAttributeDiscoveredBehavior : IAttributeDiscoveredBehavior
  {
    /// <summary>
    /// Type of Attribute
    /// </summary>
    public Type AttributeType { get; }

    /// <summary>
    /// Behavior that should be executed
    /// </summary>
    public Action<object, Type> Behavior { get; }

    /// <summary>
    /// Default constructor
    /// </summary>
    /// <param name="i_AttributeType"></param>
    /// <param name="i_Behavior"></param>
    public DefaultAttributeDiscoveredBehavior(Type i_AttributeType, Action<object, Type> i_Behavior)
    {
      AttributeType = i_AttributeType;
      Behavior = i_Behavior;
    }
  }
}
