﻿using NiceCore.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace NiceCore.Discovery.Impl
{
  /// <summary>
  /// Default impl of IAttributeDiscovery
  /// </summary>
  public class DefaultAttributeDiscovery : IAttributeDiscovery
  {
    /// <summary>
    /// Discover attributes on i_Type
    /// </summary>
    /// <param name="i_Type"></param>
    /// <param name="i_Behaviors"></param>
    public void Discover(Type i_Type, IEnumerable<IAttributeDiscoveredBehavior> i_Behaviors)
    {
      foreach (Attribute attr in i_Type.GetCustomAttributes(true))
      {
        var behaviors = i_Behaviors.Where(r => r.AttributeType == attr.GetType());
        behaviors.ForEachItem(r => r.Behavior(attr, i_Type));
      }
    }

    /// <summary>
    /// Discover attributes on all types in i_Assembly
    /// </summary>
    /// <param name="i_Assembly"></param>
    /// <param name="i_Behaviors"></param>
    public void Discover(Assembly i_Assembly, IEnumerable<IAttributeDiscoveredBehavior> i_Behaviors)
    {
      if (i_Behaviors.Any())
      {
        i_Assembly.GetExportedTypes()
          .ForEachItem(r => Discover(r, i_Behaviors));
      }
    }
  }
}
