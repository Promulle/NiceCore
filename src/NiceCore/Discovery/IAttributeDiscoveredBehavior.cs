﻿using System;

namespace NiceCore.Discovery
{
  /// <summary>
  /// Behavior to run once AttributeType is discovered
  /// </summary>
  public interface IAttributeDiscoveredBehavior
  {
    /// <summary>
    /// Type of attribute to discover
    /// </summary>
    Type AttributeType { get; }

    /// <summary>
    /// Behavior to run
    /// </summary>
    Action<object, Type> Behavior { get; }
  }
}
