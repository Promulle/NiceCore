﻿using NiceCore.Base.Expressions;
using System;
using System.Linq.Expressions;

namespace NiceCore.Filtering
{
  /// <summary>
  /// Placeholder for filter values
  /// </summary>
  public class FilterPlaceHolderExpression : BasePlaceholderExpression<ConstantExpression>
  {
    /// <summary>
    /// Constructor filling type
    /// </summary>
    /// <param name="i_Type"></param>
    public FilterPlaceHolderExpression(Type i_Type) : base(i_Type)
    {
    }

    /// <summary>
    /// Creates a new FilterExpressionPlaceholder for i_Type
    /// </summary>
    /// <param name="i_Type"></param>
    /// <returns></returns>
    public static FilterPlaceHolderExpression Create(Type i_Type)
    {
      return new FilterPlaceHolderExpression(i_Type);
    }
  }
}
