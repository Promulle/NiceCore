﻿using System.Collections.Generic;

namespace NiceCore.Filtering
{
  /// <summary>
  /// Default impl for INcFilter
  /// </summary>
  public class DefaultNcFilter : INcFilter
  {
    /// <summary>
    /// Conditions
    /// </summary>
    public List<DefaultNcFilterCondition> Conditions { get; set; }

    /// <summary>
    /// Filter link
    /// </summary>
    public FilterLinks FilterLink { get; set; }

    /// <summary>
    /// Sub FIlters
    /// </summary>
    public List<DefaultNcFilter> SubFilters { get; set; }
  }
}
