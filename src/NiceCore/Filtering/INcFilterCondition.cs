﻿namespace NiceCore.Filtering
{
  /// <summary>
  /// Condition used by INcFilter
  /// </summary>
  public interface INcFilterCondition
  {
    /// <summary>
    /// name of the property that should be used for filtering
    /// </summary>
    string PropertyName { get; }

    /// <summary>
    /// Operation that compares the value of property name to Value
    /// </summary>
    FilterOperations Operation { get; }

    /// <summary>
    /// Expected value
    /// </summary>
    object Value { get; }
  }
}
