using System;
using System.Linq.Expressions;

namespace NiceCore.Filtering
{
  /// <summary>
  /// CompareExpression Provider
  /// </summary>
  public interface ICompareExpressionProvider
  {
    /// <summary>
    /// Compared Type
    /// </summary>
    Type ComparedType { get; }

    /// <summary>
    /// Get Compare Expression
    /// </summary>
    /// <param name="i_Operation"></param>
    /// <param name="i_PropertyExpression"></param>
    /// <param name="i_ValueExpression"></param>
    /// <param name="i_Options"></param>
    /// <param name="i_FilterTarget"></param>
    /// <returns></returns>
    Expression GetCompareExpression(FilterOperations i_Operation, Expression i_PropertyExpression, Expression i_ValueExpression, NcFilterTranslationOptions i_Options, FilterTargets i_FilterTarget);
  }
}