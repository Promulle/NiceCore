﻿namespace NiceCore.Filtering
{
  /// <summary>
  /// Helper class for creation of Filter Conditions
  /// </summary>
  public static class NcFilterConditions
  {
    /// <summary>
    /// Creates a new default impl for INcFilterCondition with provided values
    /// </summary>
    /// <param name="i_PropertyName"></param>
    /// <param name="i_Operation"></param>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public static DefaultNcFilterCondition Create(string i_PropertyName, FilterOperations i_Operation, object i_Value)
    {
      return new DefaultNcFilterCondition()
      {
        Operation = i_Operation,
        PropertyName = i_PropertyName,
        Value = i_Value
      };
    }

    /// <summary>
    /// Creates an equals filter condition that compares the value i_PropertyName with i_Value
    /// </summary>
    /// <param name="i_PropertyName"></param>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public static DefaultNcFilterCondition CreateEquals(string i_PropertyName, object i_Value)
    {
      return Create(i_PropertyName, FilterOperations.Equals, i_Value);
    }

    /// <summary>
    /// Create Contains
    /// </summary>
    /// <param name="i_PropertyName"></param>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public static DefaultNcFilterCondition CreateContains(string i_PropertyName, object i_Value)
    {
      return Create(i_PropertyName, FilterOperations.Contains, i_Value);
    }

    /// <summary>
    /// Creates an greater filter condition that compares the value i_PropertyName with i_Value
    /// </summary>
    /// <param name="i_PropertyName"></param>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public static DefaultNcFilterCondition CreateGreater(string i_PropertyName, object i_Value)
    {
      return Create(i_PropertyName, FilterOperations.Greater, i_Value);
    }

    /// <summary>
    /// Creates an in filter condition that compares the value i_PropertyName with i_Value
    /// </summary>
    /// <param name="i_PropertyName"></param>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public static DefaultNcFilterCondition CreateIn(string i_PropertyName, object i_Value)
    {
      return Create(i_PropertyName, FilterOperations.In, i_Value);
    }

    /// <summary>
    /// Creates an overlaps filter condition that compares the value i_PropertyName with i_Value
    /// </summary>
    /// <param name="i_PropertyName"></param>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public static DefaultNcFilterCondition CreateOverlaps(string i_PropertyName, object i_Value)
    {
      return Create(i_PropertyName, FilterOperations.Overlaps, i_Value);
    }

    /// <summary>
    /// Creates an less filter condition that compares the value i_PropertyName with i_Value
    /// </summary>
    /// <param name="i_PropertyName"></param>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public static DefaultNcFilterCondition CreateLess(string i_PropertyName, object i_Value)
    {
      return Create(i_PropertyName, FilterOperations.Less, i_Value);
    }

    /// <summary>
    /// Creates a like filter.
    /// </summary>
    /// <param name="i_PropertyName"></param>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public static DefaultNcFilterCondition CreateLike(string i_PropertyName, object i_Value)
    {
      return Create(i_PropertyName, FilterOperations.Like, i_Value);
    }
  }
}
