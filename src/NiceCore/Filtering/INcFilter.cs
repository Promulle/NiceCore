﻿using System.Collections.Generic;

namespace NiceCore.Filtering
{
  /// <summary>
  /// Class that will be translated into .Where Linq statements
  /// </summary>
  public interface INcFilter
  {
    /// <summary>
    /// Sub Filters
    /// </summary>
    List<DefaultNcFilter> SubFilters { get; }

    /// <summary>
    /// Conditions that get translated into .Where statements
    /// </summary>
    List<DefaultNcFilterCondition> Conditions { get; }

    /// <summary>
    /// Link of Filter
    /// </summary>
    FilterLinks FilterLink { get; }
  }
}
