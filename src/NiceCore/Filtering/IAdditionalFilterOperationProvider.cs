﻿using System.Linq.Expressions;

namespace NiceCore.Filtering
{
  /// <summary>
  /// Provider that enables more functionality for certain fitler operations for filter conditions
  /// </summary>
  public interface IAdditionalFilterOperationProvider
  {
    /// <summary>
    /// Operation that is extended by this provider
    /// </summary>
    FilterOperations FilterOperation { get; }

    /// <summary>
    /// Provide the expression based on the expressions passed
    /// </summary>
    /// <param name="i_PropertyExpression"></param>
    /// <param name="i_ValueExpression"></param>
    /// <returns></returns>
    Expression ProvideExpression(Expression i_PropertyExpression, Expression i_ValueExpression);
  }
}
