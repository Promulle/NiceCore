﻿namespace NiceCore.Filtering
{
  /// <summary>
  /// How conditions inside a filter are linked
  /// </summary>
  public enum FilterLinks
  {
    /// <summary>
    /// And
    /// </summary>
    And = 0,

    /// <summary>
    /// Or
    /// </summary>
    Or = 1,

    /// <summary>
    /// Exclusive Or
    /// </summary>
    Xor = 2,
  }
}
