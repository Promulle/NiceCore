using System;
using System.Collections.Generic;

namespace NiceCore.Filtering
{
  /// <summary>
  /// Options for filter translation
  /// </summary>
  public class NcFilterTranslationOptions
  {
    /// <summary>
    /// Empty Instance
    /// </summary>
    public static readonly NcFilterTranslationOptions EMPTY = new();
    
    /// <summary>
    /// This setting determines whether any identifiers used to get properties should be converted to PascalCase (UpperCamelCase - every word beginning is capitalized)
    /// regardless what they actually are
    /// </summary>
    public bool TreatIdentifiersAsPascalCase { get; init; }
    
    /// <summary>
    /// Additional Providers for translating filters
    /// </summary>
    public IDictionary<FilterOperations, IAdditionalFilterOperationProvider> AdditionalProviders { get; init; }
    
    /// <summary>
    /// Additional Compare Expression Providers
    /// </summary>
    public IDictionary<Type, ICompareExpressionProvider> AdditionalCompareExpressionProviders { get; init; }
  }
}