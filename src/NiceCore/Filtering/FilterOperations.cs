﻿namespace NiceCore.Filtering
{
  /// <summary>
  /// Possible filter operations
  /// </summary>
  public enum FilterOperations
  {
    /// <summary>
    /// ==
    /// </summary>
    Equals = 0,

    /// <summary>
    /// greater
    /// </summary>
    Greater = 1,

    /// <summary>
    /// greater or Equals
    /// </summary>
    GreaterOrEquals = 2,

    /// <summary>
    /// less
    /// </summary>
    Less = 3,

    /// <summary>
    /// less or equals
    /// </summary>
    LessOrEquals = 4,

    /// <summary>
    /// Check if element is in collection
    /// </summary>
    In = 5,

    /// <summary>
    /// Checks if two intervals overlap
    /// </summary>
    Overlaps = 6,

    /// <summary>
    /// Performs like on string values
    /// </summary>
    Like = 7,
    
    /// <summary>
    /// !=
    /// </summary>
    NotEquals = 8,
    
    /// <summary>
    /// Checks if element is not in collection
    /// </summary>
    NotIn = 9,
    
    /// <summary>
    /// Is null
    /// </summary>
    IsNull = 10,
    
    /// <summary>
    /// Not null
    /// </summary>
    NotNull = 11,
    
    /// <summary>
    /// Contains
    /// </summary>
    Contains = 12,
  }
}
