﻿using NiceCore.Base;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using NiceCore.Base.Caching;
using NiceCore.Expressions;
using NiceCore.Filtering.Model;
using NiceCore.Reflection;
using NiceCore.Text;

namespace NiceCore.Filtering
{
  /// <summary>
  /// Static class that translates instances of INcFilter into LINQ
  /// </summary>
  public static class NcFilterTranslation
  {
    private static readonly MethodInfo STRING_CONTAINS = GetStringContains();
    private static readonly SimpleCache<Type, MethodInfo> m_ContainsMethodInfoCache = new();
    private static readonly SimpleCache<Type, Type> m_EnumerableTypeCache = new();
    //private static readonly SimpleCache<FilterExpressionKey, Func<object, object[], bool>> m_FilterFuncCache = new();

    private static MethodInfo GetStringContains()
    {
      var parameter = new List<MethodParameterType>()
      {
        MethodParameterType.Of(typeof(string))
      };
      var method = ReflectionUtils.GetMethodEx(typeof(string), nameof(string.Contains), parameter);
      if (method == null)
      {
        throw new NotSupportedException($"Couldnt find method {nameof(string.Contains)} in type {typeof(string)}");
      }

      return method;
    }

    /// <summary>
    /// Filters i_Collection using i_Filter
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    /// <param name="i_Collection"></param>
    /// <param name="i_Filter"></param>
    /// <param name="i_Options"></param>
    /// <returns></returns>
    public static IEnumerable<TValue> ApplyFilter<TValue>(IEnumerable<TValue> i_Collection, INcFilter i_Filter, NcFilterTranslationOptions i_Options)
    {
      if (i_Filter == null || i_Collection?.Any() != true || (i_Filter.Conditions?.Any() != true && i_Filter.SubFilters?.Any() != true))
      {
        return i_Collection;
      }

      var coll = i_Collection;
      var filterPredicate = ConstructExpressionFromFilter<TValue>(i_Filter, i_Options, FilterTargets.Enumerable);
      if (filterPredicate != null)
      {
        var compiled = filterPredicate.Compile();
        return coll.Where(r => compiled.Invoke(r));
      }

      return coll;
    }

    /// <summary>
    /// Filters i_Collection using i_Filter
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    /// <param name="i_Collection"></param>
    /// <param name="i_Filter"></param>
    /// <param name="i_Options"></param>
    /// <returns></returns>
    public static IQueryable<TValue> ApplyFilter<TValue>(IQueryable<TValue> i_Collection, INcFilter i_Filter, NcFilterTranslationOptions i_Options)
    {
      if (i_Filter == null || (i_Filter.Conditions?.Any() != true && i_Filter.SubFilters?.Any() != true))
      {
        return i_Collection;
      }

      var coll = i_Collection;
      var filterPredicate = ConstructExpressionFromFilter<TValue>(i_Filter, i_Options, FilterTargets.Queryable);
      if (filterPredicate != null)
      {
        return coll.Where(filterPredicate);
      }

      return coll;
    }

    /// <summary>
    /// Translates i_Filter to a func
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    /// <param name="i_Filter"></param>
    /// <param name="i_Options"></param>
    /// <param name="i_FilterTarget"></param>
    /// <returns></returns>
    public static Func<TValue, bool> TranslateFilter<TValue>(INcFilter i_Filter, NcFilterTranslationOptions i_Options, FilterTargets i_FilterTarget)
    {
      if (i_Filter == null)
      {
        return null;
      }

      return TranslateToFilterExpression<TValue>(i_Filter, i_Options, i_FilterTarget).Compile();
    }

    /// <summary>
    /// Translates i_Filter to a func
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    /// <param name="i_Filter"></param>
    /// <param name="i_Options"></param>
    /// <param name="i_FilterTarget"></param>
    /// <returns></returns>
    public static Expression<Func<TValue, bool>> TranslateToFilterExpression<TValue>(INcFilter i_Filter, NcFilterTranslationOptions i_Options, FilterTargets i_FilterTarget)
    {
      if (i_Filter == null)
      {
        return null;
      }

      return ConstructExpressionFromFilter<TValue>(i_Filter, i_Options, i_FilterTarget);
    }

    /// <summary>
    /// Creates a filter expression from i_Filter using the additional provider to realize special actions
    /// </summary>
    /// <param name="i_Filter"></param>
    /// <param name="i_Options"></param>
    /// <param name="i_Target"></param>
    /// <typeparam name="TValue"></typeparam>
    /// <returns></returns>
    public static Expression<Func<TValue, bool>> ConstructExpressionFromFilter<TValue>(INcFilter i_Filter, NcFilterTranslationOptions i_Options, FilterTargets i_Target)
    {
      var instParameterExpression = Expression.Parameter(typeof(TValue));
      var filterBody = CreateExpressionForFilter(i_Filter, instParameterExpression, i_Options, i_Target);
      if (filterBody != null)
      {
        //Console.WriteLine($"Created filter expression: {filterBody}");
        var lambdaExpr = Expression.Lambda<Func<TValue, bool>>(filterBody, instParameterExpression);
        //Console.WriteLine($"Full expr: {lambdaExpr}");
        return lambdaExpr;
      }

      return null;
    }

    private static Expression CreateExpressionForFilter(INcFilter i_Filter, ParameterExpression i_InstParameterExpression, NcFilterTranslationOptions i_Options, FilterTargets i_FilterTarget)
    {
      var parts = new List<Expression>();
      var conds = i_Filter.Conditions.ToList();
      for (var i = 0; i < conds.Count; i++)
      {
        var cond = conds[i];
        parts.Add(ConstructFilterPartFromCondition(cond, i_InstParameterExpression, i_Options, i_FilterTarget));
      }

      var joined = GetCorrectFilterBodyExpression(i_Filter.FilterLink, parts);
      if (i_Filter.SubFilters?.Any() == true)
      {
        var partsPerFilter = i_Filter.SubFilters.ConvertAll(r => CreateExpressionForFilter(r, i_InstParameterExpression, i_Options, i_FilterTarget));
        var subFiltersExpression = GetCorrectFilterBodyExpression(i_Filter.FilterLink, partsPerFilter);
        if (joined != null)
        {
          var newParts = new List<Expression>();
          if (joined != null)
          {
            newParts.Add(joined);
          }

          newParts.Add(subFiltersExpression);
          return GetCorrectFilterBodyExpression(i_Filter.FilterLink, newParts);
        }

        return subFiltersExpression;
      }

      return joined;
    }

    private static Expression GetCorrectFilterBodyExpression(FilterLinks i_Link, List<Expression> i_Parts)
    {
      if (i_Parts.Count < 1)
      {
        return null;
      }

      if (i_Parts.Count > 1)
      {
        return CreateComplexFilterBodyExpression(i_Link, i_Parts);
      }

      return i_Parts[0];
    }

    private static Expression CreateComplexFilterBodyExpression(FilterLinks i_Link, List<Expression> i_Parts)
    {
      var linkedExpression = LinkExpression(i_Link, i_Parts[0], i_Parts[1]);
      if (i_Parts.Count > 2)
      {
        for (var i = 2; i < i_Parts.Count; i++)
        {
          linkedExpression = LinkExpression(i_Link, linkedExpression, i_Parts[i]);
        }
      }

      return linkedExpression;
    }

    private static Expression LinkExpression(FilterLinks i_Link, Expression i_LeftPart, Expression i_RightPart)
    {
      return i_Link switch
      {
        FilterLinks.And => Expression.AndAlso(i_LeftPart, i_RightPart),
        FilterLinks.Or => Expression.OrElse(i_LeftPart, i_RightPart),
        FilterLinks.Xor => Expression.ExclusiveOr(i_LeftPart, i_RightPart),
        _ => throw new NotSupportedException($"Unknown link provided: {i_Link}")
      };
    }

    private static Expression ConstructFilterPartFromCondition(INcFilterCondition i_Condition, ParameterExpression i_InstanceParameterExpression, NcFilterTranslationOptions i_Options, FilterTargets i_FilterTarget)
    {
      var operation = MapSpecialOperation(i_Condition.Operation);
      var propertyExpression = ResolveMemberExpression(i_InstanceParameterExpression, i_Condition, i_Options);
      var value = HandlePossibleEnumValue(i_Condition, propertyExpression.MemberExpression);
      var valueExpression = GetValueExpressionNullableSafe(i_Condition, value, propertyExpression.MemberExpression);
      return GetCorrectCompareExpression(operation, propertyExpression.ActualExpression, valueExpression, i_Options, i_FilterTarget);
    }

    /// <summary>
    /// This method makes sure that enum values are translated correctly. Only applies if i_PropertyExpression is of type enum
    /// if i_Condition.Value is Enum or null -> return i_Condition.Value
    /// else if the value is a string, try to treat the value as an identifier for an enum member.
    /// If the value is an int -> For now throw NotImplementedException, but later this will translate the int to the corresponding
    /// enum value
    /// </summary>
    /// <param name="i_Condition"></param>
    /// <param name="i_PropertyExpression">Expression that defines the access to the enum property. Delivers the type of the enum</param>
    /// <returns></returns>
    private static object HandlePossibleEnumValue(INcFilterCondition i_Condition, MemberExpression i_PropertyExpression)
    {
      if (Nullable.GetUnderlyingType(i_PropertyExpression.Type) is { } actualType)
      {
        return HandlePossibleEnumType(i_Condition, actualType);
      }

      return HandlePossibleEnumType(i_Condition, i_PropertyExpression.Type);
    }

    private static object HandlePossibleEnumType(INcFilterCondition i_Condition, Type i_Type)
    {
      if (i_Type.IsEnum)
      {
        if (i_Condition.Value == null)
        {
          return i_Condition.Value;
        }

        if (i_Condition.Value.GetType().IsEnum || i_Condition.Value is int)
        {
          return i_Condition.Value;
        }

        if (i_Condition.Value is string casted)
        {
          return (int)Enum.Parse(i_Type, casted, true);
        }

        if (i_Condition.Value is IEnumerable<string> valueCollection)
        {
          var res = new List<int>(10);
          foreach (var value in valueCollection)
          {
            res.Add((int)Enum.Parse(i_Type, value, true));
          }

          return res;
        }

        if (i_Condition.Value is IEnumerable<int> intValueCollection)
        {
          return intValueCollection;
        }

        var enumerableType = m_EnumerableTypeCache.GetOrCreate(i_Type, () => CreateEnumerableEnumTypeFunc(i_Type));
        if (enumerableType.IsInstanceOfType(i_Condition.Value))
        {
          if (i_Condition.Value is IEnumerable nonGenericEnumerable)
          {
            var res = new List<int>();
            foreach (var val in nonGenericEnumerable)
            {
              res.Add((int)(val));
            }

            return res;
          }

          throw new InvalidOperationException($"Translation from a value of type {i_Condition.Value.GetType().FullName} to an enum value failed: {i_Condition.Value.GetType().FullName} implements {enumerableType.FullName} but not {typeof(IEnumerable)} something is wrong here.");
        }

        throw new NotImplementedException($"Translation from a value of type {i_Condition.Value.GetType().FullName} to an enum value is not implemented yet.");
      }

      return i_Condition.Value;
    }

    private static Type CreateEnumerableEnumTypeFunc(Type i_EnumType)
    {
      var baseType = typeof(IEnumerable<>);
      var generic = baseType.MakeGenericType(i_EnumType);
      return generic;
    }

    private static FilterOperations MapSpecialOperation(FilterOperations i_Operation)
    {
      if (i_Operation == FilterOperations.IsNull)
      {
        return FilterOperations.Equals;
      }

      if (i_Operation == FilterOperations.NotNull)
      {
        return FilterOperations.NotEquals;
      }

      return i_Operation;
    }

    private static Expression GetValueExpressionNullableSafe(INcFilterCondition i_Condition, object i_Value, Expression i_PropertyExpression)
    {
      if (i_Value == null || i_Condition.Operation is FilterOperations.IsNull or FilterOperations.NotNull)
      {
        //if no value was provided -> use null constant expression whose type matches the property it is compared to
        return Expression.Constant(null, i_PropertyExpression.Type);
      }

      //is the "compared to" side nullable but the actual value is passed as "normal"?
      if (IsGenericNullable(i_PropertyExpression.Type) && !IsGenericNullable(i_Value.GetType()))
      {
        var genericArguments = i_PropertyExpression.Type.GenericTypeArguments;
        if (genericArguments.Length == 1)
        {
          var firstGenericArgument = genericArguments[0];
          //make sure that types actually match so that conversion to nullable just works
          if (firstGenericArgument == i_Value.GetType())
          {
            return Expression.Constant(i_Value, i_PropertyExpression.Type);
          }

          if (firstGenericArgument.IsEnum)
          {
            return Expression.Constant(i_Value, typeof(Nullable<>).MakeGenericType(i_Value.GetType()));
          }
        }
        

        throw new InvalidOperationException($"Either {i_PropertyExpression.Type.FullName} has more than one generic parameter or the types of the parameter and the value do not fit.");
      }
      else if (!IsGenericNullable(i_PropertyExpression.Type) && IsGenericNullable(i_Value.GetType()))
      {
        //comparing a not-nullable to a nullable sent value is not supported, since this would mean that we would have to call .Value on the nullable which might result in a null pointer
        //and checking for null is not really possible when this is used to translate a condition to Sql-Linq
        throw new InvalidOperationException($"Expression {i_PropertyExpression} was of type {i_PropertyExpression.Type.FullName} but comparing this to a nullable type {i_Condition.Value.GetType()} is not supported.");
      }

      //no nullables involved, or both types match -> just return the constant expression
      return Expression.Constant(i_Value);
    }

    private static bool IsGenericNullable(Type i_Type)
    {
      return i_Type.IsGenericType && !i_Type.IsGenericTypeDefinition && i_Type.GetGenericTypeDefinition() == typeof(Nullable<>);
    }

    private static FilterPropertyExpressions ResolveMemberExpression(ParameterExpression i_InstanceParameterExpression, INcFilterCondition i_Condition, NcFilterTranslationOptions i_Options)
    {
      var propertyExpression = ExpressionUtils.GetPropertyExpression(i_InstanceParameterExpression, i_Condition.PropertyName, i_Options);
      if (propertyExpression.Type.IsEnum)
      {
        return new()
        {
          MemberExpression = propertyExpression,
          ActualExpression = Expression.Convert(propertyExpression, typeof(int))
        };
      }

      if (IsNullableEnumType(propertyExpression))
      {
        return new()
        {
          MemberExpression = propertyExpression,
          ActualExpression = Expression.Convert(propertyExpression, typeof(int?))
        };
      }

      return new()
      {
        ActualExpression = propertyExpression,
        MemberExpression = propertyExpression
      };
    }

    private static bool IsNullableEnumType(MemberExpression i_Expression)
    {
      if (IsGenericNullable(i_Expression.Type))
      {
        var args = i_Expression.Type.GenericTypeArguments;
        if (args.Length > 0)
        {
          return args[0].IsEnum;
        }
      }

      return false;
    }

    private static Expression GetCorrectCompareExpression(FilterOperations i_Operation, Expression i_PropertyExpression, Expression i_ValueExpression, NcFilterTranslationOptions i_Options, FilterTargets i_FilterTarget)
    {
      var convertedValueExpr = i_ValueExpression; //i_Operation != FilterOperations.In ? Expression.Convert(i_ValueExpression, i_PropertyExpression.Type) : i_ValueExpression;
      var type = Nullable.GetUnderlyingType(i_PropertyExpression.Type) ?? i_PropertyExpression.Type;
      if (i_Options.AdditionalCompareExpressionProviders != null && i_Options.AdditionalCompareExpressionProviders.TryGetValue(type, out var compareExpressionProvider))
      {
        return compareExpressionProvider.GetCompareExpression(i_Operation, i_PropertyExpression, convertedValueExpr, i_Options, i_FilterTarget);
      }
      var compareExpression = i_Operation switch
      {
        FilterOperations.Equals => Expression.Equal(i_PropertyExpression, convertedValueExpr),
        FilterOperations.NotEquals => Expression.NotEqual(i_PropertyExpression, convertedValueExpr),
        FilterOperations.Greater => Expression.GreaterThan(i_PropertyExpression, convertedValueExpr),
        FilterOperations.GreaterOrEquals => Expression.GreaterThanOrEqual(i_PropertyExpression, convertedValueExpr),
        FilterOperations.Less => Expression.LessThan(i_PropertyExpression, convertedValueExpr),
        FilterOperations.LessOrEquals => Expression.LessThanOrEqual(i_PropertyExpression, convertedValueExpr),
        FilterOperations.Overlaps => CreateOverlapsExpression(i_PropertyExpression, convertedValueExpr),
        FilterOperations.In => CreateInExpression(i_PropertyExpression, convertedValueExpr),
        FilterOperations.NotIn => CreateNotInExpression(i_PropertyExpression, convertedValueExpr),
        FilterOperations.Contains => CreateContainsExpression(i_PropertyExpression, convertedValueExpr),
        _ => null
      };
      if (compareExpression == null)
      {
        if (!i_Options.AdditionalProviders.TryGetValue(i_Operation, out var addinProvider))
        {
          throw new InvalidOperationException($"Operation {i_Operation} was neither translated by defaults nor by any addin provider.");
        }

        return addinProvider.ProvideExpression(i_PropertyExpression, i_ValueExpression);
      }

      return compareExpression;
    }

    private static Expression CreateContainsExpression(Expression i_PropertyExpression, Expression i_ConvertedValueExpr)
    {
      if (i_PropertyExpression.Type != typeof(string))
      {
        throw new InvalidOperationException($"Could not create contains expression for property {i_PropertyExpression} because the given property is not of type {typeof(string)}");
      }

      return Expression.Call(i_PropertyExpression, STRING_CONTAINS, i_ConvertedValueExpr);
    }

    private static Expression CreateInExpression(Expression i_PropertyExpression, Expression i_ValueExpression)
    {
      if (typeof(IList).IsAssignableFrom(i_ValueExpression.Type))
      {
        var methodInfo = m_ContainsMethodInfoCache.GetOrCreate(i_ValueExpression.Type, static type => type.GetMethod(nameof(List<object>.Contains)));
        if (methodInfo != null)
        {
          return Expression.Call(i_ValueExpression, methodInfo, i_PropertyExpression);
        }

        throw new InvalidOperationException($"Cannot create IN expression: {i_ValueExpression.Type.FullName} does not contain a definition for {nameof(List<object>.Contains)}");
      }

      throw new InvalidOperationException($"Cannot create IN expression: {i_ValueExpression.Type.FullName} does not implement {typeof(IList).FullName}.");
    }

    private static Expression CreateNotInExpression(Expression i_PropertyExpression, Expression i_ValueExpression)
    {
      if (typeof(IList).IsAssignableFrom(i_ValueExpression.Type))
      {
        var methodInfo = m_ContainsMethodInfoCache.GetOrCreate(i_ValueExpression.Type, () => i_ValueExpression.Type.GetMethod(nameof(List<object>.Contains)));
        if (methodInfo != null)
        {
          return Expression.Not(Expression.Call(i_ValueExpression, methodInfo, i_PropertyExpression));
        }

        throw new InvalidOperationException($"Cannot create IN expression: {i_ValueExpression.Type.FullName} does not contain a definition for {nameof(List<object>.Contains)}");
      }

      throw new InvalidOperationException($"Cannot create IN expression: {i_ValueExpression.Type.FullName} does not implement {typeof(IList).FullName}.");
    }

    private static Expression CreateOverlapsExpression(Expression i_PropertyExpression, Expression i_ValueExpression)
    {
      if (typeof(INcTimeInterval).IsAssignableFrom(i_PropertyExpression.Type) && typeof(INcTimeInterval).IsAssignableFrom(i_ValueExpression.Type))
      {
        var propFrom = Expression.Property(i_PropertyExpression, nameof(INcTimeInterval.TimeFrom));
        var propTo = Expression.Property(i_PropertyExpression, nameof(INcTimeInterval.TimeTo));
        var valFrom = Expression.Property(i_ValueExpression, nameof(INcTimeInterval.TimeFrom));
        var valTo = Expression.Property(i_ValueExpression, nameof(INcTimeInterval.TimeTo));
        return Expression.AndAlso(Expression.LessThan(propFrom, valTo), Expression.GreaterThan(propTo, valFrom));
      }

      throw new InvalidOperationException($"Cannot create Overlaps expression: {typeof(INcTimeInterval).FullName} needs to be implemented by both: Property and Value.");
    }
  }
}