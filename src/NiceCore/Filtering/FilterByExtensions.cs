﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace NiceCore.Filtering
{
  /// <summary>
  /// Extension methods to filter collections using INcFilter instances
  /// </summary>
  public static class FilterByExtensions
  {
    /// <summary>
    /// Filters i_Values using i_Filter
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Values"></param>
    /// <param name="i_Filter"></param>
    /// <param name="i_Options"></param>
    /// <returns></returns>
    public static IEnumerable<T> FilterBy<T>(this IEnumerable<T> i_Values, INcFilter i_Filter, NcFilterTranslationOptions i_Options)
    {
      return NcFilterTranslation.ApplyFilter(i_Values, i_Filter, i_Options);
    }

    /// <summary>
    /// Filters i_Values using i_Filter
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Values"></param>
    /// <param name="i_Filter"></param>
    /// <param name="i_Options"></param>
    /// <returns></returns>
    public static IQueryable<T> FilterBy<T>(this IQueryable<T> i_Values, INcFilter i_Filter, NcFilterTranslationOptions i_Options)
    {
      return NcFilterTranslation.ApplyFilter(i_Values, i_Filter, i_Options);
    }
    
    /// <summary>
    /// Filters i_Values using i_Filter
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Values"></param>
    /// <param name="i_Filter"></param>
    /// <returns></returns>
    public static IQueryable<T> FilterByExpr<T>(this IQueryable<T> i_Values, Expression<Func<T, bool>> i_Filter)
    {
      if (i_Filter == null)
      {
        return i_Values;
      }
      return i_Values.Where(i_Filter);
    }
  }
}
