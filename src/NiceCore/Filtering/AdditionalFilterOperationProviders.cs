﻿using System.Collections.Generic;

namespace NiceCore.Filtering
{
  /// <summary>
  /// Static helper class for providing the Dictionary
  /// </summary>
  public static class AdditionalFilterOperationProviders
  {
    /// <summary>
    /// Returns an empty dictioary
    /// </summary>
    /// <returns></returns>
    public static IDictionary<FilterOperations, IAdditionalFilterOperationProvider> Empty()
    {
      return new Dictionary<FilterOperations, IAdditionalFilterOperationProvider>();
    }
  }
}
