﻿using System;
using System.Collections.Generic;

namespace NiceCore.Filtering.Caching
{
  /// <summary>
  /// Key used by NcFilterTranslation
  /// </summary>
  public class FilterExpressionKey
  {
    /// <summary>
    /// Type
    /// </summary>
    public Type Type { get; init; }

    /// <summary>
    /// Links
    /// </summary>
    public FilterLinks Links { get; init; }

    /// <summary>
    /// SubKeys
    /// </summary>
    public List<FilterSubExpressionKey> SubKeys { get; init; }

    /// <summary>
    /// Equals
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj)
    {
      return obj is FilterExpressionKey key &&
             EqualityComparer<Type>.Default.Equals(Type, key.Type) &&
             Links == key.Links &&
             EqualityComparer<List<FilterSubExpressionKey>>.Default.Equals(SubKeys, key.SubKeys);
    }

    /// <summary>
    /// GetHashCode
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode()
    {
      return HashCode.Combine(Type, Links, SubKeys);
    }
  }
}
