﻿using System;

namespace NiceCore.Filtering.Caching
{
  /// <summary>
  /// sub key for single expression
  /// </summary>
  public class FilterSubExpressionKey
  {
    /// <summary>
    /// Property Name
    /// </summary>
    public string PropertyName { get; set; }

    /// <summary>
    /// Operation
    /// </summary>
    public FilterOperations Operation { get; set; }

    /// <summary>
    /// Equals
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj)
    {
      return obj is FilterSubExpressionKey key &&
             PropertyName == key.PropertyName &&
             Operation == key.Operation;
    }

    /// <summary>
    /// GetHashCode
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode()
    {
      return HashCode.Combine(PropertyName, Operation);
    }
  }
}
