﻿namespace NiceCore.Filtering
{
  /// <summary>
  /// Default impl for INcFilterCondition
  /// </summary>
  public class DefaultNcFilterCondition : INcFilterCondition
  {
    /// <summary>
    /// Property name
    /// </summary>
    public string PropertyName { get; set; }

    /// <summary>
    /// Operation to be used
    /// </summary>
    public FilterOperations Operation { get; set; }

    /// <summary>
    /// Value
    /// </summary>
    public object Value { get; set; }
  }
}
