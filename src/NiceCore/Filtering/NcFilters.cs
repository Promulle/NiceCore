﻿using System.Collections.Generic;
using System.Linq;

namespace NiceCore.Filtering
{
  /// <summary>
  /// Static Helper class for creation of filters
  /// </summary>
  public static class NcFilters
  {
    /// <summary>
    /// Creates a new instance of the default NcFilter impl
    /// </summary>
    /// <returns></returns>
    public static DefaultNcFilter CreateAnd(IEnumerable<DefaultNcFilterCondition> i_Conditions)
    {
      return Create(i_Conditions, FilterLinks.And);
    }

    /// <summary>
    /// Creates a new instance of the default NcFilter impl
    /// </summary>
    /// <param name="i_Conditions"></param>
    /// <returns></returns>
    public static DefaultNcFilter CreateAnd(params DefaultNcFilterCondition[] i_Conditions)
    {
      return Create(i_Conditions, FilterLinks.And);
    }

    /// <summary>
    /// Creates a new instance of the default NcFilter impl
    /// </summary>
    /// <param name="i_SubFilters"></param>
    /// <param name="i_Conditions"></param>
    /// <returns></returns>
    public static DefaultNcFilter CreateAnd(IEnumerable<DefaultNcFilter> i_SubFilters, params DefaultNcFilterCondition[] i_Conditions)
    {
      return Create(i_Conditions, i_SubFilters, FilterLinks.And);
    }

    /// <summary>
    /// Creates a new instance of the default NcFilter impl
    /// </summary>
    /// <returns></returns>
    public static DefaultNcFilter CreateOr(IEnumerable<DefaultNcFilterCondition> i_Conditions)
    {
      return Create(i_Conditions, FilterLinks.Or);
    }

    /// <summary>
    /// Creates a new instance of the default NcFilter impl
    /// </summary>
    /// <returns></returns>
    public static DefaultNcFilter CreateXor(IEnumerable<DefaultNcFilterCondition> i_Conditions)
    {
      return Create(i_Conditions, FilterLinks.Xor);
    }

    /// <summary>
    /// Creates an INcFilter from a single condition
    /// </summary>
    /// <param name="i_Condition"></param>
    /// <returns></returns>
    public static DefaultNcFilter CreateAnd(DefaultNcFilterCondition i_Condition)
    {
      return CreateAnd(new List<DefaultNcFilterCondition>() { i_Condition });
    }

    /// <summary>
    /// Creates an INcFilter from a single condition
    /// </summary>
    /// <param name="i_Condition"></param>
    /// <returns></returns>
    public static DefaultNcFilter CreateOr(DefaultNcFilterCondition i_Condition)
    {
      return CreateOr(new List<DefaultNcFilterCondition>() { i_Condition });
    }

    /// <summary>
    /// Creates an INcFilter from a single condition
    /// </summary>
    /// <param name="i_Condition"></param>
    /// <returns></returns>
    public static DefaultNcFilter CreateXor(DefaultNcFilterCondition i_Condition)
    {
      return CreateXor(new List<DefaultNcFilterCondition>() { i_Condition });
    }

    /// <summary>
    /// Creates a new instance of the default NcFilter impl
    /// </summary>
    /// <returns></returns>
    public static DefaultNcFilter Create(IEnumerable<DefaultNcFilterCondition> i_Conditions, FilterLinks i_Link)
    {
      return Create(i_Conditions, new List<DefaultNcFilter>(), i_Link);
    }

    /// <summary>
    /// Creates a new instance of the default NcFilter impl
    /// </summary>
    /// <returns></returns>
    public static DefaultNcFilter Create(IEnumerable<DefaultNcFilterCondition> i_Conditions, IEnumerable<DefaultNcFilter> i_Subfilters, FilterLinks i_Link)
    {
      return new DefaultNcFilter()
      {
        Conditions = i_Conditions.ToList(),
        SubFilters = i_Subfilters.ToList(),
        FilterLink = i_Link
      };
    }
  }
}
