using System.Linq.Expressions;

namespace NiceCore.Filtering.Model
{
  /// <summary>
  /// Filter property expression model
  /// </summary>
  public class FilterPropertyExpressions
  {
    /// <summary>
    /// The member expression that is actually shows the property access
    /// </summary>
    public MemberExpression MemberExpression { get; set; }
    
    /// <summary>
    /// Actual expression, might be the same as MemberExpression, might be a cast expression
    /// for to enable for example Enum comparisons
    /// </summary>
    public Expression ActualExpression { get; set; }
  }
}
