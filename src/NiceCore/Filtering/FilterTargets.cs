namespace NiceCore.Filtering
{
  /// <summary>
  /// Filter Targets
  /// </summary>
  public enum FilterTargets
  {
    /// <summary>
    /// Queryable
    /// </summary>
    Queryable = 0,
    
    /// <summary>
    /// Enumerable
    /// </summary>
    Enumerable = 1
  }
}