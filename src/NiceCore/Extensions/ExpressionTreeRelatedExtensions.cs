using System;
using System.Threading.Tasks;

namespace NiceCore.Extensions
{
  /// <summary>
  /// Expression Tree Related Extensions
  /// </summary>
  public static class ExpressionTreeRelatedExtensions
  {
    /// <summary>
    /// Method that converts the given task to a task of object.
    /// This is needed for expression trees since you cannot use "await" in Expression Trees but if you want Task of object as result you have to convert the
    /// Task that is returned by the expression body. 
    /// </summary>
    /// <param name="i_Task"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    /// <exception cref="ArgumentException"></exception>
    public static async Task<object> AwaitToObject<T>(this Task<T> i_Task)
    {
      if (i_Task == null)
      {
        throw new ArgumentException($"Cannot await task to object: {nameof(i_Task)} was passed as null. Cannot await Null Task");
      }

      return await i_Task.ConfigureAwait(false);
    }
  }
}
