﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace NiceCore.Extensions
{
  /// <summary>
  /// Extion Methods
  /// </summary>
  public static class ExtensionMethods
  {
    /// <summary>
    /// Map that calls i_MapperFunc for each entry in i_Enumerable.
    /// This does not implement lazy execution. (hence the list return type)
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="i_Enumerable"></param>
    /// <param name="i_MapperFunc"></param>
    /// <returns></returns>
    public static async Task<List<TResult>> MapItems<TValue, TResult>(this IEnumerable<TValue> i_Enumerable, Func<TValue, Task<TResult>> i_MapperFunc)
    {
      var res = new List<TResult>();
      foreach (var entry in i_Enumerable)
      {
        var mappedEntry = await i_MapperFunc.Invoke(entry).ConfigureAwait(false);
        res.Add(mappedEntry);
      }
      return res;
    }

    /// <summary>
    /// Same as TryGetValue but encapsulates the out parameter
    /// </summary>
    /// <param name="i_Dictionary"></param>
    /// <param name="i_Key"></param>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <returns></returns>
    public static TValue GetOrDefault<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> i_Dictionary, TKey i_Key)
    {
      i_Dictionary.TryGetValue(i_Key, out var value);
      return value;
    }
    /// <summary>
    /// ForEach for all enumerables
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Enumerable"></param>
    /// <param name="i_Method"></param>
    public static void ForEachItem<T>(this IEnumerable<T> i_Enumerable, Action<T> i_Method)
    {
      foreach (var inst in i_Enumerable)
      {
        i_Method(inst);
      }
    }

    /// <summary>
    /// ForEach for all enumerables. Awaits completion of each execution of i_AsyncFunc
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Enumerable"></param>
    /// <param name="i_AsyncFunc"></param>
    public static async Task ForEachItem<T>(this IEnumerable<T> i_Enumerable, Func<T, Task> i_AsyncFunc)
    {
      foreach (var inst in i_Enumerable)
      {
        await i_AsyncFunc(inst).ConfigureAwait(false);
      }
    }

    /// <summary>
    /// ForEach for all enumerables, that stops the loop when i_Function returns false
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Enumerable"></param>
    /// <param name="i_Function"></param>
    public static void ForEachItem<T>(this IEnumerable<T> i_Enumerable, Func<T, bool> i_Function)
    {
      foreach (var inst in i_Enumerable)
      {
        var cont = i_Function(inst);
        if (!cont)
        {
          break;
        }
      }
    }

    /// <summary>
    /// Maps i_Elements to a enumerable of Destination in parallel. So correct sequence is not garan
    /// </summary>
    /// <typeparam name="Original"></typeparam>
    /// <typeparam name="Destination"></typeparam>
    /// <param name="i_Elements"></param>
    /// <param name="i_TransformFunc"></param>
    /// <returns></returns>
    public static IEnumerable<Destination> MapParallel<Original, Destination>(this IEnumerable<Original> i_Elements, Func<Original, Destination> i_TransformFunc)
    {
      var res = new ConcurrentBag<Destination>();
      Parallel.ForEach(i_Elements, r => res.Add(i_TransformFunc.Invoke(r)));
      return res;
    }
    
    /// <summary>
    /// Maps i_Elements to a enumerable of Destination in parallel. So correct sequence is not garan
    /// </summary>
    /// <typeparam name="Original"></typeparam>
    /// <typeparam name="Destination"></typeparam>
    /// <param name="i_Elements"></param>
    /// <param name="i_TransformFunc"></param>
    /// <returns></returns>
    public static async Task<IEnumerable<Destination>> MapParallel<Original, Destination>(this IEnumerable<Original> i_Elements, Func<Original, Task<Destination>> i_TransformFunc)
    {
      var res = new ConcurrentBag<Destination>();
      await Parallel.ForEachAsync(i_Elements, async (r, token) => res.Add( await i_TransformFunc.Invoke(r).ConfigureAwait(false)));
      return res;
    }

    /// <summary>
    /// Element At
    /// </summary>
    /// <param name="i_Enumerable"></param>
    /// <param name="i_Index"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static T ElementAtEx<T>(this IReadOnlyCollection<T> i_Enumerable, int i_Index)
    {
      if (i_Enumerable is List<T> list)
      {
        return list[i_Index];
      }
      if (i_Enumerable is T[] array)
      {
        return array[i_Index];
      }

      return i_Enumerable.ElementAt(i_Index);
    }

    /// <summary>
    /// Serializes the object to pretty printed json. More costly than normal toString;
    /// </summary>
    /// <param name="i_Object"></param>
    /// <returns></returns>
    public static string ToJsonString(this object i_Object)
    {
      var options = new JsonSerializerOptions()
      {
        WriteIndented = true,
        ReferenceHandler = ReferenceHandler.IgnoreCycles
      };
      return JsonSerializer.Serialize(i_Object, options);
    }
  }
}
