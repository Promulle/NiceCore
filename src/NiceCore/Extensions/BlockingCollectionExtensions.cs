using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace NiceCore.Extensions
{
  /// <summary>
  /// Extension methods for blocking collection
  /// </summary>
  public static class BlockingCollectionExtensions
  {
    /// <summary>
    /// Function that blocks the current thread until i_Collection was marked complete and all entries were processed
    /// </summary>
    /// <param name="i_Collection">Collection that is blocking</param>
    /// <param name="i_Handler">Method that is called for every entry that is retrieved from the collection</param>
    /// <param name="i_Token">Token to cancel taking from collection</param>
    /// <typeparam name="T">Type of entries that the collection contains</typeparam>
    public static Task BlockUntilComplete<T>(this BlockingCollection<T> i_Collection, Action<T> i_Handler, CancellationToken i_Token)
    {
      return BlockUntilCompleteInternal(i_Collection, (value) =>
      {
        i_Handler.Invoke(value);
        return Task.CompletedTask;
      }, i_Token);
    }
    
    /// <summary>
    /// Function that blocks the current thread until i_Collection was marked complete and all entries were processed
    /// </summary>
    /// <param name="i_Collection">Collection that is blocking</param>
    /// <param name="i_AsyncHandler">Method that is called for every entry that is retrieved from the collection</param>
    /// <param name="i_Token">Token to cancel taking from collection</param>
    /// <typeparam name="T">Type of entries that the collection contains</typeparam>
    public static Task BlockUntilComplete<T>(this BlockingCollection<T> i_Collection, Func<T, Task> i_AsyncHandler, CancellationToken i_Token)
    {
      return BlockUntilCompleteInternal(i_Collection, i_AsyncHandler, i_Token);
    }

    private static async Task BlockUntilCompleteInternal<T>(BlockingCollection<T> i_Collection, Func<T, Task> i_Handler, CancellationToken i_Token)
    {
      var hasReachedEnd = false;
      while (!hasReachedEnd)
      {
        var (success, value) = TakeValue(i_Collection, i_Token);
        if (success)
        {
          await i_Handler.Invoke(value).ConfigureAwait(false);
        }
        else
        {
          hasReachedEnd = true;
        }
      }
    }
    
    private static (bool success, T value) TakeValue<T>(BlockingCollection<T> i_Collection, CancellationToken i_Token)
    {
      try
      {
        var element = i_Collection.Take(i_Token);
        return (true, element);
      }
      catch (InvalidOperationException )
      {
        return (false, default);
      }
    }
  }
}