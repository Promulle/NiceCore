﻿using NiceCore.Base.Caching;
using NiceCore.Base.Instances;
using NiceCore.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using NiceCore.Base;
using NiceCore.Text;

namespace NiceCore.Reflection
{
  /// <summary>
  /// Helper class for common reflection actions
  /// </summary>
  public static class ReflectionUtils
  {
    private const string ENUM_VALUE_FIELD = "value__";

    //caches for keys (returnType, ctorParameterTypes) what the function to create the wanted list type, using the ctor that fills the collection with an IEnumerable instance, is
    private static readonly SimpleCache<InstanceCreationFunctionKey, Func<IEnumerable, object>> m_CachedCollectionCreationFuncs = new();

    //caches for list types what function should be called to create them
    private static readonly SimpleCache<Type, Func<IList>> m_CachedListCreationFuncs = new();

    //caches for generic type arguments what the corresponding type for List of T is
    private static readonly SimpleCache<Type, Type> m_GenericTypeArgumentToListTypeMap = new();

    //caches for types what the corresponding for IEnumerable of T is
    private static readonly SimpleCache<Type, Type> m_CorrectEnumTypes = new();

    //caches for types whether they are generic collections or not
    private static readonly SimpleCache<Type, bool> m_IsGenericCollectionMap = new();

    //caches whether the openmType of the key is the "rough base" for the type to check of the key
    private static readonly SimpleCache<OpenGenericTypeIsBaseForKey, Result<Type>> m_OpenTypeIsBaseForCache = new();

    //caches the type of the last property in the path that is the key
    private static readonly SimpleCache<string, Type> m_TypeLastPropertyInPathCache = new();

    //caches the function that can be called to convert an int into an object
    private static readonly SimpleCache<Type, Func<int, object>> m_EnumCastFunc = new();

    /// <summary>
    /// Returns the type of the last part
    /// </summary>
    /// <param name="i_PropertyPath"></param>
    /// <param name="i_StartType"></param>
    /// <returns></returns>
    public static Type GetTypeOfLastPropertyInPath(string i_PropertyPath, Type i_StartType)
    {
      if (i_PropertyPath == null)
      {
        throw new ArgumentException($"Null is not allowed for parameter {nameof(i_PropertyPath)} in method {nameof(GetTypeOfLastPropertyInPath)}");
      }

      return m_TypeLastPropertyInPathCache.GetOrCreate(i_PropertyPath, () =>
      {
        var type  = i_StartType;
        var split = i_PropertyPath.Split(new[] {'.'}, StringSplitOptions.RemoveEmptyEntries);
        foreach (var propertyName in split)
        {
          var actualProperty = NcTextUtils.SingleWordToPascalCase(propertyName);
          var property       = type.GetProperty(actualProperty);
          if (property == null)
          {
            throw new InvalidOperationException($"Could not execute {nameof(GetTypeOfLastPropertyInPath)} because path contained wrong property: {actualProperty} could not be found on type {type.FullName}");
          }

          type = property.PropertyType;
        }

        return type;
      });
    }

    /// <summary>
    /// Function that works like Type.GetMethod but handles generics different
    /// </summary>
    /// <param name="i_Type"></param>
    /// <param name="i_MethodName"></param>
    /// <param name="i_Types"></param>
    /// <returns></returns>
    /// <exception cref="AmbiguousMatchException">Throws ambigous match exception if there are multiple methods found for description</exception>
    public static MethodInfo GetMethodEx(Type i_Type, string i_MethodName, IReadOnlyCollection<MethodParameterType> i_Types)
    {
      return GetMethodExUtil.GetMethodEx(i_Type, i_MethodName, i_Types);
    }

    /// <summary>
    /// Function that works like Type.GetMethod but handles generics different
    /// </summary>
    /// <param name="i_Type"></param>
    /// <param name="i_MethodName"></param>
    /// <param name="i_ParameterCount"></param>
    /// <param name="i_GenericArgsCount"></param>
    /// <param name="i_Types"></param>
    /// <returns></returns>
    /// <exception cref="AmbiguousMatchException">Throws ambigous match exception if there are multiple methods found for description</exception>
    public static MethodInfo GetMethodEx(Type i_Type, string i_MethodName, int i_ParameterCount, int i_GenericArgsCount, IReadOnlyCollection<MethodParameterType> i_Types)
    {
      return GetMethodExUtil.GetMethodEx(i_Type, i_GenericArgsCount, i_ParameterCount, i_MethodName, i_Types);
    }

    /// <summary>
    /// Function that works like Type.GetMethod but handles generics different
    /// </summary>
    /// <param name="i_Type"></param>
    /// <param name="i_MethodName"></param>
    /// <param name="i_ParameterCount"></param>
    /// <param name="i_GenericArgsCount"></param>
    /// <param name="i_ExpectedReturnType"></param>
    /// <param name="i_Types"></param>
    /// <returns></returns>
    /// <exception cref="AmbiguousMatchException">Throws ambigous match exception if there are multiple methods found for description</exception>
    public static MethodInfo GetMethodEx(Type i_Type, string i_MethodName, int i_ParameterCount, int i_GenericArgsCount, Type i_ExpectedReturnType, IReadOnlyCollection<MethodParameterType> i_Types)
    {
      return GetMethodExUtil.GetMethodEx(i_Type, i_GenericArgsCount, i_ParameterCount, i_MethodName, i_ExpectedReturnType, i_Types);
    }

    /// <summary>
    /// Checks if i_OpenGenericType (for example typeof(ListGEN_OPEN-CLOSE_GEN)) is the "rough base" for i_TypeToCheck (List of int)
    /// </summary>
    /// <param name="i_OpenGenericType"></param>
    /// <param name="i_TypeToCheck"></param>
    /// <returns></returns>
    public static Result<Type> OpenGenericTypeIsBaseFor(Type i_OpenGenericType, Type i_TypeToCheck)
    {
      var key = new OpenGenericTypeIsBaseForKey()
      {
        OpenGenericType = i_OpenGenericType,
        TypeToCheck     = i_TypeToCheck
      };
      return m_OpenTypeIsBaseForCache.GetOrCreate(key, static (key) => CheckOpenGenericTypeIsBaseFor(key.OpenGenericType, key.TypeToCheck));
    }

    private static Result<Type> CheckOpenGenericTypeIsBaseFor(Type i_OpenGenericType, Type i_TypeToCheck)
    {
      var genericArgumentsOpenType    = i_OpenGenericType.GetGenericArguments();
      var genericArgumentsTypeToCheck = i_TypeToCheck.GetGenericArguments();
      if (genericArgumentsOpenType.Length == 0 || genericArgumentsTypeToCheck.Length == 0 || genericArgumentsOpenType.Length != genericArgumentsTypeToCheck.Length)
      {
        return Result<Type>.Failure();
      }

      var actualGenericType = i_OpenGenericType.MakeGenericType(genericArgumentsTypeToCheck);
      if (actualGenericType.IsAssignableFrom(i_TypeToCheck))
      {
        return Result<Type>.Ok(actualGenericType);
      }
      return Result<Type>.Failure();
    }

    /// <summary>
    /// Checks whether i_PossibleCollectionType can be assigned to IEnumerable of T
    /// </summary>
    /// <param name="i_PossibleCollectionType"></param>
    /// <returns></returns>
    public static bool IsGenericCollectionType(Type i_PossibleCollectionType)
    {
      if (i_PossibleCollectionType.GenericTypeArguments.Length > 0)
      {
        return m_IsGenericCollectionMap.GetOrCreate(i_PossibleCollectionType, () => ConstructGenericEnumerableType(i_PossibleCollectionType).IsAssignableFrom(i_PossibleCollectionType));
      }

      return false;
    }

    /// <summary>
    /// Returns Namespace.Name, AssemblyName
    /// </summary>
    /// <param name="i_Type"></param>
    /// <returns></returns>
    public static string GetMinimalAssemblyQualifiedName(Type i_Type)
    {
      return $"{i_Type.FullName}, {i_Type.Assembly.GetName().Name}";
    }

    /// <summary>
    /// Construct a generic enumerable type from the generic parameter from i_HostType.
    /// If i_HostType does not have any generic parameters a NotSupportedException is thrown.
    /// </summary>
    /// <param name="i_HostType"></param>
    /// <returns></returns>
    public static Type ConstructGenericEnumerableType(Type i_HostType)
    {
      return m_CorrectEnumTypes.GetOrCreate(i_HostType, () => ConstructGenericEnumerableTypeInternal(i_HostType));
    }

    private static Type ConstructGenericEnumerableTypeInternal(Type i_HostType)
    {
      var enumType     = typeof(IEnumerable<>);
      var genericParam = i_HostType.GenericTypeArguments.FirstOrDefault();
      if (genericParam != null)
      {
        return enumType.MakeGenericType(genericParam);
      }

      throw new NotSupportedException($"Cannot create enumerable type for {i_HostType.FullName} because the type does not contain any generic parameters.");
    }

    /// <summary>
    /// Creates an instance of i_CollectionType filled by i_ValuesFunc.
    /// </summary>
    /// <param name="i_CollectionType">The type of Collection to create, can not be an interface or an abstract class. And needs to implement IEnumerable of T and contain a constructor that can accepts an instance of IEnumerable of T</param>
    /// <param name="i_ValuesFunc"></param>
    /// <returns></returns>
    public static object CreateGenericCollectionOf(Type i_CollectionType, Func<IEnumerable<object>> i_ValuesFunc)
    {
      if (i_CollectionType.IsInterface || i_CollectionType.IsAbstract)
      {
        throw new InvalidOperationException($"Cannot create instance of type {i_CollectionType.FullName} because the type is either an interface or abstract.");
      }

      var values = i_ValuesFunc.Invoke();
      if (values?.Any() != true)
      {
        return NcInstanceFactory.CreateInstance(i_CollectionType);
      }

      var enumType = ConstructGenericEnumerableType(i_CollectionType);
      var key = new InstanceCreationFunctionKey()
      {
        ReturnType     = i_CollectionType,
        ParameterTypes = new() {enumType}
      };
      var func = m_CachedCollectionCreationFuncs.GetOrCreate(key, () => CreateCollectionCreationFunc(i_CollectionType, enumType));
      if (!enumType.IsAssignableFrom(values.GetType()))
      {
        var valuesWithCorrectType = CreateCorrectEnumerableFor(values, enumType);
        return func.Invoke(valuesWithCorrectType);
      }

      return func.Invoke(values);
    }

    private static IEnumerable CreateCorrectEnumerableFor(IEnumerable<object> i_Values, Type enumType)
    {
      var listInst = CreateNewEmptyArrayListInstance(enumType.GetGenericArguments()[0]);
      i_Values.ForEachItem(r => listInst.Add(r));
      return listInst;
    }

    private static IList CreateNewEmptyArrayListInstance(Type i_GenericTypeArgument)
    {
      var listType         = m_GenericTypeArgumentToListTypeMap.GetOrCreate(i_GenericTypeArgument, () => typeof(List<>).MakeGenericType(i_GenericTypeArgument));
      var listCreationFunc = m_CachedListCreationFuncs.GetOrCreate(listType, () => CreateEmptyListCreationFunc(listType));
      return listCreationFunc.Invoke();
    }

    private static Func<IList> CreateEmptyListCreationFunc(Type i_ListType)
    {
      var ctor       = i_ListType.GetConstructor(Array.Empty<Type>());
      var ctorExpr   = Expression.New(ctor);
      var lambdaExpr = Expression.Lambda<Func<IList>>(ctorExpr);
      return lambdaExpr.Compile();
    }

    private static Func<IEnumerable, object> CreateCollectionCreationFunc(Type i_CollectionType, Type i_CorrectEnumType)
    {
      var ctor = i_CollectionType.GetConstructor(new[] {i_CorrectEnumType});
      if (ctor == null)
      {
        throw new NotSupportedException($"Cannot create instance of {i_CollectionType.FullName} because the type does not contain a constructor that accepts a argument of type {i_CorrectEnumType.FullName}.");
      }

      var lambdaParamExpr = Expression.Parameter(typeof(IEnumerable));
      var ctorExpr        = Expression.New(ctor, Expression.Convert(lambdaParamExpr, i_CorrectEnumType));
      var lambdaExpr      = Expression.Lambda<Func<IEnumerable, object>>(ctorExpr, lambdaParamExpr);
      return lambdaExpr.Compile();
    }

    /// <summary>
    /// Gets all members of the type that are defined on it and not in any base classes. Makes the most sense when using enums
    /// </summary>
    /// <param name="i_EnumType"></param>
    /// <returns></returns>
    public static MemberInfo[] GetDirectEnumMembers(Type i_EnumType)
    {
      return i_EnumType.GetMembers()
        .Where(r => r.DeclaringType == i_EnumType && r.Name != ENUM_VALUE_FIELD)
        .ToArray();
    }

    /// <summary>
    /// Casts the int value to a value of i_EnumType. Needed since Convert.ChangeType fails if trying to convert int to enum instance
    /// </summary>
    /// <param name="i_EnumType"></param>
    /// <param name="i_EnumIntValue"></param>
    /// <returns></returns>
    public static object CastEnumFromInt(Type i_EnumType, int i_EnumIntValue)
    {
      var castFunc = m_EnumCastFunc.GetOrCreate(i_EnumType, () =>
      {
        var valueExpr   = Expression.Parameter(typeof(int));
        var castExpr    = Expression.Convert(valueExpr, i_EnumType);
        var objCastExpr = Expression.Convert(castExpr, typeof(object));
        var lambda      = Expression.Lambda<Func<int, object>>(objCastExpr, valueExpr);
        return lambda.Compile();
      });
      return castFunc.Invoke(i_EnumIntValue);
    }

    /// <summary>
    /// Check if a type is nullable, also handles Nullable-Of
    /// </summary>
    /// <param name="i_Type"></param>
    /// <returns></returns>
    public static bool IsNullable(Type i_Type)
    {
      if (!i_Type.IsValueType)
      {
        return true;
      }

      if (Nullable.GetUnderlyingType(i_Type) != null)
      {
        return true;
      }

      return false;
    }
  }
}
