using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using NiceCore.Base.Caching;

namespace NiceCore.Reflection
{
  /// <summary>
  /// Reflection helper functions that enable things about list
  /// </summary>
  public static class ReflectionCollectionAccessor
  {
    private static readonly SimpleCache<Type, Func<object, int>> m_GetCountFuncCache = new();

    /// <summary>
    /// Get Count of i_Instance. It is assumed that i_Instance can be cast to ICollection of T
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <returns></returns>
    public static int GetCountFor(object i_Instance)
    {
      if (i_Instance == null)
      {
        throw new ArgumentException("Could not call GetCountFor, i_Instance was passed as null");
      }
      var func = m_GetCountFuncCache.GetOrCreate(i_Instance.GetType(), type => GetCountFunc(type));
      return func.Invoke(i_Instance);
    }

    private static Func<object, int> GetCountFunc(Type i_InstanceType)
    {
      var genericArguments = i_InstanceType.GetGenericArguments();
      var listType = typeof(ICollection<>).MakeGenericType(genericArguments);
      var paramExpr = Expression.Parameter(typeof(object));
      var paramAsListExpr = Expression.Convert(paramExpr, listType);
      var propExpr = Expression.Property(paramAsListExpr, nameof(ICollection<object>.Count));
      var lambdaExpr = Expression.Lambda<Func<object, int>>(propExpr, paramExpr);
      return lambdaExpr.Compile();
    }
  }
}