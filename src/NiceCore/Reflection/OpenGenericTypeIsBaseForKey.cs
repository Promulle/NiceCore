using System;

namespace NiceCore.Reflection
{
  /// <summary>
  /// Cache key for two types
  /// </summary>
  public class OpenGenericTypeIsBaseForKey
  {
    /// <summary>
    /// Open Generic Type
    /// </summary>
    public Type OpenGenericType { get; init; }
    
    /// <summary>
    /// Type to check
    /// </summary>
    public Type TypeToCheck { get; init; }
  }
}
