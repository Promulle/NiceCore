﻿using NiceCore.Base.Caching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using NiceCore.Extensions;

namespace NiceCore.Reflection
{
  /// <summary>
  /// Util that is called by ReflectionUtils
  /// </summary>
  internal static class GetMethodExUtil
  {
    private static readonly SimpleCache<MethodExKey, MethodInfo> m_MethodCache = new();

    /// <summary>
    /// GetMethod like method that handles generic parameters
    /// </summary>
    /// <param name="i_Type"></param>
    /// <param name="i_MethodName"></param>
    /// <param name="i_Types"></param>
    /// <returns></returns>
    public static MethodInfo GetMethodEx(Type i_Type, string i_MethodName, IReadOnlyCollection<MethodParameterType> i_Types)
    {
      return GetMethodEx(i_Type, -1, -1, i_MethodName, i_Types);
    }

    /// <summary>
    /// Get Method ex
    /// </summary>
    /// <param name="i_Type"></param>
    /// <param name="i_GenericArgsCount"></param>
    /// <param name="i_ParameterCount"></param>
    /// <param name="i_MethodName"></param>
    /// <param name="i_Types"></param>
    /// <returns></returns>
    public static MethodInfo GetMethodEx(Type i_Type, int i_GenericArgsCount, int i_ParameterCount, string i_MethodName, IReadOnlyCollection<MethodParameterType> i_Types)
    {
      return GetMethodEx(i_Type, i_GenericArgsCount, i_ParameterCount, i_MethodName, null, i_Types);
    }

    /// <summary>
    /// getMethod like method handles generic parameters and arguments
    /// </summary>
    /// <param name="i_Type"></param>
    /// <param name="i_GenericArgsCount"></param>
    /// <param name="i_ParameterCount"></param>
    /// <param name="i_MethodName"></param>
    /// <param name="i_ExpectedReturnType"></param>
    /// <param name="i_Types"></param>
    /// <returns></returns>
    public static MethodInfo GetMethodEx(Type i_Type, int i_GenericArgsCount, int i_ParameterCount, string i_MethodName, Type i_ExpectedReturnType, IReadOnlyCollection<MethodParameterType> i_Types)
    {
      if (i_Type == null)
      {
        throw new ArgumentException($"i_Type is needed as parameter for {nameof(GetMethodEx)} and cannot be null.");
      }
      if (i_MethodName == null)
      {
        throw new ArgumentException($"i_MethodName is needed as parameter for {nameof(GetMethodEx)} and cannot be null.");
      }

      var typeList = i_Types ?? Array.Empty<MethodParameterType>();
      var key = new MethodExKey()
      {
        MethodName = i_MethodName,
        Type = i_Type,
        TypeArguments = typeList,
        GenericArgumentCount = i_GenericArgsCount
      };
      return m_MethodCache.GetOrCreate(key, () => GetMethodExInternal(i_Type, i_GenericArgsCount, i_ParameterCount, i_MethodName, i_ExpectedReturnType, typeList));
    }

    private static MethodInfo GetMethodExInternal(Type i_Type, int i_GenericArgsCount, int i_ParameterCount, string i_MethodName, Type i_ExpectedReturnType, IReadOnlyCollection<MethodParameterType> i_Types)
    {
      var allMatchingMethods = i_Type.GetMethods(BindingFlags.Static | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
        .Where(r => MatchesGenericArgsCount(r, i_GenericArgsCount) && MatchesParameterCount(r, i_ParameterCount) && r.Name.Equals(i_MethodName) && AllParametersMatch(r, i_Types) && ReturnTypeMatches(r, i_ExpectedReturnType))
        .ToList();

      if (allMatchingMethods.Count > 1)
      {
        throw new AmbiguousMatchException(BuildAmbiguousErrorMessage(allMatchingMethods, i_Type, i_MethodName, i_Types));
      }
      if (allMatchingMethods.Count == 1)
      {
        return allMatchingMethods[0];
      }
      return null;
    }

    private static bool ReturnTypeMatches(MethodInfo i_Method, Type i_ExpectedReturnCount)
    {
      if (i_ExpectedReturnCount == null)
      {
        return true;
      }
      return i_Method.ReturnType == i_ExpectedReturnCount;
    }

    private static bool MatchesGenericArgsCount(MethodInfo i_Method, int i_GenericArgsCount)
    {
      return i_GenericArgsCount == -1 || i_Method.GetGenericArguments().Length == i_GenericArgsCount;
    }

    private static bool MatchesParameterCount(MethodInfo i_Method, int i_ParameterCount)
    {
      return i_ParameterCount == -1 || i_Method.GetParameters().Length == i_ParameterCount;
    }

    private static string BuildAmbiguousErrorMessage(List<MethodInfo> i_FoundMethods, Type i_Type, string i_MethodName, IReadOnlyCollection<MethodParameterType> i_Types)
    {
      var builder = new StringBuilder();
      builder.Append("Found ")
        .Append(i_FoundMethods.Count)
        .Append(" methods for search parameters: (Name: ")
        .Append(i_MethodName)
        .Append(", ParameterTypes: ");
      var typesLastPlace = i_Types.Count - 1;
      for (var i = 0; i < i_Types.Count; i++)
      {
        var type = i_Types.ElementAtEx(i).ActualType;
        builder.Append('[')
          .Append(type != null ? type.FullName : "Generic PlaceHolder")
          .Append(']');
        if (i < typesLastPlace)
        {
          builder.Append(", ");
        }
      }
      builder.Append(") in Type: ")
        .Append(i_Type.FullName)
        .AppendLine(".")
        .AppendLine("Found methods: ");
      for (var i = 0; i < i_Types.Count; i++)
      {
        var method = i_FoundMethods[i];
        AddMethodInfoToStringBuilder(method, builder);
      }
      return builder.ToString();
    }

    private static void AddMethodInfoToStringBuilder(MethodInfo i_MethodInfo, StringBuilder t_Builder)
    {
      t_Builder.Append(i_MethodInfo.Name)
         .Append(": ReturnType=[")
         .Append(i_MethodInfo.ReturnType.FullName)
         .Append("] GenericArguments=[");
      var genericArguments = i_MethodInfo.GetGenericArguments();
      for (var i = 0; i < genericArguments.Length; i++)
      {
        t_Builder.Append(genericArguments[i].FullName);
        if (i < genericArguments.Length - 1)
        {
          t_Builder.Append(", ");
        }
      }
      t_Builder.Append("] Parameters=[");
      var parameters = i_MethodInfo.GetParameters();
      for (var i = 0; i < parameters.Length; i++)
      {
        t_Builder.Append(parameters[i].Name)
          .Append(": ")
          .Append(parameters[i].ParameterType.FullName);
        if (i < parameters.Length - 1)
        {
          t_Builder.Append(", ");
        }
      }
      t_Builder.AppendLine("]");
    }

    private static bool AllParametersMatch(MethodInfo i_Info, IReadOnlyCollection<MethodParameterType> i_Types)
    {
      var methodParams = i_Info.GetParameters();
      if (methodParams.Length == i_Types.Count)
      {
        return CompareTopLevelTypeList(methodParams.Select(r => r.ParameterType).ToList(), i_Types);
      }
      return false;
    }

    private static bool ParameterIsPlaceHolderOrMatches(Type i_ParameterType, MethodParameterType i_TypeDef)
    {
      return TypeIsPlaceHolder(i_ParameterType, i_TypeDef) || (!i_TypeDef.IsGenericPlaceHolder && ParameterMatches(i_ParameterType, i_TypeDef.ActualType, i_TypeDef));
    }

    private static bool ParameterMatches(Type i_ParameterType, Type i_Type, MethodParameterType i_TypeDef)
    {
      var paramType = i_ParameterType.IsByRef ? i_ParameterType.GetElementType() : i_ParameterType;
      var checkType = i_Type.IsByRef ? i_Type.GetElementType() : i_Type;
      if (paramType.Equals(checkType))
      {
        return true;
      }
      if (paramType.GUID == checkType.GUID)
      {
        if (paramType.IsGenericType && checkType.IsGenericType)
        {
          return CheckGenericTypeArguments(paramType, i_TypeDef);
        }
        if (paramType.IsArray && checkType.IsArray)
        {
          return CheckArrayTypes(paramType, checkType, i_TypeDef);
        }
      }
      return false;
    }

    private static bool TypeIsPlaceHolder(Type i_ParameterType, MethodParameterType i_TypeDef)
    {
      return i_ParameterType.IsGenericParameter && i_TypeDef.IsGenericPlaceHolder;
    }

    private static bool CheckArrayTypes(Type i_ParameterArrayType, Type i_ArrayType, MethodParameterType i_TypeDef)
    {
      return ParameterMatches(i_ParameterArrayType.GetElementType(), i_ArrayType.GetElementType(), i_TypeDef.ArrayElementType);
    }

    private static bool CheckGenericTypeArguments(Type i_ParameterType, MethodParameterType i_TypeDef)
    {
      var parameterGenericTypes = i_ParameterType.GenericTypeArguments.ToList();
      var typeGenericTypes = i_TypeDef.ActualTypeGenericArguments;
      if (parameterGenericTypes.Count == typeGenericTypes.Count)
      {
        return CompareTopLevelTypeList(parameterGenericTypes, typeGenericTypes);
      }
      return false;
    }

    private static bool CompareTopLevelTypeList(IReadOnlyCollection<Type> i_ParameterTypes, IReadOnlyCollection<MethodParameterType> i_CheckTypes)
    {
      for (var i = 0; i < i_ParameterTypes.Count; i++)
      {
        if (!ParameterIsPlaceHolderOrMatches(i_ParameterTypes.ElementAtEx(i), i_CheckTypes.ElementAtEx(i)))
        {
          return false;
        }
      }
      return true;
    }
  }
}
