﻿using System;
using System.Collections.Generic;

namespace NiceCore.Reflection
{
  /// <summary>
  /// Definition of a Type for a method parameter
  /// </summary>
  public sealed class MethodParameterType
  {
    /// <summary>
    /// Type of the parameter, null if generic parameter
    /// </summary>
    public Type ActualType { get; init; }

    /// <summary>
    /// All parameters of actual type
    /// </summary>
    public List<MethodParameterType> ActualTypeGenericArguments { get; init; }

    /// <summary>
    /// Array element type
    /// </summary>
    public MethodParameterType ArrayElementType { get; init; }

    /// <summary>
    /// If this parameter type is a generic placeholder like T
    /// </summary>
    public bool IsGenericPlaceHolder { get; init; }

    private MethodParameterType()
    {
    }

    /// <summary>
    /// Creates a definition for an actual type of a parameter
    /// </summary>
    /// <param name="i_ParameterType"></param>
    /// <returns></returns>
    public static MethodParameterType Of(Type i_ParameterType)
    {
      return Of(i_ParameterType, new List<MethodParameterType>());
    }

    /// <summary>
    /// Creates a definition for an actual type of a parameter
    /// </summary>
    /// <param name="i_ParameterType"></param>
    /// <param name="i_ActualTypeGenericArguments"></param>
    /// <returns></returns>
    public static MethodParameterType Of(Type i_ParameterType, List<MethodParameterType> i_ActualTypeGenericArguments)
    {
      return Of(i_ParameterType, i_ActualTypeGenericArguments, null);
    }

    /// <summary>
    /// Creates a definition for an actual type of a parameter
    /// </summary>
    /// <param name="i_ParameterType"></param>
    /// <param name="i_ActualTypeGenericArguments"></param>
    /// <param name="i_ArrayElementType"></param>
    /// <returns></returns>
    public static MethodParameterType Of(Type i_ParameterType, List<MethodParameterType> i_ActualTypeGenericArguments, MethodParameterType i_ArrayElementType)
    {
      return new MethodParameterType()
      {
        ActualType = i_ParameterType,
        IsGenericPlaceHolder = false,
        ActualTypeGenericArguments = i_ActualTypeGenericArguments,
        ArrayElementType = i_ArrayElementType
      };
    }

    /// <summary>
    /// Creates a definition for a generic placeholder
    /// </summary>
    /// <returns></returns>
    public static MethodParameterType GenericPlaceHolder()
    {
      return new MethodParameterType()
      {
        IsGenericPlaceHolder = true
      };
    }

    /// <summary>
    /// Generated equals
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj)
    {
      return obj is MethodParameterType type &&
             EqualityComparer<Type>.Default.Equals(ActualType, type.ActualType) &&
             IsGenericPlaceHolder == type.IsGenericPlaceHolder;
    }

    /// <summary>
    /// GetHashCode
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode()
    {
      return HashCode.Combine(ActualType, IsGenericPlaceHolder);
    }

    /// <summary>
    /// To String
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
      if (IsGenericPlaceHolder)
      {
        return "<>";
      }

      var resultString = ActualType.Name;
      if (ActualTypeGenericArguments?.Count > 0)
      {
        resultString += "<";
        
        for (var i = 0; i < ActualTypeGenericArguments.Count; i++)
        {
          var element = ActualTypeGenericArguments[i];
          resultString += element.ToString();
          if (i < ActualTypeGenericArguments.Count - 1)
          {
            resultString += ", ";
          }
        }

        resultString += ">";
      }

      return resultString;
    }
  }
}
