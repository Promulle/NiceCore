﻿using NiceCore.Base.Caching;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace NiceCore.Reflection
{
  /// <summary>
  /// Key for cache in GetGenericMethodUtil
  /// </summary>
  public class MethodExKey : BaseCacheKey
  {
    /// <summary>
    /// Type that has the method that is searched for
    /// </summary>
    public Type Type { get; init; }

    /// <summary>
    /// Name of the method we are searching for
    /// </summary>
    public string MethodName { get; init; }

    /// <summary>
    /// Type arguments used
    /// </summary>
    public IReadOnlyCollection<MethodParameterType> TypeArguments { get; init; }

    /// <summary>
    /// Number of generic arguments, -1 for none/not searched for
    /// </summary>
    public int GenericArgumentCount { get; init; }

    /// <summary>
    /// Number of parameter of function, -1 for none/not searched for
    /// </summary>
    public int ParameterCount { get; init; }

    /// <summary>
    /// Generated equals
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj)
    {
      return obj is MethodExKey key &&
             EqualityComparer<Type>.Default.Equals(Type, key.Type) &&
             MethodName == key.MethodName &&
             GenericArgumentCount == key.GenericArgumentCount &&
             ParameterCount == key.ParameterCount &&
             ListEquals(TypeArguments, key.TypeArguments);
    }

    /// <summary>
    /// Generated hash code
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode()
    {
      return HashCode.Combine(Type, MethodName, GenericArgumentCount, ParameterCount, GetListHashCode(TypeArguments));
    }
  }
}
