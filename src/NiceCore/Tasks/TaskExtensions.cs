using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace NiceCore.Tasks
{
  /// <summary>
  /// Task Extensions
  /// </summary>
  public static class TaskExtensions
  {
    /// <summary>
    /// Get awaiter
    /// </summary>
    /// <param name="i_Tasks"></param>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    /// <returns></returns>
    public static TaskAwaiter<(T1, T2)> GetAwaiter<T1, T2>(this (Task<T1>, Task<T2>) i_Tasks)
    {
      return CombineTasks(i_Tasks).GetAwaiter();
    }

    private static async Task<(T1, T2)> CombineTasks<T1, T2>((Task<T1>, Task<T2>) i_Tasks)
    {
      var (firstTask, secondTask) = i_Tasks;
      await Task.WhenAll(firstTask, secondTask).ConfigureAwait(false);
      return (firstTask.Result, secondTask.Result);
    }
  }
}