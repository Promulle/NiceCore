using System;
using System.Threading.Tasks;

namespace NiceCore.Concurrency.Exceptions
{
  /// <summary>
  /// OrderedLockManagerWaitCanceledException
  /// </summary>
  public class OrderedLockManagerWaitCanceledException : Exception
  {
    /// <summary>
    /// Ctor
    /// </summary>
    public OrderedLockManagerWaitCanceledException(TaskCanceledException i_Exception): base("Waiting for locks of LockManager was canceled by forceRelease", i_Exception)
    {
      
    }
  }
}