using System;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using NiceCore.Concurrency.Channels.ReadingBehaviors;

namespace NiceCore.Concurrency.Channels
{
  /// <summary>
  /// Concurrent Channel
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <typeparam name="TState"></typeparam>
  public sealed class CallbackChannel<T, TState>
  {
    private readonly Channel<T>                         m_InternalChannel;
    private readonly IChannelReadingBehavior<T, TState> m_Callback;
    private readonly TState                             m_State;
    private readonly Task                               m_ReadTask;

    /// <summary>
    /// Ctor that creates an instance that uses the given channel instance
    /// </summary>
    /// <param name="i_InternalChannel"></param>
    /// <param name="i_ReadingBehavior"></param>
    /// <param name="i_State"></param>
    /// <param name="i_Token"></param>
    public CallbackChannel(Channel<T> i_InternalChannel, IChannelReadingBehavior<T, TState> i_ReadingBehavior, TState i_State, CancellationToken i_Token)
    {
      m_State           = i_State;
      m_Callback        = i_ReadingBehavior ?? throw new ArgumentException($"Could not create instance of type {typeof(CallbackChannel<T, TState>).FullName} because provided callback was null/default.");
      m_InternalChannel = i_InternalChannel ?? throw new ArgumentException($"Could not create instance of type {typeof(CallbackChannel<T, TState>).FullName} because provided channel was null.");
      m_ReadTask        = StartReading(i_Token);
    }

    private Task StartReading(CancellationToken i_Token)
    {
      var callBackState = new CallbackChannelState<T, TState>()
      {
        ReadingBehavior = m_Callback,
        CallbackState   = m_State,
        Channel         = this,
        CancelToken     = i_Token
      };
      return Task.Factory.StartNew(static async state =>
      {
        var actual        = state as CallbackChannelState<T, TState>;
        var channel       = actual!.Channel;
        var callback      = actual.ReadingBehavior;
        var callbackState = actual.CallbackState;
        var cancelToken   = actual.CancelToken;
        await callback.Read(callbackState, channel.m_InternalChannel, cancelToken).ConfigureAwait(false);
      }, callBackState, i_Token, TaskCreationOptions.LongRunning, TaskScheduler.Default);
    }

    /// <summary>
    /// Write Async
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public ValueTask WriteAsync(T i_Value)
    {
      return m_InternalChannel.Writer.WriteAsync(i_Value);
    }

    /// <summary>
    /// Tries to write i_Value 
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public bool TryWrite(T i_Value)
    {
      return m_InternalChannel.Writer.TryWrite(i_Value);
    }

    /// <summary>
    /// Completes the channel. Use with caution if the concurrentChannel did not create the channel instance it is using.
    /// This will throw an exception if the internal channel is already completed.
    /// </summary>
    /// <returns>A Task that represents the read operation this concurrentChannel performed.</returns>
    public async Task Complete()
    {
      m_InternalChannel.Writer.Complete();
      await Task.WhenAll(m_InternalChannel.Reader.Completion, m_ReadTask);
      //await Task.WhenAll(m_InternalChannel.Reader.Completion, m_ReadTask).ConfigureAwait(false);
    }

    /// <summary>
    /// Safer call to complete that does disregard the read task
    /// </summary>
    /// <returns></returns>
    public bool TryComplete()
    {
      return m_InternalChannel.Writer.TryComplete();
    }
  }
}
