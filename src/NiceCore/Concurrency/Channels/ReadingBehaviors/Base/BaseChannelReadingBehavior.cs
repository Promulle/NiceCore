using System;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace NiceCore.Concurrency.Channels.ReadingBehaviors.Base
{
  /// <summary>
  /// Base for reading behaviors
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <typeparam name="TItem"></typeparam>
  /// <typeparam name="TState"></typeparam>
  public abstract class BaseChannelReadingBehavior<T, TItem, TState> : IChannelReadingBehavior<TItem, TState>
  {
    /// <summary>
    /// Callback to be invoked by the inheriting instance
    /// </summary>
    protected readonly Func<T, TState, ValueTask> m_Callback;
    
    /// <summary>
    /// Ctor setting callback
    /// </summary>
    /// <param name="i_Callback"></param>
    protected BaseChannelReadingBehavior(Func<T, TState, ValueTask> i_Callback)
    {
      m_Callback = i_Callback;
    }
    
    /// <summary>
    /// Read. To Be implemented
    /// </summary>
    /// <param name="i_CallbackState"></param>
    /// <param name="i_Channel"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    public abstract ValueTask Read(TState i_CallbackState, Channel<TItem> i_Channel, CancellationToken i_Token);
  }
}