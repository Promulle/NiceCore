using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NiceCore.Concurrency.Channels.ReadingBehaviors.Base
{
  /// <summary>
  /// Base Reading Behavior for batched callbacks
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <typeparam name="TState"></typeparam>
  public abstract class BaseBatchingChannelReadingBehavior<T, TState> : BaseChannelReadingBehavior<IReadOnlyCollection<T>, T, TState>
  {
    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_Callback"></param>
    protected BaseBatchingChannelReadingBehavior(Func<IReadOnlyCollection<T>, TState, ValueTask> i_Callback) : base(i_Callback)
    {
    }
    
    /// <summary>
    /// invokes the callback for the current buffer and clears the buffer afterwards.
    /// If the invocation throws an exception, the buffer is cleared anyway
    /// </summary>
    /// <param name="t_Buffer"></param>
    /// <param name="i_CallbackState"></param>
    protected async ValueTask InvokeCallbackForBuffer(List<T> t_Buffer, TState i_CallbackState)
    {
      try
      {
        await m_Callback.Invoke(t_Buffer, i_CallbackState).ConfigureAwait(false);
      }
      catch (Exception )
      {
        //I do not know what to do here actually
      }
      finally
      {
        t_Buffer.Clear();
      }
    }
  }
}