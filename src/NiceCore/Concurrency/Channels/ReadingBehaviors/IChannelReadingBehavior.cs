using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace NiceCore.Concurrency.Channels.ReadingBehaviors
{
  /// <summary>
  /// Interface for reading Behaviors used by callbackChannels
  /// </summary>
  /// <typeparam name="TItem"></typeparam>
  /// <typeparam name="TState"></typeparam>
  public interface IChannelReadingBehavior<TItem, TState>
  {
    /// <summary>
    /// Reads data from Channel and should invoke the callback with the given callback state
    /// </summary>
    /// <param name="i_CallbackState"></param>
    /// <param name="i_Channel"></param>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    ValueTask Read(TState i_CallbackState, Channel<TItem> i_Channel, CancellationToken i_Token);
  }
}