using System;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using NiceCore.Concurrency.Channels.ReadingBehaviors.Base;

namespace NiceCore.Concurrency.Channels.ReadingBehaviors.Impl
{
  /// <summary>
  /// Simple Implementation of a reading behavior
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <typeparam name="TState"></typeparam>
  public sealed class SimpleReadingBehavior<T, TState> : BaseChannelReadingBehavior<T, T, TState>
  {
    /// <summary>
    /// Ctor setting callback
    /// </summary>
    /// <param name="i_Callback"></param>
    public SimpleReadingBehavior(Func<T, TState, ValueTask> i_Callback) : base(i_Callback)
    {
    }
    
    /// <summary>
    /// Reads and calls function
    /// </summary>
    /// <param name="i_CallbackState"></param>
    /// <param name="i_Channel"></param>
    /// <param name="i_Token"></param>
    public override async ValueTask Read(TState i_CallbackState, Channel<T> i_Channel, CancellationToken i_Token)
    {
      try
      {
        while (await i_Channel.Reader.WaitToReadAsync(i_Token))
        {
          await foreach (var val in i_Channel.Reader.ReadAllAsync(i_Token).ConfigureAwait(false))
          {
            try
            {
              await m_Callback.Invoke(val, i_CallbackState).ConfigureAwait(false);
            }
            catch (Exception e)
            {
              Console.ForegroundColor = ConsoleColor.Red;
              Console.WriteLine("Exception in reading " + e);
              Console.ResetColor();
            }
          }
        }
      }
      catch (Exception e)
      {
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.WriteLine(e);
        Console.ResetColor();
      }
    }
  }
}