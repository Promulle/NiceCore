using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using NiceCore.Concurrency.Channels.ReadingBehaviors.Base;

namespace NiceCore.Concurrency.Channels.ReadingBehaviors.Impl
{
  /// <summary>
  /// Batching behavior that uses cancelling as batch controlling
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <typeparam name="TState"></typeparam>
  public class CancellingBatchingChannelReadingBehavior<T, TState> : BaseBatchingChannelReadingBehavior<T, TState>
  {
    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_Callback"></param>
    public CancellingBatchingChannelReadingBehavior(Func<IReadOnlyCollection<T>, TState, ValueTask> i_Callback) : base(i_Callback)
    {
    }

    /// <summary>
    /// Read
    /// </summary>
    /// <param name="i_CallbackState"></param>
    /// <param name="i_Channel"></param>
    /// <param name="i_Token"></param>
    public override async ValueTask Read(TState i_CallbackState, Channel<T> i_Channel, CancellationToken i_Token)
    {
      var buffer = new List<T>();
      while (await i_Channel.Reader.WaitToReadAsync(i_Token))
      {
        var completionTokenSource = new CancellationTokenSource();

        try
        {
          await foreach (var val in i_Channel.Reader.ReadAllAsync(completionTokenSource.Token).ConfigureAwait(false))
          {
            buffer.Add(val);
            if (!completionTokenSource.IsCancellationRequested)
            {
              completionTokenSource.Cancel();
            }
          }
        }
        catch (TaskCanceledException )
        {
          
        }

        await InvokeCallbackForBuffer(buffer, i_CallbackState).ConfigureAwait(false);
      }
    }
  }
}