using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using NiceCore.Concurrency.Channels.ReadingBehaviors.Base;

namespace NiceCore.Concurrency.Channels.ReadingBehaviors.Impl
{
  /// <summary>
  /// Batching Behavior that guarantees a maxBatchSize
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <typeparam name="TState"></typeparam>
  public sealed class MaxBatchSizeBatchingChannelReadingBehavior<T, TState>: BaseBatchingChannelReadingBehavior<T, TState>
  {
    private readonly int m_MaxBatchSize;
    
    /// <summary>
    /// Ctor setting callback and max batch size
    /// </summary>
    /// <param name="i_Callback"></param>
    /// <param name="i_MaxBatchSize"></param>
    public MaxBatchSizeBatchingChannelReadingBehavior(Func<IReadOnlyCollection<T>, TState, ValueTask> i_Callback, int i_MaxBatchSize) : base(i_Callback)
    {
      m_MaxBatchSize = i_MaxBatchSize;
    }

    /// <summary>
    /// Read
    /// </summary>
    /// <param name="i_CallbackState"></param>
    /// <param name="i_Channel"></param>
    /// <param name="i_Token"></param>
    public override async ValueTask Read(TState i_CallbackState, Channel<T> i_Channel, CancellationToken i_Token)
    {
      var buffer = new List<T>();
      while (await i_Channel.Reader.WaitToReadAsync(i_Token))
      {
        while (i_Channel.Reader.TryRead(out var item))
        {
          buffer.Add(item);
          if (buffer.Count == m_MaxBatchSize)
          {
            await InvokeCallbackForBuffer(buffer, i_CallbackState).ConfigureAwait(false);
          }
        }

        if (buffer.Count > 0)
        {
          await InvokeCallbackForBuffer(buffer, i_CallbackState).ConfigureAwait(false);
        }
      }
    }
  }
}