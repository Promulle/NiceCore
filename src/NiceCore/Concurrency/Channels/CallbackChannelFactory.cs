using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using NiceCore.Concurrency.Channels.ReadingBehaviors.Impl;

namespace NiceCore.Concurrency.Channels
{
  /// <summary>
  /// Factory for easy creation of callback channels
  /// </summary>
  public static class CallbackChannelFactory
  {
    /// <summary>
    /// Creates a simple Callback Channel
    /// </summary>
    /// <param name="i_Callback"></param>
    /// <param name="i_State"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TState"></typeparam>
    /// <returns></returns>
    public static CallbackChannel<T, TState> CreateSimple<T, TState>(Func<T, TState, ValueTask> i_Callback, TState i_State)
    {
      return CreateSimple(i_Callback, i_State, CancellationToken.None);
    }

    /// <summary>
    /// Creates a simple Callback Channel
    /// </summary>
    /// <param name="i_Callback"></param>
    /// <param name="i_State"></param>
    /// <param name="i_Token"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TState"></typeparam>
    /// <returns></returns>
    public static CallbackChannel<T, TState> CreateSimple<T, TState>(Func<T, TState, ValueTask> i_Callback, TState i_State, CancellationToken i_Token)
    {
      return new(CreateDefaultChannel<T>(), new SimpleReadingBehavior<T, TState>(i_Callback), i_State, i_Token);
    }

    /// <summary>
    /// Creates a callback channel that guarantees batches are run with at most i_MaxBatchSize entries
    /// </summary>
    /// <param name="i_Callback"></param>
    /// <param name="i_State"></param>
    /// <param name="i_MaxBatchSize"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TState"></typeparam>
    /// <returns></returns>
    public static CallbackChannel<T, TState> CreateBatched<T, TState>(Func<IReadOnlyCollection<T>, TState, ValueTask> i_Callback, TState i_State, int i_MaxBatchSize)
    {
      return CreateBatched(i_Callback, i_State, i_MaxBatchSize, CancellationToken.None);
    }

    /// <summary>
    /// Creates a callback channel that guarantees batches are run with at most i_MaxBatchSize entries
    /// </summary>
    /// <param name="i_Callback"></param>
    /// <param name="i_State"></param>
    /// <param name="i_Token"></param>
    /// <param name="i_MaxBatchSize"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TState"></typeparam>
    /// <returns></returns>
    public static CallbackChannel<T, TState> CreateBatched<T, TState>(Func<IReadOnlyCollection<T>, TState, ValueTask> i_Callback, TState i_State,  int i_MaxBatchSize, CancellationToken i_Token)
    {
      return new(CreateDefaultChannel<T>(), new MaxBatchSizeBatchingChannelReadingBehavior<T, TState>(i_Callback, i_MaxBatchSize), i_State, i_Token);
    }

    private static Channel<T> CreateDefaultChannel<T>()
    {
      return Channel.CreateUnbounded<T>(new()
      {
        SingleReader = true,
        SingleWriter = false
      });
    }
  }
}