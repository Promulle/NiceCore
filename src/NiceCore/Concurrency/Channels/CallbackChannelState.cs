using System;
using System.Threading;
using System.Threading.Tasks;
using NiceCore.Concurrency.Channels.ReadingBehaviors;

namespace NiceCore.Concurrency.Channels
{
  /// <summary>
  /// State used for the lambda in concurrentChannel
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <typeparam name="TState"></typeparam>
  public sealed class CallbackChannelState<T, TState>
  {
    /// <summary>
    /// The cannel to read on
    /// </summary>
    public required CallbackChannel<T, TState> Channel { get; init; }
    
    /// <summary>
    /// The Callback to execute for a value
    /// </summary>
    public required IChannelReadingBehavior<T, TState> ReadingBehavior { get; init; }
    
    /// <summary>
    /// The State that is to be passed to a handle function
    /// </summary>
    public required TState CallbackState { get; init; }
    
    /// <summary>
    /// The Cancel Token
    /// </summary>
    public required CancellationToken CancelToken { get; init; }
  } 
}