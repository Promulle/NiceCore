using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace NiceCore.Concurrency
{
  /// <summary>
  /// Lock that is fifo compliant.
  /// </summary>
  public sealed class OrderedLock
  {
    private readonly SemaphoreSlim m_Semaphore = new (1, 1);
    private bool IsExecuting = false;
    private readonly Queue<TaskCompletionSource> m_NextSources = new();

    /// <summary>
    /// Gets the count of currently waiting tasks
    /// </summary>
    /// <returns></returns>
    public int GetCurrentlyWaitingCount()
    {
      return m_NextSources.Count;
    }
    
    /// <summary>
    /// Wait
    /// </summary>
    public Task Wait()
    {
      m_Semaphore.Wait();
      try
      {
        if (!IsExecuting)
        {
          IsExecuting = true;
          return Task.CompletedTask;
        }

        var newSource = new TaskCompletionSource(TaskCreationOptions.RunContinuationsAsynchronously);
        m_NextSources.Enqueue(newSource);
        return newSource.Task;
      }
      finally
      {
        m_Semaphore.Release();
      }
    }

    /// <summary>
    /// Force Release
    /// </summary>
    public void ForceRelease()
    {
      try
      {
        m_Semaphore.Release();  
      }
      catch
      {}
      
      while (m_NextSources.TryDequeue(out var src))
      {
        try
        {
          src.SetCanceled();
        }
        catch
        {
        }
      }

      IsExecuting = false;
    }

    /// <summary>
    /// Release
    /// </summary>
    public int Release()
    {
      m_Semaphore.Wait();
      try
      {
        if (m_NextSources.Count == 0)
        {
          IsExecuting = false;
        }
        
        if (m_NextSources.TryDequeue(out var nextSrc))
        {
          nextSrc.SetResult();
        }
        
        return m_NextSources.Count;
      }
      finally
      {
        m_Semaphore.Release();
      }
    }
  }
}