using System.Threading.Tasks;

namespace NiceCore.Concurrency
{
  /// <summary>
  /// OrderedLock with the key it is used for
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  public struct OrderedLockByKey<TKey>
  {
    /// <summary>
    /// Key
    /// </summary>
    public TKey Key { get; }
    
    /// <summary>
    /// Lock
    /// </summary>
    public OrderedLock Lock { get; }

    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_Key"></param>
    /// <param name="i_Lock"></param>
    public OrderedLockByKey(TKey i_Key, OrderedLock i_Lock)
    {
      Key = i_Key;
      Lock = i_Lock;
    }

    /// <summary>
    /// Wait with the lock
    /// </summary>
    /// <returns></returns>
    public Task Wait()
    {
      return Lock.Wait();
    }

    /// <summary>
    /// Release
    /// </summary>
    /// <returns></returns>
    public bool Release()
    {
      var remaining = Lock.Release();
      return remaining == 0;
    }
  }
}