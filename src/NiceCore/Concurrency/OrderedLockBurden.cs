using System.Collections.Generic;

namespace NiceCore.Concurrency
{
  /// <summary>
  /// Ordered Lock Burden that is used to release all locks from a given key context
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  public readonly struct OrderedLockBurden<TKey>
  {
    private readonly OrderedLockByKey<TKey>[] m_Locks;
    private readonly OrderedLockManager<TKey> m_LockManager;

    /// <summary>
    /// Create a new Burden for the given locks in the context of the lock Manager
    /// </summary>
    /// <param name="i_OrderedLocks"></param>
    /// <param name="i_Manager"></param>
    public OrderedLockBurden(OrderedLockByKey<TKey>[] i_OrderedLocks, OrderedLockManager<TKey> i_Manager)
    {
      m_Locks       = i_OrderedLocks;
      m_LockManager = i_Manager;
    }

    /// <summary>
    /// Release internal locks
    /// </summary>
    public void Release()
    {
      lock (m_LockManager.LockObject)
      {
        var keysToFullRelease = new List<TKey>();
        foreach (var internalLock in m_Locks)
        {
          if (internalLock.Release())
          {
            keysToFullRelease.Add(internalLock.Key);
          }
        }

        if (keysToFullRelease.Count > 0)
        {
          m_LockManager.RemoveByKeys(keysToFullRelease);
        }
      }
    }
  }
}
