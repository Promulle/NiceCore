namespace NiceCore.Concurrency
{
  /// <summary>
  /// Lock State
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  public class LockState<TKey>
  {
    /// <summary>
    /// Waiting
    /// </summary>
    public int  Waiting { get; init; }
    
    /// <summary>
    /// Key
    /// </summary>
    public TKey Key     { get; init; }
  }
}
