using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Concurrency.Exceptions;

namespace NiceCore.Concurrency
{
  /// <summary>
  /// Class that enables keyed locking for orderedLocks
  /// </summary>
  public sealed class OrderedLockManager<TKey>
  {
    private readonly ConcurrentDictionary<TKey, OrderedLock> m_OrderedLocks = new();

    /// <summary>
    /// BE REALLY CAREFUL WHEN USING THIS LOCK OBJECT THIS IS INTERNAL FOR A REASON
    /// </summary>
    internal object LockObject { get; } = new();

    /// <summary>
    /// Get Current Lock State
    /// </summary>
    /// <returns></returns>
    public IReadOnlyCollection<LockState<TKey>> GetCurrentLockState()
    {
      if (m_OrderedLocks.Count == 0)
      {
        return Array.Empty<LockState<TKey>>();
      }

      var res = new List<LockState<TKey>>();
      foreach (var (key, lockInstance) in m_OrderedLocks)
      {
        res.Add(new LockState<TKey>()
        {
          Key     = key,
          Waiting = lockInstance.GetCurrentlyWaitingCount()
        });
      }

      return res;
    }

    /// <summary>
    /// Force Release all locks
    /// </summary>
    public int ForceReleaseAllLocks()
    {
      var countReleased = 0;
      foreach (var (_, lockInstance) in m_OrderedLocks)
      {
        try
        {
          lockInstance.ForceRelease();
          countReleased += 1;
        }
        catch
        {
        }
      }

      return countReleased;
    }

    /// <summary>
    /// Locks for the given keys
    /// </summary>
    /// <param name="i_Keys"></param>
    /// <returns></returns>
    public async Task<OrderedLockBurden<TKey>> LockMultiple(ISet<TKey> i_Keys)
    {
      if (i_Keys == null || i_Keys.Count == 0)
      {
        throw new ArgumentException("Could not lock for multiple keys. Given KeyList were empty or null.");
      }

      var (tasks, locks) = InitiateWaitOnLocks(i_Keys);
      try
      {
        await Task.WhenAll(tasks).ConfigureAwait(false);
      }
      catch (TaskCanceledException e)
      {
        throw new OrderedLockManagerWaitCanceledException(e);
      }
      
      return new OrderedLockBurden<TKey>(locks, this);
    }

    private (Task[] WaitTasks, OrderedLockByKey<TKey>[] Locks) InitiateWaitOnLocks(ISet<TKey> i_Keys)
    {
      lock (LockObject)
      {
        var locks = GetLocks(i_Keys);

        var tasks = new Task[locks.Length];
        var j     = 0;
        foreach (var actualLock in locks)
        {
          tasks[j] = actualLock.Wait();
          j++;
        }

        return (tasks, locks);
      }
    }

    private OrderedLockByKey<TKey>[] GetLocks(ISet<TKey> i_Keys)
    {
      var locks = new OrderedLockByKey<TKey>[i_Keys.Count];
      var i     = 0;
      foreach (var key in i_Keys)
      {
        var actualLock = m_OrderedLocks.GetOrAdd(key, static (_) => new());
        locks[i] = new OrderedLockByKey<TKey>(key, actualLock);
        i++;
      }

      return locks;
    }

    /// <summary>
    /// Locks for the given keys
    /// </summary>
    /// <param name="i_Keys"></param>
    /// <returns></returns>
    public Task<OrderedLockBurden<TKey>> LockItems(params TKey[] i_Keys)
    {
      return LockMultiple(i_Keys.ToHashSet());
    }

    internal bool HasLockForKey(TKey i_Key)
    {
      return m_OrderedLocks.ContainsKey(i_Key);
    }

    internal void RemoveByKeys(IReadOnlyCollection<TKey> i_Keys)
    {
      lock (LockObject)
      {
        foreach (var key in i_Keys)
        {
          m_OrderedLocks.Remove(key, out var _);
        }
      }
    }
  }
}
