﻿using System.Collections.Generic;

namespace NiceCore.Entities
{
  /// <summary>
  /// Base entity for storage
  /// </summary>
  public abstract class DomainEntity<TId> : IDomainEntity<TId>
  {
    /// <summary>
    /// ID of entity
    /// </summary>
    public virtual TId ID { get; set; }

    /// <summary>
    /// Equals
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj)
    {
      if (obj is DomainEntity<TId> ent)
      {
        if (IDsAreDefault(ent, this))
        {
          return ReferenceEquals(this, ent);
        }
        return EqualityComparer<TId>.Default.Equals(ent.ID, ID);
      }
      return false;
    }

    private static bool IDsAreDefault(DomainEntity<TId> original, DomainEntity<TId> compare)
    {
      var comparer = EqualityComparer<TId>.Default;
      
      return comparer.Equals(original.ID, default(TId)) && comparer.Equals(compare.ID, default(TId));
    }

    /// <summary>
    /// Equals
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode()
    {
      return (GetType().GetHashCode() * 31) ^ ID.GetHashCode();
    }
  }
}
