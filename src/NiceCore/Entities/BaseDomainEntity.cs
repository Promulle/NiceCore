﻿namespace NiceCore.Entities
{
  /// <summary>
  /// DomainEntity with long ID
  /// </summary>
  public class BaseDomainEntity : DomainEntity<long>, IBaseDomainEntity
  {
  }
}
