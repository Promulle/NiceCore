﻿namespace NiceCore.Entities
{
  /// <summary>
  /// Interface for DefaultDomainEntity
  /// </summary>
  public interface IBaseDomainEntity : IDomainEntity<long>
  {
  }
}
