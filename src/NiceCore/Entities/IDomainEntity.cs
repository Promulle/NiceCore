﻿namespace NiceCore.Entities
{
  /// <summary>
  /// Interface for DomainEntity
  /// </summary>
  /// <typeparam name="TId"></typeparam>
  public interface IDomainEntity<TId>
  {
    /// <summary>
    /// Id of Entity
    /// </summary>
    TId ID { get; }
  }
}