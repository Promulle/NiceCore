using System;
using System.Collections.Generic;
using System.Linq;

namespace NiceCore.Collections
{
  /// <summary>
  /// Value Based Recursion Level
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public sealed class ValueBasedRecursionLevel<T>
  {
    /// <summary>
    /// Parent of the current level. If this is null, this is the top level
    /// </summary>
    public ValueBasedRecursionLevel<T> Parent { get; init; }
    
    /// <summary>
    /// Value of the level
    /// </summary>
    public T Value { get; init; }
    
    /// <summary>
    /// Checks whether this node is part of a cycle of node-values
    /// </summary>
    /// <returns></returns>
    public bool ContainsCycleForValue(T i_Value)
    {
      var valuesToRoot = GetValuesUpward();//go up tree til root -> get values stored
      //add value to also check
      valuesToRoot.Add(i_Value);
      //this should return true if valuesToRoot contains duplicates which should signal a loop if the provided data tends to loop
      return valuesToRoot.Count != valuesToRoot.Distinct().Count();
    }
    
    private List<T> GetValuesUpward()
    {
      var res = new List<T>
      {
        Value
      };
      if (Parent != null)
      {
        res.AddRange(Parent.GetValuesUpward());
      }
      return res;
    }
  }
}