using System.Collections.Generic;

namespace NiceCore.Collections
{
  /// <summary>
  /// Nc Collections
  /// </summary>
  public static class NcCollections
  {
    /// <summary>
    /// Empty Dictionary
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <returns></returns>
    public static IReadOnlyDictionary<TKey, TValue> EmptyDictionary<TKey, TValue>()
    {
      return NcDefaultDictionaries<TKey, TValue>.Empty;
    }
  }
}