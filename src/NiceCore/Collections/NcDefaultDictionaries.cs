using System.Collections.Generic;
using System.Threading.Tasks;

namespace NiceCore.Collections
{
  /// <summary>
  /// Default Dictionaries
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  /// <typeparam name="TValue"></typeparam>
  public static class NcDefaultDictionaries<TKey, TValue>
  {
    /// <summary>
    /// Empty
    /// </summary>
    public static readonly IReadOnlyDictionary<TKey, TValue> Empty = new Dictionary<TKey, TValue>();
  }
}