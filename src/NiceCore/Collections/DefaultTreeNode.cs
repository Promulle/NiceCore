﻿using System.Collections.Generic;
using System.Linq;

namespace NiceCore.Collections
{
  /// <summary>
  /// Default implementation for class representing tree Node
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public class DefaultTreeNode<T>
  {
    /// <summary>
    /// Value of Node
    /// </summary>
    public T Value { get; set; }
    /// <summary>
    /// Whether this Node is a leaf or not
    /// </summary>
    public bool IsLeaf
    {
      get { return Nodes.Count == 0; }
    }
    /// <summary>
    /// Whether this Node is a root node
    /// </summary>
    public bool IsRoot
    {
      get { return ParentNode == null; }
    }
    /// <summary>
    /// SubNodes of this
    /// </summary>
    public List<DefaultTreeNode<T>> Nodes { get; set; }
    /// <summary>
    /// Parent of this Node
    /// </summary>
    public DefaultTreeNode<T> ParentNode { get; set; }
    /// <summary>
    /// Default constructor
    /// </summary>
    public DefaultTreeNode()
    {
      Nodes = new List<DefaultTreeNode<T>>();
    }
    /// <summary>
    /// Equals
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj)
    {
      if (obj is T)
      {
        return EqualityComparer<T>.Default.Equals(Value, (obj as DefaultTreeNode<T>).Value);
      }
      return false;
    }
    /// <summary>
    /// Returns hascode
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode()
    {
      var nHash = GetType().GetHashCode();
      return nHash ^ Value.GetHashCode() ^ 13;
    }
    /// <summary>
    /// Checks whether this node is part of a cycle of node-values
    /// </summary>
    /// <returns></returns>
    public bool ContainsCycle()
    {
      var valuesToRoot = GetValuesUpward(); //go up tree til root -> get values stored
      foreach (var group in valuesToRoot.GroupBy(r => r)) //group by values themself -> if a list of a single grouping is larger than 1 -> cycle
      {
        if (group.ToList().Count > 1)
        {
          return true;
        }
      }
      return false;
    }
    private List<T> GetValuesUpward()
    {
      var res = new List<T>
      {
        Value
      };
      if (!IsRoot)
      {
        res.AddRange(ParentNode.GetValuesUpward());
      }
      return res;
    }
  }
}
