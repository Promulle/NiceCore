// Copyright 2021 Sycorax Systemhaus GmbH

using System.Collections.Generic;

namespace NiceCore.Collections
{
  /// <summary>
  /// Nc Default Sets
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public static class NcDefaultSets<T>
  {
    /// <summary>
    /// Empty
    /// </summary>
    public static IReadOnlySet<T> Empty { get; } = new HashSet<T>();
  }
}
