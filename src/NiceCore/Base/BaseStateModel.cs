﻿using System;

namespace NiceCore.Base
{
  /// <summary>
  /// Base for IStateModel
  /// </summary>
  public class BaseStateModel<T> : EventRaiser, IStateModel<T>
  {
    /// <summary>
    /// Instance
    /// </summary>
    public T Instance { get; set; }
    /// <summary>
    /// StateChanged
    /// </summary>
    public event EventHandler StateChanged;
    /// <summary>
    /// Raises StateChanged
    /// </summary>
    public void RaiseStateChanged()
    {
      Raise(StateChanged);
    }
  }
}
