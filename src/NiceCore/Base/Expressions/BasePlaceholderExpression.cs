﻿using System;
using System.Linq.Expressions;

namespace NiceCore.Base.Expressions
{
  /// <summary>
  /// Expression whose value can be freely set before the syntax tree is compiled
  /// </summary>
  public abstract class BasePlaceholderExpression<TExpression> : Expression where TExpression : Expression
  {
    private readonly Type m_Type;

    /// <summary>
    /// NodeType = Extension
    /// </summary>
    public override ExpressionType NodeType
    {
      get { return ExpressionType.Extension; }
    }

    /// <summary>
    /// Type specified by TValue
    /// </summary>
    public override Type Type
    {
      get { return m_Type; }
    }

    /// <summary>
    /// The actual expression to use when compiling
    /// </summary>
    public TExpression ActualExpression { get; set; }

    /// <summary>
    /// Reduce that returns the actual expression
    /// </summary>
    /// <returns></returns>
    public override Expression Reduce()
    {
      if (ActualExpression == null)
      {
        throw new InvalidOperationException($"Can not compile a placeholder expression that has no actual expression. ExpressionType expected: {typeof(TExpression)}");
      }
      return ActualExpression;
    }

    /// <summary>
    /// Protected ctor because this is abstract
    /// </summary>
    protected BasePlaceholderExpression(Type i_Type)
    {
      m_Type = i_Type;
    }
  }
}
