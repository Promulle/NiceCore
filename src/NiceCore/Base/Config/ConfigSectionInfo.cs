﻿using System;
using System.Configuration;
using System.Reflection;

namespace NiceCore.Base.Config
{
  /// <summary>
  /// Definiton of a Section
  /// </summary>
  public class ConfigSectionInfo
  {
    /// <summary>
    /// Name of config section
    /// </summary>
    public string SectionName { get; set; }
    /// <summary>
    /// Type of section
    /// </summary>
    public Type SectionType { get; set; }
    /// <summary>
    /// Assembly, from whiches Config this has been loaded
    /// </summary>
    public Assembly ConfigAssembly { get; set; }
    /// <summary>
    /// Instance of Section
    /// </summary>
    public object SectionInstance { get; set; }
    /// <summary>
    /// Configuration this Section is loaded from
    /// </summary>
    public Configuration HostConfiguration { get; set; }
  }
}
