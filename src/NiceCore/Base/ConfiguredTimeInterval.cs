using System;

namespace NiceCore.Base
{
  /// <summary>
  /// Configured Time Interval
  /// </summary>
  public class ConfiguredTimeInterval
  {
    /// <summary>
    /// Time Kind
    /// </summary>
    public TimeKind TimeKind { get; set; }
    
    /// <summary>
    /// Time Value
    /// </summary>
    public double TimeValue { get; set; }

    /// <summary>
    /// To Timespan
    /// </summary>
    /// <returns></returns>
    public TimeSpan ToTimeSpan()
    {
      return TimeKind.ToTimeSpan(TimeValue);
    }
  }
}