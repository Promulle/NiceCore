﻿namespace NiceCore.Base
{
  /// <summary>
  /// Enum for kind of time operations (Millisecond, Second, Minute, Hour, Day)
  /// </summary>
  public enum TimeKind
  {
    /// <summary>
    /// In ms
    /// </summary>
    Milliseconds = 0,

    /// <summary>
    /// Seconds
    /// </summary>
    Seconds = 1,

    /// <summary>
    /// Minutes
    /// </summary>
    Minutes = 2,

    /// <summary>
    /// Hours
    /// </summary>
    Hours = 3,

    /// <summary>
    /// Days
    /// </summary>
    Days = 4,
    
    /// <summary>
    /// Weeks (Days * 7)
    /// </summary>
    Weeks = 5,
    
    /// <summary>
    /// Months (Days * i_MonthLength)
    /// </summary>
    Months = 6
  }
}
