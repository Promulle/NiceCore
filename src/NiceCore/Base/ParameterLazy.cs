// Copyright 2021 Sycorax Systemhaus GmbH

using System;

namespace NiceCore.Base
{
  /// <summary>
  /// Lazy Load implementation that can take parameters for initialization
  /// </summary>
  public class ParameterLazy<T, TParameter>
  {
    private readonly object m_Lock = new();
    private bool m_IsInitialized = false;
    private T m_Value;
    private readonly Func<TParameter, T> m_FactoryFunc;

    /// <summary>
    /// Ctor setting ctor func
    /// </summary>
    /// <param name="i_FactoryFunc"></param>
    public ParameterLazy(Func<TParameter, T> i_FactoryFunc)
    {
      m_FactoryFunc = i_FactoryFunc;
    }

    /// <summary>
    /// Get
    /// </summary>
    /// <param name="i_Parameter"></param>
    /// <returns></returns>
    public T Get(TParameter i_Parameter)
    {
      lock (m_Lock)
      {
        if (!m_IsInitialized)
        {
          m_Value = m_FactoryFunc.Invoke(i_Parameter);
          m_IsInitialized = true;
        }
      }

      return m_Value;
    }
  }
}
