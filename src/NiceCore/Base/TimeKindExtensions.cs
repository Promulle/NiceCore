﻿using System;

namespace NiceCore.Base
{
  /// <summary>
  /// Extension Methods for time kind
  /// </summary>
  public static class TimeKindExtensions
  {
    /// <summary>
    /// Adds correct amount of time to i_From, using i_Value as value to add
    /// </summary>
    /// <param name="i_Kind"></param>
    /// <param name="i_Value"></param>
    /// <param name="i_From"></param>
    /// <param name="i_MonthLength">Length of month in days. If you need a more precise calculation this method is not suitable</param>
    /// <returns></returns>
    public static DateTime GetTimeIn(this TimeKind i_Kind, double i_Value, DateTime i_From, uint i_MonthLength = 30)
    {
      return i_Kind switch
      {
        TimeKind.Milliseconds => i_From.AddMilliseconds(i_Value),
        TimeKind.Seconds => i_From.AddSeconds(i_Value),
        TimeKind.Minutes => i_From.AddMinutes(i_Value),
        TimeKind.Hours => i_From.AddHours(i_Value),
        TimeKind.Days => i_From.AddDays(i_Value),
        TimeKind.Months => i_From.AddDays(i_MonthLength * i_Value),
        TimeKind.Weeks => i_From.AddDays(7 * i_Value),
        _ => i_From.AddMinutes(i_Value)
      };
    }
    
    /// <summary>
    /// Adds correct amount of time to now, using i_Value as value to add
    /// </summary>
    /// <param name="i_Kind"></param>
    /// <param name="i_Value"></param>
    /// <param name="i_MonthLength">Length of month in days. If you need a more precise calculation this method is not suitable</param>
    /// <returns></returns>
    public static DateTime GetTimeIn(this TimeKind i_Kind, double i_Value, uint i_MonthLength = 30)
    {
      return GetTimeIn(i_Kind, i_Value, DateTime.Now, i_MonthLength);
    }

    /// <summary>
    /// Converts i_Value to the timespan that corresponds to its value in this timekind
    /// </summary>
    /// <param name="i_Kind"></param>
    /// <param name="i_Value"></param>
    /// <param name="i_MonthLength">Length of month in days. If you need a more precise calculation this method is not suitable</param>
    /// <returns></returns>
    public static TimeSpan ToTimeSpan(this TimeKind i_Kind, double i_Value, uint i_MonthLength = 30)
    {
      return i_Kind switch
      {
        TimeKind.Months => TimeSpan.FromDays(i_MonthLength * i_Value),
        TimeKind.Weeks => TimeSpan.FromDays(7 * i_Value),
        TimeKind.Days => TimeSpan.FromDays(i_Value),
        TimeKind.Hours => TimeSpan.FromHours(i_Value),
        TimeKind.Minutes => TimeSpan.FromMinutes(i_Value),
        TimeKind.Seconds => TimeSpan.FromSeconds(i_Value),
        TimeKind.Milliseconds => TimeSpan.FromMilliseconds(i_Value),
        _ => TimeSpan.FromMinutes(i_Value)
      };
    }
  }
}
