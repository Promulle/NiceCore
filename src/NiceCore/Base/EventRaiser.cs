﻿using System;

namespace NiceCore.Base
{
  /// <summary>
  /// Object, implementing raise functions for defautl eventhandlers
  /// </summary>
  public abstract class EventRaiser
  {
    /// <summary>
    /// Raises i_Event
    /// </summary>
    /// <param name="i_Event">Event to be raised</param>
    public virtual void Raise(EventHandler i_Event)
    {
      i_Event?.Invoke(this, EventArgs.Empty);
    }
    /// <summary>
    /// Raises i_Event
    /// </summary>
    /// <typeparam name="T">type of EventArgs</typeparam>
    /// <param name="i_Event">Event to be raised</param>
    /// <param name="i_EventArgs"></param>
    public virtual void Raise<T>(EventHandler<T> i_Event, T i_EventArgs)
    {
      i_Event?.Invoke(this, i_EventArgs);
    }
  }
}
