﻿namespace NiceCore.Base.Enums
{
  /// <summary>
  /// Status
  /// </summary>
  public enum RequestStatus
  {
    /// <summary>
    /// Request has not been evaluated
    /// </summary>
    NotEvaluated,
    /// <summary>
    /// Request has been accepted
    /// </summary>
    Accepted,
    /// <summary>
    /// Request has been denied
    /// </summary>
    Denied,
  }
}
