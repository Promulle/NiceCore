﻿using System;

namespace NiceCore.Base.Instances
{
  /// <summary>
  /// Parameter declaration containing type and actual value to use.
  /// Needed so that for null valued parameters can still be used
  /// </summary>
  public sealed class InstanceParameter
  {
    /// <summary>
    /// Value to use for constructior
    /// </summary>
    public object Value { get; init; }

    /// <summary>
    /// Type of the parameter. Mandatory if Instance is null
    /// </summary>
    public Type ParameterType { get; init; }

    private InstanceParameter()
    {
    }

    /// <summary>
    /// Static factory constructor creating a new instance of InstanceParameter with i_Instance as value and i_Instance.GetType() as ParameterType
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <returns></returns>
    public static InstanceParameter For(object i_Instance)
    {
      if (i_Instance == null)
      {
        throw new InvalidOperationException($"{nameof(For)} factory constructor can only be called with non-null values.");
      }
      return For(i_Instance, i_Instance.GetType());
    }

    /// <summary>
    /// Static factory constructor creating a new instance of InstanceParameter with i_Instance as value and i_ParameterType as ParameterType
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_ParameterType"></param>
    /// <returns></returns>
    public static InstanceParameter For(object i_Instance, Type i_ParameterType)
    {
      return new InstanceParameter()
      {
        Value = i_Instance,
        ParameterType = i_ParameterType
      };
    }
  }
}
