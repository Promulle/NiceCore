﻿using NiceCore.Base.Caching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace NiceCore.Base.Instances
{
  /// <summary>
  /// Alternative for Activator.CreateInstance
  /// </summary>
  public static class NcInstanceFactory
  {
    private static readonly SimpleCache<InstanceCreationFunctionKey, Func<Type, object[], object>> m_CachedFunctions = new();

    /// <summary>
    /// Creates an instance of T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static T CreateInstance<T>() where T : class
    {
      return CreateInstance(typeof(T)) as T;
    }

    /// <summary>
    /// Creates an instance of T. Important: having any null values in i_Parameters is NOT supported
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Parameters">parameters used to determine constructor and then passed to the constructor</param>
    /// <returns></returns>
    public static T CreateInstance<T>(params object[] i_Parameters) where T : class
    {
      return CreateInstance(typeof(T), i_Parameters) as T;
    }

    /// <summary>
    /// Creates an instance of T. Overload that allows null values when packaged in InstanceParameter objects
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Parameters">parameters used to determine constructor and then passed to the constructor</param>
    /// <returns></returns>
    public static T CreateInstance<T>(params InstanceParameter[] i_Parameters) where T : class
    {
      return CreateInstance(typeof(T), i_Parameters) as T;
    }

    /// <summary>
    /// Creates an instance of T. Overload that allows null values when packaged in InstanceParameter objects
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Parameters">parameters used to determine constructor and then passed to the constructor</param>
    /// <returns></returns>
    public static T CreateInstance<T>(IEnumerable<InstanceParameter> i_Parameters) where T : class
    {
      return CreateInstance(typeof(T), i_Parameters) as T;
    }

    /// <summary>
    /// Creates an instance of the given type if there is a parameterless constructor
    /// </summary>
    /// <param name="i_Type"></param>
    /// <returns></returns>
    public static object CreateInstance(Type i_Type)
    {
      return CreateInstance(i_Type, Array.Empty<object>());
    }

    /// <summary>
    /// Creates an instance of the given type if the there is a constructor with matching parameters. Important: having any null values in i_Parameters is NOT supported
    /// </summary>
    /// <param name="i_Type"></param>
    /// <param name="i_Parameters">parameters used to determine constructor and then passed to the constructor</param>
    /// <returns></returns>
    public static object CreateInstance(Type i_Type, params object[] i_Parameters)
    {
      return CreateInstance(i_Type, ConvertToInstanceParameters(i_Parameters));
    }

    /// <summary>
    /// Creates an instance of the given type if the there is a constructor with matching parameters. Important: having any null values in i_Parameters is NOT supported
    /// </summary>
    /// <param name="i_Type"></param>
    /// <param name="i_Parameters">parameters used to determine constructor and then passed to the constructor</param>
    /// <returns></returns>
    public static object CreateInstance(Type i_Type, IEnumerable<InstanceParameter> i_Parameters)
    {
      return CreateInstance(i_Type, i_Parameters.ToArray());
    }

    /// <summary>
    /// Creates an instance of the given type if the there is a constructor with matching parameters. Important: having any null values in i_Parameters is NOT supported
    /// </summary>
    /// <param name="i_Type"></param>
    /// <param name="i_Parameters">parameters used to determine constructor and then passed to the constructor</param>
    /// <returns></returns>
    public static object CreateInstance(Type i_Type, InstanceParameter[] i_Parameters)
    {
      if (i_Type.IsPrimitive || i_Type.IsValueType)
      {
        return Activator.CreateInstance(i_Type, i_Parameters.Select(r => r.Value).ToArray());
      }
      //use activator if there are no parameters since that is somehow faster
      if (i_Parameters == null || i_Parameters.Length == 0)
      {
        return Activator.CreateInstance(i_Type);
      }
      var key = new InstanceCreationFunctionKey()
      {
        ReturnType = i_Type,
        ParameterTypes = i_Parameters.Select(r => r.ParameterType).ToList()
      };
      return m_CachedFunctions.GetOrCreate(key, () => CreateFuncToCall(i_Type, i_Parameters)).Invoke(i_Type, i_Parameters.Select(r => r.Value).ToArray());
    }

    private static LambdaExpression CreateCtorFunc(Type i_Type, List<ParameterExpression> i_ParamExprList, Type[] i_Types)
    {
      var ctor = i_Type.GetConstructor(i_Types);
      if (ctor is null)
      {
        throw new InvalidOperationException("No constructor found for parameters. Did you pass them in the right order?");
      }
      var newExpr = Expression.New(ctor, i_ParamExprList);
      return Expression.Lambda(newExpr, i_ParamExprList);
    }

    private static Func<Type, object[], object> CreateFuncToCall(Type i_Type, params InstanceParameter[] i_Parameters)
    {
      Console.WriteLine($"Creating func for type {i_Type.FullName}");
      var paramTypeExpr = Expression.Parameter(typeof(Type));
      var paramObjectListExpr = Expression.Parameter(typeof(object[]));
      var paramTypeArray = i_Parameters.Select(r => r.ParameterType).ToArray();
      var paramExprList = paramTypeArray.Select(r => Expression.Parameter(r)).ToList();
      var ctorLambda = CreateCtorFunc(i_Type, paramExprList, paramTypeArray);
      var arrayParamExprs = new List<Expression>();

      for (var i = 0; i < i_Parameters.Length; i++)
      {
        var arrayIndexExpr = Expression.ArrayAccess(paramObjectListExpr, Expression.Constant(i));
        arrayParamExprs.Add(Expression.Convert(arrayIndexExpr, i_Parameters[i].ParameterType));
      }
      var invokeExpr = Expression.Invoke(ctorLambda, arrayParamExprs);
      var finalLambdaExpr = Expression.Lambda<Func<Type, object[], object>>(invokeExpr, paramTypeExpr, paramObjectListExpr);
      return finalLambdaExpr.Compile();
    }

    private static InstanceParameter[] ConvertToInstanceParameters(object[] i_ObjectParameters)
    {
      try
      {
        return i_ObjectParameters.Select(r => InstanceParameter.For(r)).ToArray();
      }
      catch (InvalidOperationException ex)
      {
        throw new NotSupportedException("CreateInstance does not support any parameters to be passed as null, but there was a null argument in i_Parameters.", ex);
      }
    }
  }
}
