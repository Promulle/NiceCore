﻿using NiceCore.Base.Caching;
using System;
using System.Collections.Generic;

namespace NiceCore.Base.Instances
{
  /// <summary>
  /// Key for dictionary
  /// </summary>
  internal class InstanceCreationFunctionKey : BaseCacheKey
  {
    /// <summary>
    /// Return type
    /// </summary>
    public Type ReturnType { get; init; }

    /// <summary>
    /// types of parameters
    /// </summary>
    public List<Type> ParameterTypes { get; init; } = new List<Type>();

    /// <summary>
    /// Equals
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj)
    {
      return obj is InstanceCreationFunctionKey key &&
             EqualityComparer<Type>.Default.Equals(ReturnType, key.ReturnType) &&
             ListEquals(ParameterTypes, key.ParameterTypes);
    }

    /// <summary>
    /// Get HashCode
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode()
    {
      return ReturnType.GetHashCode() ^ GetListHashCode(ParameterTypes);
    }
  }
}
