﻿using System;

namespace NiceCore.Base
{
  /// <summary>
  /// Interface for classes that have a isVisible property
  /// </summary>
  public interface ICanBeVisible
  {
    /// <summary>
    /// Event that is fired when the IsVisible property is changed
    /// </summary>
    event EventHandler<bool> VisibilityChanged;
    /// <summary>
    /// Whether it is visible or not
    /// </summary>
    bool IsVisible { get; set; }
  }
}
