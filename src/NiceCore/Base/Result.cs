﻿namespace NiceCore.Base
{
  /// <summary>
  /// Result class
  /// </summary>
  public class Result<T>
  {
    /// <summary>
    /// Whether the operation was a success
    /// </summary>
    public bool Success { get; init; }

    /// <summary>
    /// Value
    /// </summary>
    public T ResultValue { get; init; }

    /// <summary>
    /// Static creator for Result that was succesfull
    /// </summary>
    /// <param name="i_Result"></param>
    /// <returns></returns>
    public static Result<T> Ok(T i_Result)
    {
      return new()
      {
        Success = true,
        ResultValue = i_Result
      };
    }

    /// <summary>
    /// Failure
    /// </summary>
    /// <returns></returns>
    public static Result<T> Failure()
    {
      return new()
      {
        Success = false,
        ResultValue = default
      };
    }
  }
}
