﻿using System;

namespace NiceCore.Base
{
  /// <summary>
  /// Class that just implements INcTimeInterval
  /// </summary>
  public class DefaultNcTimeInterval : INcTimeInterval
  {
    /// <summary>
    /// Start Time
    /// </summary>
    public DateTime TimeFrom { get; init; }

    /// <summary>
    /// End Time
    /// </summary>
    public DateTime TimeTo { get; init; }
  }
}
