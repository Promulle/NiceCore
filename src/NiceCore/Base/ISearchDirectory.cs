﻿using System.Collections.Generic;

namespace NiceCore.Base
{
  /// <summary>
  /// Interface for searchdirectories
  /// </summary>
  public interface ISearchDirectory
  {
    /// <summary>
    /// Path of directory
    /// </summary>
    string DirectoryPath { get; set; }
    /// <summary>
    /// Include subdirectories or not
    /// </summary>
    bool UseRecursive { get; set; }
    /// <summary>
    /// Returns list of directories to search in. Uses recursive to determine if sub directories should be used
    /// </summary>
    /// <returns></returns>
    List<string> GetDirectories();
  }
}