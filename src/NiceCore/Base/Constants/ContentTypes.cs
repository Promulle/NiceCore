﻿namespace NiceCore.Base.Constants
{
  /// <summary>
  /// Constant class for Content-types of http responses
  /// </summary>
  public static class ContentTypes
  {
    /// <summary>
    /// Type of content gets returned by Invoke as ContentWrapper of T
    /// </summary>
    public const string FUNCTION_DEFINED = "defined";

    /// <summary>
    /// unkown content type
    /// </summary>
    public const string UNKNOWN = "unkown";

    #region App
    /// <summary>
    /// type for application/json
    /// </summary>
    public const string APP_JSON = "application/json";

    /// <summary>
    /// javascript server
    /// </summary>
    public const string APP_SCRIPT = "application/javascript";

    /// <summary>
    /// Application pdf
    /// </summary>
    public const string APP_PDF = "application/pdf";

    /// <summary>
    /// Application octet stream
    /// </summary>
    public const string APP_OCT_STREAM = "application/octet-stream";

    /// <summary>
    /// App Zip
    /// </summary>
    public const string APP_ZIP = "application/zip";

    #endregion
    #region Images
    /// <summary>
    /// Icons
    /// </summary>
    public const string IMAGE_ICO = "image/x-icon";

    /// <summary>
    /// jpegs
    /// </summary>
    public const string IMAGE_JPG = "image/jpeg";

    /// <summary>
    /// png images
    /// </summary>
    public const string IMAGE_PNG = "image/png";

    #endregion
    #region Text
    /// <summary>
    /// css styles
    /// </summary>
    public const string TEXT_CSS = "text/css";

    /// <summary>
    /// css styles
    /// </summary>
    public const string TEXT_PLAIN = "text/plain";

    /// <summary>
    /// type for text/html
    /// </summary>
    public const string TEXT_HTML = "text/html";

    /// <summary>
    /// javascript
    /// </summary>
    public const string TEXT_JS = "text/javascript";
    #endregion
  }
}
