﻿namespace NiceCore.Base
{
  /// <summary>
  /// Interface for Objects with Initialize-Method
  /// </summary>
  public interface IInitializable
  {
    /// <summary>
    /// Whether this object has been initialized
    /// </summary>
    bool IsInitialized { get; }
    /// <summary>
    /// Initialize method
    /// </summary>
    void Initialize();
  }
}
