﻿using System;

namespace NiceCore.Base
{
  /// <summary>
  /// Interval for classes that represent some kind of Time interval
  /// </summary>
  public interface INcTimeInterval
  {
    /// <summary>
    /// Start time of the interval
    /// </summary>
    public DateTime TimeFrom { get; }

    /// <summary>
    /// End time of the interval
    /// </summary>
    public DateTime TimeTo { get; }
  }
}
