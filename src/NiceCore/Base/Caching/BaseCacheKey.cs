﻿using System.Collections.Generic;
using System.Linq;
using NiceCore.Extensions;

namespace NiceCore.Base.Caching
{
  /// <summary>
  /// Base class that cache keys should inherit from
  /// </summary>
  public abstract class BaseCacheKey
  {
    /// <summary>
    /// Returns a value based hashcode of i_List that actually takes order into account
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_List"></param>
    /// <returns></returns>
    protected static int GetListHashCode<T>(IReadOnlyCollection<T> i_List)
    {
      var hashCode = 0;
      for (var i = 0; i < i_List.Count; i++)
      {
        const int uniqHash = 13;
        var element = i_List.ElementAtEx(i);
        hashCode ^= element.GetHashCode() + ((i + uniqHash) * 7);
      }
      return hashCode;
    }

    /// <summary>
    /// Checks that list are equal using SequenceEqual and is null safe
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_FirstList"></param>
    /// <param name="i_SecondList"></param>
    /// <returns></returns>
    protected static bool ListEquals<T>(IReadOnlyCollection<T> i_FirstList, IReadOnlyCollection<T> i_SecondList)
    {
      if (i_FirstList == null && i_SecondList == null)
      {
        return true;
      }
      if (i_FirstList == null || i_SecondList == null)
      {
        return false;
      }
      return i_FirstList.SequenceEqual(i_SecondList);
    }
  }
}
