﻿namespace NiceCore.Base.Caching
{
  /// <summary>
  /// Cache that is typed for some type
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  /// <typeparam name="TValue"></typeparam>
  public abstract class BaseSpecializedSimpleCache<TKey, TValue>
  {
    private readonly SimpleCache<TKey, TValue> m_InternalCache = new();

    /// <summary>
    /// Create Func that is used if no value was found for i_Key
    /// </summary>
    /// <param name="i_Key"></param>
    /// <returns></returns>
    protected abstract TValue CreateFunc(TKey i_Key);

    /// <summary>
    /// Searches for value identified by i_Key, if none is found the internal create func is used
    /// </summary>
    /// <param name="i_Key"></param>
    /// <returns></returns>
    public TValue Get(TKey i_Key)
    {
      return m_InternalCache.GetOrCreate(i_Key, () => CreateFunc(i_Key));
    }
  }
}
