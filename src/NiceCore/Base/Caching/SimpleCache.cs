﻿using NiceCore.Base.Caching.Exceptions;
using System;
using System.Collections.Generic;

namespace NiceCore.Base.Caching
{
  /// <summary>
  /// Simple cache for reusing values
  /// </summary>
  public class SimpleCache<TKey, TValue>
  {
    private readonly object m_LockObject = new();
    private readonly Dictionary<TKey, TValue> m_InternalDictionary = new();

    /// <summary>
    /// Adds i_Value for i_Key, replaces old value if key was already added
    /// </summary>
    /// <param name="i_Key"></param>
    /// <param name="i_Value"></param>
    public void Add(TKey i_Key, TValue i_Value)
    {
      lock (m_LockObject)
      {
        m_InternalDictionary[i_Key] = i_Value;
      }
    }

    /// <summary>
    /// Returns the value that has been associated with i_Key. If none is found, a new one is created by invoking i_ValueFunc and added for i_Key
    /// </summary>
    /// <param name="i_Key"></param>
    /// <param name="i_ValueFunc"></param>
    /// <returns></returns>
    /// <exception cref="CacheValueException">CacheValueException if i_ValueFunc throws any exception. The thrown exception will be encapsulated by the CacheValueException. See InnerException for details about what went wrong.</exception>
    public TValue GetOrCreate(TKey i_Key, Func<TValue> i_ValueFunc)
    {
      lock (m_LockObject)
      {
        if (!m_InternalDictionary.TryGetValue(i_Key, out var val))
        {
          val = CreateValue(i_ValueFunc);
          Add(i_Key, val);
        }
        return val;
      }
    }
    
    /// <summary>
    /// Returns the value that has been associated with i_Key. If none is found, a new one is created by invoking i_ValueFunc and added for i_Key
    /// </summary>
    /// <param name="i_Key"></param>
    /// <param name="i_ValueFunc"></param>
    /// <returns></returns>
    /// <exception cref="CacheValueException">CacheValueException if i_ValueFunc throws any exception. The thrown exception will be encapsulated by the CacheValueException. See InnerException for details about what went wrong.</exception>
    public TValue GetOrCreate(TKey i_Key, Func<TKey, TValue> i_ValueFunc)
    {
      lock (m_LockObject)
      {
        if (!m_InternalDictionary.TryGetValue(i_Key, out var val))
        {
          val = CreateValue(i_ValueFunc, i_Key);
          Add(i_Key, val);
        }
        return val;
      }
    }

    private static TValue CreateValue(Func<TValue> i_ValueFunc)
    {
      try
      {
        return i_ValueFunc.Invoke();
      }
      catch (Exception ex)
      {
        throw new CacheValueException(ex);
      }
    }
    
    private static TValue CreateValue(Func<TKey, TValue> i_ValueFunc, TKey i_Key)
    {
      try
      {
        return i_ValueFunc.Invoke(i_Key);
      }
      catch (Exception ex)
      {
        throw new CacheValueException(ex);
      }
    }
  }
}
