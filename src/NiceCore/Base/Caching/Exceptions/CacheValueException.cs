﻿using System;

namespace NiceCore.Base.Caching.Exceptions
{
  /// <summary>
  /// Exception that is thrown if anything goes wrong inside the creation of the object searched for
  /// </summary>
#pragma warning disable RCS1194 // Implement exception constructors.
  public class CacheValueException : Exception
#pragma warning restore RCS1194 // Implement exception constructors.
  {
    private const string MESSAGE = "Value for cache could not be created because something went wrong. Please see the inner exception for more details.";

    /// <summary>
    /// Constructor that is filled by exception. sets the default message
    /// </summary>
    /// <param name="i_InnerException"></param>
    public CacheValueException(Exception i_InnerException) : base(MESSAGE, i_InnerException)
    {
    }
  }
}
