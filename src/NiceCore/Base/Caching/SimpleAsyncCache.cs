﻿using NiceCore.Base.Caching.Exceptions;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace NiceCore.Base.Caching
{
  /// <summary>
  /// Simple cache for reusing values
  /// </summary>
  public class SimpleAsyncCache<TKey, TValue>
  {
    private readonly SemaphoreSlim m_LockObject = new(1,1);
    private readonly Dictionary<TKey, TValue> m_InternalDictionary = new();

    /// <summary>
    /// Adds i_Value for i_Key, replaces old value if key was already added
    /// </summary>
    /// <param name="i_Key"></param>
    /// <param name="i_Value"></param>
    public async Task Add(TKey i_Key, TValue i_Value)
    {
      await m_LockObject.WaitAsync();
      try
      {
        m_InternalDictionary[i_Key] = i_Value;
      }
      finally
      {
        m_LockObject.Release();
      }
    }

    /// <summary>
    /// Remove
    /// </summary>
    /// <param name="i_Key"></param>
    public async Task Remove(TKey i_Key)
    {
      await m_LockObject.WaitAsync();
      try
      {
        if (m_InternalDictionary.ContainsKey(i_Key))
        {
          m_InternalDictionary.Remove(i_Key);
        }
      }
      finally
      {
        m_LockObject.Release();
      }
    }

    /// <summary>
    /// Returns the value that has been associated with i_Key. If none is found, a new one is created by invoking i_ValueFunc and added for i_Key
    /// </summary>
    /// <param name="i_Key"></param>
    /// <param name="i_ValueFunc"></param>
    /// <returns></returns>
    /// <exception cref="CacheValueException">CacheValueException if i_ValueFunc throws any exception. The thrown exception will be encapsulated by the CacheValueException. See InnerException for details about what went wrong.</exception>
    public async Task<TValue> GetOrCreate(TKey i_Key, Func<Task<TValue>> i_ValueFunc)
    {
      await m_LockObject.WaitAsync();
      try
      {
        if (!m_InternalDictionary.TryGetValue(i_Key, out var val))
        {
          val = await CreateValue(i_ValueFunc).ConfigureAwait(false);
          if (!EqualityComparer<TValue>.Default.Equals(val, default))
          {
            m_InternalDictionary[i_Key] = val;  
          }
        }
        return val;
      }
      finally
      {
        m_LockObject.Release();
      }
    }

    private static Task<TValue> CreateValue(Func<Task<TValue>> i_ValueFunc)
    {
      try
      {
        return i_ValueFunc.Invoke();
      }
      catch (Exception ex)
      {
        throw new CacheValueException(ex);
      }
    }
  }
}
