﻿namespace NiceCore.Base.Services
{
  /// <summary>
  /// Base implementation for iService
  /// </summary>
  public abstract class BaseService : BaseDisposable, IService
  {
    /// <summary>
    /// Whether this Service is initialized
    /// </summary>
    public virtual bool IsInitialized { get; protected set; }

    /// <summary>
    /// Initializes the service
    /// </summary>
    public void Initialize()
    {
      if (!IsInitialized)
      {
        InternalInitialize();
        IsInitialized = true;
      }
    }

    /// <summary>
    /// Initialize function to override
    /// </summary>
    protected virtual void InternalInitialize()
    {
    }
  }
}
