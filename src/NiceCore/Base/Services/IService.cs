﻿namespace NiceCore.Base.Services
{
  /// <summary>
  /// Default interface for services
  /// </summary>
  public interface IService : IInitializable, IBaseDisposable
  {
  }
}
