﻿using System;

namespace NiceCore.Base
{
  /// <summary>
  /// Interface for disposable objects
  /// </summary>
  public interface IBaseDisposable : IDisposable
  {
    /// <summary>
    /// Event, that fires, when a dispose-process starts
    /// </summary>
    event EventHandler OnDisposeStart;
    /// <summary>
    /// Event, that fires, when a dispose-process is done
    /// </summary>
    event EventHandler OnDisposeDone;
  }
}
