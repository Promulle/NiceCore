using System;

namespace NiceCore.Base
{
  /// <summary>
  /// base async disposable
  /// </summary>
  public interface IBaseAsyncDisposable: IAsyncDisposable
  {
    /// <summary>
    /// Event, that fires, when a dispose-process starts
    /// </summary>
    event EventHandler OnDisposeStart;
    /// <summary>
    /// Event, that fires, when a dispose-process is done
    /// </summary>
    event EventHandler OnDisposeDone;
  }
}
