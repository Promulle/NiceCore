﻿namespace NiceCore.Base
{
  /// <summary>
  /// Converts between values
  /// </summary>
  public interface IOneWayValueTransformer<TIn, TOut>
  {
    /// <summary>
    /// Transforms a TIn to TOut
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    TOut Transform(TIn i_Value);
  }
}
