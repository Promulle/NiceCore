﻿using System;

namespace NiceCore.Base
{
  /// <summary>
  /// Class for throwing exceptions
  /// </summary>
  public static class ExceptionHelper
  {
    /// <summary>
    /// Throws an exception if i_Condition is true
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Condition"></param>
    public static void ThrowIf<T>(bool i_Condition) where T : Exception
    {
      if (i_Condition)
      {
        throw Activator.CreateInstance<T>();
      }
    }

    /// <summary>
    /// Throws an exception if i_Condition is true
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Condition"></param>
    /// <param name="i_Message"></param>
    public static void ThrowIf<T>(bool i_Condition, string i_Message) where T : Exception
    {
      if (i_Condition)
      {
        throw Activator.CreateInstance(typeof(T), new[] { i_Message }) as Exception;
      }
    }
  }
}
