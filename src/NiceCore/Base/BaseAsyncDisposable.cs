using System;
using System.Threading.Tasks;

namespace NiceCore.Base
{
  /// <summary>
  /// Base Disposable for async dispose actions
  /// </summary>
  public class BaseAsyncDisposable : BaseDisposable, IBaseAsyncDisposable
  {
    /// <summary>
    /// Override for async dispose
    /// </summary>
    /// <returns></returns>
    protected virtual ValueTask DisposeAsyncInternal()
    {
      return ValueTask.CompletedTask;
    }

    /// <summary>
    /// Dispose Async
    /// </summary>
    /// <returns></returns>
    public async ValueTask DisposeAsync()
    {
      RaiseDisposeStart();
      await DisposeAsyncInternal().ConfigureAwait(false);
      GC.SuppressFinalize(this);
      DisposeInternal(false);
      RaiseDisposeDone();
      ClearEvents();
    }
  }
}
