﻿using System;

namespace NiceCore.Base
{
  /// <summary>
  /// Base implementation of IBaseDisposable
  /// </summary>
  public abstract class BaseDisposable : EventRaiser, IBaseDisposable
  {
    private bool m_IsDisposed = false;

    /// <summary>
    /// Event, that fires once a dispose process has started
    /// </summary>
    public event EventHandler OnDisposeStart;

    /// <summary>
    /// Event, that fires once a dispose process is done
    /// </summary>
    public event EventHandler OnDisposeDone;

    /// <summary>
    /// Disposes
    /// </summary>
    public void Dispose()
    {
      RaiseDisposeStart();
      DisposeInternal(true);
      GC.SuppressFinalize(this);
      RaiseDisposeDone();
      ClearEvents();
    }

    /// <summary>
    /// Raises dispose start
    /// </summary>
    protected void RaiseDisposeStart()
    {
      Raise(OnDisposeStart);
    }

    /// <summary>
    /// Raises dispose done
    /// </summary>
    protected void RaiseDisposeDone()
    {
      Raise(OnDisposeDone);
    }

    /// <summary>
    /// Clear Events
    /// </summary>
    protected void ClearEvents()
    {
      OnDisposeStart = delegate { };
      OnDisposeDone  = delegate { };
    }

    /// <summary>
    /// Internal Dispose
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    internal void DisposeInternal(bool i_IsDisposing)
    {
      if (m_IsDisposed)
      {
        return;
      }

      m_IsDisposed = true;

      if (i_IsDisposing)
      {
        Dispose(true);
      }
    }

    /// <summary>
    /// Internal, overridable Dispose function
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    protected virtual void Dispose(bool i_IsDisposing)
    {
    }
  }
}
