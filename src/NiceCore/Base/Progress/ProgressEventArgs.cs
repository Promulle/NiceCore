﻿using System;

namespace NiceCore.Base.Progress
{
  /// <summary>
  /// Class for any event thing that is related to progress
  /// </summary>
  public class ProgressEventArgs : EventArgs
  {
    /// <summary>
    /// State that has something to do with this args event
    /// </summary>
    public ProgressState State { get; set; }
  }
}
