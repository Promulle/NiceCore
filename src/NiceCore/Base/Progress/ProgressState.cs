﻿namespace NiceCore.Base.Progress
{
  /// <summary>
  /// Class that tracks progress for a specific name
  /// </summary>
  public class ProgressState
  {
    /// <summary>
    /// Name of progress
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// Where the progress currently is
    /// </summary>
    public long Current { get; set; }
    /// <summary>
    /// Maximum value of progress
    /// </summary>
    public long MaxValue { get; set; }
    /// <summary>
    /// Whether the progress is determinate or not
    /// </summary>
    public bool IsIndeterminate { get; set; }
    /// <summary>
    /// Whether Current has reached/surpassed MaxValue or not
    /// </summary>
    public bool IsDone
    {
      get { return Current >= MaxValue; }
    }
  }
}
