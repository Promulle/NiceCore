﻿using System;

namespace NiceCore.Base
{
  /// <summary>
  /// model that holds state of something
  /// </summary>
  public interface IStateModel<T>
  {
    /// <summary>
    /// Event that is fired when state is changed
    /// </summary>
    event EventHandler StateChanged;
    /// <summary>
    /// Instance whose state is held by this object
    /// </summary>
    T Instance { get; set; }
  }
}
