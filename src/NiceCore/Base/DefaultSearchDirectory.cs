﻿using System.Collections.Generic;
using System.IO;

namespace NiceCore.Base
{
  /// <summary>
  /// Class representing a SearchDirectory
  /// </summary>
  public class DefaultSearchDirectory : ISearchDirectory
  {
    /// <summary>
    /// Path of Top-Directory
    /// </summary>
    public string DirectoryPath { get; set; }
    /// <summary>
    /// Whether subdirectories should be used
    /// </summary>
    public bool UseRecursive { get; set; }
    /// <summary>
    /// Constructor filling DirectoryPath
    /// </summary>
    /// <param name="i_DirectoryPath"></param>
    public DefaultSearchDirectory(string i_DirectoryPath)
    {
      DirectoryPath = i_DirectoryPath;
    }
    /// <summary>
    /// Returns directories to search in. Includes subdirectories if [Recursive] is true
    /// </summary>
    /// <returns></returns>
    public List<string> GetDirectories()
    {
      var res = new List<string>();
      if (DirectoryPath != null)
      {
        res.Add(DirectoryPath);
        if (UseRecursive)
        {
          res.AddRange(Directory.GetDirectories(DirectoryPath, "*", SearchOption.AllDirectories));
        }
      }
      return res;
    }
  }
}
