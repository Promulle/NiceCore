﻿namespace NiceCore.Base
{
  /// <summary>
  /// Wrapper for instances
  /// </summary>
  public class InstanceWrapper<TType>
  {
    /// <summary>
    /// Instance that is held by this wrapper
    /// </summary>
    public TType Instance { get; set; }
  }
}
