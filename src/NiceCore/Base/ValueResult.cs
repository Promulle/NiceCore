// Copyright 2021 Sycorax Systemhaus GmbH

namespace NiceCore.Base
{
  /// <summary>
  /// Value Result
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public struct ValueResult<T>
  {
    /// <summary>
    /// Value
    /// </summary>
    public T Value { get; init; }
    
    /// <summary>
    /// Success
    /// </summary>
    public bool Success { get; init; }
  }
}