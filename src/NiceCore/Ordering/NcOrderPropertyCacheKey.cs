﻿using NiceCore.Base.Caching;
using System;
using System.Collections.Generic;

namespace NiceCore.Ordering
{
  /// <summary>
  /// Cache key for order func
  /// </summary>
  public class NcOrderPropertyCacheKey : BaseCacheKey
  {
    /// <summary>
    /// Type
    /// </summary>
    public Type Type { get; init; }

    /// <summary>
    /// Property Path
    /// </summary>
    public string PropertyPath { get; init; }

    /// <summary>
    /// Equals
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj)
    {
      return obj is NcOrderPropertyCacheKey key &&
             EqualityComparer<Type>.Default.Equals(Type, key.Type) &&
             PropertyPath == key.PropertyPath;
    }

    /// <summary>
    /// GethashCode
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode()
    {
      return HashCode.Combine(Type, PropertyPath);
    }
  }
}
