﻿using NiceCore.Base.Caching;
using NiceCore.Caches;
using NiceCore.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace NiceCore.Ordering
{
  /// <summary>
  /// Static class for translation of order values
  /// </summary>
  public static class NcOrderTranslation
  {
    private static readonly MethodInfo m_ApplyDescriptorEnumerableFunc = GetApplyDescriptorEnumerableFunc();
    private static readonly MethodInfo m_ApplyDescriptorQueryableFunc = GetApplyDescriptorQueryableFunc();
    /// <summary>
    /// Cache that stores func of T,TKey for each TopType/PropertyPath pair
    /// </summary>
    private static readonly SimpleCache<NcOrderPropertyCacheKey, object> m_OrderFuncCache = new();

    /// <summary>
    /// Cache that stores expression of func of T,TKey for each TopType/PropertyPath pair
    /// </summary>
    private static readonly SimpleCache<NcOrderPropertyCacheKey, object> m_OrderExprCache = new();

    /// <summary>
    /// Cache that holds the func of IEnumerable of T, NcOrderDescriptor, IEnumerable of T used to apply the ordering for enumerables
    /// </summary>
    private static readonly SimpleCache<NcOrderPropertyCacheKey, object> m_ApplyDescriptorEnumerableCache = new();

    /// <summary>
    /// Cache that holds the func of IQueryable of T, NcOrderDescriptor, IQueryable of T used to apply the ordering for enumerables
    /// </summary>
    private static readonly SimpleCache<NcOrderPropertyCacheKey, object> m_ApplyDescriptorQueryableCache = new();

    /// <summary>
    /// Translates and applies the order descriptors to i_Source
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Source"></param>
    /// <param name="i_OrderDescriptors"></param>
    /// <returns></returns>
    public static IEnumerable<T> Translate<T>(IEnumerable<T> i_Source, IEnumerable<NcOrderDescriptor> i_OrderDescriptors)
    {
      var enumerable = i_Source;
      foreach (var descriptor in i_OrderDescriptors)
      {
        enumerable = InvokeApplyDescriptor(enumerable, descriptor);
      }
      return enumerable;
    }

    /// <summary>
    /// Translates and applies the order descriptors to i_Source
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Source"></param>
    /// <param name="i_OrderDescriptors"></param>
    /// <returns></returns>
    public static IQueryable<T> Translate<T>(IQueryable<T> i_Source, IEnumerable<NcOrderDescriptor> i_OrderDescriptors)
    {
      var enumerable = i_Source;
      foreach (var descriptor in i_OrderDescriptors)
      {
        enumerable = InvokeApplyDescriptor(enumerable, descriptor);
      }
      return enumerable;
    }

    private static IEnumerable<T> InvokeApplyDescriptor<T>(IEnumerable<T> i_Source, NcOrderDescriptor i_Descriptor)
    {
      var key = new NcOrderPropertyCacheKey()
      {
        Type = typeof(T),
        PropertyPath = i_Descriptor.PropertyName
      };
      var applyFunc = m_ApplyDescriptorEnumerableCache.GetOrCreate(key, () => GetApplyDescriptorFunc(m_ApplyDescriptorEnumerableFunc, typeof(IEnumerable<T>), typeof(T), i_Descriptor)) as Func<IEnumerable<T>, NcOrderDescriptor, IEnumerable<T>>;
      return applyFunc.Invoke(i_Source, i_Descriptor);
    }

    private static IQueryable<T> InvokeApplyDescriptor<T>(IQueryable<T> i_Source, NcOrderDescriptor i_Descriptor)
    {
      var key = new NcOrderPropertyCacheKey()
      {
        Type = typeof(T),
        PropertyPath = i_Descriptor.PropertyName
      };
      var applyFunc = m_ApplyDescriptorQueryableCache.GetOrCreate(key, () => GetApplyDescriptorFunc(m_ApplyDescriptorQueryableFunc, typeof(IQueryable<T>), typeof(T), i_Descriptor)) as Func<IQueryable<T>, NcOrderDescriptor, IQueryable<T>>;
      return applyFunc.Invoke(i_Source, i_Descriptor);
    }

    private static object GetApplyDescriptorFunc(MethodInfo i_ApplyFunc, Type i_SourceType, Type i_ObjectType, NcOrderDescriptor i_Descriptor)
    {
      var sourceParamExpr = Expression.Parameter(i_SourceType);
      var descriptorParamExpr = Expression.Parameter(typeof(NcOrderDescriptor));
      var genericMethod = i_ApplyFunc.MakeGenericMethod(new[] { i_ObjectType, GetTKeyType(i_Descriptor, i_ObjectType) });
      var callExpr = Expression.Call(null, genericMethod, sourceParamExpr, descriptorParamExpr);
      var lambdaExpr = Expression.Lambda(callExpr, sourceParamExpr, descriptorParamExpr);
      return lambdaExpr.Compile();
    }

    private static Type GetTKeyType(NcOrderDescriptor i_Descriptor, Type i_ObjectType)
    {
      var names = i_Descriptor.PropertyName.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
      var curType = i_ObjectType;
      foreach (var name in names)
      {
        var key = new PropertyTypeCacheKey()
        {
          PropertyName = name,
          DeclaringType = curType
        };
        curType = ExpressionCaches.PropertyTypeCache.Get(key);
      }
      return curType;
    }

    private static IEnumerable<T> ApplyDescriptor<T, TKey>(IEnumerable<T> i_Source, NcOrderDescriptor i_Descriptor)
    {
      var key = new NcOrderPropertyCacheKey()
      {
        PropertyPath = i_Descriptor.PropertyName,
        Type = typeof(T)
      };
      var func = m_OrderFuncCache.GetOrCreate(key, () => GetOrderByFunc<T, TKey>(i_Descriptor.PropertyName)) as Func<T, TKey>;
      if (i_Descriptor.Direction == NcOrderDirections.Ascending)
      {
        return i_Source.OrderBy(func);
      }
      else if (i_Descriptor.Direction == NcOrderDirections.Descending)
      {
        return i_Source.OrderByDescending(func);
      }
      throw new InvalidOperationException($"Could not translate order. Direction {i_Descriptor.Direction} is currently not supported. Use either {NcOrderDirections.Ascending} or {NcOrderDirections.Descending}.");
    }

    private static IQueryable<T> ApplyDescriptor<T, TKey>(IQueryable<T> i_Source, NcOrderDescriptor i_Descriptor)
    {
      var key = new NcOrderPropertyCacheKey()
      {
        PropertyPath = i_Descriptor.PropertyName,
        Type = typeof(T)
      };
      var expr = m_OrderExprCache.GetOrCreate(key, () => GetOrderByExpression<T, TKey>(i_Descriptor.PropertyName)) as Expression<Func<T, TKey>>;
      if (i_Descriptor.Direction == NcOrderDirections.Ascending)
      {
        return i_Source.OrderBy(expr);
      }
      else if (i_Descriptor.Direction == NcOrderDirections.Descending)
      {
        return i_Source.OrderByDescending(expr);
      }
      throw new InvalidOperationException($"Could not translate order. Direction {i_Descriptor.Direction} is currently not supported. Use either {NcOrderDirections.Ascending} or {NcOrderDirections.Descending}.");
    }

    private static Expression<Func<T, TKey>> GetOrderByExpression<T, TKey>(string i_PropertyName)
    {
      var parameter = Expression.Parameter(typeof(T));
      var orderByBodyExpr = GetPropertyExpression(i_PropertyName, parameter);
      if (orderByBodyExpr != null)
      {
        return Expression.Lambda<Func<T, TKey>>(orderByBodyExpr, parameter);
      }
      return null;
    }

    private static Func<T, TKey> GetOrderByFunc<T, TKey>(string i_PropertyName)
    {
      var expression = GetOrderByExpression<T, TKey>(i_PropertyName);
      return expression.Compile();
    }

    private static Expression GetPropertyExpression(string i_PropertyName, ParameterExpression i_ParamExpr)
    {
      if (!i_PropertyName.Contains('.'))
      {
        return Expression.Property(i_ParamExpr, i_PropertyName);
      }
      else
      {
        var names = i_PropertyName.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
        var curExpr = Expression.Property(i_ParamExpr, names[0]);
        for (var i = 1; i < names.Length; i++)
        {
          curExpr = Expression.Property(curExpr, names[i]);
        }
        return curExpr;
      }
    }

    private static MethodInfo GetApplyDescriptorEnumerableFunc()
    {
      return GetApplyDescriptorFunc(typeof(IEnumerable<>));
    }

    private static MethodInfo GetApplyDescriptorQueryableFunc()
    {
      return GetApplyDescriptorFunc(typeof(IQueryable<>));
    }

    private static MethodInfo GetApplyDescriptorFunc(Type i_FirstParameterType)
    {
      return ReflectionUtils.GetMethodEx(typeof(NcOrderTranslation), nameof(ApplyDescriptor), new List<MethodParameterType>()
      {
        MethodParameterType.Of(i_FirstParameterType, new List<MethodParameterType>()
        {
          MethodParameterType.GenericPlaceHolder()
        }),
        MethodParameterType.Of(typeof(NcOrderDescriptor))
      });
    }
  }
}
