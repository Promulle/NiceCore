﻿namespace NiceCore.Ordering
{
  /// <summary>
  /// Directions for ordering
  /// </summary>
  public enum NcOrderDirections
  {
    /// <summary>
    /// Ascending
    /// </summary>
    Ascending,

    /// <summary>
    /// Descending
    /// </summary>
    Descending,
  }
}
