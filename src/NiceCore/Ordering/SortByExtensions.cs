﻿using System.Collections.Generic;
using System.Linq;

namespace NiceCore.Ordering
{
  /// <summary>
  /// Extensions for queries that enable sort by
  /// </summary>
  public static class SortByExtensions
  {
    /// <summary>
    /// Filters i_Values using i_Filter
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Values"></param>
    /// <param name="i_Orders"></param>
    /// <returns></returns>
    public static IQueryable<T> SortBy<T>(this IQueryable<T> i_Values, params NcOrderDescriptor[] i_Orders)
    {
      return NcOrderTranslation.Translate(i_Values, i_Orders);
    }
    
    /// <summary>
    /// Filters i_Values using i_Filter
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Values"></param>
    /// <param name="i_Orders"></param>
    /// <returns></returns>
    public static IQueryable<T> SortBy<T>(this IQueryable<T> i_Values, IEnumerable<NcOrderDescriptor> i_Orders)
    {
      return NcOrderTranslation.Translate(i_Values, i_Orders);
    }
    
    /// <summary>
    /// Filters i_Values using i_Filter
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Values"></param>
    /// <param name="i_Orders"></param>
    /// <returns></returns>
    public static IEnumerable<T> SortBy<T>(this IEnumerable<T> i_Values, params NcOrderDescriptor[] i_Orders)
    {
      return NcOrderTranslation.Translate(i_Values, i_Orders);
    }
  }
}
