﻿namespace NiceCore.Ordering
{
  /// <summary>
  /// Descriptor describing a order to be applied to a query/enumerable
  /// </summary>
  public class NcOrderDescriptor
  {
    /// <summary>
    /// Property Name
    /// </summary>
    public string PropertyName { get; set; }

    /// <summary>
    /// Direction
    /// </summary>
    public NcOrderDirections Direction { get; set; }
  }
}
