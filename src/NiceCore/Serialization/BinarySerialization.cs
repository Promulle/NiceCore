﻿using ProtoBuf;
using System.IO;

namespace NiceCore.Serialization
{
  /// <summary>
  /// Serialization abstraction to be used for serialization
  /// </summary>
  public static class BinarySerialization
  {
    /// <summary>
    /// Serialize i_Object into the provided stream
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Object"></param>
    /// <param name="i_Stream"></param>
    public static void Serialize<T>(T i_Object, Stream i_Stream)
    {
      Serializer.Serialize(i_Stream, i_Object);
    }

    /// <summary>
    /// Deserialize from i_Stream into an instance of T
    /// </summary>
    /// <param name="i_Stream"></param>
    /// <returns></returns>
    public static T Deserialize<T>(Stream i_Stream)
    {
      return Serializer.Deserialize<T>(i_Stream);
    }

    /// <summary>
    /// Deserialize from byte array
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Bytes"></param>
    /// <returns></returns>
    public static T Deserialize<T>(byte[] i_Bytes)
    {
      using(var memStream = new MemoryStream(i_Bytes))
      {
        return Deserialize<T>(memStream);
      }
    }

    /// <summary>
    /// Serialize the instance to an array of bytes
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Instance"></param>
    /// <returns></returns>
    public static byte[] Serialize<T>(T i_Instance)
    {
      using(var memStream = new MemoryStream())
      {
        Serialize(i_Instance, memStream);
        return memStream.ToArray();
      }
    }
  }
}
