﻿using System;
using System.Text.Json;

namespace NiceCore.Serialization
{
  /// <summary>
  /// Static helper class for serialization to/from json
  /// </summary>
  public static class JSONSerialization
  {
    /// <summary>
    /// Serializes an object to string
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Object"></param>
    /// <returns></returns>
    public static string Serialize<T>(T i_Object)
    {
      return JsonSerializer.Serialize(i_Object);
    }

    /// <summary>
    /// Serializes an object to string
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Object"></param>
    /// <returns></returns>
    public static string SerializePretty<T>(T i_Object)
    {
      return JsonSerializer.Serialize(i_Object, new JsonSerializerOptions() { WriteIndented = true });
    }

    /// <summary>
    /// Serializes an object to string
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Object"></param>
    /// <param name="i_Options"></param>
    /// <returns></returns>
    public static string SerializeWithOptions<T>(T i_Object, JsonSerializerOptions i_Options)
    {
      return JsonSerializer.Serialize(i_Object, i_Options);
    }

    /// <summary>
    /// Serializes an object to string
    /// </summary>
    /// <param name="i_Object"></param>
    /// <param name="i_Type"></param>
    /// <returns></returns>
    public static string Serialize(object i_Object, Type i_Type)
    {
      return JsonSerializer.Serialize(i_Object, i_Type);
    }

    /// <summary>
    /// Serializes an object to string
    /// </summary>
    /// <param name="i_Object"></param>
    /// <param name="i_Type"></param>
    /// <param name="i_Options"></param>
    /// <returns></returns>
    public static string SerializeWithOptions(object i_Object, Type i_Type, JsonSerializerOptions i_Options)
    {
      return JsonSerializer.Serialize(i_Object, i_Type, i_Options);
    }

    /// <summary>
    /// Deserialize json into an object of type T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Json"></param>
    /// <returns></returns>
    public static T Deserialize<T>(string i_Json)
    {
      return JsonSerializer.Deserialize<T>(i_Json);
    }

    /// <summary>
    /// Deserialize json into an object of type T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Json"></param>
    /// <param name="i_Options"></param>
    /// <returns></returns>
    public static T DeserializeWithOptions<T>(string i_Json, JsonSerializerOptions i_Options)
    {
      return JsonSerializer.Deserialize<T>(i_Json, i_Options);
    }

    /// <summary>
    /// Deserialize json into an object of type T
    /// </summary>
    /// <param name="i_Json"></param>
    /// <param name="i_Type"></param>
    /// <param name="i_Options"></param>
    /// <returns></returns>
    public static object DeserializeWithOptions(string i_Json, Type i_Type, JsonSerializerOptions i_Options)
    {
      return JsonSerializer.Deserialize(i_Json, i_Type, i_Options);
    }

    /// <summary>
    /// Deserialize with given type
    /// </summary>
    /// <param name="i_Json"></param>
    /// <param name="i_Type"></param>
    /// <returns></returns>
    public static object Deserialize(string i_Json, Type i_Type)
    {
      return JsonSerializer.Deserialize(i_Json, i_Type);
    }
  }
}
