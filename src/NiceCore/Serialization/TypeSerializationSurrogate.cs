﻿using System;
using System.Runtime.Serialization;

namespace NiceCore.Serialization
{
  /// <summary>
  /// Surrogate for serializing Type
  /// This does NOT care about loaded assemblies. If you use this surrogate the assembly for the type must have been loaded beforehand
  /// </summary>
  public class TypeSerializationSurrogate : ISerializationSurrogate
  {
    /// <summary>
    /// String that indicates where type string is stored
    /// </summary>
    public static readonly string TYPE_STRING_FIELD = "TYPE_STRING";

    /// <summary>
    /// add object data to info
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="info"></param>
    /// <param name="context"></param>
    public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
    {
      if (obj is Type typeObj)
      {
        info.AddValue(TYPE_STRING_FIELD, typeObj.FullName);
      }
    }

    /// <summary>
    /// creates a type from stored typestring if found
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="info"></param>
    /// <param name="context"></param>
    /// <param name="selector"></param>
    /// <returns></returns>
    public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
    {
      var typeString = info.GetValue(TYPE_STRING_FIELD, typeof(string)) as string;
      return Type.GetType(typeString);
    }
  }
}
