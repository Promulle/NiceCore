﻿using System;

namespace NiceCore.Exceptions
{
  /// <summary>
  /// Static Exception helper class
  /// </summary>
  public static class NcExceptions
  {
    /// <summary>
    /// Throws a new Exception of T if i_Condition is true
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Condition"></param>
    public static void ThrowIf<T>(bool i_Condition) where T : Exception
    {
      if (i_Condition)
      {
        throw Activator.CreateInstance<T>();
      }
    }
  }
}
