using System;

namespace NiceCore.Exceptions
{
  /// <summary>
  /// Utils for exceptions
  /// </summary>
  public static class ExceptionUtils
  {
    /// <summary>
    /// Formats an exception into a readable string with more information than just .tostring
    /// </summary>
    /// <param name="ex"></param>
    /// <returns></returns>
    public static string FormatException(Exception ex)
    {
      var innerExceptionText = ex.InnerException != null ? FormatException(ex.InnerException) : "None"; 
      return $"""
                    Exception: {ex.GetType().FullName}
                    Message: {ex.Message}
                    StackTrace: {ex.StackTrace}
                    Method: {ex.TargetSite}
                    InnerException: {innerExceptionText}
              """;
    }
  }
}
