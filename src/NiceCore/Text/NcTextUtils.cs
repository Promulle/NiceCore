using System;

namespace NiceCore.Text
{
  /// <summary>
  /// utils for anything string/char
  /// </summary>
  public static class NcTextUtils
  {
    /// <summary>
    /// Transforms i_Input to be pascal case (this assumes i_Input is a word)
    /// </summary>
    /// <param name="i_Input"></param>
    /// <returns></returns>
    public static string SingleWordToPascalCase(string i_Input)
    {
      var start = i_Input[0];
      var rest  = i_Input[1..];
      return char.ToUpper(start) + rest;
    }

    /// <summary>
    /// Transforms i_Input to have its first character as lowercase (this assumes i_Input is a word)
    /// </summary>
    /// <param name="i_Input"></param>
    /// <returns></returns>
    public static string SingleWordLowerCaseFirstCharacter(string i_Input)
    {
      var start = i_Input[0];
      var rest  = i_Input[1..];
      return char.ToLower(start) + rest;
    }

    /// <summary>
    /// Removes the first appearance of i_Search from i_Text
    /// </summary>
    /// <param name="i_Text"></param>
    /// <param name="i_Search"></param>
    /// <returns></returns>
    public static string RemoveFirstAppearanceOf(string i_Text, string i_Search)
    {
      var text  = i_Text;
      var index = text.IndexOf(i_Search, StringComparison.Ordinal);
      if (index > -1)
      {
        text = text.Remove(index, i_Search.Length);
      }

      return text;
    }

    /// <summary>
    /// Removes the last appearance of i_Search from i_Text
    /// </summary>
    /// <param name="i_Text"></param>
    /// <param name="i_Search"></param>
    /// <returns></returns>
    public static string RemoveLastAppearanceOf(string i_Text, string i_Search)
    {
      var text  = i_Text;
      var index = text.LastIndexOf(i_Search, StringComparison.Ordinal);
      if (index > -1)
      {
        text = text.Remove(index, i_Search.Length);
      }

      return text;
    }

    /// <summary>
    /// SubstringWordAware
    /// </summary>
    /// <param name="i_Text"></param>
    /// <param name="i_Start"></param>
    /// <param name="i_Length"></param>
    /// <returns></returns>
    public static (string i_Part, int i_ActualEnd) SubstringWordAware(string i_Text, int i_Start, int i_Length)
    {
      if (i_Start == 0 && i_Length >= i_Text.Length)
      {
        return (i_Text, i_Text.Length - 1);
      }

      var remainingLength = i_Text.Length - i_Start;
      if (i_Length > remainingLength)
      {
        return (i_Text.Substring(i_Start, remainingLength), i_Text.Length - 1);
      }

      var lengthToTake = i_Length - 1;
      var endIndex = i_Start + lengthToTake;
      var end      = i_Text[endIndex];
      while (char.IsLetter(end))
      {
        endIndex -= 1;
        lengthToTake -= 1;
        end      =  i_Text[endIndex];
      }
      
      return (i_Text.Substring(i_Start, lengthToTake), endIndex);
    }
  }
}
