using System.Collections.Generic;

namespace NiceCore.Text.Html
{
  /// <summary>
  /// Utils for Handling Formatted HTML Text
  /// </summary>
  public static class FormattedHtmlUtils
  {
    /// <summary>
    /// Returns the actual index of i_FormattedIndex in i_MarkupText
    /// </summary>
    /// <param name="i_MarkupText"></param>
    /// <param name="i_FormattedIndex"></param>
    /// <param name="i_SpecialTagOffsets">Dictionary used to customize the length some tags are accounted for. Tag names are expected without tagStart and tagEnd. Also Closing-Tags and Opening-Tags are treated separately. </param>
    /// <returns></returns>
    public static int GetTrueIndex(string i_MarkupText, int i_FormattedIndex, IReadOnlyDictionary<string, int> i_SpecialTagOffsets)
    {
      return GetTrueIndex(i_FormattedIndex, HTMLTagOffsetUtils.GetOffsets(i_MarkupText, i_SpecialTagOffsets));
    }
    
    /// <summary>
    /// Returns the actual index of i_FormattedIndex in i_MarkupText
    /// </summary>
    /// <param name="i_FormattedIndex"></param>
    /// <param name="i_Offsets"></param>
    /// <returns></returns>
    public static int GetTrueIndex(int i_FormattedIndex, List<HTMLTagOffset> i_Offsets)
    {
      var trueIndex = i_FormattedIndex;
      var cont = true;
      var offsetIndex = 0;
      while (cont)
      {
        var offsetObject = i_Offsets[offsetIndex];
        if (offsetObject.Start < trueIndex || offsetIndex == 0)
        {
          if (offsetObject.Type == HTMLTagOffsetType.Tag)
          {
            trueIndex += offsetObject.CalculationOffset;
          }
          else if (offsetObject.Type == HTMLTagOffsetType.EscapedCharacter)
          {
            trueIndex += offsetObject.Tag.Length - 1;
          }
        }
        else
        {
          cont = false;
        }
        
        offsetIndex++;
        if (offsetIndex == i_Offsets.Count)
        {
          cont = false;
        }
      }
      return trueIndex;
    }
  }
}