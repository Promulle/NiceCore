namespace NiceCore.Text.Html
{
  /// <summary>
  /// Object that abstracts a tag/escaped character in HTML Markup text
  /// </summary>
  public class HTMLTagOffset
  {
    /// <summary>
    /// HTML Tag
    /// </summary>
    public string Tag { get; set; }
    
    /// <summary>
    /// Start index of tag
    /// </summary>
    public int Start { get; set; }
    
    /// <summary>
    /// offset to use for calculation when using this tag
    /// </summary>
    public int CalculationOffset { get; set; }
    
    /// <summary>
    /// Type of TagOffsetObject
    /// </summary>
    public HTMLTagOffsetType Type { get; set; }
  }
}