using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace NiceCore.Text.Html
{
  /// <summary>
  /// Utils for search for tag offsets
  /// </summary>
  public static class HTMLTagOffsetUtils
  {
    /// <summary>
    /// Searches Tag offsets
    /// </summary>
    /// <param name="i_Text"></param>
    /// <param name="i_SpecialTagOffsets"></param>
    /// <returns></returns>
    public static List<HTMLTagOffset> GetOffsets(string i_Text, IReadOnlyDictionary<string, int> i_SpecialTagOffsets)
    {
      var res = new List<HTMLTagOffset>();
      foreach (var matchObj in HTMLTagRegex.Regex.Matches(i_Text))
      {
        if (matchObj is Match match)
        {
          res.Add(new()
          {
            Start = match.Index,
            Type = GetMatchType(match),
            CalculationOffset = GetCalculationOffset(match, i_SpecialTagOffsets),
            Tag = match.Value
          });
        }
      }

      return res;
    }

    private static int GetCalculationOffset(Match i_Match, IReadOnlyDictionary<string, int> i_SpecialTagOffsets)
    {
      if (i_Match.Groups.ContainsKey(HTMLTagRegex.MATCH_GROUP_NAME_TAG_NAME) && i_Match.Groups.TryGetValue(HTMLTagRegex.MATCH_GROUP_NAME_TAG_NAME, out var group))
      {
        if (group.Value.Length > 0 && i_SpecialTagOffsets.TryGetValue(group.Value, out var offset))
        {
          return i_Match.Value.Length + offset;
        }
      }
      return i_Match.Value.Length;
    }
    
    private static HTMLTagOffsetType GetMatchType(Match i_Match)
    {
      if (i_Match.Groups.ContainsKey(HTMLTagRegex.MATCH_GROUP_NAME_ESCAPED) && i_Match.Groups[HTMLTagRegex.MATCH_GROUP_NAME_ESCAPED].Length > 0)
      {
        return HTMLTagOffsetType.EscapedCharacter;
      }

      return HTMLTagOffsetType.Tag;
    }
  }
}