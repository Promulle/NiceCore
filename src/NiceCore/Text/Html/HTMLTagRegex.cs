using System.Text.RegularExpressions;

namespace NiceCore.Text.Html
{
  /// <summary>
  /// HTML Tag Regex
  /// </summary>
  public static partial class HTMLTagRegex
  {
    /// <summary>
    /// Match Group Name Escaped
    /// </summary>
    public const string MATCH_GROUP_NAME_ESCAPED = "escaped";
    
    /// <summary>
    /// Match Group Tag Name
    /// </summary>
    public const string MATCH_GROUP_NAME_TAG_NAME = "tagName";
    
    /// <summary>
    /// Regex
    /// </summary>
    public static Regex Regex { get; } = HtmlRegex();
    
    [GeneratedRegex(@$"<(?'{MATCH_GROUP_NAME_TAG_NAME}'\/*\w+)[^>]*>|(?'{MATCH_GROUP_NAME_ESCAPED}'&[^;]*;)")]
    private static partial Regex HtmlRegex();
  }
}