namespace NiceCore.Text.Html
{
  /// <summary>
  /// Possible Types of HTMLTagOffset Objects
  /// </summary>
  public enum HTMLTagOffsetType
  {
    /// <summary>
    /// HTML Tag 
    /// </summary>
    Tag = 0,
    
    /// <summary>
    /// Escaped Character &lt; for example
    /// </summary>
    EscapedCharacter = 1
  }
}
