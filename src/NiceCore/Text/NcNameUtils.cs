// Copyright 2021 Sycorax Systemhaus GmbH

using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace NiceCore.Text
{
  /// <summary>
  /// Name Utils
  /// </summary>
  public static partial class NcNameUtils
  {
    /// <summary>
    /// Get Next Not Repeating Name
    /// </summary>
    /// <param name="i_BaseName"></param>
    /// <param name="i_Existing"></param>
    /// <param name="i_AppendOnZero"></param>
    /// <returns></returns>
    public static string GetNextNotRepeatingName(string i_BaseName, IReadOnlyCollection<string> i_Existing, bool i_AppendOnZero)
    {
      if (i_Existing == null || i_Existing.Count == 0)
      {
        return GetNameForNumber(i_BaseName, 0, i_AppendOnZero);
      }

      var regex = new Regex(i_BaseName + "\\s*\\((\\d*)\\)");
      var actualFound = false;
      var highest = int.MinValue;
      foreach (var existingName in i_Existing)
      {
        if (existingName != null)
        {
          if (existingName.Equals(i_BaseName))
          {
            actualFound = true;
            if (highest == int.MinValue)
            {
              highest = 0;  
            }
          }
          else
          {
            var match = regex.Match(existingName);
            if (match.Success && match.Groups.Count > 1 && int.TryParse(match.Groups[1].ValueSpan, out var capturedNumber))
            {
              if (highest < capturedNumber)
              {
                highest = capturedNumber;
              }
            }  
          }
        }
      }

      if (!actualFound)
      {
        return i_BaseName;
      }
      var actualNumber = highest != int.MinValue ? highest + 1 : 0;
      return GetNameForNumber(i_BaseName, actualNumber, i_AppendOnZero);
    }

    private static string GetNameForNumber(string i_BaseName, int i_NumberToAppend, bool i_AppendOnZero)
    {
      if (i_NumberToAppend == 0 && !i_AppendOnZero)
      {
        return i_BaseName;
      }
      return i_BaseName + " (" + i_NumberToAppend + ")";
    }
  }
}