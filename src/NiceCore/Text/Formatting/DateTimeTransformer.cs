using System;
using System.Globalization;

namespace NiceCore.Text.Formatting
{
  /// <summary>
  /// small wrapper for formatting DateTime
  /// Inspired by Angular Datepipe
  /// </summary>
  public sealed class DateTimeTransformer
  {
    private const string DEFAULT_LOCALE = "en-US";
    private readonly CultureInfo m_CultureInfo;

    private DateTimeTransformer(CultureInfo i_CultureInfo)
    {
      m_CultureInfo = i_CultureInfo;
    }

    /// <summary>
    /// Short
    /// </summary>
    /// <param name="i_Time"></param>
    /// <returns></returns>
    public string Short(DateTime i_Time)
    {
      return Format(i_Time, "g");
    }

    /// <summary>
    /// Medium Format
    /// </summary>
    /// <param name="i_Time"></param>
    /// <returns></returns>
    public string Medium(DateTime i_Time)
    {
      return Format(i_Time, "G");
    }

    /// <summary>
    /// Full
    /// </summary>
    /// <param name="i_Time"></param>
    /// <returns></returns>
    public string Full(DateTime i_Time)
    {
      return Format(i_Time, "U");
    }

    /// <summary>
    /// Short Date
    /// </summary>
    /// <param name="i_Time"></param>
    /// <returns></returns>
    public string ShortDate(DateTime i_Time)
    {
      return Format(i_Time, "d");
    }

    /// <summary>
    /// Medium Date
    /// </summary>
    /// <param name="i_Time"></param>
    /// <returns></returns>
    public string FullDate(DateTime i_Time)
    {
      return Format(i_Time, "D");
    }

    /// <summary>
    /// Short Time
    /// </summary>
    /// <param name="i_Time"></param>
    /// <returns></returns>
    public string ShortTime(DateTime i_Time)
    {
      return Format(i_Time, "t");
    }

    /// <summary>
    /// Medium Time
    /// </summary>
    /// <param name="i_Time"></param>
    /// <returns></returns>
    public string MediumTime(DateTime i_Time)
    {
      return Format(i_Time, "T");
    }

    /// <summary>
    /// Format
    /// </summary>
    /// <param name="i_Time"></param>
    /// <param name="i_Format"></param>
    /// <returns></returns>
    public string Format(DateTime i_Time, string i_Format)
    {
      return i_Time.ToString(i_Format, m_CultureInfo);
    }
    
    /// <summary>
    /// Create a new DateTimeTransformer for i_Locale with a fallback locale of 'en'
    /// </summary>
    /// <param name="i_Locale"></param>
    /// <returns></returns>
    public static DateTimeTransformer Create(string i_Locale)
    {
      try
      {
        var localeToUse = i_Locale ?? DEFAULT_LOCALE;
        var cultureInfo = new CultureInfo(localeToUse);
        return new(cultureInfo);
      }
      catch (CultureNotFoundException )
      {
        var cultureInfo = new CultureInfo(DEFAULT_LOCALE);
        return new(cultureInfo);
      }
    }
  }
}