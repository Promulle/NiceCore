﻿using System;
using System.IO;
using Microsoft.Extensions.Logging;

namespace NiceCore.Diagnostics.Logging.Providers
{
  /// <summary>
  /// Simple file logger for diagnostics
  /// </summary>
  public sealed class DiagnosticFileLogger : ILogger
  {
    private readonly string m_FileName;
    
    /// <summary>
    /// Ctor setting file name
    /// </summary>
    /// <param name="i_FileName"></param>
    public DiagnosticFileLogger(string i_FileName)
    {
      m_FileName = i_FileName;
    }
    
    /// <summary>
    /// Log
    /// </summary>
    /// <param name="logLevel"></param>
    /// <param name="eventId"></param>
    /// <param name="state"></param>
    /// <param name="exception"></param>
    /// <param name="formatter"></param>
    /// <typeparam name="TState"></typeparam>
    public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
    {
      if (!IsEnabled(logLevel))
      {
        return;
      }

      try
      {
        File.AppendAllText(m_FileName, formatter.Invoke(state, exception));
      }
      catch (Exception e)
      {
        Console.WriteLine($"Diagnostic FileLogger failed with exception: {e}");
      }
    }

    /// <summary>
    /// Is Enabled
    /// </summary>
    /// <param name="logLevel"></param>
    /// <returns></returns>
    public bool IsEnabled(LogLevel logLevel)
    {
      if (logLevel == LogLevel.None)
      {
        return false;
      }

      return true;
    }

    /// <summary>
    /// Begin Scope
    /// </summary>
    /// <param name="state"></param>
    /// <typeparam name="TState"></typeparam>
    /// <returns></returns>
    public IDisposable BeginScope<TState>(TState state)
      where TState : notnull
    {
      return null;
    }
  }
}