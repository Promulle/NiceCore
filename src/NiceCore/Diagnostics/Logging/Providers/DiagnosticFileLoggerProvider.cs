﻿using System.Collections.Concurrent;
using Microsoft.Extensions.Logging;

namespace NiceCore.Diagnostics.Logging.Providers
{
  /// <summary>
  /// File Logger Provider
  /// </summary>
  [ProviderAlias("DiagnosticFile")]
  public class DiagnosticFileLoggerProvider : ILoggerProvider
  {
    private readonly string m_FileName;
    private readonly ConcurrentDictionary<string, ILogger> m_Loggers = new();

    /// <summary>
    /// Ctor setting file name
    /// </summary>
    /// <param name="i_FileName"></param>
    public DiagnosticFileLoggerProvider(string i_FileName)
    {
      m_FileName = i_FileName;
    }
    
    /// <summary>
    /// Clear Loggers on dispose
    /// </summary>
    public void Dispose()
    {
      m_Loggers.Clear();
    }

    /// <summary>
    /// Create logger
    /// </summary>
    /// <param name="categoryName"></param>
    /// <returns></returns>
    public ILogger CreateLogger(string categoryName)
    {
      return m_Loggers.GetOrAdd(categoryName, static (_, fileName) => new DiagnosticFileLogger(fileName), m_FileName);
    }
  }
}