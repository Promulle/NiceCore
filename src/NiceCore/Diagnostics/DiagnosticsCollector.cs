﻿using System;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using NiceCore.Diagnostics.Logging.Providers;

namespace NiceCore.Diagnostics
{
  /// <summary>
  /// Diagnostivs Collector
  /// </summary>
  public sealed class DiagnosticsCollector
  {
    private IConfigurationRoot m_Configuration;
    private ILoggerFactory m_Factory;
    
    /// <summary>
    /// Static instance to be used
    /// </summary>
    public static readonly DiagnosticsCollector Instance = new();

    /// <summary>
    /// Configure
    /// </summary>
    public void Configure()
    {
      ConfigureBy(GetDefaultConfiguration());
    }

    /// <summary>
    /// Get Logger
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public ILogger<T> GetLogger<T>()
    {
      //if the factory is null -> not a diagnose run -> dont return a logger
      if (m_Factory == null)
      {
        return NullLogger<T>.Instance;
      }
      //this is a diagnose run -> return a logger
      return m_Factory.CreateLogger<T>();
    }

    /// <summary>
    /// Configures the Diagnostics Collector
    /// </summary>
    public void ConfigureBy(DiagnosticCollectorConfiguration i_Config)
    {
      var args = Environment.GetCommandLineArgs();
      if (args.Contains(i_Config.Command))
      {
        m_Configuration = new ConfigurationBuilder()
          .AddJsonFile(i_Config.JsonConfigFile)
          .AddEnvironmentVariables(i_Config.EnvironmentVariablePrefix)
          .AddCommandLine(Environment.GetCommandLineArgs())
          .Build();
        m_Factory = LoggerFactory.Create(builder =>
        {
          builder.AddConfiguration(m_Configuration.GetSection(i_Config.JsonConfigLoggingSection));
          builder.AddSimpleConsole();
          AddDiagnosticFileLoggerProvider(m_Configuration, i_Config, builder);
        });
      }
    }

    private static void AddDiagnosticFileLoggerProvider(IConfiguration i_Configuration, DiagnosticCollectorConfiguration i_Config, ILoggingBuilder t_Builder)
    {
      var fileName = i_Configuration.GetValue(i_Config.FileLoggerFileNameConfigurationKey, i_Config.FileLoggerFileNameFallback);
      var descriptor = ServiceDescriptor.Singleton<ILoggerProvider>(new DiagnosticFileLoggerProvider(fileName));
      t_Builder.Services.TryAddEnumerable(descriptor);
    }

    private static DiagnosticCollectorConfiguration GetDefaultConfiguration()
    {
      return new DiagnosticCollectorConfiguration()
      {
        Command = "--diagnose",
        EnvironmentVariablePrefix = "Nc",
        JsonConfigFile = "NcDiagnostic.json",
        JsonConfigLoggingSection = "Logging",
        FileLoggerFileNameConfigurationKey = "Logging:DiagnosticFileLogger:FileName",
        FileLoggerFileNameFallback = "NcDiagnostics.log"
      };
    }
  }
}