﻿namespace NiceCore.Diagnostics
{
  /// <summary>
  /// Configuration for Collectors
  /// </summary>
  public struct DiagnosticCollectorConfiguration
  {
    /// <summary>
    /// Command
    /// </summary>
    public string Command { get; init; }
    
    /// <summary>
    /// Prefix for Environment variables
    /// </summary>
    public string EnvironmentVariablePrefix { get; init; }
    
    /// <summary>
    /// Json Config File
    /// </summary>
    public string JsonConfigFile { get; init; }
    
    /// <summary>
    /// Section in JsonConfigFile used for logging
    /// </summary>
    public string JsonConfigLoggingSection { get; init; }
    
    /// <summary>
    /// Key to be used for setting that configures which file to use for file logger
    /// </summary>
    public string FileLoggerFileNameConfigurationKey { get; init; }
    
    /// <summary>
    /// Default File Logger file name if none is configured
    /// </summary>
    public string FileLoggerFileNameFallback { get; init; }
  }
}