using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Base;

namespace NiceCore.Timers
{
  /// <summary>
  /// Timer that runs the callback exactly one time after the given interval passed and then cleans itself up
  /// This implements IDisposable BUT disposing is optional since the completion of a callback also signals the timer to cleanup
  /// </summary>
  /// <typeparam name="TState"></typeparam>
  public class OneTimeTimer<TState> : BaseDisposable
  {
    private readonly PeriodicTimer m_Timer;
    private readonly Func<TState, CancellationToken, ValueTask> m_Callback;
    private readonly TState m_State;
    private readonly ILogger m_Logger;
    private bool m_Disposed = false;

    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_Interval"></param>
    /// <param name="i_Callback"></param>
    /// <param name="i_State"></param>
    /// <param name="i_Logger"></param>
    public OneTimeTimer(TimeSpan i_Interval, Func<TState, CancellationToken, ValueTask> i_Callback, TState i_State, ILogger i_Logger)
    {
      m_Timer = new(i_Interval);
      m_Callback = i_Callback;
      m_State = i_State;
      m_Logger = i_Logger;
    }

    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_Interval"></param>
    /// <param name="i_Callback"></param>
    /// <param name="i_State"></param>
    public OneTimeTimer(TimeSpan i_Interval, Func<TState, CancellationToken, ValueTask> i_Callback, TState i_State) : this(i_Interval, i_Callback, i_State, null)
    {
    }

    /// <summary>
    /// Runs the timer
    /// </summary>
    /// <param name="i_Token"></param>
    public async Task Run(CancellationToken i_Token)
    {
      await m_Timer.WaitForNextTickAsync(i_Token).ConfigureAwait(false);
      //if the timer exits waitForNextTickAsync because dispose was called we dont run the callback
      if (!m_Disposed)
      {
        try
        {
          await m_Callback.Invoke(m_State, i_Token).ConfigureAwait(false);
        }
        catch (Exception e)
        {
          m_Logger?.LogError(e, "An Error occurred during execution of callback");
        }

        m_Timer.Dispose();
      }
    }

    /// <summary>
    /// Dispose
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    protected override void Dispose(bool i_IsDisposing)
    {
      m_Disposed = true;
      m_Timer?.Dispose();
    }
  }
}