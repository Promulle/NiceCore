// Copyright 2021 Sycorax Systemhaus GmbH

using System;
using System.Threading;
using System.Threading.Tasks;
using NiceCore.Base;

namespace NiceCore.Timers
{
  /// <summary>
  /// Timer that uses the periodic timer to call callbacks only if timer does not need to be reset
  /// </summary>
  /// <typeparam name="TState"></typeparam>
  /// <typeparam name="TErrorHandlerState"></typeparam>
  public sealed class RefreshablePeriodicTimer<TState, TErrorHandlerState> : BaseDisposable
  {
    private readonly Func<TState, CancellationToken, ValueTask> m_Callback;
    private readonly Action<Exception, TErrorHandlerState> m_ErrorHandler;
    private readonly TState                                m_State;
    private readonly TErrorHandlerState                    m_ErrorHandlerState;
    private readonly CancellationToken                     m_Token;
    private readonly PeriodicTimer                         m_Timer;
    private          bool                                  m_ShouldRefresh = false;

    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_Callback"></param>
    /// <param name="i_State"></param>
    /// <param name="i_TimeSpan"></param>
    /// <param name="i_Token"></param>
    /// <param name="i_ErrorHandler"></param>
    /// <param name="i_HandlerState"></param>
    public RefreshablePeriodicTimer(Func<TState, CancellationToken, ValueTask> i_Callback, TState i_State, TimeSpan i_TimeSpan, CancellationToken i_Token, Action<Exception, TErrorHandlerState> i_ErrorHandler, TErrorHandlerState i_HandlerState)
    {
      m_Callback          = i_Callback;
      m_State             = i_State;
      m_Token             = i_Token;
      m_Timer             = new(i_TimeSpan);
      m_ErrorHandler      = i_ErrorHandler;
      m_ErrorHandlerState = i_HandlerState;
    }

    /// <summary>
    /// Start the timer loop
    /// </summary>
    public async Task Start()
    {
      try
      {
        while (await m_Timer.WaitForNextTickAsync(m_Token) && !m_Token.IsCancellationRequested)
        {
          if (m_ShouldRefresh)
          {
            m_ShouldRefresh = false;
            continue;
          }

          await m_Callback.Invoke(m_State, m_Token).ConfigureAwait(false);
        }
      }
      catch (Exception ex)
      {
        m_ErrorHandler?.Invoke(ex, m_ErrorHandlerState);
      }
    }

    /// <summary>
    /// Refresh
    /// </summary>
    public void Refresh()
    {
      m_ShouldRefresh = true;
    }

    /// <summary>
    /// Dispose the internal timer
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    protected override void Dispose(bool i_IsDisposing)
    {
      m_Timer?.Dispose();
      base.Dispose(i_IsDisposing);
    }
  }
}
