using System;

namespace NiceCore.Infrastructure
{
  /// <summary>
  /// Documentation Attribute
  /// Attribute that signals that the applied field/method is used by the given Expression Tree generating class
  /// </summary>
  [AttributeUsage(AttributeTargets.Field | AttributeTargets.Method | AttributeTargets.Property, Inherited = true, AllowMultiple = true)]
  public class UsedByExpressionTreeAttribute : Attribute
  {
    /// <summary>
    /// Class that uses the method/field
    /// </summary>
    public string CallingClass { get; }
    
    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_CallingClass">Class that is using the target method/field/property</param>
    public UsedByExpressionTreeAttribute(string i_CallingClass)
    {
      CallingClass = i_CallingClass;
    }
  }
}