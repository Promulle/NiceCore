using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using NiceCore.Extensions;
using NiceCore.Filtering;
using NiceCore.Reflection;
using NiceCore.Text;

namespace NiceCore.Expressions
{
  /// <summary>
  /// Utils for Linq.Expressions (Expression Tree Building)
  /// </summary>
  public static class ExpressionUtils
  {
    /// <summary>
    /// Returns the Task{T} type for i_GenericParameter as T
    /// </summary>
    /// <param name="i_GenericParameter"></param>
    /// <returns></returns>
    public static Type GetGenericTaskType(Type i_GenericParameter)
    {
      return typeof(Task<>).MakeGenericType(i_GenericParameter);
    }

    /// <summary>
    /// Returns the OR-Combinations of all of i_Expressions
    /// </summary>
    /// <param name="i_Expressions"></param>
    /// <returns></returns>
    public static Expression OrElseAllExpressions(params Expression[] i_Expressions)
    {
      return OrElseEverything(i_Expressions);
    }
    
    /// <summary>
    /// Returns the OR-Combinations of all of i_Expressions
    /// </summary>
    /// <param name="i_Expressions"></param>
    /// <returns></returns>
    public static Expression OrElseEverything(IReadOnlyCollection<Expression> i_Expressions)
    {
      return LinkExpressions((left, right) => Expression.OrElse(left, right), i_Expressions);
    }

    /// <summary>
    /// Returns the OR-Combinations of all of i_Expressions
    /// </summary>
    /// <param name="i_Expressions"></param>
    /// <returns></returns>
    public static Expression AndAlsoAllExpressions(params Expression[] i_Expressions)
    {
      return AndAlsoEverything(i_Expressions);
    }
    
    /// <summary>
    /// Returns the OR-Combinations of all of i_Expressions
    /// </summary>
    /// <param name="i_Expressions"></param>
    /// <returns></returns>
    public static Expression AndAlsoEverything(IReadOnlyCollection<Expression> i_Expressions)
    {
      return LinkExpressions((left, right) => Expression.AndAlso(left, right), i_Expressions);
    }

    private static Expression LinkExpressions(Func<Expression, Expression, Expression> i_LinkFunc, IReadOnlyCollection<Expression> i_Expressions)
    {
      if (i_Expressions.Count == 0)
      {
        throw new ArgumentException("Can not link-everything an empty array");
      }
      if (i_Expressions.Count == 1)
      {
        return i_Expressions.ElementAt(0);
      }
      var expr = i_Expressions.ElementAt(0);
      for (var i = 1; i < i_Expressions.Count; i++)
      {
        expr = i_LinkFunc.Invoke(expr, i_Expressions.ElementAt(i));
      }
      return expr;
    }
    
    /// <summary>
    /// Resolves a property expression that might contain one or more dots.
    /// TODO: replace i_Options with a proper string-format translator
    /// </summary>
    /// <param name="i_InstanceParameterExpression"></param>
    /// <param name="i_PropertyPath"></param>
    /// <param name="i_Options"></param>
    /// <returns></returns>
    public static MemberExpression GetPropertyExpression(Expression i_InstanceParameterExpression, string i_PropertyPath, NcFilterTranslationOptions i_Options)
    {
      if (i_PropertyPath.Contains('.'))
      {
        var parts        = i_PropertyPath.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
        return GetPropertyExpression(i_InstanceParameterExpression, parts, i_Options);
      }
      return Expression.Property(i_InstanceParameterExpression, i_PropertyPath);
    }

    /// <summary>
    /// Resolves a property expression that might contain one or more dots.
    /// TODO: replace i_Options with a proper string-format translator
    /// </summary>
    /// <param name="i_InstanceParameterExpression"></param>
    /// <param name="i_PropertyParts"></param>
    /// <param name="i_Options"></param>
    /// <returns></returns>
    public static MemberExpression GetPropertyExpression(Expression i_InstanceParameterExpression, string[] i_PropertyParts, NcFilterTranslationOptions i_Options)
    {
      var propertyName = GetCorrectPropertyName(i_PropertyParts[0], i_Options);
      var propertyExpr = Expression.Property(i_InstanceParameterExpression, propertyName);
      if (i_PropertyParts.Length > 1)
      {
        for (var i = 1; i < i_PropertyParts.Length; i++)
        {
          propertyExpr = Expression.Property(propertyExpr, i_PropertyParts[i]);
        }
      }
      return propertyExpr;
    }

    private static string GetCorrectPropertyName(string i_Name, NcFilterTranslationOptions i_Options)
    {
      if (string.IsNullOrEmpty(i_Name) || string.IsNullOrWhiteSpace(i_Name))
      {
        throw new ArgumentException("Name was passed as null or whitespace. Can not get property for that.");
      }
      if (i_Options.TreatIdentifiersAsPascalCase)
      {
        return NcTextUtils.SingleWordToPascalCase(i_Name);
      }
      return i_Name;
    }
  }
}
