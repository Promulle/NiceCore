using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using NiceCore.Expressions.Exceptions;
using NiceCore.Extensions;
using NiceCore.Reflection;

namespace NiceCore.Expressions
{
  /// <summary>
  /// Expression Utils that specialize on calling methods on expressions
  /// </summary>
  public static partial class MethodCallExpressionUtils
  {
    /// <summary>
    /// Get Method
    /// </summary>
    /// <param name="i_Type"></param>
    /// <param name="i_MethodName"></param>
    /// <param name="i_ParameterCount"></param>
    /// <param name="i_GenericParameterCount"></param>
    /// <param name="i_ParameterTypes"></param>
    /// <returns></returns>
    public static MethodInfo GetMethodForExpression(Type i_Type, string i_MethodName, int i_ParameterCount, int i_GenericParameterCount, IReadOnlyCollection<MethodParameterType> i_ParameterTypes)
    {
      var methodInfo = ReflectionUtils.GetMethodEx(i_Type, i_MethodName, i_ParameterCount, i_GenericParameterCount, i_ParameterTypes);
      if (methodInfo == null)
      {
        throw new MethodNotCorrectlyFoundException(i_Type.Name, i_MethodName, i_ParameterCount, i_GenericParameterCount, i_ParameterTypes);
      }

      return methodInfo;
    }
    
    /// <summary>
    /// Calls <see cref="ExpressionTreeRelatedExtensions.AwaitToObject{T}"/> on the given Task Expression with i_TaskGenericParameter as {T}
    /// </summary>
    /// <param name="i_TargetExpression">The given Task</param>
    /// <param name="i_TaskGenericParameter"></param>
    /// <returns></returns>
    public static Expression CallAwaitToObjectOnTask(Expression i_TargetExpression, Type i_TaskGenericParameter)
    {
      var method = GetAwaitToObjectMethod(i_TaskGenericParameter);
      return Expression.Call(null, method, i_TargetExpression);
    }
    
    private static MethodInfo GetAwaitToObjectMethod(Type i_GenericParameter)
    {
      var types = new List<MethodParameterType>()
      {
        MethodParameterType.Of(typeof(Task<>), new()
        {
          MethodParameterType.GenericPlaceHolder()
        })
      };
      var method = ReflectionUtils.GetMethodEx(typeof(ExpressionTreeRelatedExtensions), nameof(ExpressionTreeRelatedExtensions.AwaitToObject), 1, 1, types);
      if (method == null)
      {
        throw new InvalidOperationException($"Could not find method {nameof(ExpressionTreeRelatedExtensions.AwaitToObject)} on {nameof(ExpressionTreeRelatedExtensions)}.");
      }

      return method.MakeGenericMethod(i_GenericParameter);
    }
  }
}
