using System;
using System.Collections.Generic;
using System.Text;
using NiceCore.Extensions;
using NiceCore.Reflection;

namespace NiceCore.Expressions.Exceptions
{
  /// <summary>
  /// MethodNotCorrectlyFoundException
  /// </summary>
  public class MethodNotCorrectlyFoundException : Exception
  {
    /// <summary>
    /// Type Name
    /// </summary>
    public string TypeName { get; }

    /// <summary>
    /// Method Name
    /// </summary>
    public string MethodName { get; }

    /// <summary>
    /// UsedParameterCount
    /// </summary>
    public int UsedParameterCount { get; }

    /// <summary>
    /// UsedGenericCount
    /// </summary>
    public int UsedGenericCount { get; }

    /// <summary>
    /// UsedParameterTypes
    /// </summary>
    public IReadOnlyCollection<MethodParameterType> UsedParameterTypes { get; }

    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_TypeName"></param>
    /// <param name="i_MethodName"></param>
    /// <param name="i_ParameterCount"></param>
    /// <param name="i_GenericCount"></param>
    /// <param name="i_Types"></param>
    public MethodNotCorrectlyFoundException(string i_TypeName, string i_MethodName, int i_ParameterCount, int i_GenericCount, IReadOnlyCollection<MethodParameterType> i_Types) : base(GetMessage(i_TypeName, i_MethodName, i_ParameterCount, i_GenericCount, i_Types))
    {
      TypeName = i_TypeName;
      MethodName = i_MethodName;
      UsedParameterCount = i_ParameterCount;
      UsedGenericCount = i_GenericCount;
      UsedParameterTypes = i_Types;
    }

    private static string GetMessage(string i_TypeName, string i_MethodName, int i_ParameterCount, int i_GenericCount, IReadOnlyCollection<MethodParameterType> i_Types)
    {
      var strBuilder = new StringBuilder($"Method {i_MethodName} was not correctly found on type {i_TypeName}. Either search was too imprecise or something else didnt match. Used Parameters: ");
      strBuilder.Append("Parameter Count: ")
        .Append(i_ParameterCount)
        .Append(" Generic Parameter Count: ")
        .Append(i_GenericCount)
        .Append(" With Method Parameters: ");
      for(var i = 0; i < i_Types.Count; i++)
      {
        var type = i_Types.ElementAtEx(i);
        strBuilder.Append(type);
        if (i < i_Types.Count - 1)
        {
          strBuilder.Append(", ");
        }
      }

      strBuilder.Append('.');
      return strBuilder.ToString();
    }
  }
}