using System;
using System.Linq.Expressions;

namespace NiceCore.Expressions
{
  /// <summary>
  /// Class that groups a query with an expression. Makes developing extensive expression Trees
  /// with IQueryables easier
  /// </summary>
  public sealed class QueryExpression
  {
    /// <summary>
    /// Expression that is the current Query
    /// </summary>
    public Expression RawQueryExpression { get; init; }
    
    /// <summary>
    /// Type of the Query. The T in IQueryable of T
    /// </summary>
    public Type QueryParameterType { get; init; }
    
    private QueryExpression()
    {
      
    }

    /// <summary>
    /// Builds a new instance from this queryExpression
    /// </summary>
    /// <param name="i_NewExpression"></param>
    /// <returns></returns>
    public QueryExpression Transformed(Expression i_NewExpression)
    {
      return new()
      {
        QueryParameterType = QueryParameterType,
        RawQueryExpression = i_NewExpression
      };
    }

    /// <summary>
    /// Creates a new QueryExpression from i_QueryExpression and given parameterType.
    /// </summary>
    /// <param name="i_QueryExpression"></param>
    /// <param name="i_ParameterType"></param>
    /// <returns></returns>
    public static QueryExpression From(Expression i_QueryExpression, Type i_ParameterType)
    {
      return new()
      {
        RawQueryExpression = i_QueryExpression,
        QueryParameterType = i_ParameterType
      };
    }
  }
}
