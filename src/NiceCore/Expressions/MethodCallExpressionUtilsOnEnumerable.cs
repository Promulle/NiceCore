using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using NiceCore.Filtering;
using NiceCore.Reflection;

namespace NiceCore.Expressions
{
  public static partial class MethodCallExpressionUtils
  {
    /// <summary>
    /// Method Call Utils for calls to methods on Enumerables
    /// </summary>
    public static class OnEnumerable
    {
      /// <summary>
      /// Ordering
      /// </summary>
      public static class Ordering
      {
        /// <summary>
        /// GetCallToOrderByExpression
        /// </summary>
        /// <param name="i_Target"></param>
        /// <param name="i_ElementType"></param>
        /// <param name="i_OrderByKey"></param>
        /// <returns></returns>
        public static MethodCallExpression GetCallToOrderByDescendingExpression(Expression i_Target, Type i_ElementType, string i_OrderByKey)
        {
          return GetCallToOrderMethodExpression(typeof(IEnumerable<>), nameof(Enumerable.OrderByDescending), i_Target, i_ElementType, i_OrderByKey);
        }

        /// <summary>
        /// GetCallToOrderByExpression
        /// </summary>
        /// <param name="i_Target"></param>
        /// <param name="i_ElementType"></param>
        /// <param name="i_OrderByKey"></param>
        /// <returns></returns>
        public static MethodCallExpression GetCallToOrderByExpression(Expression i_Target, Type i_ElementType, string i_OrderByKey)
        {
          return GetCallToOrderMethodExpression(typeof(IEnumerable<>), nameof(Enumerable.OrderBy), i_Target, i_ElementType, i_OrderByKey);
        }
        
        /// <summary>
        /// GetCallToOrderByExpression
        /// </summary>
        /// <param name="i_Target"></param>
        /// <param name="i_ElementType"></param>
        /// <param name="i_OrderByKey"></param>
        /// <returns></returns>
        public static MethodCallExpression GetCallToThenByDescendingExpression(Expression i_Target, Type i_ElementType, string i_OrderByKey)
        {
          return GetCallToOrderMethodExpression(typeof(IOrderedEnumerable<>), nameof(Enumerable.ThenByDescending), i_Target, i_ElementType, i_OrderByKey);
        }

        /// <summary>
        /// GetCallToOrderByExpression
        /// </summary>
        /// <param name="i_Target"></param>
        /// <param name="i_ElementType"></param>
        /// <param name="i_OrderByKey"></param>
        /// <returns></returns>
        public static MethodCallExpression GetCallToThenByExpression(Expression i_Target, Type i_ElementType, string i_OrderByKey)
        {
          return GetCallToOrderMethodExpression(typeof(IOrderedEnumerable<>), nameof(Enumerable.ThenBy), i_Target, i_ElementType, i_OrderByKey);
        }

        private static MethodCallExpression GetCallToOrderMethodExpression(Type i_EnumerableType, string i_MethodName, Expression i_Target, Type i_ElementType, string i_OrderByKey)
        {
          var orderKeyType = ReflectionUtils.GetTypeOfLastPropertyInPath(i_OrderByKey, i_ElementType);
          var orderByLambdaParameterExpression = Expression.Parameter(i_ElementType, "orderByParameter");
          var propertyExpression = ExpressionUtils.GetPropertyExpression(orderByLambdaParameterExpression, i_OrderByKey, NcFilterTranslationOptions.EMPTY);
          var lambda = Expression.Lambda(propertyExpression, orderByLambdaParameterExpression);
          var orderByMethod = GetEnumerableOrderMethod(i_EnumerableType, i_MethodName);
          var genericOrderByMethod = orderByMethod.MakeGenericMethod(i_ElementType, orderKeyType);
          return Expression.Call(null, genericOrderByMethod, i_Target, lambda);
        }

        private static MethodInfo GetEnumerableOrderMethod(Type i_EnumerableType, string i_MethodName)
        {
          var parameters = new[]
          {
            MethodParameterType.Of(i_EnumerableType, new List<MethodParameterType>()
            {
              MethodParameterType.GenericPlaceHolder()
            }),
            MethodParameterType.Of(typeof(Func<,>), new List<MethodParameterType>()
            {
              MethodParameterType.GenericPlaceHolder(),
              MethodParameterType.GenericPlaceHolder()
            }),
          };
          return GetMethodForExpression(typeof(Enumerable), i_MethodName, 2, 2, parameters);
        }
      }


      /// <summary>
      /// Gets the call to i_Target.ToArray
      /// </summary>
      /// <param name="i_Target"></param>
      /// <param name="i_ArrayElementType"></param>
      /// <returns></returns>
      public static MethodCallExpression GetCallToArrayExpression(Expression i_Target, Type i_ArrayElementType)
      {
        var method = GetEnumerableToArrayMethod();
        var genericMethod = method.MakeGenericMethod(i_ArrayElementType);
        return Expression.Call(null, genericMethod, i_Target);
      }

      private static MethodInfo GetEnumerableToArrayMethod()
      {
        var parameters = new[]
        {
          MethodParameterType.Of(typeof(IEnumerable<>), new List<MethodParameterType>()
          {
            MethodParameterType.GenericPlaceHolder()
          })
        };
        return GetMethodForExpression(typeof(Enumerable), nameof(Enumerable.ToArray), 1, 1, parameters);
      }
    }
  }
}