﻿using NiceCore.ServiceLocation.Discovery.Serialization.Types;
using System.Collections.Generic;

namespace NiceCore.ServiceLocation.Tests.DebugImpls
{
  /// <summary>
  /// Registrations by assembly
  /// </summary>
  public class RegistrationByAssembly
  {
    /// <summary>
    /// name of Assembly
    /// </summary>
    public string AssemblyName { get; set; }

    /// <summary>
    /// Registrations
    /// </summary>
    public List<SerializedRegistrationInfo> Registrations { get; set; }

    /// <summary>
    /// Default ctor
    /// </summary>
    public RegistrationByAssembly()
    {
    }

    /// <summary>
    /// Fill constructor
    /// </summary>
    /// <param name="i_Info"></param>
    public RegistrationByAssembly(KeyValuePair<SerialAssembly, List<SerializedRegistrationInfo>> i_Info)
    {
      AssemblyName = i_Info.Key.FilePath;
      Registrations = new List<SerializedRegistrationInfo>(i_Info.Value);
    }
  }
}
