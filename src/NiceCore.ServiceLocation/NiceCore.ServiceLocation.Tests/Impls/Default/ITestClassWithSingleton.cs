﻿namespace NiceCore.ServiceLocation.Tests.Impls.Default
{
  /// <summary>
  /// TestInterface
  /// </summary>
  public interface ITestClassWithSingleton
  {
    /// <summary>
    /// TestClass
    /// </summary>
    ISingletonTest TestClass { get; set; }
  }
}