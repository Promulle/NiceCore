﻿namespace NiceCore.ServiceLocation.Tests.Impls.Default
{
  /// <summary>
  /// Test class
  /// </summary>
  public class TestClassWithSingleton : ITestClassWithSingleton
  {
    /// <summary>
    /// TestClass to be registered as singleton
    /// </summary>
    public ISingletonTest TestClass { get; set; }
  }
}
