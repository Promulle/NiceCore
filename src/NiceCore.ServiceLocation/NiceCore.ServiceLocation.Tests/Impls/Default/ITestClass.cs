﻿using NiceCore.Base;

namespace NiceCore.ServiceLocation.Tests.Impls.Default
{
  /// <summary>
  /// TestInterface
  /// </summary>
  public interface ITestClass : IBaseDisposable
  {
    /// <summary>
    /// Name
    /// </summary>
    string Name { get; set; }
  }
}
