﻿using NiceCore.Base;

namespace NiceCore.ServiceLocation.Tests.Impls.Default
{
  /// <summary>
  /// TestClass
  /// </summary>
  public class TestClass : BaseDisposable, ITestClass
  {
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }
  }
}
