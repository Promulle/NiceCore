﻿using System;

namespace NiceCore.ServiceLocation.Tests.Impls.CustomRegisterAttributes
{
  /// <summary>
  /// Custom Register Attribute
  /// </summary>
  [AttributeUsage(AttributeTargets.Class)]
  public class CustomRegisterAttribute : Attribute
  {
    
  }
}