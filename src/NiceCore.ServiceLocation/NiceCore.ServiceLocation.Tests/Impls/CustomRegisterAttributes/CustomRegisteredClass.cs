﻿namespace NiceCore.ServiceLocation.Tests.Impls.CustomRegisterAttributes
{
  /// <summary>
  /// Class registered with custom attribute
  /// </summary>
  [CustomRegister]
  public class CustomRegisteredClass
  {
  }
}