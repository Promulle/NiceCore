﻿using System;
using System.Collections.Generic;

namespace NiceCore.ServiceLocation.Tests.Impls.CustomRegisterAttributes
{
  /// <summary>
  /// Test Custom register attribute handler
  /// </summary>
  public class TestCustomRegisterAttributeHandler : ICustomRegistrationAttributeHandler
  {
    /// <summary>
    /// Handle custom attr
    /// </summary>
    /// <param name="i_Attribute"></param>
    /// <param name="i_Type"></param>
    /// <returns></returns>
    public ICollection<RegisterAttribute> HandleCustomAttribute(Attribute i_Attribute, Type i_Type)
    {
      return new List<RegisterAttribute>()
      {
        new()
        {
          InterfaceType = i_Type
        }
      };
    }
  }
}