﻿using System.Collections.Generic;

namespace NiceCore.ServiceLocation.Tests.Impls.CollectionInjection
{
  /// <summary>
  /// Class with ctor fill
  /// </summary>
  public class ClassWithCtorFilling : IClassWithCtorFilling
  {
    /// <summary>
    /// MyList
    /// </summary>
    public List<MyTestClass> MyList { get; set; }

    /// <summary>
    /// ctor
    /// </summary>
    public ClassWithCtorFilling()
    {
      MyList = new List<MyTestClass>()
      {
        new MyTestClass() { Name = "Hans Paul" }
      };
    }
  }
}
