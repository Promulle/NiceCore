﻿using NiceCore.ServiceLocation.Tests.Impls.Default;
using System.Collections.Generic;

namespace NiceCore.ServiceLocation.Tests.Impls.CollectionInjection
{
  /// <summary>
  /// Class with collection to be injected
  /// </summary>
  public class CollectionInjectionTestImpl : ICollectionInjectionTest
  {
    /// <summary>
    /// Not empty list
    /// </summary>
    public List<ITestClass> NonEmptyList { get; set; }

    /// <summary>
    /// Not injected list
    /// </summary>
    public IList<ITestClass> NotInjectedList { get; set; }
  }
}
