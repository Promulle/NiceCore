﻿using System.Collections.Generic;

namespace NiceCore.ServiceLocation.Tests.Impls.CollectionInjection
{
  /// <summary>
  /// Test class
  /// </summary>
  public interface IClassWithCtorFilling
  {
    /// <summary>
    /// Not touched list
    /// </summary>
    List<MyTestClass> MyList { get; set; }
  }
}