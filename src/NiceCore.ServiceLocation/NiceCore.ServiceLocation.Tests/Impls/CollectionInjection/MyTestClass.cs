﻿namespace NiceCore.ServiceLocation.Tests.Impls.CollectionInjection
{
  /// <summary>
  /// Not registered test class
  /// </summary>
  public class MyTestClass
  {
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; init; }
  }
}
