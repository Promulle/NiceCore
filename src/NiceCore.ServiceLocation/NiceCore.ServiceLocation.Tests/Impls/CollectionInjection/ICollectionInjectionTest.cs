﻿using NiceCore.ServiceLocation.Tests.Impls.Default;
using System.Collections.Generic;

namespace NiceCore.ServiceLocation.Tests.Impls.CollectionInjection
{
  /// <summary>
  /// Test class
  /// </summary>
  public interface ICollectionInjectionTest
  {
    /// <summary>
    /// Not empty list
    /// </summary>
    List<ITestClass> NonEmptyList { get; set; }

    /// <summary>
    /// null list
    /// </summary>
    IList<ITestClass> NotInjectedList { get; set; }
  }
}