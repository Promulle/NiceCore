﻿using NiceCore.ServiceLocation.Tests.Impls.Default;

namespace NiceCore.ServiceLocation.Tests.Impls.CorrectResolve
{
  /// <summary>
  /// Testclass
  /// </summary>
  public interface IClassWithPrivateProperty
  {
    /// <summary>
    /// Public Property
    /// </summary>
    ITestClass PublicProperty { get; set; }

    /// <summary>
    /// Checks if correct resolved
    /// </summary>
    /// <returns></returns>
    bool CorrectlyResolved();
  }
}