﻿using NiceCore.ServiceLocation.Tests.Impls.Default;

namespace NiceCore.ServiceLocation.Tests.Impls.CorrectResolve
{
  /// <summary>
  /// Class for testing private property resolve
  /// </summary>
  public class ClassWithPrivateProperty : IClassWithPrivateProperty
  {
    /// <summary>
    /// Public property that should be resolved
    /// </summary>
    public ITestClass PublicProperty { get; set; }

    /// <summary>
    /// Protected Property that should not be resolved
    /// </summary>
    protected ITestClass ProtectedProperty { get; set; }

    /// <summary>
    /// Private Property that should not be resolved
    /// </summary>
    private ITestClass PrivateProperty { get; set; }

    /// <summary>
    /// Checks if resolve went correctly
    /// </summary>
    /// <returns></returns>
    public bool CorrectlyResolved()
    {
      return (PublicProperty != null && ProtectedProperty == null && PrivateProperty == null);
    }
  }
}
