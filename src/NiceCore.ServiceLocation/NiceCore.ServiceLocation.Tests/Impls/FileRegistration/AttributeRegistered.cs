﻿namespace NiceCore.ServiceLocation.Tests.Impls.FileRegistration
{
  /// <summary>
  /// Registered by attribute
  /// </summary>
  [Register(InterfaceType = typeof(IFileService))]
  public class AttributeRegistered : IFileService
  {
    /// <summary>
    /// value
    /// </summary>
    public const string VALUE = "5";

    /// <summary>
    /// Value
    /// </summary>
    public string Value
    {
      get { return VALUE; }
    }
  }
}
