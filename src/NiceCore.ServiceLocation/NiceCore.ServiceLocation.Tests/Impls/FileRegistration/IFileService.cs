﻿namespace NiceCore.ServiceLocation.Tests.Impls.FileRegistration
{
  /// <summary>
  /// testRegistration
  /// </summary>
  public interface IFileService
  {
    /// <summary>
    /// Value
    /// </summary>
    string Value { get; }
  }
}
