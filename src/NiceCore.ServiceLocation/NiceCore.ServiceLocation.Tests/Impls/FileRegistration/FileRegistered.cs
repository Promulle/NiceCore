﻿namespace NiceCore.ServiceLocation.Tests.Impls.FileRegistration
{
  /// <summary>
  /// FileRegistered
  /// </summary>
  public class FileRegistered : IFileService
  {
    /// <summary>
    /// Value
    /// </summary>
    public const string VALUE = "10";
    /// <summary>
    /// 
    /// </summary>
    public string Value
    {
      get { return VALUE; }
    }
  }
}