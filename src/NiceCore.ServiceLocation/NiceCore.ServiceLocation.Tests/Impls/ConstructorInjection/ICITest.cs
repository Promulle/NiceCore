﻿using NiceCore.ServiceLocation.Tests.Impls.PropertyResolve;

namespace NiceCore.ServiceLocation.Tests.Impls.ConstructorInjection
{
  /// <summary>
  /// TestInterface for ConstrcutorInjection
  /// </summary>
  public interface ICITest
  {
    /// <summary>
    /// Property
    /// </summary>
    ITestService TestProperty { get; set; }
  }
}