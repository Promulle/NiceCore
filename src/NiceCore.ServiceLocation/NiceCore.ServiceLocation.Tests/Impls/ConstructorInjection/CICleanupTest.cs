﻿using NiceCore.ServiceLocation.Tests.Impls.PropertyResolve;

namespace NiceCore.ServiceLocation.Tests.Impls.ConstructorInjection
{
  /// <summary>
  /// Test for cleanup
  /// </summary>
  public class CICleanupTest : ICICleanupTest
  {
    /// <summary>
    /// TestProperty
    /// </summary>
    [DoNotResolve]
    public ITestService TestProperty { get; set; }

    /// <summary>
    /// Value
    /// </summary>
    public string Value { get; set; }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="i_Service"></param>
    /// <param name="i_ValueGetter"></param>
    public CICleanupTest(ITestService i_Service, IValueGetter i_ValueGetter)
    {
      TestProperty = i_Service;
      Value = i_ValueGetter.GetValue();
    }
  }
}
