﻿using NiceCore.ServiceLocation.Tests.Impls.Default;
using NiceCore.ServiceLocation.Tests.Impls.PropertyResolve;

namespace NiceCore.ServiceLocation.Tests.Impls.ConstructorInjection
{
  /// <summary>
  /// CI test multi constructor
  /// </summary>
  public class CIMultiCtorTestFail : ICIMultiCtorTest
  {
    /// <summary>
    /// test service
    /// </summary>
    [DoNotResolve]
    public ITestService TestService { get; set; }

    /// <summary>
    /// Test class
    /// </summary>
    [DoNotResolve]
    public ITestClass TestClass { get; set; }

    /// <summary>
    /// Test property
    /// </summary>
    [DoNotResolve]
    public ITestProperty TestProperty { get; set; }

    /// <summary>
    /// ctor
    /// </summary>
    /// <param name="i_TestService"></param>
    /// <param name="i_Class"></param>
    public CIMultiCtorTestFail(ITestService i_TestService, ITestClass i_Class)
    {
      TestService = i_TestService;
      TestClass = i_Class;
    }

    /// <summary>
    /// Ctor
    /// </summary>
    public CIMultiCtorTestFail(ITestService i_TestService, ITestProperty i_Property)
    {
      TestService = i_TestService;
      TestProperty = i_Property;
    }
  }
}
