﻿using NiceCore.ServiceLocation.Tests.Impls.Default;
using NiceCore.ServiceLocation.Tests.Impls.PropertyResolve;

namespace NiceCore.ServiceLocation.Tests.Impls.ConstructorInjection
{
  /// <summary>
  /// multi ctor test
  /// </summary>
  public interface ICIMultiCtorTest
  {
    /// <summary>
    /// Test class
    /// </summary>
    ITestClass TestClass { get; set; }
    /// <summary>
    /// Test property
    /// </summary>
    ITestProperty TestProperty { get; set; }
    /// <summary>
    /// Test service
    /// </summary>
    ITestService TestService { get; set; }
  }
}