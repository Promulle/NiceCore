﻿using NiceCore.ServiceLocation.Tests.Impls.PropertyResolve;

namespace NiceCore.ServiceLocation.Tests.Impls.ConstructorInjection
{
  /// <summary>
  /// TestClass for Constructor Injection
  /// </summary>
  public class CITest : ICITest
  {
    /// <summary>
    /// TestProperty
    /// </summary>
    [DoNotResolve]
    public ITestService TestProperty { get; set; }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="i_Service"></param>
    public CITest(ITestService i_Service)
    {
      TestProperty = i_Service;
    }
  }
}
