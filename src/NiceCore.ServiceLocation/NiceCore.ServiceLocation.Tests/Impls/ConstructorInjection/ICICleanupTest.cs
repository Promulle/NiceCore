﻿using NiceCore.ServiceLocation.Tests.Impls.PropertyResolve;

namespace NiceCore.ServiceLocation.Tests.Impls.ConstructorInjection
{
  /// <summary>
  /// CI Cleanup Test
  /// </summary>
  public interface ICICleanupTest
  {
    /// <summary>
    /// Property
    /// </summary>
    ITestService TestProperty { get; set; }

    /// <summary>
    /// Value
    /// </summary>
    string Value { get; set; }
  }
}