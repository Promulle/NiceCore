﻿namespace NiceCore.ServiceLocation.Tests.Impls.CycleDetection
{
  /// <summary>
  /// Cycle Host class
  /// </summary>
  public class CycleHost : ICycleHost
  {
    /// <summary>
    /// Cycle property
    /// </summary>
    public ICycleProperty Cycle { get; set; }
  }
}
