﻿namespace NiceCore.ServiceLocation.Tests.Impls.CycleDetection
{
  /// <summary>
  /// Interface property
  /// </summary>
  public interface ICycleProperty
  {
    /// <summary>
    /// Host
    /// </summary>
    ICycleHost Host { get; set; }
  }
}