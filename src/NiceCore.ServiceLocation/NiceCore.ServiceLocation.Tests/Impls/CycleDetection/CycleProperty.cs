﻿namespace NiceCore.ServiceLocation.Tests.Impls.CycleDetection
{
  /// <summary>
  /// Property
  /// </summary>
  public class CycleProperty : ICycleProperty
  {
    /// <summary>
    /// Host
    /// </summary>
    public ICycleHost Host { get; set; }
  }
}
