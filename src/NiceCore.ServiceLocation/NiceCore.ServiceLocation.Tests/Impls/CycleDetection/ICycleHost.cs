﻿namespace NiceCore.ServiceLocation.Tests.Impls.CycleDetection
{
  /// <summary>
  /// Interface host
  /// </summary>
  public interface ICycleHost
  {
    /// <summary>
    /// Interface cycle
    /// </summary>
    ICycleProperty Cycle { get; set; }
  }
}