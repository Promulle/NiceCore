﻿namespace NiceCore.ServiceLocation.Tests.Impls.PropertyResolve
{
  /// <summary>
  /// Test interface for singleton property
  /// </summary>
  public interface ISingletonTestProperty
  {
    /// <summary>
    /// Name
    /// </summary>
    string Name { get; set; }
  }
}
