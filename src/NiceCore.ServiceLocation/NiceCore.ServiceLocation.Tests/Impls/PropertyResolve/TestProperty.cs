﻿namespace NiceCore.ServiceLocation.Tests.Impls.PropertyResolve

{
  /// <summary>
  /// TEstProperty
  /// </summary>
  public class TestProperty : ITestProperty, ISingletonTestProperty
  {
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }
  }
}
