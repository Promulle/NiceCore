﻿namespace NiceCore.ServiceLocation.Tests.Impls.PropertyResolve
{
  /// <summary>
  /// Interface for itest service
  /// </summary>
  public interface ITestService
  {
    /// <summary>
    /// Notresolved property
    /// </summary>
    ITestProperty NotResolvedProperty { get; set; }
    /// <summary>
    /// resolved property
    /// </summary>
    ITestProperty ResolvedProperty { get; set; }
    /// <summary>
    /// Singletonproperty
    /// </summary>
    ISingletonTestProperty SingletonProperty { get; set; }
  }
}