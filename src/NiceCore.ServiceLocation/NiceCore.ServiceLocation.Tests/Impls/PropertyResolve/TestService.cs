﻿namespace NiceCore.ServiceLocation.Tests.Impls.PropertyResolve
{
  /// <summary>
  /// TestClass with properties
  /// </summary>
  public class TestService : ITestService
  {
    /// <summary>
    /// Property that should be resolved
    /// </summary>
    public ITestProperty ResolvedProperty { get; set; }
    /// <summary>
    /// Property that should not be resolved
    /// </summary>
    [DoNotResolve]
    public ITestProperty NotResolvedProperty { get; set; }
    /// <summary>
    /// SingletonProperty
    /// </summary>
    public ISingletonTestProperty SingletonProperty { get; set; }
  }
}
