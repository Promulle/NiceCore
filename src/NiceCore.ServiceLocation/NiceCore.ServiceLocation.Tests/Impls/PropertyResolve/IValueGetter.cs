﻿namespace NiceCore.ServiceLocation.Tests.Impls.PropertyResolve
{
  /// <summary>
  /// Test CI
  /// </summary>
  public interface IValueGetter
  {
    /// <summary>
    /// GetValue
    /// </summary>
    /// <returns></returns>
    string GetValue();
  }
}
