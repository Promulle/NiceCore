﻿namespace NiceCore.ServiceLocation.Tests.Impls.PropertyResolve
{
  /// <summary>
  /// IValueGetter Impl
  /// </summary>
  public class ValueGetter : IValueGetter
  {
    /// <summary>
    /// Value
    /// </summary>
    public static readonly string VALUE = "5";
    /// <summary>
    /// Returns value
    /// </summary>
    /// <returns></returns>
    public string GetValue()
    {
      return VALUE;
    }
  }
}
