﻿namespace NiceCore.ServiceLocation.Tests.Impls.PropertyResolve
{
  /// <summary>
  /// Interface for TestProperty
  /// </summary>
  public interface ITestProperty
  {
    /// <summary>
    /// Name
    /// </summary>
    string Name { get; set; }
  }
}