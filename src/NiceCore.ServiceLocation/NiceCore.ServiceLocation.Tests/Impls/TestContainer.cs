﻿namespace NiceCore.ServiceLocation.Tests.Impls
{
  /// <summary>
  /// class collecting container and extension
  /// </summary>
  public class TestContainer
  {
    /// <summary>
    /// Container
    /// </summary>
    public IServiceContainer Container { get; }

    /// <summary>
    /// Inspector
    /// </summary>
    public IContainerInspector Inspector { get; }

    /// <summary>
    /// Fill constructor
    /// </summary>
    /// <param name="i_Container"></param>
    /// <param name="i_Inspector"></param>
    public TestContainer(IServiceContainer i_Container, IContainerInspector i_Inspector)
    {
      Container = i_Container;
      Inspector = i_Inspector;
    }
  }
}
