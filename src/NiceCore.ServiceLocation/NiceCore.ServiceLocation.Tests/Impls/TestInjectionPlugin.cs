﻿using NiceCore.ServiceLocation.Plugins;
using System;

namespace NiceCore.ServiceLocation.Tests.Impls
{
  /// <summary>
  /// Plugin to test that methods are called
  /// </summary>
  public class TestInjectionPlugin : BasePlugin
  {
    /// <summary>
    /// Release called or not
    /// </summary>
    public bool ReleaseCalled { get; private set; }

    /// <summary>
    /// Whether resolve called
    /// </summary>
    public bool ResolveCalled { get; private set; }

    /// <summary>
    /// enable
    /// </summary>
    /// <param name="i_Container"></param>
    /// <returns></returns>
    protected override bool InternalEnablePlugin(IServiceContainer i_Container)
    {
      return true;
    }

    /// <summary>
    /// Release
    /// </summary>
    /// <param name="t_Instance"></param>
    protected override void InternalInstanceReleased(object t_Instance)
    {
      ReleaseCalled = true;
    }

    /// <summary>
    /// Resolve
    /// </summary>
    /// <param name="t_Instance"></param>
    /// <param name="i_InterfaceType"></param>
    protected override void InternalInstanceResolved(object t_Instance, Type i_InterfaceType)
    {
      ResolveCalled = true;
    }
  }
}
