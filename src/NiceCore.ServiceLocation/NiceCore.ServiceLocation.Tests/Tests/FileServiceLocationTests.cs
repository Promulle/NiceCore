﻿using System.IO;
using FluentAssertions;
using NiceCore.ServiceLocation.Discovery;
using NiceCore.ServiceLocation.Discovery.Files;
using NiceCore.ServiceLocation.Tests.Impls.FileRegistration;
using NUnit.Framework;

namespace NiceCore.ServiceLocation.Tests.Tests
{
  /// <summary>
  /// Tests for xml registrations
  /// </summary>
  [TestFixture]
  public abstract class FileServiceLocationTests : ContainerUsingTests
  {
    private const string XML_PATTERN = "*.xml";
    private const string XML_SUFFIX = ".registration.xml";
    private const int TEST_CREATION_AND_REGISTER_EXPECTED_REGISTRATION_COUNT = 1;
    private string m_Path;

    /// <summary>
    /// Setup
    /// </summary>
    [SetUp]
    public void DiscoverySetup()
    {
      m_Path = Path.GetDirectoryName(GetType().Assembly.Location);
      //clean existing registration files
      foreach (var xmlFile in Directory.GetFiles(m_Path, XML_PATTERN))
      {
        File.Delete(xmlFile);
      }
    }

    /// <summary>
    /// Tests Creation and register
    /// </summary>
    [Test]
    public void TestCreationAndRegister()
    {
      CreateRegistrationFile<IFileService, FileRegistered>();
      m_Discovery.Discover(new DiscoveryRunConfiguration()
      {
        TargetMode = DiscoveryModes.Files,
        DependencyFileAdapter = new NiceCoreFileAdapter()
      });
      TEST_CREATION_AND_REGISTER_EXPECTED_REGISTRATION_COUNT.Should().Be(RegistrationCount<IFileService>());
    }

    private int RegistrationCount<TType>()
    {
      return m_Inspector.RegistrationCount<TType>();
    }

    private void CreateRegistrationFile<TInterface, TImplementor>()
    {
      var regInfo = new RegistrationInfo
      {
        ImplementorType = typeof(TImplementor),
        InterfaceType = typeof(TInterface),
        LifeStyle = LifeStyles.Transient
      };
      var xmlInfo = new XmlRegistrationInfo(regInfo);
      var xmlFile = new XmlRegistrationsFile();
      xmlFile.Registrations.Add(xmlInfo);
      xmlFile.SerializeToFile(m_Path + Path.DirectorySeparatorChar + typeof(TInterface).Name + XML_SUFFIX);
    }
  }
}
