﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using FluentAssertions;
using NiceCore.Eventing.Messaging;
using NiceCore.ServiceLocation.Discovery;
using NiceCore.ServiceLocation.Discovery.DefaultDirectoryProviders;
using NiceCore.Storage.NH.Conversation;
using NiceCore.Storage.NH.Mapping;
using NUnit.Framework;

namespace NiceCore.ServiceLocation.Tests.Tests
{
  /// <summary>
  /// Tests for constrained modes of service registration
  /// </summary>
  [TestFixture]
  public abstract class ConstrainedServiceLocationTests : ContainerUsingTests
  {
    private const string PATTERN_MODULES = "*.ncmodules";
    private const string EXT_MODULE = ".ncmodules";

    /// <summary>
    /// Tests constraining registrations by a default file
    /// </summary>
    [Test]
    public void ConstrainedDefaultFile()
    {
      ClearModuleFiles();
      CreateDefaultModulesFile();
      m_Discovery.Discover(new DiscoveryRunConfiguration()
      {
        TargetMode = DiscoveryModes.Assemblies,
        AssemblyTargetMode = AssemblyProvidingModes.ConstrainedSingleFile,
        DiscoveryDefaultDirectoryProvider = new ExecutingAssemblyDefaultDirectoryProvider()
      });
      RegistrationCount<IMessageService>().Should().Be(1);
      RegistrationCount<INHConversation>().Should().Be(0);
    }

    /// <summary>
    /// Tests constraining registrations with multiple files
    /// </summary>
    [Test]
    public void ConstrainedMultiFile()
    {
      var firstList = new List<string>
      {
        "NiceCore.Services",
        "NiceCore.Eventing.dll",
        "NiceCore.Services.Default.dll",
      };
      var secondList = new List<string>
      {
        "NiceCore.Storage.NH.dll",
      };
      ClearModuleFiles();
      CreateModulesFile("services", firstList);
      CreateModulesFile("storage", secondList);
      m_Discovery.Discover(new DiscoveryRunConfiguration()
      {
        TargetMode = DiscoveryModes.Assemblies,
        AssemblyTargetMode = AssemblyProvidingModes.Constrained,
        DiscoveryDefaultDirectoryProvider = new ExecutingAssemblyDefaultDirectoryProvider(),
      });
      RegistrationCount<IMessageService>().Should().Be(1);
      RegistrationCount<INHConversation>().Should().Be(1);
      RegistrationCount<IFluentConvention>().Should().Be(0);
    }

    private int RegistrationCount<TType>()
    {
      return m_Inspector.RegistrationCount<TType>();
    }

    private void ClearModuleFiles()
    {
      Directory.GetFiles(Path.GetDirectoryName(GetType().Assembly.Location), PATTERN_MODULES)
        .ToList()
        .ForEach(r => File.Delete(r));
    }

    private void CreateModulesFile(string i_BaseName, IEnumerable<string> i_Lines)
    {
      var fileName = i_BaseName + EXT_MODULE;
      File.AppendAllLines(fileName, i_Lines);
    }

    private void CreateDefaultModulesFile()
    {
      var list = new List<string>
      {
        "NiceCore.dll",
        "NiceCore.Eventing.dll",
        "NiceCore.ServiceLocation.dll",
        "NiceCore.Services.dll",
        "NiceCore.Services.Default.dll"
      };
      CreateModulesFile("nicecore", list);
    }
  }
}
