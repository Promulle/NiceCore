﻿using FluentAssertions;
using NiceCore.ServiceLocation.Tests.Impls.CollectionInjection;
using NiceCore.ServiceLocation.Tests.Impls.Default;
using NUnit.Framework;

namespace NiceCore.ServiceLocation.Tests.Tests
{
  /// <summary>
  /// Test for collection injection
  /// </summary>
  [TestFixture]
  public abstract class CollectionInjectionTests : ContainerUsingTests
  {
    /// <summary>
    /// Tests collection injection functionality
    /// </summary>
    [Test]
    public void CollectionInjection()
    {
      m_Container.Register<ITestClass, TestClass>();
      m_Container.Register<ITestClass, SecondTestClass>();
      m_Container.Register<ICollectionInjectionTest, CollectionInjectionTestImpl>();

      var inst = m_Container.Resolve<ICollectionInjectionTest>();
      inst.Should().NotBeNull();
      inst.NotInjectedList.Should().BeNull();
      inst.NonEmptyList.Should().NotBeNull();
      inst.NonEmptyList.Should().NotBeEmpty();
      inst.NonEmptyList.Should().HaveCount(2);
    }

    /// <summary>
    /// Tests that in ctor created, not injected lists still contain their ctor value
    /// </summary>
    [Test]
    public void DontTouchCtorFilled()
    {
      m_Container.Register<IClassWithCtorFilling, ClassWithCtorFilling>();
      var inst = m_Container.Resolve<IClassWithCtorFilling>();
      inst.Should().NotBeNull();
      inst.MyList.Should().NotBeNull();
      inst.MyList.Should().HaveCount(1);
    }
  }
}
