﻿using FluentAssertions;
using NUnit.Framework;

namespace NiceCore.ServiceLocation.Tests.Tests
{
  /// <summary>
  /// Base class for tests using TestContainer
  /// </summary>
  [TestFixture]
  public abstract class ContainerUsingTests
  {
    /// <summary>
    /// Inspector
    /// </summary>
    protected IContainerInspector m_Inspector;

    /// <summary>
    /// Container
    /// </summary>
    protected IServiceContainer m_Container;

    /// <summary>
    /// Discovery
    /// </summary>
    protected IRegistrationDiscovery m_Discovery;

    /// <summary>
    /// GetContainer
    /// </summary>
    /// <returns></returns>
    protected abstract (IServiceContainer Container, IRegistrationDiscovery Discovery, IContainerInspector Inspector) GetContainer();

    /// <summary>
    /// Setup for Tests
    /// </summary>
    [SetUp()]
    public void Setup()
    {
      var vals = GetContainer();
      m_Container = vals.Container;
      m_Inspector = vals.Inspector;
      m_Discovery = vals.Discovery;
      m_Container.Should().NotBeNull("Container has not been initialized. Check your implementation of GetContainer().");
    }
  }
}
