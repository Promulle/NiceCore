﻿using FluentAssertions;
using NiceCore.Base.Caching.Exceptions;
using NiceCore.ServiceLocation.Tests.Impls.ConstructorInjection;
using NiceCore.ServiceLocation.Tests.Impls.Default;
using NiceCore.ServiceLocation.Tests.Impls.PropertyResolve;
using System;
using NUnit.Framework;

namespace NiceCore.ServiceLocation.Tests.Tests
{
  /// <summary>
  /// Tests for constructor injection
  /// </summary>
  public abstract class ConstructorInjectionTests : ContainerUsingTests
  {
    private const int TEST_CI_CLEANUP_EXP = 0;

    /// <summary>
    /// Tests constructor injection
    /// </summary>
    [Test]
    public void TestConstructorInjection()
    {
      m_Container.Register<ICITest, CITest>();
      m_Container.Register<ITestService, TestService>(LifeStyles.Singleton, null);
      var ciTest = m_Container.Resolve<ICITest>();
      ciTest.Should().NotBeNull();
      ciTest.TestProperty.Should().NotBeNull();
    }

    /// <summary>
    /// Tests constructor injection
    /// </summary>
    [Test]
    public void TestConstructorInjectionAndCleanup()
    {
      m_Container.Register<ICICleanupTest, CICleanupTest>();
      m_Container.Register<IValueGetter, ValueGetter>();
      m_Container.Register<ITestService, TestService>(LifeStyles.Singleton, null);
      var ciTest = m_Container.Resolve<ICICleanupTest>();
      ciTest.Should().NotBeNull();
      ciTest.TestProperty.Should().NotBeNull();
      ValueGetter.VALUE.Should().Be(ciTest.Value);
      m_Inspector.InstanceCount<IValueGetter>().Should().Be(TEST_CI_CLEANUP_EXP);
    }

    /// <summary>
    /// Tests that the injection attribute is used
    /// </summary>
    [Test]
    public void ConstructorInjectionAttribute()
    {
      m_Container.Register<ICIMultiCtorTest, CIMultiCtorTest>();
      m_Container.Register<ITestService, TestService>();
      m_Container.Register<ITestProperty, TestProperty>();
      m_Container.Register<ITestClass, TestClass>();
      var ciTest = m_Container.Resolve<ICIMultiCtorTest>();
      ciTest.Should().NotBeNull();
      ciTest.TestService.Should().NotBeNull();
      ciTest.TestClass.Should().NotBeNull();
      ciTest.TestProperty.Should().BeNull();
    }

    /// <summary>
    /// Tests that the injection attribute is used
    /// </summary>
    [Test]
    public void ConstructorInjectionMultiConstructorParamlessUsed()
    {
      m_Container.Register<ICIMultiCtorTest, CIMultiCtorTestParamLess>();
      m_Container.Register<ITestService, TestService>();
      m_Container.Register<ITestProperty, TestProperty>();
      m_Container.Register<ITestClass, TestClass>();
      var ciTest = m_Container.Resolve<ICIMultiCtorTest>();
      ciTest.Should().NotBeNull();
      ciTest.TestService.Should().BeNull();
      ciTest.TestClass.Should().BeNull();
      ciTest.TestProperty.Should().BeNull();
    }

    /// <summary>
    /// Tests that the injection attribute is used
    /// </summary>
    [Test]
    public void ConstructorInjectionFailWithMultipleNoAttribute()
    {
      m_Container.Register<ICIMultiCtorTest, CIMultiCtorTestFail>();
      m_Container.Register<ITestService, TestService>();
      m_Container.Register<ITestProperty, TestProperty>();
      m_Container.Register<ITestClass, TestClass>();
      Func<ICIMultiCtorTest> act = () => m_Container.Resolve<ICIMultiCtorTest>();
      act.Should().Throw<CacheValueException>().WithInnerException<Exception>();
    }
  }
}
