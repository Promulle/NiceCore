﻿using System.Linq;
using FluentAssertions;
using NiceCore.Extensions;
using NiceCore.ServiceLocation.Helpers;
using NiceCore.ServiceLocation.Plugins;
using NiceCore.ServiceLocation.Tests.Impls;
using NiceCore.ServiceLocation.Tests.Impls.CycleDetection;
using NiceCore.ServiceLocation.Tests.Impls.Default;
using NiceCore.ServiceLocation.Tests.Impls.PropertyResolve;
using NUnit.Framework;

namespace NiceCore.ServiceLocation.Tests.Tests
{
  /// <summary>
  /// Tests for advanced features of ServiceLocation
  /// </summary>
  public abstract class AdvancedServiceLocationTest : ContainerUsingTests
  {
    private const int TEST_RESOLVE_SERVICE_RESULT_EXP = 1;
    private const int TEST_RELEASE_SERVICE_RES_EXP = 0;

    /// <summary>
    /// locate function tests
    /// </summary>
    [Test]
    public void LocateFunctions()
    {
      m_Container.Register<ITestClass, TestClass>();
      using (var instance = m_Container.Locate<ITestClass>())
      {
        instance.HasInstance().Should().BeTrue();
        m_Inspector.HasInstances<ITestClass>().Should().BeTrue();
      }
      m_Inspector.HasInstances<ITestClass>().Should().BeFalse();
      m_Container.Register<ITestClass, SingletonTest>();
      using (var instances = m_Container.LocateAll<ITestClass>())
      {
        instances.HasInstances().Should().BeTrue();
        m_Inspector.HasInstances<ITestClass>().Should().BeTrue();
        m_Inspector.InstanceCount<ITestClass>().Should().Be(2);
      }
      m_Inspector.HasInstances<ITestClass>().Should().BeFalse();
    }

    /// <summary>
    /// Tests plugin functionality
    /// </summary>
    [Test]
    public void InjectionPlugins()
    {
      m_Container.Plugins.Should().NotBeNull();
      var testPluginInst = new TestInjectionPlugin();
      testPluginInst.ResolveCalled.Should().BeFalse();
      testPluginInst.ReleaseCalled.Should().BeFalse();
      new IDependencyInjectionLifecyclePlugin[]
      {
        new ReleaseOnDisposePlugin(),
        new InitializePlugin(),
        testPluginInst,
      }.ForEachItem(r => m_Container.Plugins.Add(r));
      m_Container.Plugins.Count.Should().Be(3);
      m_Container.EnablePlugins();
      m_Container.Plugins.Any(r => !r.IsEnabled).Should().BeFalse();
      m_Container.Register<ITestClass, TestClass>();
      var testInst = m_Container.Resolve<ITestClass>();
      m_Container.Release(testInst);
      testPluginInst.ResolveCalled.Should().BeTrue();
      testPluginInst.ReleaseCalled.Should().BeTrue();
    }

    /// <summary>
    /// tests whether cycle detection in release works
    /// </summary>
    [Test]
    public void TestCycleRelease()
    {
      var cycleHost = new CycleHost();
      m_Container.RegisterInstance<ICycleHost>(cycleHost);
      var cycleProperty = new CycleProperty()
      {
        Host = cycleHost,
      };
      m_Container.RegisterInstance<ICycleProperty>(cycleProperty);
      cycleHost.Cycle = cycleProperty;
      m_Inspector.InstanceCount<ICycleHost>().Should().BeGreaterThan(0);
      m_Inspector.InstanceCount<ICycleProperty>().Should().BeGreaterThan(0);
      m_Container.Release(cycleHost);
      m_Inspector.InstanceCount<ICycleHost>().Should().Be(0);
      m_Inspector.InstanceCount<ICycleProperty>().Should().Be(0);
    }

    /// <summary>
    /// Test for RegisterInstance function
    /// </summary>
    [Test]
    public void TestRegisterInstance()
    {
      m_Container.Register<ISingletonTest, SingletonTest>(LifeStyles.Singleton, null);
      ISingletonTest singletonTestClass = new SingletonTest();
      ITestClass testClass = new TestClass();
      m_Container.RegisterInstance(testClass);
      m_Container.RegisterInstance(singletonTestClass, LifeStyles.Singleton);
      m_Inspector.InstanceCount<ITestClass>().Should().Be(TEST_RESOLVE_SERVICE_RESULT_EXP);
      m_Inspector.InstanceCount<ISingletonTest>().Should().Be(TEST_RESOLVE_SERVICE_RESULT_EXP);
    }

    private int GetInstanceCount<T>()
    {
      return m_Inspector.InstanceCount<T>();
    }

    /// <summary>
    /// Tests for recursive releasing
    /// </summary>
    [Test]
    public void TestRecursiveRelease()
    {
      //Register test classes
      m_Container.Register<ITestProperty, TestProperty>();
      m_Container.Register<ISingletonTestProperty, TestProperty>(LifeStyles.Singleton, null);
      m_Container.Register<ITestService, TestService>(LifeStyles.Transient, null);
      //Check whether the resolve of servInstance was successfull
      var servInstance = m_Container.Resolve<ITestService>();
      TEST_RESOLVE_SERVICE_RESULT_EXP.Should().Be(GetInstanceCount<ITestService>());
      TEST_RESOLVE_SERVICE_RESULT_EXP.Should().Be(GetInstanceCount<ITestProperty>());
      TEST_RESOLVE_SERVICE_RESULT_EXP.Should().Be(GetInstanceCount<ISingletonTestProperty>());
      m_Container.Release(servInstance);
      //check whether every property, that is not singleton was released
      TEST_RELEASE_SERVICE_RES_EXP.Should().Be(GetInstanceCount<ITestProperty>());
      TEST_RELEASE_SERVICE_RES_EXP.Should().Be(GetInstanceCount<ITestService>());
      TEST_RESOLVE_SERVICE_RESULT_EXP.Should().Be(GetInstanceCount<ISingletonTestProperty>());
    }

    /// <summary>
    /// Test for auto release on dispose
    /// </summary>
    [Test]
    public void TestAutoReleaseOnDispose()
    {
      m_Container.Plugins.Add(new ReleaseOnDisposePlugin());
      m_Container.EnablePlugins();
      m_Container.Register<ITestClass, TestClass>();
      var testInst = m_Container.Resolve<ITestClass>();
      TEST_RESOLVE_SERVICE_RESULT_EXP.Should().Be(GetInstanceCount<ITestClass>());
      testInst.Dispose();
      TEST_RELEASE_SERVICE_RES_EXP.Should().Be(GetInstanceCount<ITestClass>());
    }

    /// <summary>
    /// test if test is registered (it should not be)
    /// </summary>
    [Test]
    public void TestTestClassNotRegistered()
    {
      m_Container.Resolve<ITestClass>().Should().BeNull();
    }
  }
}
