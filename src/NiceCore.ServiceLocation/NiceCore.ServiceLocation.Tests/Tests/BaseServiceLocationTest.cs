﻿using FluentAssertions;
using NiceCore.ServiceLocation.Tests.Impls.CorrectResolve;
using NiceCore.ServiceLocation.Tests.Impls.Default;
using NiceCore.ServiceLocation.Tests.Impls.PropertyResolve;
using NUnit.Framework;

namespace NiceCore.ServiceLocation.Tests.Tests
{
  /// <summary>
  /// base Tests for NiceCore.Base.ServiceLocation and NiceCore.ServiceLocation
  /// </summary>
  public abstract class BaseServiceLocationTest : ContainerUsingTests
  {
    /// <summary>
    /// Tests correct resolve
    /// </summary>
    [Test]
    public void TestCorrectResolve()
    {
      m_Container.Register<IClassWithPrivateProperty, ClassWithPrivateProperty>();
      m_Container.Register<ITestClass, TestClass>();
      var inst = m_Container.Resolve<IClassWithPrivateProperty>();
      inst.CorrectlyResolved().Should().BeTrue();
    }

    /// <summary>
    /// Tests register and resolve
    /// </summary>
    [Test]
    public void TestRegisterResolve()
    {
      m_Container.Register<ITestClass, TestClass>();
      var inst = m_Container.Resolve<ITestClass>();
      inst.Should().NotBeNull();
    }

    /// <summary>
    /// Tests lifestyles transient and singleton
    /// </summary>
    [Test]
    public void TestLifestyles()
    {
      m_Container.Register<ITestClass, TestClass>();
      m_Container.Register<ISingletonTest, SingletonTest>(LifeStyles.Singleton, null);
      var trInst = m_Container.Resolve<ITestClass>();
      var trInst_2 = m_Container.Resolve<ITestClass>();
      var siInst = m_Container.Resolve<ISingletonTest>();
      var siInst_2 = m_Container.Resolve<ISingletonTest>();
      trInst.Name = "1";
      trInst.Name.Should().NotBe(trInst_2.Name);
      siInst.Name = "2";
      siInst.Name.Should().Be(siInst_2.Name);
    }

    /// <summary>
    /// TEsts releasing of objects
    /// </summary>
    [Test]
    public void TestRelease()
    {
      //test transient release
      m_Container.Register<ITestClass, TestClass>();
      var inst = m_Container.Resolve<ITestClass>();
      m_Inspector.HasInstances<ITestClass>().Should().BeTrue();
      m_Container.Release(inst);
      m_Inspector.HasInstances<ITestClass>().Should().BeFalse();
      //Test singleton release
      m_Container.Register<ITestClass, SingletonTest>(LifeStyles.Singleton, null);
      var singletonInst = m_Container.Resolve<ITestClass>("single");
      m_Inspector.HasInstances<ITestClass>("single").Should().BeTrue();
      m_Container.Release(singletonInst);
      m_Inspector.HasInstances<ITestClass>("single").Should().BeTrue();
    }

    /// <summary>
    /// tests setting AllowReleaseSingletons to false and then -> no release of singleton possible
    /// </summary>
    [Test]
    public void TestNotReleaseSingletonProperty()
    {
      m_Container.Register<ITestClassWithSingleton, TestClassWithSingleton>();
      m_Container.Register<ISingletonTest, SingletonTest>(LifeStyles.Singleton, null);
      var inst = m_Container.Resolve<ITestClassWithSingleton>();
      m_Inspector.HasInstances<ISingletonTest>().Should().BeTrue();
      m_Inspector.HasInstances<ITestClassWithSingleton>().Should().BeTrue();
      m_Container.Release(inst);
      m_Inspector.HasInstances<ISingletonTest>().Should().BeTrue();
      m_Inspector.HasInstances<ITestClassWithSingleton>().Should().BeFalse();
    }

    private void RegisterPropertiesAndService()
    {
      m_Container.Register<ITestProperty, TestProperty>();
      m_Container.Register<ISingletonTestProperty, TestProperty>(LifeStyles.Singleton, null);
      m_Container.Register<ITestService, TestService>();
    }

    /// <summary>
    /// Tests whether properties are resolved and not resolved if [DoNotResolve] is applied
    /// </summary>
    [Test]
    public void TestResolveProperties()
    {
      RegisterPropertiesAndService();
      var service = m_Container.Resolve<ITestService>();
      service.ResolvedProperty.Should().NotBeNull();
      service.NotResolvedProperty.Should().BeNull();
      service.SingletonProperty.Should().NotBeNull();
    }

    /// <summary>
    /// Tests whether properties are released correctly when an object is released
    /// </summary>
    [Test]
    public void TestReleaseProperties()
    {
      RegisterPropertiesAndService();
      var service = m_Container.Resolve<ITestService>();
      m_Inspector.HasInstances<ITestService>().Should().BeTrue();
      m_Inspector.HasInstances<ITestProperty>().Should().BeTrue();
      m_Inspector.HasInstances<ISingletonTestProperty>().Should().BeTrue();
      m_Container.Release(service);
      m_Inspector.HasInstances<ITestService>().Should().BeFalse();
      m_Inspector.HasInstances<ITestProperty>().Should().BeFalse();
      m_Inspector.HasInstances<ISingletonTestProperty>().Should().BeTrue();
    }
  }
}
