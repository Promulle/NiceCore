﻿// using FluentAssertions;
// using NiceCore.ServiceLocation.Tests.Impls.Default;
// using NUnit.Framework;
//
// namespace NiceCore.ServiceLocation.Tests.Tests
// {
//   /// <summary>
//   /// Tests for rewire function
//   /// </summary>
//   public abstract class RewireServiceLocationTests : ContainerUsingTests
//   {
//     private const string KEY_REGISTER_TEST_CLASS = "tclass";
//     private const LifeStyles LIFESTYLE_TEST_CLASS = LifeStyles.Transient;
//
//     /// <summary>
//     /// Tests rewire functions
//     /// </summary>
//     [Test]
//     public void TestRewire()
//     {
//       //basic rewire
//       m_Container.Register<ITestClass, TestClass>();
//       var testInstance1 = m_Container.Resolve<ITestClass>();
//       testInstance1.Should().BeOfType<TestClass>();
//       m_Container.Release(testInstance1);
//       m_Container.Rewire<ITestClass, TestClass, SecondTestClass>();
//       var testInstance2 = m_Container.Resolve<ITestClass>();
//       testInstance2.Should().BeOfType<SecondTestClass>();
//     }
//
//     /// <summary>
//     /// Rewire name parameter
//     /// </summary>
//     [Test]
//     public void TestRewireNamed()
//     {
//       //rewire with name
//       m_Container.Register<ITestClass, TestClass>(KEY_REGISTER_TEST_CLASS);
//       var testInstanceNamed1 = m_Container.Resolve<ITestClass>(KEY_REGISTER_TEST_CLASS);
//       testInstanceNamed1.Should().BeOfType<TestClass>();
//       m_Container.Release(testInstanceNamed1);
//       m_Container.Rewire<ITestClass, TestClass, SecondTestClass>(KEY_REGISTER_TEST_CLASS);
//       var testInstanceNamed2 = m_Container.Resolve<ITestClass>(KEY_REGISTER_TEST_CLASS);
//       testInstanceNamed2.Should().BeOfType<SecondTestClass>();
//     }
//
//     /// <summary>
//     /// Rewire lifestyle parameter
//     /// </summary>
//     [Test]
//     public void TestRewireLifestyle()
//     {
//       //rewire with lifestyle
//       m_Container.Register<ITestClass, TestClass>(LIFESTYLE_TEST_CLASS, null);
//       var testInstanceLifestyle1 = m_Container.Resolve<ITestClass>();
//       testInstanceLifestyle1.Should().BeOfType<TestClass>();
//       m_Container.Release(testInstanceLifestyle1);
//       m_Container.Rewire<ITestClass, TestClass, SecondTestClass>(LIFESTYLE_TEST_CLASS);
//       var testInstanceLifestyle2 = m_Container.Resolve<ITestClass>();
//       testInstanceLifestyle2.Should().BeOfType<SecondTestClass>();
//     }
//
//     /// <summary>
//     /// Test for rewire if instances exists
//     /// </summary>
//     [Test]
//     public void TestRewireWithInstances()
//     {
//       //rewire should do nothing if there are instances for a registration
//       m_Container.Register<ITestClass, TestClass>();
//       var testInstance1 = m_Container.Resolve<ITestClass>();
//       testInstance1.Should().BeOfType<TestClass>();
//       m_Container.Rewire<ITestClass, TestClass, SecondTestClass>();
//       var testInstance2 = m_Container.Resolve<ITestClass>();
//       testInstance2.Should().NotBeOfType<SecondTestClass>();
//     }
//   }
// }
