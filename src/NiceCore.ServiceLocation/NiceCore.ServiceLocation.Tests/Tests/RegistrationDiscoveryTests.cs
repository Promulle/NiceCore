﻿using FluentAssertions;
using NiceCore.Serialization;
using NiceCore.ServiceLocation.Discovery.Serialization;
using NiceCore.ServiceLocation.Discovery.Serialization.Types;
using NiceCore.ServiceLocation.Tests.DebugImpls;
using NiceCore.ServiceLocation.Tests.Impls.Default;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using NiceCore.ServiceLocation.Tests.Impls.CustomRegisterAttributes;
using NUnit.Framework;

namespace NiceCore.ServiceLocation.Tests.Tests
{
  /// <summary>
  /// Tests for Registration Discovery
  /// </summary>
  public abstract class RegistrationDiscoveryTests : ContainerUsingTests
  {
    private string m_FilePath;

    private static void ApplySerializationSupport(IRegistrationDiscovery t_Discovery)
    {
      t_Discovery.SerializationDiscovery = RegistrationSerialization.GetDefaultDiscovery();
    }

    /// <summary>
    /// Setup the discovery instance
    /// </summary>
    [SetUp]
    public void SetupDiscoveryInstance()
    {
      ApplySerializationSupport(m_Discovery);
      m_FilePath = Path.GetDirectoryName(GetType().Assembly.Location) + Path.DirectorySeparatorChar + "nicecore" + SerializationConstants.SERIAL_FILE_EXTENSION;
      if (File.Exists(m_FilePath))
      {
        File.Delete(m_FilePath);
      }
      m_Discovery.SerializationDiscovery.Should().NotBeNull();
    }

    /// <summary>
    /// Test custom register attribute
    /// </summary>
    [Test]
    public void TestCustomRegisterAttributes()
    {
      ApplyCustomAttributeHandlers(new TestCustomRegisterAttributeHandler());
      m_Discovery.Discover();
      var registeredInstance = m_Container.Resolve<CustomRegisteredClass>();
      registeredInstance.Should().NotBeNull();
      RemoveCustomAttributeHandlers();
    }

    /// <summary>
    /// Apply Custom Attribute handlers
    /// </summary>
    protected abstract void ApplyCustomAttributeHandlers(TestCustomRegisterAttributeHandler i_Handler);
    
    /// <summary>
    /// Apply Custom Attribute handlers
    /// </summary>
    protected abstract void RemoveCustomAttributeHandlers();

    /// <summary>
    /// Tests reading registrations from serialization
    /// </summary>
    [Test]
    public void ReadSerializedRegistrations()
    {
      var regs = CreateRegistration();
      using (var file = File.OpenWrite(m_FilePath))
      {
        BinarySerialization.Serialize(regs, file);
      }
      m_Discovery.Discover();
      m_Inspector.HasImplementation<ITestClass>()
        .Should()
        .BeTrue();
      m_Inspector.HasImplementation<ISingletonTest>()
        .Should()
        .BeTrue();
    }

    private SerialRegistrations CreateRegistration()
    {
      var regs = new SerialRegistrations();
      regs.RegistrationsByAssembly.Add(new SerialAssembly() { FilePath = GetType().Assembly.Location, SerializedTime = DateTime.Now }, new List<SerializedRegistrationInfo>()
      {
        new SerializedRegistrationInfo()
        {
          ImplementationTypeName = typeof(TestClass).AssemblyQualifiedName,
          InterfaceType = typeof(ITestClass).AssemblyQualifiedName,
          RegisterLifestyle = LifeStyles.Transient,
        },
        new SerializedRegistrationInfo()
        {
          ImplementationTypeName = typeof(SingletonTest).AssemblyQualifiedName,
          InterfaceType = typeof(ISingletonTest).AssemblyQualifiedName,
          RegisterLifestyle = LifeStyles.Singleton,
        },
      });
      return regs;
    }

    /// <summary>
    /// Test whether serialization creates a new registrations file
    /// </summary>
    [Test]
    public void SerializationCreateFileIfMissing()
    {
      File.Exists(m_FilePath)
        .Should()
        .BeFalse();
      m_Discovery.Discover();
      File.Exists(m_FilePath)
        .Should()
        .BeTrue();
    }

    /// <summary>
    /// Test serialize and read the serialization correctly back
    /// </summary>
    [Test]
    public void SerializeAndRead()
    {
      m_Discovery.Discover();
      var initialDiscoveredRegistrationsCount = m_Inspector.OverallRegistrationCount();
      //call setup to reset instances
      Setup();
      var serializedRegistrations = Deserialize(m_FilePath);
      WriteToXmlFile(serializedRegistrations);
      RegistrationCount(serializedRegistrations)
        .Should()
        .Be(initialDiscoveredRegistrationsCount);
      ApplySerializationSupport(m_Discovery);
      m_Discovery.Discover();
      m_Inspector.OverallRegistrationCount()
        .Should()
        .Be(initialDiscoveredRegistrationsCount);
    }

    private static SerialRegistrations Deserialize(string i_FilePath)
    {
      using (var file = File.OpenRead(i_FilePath))
      {
        return BinarySerialization.Deserialize<SerialRegistrations>(file);
      }
    }

    private static void WriteToXmlFile(SerialRegistrations i_Registrations)
    {
      var xmlSerializer = new XmlSerializer(typeof(List<RegistrationByAssembly>));
      var filePath = "registrations." + Guid.NewGuid() + ".xml";
      var regList = i_Registrations.RegistrationsByAssembly.Select(r => new RegistrationByAssembly(r)).ToList();
      using (var fileStream = File.OpenWrite(filePath))
      {
        xmlSerializer.Serialize(fileStream, regList);
      }
    }

    private static int RegistrationCount(SerialRegistrations i_Registrations)
    {
      return i_Registrations.RegistrationsByAssembly.Sum(r => r.Value.Count);
    }
  }
}
