using System.Linq;
using NiceCore.ServiceLocation.Discovery.ServiceDescriptors;

namespace NiceCore.ServiceLocation.Tests.General
{
  public static class TestNcServiceCollectionExtensions
  {
    public static int RegistrationCount<T>(this NcServiceCollection i_Collection)
    {
      return i_Collection.Count(r => r.InterfaceType == typeof(T));
    }
  }
}