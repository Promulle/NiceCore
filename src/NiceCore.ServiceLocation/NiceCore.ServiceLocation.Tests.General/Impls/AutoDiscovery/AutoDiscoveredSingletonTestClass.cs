﻿namespace NiceCore.ServiceLocation.Tests.General.Impls.AutoDiscovery
{
  /// <summary>
  /// Testclass auto discovery singleton
  /// </summary>
  [Register(InterfaceType = typeof(IAutoDiscoveredSingletonTestClass), LifeStyle = LifeStyles.Singleton)]
  public class AutoDiscoveredSingletonTestClass : IAutoDiscoveredSingletonTestClass
  {
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }
  }
}
