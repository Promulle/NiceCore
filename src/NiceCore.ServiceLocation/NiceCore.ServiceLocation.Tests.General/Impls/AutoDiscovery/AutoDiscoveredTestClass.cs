﻿namespace NiceCore.ServiceLocation.Tests.General.Impls.AutoDiscovery
{
  /// <summary>
  /// TestClass auto discover
  /// </summary>
  [Register(InterfaceType = typeof(IAutoDiscoveredTestClass))]
  [Register(InterfaceType = typeof(IAttributeTestClass))]
  public class AutoDiscoveredTestClass : IAutoDiscoveredTestClass, IAttributeTestClass
  {
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }
  }
}
