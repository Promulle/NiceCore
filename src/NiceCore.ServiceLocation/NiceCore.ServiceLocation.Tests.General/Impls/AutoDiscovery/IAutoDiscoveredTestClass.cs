﻿namespace NiceCore.ServiceLocation.Tests.General.Impls.AutoDiscovery
{
  /// <summary>
  /// TEST interface
  /// </summary>
  public interface IAutoDiscoveredTestClass
  {
    /// <summary>
    /// Name
    /// </summary>
    string Name { get; set; }
  }
}