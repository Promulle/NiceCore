﻿namespace NiceCore.ServiceLocation.Tests.General.Impls.AutoDiscovery
{
  /// <summary>
  /// Singleton tst class autodiscovered
  /// </summary>
  public interface IAutoDiscoveredSingletonTestClass
  {
    /// <summary>
    /// Name
    /// </summary>
    string Name { get; set; }
  }
}