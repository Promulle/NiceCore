namespace NiceCore.ServiceLocation.Tests.General.Impls.Model
{
  /// <summary>
  /// Override Test Class
  /// </summary>
  public class OverrideTestClass : ITestClass
  {
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }
  }
}