namespace NiceCore.ServiceLocation.Tests.General.Impls.Model
{
  /// <summary>
  /// Test Class
  /// </summary>
  public interface ITestClass
  {
    /// <summary>
    /// Name
    /// </summary>
    string Name { get; set; } 
  }
}