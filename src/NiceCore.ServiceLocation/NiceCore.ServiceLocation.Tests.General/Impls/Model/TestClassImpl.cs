using NiceCore.ServiceLocation.Attributes;

namespace NiceCore.ServiceLocation.Tests.General.Impls.Model
{
  /// <summary>
  /// Impl
  /// </summary>
  [Register(InterfaceType = typeof(ITestClass), LifeStyle = LifeStyles.Singleton)]
  [RegisterAlias(typeof(ITestClass), typeof(ISecondTestClass))]
  public class TestClassImpl : ITestClass, ISecondTestClass
  {
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }
  }
}