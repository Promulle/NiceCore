﻿using System.Collections.Generic;
using NiceCore.ServiceLocation.Discovery;

namespace NiceCore.ServiceLocation.Tests.General.Impls
{
  /// <summary>
  /// Module provider that does not return any file 
  /// </summary>
  public class DummyModuleProvider : IModuleProvider
  {
    /// <summary>
    /// returns empty list of string
    /// </summary>
    /// <param name="i_Directory"></param>
    /// <returns></returns>
    public IEnumerable<string> GetModuleFiles(string i_Directory)
    {
      return new List<string>();
    }
  }
}
