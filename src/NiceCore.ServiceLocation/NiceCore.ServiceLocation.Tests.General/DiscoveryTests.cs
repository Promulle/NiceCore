using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NiceCore.ServiceLocation.Discovery;
using NiceCore.ServiceLocation.Discovery.DefaultDirectoryProviders;
using NiceCore.ServiceLocation.Tests.General.Impls;
using NiceCore.ServiceLocation.Tests.General.Impls.AutoDiscovery;
using NiceCore.ServiceLocation.Tests.General.Impls.Model;
using NUnit.Framework;

namespace NiceCore.ServiceLocation.Tests.General
{
  [TestFixture]
  public class DiscoveryTests
  {
    [Test]
    public void TestDiscoveryInAssembly()
    {
      var discovery = new DefaultRegistrationDiscovery();
      var services = discovery.Discover(new DiscoveryRunConfiguration()
      {
        DiscoveryDefaultDirectoryProvider = new ExecutingAssemblyDefaultDirectoryProvider()
      });
      services.RegistrationCount<IAutoDiscoveredTestClass>().Should().Be(1);
    }
    
    /// <summary>
    /// Tests
    /// </summary>
    [Test]
    public void DiscoveryInLoadedAssemblies()
    {
      var discovery = new DefaultRegistrationDiscovery();
      var regs = discovery.Discover(new DiscoveryRunConfiguration()
      {
        AllowSearchOfLoadedAssemblies = true,
        ModuleProvider = new DummyModuleProvider(),
        TargetMode = DiscoveryModes.Assemblies,
        AssemblyTargetMode = AssemblyProvidingModes.All
      });
      regs.RegistrationCount<IAttributeTestClass>().Should().BeGreaterThan(0);
    }

    /// <summary>
    /// Tests
    /// </summary>
    [Test]
    public void DiscoveryInLoadedAssembliesNoRegistrations()
    {
      var discovery = new DefaultRegistrationDiscovery();
      var regs = discovery.Discover(new DiscoveryRunConfiguration()
      {
        AllowSearchOfLoadedAssemblies = false,
        ModuleProvider = new DummyModuleProvider(),
        TargetMode = DiscoveryModes.Assemblies,
        AssemblyTargetMode = AssemblyProvidingModes.All
      });
      regs.RegistrationCount<IAttributeTestClass>().Should().Be(0);
    }
    
    /// <summary>
    /// Test Override registrations
    /// </summary>
    [Test]
    public void TestOverrideRegistrations()
    {
      var regDiscovery = new DefaultRegistrationDiscovery();
      var services = regDiscovery.Discover(new DiscoveryRunConfiguration()
      {
        OverrideRegistrations = new List<DiscoveryOverrideRegistration>()
        {
          new()
          {
            RegistrationType = typeof(ITestClass),
            ImplementationType = typeof(OverrideTestClass)
          }
        }
      });
      var testClassReg = services.FirstOrDefault(r => r.InterfaceType == typeof(ITestClass));
      testClassReg.Should().NotBeNull();
      testClassReg!.ImplementorType.Should().Be(typeof(OverrideTestClass));
    }
  }
}