using System;
using System.Collections.Generic;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using NiceCore.ServiceLocation.Discovery.ServiceDescriptors;
using NiceCore.ServiceLocation.Tests.General.Impls.AutoDiscovery;
using NUnit.Framework;

namespace NiceCore.ServiceLocation.Tests.General
{
  [TestFixture]
  [Parallelizable]
  public class NcServiceCollectionTests
  {
    [Test]
    [Parallelizable]
    public void TestAdd()
    {
      var coll = new NcServiceCollection
      {
        new()
        {
          InterfaceType = typeof(string),
          ImplementorType = typeof(string)
        }
      };
      coll.Should().HaveCount(1);
    }

    [Test]
    [Parallelizable]
    public void TestTryAdd()
    {
      var coll = new NcServiceCollection();
      var addedFirst = coll.TryAdd(new()
      {
        InterfaceType = typeof(string),
        ImplementorType = typeof(string)
      });
      addedFirst.Should().BeTrue();
      coll.Should().HaveCount(1);
      var addedSecond = coll.TryAdd(new()
      {
        InterfaceType = typeof(string),
        ImplementorType = typeof(string)
      });
      addedSecond.Should().BeFalse();
      coll.Should().HaveCount(1);
    }

    [Test]
    [Parallelizable]
    public void TestEnumerate()
    {
      var coll = new NcServiceCollection
      {
        new()
        {
          InterfaceType = typeof(string),
          ImplementorType = typeof(string)
        },
        new()
        {
          InterfaceType = typeof(string),
          ImplementorType = typeof(string)
        }
      };
      var resList = new List<Type>();
      foreach (var descriptor in coll)
      {
        resList.Add(descriptor.InterfaceType);
      }

      resList.Should().HaveCount(2);
    }

    [Test]
    [Parallelizable]
    public void TestRemoveByInterface()
    {
      var coll = new NcServiceCollection
      {
        new()
        {
          InterfaceType = typeof(string),
          ImplementorType = typeof(string)
        },
        new()
        {
          InterfaceType = typeof(string),
          ImplementorType = typeof(string)
        }
      };
      coll.Should().HaveCount(2);
      coll.RemoveByInterfaceType(typeof(string));
      coll.Should().HaveCount(0);
    }

    [Test]
    [Parallelizable]
    public void TestServiceCollectionAdapter()
    {
      var coll = new NcServiceCollection()
      {
        new()
        {
          InterfaceType = typeof(string),
          ImplementorType = typeof(string)
        }
      };
      var serviceCollection = coll.GetViewAsServiceCollection();
      serviceCollection.Should().NotBeNull().And.HaveCount(1);
      serviceCollection.AddSingleton<AutoDiscoveredTestClass>();
      serviceCollection.Should().HaveCount(2);
      coll.Should().HaveCount(2);
    }

    [Test]
    [Parallelizable]
    public void TestDescriptorConvert()
    {
      var serviceDescriptor = ServiceDescriptor.Singleton<IAutoDiscoveredTestClass, AutoDiscoveredTestClass>();
      var ncDescriptor = NcServiceRegistrationDescriptor.FromServiceDescriptor(serviceDescriptor);
      ncDescriptor.Should().NotBeNull();
      ncDescriptor.Instance.Should().BeNull();
      ncDescriptor.AliasAttributes.Should().BeEmpty();
      ncDescriptor.CustomLifestyleName.Should().BeNull();
      ncDescriptor.ServiceLifestyle.Should().Be(LifeStyles.Singleton);
      ncDescriptor.InterfaceType.Should().Be(typeof(IAutoDiscoveredTestClass));
      ncDescriptor.ImplementorType.Should().Be(typeof(AutoDiscoveredTestClass));

      var serviceDescriptor2 = ncDescriptor.ToServiceDescriptor();
      serviceDescriptor2.Should().NotBeNull();
      serviceDescriptor2.ImplementationInstance.Should().BeNull();
      serviceDescriptor2.ServiceType.Should().Be(typeof(IAutoDiscoveredTestClass));
      serviceDescriptor2.ImplementationType.Should().Be(typeof(AutoDiscoveredTestClass));
      serviceDescriptor2.Lifetime.Should().Be(ServiceLifetime.Singleton);
      serviceDescriptor2.ImplementationFactory.Should().BeNull();
    }
  }
}