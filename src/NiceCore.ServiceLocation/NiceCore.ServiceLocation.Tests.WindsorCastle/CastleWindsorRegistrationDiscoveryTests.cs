using System.IO;
using System.Reflection;
using FluentAssertions;
using NiceCore.ServiceLocation.CastleWindsor;
using NiceCore.ServiceLocation.Discovery;
using NiceCore.ServiceLocation.Discovery.DefaultDirectoryProviders;
using NiceCore.ServiceLocation.Tests.CastleWindsor.Model;
using NUnit.Framework;

namespace NiceCore.ServiceLocation.Tests.CastleWindsor
{
  /// <summary>
  /// Tests for registration discovery related to castle windsor container
  /// </summary>
  [TestFixture]
  public class CastleWindsorRegistrationDiscoveryTests 
  {
    /// <summary>
    /// Test Alias Registration
    /// </summary>
    [Test]
    public void TestAliasRegistration()
    {
      var regDiscovery = new DefaultRegistrationDiscovery();
      var container = new CastleWindsorServiceContainer();
      var services = regDiscovery.Discover(new DiscoveryRunConfiguration()
      {
        DiscoveryDefaultDirectoryProvider = new ExecutingAssemblyDefaultDirectoryProvider()
      });
      container.RegisterServiceCollection(services);
      var inst = container.Resolve<ITestClass>();
      inst.Name = "Modified";
      var secondInst = container.Resolve<ISecondTestClass>();
      inst.Should().NotBeNull();
      inst.Should().Be(secondInst);
    }
  }
}