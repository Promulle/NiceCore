﻿using FluentAssertions;
using NiceCore.Extensions;
using NiceCore.ServiceLocation.CastleWindsor;
using NiceCore.ServiceLocation.Plugins;
using NiceCore.ServiceLocation.Tests.Impls;
using NiceCore.ServiceLocation.Tests.Impls.Default;
using System.Linq;
using NiceCore.ServiceLocation.Helpers;
using NUnit.Framework;

namespace NiceCore.ServiceLocation.Tests.CastleWindsor
{
  /// <summary>
  /// Tests for castle windsor impl
  /// </summary>
  [TestFixture]
  public class ServiceLocationTests
  {
    /// <summary>
    /// Tests register/resolve
    /// </summary>
    [Test]
#pragma warning disable CA1822 // Should not be static -> test method
    public void RegisterResolve()
#pragma warning restore CA1822 // Should not be static -> test method
    {
      var container = new CastleWindsorServiceContainer();
      container.Initialize(null);
      container.Register<ITestClass, TestClass>();
      var inst = container.Resolve<ITestClass>();
      container.HasImplementationOf<ITestClass>().Should().BeTrue();
      inst.Should().NotBeNull();
      inst.Should().BeOfType<TestClass>();
    }

    /// <summary>
    /// Tests the plugins
    /// </summary>
    [Test]
#pragma warning disable CA1822 // Should not be static -> test method
    public void Plugins()
#pragma warning restore CA1822 // Should not be static -> test method
    {
      var container = new CastleWindsorServiceContainer();
      container.Initialize(null);
      container.Plugins.Should().NotBeNull();
      var testPluginInst = new TestInjectionPlugin();
      testPluginInst.ResolveCalled.Should().BeFalse();
      testPluginInst.ReleaseCalled.Should().BeFalse();
      new IDependencyInjectionLifecyclePlugin[]
      {
        new ReleaseOnDisposePlugin(),
        new InitializePlugin(),
        testPluginInst,
      }.ForEachItem(r => container.Plugins.Add(r));
      container.Plugins.Count.Should().Be(3);
      container.EnablePlugins();
      container.Plugins.Any(r => !r.IsEnabled).Should().BeFalse();
      container.Register<ITestClass, TestClass>();
      var testInst = container.Resolve<ITestClass>();
      container.Release(testInst);
      testPluginInst.ResolveCalled.Should().BeTrue();
      testPluginInst.ReleaseCalled.Should().BeTrue();
    }

    /// <summary>
    /// Test On Demand Properties
    /// </summary>
    [Test]
    public void TestOnDemandProperties()
    {
      var container = new CastleWindsorServiceContainer();
      container.Initialize(null);
      var onDemandProperty = new OnDemandResolvedInstance<ITestClass>(container);
      var nullInstance = onDemandProperty.GetInstance();
      nullInstance.Should().BeNull();
      container.Register<ITestClass, TestClass>();
      var actualInstance = onDemandProperty.GetInstance();
      actualInstance.Should().NotBeNull();
    }
  }
}
