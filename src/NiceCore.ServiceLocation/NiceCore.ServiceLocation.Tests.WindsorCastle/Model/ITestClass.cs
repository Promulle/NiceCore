namespace NiceCore.ServiceLocation.Tests.CastleWindsor.Model
{
  /// <summary>
  /// Test Class
  /// </summary>
  public interface ITestClass
  {
    /// <summary>
    /// Name
    /// </summary>
    string Name { get; set; } 
  }
}