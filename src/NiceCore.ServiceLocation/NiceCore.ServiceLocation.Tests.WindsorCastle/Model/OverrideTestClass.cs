namespace NiceCore.ServiceLocation.Tests.CastleWindsor.Model
{
  /// <summary>
  /// Override Test Class
  /// </summary>
  public class OverrideTestClass : ITestClass
  {
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }
  }
}