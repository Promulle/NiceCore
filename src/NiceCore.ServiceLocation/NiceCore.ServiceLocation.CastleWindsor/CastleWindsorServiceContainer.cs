﻿using Castle.Core;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;
using Castle.Windsor.MsDependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using NiceCore.Base;
using NiceCore.Extensions;
using NiceCore.ServiceLocation.Lifestyles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Logging;
using NiceCore.Diagnostics;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation.Attributes;
using NiceCore.ServiceLocation.Container;
using NiceCore.ServiceLocation.Discovery;
using NiceCore.ServiceLocation.Discovery.ServiceDescriptors;

namespace NiceCore.ServiceLocation.CastleWindsor
{
  /// <summary>
  /// Impl of IServiceContainer using Castle.Windsor library
  /// </summary>
  public class CastleWindsorServiceContainer : BaseDisposable, IServiceContainer
  {
    private readonly object m_LockObject = new();

    private static readonly Dictionary<string, ICustomLifestyle> m_SupportedCustomLifestyles = new()
    {
      {CustomLifestyles.MS_SCOPED, new CastleWindsorMsScopedLifestyle()}
    };

    public WindsorContainer ContainerInstance { get; }

    /// <summary>
    /// Plugins
    /// </summary>
    public IList<IDependencyInjectionLifecyclePlugin> Plugins { get; } = new List<IDependencyInjectionLifecyclePlugin>();

    /// <summary>
    /// Initialized
    /// </summary>
    public bool IsInitialized { get; protected set; }

    /// <summary>
    /// Logger
    /// </summary>
    public ILogger Logger { get; set; } = DiagnosticsCollector.Instance.GetLogger<CastleWindsorServiceContainer>();

    /// <summary>
    /// Default ctor, uses new instance of windsor container
    /// </summary>
    public CastleWindsorServiceContainer() : this(new())
    {
    }

    /// <summary>
    /// Constructor setting container
    /// </summary>
    /// <param name="i_Container"></param>
    public CastleWindsorServiceContainer(WindsorContainer i_Container)
    {
      ContainerInstance = i_Container;
    }

    /// <summary>
    /// Sets the logger
    /// </summary>
    /// <param name="i_Logger"></param>
    public void SetLogger(ILogger i_Logger)
    {
      Logger = i_Logger;
    }

    /// <summary>
    /// Enabled Plugins
    /// </summary>
    public void EnablePlugins()
    {
      foreach (var plugin in Plugins)
      {
        plugin.EnablePlugin(this);
      }
    }

    /// <summary>
    /// Drop impls for T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public bool Drop<T>()
    {
      throw new NotSupportedException("Dropping registrations is not supported in CastleWindsor.");
    }

    /// <summary>
    /// Drop impls for T defined by i_Name
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    public bool Drop<T>(string i_Name)
    {
      throw new NotSupportedException("Dropping registrations is not supported in CastleWindsor.");
    }

    /// <summary>
    /// Drop registered instance i_Instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Instance"></param>
    /// <returns></returns>
    public bool Drop<T>(T i_Instance)
    {
      throw new NotSupportedException("Dropping registrations is not supported in CastleWindsor.");
    }

    /// <summary>
    /// Clear registrations
    /// </summary>
    public void Clear()
    {
      throw new NotSupportedException("Clearing registrations is not supported in CastleWindsor.");
    }

    

    /// <summary>
    /// Checks if there is an impl for i_Type
    /// </summary>
    /// <param name="i_Type"></param>
    /// <returns></returns>
    public bool HasImplementationOf(Type i_Type)
    {
      return ContainerInstance.Kernel.HasComponent(i_Type);
    }

    /// <summary>
    /// Initialize
    /// </summary>
    public void Initialize(NcServiceCollection t_Collection)
    {
      lock (m_LockObject)
      {
        if (!IsInitialized)
        {
          ContainerInstance.Kernel.ComponentCreated += OnComponentCreated;
          ContainerInstance.Kernel.Resolver.AddSubResolver(new ListResolver(ContainerInstance.Kernel));
          t_Collection?.AddInstance<IServiceContainer>(this);
          EnablePlugins();
          this.RegisterServiceCollection(t_Collection);

          IsInitialized = true;
        }
      }
    }

    private void OnComponentCreated(ComponentModel i_Model, object i_Instance)
    {
      if (i_Instance != null)
      {
        PluginLifecycleResolve(i_Instance, i_Instance.GetType());
      }
    }

    #region Register
    
    /// <summary>
    /// Register by type, impl lifestyle and name
    /// </summary>
    /// <param name="i_InterfaceTypes"></param>
    /// <param name="i_ImplementingType"></param>
    /// <param name="i_Lifestyle"></param>
    /// <param name="i_CustomLifestyleName">Name of custom lifestyle to use, only applicable if i_Lifestyle == LifeStyles.Custom</param>
    public void Register(IReadOnlyCollection<Type> i_InterfaceTypes, Type i_ImplementingType, LifeStyles i_Lifestyle, string i_CustomLifestyleName)
    {
      ContainerInstance.Register(CreateRegistration(i_InterfaceTypes, i_ImplementingType, i_Lifestyle, i_CustomLifestyleName, null));
    }

    private static ComponentRegistration<object> ApplyLifestyle(ComponentRegistration<object> t_Registration, LifeStyles i_Lifestyle, string i_CustomLifestyleName)
    {
      if (i_Lifestyle == LifeStyles.Singleton)
      {
        return t_Registration.LifestyleSingleton();
      }
      else if (i_Lifestyle == LifeStyles.Transient)
      {
        return t_Registration.LifestyleTransient();
      }
      else if (i_Lifestyle == LifeStyles.Scoped)
      {
        return t_Registration.LifestyleScoped();
      }
      else if (i_Lifestyle == LifeStyles.Custom)
      {
        if (m_SupportedCustomLifestyles.TryGetValue(i_CustomLifestyleName, out var lifeStyle))
        {
          return lifeStyle.ApplyLifestyle(t_Registration) as ComponentRegistration<object>;
        }

        throw new NotSupportedException($"Custom lifestyle {i_CustomLifestyleName} is not supported. Add an ICustomLifestyle implementation for it to fix this.");
      }
      else
      {
        throw new NotSupportedException($"Lifestyle {i_Lifestyle} is not supported.");
      }
    }

    private static ComponentRegistration<object> CreateRegistration(IReadOnlyCollection<Type> i_InterfaceTypes, Type i_ImplementingType, LifeStyles i_Lifestyle, string i_CustomLifestyleName, object i_Instance)
    {
      return ApplyLifestyle(ApplyInstanceOrImplementingType(Component.For(i_InterfaceTypes).IsDefault(), i_InterfaceTypes, i_ImplementingType, i_Instance), i_Lifestyle, i_CustomLifestyleName);
    }

    private static ComponentRegistration<object> ApplyInstanceOrImplementingType(ComponentRegistration<object> t_Registration, IReadOnlyCollection<Type> i_InterfaceTypes, Type i_ImplementingType, object i_Instance)
    {
      var name = string.Join('_', i_InterfaceTypes) + "_" + i_ImplementingType; 
      if (i_Instance != null)
      {
        return t_Registration.Instance(i_Instance).Named(name);
      }

      return t_Registration.ImplementedBy(i_ImplementingType).Named(name);
    }

    /// <summary>
    /// Register instance
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_InterfaceType"></param>
    /// <param name="i_LifeStyle"></param>
    /// <param name="i_CustomLifestyleName"></param>
    public void RegisterInstance(object i_Instance, Type i_InterfaceType, LifeStyles i_LifeStyle = LifeStyles.Transient, string i_CustomLifestyleName = null)
    {
      var instReg = CreateRegistration(new List<Type>() {i_InterfaceType}, i_Instance.GetType(), i_LifeStyle, i_CustomLifestyleName, i_Instance);
      ContainerInstance.Register(instReg);
    }

    #endregion

    /// <summary>
    /// Release i_Instance from the container
    /// </summary>
    /// <param name="i_Instance"></param>
    public void Release(object i_Instance)
    {
      ContainerInstance.Release(i_Instance);
      PluginLifecycleRelease(i_Instance);
    }

    /// <summary>
    /// No change in behavior since I do not think that castle supports releasing singletons
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_ReleaseSingleton"></param>
    public void Release(object i_Instance, bool i_ReleaseSingleton)
    {
      Release(i_Instance);
    }

    /// <summary>
    /// Releae all
    /// </summary>
    public void ReleaseAll()
    {
      Array.ForEach(ContainerInstance.Kernel.GetAssignableHandlers(typeof(object)), reg =>
      {
        var serviceType = reg.ComponentModel.Services.FirstOrDefault();
        if (serviceType != null)
        {
          ContainerInstance.ResolveAll(serviceType).OfType<object>().ForEachItem(inst => Release(inst));
        }
      });
    }

    #region Resolve

    /// <summary>
    /// Resolve by type
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public T Resolve<T>()
    {
      if (HasImplementationOf(typeof(T)))
      {
        return ContainerInstance.Resolve<T>();
      }

      Logger.Debug($"ServiceLocation Castle.Windsor - Trying to resolve but no component found for {typeof(T).FullName}");
      return default;
    }

    /// <summary>
    /// Resolve by i_Type, casts result to T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Type"></param>
    /// <returns></returns>
    public T Resolve<T>(Type i_Type)
    {
      if (HasImplementationOf(i_Type))
      {
        return (T) ContainerInstance.Resolve(i_Type);
      }

      Logger.Debug($"ServiceLocation Castle.Windsor - Trying to resolve but no component found for {i_Type.FullName}");
      return default;
    }

    /// <summary>
    /// Resolve by interface type and name
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    public T Resolve<T>(string i_Name)
    {
      if (ContainerInstance.Kernel.HasComponent(i_Name))
      {
        return ContainerInstance.Resolve<T>(i_Name);
      }

      Logger.Debug($"ServiceLocation Castle.Windsor - Trying to resolve but no component found for type {typeof(T).FullName} with name: {i_Name}");
      return default;
    }

    /// <summary>
    /// Resolve all
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public IEnumerable<T> ResolveAll<T>()
    {
      if (HasImplementationOf(typeof(T)))
      {
        return ContainerInstance.ResolveAll<T>();
      }

      Logger.Debug($"ServiceLocation Castle.Windsor - Trying to resolve all but no component found for {typeof(T).FullName}");
      return Enumerable.Empty<T>();
    }

    /// <summary>
    /// Resolve all and casts to T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public IEnumerable<T> ResolveAll<T>(Type i_InterfaceType)
    {
      if (HasImplementationOf(i_InterfaceType))
      {
        return ContainerInstance.Kernel.ResolveAll(i_InterfaceType).OfType<T>();
      }

      Logger.Debug($"ServiceLocation Castle.Windsor - Trying to resolve all but no component found for {typeof(T).FullName}");
      return Enumerable.Empty<T>();
    }

    #endregion

    #region Rewire

    /// <summary>
    /// Not supported
    /// </summary>
    /// <typeparam name="TInterfaceType"></typeparam>
    /// <typeparam name="TOldImplementorType"></typeparam>
    /// <typeparam name="TNewImplementorType"></typeparam>
    public void Rewire<TInterfaceType, TOldImplementorType, TNewImplementorType>()
    {
      throw new NotSupportedException("Rewiring registrations is not supported in CastleWindsor.");
    }

    /// <summary>
    /// Not supported
    /// </summary>
    /// <typeparam name="TInterfaceType"></typeparam>
    /// <typeparam name="TOldImplementorType"></typeparam>
    /// <typeparam name="TNewImplementorType"></typeparam>
    /// <param name="i_Name"></param>
    public void Rewire<TInterfaceType, TOldImplementorType, TNewImplementorType>(string i_Name)
    {
      throw new NotSupportedException("Rewiring registrations is not supported in CastleWindsor.");
    }

    /// <summary>
    /// Not supported
    /// </summary>
    /// <typeparam name="TInterfaceType"></typeparam>
    /// <typeparam name="TOldImplementorType"></typeparam>
    /// <typeparam name="TNewImplementorType"></typeparam>
    /// <param name="i_Name"></param>
    /// <param name="i_Lifestyle"></param>
    public void Rewire<TInterfaceType, TOldImplementorType, TNewImplementorType>(string i_Name, LifeStyles i_Lifestyle)
    {
      throw new NotSupportedException("Rewiring registrations is not supported in CastleWindsor.");
    }

    /// <summary>
    /// Not Supported
    /// </summary>
    /// <typeparam name="TInterfaceType"></typeparam>
    /// <typeparam name="TOldImplementorType"></typeparam>
    /// <typeparam name="TNewImplementorType"></typeparam>
    /// <param name="i_Lifestyle"></param>
    public void Rewire<TInterfaceType, TOldImplementorType, TNewImplementorType>(LifeStyles i_Lifestyle)
    {
      throw new NotSupportedException("Rewiring registrations is not supported in CastleWindsor.");
    }

    #endregion

    private void PluginLifecycleResolve(object t_Instance, Type i_InterfaceType)
    {
      foreach (var plugin in Plugins)
      {
        plugin.InstanceResolved(t_Instance, i_InterfaceType);
      }
    }

    private void PluginLifecycleRelease(object t_Instance)
    {
      foreach (var plugin in Plugins)
      {
        plugin.InstanceReleased(t_Instance);
      }
    }

    /// <summary>
    /// Dispose
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    protected override void Dispose(bool i_IsDisposing)
    {
      ContainerInstance.Kernel.ComponentCreated -= OnComponentCreated;
      ContainerInstance.Dispose();
      base.Dispose(i_IsDisposing);
    }

    /// <summary>
    /// Returns a ms compatible service provider
    /// </summary>
    /// <param name="i_Services"></param>
    /// <returns></returns>
    public IServiceProvider ToServiceProvider(IServiceCollection i_Services)
    {
      return WindsorRegistrationHelper.CreateServiceProvider(ContainerInstance, i_Services);
    }
  }
}