﻿using Castle.MicroKernel.Registration;
using Castle.Windsor.MsDependencyInjection;
using NiceCore.ServiceLocation.Lifestyles;

namespace NiceCore.ServiceLocation.CastleWindsor
{
  /// <summary>
  /// Lifestyle used for castle windsor registrations
  /// </summary>
  public class CastleWindsorMsScopedLifestyle : ICustomLifestyle
  {
    /// <summary>
    /// Name of Ms Scoped Lifestyle
    /// </summary>
    public string Name { get; } = CustomLifestyles.MS_SCOPED;

    /// <summary>
    /// Apply
    /// </summary>
    /// <param name="i_Registration"></param>
    /// <returns></returns>
    public object ApplyLifestyle(object i_Registration)
    {
      var registration = i_Registration as ComponentRegistration<object>;
      return registration.LifestyleCustom<MsScopedLifestyleManager>();
    }
  }
}
