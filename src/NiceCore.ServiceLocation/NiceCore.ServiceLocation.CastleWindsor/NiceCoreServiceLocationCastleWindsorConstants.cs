﻿namespace NiceCore.ServiceLocation.CastleWindsor
{
  /// <summary>
  /// Constants for castle windsor
  /// </summary>
  public static class NiceCoreServiceLocationCastleWindsorConstants
  {
    /// <summary>
    /// Assembly name
    /// </summary>
    public static readonly string ASSEMBLY_NAME = "NiceCore.ServiceLocation.CastleWindsor";
  }
}
