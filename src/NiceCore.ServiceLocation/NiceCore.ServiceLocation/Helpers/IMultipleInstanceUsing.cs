﻿using System;
using System.Collections.Generic;

namespace NiceCore.ServiceLocation.Helpers
{
  /// <summary>
  /// InstanceUsing for multiple registrations
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public interface IMultipleInstanceUsing<T> : IDisposable
  {
    /// <summary>
    /// Returns the resolved instance
    /// </summary>
    /// <returns></returns>
    IEnumerable<T> Instances();

    /// <summary>
    /// Whether an instance could be resolved
    /// </summary>
    /// <returns></returns>
    bool HasInstances();
  }
}
