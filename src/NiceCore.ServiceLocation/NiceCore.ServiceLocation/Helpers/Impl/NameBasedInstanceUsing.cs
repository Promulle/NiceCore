﻿using System;

namespace NiceCore.ServiceLocation.Helpers.Impl
{
  /// <summary>
  /// Instance using based on Name
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public class NameBasedInstanceUsing<T> : BaseInstanceUsing, IInstanceUsing<T>
  {
    private readonly string m_ServiceName;
    private readonly Lazy<T> m_Instance;

    /// <summary>
    /// Constructor setting container and Name
    /// </summary>
    /// <param name="t_Container"></param>
    /// <param name="i_Name"></param>
    public NameBasedInstanceUsing(IServiceContainer t_Container, string i_Name) : base(t_Container)
    {
      m_ServiceName = i_Name;
      m_Instance = new Lazy<T>(Resolve(this));
    }

    private static T Resolve(NameBasedInstanceUsing<T> t_Using)
    {
      var inst = t_Using.m_Container.Resolve<T>(t_Using.m_ServiceName);
      t_Using.m_ResolveCalled = true;
      return inst;
    }

    /// <summary>
    /// Wether an instance was resolved
    /// </summary>
    /// <returns></returns>
    public bool HasInstance()
    {
      return m_Instance.Value != null;
    }

    /// <summary>
    /// Returns the resolved instance
    /// </summary>
    /// <returns></returns>
    public T Instance()
    {
      return m_Instance.Value;
    }

    /// <summary>
    /// Release registration
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    protected override void Dispose(bool i_IsDisposing)
    {
      //only resolve if resolve has actually been called
      if (m_ResolveCalled)
      {
        if (HasInstance())
        {
          m_Container.Release(Instance());
        }
      }
      base.Dispose(i_IsDisposing);
    }
  }
}
