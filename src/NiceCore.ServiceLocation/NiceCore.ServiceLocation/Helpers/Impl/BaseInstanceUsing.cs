﻿using NiceCore.Base;

namespace NiceCore.ServiceLocation.Helpers.Impl
{
  /// <summary>
  /// base impl for IIinstanceUsing
  /// </summary>
  public abstract class BaseInstanceUsing : BaseDisposable
  {
    /// <summary>
    /// Container for registration
    /// </summary>
    protected readonly IServiceContainer m_Container;

    /// <summary>
    /// Whether resolve has been called
    /// </summary>
    protected bool m_ResolveCalled;

    /// <summary>
    /// Constructor setting container
    /// </summary>
    /// <param name="t_Container"></param>
    protected BaseInstanceUsing(IServiceContainer t_Container)
    {
      m_Container = t_Container;
    }
  }
}
