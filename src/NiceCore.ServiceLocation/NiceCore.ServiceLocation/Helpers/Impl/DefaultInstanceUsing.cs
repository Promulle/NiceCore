﻿using System;

namespace NiceCore.ServiceLocation.Helpers.Impl
{
  /// <summary>
  /// Default impl for IInstanceUsing
  /// </summary>
  public class DefaultInstanceUsing<T> : BaseInstanceUsing, IInstanceUsing<T>
  {
    private readonly Lazy<T> m_Instance;

    /// <summary>
    /// Constructor setting container
    /// </summary>
    /// <param name="t_Container"></param>
    public DefaultInstanceUsing(IServiceContainer t_Container) : base(t_Container)
    {
      m_Instance = new Lazy<T>(Resolve(this));
    }

    /// <summary>
    /// Wether an instance was resolved
    /// </summary>
    /// <returns></returns>
    public bool HasInstance()
    {
      return m_Instance.Value != null;
    }

    /// <summary>
    /// Returns the resolved instance
    /// </summary>
    /// <returns></returns>
    public T Instance()
    {
      return m_Instance.Value;
    }

    private static T Resolve(DefaultInstanceUsing<T> t_Using)
    {
      var inst = t_Using.m_Container.Resolve<T>();
      t_Using.m_ResolveCalled = true;
      return inst;
    }

    /// <summary>
    /// Release registration
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    protected override void Dispose(bool i_IsDisposing)
    {
      //only resolve if resolve has actually been called
      if (m_ResolveCalled)
      {
        if (HasInstance())
        {
          m_Container.Release(Instance());
        }
      }
      base.Dispose(i_IsDisposing);
    }
  }
}
