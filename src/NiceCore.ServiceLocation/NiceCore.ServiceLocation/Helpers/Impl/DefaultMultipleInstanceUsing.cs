﻿using NiceCore.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NiceCore.ServiceLocation.Helpers.Impl
{
  /// <summary>
  /// Default impl for IMultiInstanceUsing
  /// </summary>
  public class DefaultMultipleInstanceUsing<T> : BaseInstanceUsing, IMultipleInstanceUsing<T>
  {
    private readonly Lazy<IEnumerable<T>> m_Instance;

    /// <summary>
    /// Constructor setting container
    /// </summary>
    /// <param name="t_Container"></param>
    public DefaultMultipleInstanceUsing(IServiceContainer t_Container) : base(t_Container)
    {
      m_Instance = new Lazy<IEnumerable<T>>(ResolveAll(this));
    }

    /// <summary>
    /// Wether an instance was resolved
    /// </summary>
    /// <returns></returns>
    public bool HasInstances()
    {
      return m_Instance.Value?.Any() == true;
    }

    /// <summary>
    /// Returns the resolved instance
    /// </summary>
    /// <returns></returns>
    public IEnumerable<T> Instances()
    {
      return m_Instance.Value;
    }

    private static IEnumerable<T> ResolveAll(DefaultMultipleInstanceUsing<T> t_Using)
    {
      var inst = t_Using.m_Container.ResolveAll<T>();
      t_Using.m_ResolveCalled = true;
      return inst;
    }

    /// <summary>
    /// Release registration
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    protected override void Dispose(bool i_IsDisposing)
    {
      if (m_ResolveCalled)
      {
        Instances().ForEachItem(r => m_Container.Release(r));
      }
      base.Dispose(i_IsDisposing);
    }
  }
}
