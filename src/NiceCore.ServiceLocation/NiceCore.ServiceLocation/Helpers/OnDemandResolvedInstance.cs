using System.Collections.Generic;

namespace NiceCore.ServiceLocation.Helpers
{
  /// <summary>
  /// Class that resolves the instance only when needed
  /// </summary>
  public sealed class OnDemandResolvedInstance<T>
  {
    private readonly object m_Lock = new();
    private readonly IServiceContainer m_Container;
    private T m_Instance;

    /// <summary>
    /// On Demand Resolved Instance ctor filling container needed to resolve the instance
    /// </summary>
    /// <param name="i_Container"></param>
    public OnDemandResolvedInstance(IServiceContainer i_Container)
    {
      m_Container = i_Container;
    }

    /// <summary>
    /// Returns the instance that already was resolved or resolves a new instance
    /// </summary>
    /// <returns></returns>
    public T GetInstance()
    {
      lock (m_Lock)
      {
        if (EqualityComparer<T>.Default.Equals(m_Instance, default))
        {
          m_Instance = m_Container.Resolve<T>();
        }
        return m_Instance;
      }
    }
  }
}