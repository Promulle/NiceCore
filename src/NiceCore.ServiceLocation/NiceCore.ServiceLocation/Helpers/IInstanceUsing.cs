﻿using System;

namespace NiceCore.ServiceLocation.Helpers
{
  /// <summary>
  /// Using for resolved instances
  /// </summary>
  public interface IInstanceUsing<T> : IDisposable
  {
    /// <summary>
    /// Returns the resolved instance
    /// </summary>
    /// <returns></returns>
    T Instance();

    /// <summary>
    /// Whether an instance could be resolved
    /// </summary>
    /// <returns></returns>
    bool HasInstance();
  }
}
