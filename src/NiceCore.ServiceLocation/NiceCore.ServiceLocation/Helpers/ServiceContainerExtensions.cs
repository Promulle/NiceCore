﻿using NiceCore.ServiceLocation.Helpers.Impl;

namespace NiceCore.ServiceLocation.Helpers
{
  /// <summary>
  /// Extension methods for ServiceContainer
  /// </summary>
  public static class ServiceContainerExtensions
  {
    /// <summary>
    /// return instance that is released once the returned instance is disposed
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Container"></param>
    /// <returns></returns>
    public static IInstanceUsing<T> Locate<T>(this IServiceContainer t_Container)
    {
      return new DefaultInstanceUsing<T>(t_Container);
    }

    /// <summary>
    /// return instance that is released once the returned instance is disposed
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Container"></param>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    public static IInstanceUsing<T> Locate<T>(this IServiceContainer t_Container, string i_Name)
    {
      return new NameBasedInstanceUsing<T>(t_Container, i_Name);
    }

    /// <summary>
    /// return instance that is released once the returned instance is disposed
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Container"></param>
    /// <returns></returns>
    public static IMultipleInstanceUsing<T> LocateAll<T>(this IServiceContainer t_Container)
    {
      return new DefaultMultipleInstanceUsing<T>(t_Container);
    }
  }
}
