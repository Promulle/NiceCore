﻿using Microsoft.Extensions.DependencyInjection;
using NiceCore.Logging;
using System;

namespace NiceCore.ServiceLocation
{
  /// <summary>
  /// Simple service provider factory
  /// </summary>
  public class NcServiceProviderFactory : IServiceProviderFactory<IServiceCollection>
  {
    private readonly IServiceContainer m_Container;

    /// <summary>
    /// Constructor setting container
    /// </summary>
    /// <param name="i_Container"></param>
    public NcServiceProviderFactory(IServiceContainer i_Container)
    {
      if (i_Container == null)
      {
        throw new ArgumentException($"Cannot create a new instance of {typeof(IServiceProvider).Name}, no container supplied. Argument i_Container must not be null.");
      }
      m_Container = i_Container;
    }

    /// <summary>
    /// Just returns i_Services again
    /// </summary>
    /// <param name="i_Services"></param>
    /// <returns></returns>
    public IServiceCollection CreateBuilder(IServiceCollection i_Services)
    {
      return i_Services;
    }

    /// <summary>
    /// Creates the service Provider for the service collection i_Services
    /// </summary>
    /// <param name="i_Services"></param>
    /// <returns></returns>
    public IServiceProvider CreateServiceProvider(IServiceCollection i_Services)
    {
      return m_Container.ToServiceProvider(i_Services);
    }
  }
}
