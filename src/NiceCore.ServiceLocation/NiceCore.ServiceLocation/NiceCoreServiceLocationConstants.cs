﻿namespace NiceCore.ServiceLocation
{
  /// <summary>
  /// Constants for NiceCoreServiceLocation Assembly
  /// </summary>
  public static class NiceCoreServiceLocationConstants
  {
    /// <summary>
    /// Name of Assembly
    /// </summary>
    public static readonly string ASSEMBLY_NAME = "NiceCore.ServiceLocation";
  }
}
