﻿namespace NiceCore.ServiceLocation
{
  /// <summary>
  /// Behavior, if a cyclic dependency is detected
  /// </summary>
  public enum CyclicDependencyBehavior
  {
    /// <summary>
    /// Throw an exception pointing to a cyclic dependency
    /// </summary>
    Exception,
    /// <summary>
    /// Property, which would be cyclic, is not resolved and remains null
    /// </summary>
    DontResolveProperty,
    /// <summary>
    /// Log to WARN if a cycle in dependencies is detected
    /// </summary>
    Log,
  }
}
