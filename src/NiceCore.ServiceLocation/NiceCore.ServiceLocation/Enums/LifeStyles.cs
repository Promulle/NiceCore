﻿namespace NiceCore.ServiceLocation
{
  /// <summary>
  /// All possible lifestyles for ServiceRegistrations
  /// </summary>
  public enum LifeStyles
  {
    /// <summary>
    /// Transient, will always be reinstanced when resolved
    /// </summary>
    Transient,
    /// <summary>
    /// Only one instance of this can exist at a time
    /// </summary>
    Singleton,

    /// <summary>
    /// Only one instance per scope, currently only supported by the castle core wrapper
    /// </summary>
    Scoped,

    /// <summary>
    /// Custom Lifestyle, this should be implemented with a provided name which is used to retrieve the CustomLifestyle that is to be used
    /// </summary>
    Custom,
  }
}
