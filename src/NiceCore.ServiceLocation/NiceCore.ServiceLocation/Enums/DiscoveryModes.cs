﻿namespace NiceCore.ServiceLocation
{
  /// <summary>
  /// Mode by which registrations are searched
  /// </summary>
  public enum DiscoveryModes
  {
    /// <summary>
    /// By RegisterAttributes
    /// </summary>
    Assemblies = 0,
    
    /// <summary>
    /// Searches Registrations in files
    /// </summary>
    Files = 1,
    
    /// <summary>
    /// Searches files and attributes, prefers file registration (if marked with exclusive) if found for a particular registration
    /// </summary>
    AssembliesAndFiles = 2,
  }
}
