﻿namespace NiceCore.ServiceLocation
{
  /// <summary>
  /// Modes that determine how assemblies with registrations are provided
  /// </summary>
  public enum AssemblyProvidingModes
  {
    /// <summary>
    /// All 
    /// </summary>
    All = 0,
    
    /// <summary>
    /// Constrained by multiple module files
    /// </summary>
    Constrained = 1,
    
    /// <summary>
    /// Constrained by the default file
    /// </summary>
    ConstrainedSingleFile = 2,
  }
}
