﻿using Microsoft.Extensions.DependencyInjection;

namespace NiceCore.ServiceLocation.Container
{
  /// <summary>
  /// Helper class for converting ServiceRegistration objects to ServiceDescriptor items
  /// </summary>
  public static class ServiceLifetimeConvertUtil
  {
    /// <summary>
    /// Converts from lifetime to LifeStyle
    /// </summary>
    /// <param name="i_LifeTime"></param>
    /// <returns></returns>
    public static LifeStyles ConvertLifeTimeToLifeStyle(ServiceLifetime i_LifeTime)
    {
      return i_LifeTime switch
      {
        ServiceLifetime.Transient => LifeStyles.Transient,
        ServiceLifetime.Singleton => LifeStyles.Singleton,
        ServiceLifetime.Scoped => LifeStyles.Scoped, //TODO: maybe support scoped since this is currently not supported
        _ => LifeStyles.Transient,
      };
    }

    /// <summary>
    /// Converts from lifestyle to LifeTime
    /// </summary>
    /// <param name="i_LifeTime"></param>
    /// <returns></returns>
    public static ServiceLifetime ConvertLifeStyleToLifeTime(LifeStyles i_LifeTime)
    {
      return i_LifeTime switch
      {
        LifeStyles.Singleton => ServiceLifetime.Singleton,
        LifeStyles.Transient => ServiceLifetime.Transient,
        LifeStyles.Scoped => ServiceLifetime.Scoped,
        LifeStyles.Custom => ServiceLifetime.Scoped,
        _ => ServiceLifetime.Transient
      };
    }
  }
}
