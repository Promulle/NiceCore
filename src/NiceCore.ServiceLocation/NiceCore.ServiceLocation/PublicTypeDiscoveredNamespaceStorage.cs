// Copyright 2021 Sycorax Systemhaus GmbH

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using NiceCore.ServiceLocation.Discovery;

namespace NiceCore.ServiceLocation
{
  /// <summary>
  /// Impl that uses only namespaces of public types
  /// </summary>
  public class PublicTypeDiscoveredNamespaceStorage : IDiscoveredNamespaceStorage
  {
    private readonly HashSet<string> m_Namespaces = new();

    /// <summary>
    /// Add the namespace of the type to the namespaces in the compile list
    /// </summary>
    /// <param name="i_Type"></param>
    public void AddNamespaceOfType(Type i_Type)
    {
      if (i_Type.IsPublic && IsNotForwardingType(i_Type))
      {
        m_Namespaces.Add(i_Type.Namespace);
      }
    }

    private static bool IsNotForwardingType(Type i_Type)
    {
      return i_Type.GetCustomAttributes(typeof(TypeForwardedFromAttribute), false).Length == 0;
    }
    
    /// <summary>
    /// Returns all namespaces as distinct string list
    /// </summary>
    /// <returns></returns>
    public IEnumerable<string> GetAllNamespaces()
    {
      return m_Namespaces;
    }
  }
}
