﻿using Microsoft.Extensions.Logging;
using NiceCore.Logging;
using NiceCore.Logging.Providers.Default;
using NiceCore.ServiceLocation.Discovery.ServiceDescriptors;

namespace NiceCore.ServiceLocation.Extensions
{
  /// <summary>
  /// Logging Registration Extensions
  /// </summary>
  public static class LoggingRegistrationExtensions
  {
    /// <summary>
    /// Registers logging with the container
    /// </summary>
    /// <param name="t_Container"></param>
    /// <param name="i_Factory"></param>
    public static void RegisterLogging(this IServiceContainer t_Container, ILoggerFactory i_Factory)
    {
      t_Container.RegisterInstance(i_Factory, typeof(ILoggerFactory), LifeStyles.Singleton);
      t_Container.Register(typeof(ILogger<>), typeof(Logger<>), LifeStyles.Singleton, null);
    }

    /// <summary>
    /// Registers logging with the container
    /// </summary>
    /// <param name="t_Container"></param>
    /// <param name="i_Factory"></param>
    public static void RegisterLogging(this NcServiceCollection t_Container, ILoggerFactory i_Factory)
    {
      t_Container.AddInstance<ILoggerFactory>(i_Factory);
      var descriptor = new NcServiceRegistrationDescriptor()
      {
        ImplementorType = typeof(Logger<>),
        InterfaceType = typeof(ILogger<>),
        ServiceLifestyle = LifeStyles.Singleton
      };
      t_Container.Add(descriptor);
    }

    /// <summary>
    /// Registers the default providers for logging
    /// </summary>
    /// <param name="t_Container"></param>
    public static void RegisterDefaultLoggingProviders(this NcServiceCollection t_Container)
    {
      t_Container.AddTransient<INcLoggingProvider, ConsoleNcLoggerProvider>();
      t_Container.AddTransient<INcLoggingProvider, EventSourceNcLoggerProvider>();
      t_Container.AddTransient<INcLoggingProvider, DebugNcLoggerProvider>();
    }
  }
}