using System;
using System.Collections.Generic;
using NiceCore.Extensions;
using NiceCore.ServiceLocation.Attributes;
using NiceCore.ServiceLocation.Constants;
using NiceCore.ServiceLocation.Discovery;
using NiceCore.ServiceLocation.Discovery.ServiceDescriptors;

namespace NiceCore.ServiceLocation
{
  /// <summary>
  /// Extension methods for interface IServiceContainer
  /// </summary>
  public static class ServiceContainerExtensions
  {
    /// <summary>
    /// Resolves an instance of TInterface. If none is found, an instance of TDefault is created instead.
    /// TDefault must implement TInterface and has to have a parameterless constructor for this to work
    /// </summary>
    /// <param name="i_Container">Container to use for resolving</param>
    /// <typeparam name="TInterface">Type to resolve an instance for</typeparam>
    /// <typeparam name="TDefault">Type of instance to create if no instance is found for TInterface</typeparam>
    /// <returns></returns>
    public static TInterface ResolveOrCreate<TInterface, TDefault>(this IServiceContainer i_Container)
    {
      var resolvedInstance = i_Container.Resolve<TInterface>();
      if (resolvedInstance != null)
      {
        return resolvedInstance;
      }

      if (!typeof(TInterface).IsAssignableFrom(typeof(TDefault)))
      {
        throw new ArgumentException($"Type TDefault: {typeof(TDefault).FullName} does not implement interface {typeof(TInterface).FullName}");
      }
      var inst = Activator.CreateInstance<TDefault>();
      return (TInterface) Convert.ChangeType(inst, typeof(TInterface));
    }
    
    /// <summary>
    /// Checks if there is an impl for TInterface
    /// </summary>
    /// <typeparam name="TInterface"></typeparam>
    /// <returns></returns>
    public static bool HasImplementationOf<TInterface>(this IServiceContainer t_Container)
    {
      return t_Container.HasImplementationOf(typeof(TInterface));
    }

    /// <summary>
    /// Register Service Collection
    /// </summary>
    /// <param name="t_Container"></param>
    /// <param name="i_Collection"></param>
    public static void RegisterServiceCollection(this IServiceContainer t_Container, NcServiceCollection i_Collection)
    {
      if (i_Collection == null)
      {
        return;
      }
      foreach (var serviceDescriptor in i_Collection)
      {
        var interfaceTypes = new List<Type>(serviceDescriptor.AliasAttributes.Count + 1)
        {
          serviceDescriptor.InterfaceType
        };
        foreach (var aliasAttribute in serviceDescriptor.AliasAttributes)
        {
          interfaceTypes.Add(aliasAttribute.AliasType);
        }

        if (serviceDescriptor.Instance == null)
        {
          t_Container.Register(interfaceTypes, serviceDescriptor.ImplementorType, serviceDescriptor.ServiceLifestyle, serviceDescriptor.CustomLifestyleName);  
        }
        else
        {
          t_Container.RegisterInstance(serviceDescriptor.Instance, serviceDescriptor.InterfaceType, LifeStyles.Singleton);
        }
      }
    }

    /// <summary>
    /// Register by type and impl
    /// </summary>
    /// <param name="t_Container"></param>
    /// <param name="i_InterfaceType"></param>
    /// <param name="i_ImplementingType"></param>
    public static void Register(this IServiceContainer t_Container, Type i_InterfaceType, Type i_ImplementingType)
    {
      t_Container.Register(i_InterfaceType, i_ImplementingType, LifeStyles.Transient, null);
    }

    /// <summary>
    /// Register by type, impl and lifestyle
    /// </summary>
    /// <param name="t_Container"></param>
    /// <param name="i_InterfaceType"></param>
    /// <param name="i_ImplementingType"></param>
    /// <param name="i_Lifestyle"></param>
    /// <param name="i_CustomLifestyleName">Name of custom lifestyle to use, only applicable if i_Lifestyle == LifeStyles.Custom</param>
    public static void Register(this IServiceContainer t_Container, Type i_InterfaceType, Type i_ImplementingType, LifeStyles i_Lifestyle, string i_CustomLifestyleName)
    {
      t_Container.Register(new List<Type>() {i_InterfaceType}, i_ImplementingType, i_Lifestyle, i_CustomLifestyleName);
    }

    /// <summary>
    /// Register by type, impl and name
    /// </summary>
    /// <param name="t_Container"></param>
    /// <param name="i_InterfaceType"></param>
    /// <param name="i_ImplementingType"></param>
    /// <param name="i_Name"></param>
    public static void Register(this IServiceContainer t_Container, Type i_InterfaceType, Type i_ImplementingType, string i_Name)
    {
      t_Container.Register(i_InterfaceType, i_ImplementingType, ServiceRegistrationConstants.DEFAULT_LIFESTYLE, i_Name);
    }

    /// <summary>
    /// Register by type and attribute
    /// </summary>
    /// <param name="t_Container"></param>
    /// <param name="i_ClassType"></param>
    /// <param name="i_Attribute"></param>
    /// <param name="i_Aliases">Aliases to also be registered</param>
    public static void Register(this IServiceContainer t_Container, Type i_ClassType, RegisterAttribute i_Attribute, IEnumerable<RegisterAliasAttribute> i_Aliases)
    {
      var types = new List<Type>
      {
        i_Attribute.InterfaceType
      };
      i_Aliases.ForEachItem(alias => types.Add(alias.AliasType));
      t_Container.Register(types, i_ClassType, i_Attribute.LifeStyle, i_Attribute.CustomLifeStyleName);
    }

    /// <summary>
    /// Register by type and impl
    /// </summary>
    /// <typeparam name="TInterfaceType"></typeparam>
    /// <typeparam name="TClassType"></typeparam>
    public static void Register<TInterfaceType, TClassType>(this IServiceContainer t_Container)
    {
      t_Container.Register(typeof(TInterfaceType), typeof(TClassType), ServiceRegistrationConstants.DEFAULT_LIFESTYLE, null);
    }

    /// <summary>
    /// Register by type, impl and lifestyle
    /// </summary>
    /// <typeparam name="TInterfaceType"></typeparam>
    /// <typeparam name="TClassType"></typeparam>
    /// <param name="t_Container"></param>
    /// <param name="i_Lifestyle"></param>
    /// <param name="i_CustomLifestyleName"></param>
    public static void Register<TInterfaceType, TClassType>(this IServiceContainer t_Container, LifeStyles i_Lifestyle, string i_CustomLifestyleName)
    {
      t_Container.Register(typeof(TInterfaceType), typeof(TClassType), i_Lifestyle, null);
    }

    /// <summary>
    /// Register instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t_Container"></param>
    /// <param name="i_Instance"></param>
    /// <param name="i_LifeStyle"></param>
    /// <param name="i_CustomLifestyleName"></param>
    /// <param name="i_Name"></param>
    public static void RegisterInstance<T>(this IServiceContainer t_Container, T i_Instance, LifeStyles i_LifeStyle = LifeStyles.Transient, string i_CustomLifestyleName = null, string i_Name = null)
    {
      t_Container.RegisterInstance(i_Instance, typeof(T), i_LifeStyle, i_CustomLifestyleName);
    }
  }
}