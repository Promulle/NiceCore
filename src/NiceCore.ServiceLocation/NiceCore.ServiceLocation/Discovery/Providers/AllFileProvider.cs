﻿using System.Collections.Generic;
using System.IO;

namespace NiceCore.ServiceLocation.Discovery.Providers
{
  /// <summary>
  /// Provider that returns all dll and exe files
  /// </summary>
  public class AllFileProvider : IModuleProvider
  {
    private const string DLL_EXTENSION = ".dll";
    private const string EXE_EXTENSION = ".exe";
    /// <summary>
    /// Impl that returns all exes and dlls
    /// </summary>
    /// <param name="i_Directory"></param>
    /// <returns></returns>
    public IEnumerable<string> GetModuleFiles(string i_Directory)
    {
      var res = new List<string>();
      if (Directory.Exists(i_Directory))
      {
        foreach (var s in Directory.GetFiles(i_Directory))
        {
          var ext = Path.GetExtension(s);
          if (ext.Equals(DLL_EXTENSION) || ext.Equals(EXE_EXTENSION))
          {
            res.Add(s);
          }
        }
      }
      return res;
    }
  }
}
