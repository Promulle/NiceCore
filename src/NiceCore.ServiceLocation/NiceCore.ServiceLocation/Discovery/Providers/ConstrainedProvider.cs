﻿using System.Collections.Generic;
using System.IO;

namespace NiceCore.ServiceLocation.Discovery.Providers
{
  /// <summary>
  /// Provider that only returns all files that are set by *.ncmodules files
  /// </summary>
  public class ConstrainedProvider : ConstrainedSingleFileProvider
  {
    private const string PATTERN_MODULEFILE = "*" + EXT_MODULEFILE;
    /// <summary>
    /// Impl that searches *.ncmodules files
    /// </summary>
    /// <param name="i_Directory"></param>
    /// <returns></returns>
    public override IEnumerable<string> GetModuleFiles(string i_Directory)
    {
      var res = new List<string>();
      foreach (var file in Directory.GetFiles(i_Directory, PATTERN_MODULEFILE, SearchOption.AllDirectories))
      {
        res.AddRange(ReadModuleFile(file));
      }
      return res;
    }
  }
}
