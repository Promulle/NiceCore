﻿using System.Collections.Generic;
using System.IO;

namespace NiceCore.ServiceLocation.Discovery.Providers
{
  /// <summary>
  /// Provider for searching of the default module file
  /// </summary>
  public class ConstrainedSingleFileProvider : IModuleProvider
  {
    /// <summary>
    /// Extension of a modulefile
    /// </summary>
    protected const string EXT_MODULEFILE = ".ncmodules";
    /// <summary>
    /// Name of default module file
    /// </summary>
    protected const string DEFAULT_MODULEFILE = "nicecore" + EXT_MODULEFILE;
    /// <summary>
    /// Comment starting with slash
    /// </summary>
    protected const string COMMENT_START_SLASH = "//";
    /// <summary>
    /// Comment starting with semicolon
    /// </summary>
    protected const string COMMENT_START_SEMICOLON = ";";
    /// <summary>
    /// Impl that returns all from default file
    /// </summary>
    /// <param name="i_Directory"></param>
    /// <returns></returns>
    public virtual IEnumerable<string> GetModuleFiles(string i_Directory)
    {
      var defFile = i_Directory + Path.DirectorySeparatorChar + DEFAULT_MODULEFILE;
      return ReadModuleFile(defFile);
    }

    /// <summary>
    /// Reads a module file
    /// </summary>
    /// <param name="i_FilePath"></param>
    /// <returns></returns>
    protected static List<string> ReadModuleFile(string i_FilePath)
    {
      var res = new List<string>();
      if (File.Exists(i_FilePath))
      {
        foreach (var line in File.ReadAllLines(i_FilePath))
        {
          if (!line.StartsWith(COMMENT_START_SEMICOLON) && !line.StartsWith(COMMENT_START_SLASH))
          {
            res.Add(Path.GetFullPath(line));
          }
        }
      }
      return res;
    }
  }
}
