﻿using System.Xml.Serialization;

namespace NiceCore.ServiceLocation.Discovery.Files
{
  /// <summary>
  /// Class for xml single registration
  /// </summary>
  public struct XmlRegistrationInfo
  {
    private const string XML_NAME_INTERFACE_TYPE = "InterfaceType";
    private const string XML_NAME_IMPLEMENTOR_TYPE = "ImplementorType";
    private const string XML_NAME_LIFESTYLE = "LifeStyle";
    
    /// <summary>
    /// Interface-Name and Assembly
    /// </summary>
    [XmlElement(ElementName = XML_NAME_INTERFACE_TYPE)]
    public string InterfaceTypeIdentifier { get; set; }
    
    /// <summary>
    /// Implementor-Name and Assembly
    /// </summary>
    [XmlElement(ElementName = XML_NAME_IMPLEMENTOR_TYPE)]
    public string ImplementorTypeIdentifier { get; set; }
    
    /// <summary>
    /// Lifestyle of Registration
    /// </summary>
    [XmlElement(ElementName = XML_NAME_LIFESTYLE)]
    public string LifeStyle { get; set; }
    
    /// <summary>
    /// Default Constructor
    /// </summary>
    public XmlRegistrationInfo()
    {
    }
    /// <summary>
    /// Constructor to create from RegistrationInfo
    /// </summary>
    /// <param name="i_Info"></param>
    public XmlRegistrationInfo(RegistrationInfo i_Info)
    {
      InterfaceTypeIdentifier = i_Info.InterfaceType.AssemblyQualifiedName;
      ImplementorTypeIdentifier = i_Info.ImplementorType.AssemblyQualifiedName;
      LifeStyle = i_Info.LifeStyle + "";
    }
  }
}
