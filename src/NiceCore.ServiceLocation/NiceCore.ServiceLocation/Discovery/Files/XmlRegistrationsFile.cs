﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace NiceCore.ServiceLocation.Discovery.Files
{
  /// <summary>
  /// Xml for Registration file
  /// </summary>
  public class XmlRegistrationsFile
  {
    /// <summary>
    /// List of xml registrations 
    /// </summary>
    public List<XmlRegistrationInfo> Registrations { get; } = new();

    /// <summary>
    /// Ctor
    /// </summary>
    public XmlRegistrationsFile()
    {
      
    }
    
    /// <summary>
    /// Serializes this xmlRegistrationsFile to an actual file
    /// </summary>
    /// <param name="i_FilePath"></param>
    public void SerializeToFile(string i_FilePath)
    {
      try
      {
        using (var fs = File.OpenWrite(i_FilePath))
        {
          var serializer = new XmlSerializer(typeof(XmlRegistrationsFile));
          serializer.Serialize(fs, this);
          fs.Flush();
          fs.Close();
        }
      }
      catch
      {
      }
    }
    /// <summary>
    /// Deserializes from i_FilePath to this instance
    /// </summary>
    /// <param name="i_FilePath"></param>
    public static XmlRegistrationsFile DeserializeFromFile(string i_FilePath)
    {
      if (File.Exists(i_FilePath))
      {
        try
        {
          using (var fileStream = File.OpenRead(i_FilePath))
          {
            var serializer = new XmlSerializer(typeof(XmlRegistrationsFile));
            var xmlRegFile = serializer.Deserialize(fileStream) as XmlRegistrationsFile;
            fileStream.Close();
            return xmlRegFile;
          }
        }
        catch
        {
        }
      }
      return null;
    }
  }
}
