﻿using NiceCore.ServiceLocation.Interface;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using NiceCore.ServiceLocation.Container;
using NiceCore.ServiceLocation.Discovery.ServiceDescriptors;
using NiceCore.ServiceLocation.Helpers;

namespace NiceCore.ServiceLocation.Discovery.Files
{
  /// <summary>
  /// NiceCore impl of IDependencyFileAdapter
  /// </summary>
  public class NiceCoreFileAdapter : IDependencyFileAdapter
  {
    private const string PATTERN_REGISTRATION_FILE = "*.registration.xml";

    /// <summary>
    /// Discover the NiceCore registration file format
    /// </summary>
    /// <param name="i_Directory"></param>
    public NcServiceCollection DiscoverFiles(string i_Directory)
    {
      var collection = new NcServiceCollection();

      foreach (var xmlFile in Directory.GetFiles(i_Directory, PATTERN_REGISTRATION_FILE))
      {
        var xmlRegFile = XmlRegistrationsFile.DeserializeFromFile(xmlFile);
        foreach (var xmlReg in xmlRegFile.Registrations)
        {
          var reg = new RegistrationInfo(xmlReg);
          if (reg.IsValid)
          {
            var res = new NcServiceRegistrationDescriptor()
            {
              ImplementorType = reg.ImplementorType,
              InterfaceType = reg.InterfaceType,
              ServiceLifestyle = reg.LifeStyle
            };
            collection.Add(res);
          }
        }
      }

      return collection;
    }
  }
}