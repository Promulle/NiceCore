﻿using System;

namespace NiceCore.ServiceLocation.Discovery.Files
{
  /// <summary>
  /// Single Info to be serialized into .xml files
  /// </summary>
  public struct RegistrationInfo
  {
    /// <summary>
    /// Type of Interface to register for
    /// </summary>
    public Type InterfaceType { get; set; }
    
    /// <summary>
    /// Type of Implementing class
    /// </summary>
    public Type ImplementorType { get; set; }
    
    /// <summary>
    /// Whether this info can actually be used to register something
    /// </summary>
    public bool IsValid
    {
      get
      {
        return InterfaceType != null && ImplementorType != null;
      }
    }
    
    /// <summary>
    /// Lifestyle to register
    /// </summary>
    public LifeStyles LifeStyle { get; set; }
    
    /// <summary>
    /// Default Constructor
    /// </summary>
    public RegistrationInfo()
    {
    }
    
    /// <summary>
    /// Constructor to get info from XmlRegistration
    /// </summary>
    /// <param name="i_XmlInfo"></param>
    public RegistrationInfo(XmlRegistrationInfo i_XmlInfo)
    {
      InterfaceType = Type.GetType(i_XmlInfo.InterfaceTypeIdentifier);
      ImplementorType = Type.GetType(i_XmlInfo.ImplementorTypeIdentifier);
      LifeStyle = (LifeStyles) Enum.Parse(typeof(LifeStyles), i_XmlInfo.LifeStyle);
    }
  }
}
