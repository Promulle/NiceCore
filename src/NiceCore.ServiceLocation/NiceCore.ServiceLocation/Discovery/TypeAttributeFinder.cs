using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using NiceCore.ServiceLocation.Attributes;

namespace NiceCore.ServiceLocation.Discovery
{
  /// <summary>
  /// Helper class for getting attributes from a type
  /// </summary>
  public static class TypeAttributeFinder
  {
    /// <summary>
    /// Finds attributes on a type
    /// </summary>
    /// <param name="i_Type"></param>
    /// <param name="i_CustomAttributeHandlers"></param>
    /// <returns></returns>
    public static FoundAttributes FindAttributesOnType(Type i_Type, IReadOnlyCollection<ICustomRegistrationAttributeHandler> i_CustomAttributeHandlers)
    {
      var registerAttributes = new List<RegisterAttribute>();
      var aliasAttributes = new List<RegisterAliasAttribute>();
      foreach (var attribute in i_Type.GetCustomAttributes())
      {
        if (attribute is RegisterAttribute registerAttribute)
        {
          registerAttributes.Add(registerAttribute);
        }

        if (attribute is RegisterAliasAttribute registerAliasAttribute)
        {
          aliasAttributes.Add(registerAliasAttribute);
        }

        foreach (var attributeHandler in i_CustomAttributeHandlers)
        {
          foreach (var registerAttrData in attributeHandler.HandleCustomAttribute(attribute, i_Type))
          {
            registerAttributes.Add(registerAttrData);
          }
        }
      }

      var dict = new Dictionary<RegisterAttribute, List<RegisterAliasAttribute>>();
      foreach (var registerAttribute in registerAttributes)
      {
        dict[registerAttribute] = aliasAttributes.Where(r => r.OriginalRegistrationType == registerAttribute.InterfaceType).ToList();
      }

      return new()
      {
        Attributes = dict
      };
    }
  }
}