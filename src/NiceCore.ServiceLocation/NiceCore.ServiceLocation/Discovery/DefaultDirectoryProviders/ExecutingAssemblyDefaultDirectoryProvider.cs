﻿using System.IO;
using System.Reflection;

namespace NiceCore.ServiceLocation.Discovery.DefaultDirectoryProviders
{
  /// <summary>
  /// Executing Assembly Default Directory provider
  /// </summary>
  public class ExecutingAssemblyDefaultDirectoryProvider : IDiscoveryDefaultDirectoryProvider
  {
    /// <summary>
    /// Get default directory
    /// </summary>
    /// <returns></returns>
    public string GetDefaultDirectory()
    {
      return Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
    }
  }
}