﻿using System;
using System.IO;

namespace NiceCore.ServiceLocation.Discovery.DefaultDirectoryProviders
{
  /// <summary>
  /// Default directory provider that just returns the previously filled value
  /// </summary>
  public class DirectDefaultDirectoryProvider : IDiscoveryDefaultDirectoryProvider
  {
    private readonly string m_DefaultDirectory;
    
    /// <summary>
    /// Ctor filling default directory
    /// </summary>
    /// <param name="i_DefaultDirectory"></param>
    public DirectDefaultDirectoryProvider(string i_DefaultDirectory)
    {
      if (!Directory.Exists(i_DefaultDirectory))
      {
        throw new ArgumentException($"Can not create instance of type {nameof(DirectDefaultDirectoryProvider)} with directory argument {i_DefaultDirectory} because the directory does not exist.");
      }
      m_DefaultDirectory = i_DefaultDirectory;
    }
    
    /// <summary>
    /// Return the filled directory
    /// </summary>
    /// <returns></returns>
    public string GetDefaultDirectory()
    {
      return m_DefaultDirectory;
    }
  }
}