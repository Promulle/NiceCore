﻿using System;
using System.IO;
using System.Reflection;

namespace NiceCore.ServiceLocation.Discovery.DefaultDirectoryProviders
{
  /// <summary>
  /// Impl that uses the entry assembly as default directory
  /// </summary>
  public class EntryAssemblyDefaultDirectoryProvider : IDiscoveryDefaultDirectoryProvider
  {
    /// <summary>
    /// Get default directory
    /// </summary>
    /// <returns></returns>
    public string GetDefaultDirectory()
    {
      var entryAssembly = Assembly.GetEntryAssembly();
      if (entryAssembly == null)
      {
        throw new InvalidOperationException("Could not provide default directory for entry assembly. Entry Assembly is null.");
      }
      return Path.GetDirectoryName(entryAssembly.Location);
    }
  }
}