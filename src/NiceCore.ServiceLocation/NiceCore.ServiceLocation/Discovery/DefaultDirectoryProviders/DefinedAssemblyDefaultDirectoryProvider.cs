using System;
using System.IO;

namespace NiceCore.ServiceLocation.Discovery.DefaultDirectoryProviders
{
  /// <summary>
  /// Defined Assembly Default Directory Provider
  /// </summary>
  public class DefinedAssemblyDefaultDirectoryProvider : IDiscoveryDefaultDirectoryProvider
  {
    private readonly string m_Location;
    
    /// <summary>
    /// Ctor
    /// </summary>
    public DefinedAssemblyDefaultDirectoryProvider(Type i_Type)
    {
      m_Location = Path.GetDirectoryName(i_Type.Assembly.Location);
    }
    
    /// <summary>
    /// Get Default Directory
    /// </summary>
    /// <returns></returns>
    public string GetDefaultDirectory()
    {
      return m_Location;
    }
  }
}