﻿using System;
using System.Reflection;
using Microsoft.Extensions.Logging;
using NiceCore.Diagnostics;
using NiceCore.Logging.Extensions;

namespace NiceCore.ServiceLocation.Discovery.ErrorHandlers
{
  /// <summary>
  /// Discovery Error Handler that logs
  /// </summary>
  public sealed class LoggingDiscoveryErrorHandler : IDiscoveryErrorHandler
  {
    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<LoggingDiscoveryErrorHandler> Logger { get; set; } = DiagnosticsCollector.Instance.GetLogger<LoggingDiscoveryErrorHandler>();

    /// <summary>
    /// Handle Exception
    /// </summary>
    /// <param name="i_Exception"></param>
    /// <param name="i_FilePath"></param>
    public void HandleException(Exception i_Exception, string i_FilePath)
    {
      Logger.Error(i_Exception, $"An Error occurred during registration discovery of file: {i_FilePath}");
    }

    /// <summary>
    /// Handle a bad image format
    /// </summary>
    /// <param name="i_Exception"></param>
    /// <param name="i_FilePath"></param>
    public void HandleBadImageFormatException(BadImageFormatException i_Exception, string i_FilePath)
    {
      Logger.Error(i_Exception, $"Could not load an assembly because of BadImageFormat: {i_FilePath}");
    }

    /// <summary>
    /// Handle Reflection Type Load
    /// </summary>
    /// <param name="i_Exception"></param>
    /// <param name="i_FilePath"></param>
    public void HandleReflectionTypeLoadException(ReflectionTypeLoadException i_Exception, string i_FilePath)
    {
      Logger.Error(i_Exception, $"Could not load a type from assembly {i_FilePath}");
    }
  }
}