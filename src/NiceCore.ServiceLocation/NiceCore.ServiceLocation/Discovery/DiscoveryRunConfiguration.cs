using System;
using System.Collections.Generic;
using NiceCore.ServiceLocation.Discovery.DefaultDirectoryProviders;
using NiceCore.ServiceLocation.Discovery.ErrorHandlers;
using NiceCore.ServiceLocation.Discovery.Providers;
using NiceCore.ServiceLocation.Interface;

namespace NiceCore.ServiceLocation.Discovery
{
  /// <summary>
  /// Discovery Run Configuration
  /// </summary>
  public class DiscoveryRunConfiguration
  {
    /// <summary>
    /// Allow search of loaded assemblies
    /// </summary>
    public bool AllowSearchOfLoadedAssemblies { get; init; } = false;

    /// <summary>
    /// SearchDirectories
    /// </summary>
    public IReadOnlyCollection<string> SearchDirectories { get; init; } = Array.Empty<string>();

    /// <summary>
    /// FileAdapter
    /// </summary>
    public IDependencyFileAdapter DependencyFileAdapter { get; init; }

    /// <summary>
    /// TargetMode
    /// </summary>
    public DiscoveryModes TargetMode { get; init; } = DiscoveryModes.AssembliesAndFiles;

    /// <summary>
    /// AssemblyTargetMode
    /// </summary>
    public AssemblyProvidingModes AssemblyTargetMode { get; init; } = AssemblyProvidingModes.All;

    /// <summary>
    /// Provider for files to search in
    /// </summary>
    public IModuleProvider ModuleProvider { get; init; }

    /// <summary>
    /// Discovery error handler
    /// </summary>
    public IDiscoveryErrorHandler DiscoveryErrorHandler { get; init; } = new LoggingDiscoveryErrorHandler();

    /// <summary>
    /// Custom registration attribute handlers
    /// </summary>
    public IReadOnlyCollection<ICustomRegistrationAttributeHandler> CustomRegistrationAttributeHandlers { get; init; } = Array.Empty<ICustomRegistrationAttributeHandler>();

    /// <summary>
    /// Discovery Default directory provider
    /// </summary>
    public IDiscoveryDefaultDirectoryProvider DiscoveryDefaultDirectoryProvider { get; init; } = new EntryAssemblyDefaultDirectoryProvider();

    /// <summary>
    /// Override Registrations
    /// </summary>
    public IReadOnlyCollection<DiscoveryOverrideRegistration> OverrideRegistrations { get; init; } = Array.Empty<DiscoveryOverrideRegistration>();
    
    /// <summary>
    /// Discovered Namespace Storage
    /// </summary>
    public IDiscoveredNamespaceStorage DiscoveredNamespaceStorage { get; init; }

    /// <summary>
    /// Whether the discovery in files is allowed
    /// </summary>
    /// <returns></returns>
    public bool IsDiscoverFilesEnabled()
    {
      return TargetMode == DiscoveryModes.AssembliesAndFiles || TargetMode == DiscoveryModes.Files;
    }

    /// <summary>
    /// Creates a default search directory if needed
    /// </summary>
    /// <returns></returns>
    public IReadOnlyCollection<string> GetSearchDirectories()
    {
      if (SearchDirectories?.Count != 0)
      {
        return SearchDirectories;
      }

      if (DiscoveryDefaultDirectoryProvider == null)
      {
        throw new InvalidOperationException($"Could not return SearchDirectories: Property {nameof(SearchDirectories)} was null or empty and Property {nameof(DiscoveryDefaultDirectoryProvider)} was not configured.");
      }

      return new[]
      {
        DiscoveryDefaultDirectoryProvider.GetDefaultDirectory()
      };
    }

    /// <summary>
    /// Whether discovery of assemblies is allowed
    /// </summary>
    /// <returns></returns>
    public bool IsDiscoverAssembliesEnabled()
    {
      return TargetMode != DiscoveryModes.Files;
    }

    /// <summary>
    /// Creates a provider if none is provided
    /// </summary>
    public IModuleProvider GetModuleProvider()
    {
      if (ModuleProvider != null)
      {
        return ModuleProvider;
      }

      if (AssemblyTargetMode == AssemblyProvidingModes.Constrained)
      {
        return new ConstrainedProvider();
      }

      if (AssemblyTargetMode == AssemblyProvidingModes.ConstrainedSingleFile)
      {
        return new ConstrainedSingleFileProvider();
      }

      return new AllFileProvider();
    }
  }
}