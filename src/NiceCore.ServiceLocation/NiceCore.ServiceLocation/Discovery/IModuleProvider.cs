﻿using System.Collections.Generic;

namespace NiceCore.ServiceLocation.Discovery
{
  /// <summary>
  /// Provider for paths to files that contain registrations
  /// </summary>
  public interface IModuleProvider
  {
    /// <summary>
    /// Returns a list of files that will be searched by the registration discovery
    /// </summary>
    /// <param name="i_Directory"></param>
    /// <returns></returns>
    IEnumerable<string> GetModuleFiles(string i_Directory);
  }
}
