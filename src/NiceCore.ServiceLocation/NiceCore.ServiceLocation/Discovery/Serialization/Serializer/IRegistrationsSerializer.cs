﻿using NiceCore.ServiceLocation.Discovery.Serialization.Types;
using System;

namespace NiceCore.ServiceLocation.Discovery.Serialization.Serializer
{
  /// <summary>
  /// Serializer for registrations
  /// </summary>
  public interface IRegistrationsSerializer : IDisposable
  {
    /// <summary>
    /// Path where file will be serialized to
    /// </summary>
    string FilePath { get; }

    /// <summary>
    /// Add a registration
    /// </summary>
    /// <param name="i_Registration"></param>
    void AddRegistrations(SerializedRegistrationDTO i_Registration);

    /// <summary>
    /// Adds an assembly to the serialization
    /// </summary>
    /// <param name="i_FilePath"></param>
    void AddAssembly(string i_FilePath);

    /// <summary>
    /// Flushes the registrations to file
    /// </summary>
    void Flush();
  }
}
