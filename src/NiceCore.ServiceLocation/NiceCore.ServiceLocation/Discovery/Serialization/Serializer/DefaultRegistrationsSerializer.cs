﻿using NiceCore.Base;
using NiceCore.Serialization;
using NiceCore.ServiceLocation.Discovery.Serialization.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace NiceCore.ServiceLocation.Discovery.Serialization.Serializer
{
  /// <summary>
  /// Default impl of IRegistrationsSerializer
  /// </summary>
  public class DefaultRegistrationsSerializer : BaseDisposable, IRegistrationsSerializer
  {
    private readonly SerialRegistrations m_Registrations = new();
    private bool m_HasBeenChanged;

    /// <summary>
    /// Path of registration file
    /// </summary>
    public string FilePath { get; }

    /// <summary>
    /// Creates a new Serializer for i_FilePath
    /// </summary>
    /// <param name="i_FilePath"></param>
    public DefaultRegistrationsSerializer(string i_FilePath)
    {
      FilePath = i_FilePath;
    }

    /// <summary>
    /// Creates a new Serializer for i_FilePath from existing registrations
    /// </summary>
    /// <param name="i_FilePath"></param>
    /// <param name="i_Registrations"></param>
    public DefaultRegistrationsSerializer(string i_FilePath, SerialRegistrations i_Registrations) : this(i_FilePath)
    {
      if (i_Registrations != null)
      {
        m_Registrations = i_Registrations;
      }
    }

    /// <summary>
    /// Add a registration to be serialized
    /// </summary>
    /// <param name="i_Registration"></param>
    public void AddRegistrations(SerializedRegistrationDTO i_Registration)
    {
      var assemblyPair = GetAssemblyPair(i_Registration.AssemblyPath);
      if (assemblyPair == null)
      {
        assemblyPair = CreateAssemblyPair(i_Registration.AssemblyPath);
        AddPairToRegistrations(assemblyPair);
      }
      var pairValue = assemblyPair.Value;
      if (!AlreadyContainsRegistration(pairValue.Value, i_Registration))
      {
        pairValue.Value.Add(new SerializedRegistrationInfo()
        {
          ImplementationTypeName = i_Registration.ImplementationTypeName,
          InterfaceType = i_Registration.InterfaceType,
          RegisterLifestyle = i_Registration.RegisterLifestyle
        });
        m_HasBeenChanged = true;
      }
    }

    /// <summary>
    /// Adds an assembly to the serialization
    /// </summary>
    /// <param name="i_FilePath"></param>
    public void AddAssembly(string i_FilePath)
    {
      var assemblyPair = GetAssemblyPair(i_FilePath);
      if (assemblyPair == null)
      {
        assemblyPair = CreateAssemblyPair(i_FilePath);
        AddPairToRegistrations(assemblyPair);
        m_HasBeenChanged = true;
      }
    }

    private void AddPairToRegistrations(KeyValuePair<SerialAssembly, List<SerializedRegistrationInfo>>? i_Pair)
    {
      m_Registrations.RegistrationsByAssembly.Add(i_Pair.Value.Key, i_Pair.Value.Value);
    }

    private static KeyValuePair<SerialAssembly, List<SerializedRegistrationInfo>> CreateAssemblyPair(string i_AssemblyFilePath)
    {
      return new KeyValuePair<SerialAssembly, List<SerializedRegistrationInfo>>(new SerialAssembly() { FilePath = i_AssemblyFilePath, SerializedTime = DateTime.Now }, new List<SerializedRegistrationInfo>());
    }

    private KeyValuePair<SerialAssembly, List<SerializedRegistrationInfo>>? GetAssemblyPair(string i_AssemblyFilePath)
    {
      var assemblyPair = m_Registrations.RegistrationsByAssembly.FirstOrDefault(r => r.Key.FilePath.Equals(i_AssemblyFilePath));
      if (assemblyPair.Key == null)
      {
        return null;
      }
      return assemblyPair;
    }

    private static bool AlreadyContainsRegistration(IEnumerable<SerializedRegistrationInfo> i_Registrations, SerializedRegistrationDTO i_Registration)
    {
      return i_Registrations.Any(r =>
      {
        return r.ImplementationTypeName.Equals(i_Registration.ImplementationTypeName) &&
               r.InterfaceType.Equals(i_Registration.InterfaceType) &&
               r.RegisterLifestyle.Equals(i_Registration.RegisterLifestyle);
      });
    }

    /// <summary>
    /// Flush serializations
    /// </summary>
    public void Flush()
    {
      if (m_HasBeenChanged)
      {
        if (File.Exists(FilePath))
        {
          File.Delete(FilePath);
        }
        using (var file = File.OpenWrite(FilePath))
        {
          BinarySerialization.Serialize(m_Registrations, file);
        }
      }
    }

    /// <summary>
    /// Flush on dispose
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    protected override void Dispose(bool i_IsDisposing)
    {
      Flush();
      base.Dispose(i_IsDisposing);
    }
  }
}
