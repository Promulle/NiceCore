﻿namespace NiceCore.ServiceLocation.Discovery.Serialization
{
  /// <summary>
  /// Constantclass for serialization
  /// </summary>
  public static class SerializationConstants
  {
    /// <summary>
    /// Pattern
    /// </summary>
    public const string SERIAL_FILE_PATTERN = "*" + SERIAL_FILE_EXTENSION;

    /// <summary>
    /// Pattern
    /// </summary>
    public const string SERIAL_FILE_EXTENSION = ".registrations";
  }
}
