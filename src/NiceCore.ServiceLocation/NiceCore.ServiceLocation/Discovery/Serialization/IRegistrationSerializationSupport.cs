﻿using NiceCore.ServiceLocation.Discovery.Serialization.Serializer;
using NiceCore.ServiceLocation.Discovery.Serialization.Types;

namespace NiceCore.ServiceLocation.Discovery.Serialization
{
  /// <summary>
  /// Provider for registration reader/serializer
  /// </summary>
  public interface IRegistrationSerializationSupport
  {
    /// <summary>
    /// Creates a new serializer
    /// </summary>
    /// <param name="i_FilePath"></param>
    /// <returns></returns>
    IRegistrationsSerializer CreateNewSerializer(string i_FilePath);

    /// <summary>
    /// Create a serializer for i_FilePath with t_Registrations
    /// </summary>
    /// <param name="i_FilePath"></param>
    /// <param name="t_Registrations"></param>
    /// <returns></returns>
    IRegistrationsSerializer CreateNewSerializer(string i_FilePath, SerialRegistrations t_Registrations);
  }
}
