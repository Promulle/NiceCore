﻿using NiceCore.ServiceLocation.Discovery.Serialization.Types;
using System.Collections.Generic;

namespace NiceCore.ServiceLocation.Discovery.Serialization.Results
{
  /// <summary>
  /// Return of SerializedDiscovery
  /// </summary>
  public class SerializedDiscoveryResult
  {
    /// <summary>
    /// Whether the discovery was a success
    /// </summary>
    public bool Success { get; set; }

    /// <summary>
    /// Path to serialized File
    /// </summary>
    public string FilePath { get; set; }

    /// <summary>
    /// Original Registrations
    /// </summary>
    public SerialRegistrations OriginalRegistrations { get; set; }

    /// <summary>
    /// List of assemblies to be reloaded
    /// </summary>
    public IList<string> AssembliesToReload { get; } = new List<string>();

    /// <summary>
    /// Registrations
    /// </summary>
    public IList<RegistrationResult> Registrations { get; } = new List<RegistrationResult>();

    /// <summary>
    /// Returns a failure state serialized discovery result
    /// </summary>
    /// <returns></returns>
    public static SerializedDiscoveryResult Failure()
    {
      return new SerializedDiscoveryResult()
      {
        Success = false,
      };
    }
  }
}
