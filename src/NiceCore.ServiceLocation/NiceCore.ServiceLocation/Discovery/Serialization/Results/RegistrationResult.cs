﻿using System;

namespace NiceCore.ServiceLocation.Discovery.Serialization.Results
{
  /// <summary>
  /// Result in a single Registration
  /// </summary>
  public class RegistrationResult
  {
    /// <summary>
    /// Type of the interface
    /// </summary>
    public Type InterfaceType { get; set; }

    /// <summary>
    /// Type of implementation
    /// </summary>
    public Type ImplemementationType { get; set; }

    /// <summary>
    /// Lifestyle to use
    /// </summary>
    public LifeStyles Lifestyle { get; set; }
  }
}
