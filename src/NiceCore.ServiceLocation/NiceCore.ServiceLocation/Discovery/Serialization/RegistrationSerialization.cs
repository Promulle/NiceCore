﻿using NiceCore.ServiceLocation.Discovery.Serialization.Impl;

namespace NiceCore.ServiceLocation.Discovery.Serialization
{
  /// <summary>
  /// Static helper class for construction of default impl of ISerializedRegistrationDiscovery
  /// </summary>
  public static class RegistrationSerialization
  {
    /// <summary>
    /// Returns a default impl for SerializedRegistration discovery
    /// </summary>
    /// <returns></returns>
    public static ISerializedRegistrationDiscovery GetDefaultDiscovery()
    {
      return new DefaultSerializedRegistrationDiscovery()
      {
        RegistrationSerializationSupport = new DefaultRegistrationSerializationSupport(),
      };
    }
  }
}
