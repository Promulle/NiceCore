﻿using NiceCore.Extensions;
using NiceCore.Serialization;
using NiceCore.ServiceLocation.Discovery.Serialization.Results;
using NiceCore.ServiceLocation.Discovery.Serialization.Types;
using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace NiceCore.ServiceLocation.Discovery.Serialization.Impl
{
  /// <summary>
  /// Discovery for serialized registrations
  /// </summary>
  public class DefaultSerializedRegistrationDiscovery : ISerializedRegistrationDiscovery
  {
    /// <summary>
    /// Support for reading serializations
    /// </summary>
    public IRegistrationSerializationSupport RegistrationSerializationSupport { get; set; }

    /// <summary>
    /// Discover serialized registrations
    /// </summary>
    /// <param name="i_Directory"></param>
    public SerializedDiscoveryResult DiscoverDirectory(string i_Directory)
    {
      var file = SearchSerializedRegistrationFiles(i_Directory);
      if (file != null)
      {
        var deserialized = DeserializeRegistrations(file);
        if (deserialized != null)
        {
          return CheckSerializedFile(deserialized, file);
        }
      }
      return SerializedDiscoveryResult.Failure();
    }

    private static SerialRegistrations DeserializeRegistrations(string i_FilePath)
    {
      using (var fileStream = File.OpenRead(i_FilePath))
      {
        return BinarySerialization.Deserialize<SerialRegistrations>(fileStream);
      }
    }

    private static string SearchSerializedRegistrationFiles(string i_Directory)
    {
      return Directory.GetFiles(i_Directory, SerializationConstants.SERIAL_FILE_PATTERN)
        .FirstOrDefault();
    }

    private static SerializedDiscoveryResult CheckSerializedFile(SerialRegistrations i_Registrations, string i_FilePath)
    {
      var res = new SerializedDiscoveryResult()
      {
        FilePath = i_FilePath,
        OriginalRegistrations = i_Registrations,
        Success = true,
      };
      foreach (var pair in i_Registrations.RegistrationsByAssembly)
      {
        var isUpToDate = IsRegistrationUpToDate(pair.Key);
        if (!isUpToDate)
        {
          res.AssembliesToReload.Add(pair.Key.FilePath);
        }
        else if (isUpToDate && pair.Value.Count > 0)
        {
          //load assembly to be sure that it is usable
          Assembly.LoadFrom(pair.Key.FilePath);
          pair.Value.Select(ToRegistrationResult)
            .ForEachItem(r => res.Registrations.Add(r));
        }
      }
      return res;
    }

    private static RegistrationResult ToRegistrationResult(SerializedRegistrationInfo i_Info)
    {
      return new RegistrationResult()
      {
        ImplemementationType = Type.GetType(i_Info.ImplementationTypeName),
        InterfaceType = Type.GetType(i_Info.InterfaceType),
        Lifestyle = i_Info.RegisterLifestyle
      };
    }

    private static bool IsRegistrationUpToDate(SerialAssembly i_Assembly)
    {
      return i_Assembly.SerializedTime > new FileInfo(i_Assembly.FilePath).LastWriteTime;
    }
  }
}
