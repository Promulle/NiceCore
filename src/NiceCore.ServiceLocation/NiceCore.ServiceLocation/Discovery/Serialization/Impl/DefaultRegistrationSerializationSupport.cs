﻿using NiceCore.ServiceLocation.Discovery.Serialization.Serializer;
using NiceCore.ServiceLocation.Discovery.Serialization.Types;

namespace NiceCore.ServiceLocation.Discovery.Serialization.Impl
{
  /// <summary>
  /// Default impl of IRegistrationSerializationSupport
  /// </summary>
  public class DefaultRegistrationSerializationSupport : IRegistrationSerializationSupport
  {
    /// <summary>
    /// Create a new serializer for i_FilePath
    /// </summary>
    /// <param name="i_FilePath"></param>
    /// <returns></returns>
    public IRegistrationsSerializer CreateNewSerializer(string i_FilePath)
    {
      return new DefaultRegistrationsSerializer(i_FilePath);
    }

    /// <summary>
    /// Create a new serializer for i_FilePah
    /// </summary>
    /// <param name="i_FilePath"></param>
    /// <param name="t_Registrations"></param>
    /// <returns></returns>
    public IRegistrationsSerializer CreateNewSerializer(string i_FilePath, SerialRegistrations t_Registrations)
    {
      return new DefaultRegistrationsSerializer(i_FilePath, t_Registrations);
    }
  }
}
