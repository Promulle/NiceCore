﻿using ProtoBuf;
using System;

namespace NiceCore.ServiceLocation.Discovery.Serialization.Types
{
  /// <summary>
  /// Info about a serialized registration
  /// </summary>
  [Serializable]
  [ProtoContract]
  public class SerializedRegistrationInfo
  {
    /// <summary>
    /// Type of implementation
    /// </summary>
    [ProtoMember(1)]
    public string ImplementationTypeName { get; set; }

    /// <summary>
    /// Type of interface
    /// </summary>
    [ProtoMember(2)]
    public string InterfaceType { get; set; }

    /// <summary>
    /// Registerlifestyle
    /// </summary>
    [ProtoMember(4)]
    public LifeStyles RegisterLifestyle { get; set; }
  }
}
