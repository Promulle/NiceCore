﻿using ProtoBuf;
using System;

namespace NiceCore.ServiceLocation.Discovery.Serialization.Types
{
  /// <summary>
  /// Assembly serialized by filePath and serialization date
  /// </summary>
  [Serializable]
  [ProtoContract]
  public class SerialAssembly
  {
    /// <summary>
    /// Path to assembly file
    /// </summary>
    [ProtoMember(1)]
    public string FilePath { get; set; }

    /// <summary>
    /// Time the assemblies registrations for serialized
    /// </summary>
    [ProtoMember(2)]
    public DateTime SerializedTime { get; set; }
  }
}
