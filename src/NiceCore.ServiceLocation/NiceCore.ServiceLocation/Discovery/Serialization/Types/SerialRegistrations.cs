﻿using ProtoBuf;
using System;
using System.Collections.Generic;

namespace NiceCore.ServiceLocation.Discovery.Serialization.Types
{
  /// <summary>
  /// Class to be serialized into a file
  /// </summary>
  [Serializable]
  [ProtoContract]
  public class SerialRegistrations
  {
    /// <summary>
    /// Registrations grouped by assembly
    /// </summary>
    [ProtoMember(1)]
    public Dictionary<SerialAssembly, List<SerializedRegistrationInfo>> RegistrationsByAssembly { get; set; } = new Dictionary<SerialAssembly, List<SerializedRegistrationInfo>>();
  }
}
