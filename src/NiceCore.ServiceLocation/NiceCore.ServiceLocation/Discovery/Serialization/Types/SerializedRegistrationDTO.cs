﻿namespace NiceCore.ServiceLocation.Discovery.Serialization.Types
{
  /// <summary>
  /// DTO of serialized registration
  /// </summary>
  public class SerializedRegistrationDTO : SerializedRegistrationInfo
  {
    /// <summary>
    /// Path to assembly
    /// </summary>
    public string AssemblyPath { get; set; }

    /// <summary>
    /// Fill constructor
    /// </summary>
    /// <param name="i_AssemblyPath"></param>
    public SerializedRegistrationDTO(string i_AssemblyPath)
    {
      AssemblyPath = i_AssemblyPath;
    }

    /// <summary>
    /// Fill constructor
    /// </summary>
    /// <param name="i_Registration"></param>
    /// <param name="i_AssemblyPath"></param>
    public SerializedRegistrationDTO(SerializedRegistrationInfo i_Registration, string i_AssemblyPath) : this(i_AssemblyPath)
    {
      ImplementationTypeName = i_Registration.ImplementationTypeName;
      InterfaceType = i_Registration.InterfaceType;
      RegisterLifestyle = i_Registration.RegisterLifestyle;
    }
  }
}
