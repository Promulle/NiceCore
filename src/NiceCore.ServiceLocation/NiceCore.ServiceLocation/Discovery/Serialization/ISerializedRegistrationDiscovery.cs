﻿using NiceCore.ServiceLocation.Discovery.Serialization.Results;

namespace NiceCore.ServiceLocation.Discovery.Serialization
{
  /// <summary>
  /// Discovery that encapsulates the reading of serialized registrations
  /// </summary>
  public interface ISerializedRegistrationDiscovery
  {
    /// <summary>
    /// Support for reading serializations
    /// </summary>
    IRegistrationSerializationSupport RegistrationSerializationSupport { get; set; }

    /// <summary>
    /// Discovers Registrations in i_Directory
    /// </summary>
    /// <param name="i_Directory"></param>
    SerializedDiscoveryResult DiscoverDirectory(string i_Directory);
  }
}
