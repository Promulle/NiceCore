﻿using System;
using System.Reflection;

namespace NiceCore.ServiceLocation.Discovery
{
  /// <summary>
  /// Error Handler for registration discovery
  /// </summary>
  public interface IDiscoveryErrorHandler
  {
    /// <summary>
    /// Handle generic exception
    /// </summary>
    /// <param name="i_Exception"></param>
    /// <param name="i_FilePath"></param>
    void HandleException(Exception i_Exception, string i_FilePath);

    /// <summary>
    /// Handles BadImageFormatException
    /// </summary>
    /// <param name="i_Exception"></param>
    /// <param name="i_FilePath"></param>
    void HandleBadImageFormatException(BadImageFormatException i_Exception, string i_FilePath);

    /// <summary>
    /// Handles ReflectionTypeLoadException
    /// </summary>
    /// <param name="i_Exception"></param>
    /// <param name="i_FilePath"></param>
    void HandleReflectionTypeLoadException(ReflectionTypeLoadException i_Exception, string i_FilePath);
  }
}
