// Copyright 2021 Sycorax Systemhaus GmbH

using System;
using System.Collections.Generic;

namespace NiceCore.ServiceLocation.Discovery
{
  /// <summary>
  /// Storage containing all namespaces that have been found by the registration discovery
  /// </summary>
  public interface IDiscoveredNamespaceStorage
  {
    /// <summary>
    /// Add the namespace of the type to the namespaces in the compile list
    /// </summary>
    /// <param name="i_Type"></param>
    void AddNamespaceOfType(Type i_Type);

    /// <summary>
    /// Returns all namespaces as distinct string list
    /// </summary>
    /// <returns></returns>
    IEnumerable<string> GetAllNamespaces();
  }
}
