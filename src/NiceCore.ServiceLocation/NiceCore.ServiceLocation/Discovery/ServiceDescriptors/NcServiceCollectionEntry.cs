using Microsoft.Extensions.DependencyInjection;

namespace NiceCore.ServiceLocation.Discovery.ServiceDescriptors
{
  /// <summary>
  /// Entry in NcService Collection
  /// </summary>
  public class NcServiceCollectionEntry
  {
    /// <summary>
    /// Nc Service Descriptor
    /// </summary>
    public required NcServiceRegistrationDescriptor NcServiceDescriptor { get; init; }
    
    /// <summary>
    /// Service Descriptor
    /// </summary>
    public required ServiceDescriptor ServiceDescriptor { get; init; }

    /// <summary>
    /// From Service Descriptor
    /// </summary>
    /// <param name="i_Descriptor"></param>
    /// <returns></returns>
    public static NcServiceCollectionEntry FromServiceDescriptor(ServiceDescriptor i_Descriptor)
    {
      return new()
      {
        ServiceDescriptor = i_Descriptor,
        NcServiceDescriptor = NcServiceRegistrationDescriptor.FromServiceDescriptor(i_Descriptor)
      };
    }

    /// <summary>
    /// From Nc Service Descriptor
    /// </summary>
    /// <param name="i_Descriptor"></param>
    /// <returns></returns>
    public static NcServiceCollectionEntry FromNcServiceDescriptor(NcServiceRegistrationDescriptor i_Descriptor)
    {
      return new()
      {
        NcServiceDescriptor = i_Descriptor,
        ServiceDescriptor = i_Descriptor.ToServiceDescriptor()
      };
    }
  }
}