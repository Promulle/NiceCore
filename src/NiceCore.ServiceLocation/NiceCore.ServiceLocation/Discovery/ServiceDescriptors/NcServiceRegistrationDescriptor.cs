using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using NiceCore.ServiceLocation.Attributes;

namespace NiceCore.ServiceLocation.Discovery.ServiceDescriptors
{
  /// <summary>
  /// Nc Service Registration Descriptor
  /// </summary>
  public class NcServiceRegistrationDescriptor
  {
    /// <summary>
    /// Type the service is registered as
    /// </summary>
    public Type InterfaceType { get; init; }
    
    /// <summary>
    /// Implementing Type
    /// </summary>
    public Type ImplementorType { get; init; }
    
    /// <summary>
    /// Instance
    /// </summary>
    public object Instance { get; init; }
    
    /// <summary>
    /// Lifestyle
    /// </summary>
    public LifeStyles ServiceLifestyle { get; init; }
    
    /// <summary>
    /// Name of custom lifestyle if applicable
    /// </summary>
    public string CustomLifestyleName { get; init; }

    /// <summary>
    /// Alias Attributes
    /// </summary>
    public IReadOnlyCollection<RegisterAliasAttribute> AliasAttributes { get; init; } = Array.Empty<RegisterAliasAttribute>();

    /// <summary>
    /// To Service Descriptor, as service descriptor aliases and custom lifestyles are ignored.
    /// Scoped Lifestyle is mapped as scoped, which is not 100% correct but should be close enough
    /// </summary>
    /// <returns></returns>
    public ServiceDescriptor ToServiceDescriptor()
    {
      if (Instance != null)
      {
        return new(InterfaceType, Instance);
      }

      return new(InterfaceType, ImplementorType, ServiceLifeTimeConverterUtil.ToServiceLifetime(ServiceLifestyle));
    }
    
    /// <summary>
    /// Creates a new ServiceDescriptor with new alias
    /// </summary>
    /// <param name="i_Attribute"></param>
    /// <returns></returns>
    public NcServiceRegistrationDescriptor WithNewAlias(RegisterAliasAttribute i_Attribute)
    {
      var newAliases = new RegisterAliasAttribute[AliasAttributes.Count + 1];
      var i = 0;
      foreach (var aliasAttribute in AliasAttributes)
      {
        newAliases[i] = aliasAttribute;
        i++;
      }

      newAliases[i] = i_Attribute;
      return new()
      {
        Instance = Instance,
        ImplementorType = ImplementorType,
        InterfaceType = InterfaceType,
        ServiceLifestyle = ServiceLifestyle,
        CustomLifestyleName = CustomLifestyleName,
        AliasAttributes = newAliases
      };
    }

    /// <summary>
    /// Checks for equality, skips aliases at the moment. Since double registrations should never differ only in aliases
    /// </summary>
    /// <param name="i_OtherDescriptor"></param>
    /// <returns></returns>
    public bool IsEqualTo(NcServiceRegistrationDescriptor i_OtherDescriptor)
    {
      return InterfaceType == i_OtherDescriptor.InterfaceType &&
             ImplementorType == i_OtherDescriptor.ImplementorType &&
             Instance == i_OtherDescriptor.Instance &&
             CustomLifestyleName == i_OtherDescriptor.CustomLifestyleName &&
             ServiceLifestyle == i_OtherDescriptor.ServiceLifestyle;
    }

    /// <summary>
    /// Create an Nc Descriptor from ServiceDescriptor
    /// </summary>
    /// <param name="i_Descriptor"></param>
    /// <returns></returns>
    public static NcServiceRegistrationDescriptor FromServiceDescriptor(ServiceDescriptor i_Descriptor)
    {
      return new()
      {
        Instance = i_Descriptor.ImplementationInstance,
        InterfaceType = i_Descriptor.ServiceType,
        ImplementorType = i_Descriptor.ImplementationType,
        ServiceLifestyle = ServiceLifeTimeConverterUtil.ToLifeStyle(i_Descriptor.Lifetime)
      };
    }
  }
}