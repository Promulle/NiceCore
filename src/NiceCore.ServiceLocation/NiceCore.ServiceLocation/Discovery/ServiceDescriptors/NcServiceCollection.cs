using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace NiceCore.ServiceLocation.Discovery.ServiceDescriptors
{
  /// <summary>
  /// Nc Service Collection
  /// </summary>
  public sealed class NcServiceCollection : IEnumerable<NcServiceRegistrationDescriptor>
  {
    private readonly List<NcServiceCollectionEntry> m_RegistrationDescriptors = new();
    private readonly NcServiceCollectionAdapter m_Adapter;

    /// <summary>
    /// Ctor
    /// </summary>
    public NcServiceCollection()
    {
      m_Adapter = new(m_RegistrationDescriptors);
    }
    
    /// <summary>
    /// Gets the serviceCollection representation of this serviceCollection.
    /// WARNING: MUTATING THE ISERVICECOLLECTION WILL MUTATE THE NCSERVICECOLLECTION
    /// </summary>
    /// <returns></returns>
    public IServiceCollection GetViewAsServiceCollection()
    {
      return m_Adapter;
    }

    /// <summary>
    /// Remove
    /// </summary>
    /// <param name="i_Descriptor"></param>
    /// <returns></returns>
    public bool Remove(NcServiceRegistrationDescriptor i_Descriptor)
    {
      for (var i = 0; i < m_RegistrationDescriptors.Count; i++)
      {
        var entry = m_RegistrationDescriptors[i];
        if (entry.NcServiceDescriptor.IsEqualTo(i_Descriptor))
        {
          return m_RegistrationDescriptors.Remove(entry);
        }
      }

      return false;
    }

    /// <summary>
    /// Add
    /// </summary>
    /// <param name="i_Descriptor"></param>
    public void Add(NcServiceRegistrationDescriptor i_Descriptor)
    {
      m_RegistrationDescriptors.Add(NcServiceCollectionEntry.FromNcServiceDescriptor(i_Descriptor));
    }

    /// <summary>
    /// Add Transient
    /// </summary>
    /// <typeparam name="TInterface"></typeparam>
    /// <typeparam name="TImpl"></typeparam>
    public void AddTransient<TInterface, TImpl>()
    {
      Add(new()
      {
        Instance = null,
        InterfaceType = typeof(TInterface),
        ImplementorType = typeof(TImpl),
        ServiceLifestyle = LifeStyles.Transient
      });
    }

    /// <summary>
    /// Add Transient
    /// </summary>
    /// <typeparam name="TInterface"></typeparam>
    /// <typeparam name="TImpl"></typeparam>
    public void AddSingleton<TInterface, TImpl>()
    {
      Add(new()
      {
        Instance = null,
        InterfaceType = typeof(TInterface),
        ImplementorType = typeof(TImpl),
        ServiceLifestyle = LifeStyles.Singleton
      });
    }

    /// <summary>
    /// Adds the descriptor only if there is no descriptor that matches the interface type of i_Descriptor
    /// </summary>
    /// <param name="i_Descriptor"></param>
    /// <returns>true if the descriptor was actually added</returns>
    public bool TryAdd(NcServiceRegistrationDescriptor i_Descriptor)
    {
      if (m_RegistrationDescriptors.Any(r => r.NcServiceDescriptor.InterfaceType == i_Descriptor.InterfaceType))
      {
        return false;
      }

      Add(i_Descriptor);
      return true;
    }

    /// <summary>
    /// Removes a registration descriptor by i_InterfaceType
    /// </summary>
    /// <param name="i_InterfaceType"></param>
    public void RemoveByInterfaceType(Type i_InterfaceType)
    {
      m_RegistrationDescriptors.RemoveAll(r => r.NcServiceDescriptor.InterfaceType == i_InterfaceType);
    }


    /// <summary>
    /// Get Enumerator
    /// </summary>
    /// <returns></returns>
    public IEnumerator<NcServiceRegistrationDescriptor> GetEnumerator()
    {
      foreach (var entry in m_RegistrationDescriptors)
      {
        yield return entry.NcServiceDescriptor;
      }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }
  }
}