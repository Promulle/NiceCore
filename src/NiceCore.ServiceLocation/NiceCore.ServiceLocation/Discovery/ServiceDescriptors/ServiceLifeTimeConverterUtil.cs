using Microsoft.Extensions.DependencyInjection;

namespace NiceCore.ServiceLocation.Discovery.ServiceDescriptors
{
  /// <summary>
  /// Service LifeTime Convert Util
  /// </summary>
  public static class ServiceLifeTimeConverterUtil
  {
    /// <summary>
    /// To Service Lifetime
    /// </summary>
    /// <param name="i_LifeStyle"></param>
    /// <returns></returns>
    public static ServiceLifetime ToServiceLifetime(LifeStyles i_LifeStyle)
    {
      return i_LifeStyle switch
      {
        LifeStyles.Singleton => ServiceLifetime.Singleton,
        LifeStyles.Transient => ServiceLifetime.Transient,
        LifeStyles.Scoped => ServiceLifetime.Scoped,
        LifeStyles.Custom => ServiceLifetime.Transient,
        _ => ServiceLifetime.Transient
      };
    }

    /// <summary>
    /// To Life Style
    /// </summary>
    /// <param name="i_Lifetime"></param>
    /// <returns></returns>
    public static LifeStyles ToLifeStyle(ServiceLifetime i_Lifetime)
    {
      return i_Lifetime switch
      {
        ServiceLifetime.Scoped => LifeStyles.Scoped,
        ServiceLifetime.Singleton => LifeStyles.Singleton,
        ServiceLifetime.Transient => LifeStyles.Transient,
        _ => LifeStyles.Transient
      };
    }
  }
}