namespace NiceCore.ServiceLocation.Discovery.ServiceDescriptors
{
  /// <summary>
  /// Nc Service Collection Extensions
  /// </summary>
  public static class NcServiceCollectionExtensions
  {
    /// <summary>
    /// Adds an instance descriptor to the collection
    /// </summary>
    /// <param name="t_Collection"></param>
    /// <param name="i_Instance"></param>
    /// <typeparam name="TInterface"></typeparam>
    public static void AddInstance<TInterface>(this NcServiceCollection t_Collection, object i_Instance)
    {
      t_Collection.Add(new()
      {
        Instance = i_Instance,
        ImplementorType = i_Instance.GetType(),
        InterfaceType = typeof(TInterface),
        ServiceLifestyle = LifeStyles.Singleton
      });
    }
  }
}