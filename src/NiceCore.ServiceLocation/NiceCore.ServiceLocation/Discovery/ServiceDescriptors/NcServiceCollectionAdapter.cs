using System.Collections;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;

namespace NiceCore.ServiceLocation.Discovery.ServiceDescriptors
{
  /// <summary>
  /// Adapter for enabling IServiceCollection Functionality for NiceCore Format
  /// WARNING: This class can mutate the entries of its parent class, SO EVEN ENTRIES CREATED BY NICECORE
  /// </summary>
  public class NcServiceCollectionAdapter : IServiceCollection
  {
    private readonly List<NcServiceCollectionEntry> m_Entries;
    
    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_Entries"></param>
    public NcServiceCollectionAdapter(List<NcServiceCollectionEntry> i_Entries)
    {
      m_Entries = i_Entries;
    }

    /// <summary>
    /// Get Enumerator
    /// </summary>
    /// <returns></returns>
    public IEnumerator<ServiceDescriptor> GetEnumerator()
    {
      foreach (var entry in m_Entries)
      {
        yield return entry.ServiceDescriptor;
      }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }

    private static NcServiceCollectionEntry CreateEntry(ServiceDescriptor i_Descriptor)
    {
      return new()
      {
        NcServiceDescriptor = NcServiceRegistrationDescriptor.FromServiceDescriptor(i_Descriptor),
        ServiceDescriptor = i_Descriptor
      };
    }

    /// <summary>
    /// Add
    /// </summary>
    /// <param name="item"></param>
    public void Add(ServiceDescriptor item)
    {
      m_Entries.Add(CreateEntry(item));
    }

    /// <summary>
    /// Clear
    /// </summary>
    public void Clear()
    {
      m_Entries.Clear();
    }

    /// <summary>
    /// Contains
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public bool Contains(ServiceDescriptor item)
    {
      foreach (var entry in m_Entries)
      {
        if (entry.ServiceDescriptor == item)
        {
          return true;
        }
      }
      return false;
    }

    /// <summary>
    /// Copy To
    /// </summary>
    /// <param name="array"></param>
    /// <param name="arrayIndex"></param>
    public void CopyTo(ServiceDescriptor[] array, int arrayIndex)
    {
      for (var i = 0; i < m_Entries.Count; i++)
      {
        array[i + arrayIndex] = m_Entries[i].ServiceDescriptor;
      }
    }

    /// <summary>
    /// Remove
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public bool Remove(ServiceDescriptor item)
    {
      for (var i = 0; i < m_Entries.Count; i++)
      {
        var entry = m_Entries[i];
        if (entry.ServiceDescriptor == item)
        {
          m_Entries.Remove(entry);
          return true;
        }
      }
      return false;
    }

    /// <summary>
    /// Count
    /// </summary>
    public int Count => m_Entries.Count;
    
    /// <summary>
    /// Is Read Only
    /// </summary>
    public bool IsReadOnly { get; } = false;
    
    /// <summary>
    /// Index Of
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public int IndexOf(ServiceDescriptor item)
    {
      for (var i = 0; i < m_Entries.Count; i++)
      {
        var entry = m_Entries[i];
        if (entry.ServiceDescriptor == item)
        {
          return i;
        }
      }

      return -1;
    }

    /// <summary>
    /// Insert
    /// </summary>
    /// <param name="index"></param>
    /// <param name="item"></param>
    public void Insert(int index, ServiceDescriptor item)
    {
      m_Entries.Insert(index, CreateEntry(item));
    }

    /// <summary>
    /// Remove At
    /// </summary>
    /// <param name="index"></param>
    public void RemoveAt(int index)
    {
      m_Entries.RemoveAt(index);
    }

    /// <summary>
    /// this Accessor
    /// </summary>
    /// <param name="index"></param>
    public ServiceDescriptor this[int index]
    {
      get => m_Entries[index].ServiceDescriptor;
      set => m_Entries[index] = CreateEntry(value);
    }
  }
}