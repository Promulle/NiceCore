using System;

namespace NiceCore.ServiceLocation.Discovery
{
  /// <summary>
  /// Override registration for a registration discovery
  /// </summary>
  public sealed class DiscoveryOverrideRegistration
  {
    /// <summary>
    /// Registration Type
    /// </summary>
    public Type RegistrationType { get; init; }
    
    /// <summary>
    /// Implementation type
    /// </summary>
    public Type ImplementationType { get; init; }

    /// <summary>
    /// Registration lifestyle
    /// </summary>
    public LifeStyles RegistrationLifeStyle { get; init; } = LifeStyles.Transient;
  }
}