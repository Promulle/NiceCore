using System.Collections.Generic;
using NiceCore.ServiceLocation.Attributes;

namespace NiceCore.ServiceLocation.Discovery
{
  /// <summary>
  /// Attributes that have been found on a type
  /// </summary>
  public class FoundAttributes
  {
    /// <summary>
    /// Attributes that were found, alias attributes are already matched to their fitting register attribute
    /// </summary>
    public IReadOnlyDictionary<RegisterAttribute, List<RegisterAliasAttribute>> Attributes { get; init; }
  }
}