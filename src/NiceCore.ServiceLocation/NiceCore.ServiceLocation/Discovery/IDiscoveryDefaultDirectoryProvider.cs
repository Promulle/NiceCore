﻿namespace NiceCore.ServiceLocation.Discovery
{
  /// <summary>
  /// Provider that returns the default directory to use when no search directory was supplied to the registration discovery
  /// </summary>
  public interface IDiscoveryDefaultDirectoryProvider
  {
    /// <summary>
    /// Get Default directory
    /// </summary>
    /// <returns></returns>
    string GetDefaultDirectory();
  }
}