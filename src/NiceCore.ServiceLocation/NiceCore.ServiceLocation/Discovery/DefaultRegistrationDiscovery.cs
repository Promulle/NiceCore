﻿using NiceCore.Extensions;
using NiceCore.ServiceLocation.Discovery.Serialization;
using NiceCore.ServiceLocation.Discovery.Serialization.Results;
using NiceCore.ServiceLocation.Discovery.Serialization.Serializer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using NiceCore.ServiceLocation.Attributes;
using NiceCore.ServiceLocation.Discovery.DefaultDirectoryProviders;
using NiceCore.ServiceLocation.Discovery.ServiceDescriptors;

namespace NiceCore.ServiceLocation.Discovery
{
  /// <summary>
  /// Base impl of Discovery
  /// </summary>
  public sealed class DefaultRegistrationDiscovery : IRegistrationDiscovery
  {
    private static readonly DiscoveryRunConfiguration DEFAULT_CONFIGURATION = new()
    {
      TargetMode = DiscoveryModes.AssembliesAndFiles,
      AssemblyTargetMode = AssemblyProvidingModes.All,
      DiscoveryDefaultDirectoryProvider = new EntryAssemblyDefaultDirectoryProvider(),
      AllowSearchOfLoadedAssemblies = false
    };
    
    /// <summary>
    /// Event that is thrown when the discovery has been finished
    /// </summary>
    public event EventHandler DiscoveryFinished;

    /// <summary>
    /// Serialization
    /// </summary>
    public ISerializedRegistrationDiscovery SerializationDiscovery { get; set; }

    /// <summary>
    /// Discover
    /// </summary>
    public NcServiceCollection Discover(NcServiceCollection t_ExistingCollection, DiscoveryRunConfiguration i_Configuration)
    {
      var searchDirectories = i_Configuration.GetSearchDirectories();
      if (i_Configuration.IsDiscoverFilesEnabled())
      {
        DiscoverFiles(t_ExistingCollection, searchDirectories, i_Configuration);
      }
      if (i_Configuration.IsDiscoverAssembliesEnabled())
      {
        if (SerializationDiscovery != null)
        {
          foreach (var dir in searchDirectories)
          {
            var result = SerializationDiscovery.DiscoverDirectory(dir);
            using var serializer = SerializationDiscovery.RegistrationSerializationSupport.CreateNewSerializer(GetCorrectSerializationFile(dir, result.FilePath), result.OriginalRegistrations);
            if (result.Success)
            {
              foreach (var registration in result.Registrations)
              {
                RegisterSerializedRegistration(registration, t_ExistingCollection);
              }

              foreach (var assemblyFileName in result.AssembliesToReload)
              {
                SearchAssemblyFile(assemblyFileName, t_ExistingCollection, i_Configuration, serializer);
              }
            }
            else
            {
              var moduleProvider = i_Configuration.GetModuleProvider();
              foreach (var file in moduleProvider.GetModuleFiles(dir))
              {
                SearchAssemblyFile(file, t_ExistingCollection, i_Configuration, serializer);
              }
            }
          }
        }
        else
        {
          SearchAllAssemblies(t_ExistingCollection, searchDirectories, i_Configuration);
        }
      }

      if (i_Configuration.OverrideRegistrations != null && i_Configuration.OverrideRegistrations.Count > 0)
      {
        foreach (var discoveryOverrideRegistration in i_Configuration.OverrideRegistrations)
        {
          t_ExistingCollection.RemoveByInterfaceType(discoveryOverrideRegistration.RegistrationType);
          t_ExistingCollection.Add(new()
          {
            InterfaceType = discoveryOverrideRegistration.RegistrationType,
            ImplementorType = discoveryOverrideRegistration.ImplementationType,
            ServiceLifestyle = discoveryOverrideRegistration.RegistrationLifeStyle
          });
        }
      }
      DiscoveryFinished?.Invoke(this, EventArgs.Empty);
      return t_ExistingCollection;
    }
    
    /// <summary>
    /// Discover
    /// </summary>
    public NcServiceCollection Discover(DiscoveryRunConfiguration i_Configuration)
    {
      return Discover(new(), i_Configuration);
    }
    
    /// <summary>
    /// Discover
    /// </summary>
    public NcServiceCollection Discover()
    {
      return Discover(new(), DEFAULT_CONFIGURATION);
    }
    
    /// <summary>
    /// Discover
    /// </summary>
    public NcServiceCollection Discover(NcServiceCollection t_Collection)
    {
      return Discover(t_Collection, DEFAULT_CONFIGURATION);
    }

    private static string GetCorrectSerializationFile(string i_Directory, string i_FilePath)
    {
      if (i_FilePath == null)
      {
        return i_Directory + Path.DirectorySeparatorChar + "nicecore" + SerializationConstants.SERIAL_FILE_EXTENSION;
      }
      return i_FilePath;
    }

    /// <summary>
    /// Serialize a registration
    /// </summary>
    /// <param name="i_AssemblyPath"></param>
    /// <param name="i_InterfaceType"></param>
    /// <param name="i_ImplementationType"></param>
    /// <param name="i_Lifestyle"></param>
    /// <param name="t_Serializer"></param>
    private void SerializeRegistration(string i_AssemblyPath, Type i_InterfaceType, Type i_ImplementationType, LifeStyles i_Lifestyle, IRegistrationsSerializer t_Serializer)
    {
      t_Serializer?.AddRegistrations(new(i_AssemblyPath)
      {
        ImplementationTypeName = i_ImplementationType.AssemblyQualifiedName,
        RegisterLifestyle = i_Lifestyle,
        InterfaceType = i_InterfaceType.AssemblyQualifiedName
      });
    }
    
    /// <summary>
    /// registers a serialized registration
    /// </summary>
    /// <param name="i_Result"></param>
    /// <param name="i_Collection"></param>
    private static void RegisterSerializedRegistration(RegistrationResult i_Result, NcServiceCollection i_Collection)
    {
      var reg = new NcServiceRegistrationDescriptor()
      {
        InterfaceType = i_Result.InterfaceType,
        ImplementorType = i_Result.ImplemementationType,
        ServiceLifestyle = i_Result.Lifestyle
      };
      i_Collection.Add(reg);
    }

    private void SearchAllAssemblies(NcServiceCollection t_Collection, IReadOnlyCollection<string> i_SearchDirectories, DiscoveryRunConfiguration i_Configuration)
    {
      SearchLoadedAssemblies(t_Collection, i_Configuration);
      SearchAssemblyFiles(t_Collection, i_SearchDirectories, i_Configuration);
    }

    private void SearchLoadedAssemblies(NcServiceCollection t_Collection, DiscoveryRunConfiguration i_Configuration)
    {
      if (i_Configuration.AllowSearchOfLoadedAssemblies)
      {
        AppDomain.CurrentDomain.GetAssemblies()
          .ForEachItem(r => SearchProvidedAssembly(() => (() => r, r.FullName), t_Collection, i_Configuration, null));
      }
    }

    private void SearchAssemblyFiles(NcServiceCollection t_Collection, IReadOnlyCollection<string> i_SearchDirectories, DiscoveryRunConfiguration i_Configuration)
    {
      var moduleProvider = i_Configuration.GetModuleProvider();
      foreach (var dir in i_SearchDirectories)
      {
        foreach (var file in moduleProvider.GetModuleFiles(dir))
        {
          SearchAssemblyFile(file, t_Collection, i_Configuration, null);
        }
      }
    }

    private void SearchProvidedAssembly(Func<(Func<Assembly> AssemblyFunc, string Name)> i_AssemblyFunc, NcServiceCollection t_ServiceCollection, DiscoveryRunConfiguration i_Configuration, IRegistrationsSerializer i_Serializer)
    {
      try
      {
        var assembly = i_AssemblyFunc().AssemblyFunc();
        if (assembly.Location != null)
        {
          i_Serializer?.AddAssembly(assembly.Location);
        }
        SearchAssembly(assembly, t_ServiceCollection, i_Configuration, i_Serializer);
      }
      catch (ReflectionTypeLoadException typeEx)
      {
        i_Configuration.DiscoveryErrorHandler?.HandleReflectionTypeLoadException(typeEx, i_AssemblyFunc().Name);
      }
      catch (BadImageFormatException imageEx)
      {
        i_Configuration.DiscoveryErrorHandler?.HandleBadImageFormatException(imageEx, i_AssemblyFunc().Name);
        
      }
      catch (Exception ex)
      {
        i_Configuration.DiscoveryErrorHandler?.HandleException(ex, i_AssemblyFunc().Name);
      }
    }

    /// <summary>
    /// Searches a file for assembly
    /// </summary>
    /// <param name="filePath"></param>
    /// <param name="t_ServiceCollection"></param>
    /// <param name="i_Configuration"></param>
    /// <param name="i_Serializer"></param>
    private void SearchAssemblyFile(string filePath, NcServiceCollection t_ServiceCollection, DiscoveryRunConfiguration i_Configuration, IRegistrationsSerializer i_Serializer)
    {
      SearchProvidedAssembly(() => (() => Assembly.LoadFrom(filePath), filePath), t_ServiceCollection, i_Configuration, i_Serializer);
    }

    /// <summary>
    /// Discovers Registrations based on files
    /// </summary>
    private static void DiscoverFiles(NcServiceCollection t_ServiceCollection, IReadOnlyCollection<string> i_SearchDirectories, DiscoveryRunConfiguration i_Configuration)
    {
      if (i_Configuration.DependencyFileAdapter == null)
      {
        return;
      }
      foreach (var dir in i_SearchDirectories)
      {
        foreach (var serviceDescriptor in i_Configuration.DependencyFileAdapter.DiscoverFiles(dir))
        {
          t_ServiceCollection.Add(serviceDescriptor);
        }
      }
    }

    /// <summary>
    /// Search an assembly for registrations
    /// </summary>
    /// <param name="i_Assembly"></param>
    /// <param name="t_ServiceCollection"></param>
    /// <param name="i_Configuration"></param>
    /// <param name="t_Serializer"></param>
    private void SearchAssembly(Assembly i_Assembly, NcServiceCollection t_ServiceCollection, DiscoveryRunConfiguration i_Configuration, IRegistrationsSerializer t_Serializer)
    {
      foreach (var singleClass in i_Assembly.GetTypes())
      {
        try
        {
          i_Configuration.DiscoveredNamespaceStorage?.AddNamespaceOfType(singleClass);
          RegisterByAttributes(singleClass, t_ServiceCollection, i_Configuration, t_Serializer);
        }
        catch (Exception ex)
        {
          Console.WriteLine("ERROR - Error registering " + singleClass.FullName, this, ex);
        }
      }
    }
    
    private void RegisterByAttributes(Type i_Type, NcServiceCollection t_ServiceCollection, DiscoveryRunConfiguration i_Configuration, IRegistrationsSerializer t_Serializer)
    {
      foreach (var (regAttr, aliases) in TypeAttributeFinder.FindAttributesOnType(i_Type, i_Configuration.CustomRegistrationAttributeHandlers).Attributes)
      {
        CreateDefaultRegistrations(i_Type, t_ServiceCollection, regAttr, aliases, t_Serializer);
      }
    }
    
    private void CreateDefaultRegistrations(Type i_Type, NcServiceCollection t_Collection, RegisterAttribute i_RegAttr, List<RegisterAliasAttribute> i_Aliases, IRegistrationsSerializer t_Serializer)
    {
      var attrDescriptor = CreateDescriptorFromAttributes(i_Type, i_RegAttr, i_Aliases);
      t_Collection.Add(attrDescriptor);
      //TODO: how does serialization work with aliases? Not at all for now
      SerializeRegistration(i_Type.Assembly.Location, i_RegAttr.InterfaceType, i_Type, i_RegAttr.LifeStyle, t_Serializer);
    }

    private static NcServiceRegistrationDescriptor CreateDescriptorFromAttributes(Type i_Type, RegisterAttribute i_Attribute, List<RegisterAliasAttribute> i_AliasAttributes)
    {
      var lifeStyle = i_Attribute.CustomLifeStyleName != null ? LifeStyles.Custom : i_Attribute.LifeStyle;
      return new()
      {
        InterfaceType = i_Attribute.InterfaceType ?? i_Type,
        ImplementorType = i_Type,
        ServiceLifestyle = lifeStyle,
        AliasAttributes = i_AliasAttributes,
        CustomLifestyleName = i_Attribute.CustomLifeStyleName
      };
    }
  }
}
