﻿using System;

namespace NiceCore.ServiceLocation
{
  /// <summary>
  /// Attribute signaling that this property should not be resolved
  /// </summary>
  [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
  public class DoNotResolveAttribute : Attribute
  {
  }
}
