﻿using System;

namespace NiceCore.ServiceLocation.Attributes
{
  /// <summary>
  /// Attribute that can be used to declare a constructor to be used for injection.
  /// </summary>
  [AttributeUsage(AttributeTargets.Constructor, AllowMultiple = false, Inherited = true)]
  public class InjectConstructorAttribute : Attribute
  {
  }
}
