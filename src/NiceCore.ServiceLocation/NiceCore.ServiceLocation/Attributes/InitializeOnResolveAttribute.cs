﻿using System;

namespace NiceCore.ServiceLocation
{
  /// <summary>
  /// Attribute signaling that an instance can be initialized on resolve
  /// </summary>
  [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
  public class InitializeOnResolveAttribute : Attribute
  {
  }
}
