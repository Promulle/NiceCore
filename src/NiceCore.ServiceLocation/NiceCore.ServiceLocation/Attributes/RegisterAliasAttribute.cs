using System;

namespace NiceCore.ServiceLocation.Attributes
{
  /// <summary>
  /// Register that enables creating an alias registration for another registration
  /// </summary>
  [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
  public class RegisterAliasAttribute : Attribute
  {
    /// <summary>
    /// Original registration type -> type that the original registration is using as "interfaceType"
    /// </summary>
    public Type OriginalRegistrationType { get; }
    
    /// <summary>
    /// Type that is used as an alias for the registration
    /// </summary>
    public Type AliasType { get; }

    /// <summary>
    /// Ctor setting attributes because both values are mandatory here
    /// </summary>
    /// <param name="i_OriginalRegistrationType"></param>
    /// <param name="i_AliasType"></param>
    public RegisterAliasAttribute(Type i_OriginalRegistrationType, Type i_AliasType)
    {
      OriginalRegistrationType = i_OriginalRegistrationType;
      AliasType = i_AliasType;
    }
  }
}