﻿using System;

namespace NiceCore.ServiceLocation
{
  /// <summary>
  /// Attribute, that can be used to register services.
  /// This attribute (or its wrapped counterparts) will be searched by the default ioc-discovery
  /// </summary>
  [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
  public class RegisterAttribute : Attribute
  {
    /// <summary>
    /// REQUIRED
    /// Interface to register for
    /// </summary>
    public Type InterfaceType { get; set; }
    /// <summary>
    /// OPTIONAL
    /// Lifestyle of this registration
    /// Default: Transient
    /// </summary>
    public LifeStyles LifeStyle { get; set; }

    /// <summary>
    /// OPTIONAL
    /// Name of custom lifestyle to use, only applies if LifeStyle == LifeStyles.Custom
    /// </summary>
    public string CustomLifeStyleName { get; set; }
    
    /// <summary>
    /// Default constructor
    /// </summary>
    public RegisterAttribute()
    {
      LifeStyle = LifeStyles.Transient;
    }
  }
}
