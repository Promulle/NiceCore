﻿using NiceCore.ServiceLocation.Interface;
using System;

namespace NiceCore.ServiceLocation.Attributes
{
  /// <summary>
  /// Attribute that determines what visbility is the max for properties to be injected
  /// </summary>
  [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
  public class InjectionVisibilitityAttribute : Attribute
  {
    /// <summary>
    /// Visibility level to be injected
    /// </summary>
    public MethodVisibility MaxInjectionVisibility { get; set; }
  }
}
