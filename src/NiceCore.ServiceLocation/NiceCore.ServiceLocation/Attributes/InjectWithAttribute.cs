﻿using System;

namespace NiceCore.ServiceLocation.Attributes
{
  /// <summary>
  /// Attribute that changes how a property is injected
  /// </summary>
  [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
  public class ResolveAsAttribute : Attribute
  {
    /// <summary>
    /// Name of the service to be injected
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Type the service registered as -> alternative to using the declared type
    /// </summary>
    public Type AlternativeInterfaceType { get; set; }
  }
}
