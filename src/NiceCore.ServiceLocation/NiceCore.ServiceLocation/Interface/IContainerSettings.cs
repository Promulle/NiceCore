﻿using NiceCore.Base;
using System.Collections.Generic;

namespace NiceCore.ServiceLocation
{
  /// <summary>
  /// Interface for ContainerSettings
  /// </summary>
  public interface IContainerSettings
  {
    /// <summary>
    /// Whether or not an instance should be initialized, when it is resolved.
    /// The instance must implement IInitializable for this to work.
    /// Default: false
    /// </summary>
    bool AllowInitializeOnResolve { get; set; }

    /// <summary>
    /// Whether or not instances should be released, if they are being disposed
    /// Default: false
    /// </summary>
    bool AllowReleaseOnDispose { get; set; }

    /// <summary>
    /// Whether or not Singletons can be released by the container.
    /// Default: true
    /// </summary>
    bool AllowReleaseSingletons { get; set; }

    /// <summary>
    /// What the container should do, if it encounters a circular reference
    /// </summary>
    CyclicDependencyBehavior CyclicBehavior { get; set; }

    /// <summary>
    /// Mode to use while searching for registrations
    /// </summary>
    DiscoveryModes SearchMode { get; set; }

    /// <summary>
    /// Mode to use while searching for assemblies to draw registrations from default: All
    /// </summary>
    AssemblyProvidingModes AssemblySearchMode { get; set; }

    /// <summary>
    /// Directories to search in
    /// </summary>
    List<ISearchDirectory> SearchDirectories { get; set; }
  }
}