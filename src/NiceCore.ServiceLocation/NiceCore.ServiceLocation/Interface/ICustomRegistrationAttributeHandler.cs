﻿using System;
using System.Collections.Generic;

namespace NiceCore.ServiceLocation
{
  /// <summary>
  /// Interface for custom handlers that can parse attributes as register attribute
  /// </summary>
  public interface ICustomRegistrationAttributeHandler
  {
    /// <summary>
    /// Handle custom register attributes on i_Type
    /// </summary>
    /// <param name="i_Attribute">Attribute to check for registration info</param>
    /// <param name="i_Type">Type the attribute is applied to</param>
    /// <returns></returns>
    ICollection<RegisterAttribute> HandleCustomAttribute(Attribute i_Attribute, Type i_Type);
  }
}