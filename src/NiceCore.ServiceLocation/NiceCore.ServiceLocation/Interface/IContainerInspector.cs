﻿namespace NiceCore.ServiceLocation
{
  /// <summary>
  /// Inspector for container providing count and other handy functions for the Specific IServiceContainer implementation
  /// </summary>
  public interface IContainerInspector
  {
    /// <summary>
    /// Checks if there is an implementation for T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    bool HasImplementation<T>();

    /// <summary>
    /// Checks whether there are not released instances of T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    bool HasInstances<T>();

    /// <summary>
    /// Checks whether there are not released instances which are  registered for T and i_Name
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    bool HasInstances<T>(string i_Name);

    /// <summary>
    /// Checks how many registrations can be found for T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    int RegistrationCount<T>();

    /// <summary>
    /// Returns number of registrations registered for T
    /// </summary>
    /// <returns></returns>
    int OverallRegistrationCount();

    /// <summary>
    /// Checks how many instances have been resolved and NOT released for T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    int InstanceCount<T>();
  }
}
