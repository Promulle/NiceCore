﻿using Microsoft.Extensions.DependencyInjection;
using NiceCore.ServiceLocation.Discovery;
using NiceCore.ServiceLocation.Discovery.ServiceDescriptors;

namespace NiceCore.ServiceLocation.Interface
{
  /// <summary>
  /// File adapter for integrating file based registrations into the default registration discovery.
  /// If you want to use custom file registrations for your impl/the default impl of IServiceContainer you should implement this interface, else use the default.
  /// </summary>
  public interface IDependencyFileAdapter
  {
    /// <summary>
    /// Searches for file registrations in i_Directory.
    /// This function is expected to only search in the directory itself, not in any subdirectory
    /// </summary>
    /// <param name="i_Directory"></param>
    NcServiceCollection DiscoverFiles(string i_Directory);
  }
}
