﻿using NiceCore.ServiceLocation.Discovery;
using NiceCore.ServiceLocation.Discovery.Serialization;
using NiceCore.ServiceLocation.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using NiceCore.ServiceLocation.Discovery.DefaultDirectoryProviders;
using NiceCore.ServiceLocation.Discovery.ServiceDescriptors;

namespace NiceCore.ServiceLocation
{
  /// <summary>
  /// Interface for registration discovery
  /// </summary>
  public interface IRegistrationDiscovery
  {
    /// <summary>
    /// Event that is thrown when the discovery has been finished
    /// </summary>
    event EventHandler DiscoveryFinished;

    /// <summary>
    /// Serialization
    /// </summary>
    ISerializedRegistrationDiscovery SerializationDiscovery { get; set; }

    /// <summary>
    /// Discover
    /// </summary>
    /// <param name="t_ExistingCollection"></param>
    /// <param name="i_Configuration"></param>
    /// <returns></returns>
    NcServiceCollection Discover(NcServiceCollection t_ExistingCollection, DiscoveryRunConfiguration i_Configuration);

    /// <summary>
    /// Discover
    /// </summary>
    /// <param name="t_ExistingCollection"></param>
    /// <returns></returns>
    NcServiceCollection Discover(NcServiceCollection t_ExistingCollection);

    /// <summary>
    /// Discover
    /// </summary>
    public NcServiceCollection Discover(DiscoveryRunConfiguration i_Configuration);
    
    /// <summary>
    /// Start discovery
    /// </summary>
    NcServiceCollection Discover();
  }
}