﻿using System;

namespace NiceCore.ServiceLocation
{
  /// <summary>
  /// Plugin realising functionality for lifecycles in container
  /// </summary>
  public interface IDependencyInjectionLifecyclePlugin
  {
    /// <summary>
    /// Whether the plugin is enabled or not
    /// </summary>
    bool IsEnabled { get; set; }

    /// <summary>
    /// Enables this plugin for i_Container
    /// </summary>
    /// <param name="i_Container"></param>
    void EnablePlugin(IServiceContainer i_Container);

    /// <summary>
    /// Is called when an instance has been resolved for i_InterfaceType
    /// </summary>
    /// <param name="t_Instance"></param>
    /// <param name="i_InterfaceType"></param>
    void InstanceResolved(object t_Instance, Type i_InterfaceType);

    /// <summary>
    /// Is called when an instance has been released
    /// </summary>
    /// <param name="t_Instance"></param>
    void InstanceReleased(object t_Instance);
  }
}
