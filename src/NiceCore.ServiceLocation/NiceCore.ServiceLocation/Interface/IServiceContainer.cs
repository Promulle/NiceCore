﻿using Microsoft.Extensions.DependencyInjection;
using NiceCore.Base;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using NiceCore.ServiceLocation.Attributes;
using NiceCore.ServiceLocation.Discovery;
using NiceCore.ServiceLocation.Discovery.ServiceDescriptors;

namespace NiceCore.ServiceLocation
{
  /// <summary>
  /// Container holding service registrations
  /// </summary>
  public interface IServiceContainer : IDisposable
  {
    /// <summary>
    /// Plugins to be used by the container
    /// </summary>
    IList<IDependencyInjectionLifecyclePlugin> Plugins { get; }

    #region Rewire - Functions to change existing registrations 
    /// <summary>
    /// Rewires existing registration to use different type of implementor.
    /// Will only work if a registration has NO instances
    /// </summary>
    /// <typeparam name="TInterfaceType"></typeparam>
    /// <typeparam name="TOldImplementorType"></typeparam>
    /// <typeparam name="TNewImplementorType"></typeparam>
    void Rewire<TInterfaceType, TOldImplementorType, TNewImplementorType>();

    /// <summary>
    /// Rewires existing registration to use different type of implementor.
    /// Will only work if a registration has NO instances
    /// </summary>
    /// <param name="i_Name"></param>
    /// <typeparam name="TInterfaceType"></typeparam>
    /// <typeparam name="TOldImplementorType"></typeparam>
    /// <typeparam name="TNewImplementorType"></typeparam>
    void Rewire<TInterfaceType, TOldImplementorType, TNewImplementorType>(string i_Name);

    /// <summary>
    /// Rewires existing registration to use different type of implementor.
    /// Will only work if a registration has NO instances
    /// </summary>
    /// <param name="i_Name"></param>
    /// <param name="i_Lifestyle"></param>
    /// <typeparam name="TInterfaceType"></typeparam>
    /// <typeparam name="TOldImplementorType"></typeparam>
    /// <typeparam name="TNewImplementorType"></typeparam>
    void Rewire<TInterfaceType, TOldImplementorType, TNewImplementorType>(string i_Name, LifeStyles i_Lifestyle);

    /// <summary>
    /// Rewires existing registration to use different type of implementor.
    /// Will only work if a registration has NO instances
    /// </summary>
    /// <param name="i_Lifestyle"></param>
    /// <typeparam name="TInterfaceType"></typeparam>
    /// <typeparam name="TOldImplementorType"></typeparam>
    /// <typeparam name="TNewImplementorType"></typeparam>
    void Rewire<TInterfaceType, TOldImplementorType, TNewImplementorType>(LifeStyles i_Lifestyle);
    #endregion
    
    /// <summary>
    /// Register
    /// </summary>
    /// <param name="i_InterfaceTypes"></param>
    /// <param name="i_ImplementingType"></param>
    /// <param name="i_Lifestyle"></param>
    /// <param name="i_CustomLifestyleName"></param>
    void Register(IReadOnlyCollection<Type> i_InterfaceTypes, Type i_ImplementingType, LifeStyles i_Lifestyle, string i_CustomLifestyleName);

    /// <summary>
    /// Registers an instance non generic
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_InterfaceType"></param>
    /// <param name="i_LifeStyle"></param>
    /// <param name="i_CustomLifestyleName"></param>
    void RegisterInstance(object i_Instance, Type i_InterfaceType, LifeStyles i_LifeStyle = LifeStyles.Transient, string i_CustomLifestyleName = null);

    /// <summary>
    /// Releases the instance
    /// </summary>
    /// <param name="i_Instance"></param>
    void Release(object i_Instance);

    /// <summary>
    /// Releases the Instance
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_ReleaseSingleton"></param>
    void Release(object i_Instance, bool i_ReleaseSingleton);

    /// <summary>
    /// Releases all stored registrations
    /// </summary>
    void ReleaseAll();

    #region Resolve
    /// <summary>
    /// Resolves an instance for typeof(T)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    T Resolve<T>();

    /// <summary>
    /// Resolves using i_Type parameter, casts result to T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Type"></param>
    /// <returns></returns>
    T Resolve<T>(Type i_Type);

    /// <summary>
    /// Resolves an instance for typeof(T) specified by name
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    T Resolve<T>(string i_Name);

    /// <summary>
    /// Resolves all registered instances for T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    IEnumerable<T> ResolveAll<T>();

    /// <summary>
    /// Resolve all and casts to T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    IEnumerable<T> ResolveAll<T>(Type i_InterfaceType);

    /// <summary>
    /// Deletes all registrations for type of T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    bool Drop<T>();

    /// <summary>
    /// Deletes the registration which is registered for type T and name i_Name
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Name">Name the registration to delete is registered under</param>
    /// <returns></returns>
    bool Drop<T>(string i_Name);

    /// <summary>
    /// Drops registration that is registered for type T and has the instance i_Instance
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_Instance"></param>
    /// <returns></returns>
    bool Drop<T>(T i_Instance);
    #endregion
    /// <summary>
    /// Checks if there is atleast one implementation of TInterface
    /// </summary>
    /// <returns></returns>
    bool HasImplementationOf(Type i_Type);

    /// <summary>
    /// Enables plugins
    /// </summary>
    void EnablePlugins();

    /// <summary>
    /// Sets the logger the container should use
    /// </summary>
    /// <param name="i_Logger"></param>
    void SetLogger(ILogger i_Logger);

    /// <summary>
    /// Returns a services provider for this container
    /// </summary>
    /// <returns></returns>
    public IServiceProvider ToServiceProvider(IServiceCollection i_Services);

    /// <summary>
    /// Initialize the container with t_Collection
    /// </summary>
    /// <param name="t_Collection"></param>
    public void Initialize(NcServiceCollection t_Collection);
  }
}
