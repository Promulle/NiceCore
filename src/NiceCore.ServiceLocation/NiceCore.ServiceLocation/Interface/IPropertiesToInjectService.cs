﻿using System.Collections.Generic;
using System.Reflection;

namespace NiceCore.ServiceLocation.Interface
{
  /// <summary>
  /// Service for getting properties to be injected
  /// </summary>
  public interface IPropertiesToInjectService
  {
    /// <summary>
    /// Gets all properties of the type of i_Instance that match/are less than i_MaxVisbility
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <param name="i_MaxVisibility"></param>
    /// <returns>List of property infos that can be injected</returns>
    IEnumerable<PropertyInfo> GetPropertiesToInject(object i_Instance, MethodVisibility i_MaxVisibility);
  }
}