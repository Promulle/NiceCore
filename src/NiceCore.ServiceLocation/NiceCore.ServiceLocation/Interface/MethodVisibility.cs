﻿namespace NiceCore.ServiceLocation.Interface
{
  /// <summary>
  /// Visibility of a method (used for property getters/setters)
  /// </summary>
  public enum MethodVisibility
  {
    /// <summary>
    /// PUBLIC
    /// </summary>
    Public = 0,

    /// <summary>
    /// INTERNAL/FRIEND
    /// </summary>
    InternalFriend = 1,

    /// <summary>
    /// PROTECTED
    /// </summary>
    Protected = 2,

    /// <summary>
    /// PROTECTED INTERNAL/FRIEND
    /// </summary>
    InternalProtected = 3,

    /// <summary>
    /// PRIVATE
    /// </summary>
    Private = 4,

  }
}
