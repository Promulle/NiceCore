// Copyright 2021 Sycorax Systemhaus GmbH

namespace NiceCore.ServiceLocation.Interface
{
  /// <summary>
  /// Interface for classes that have a ServiceContainer
  /// </summary>
  public interface IHasServiceContainer
  {
    /// <summary>
    /// Service Container
    /// </summary>
    IServiceContainer ServiceContainer { get; }
  }
}
