﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NiceCore.ServiceLocation
{
  /// <summary>
  /// Interface for classes using the service container
  /// </summary>
  public interface INeedsInjection
  {
    /// <summary>
    /// Container
    /// </summary>
    IServiceContainer Container { get; set; }
  }
}
