namespace NiceCore.ServiceLocation.Constants
{
  /// <summary>
  /// Service Registration Constants
  /// </summary>
  public static class ServiceRegistrationConstants
  {
    /// <summary>
    /// Default Lifestyle for service Registrations
    /// </summary>
    public static readonly LifeStyles DEFAULT_LIFESTYLE = LifeStyles.Transient;
  }
}