﻿namespace NiceCore.ServiceLocation.Lifestyles
{
  /// <summary>
  /// Interface that can be implemented to apply custom lifestyles
  /// </summary>
  public interface ICustomLifestyle
  {
    /// <summary>
    /// Name of custom lifestyle
    /// </summary>
    string Name { get; }

    /// <summary>
    /// Applies this lifestyle to i_Registration
    /// </summary>
    /// <param name="i_Registration"></param>
    /// <returns>to support fluent apis, this might return the changed registration</returns>
    object ApplyLifestyle(object i_Registration);
  }
}
