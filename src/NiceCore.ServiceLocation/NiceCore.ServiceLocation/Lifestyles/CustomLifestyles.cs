﻿namespace NiceCore.ServiceLocation.Lifestyles
{
  /// <summary>
  /// Constant class for custom lifestyles
  /// </summary>
  public static class CustomLifestyles
  {
    /// <summary>
    /// Scope used by AddScoped in ASP.NET Core
    /// </summary>
    public const string MS_SCOPED = "MsScopedLifestyle";
  }
}
