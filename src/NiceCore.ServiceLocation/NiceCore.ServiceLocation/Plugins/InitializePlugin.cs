﻿using NiceCore.Base;
using System;

namespace NiceCore.ServiceLocation.Plugins
{
  /// <summary>
  /// Plugin for initialize of Instances
  /// </summary>
  public class InitializePlugin : BasePlugin
  {
    /// <summary>
    /// return true
    /// </summary>
    /// <param name="i_Container"></param>
    /// <returns></returns>
    protected override bool InternalEnablePlugin(IServiceContainer i_Container)
    {
      return true;
    }

    /// <summary>
    /// Do nothing
    /// </summary>
    /// <param name="t_Instance"></param>
    protected override void InternalInstanceReleased(object t_Instance)
    {
    }

    /// <summary>
    /// Initialize plugin if instance is Initializable
    /// </summary>
    /// <param name="t_Instance"></param>
    /// <param name="i_InterfaceType"></param>
    protected override void InternalInstanceResolved(object t_Instance, Type i_InterfaceType)
    {
      if (HasAttribute<InitializeOnResolveAttribute>(t_Instance.GetType()) && t_Instance is IInitializable)
      {
        (t_Instance as IInitializable)?.Initialize();
      }
    }
  }
}
