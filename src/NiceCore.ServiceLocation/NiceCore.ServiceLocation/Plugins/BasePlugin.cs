﻿using System;
using System.Reflection;

namespace NiceCore.ServiceLocation.Plugins
{
  /// <summary>
  /// Base impl for IDependencyInjectionLifecyclePlugin
  /// </summary>
  public abstract class BasePlugin : IDependencyInjectionLifecyclePlugin
  {
    /// <summary>
    /// Whether the plugin is enabled or not
    /// </summary>
    public bool IsEnabled { get; set; }

    /// <summary>
    /// Outward facing impl that checks whether IsEnabled is true and then runs InternalInstanceReleased
    /// </summary>
    /// <param name="t_Instance"></param>
    public void InstanceReleased(object t_Instance)
    {
      if (IsEnabled)
      {
        InternalInstanceReleased(t_Instance);
      }
    }

    /// <summary>
    /// Outward facing impl that checks whether IsEnabled is true and then runs InternalInstanceResolved
    /// </summary>
    /// <param name="t_Instance"></param>
    /// <param name="i_InterfaceType"></param>
    public void InstanceResolved(object t_Instance, Type i_InterfaceType)
    {
      if (IsEnabled)
      {
        InternalInstanceResolved(t_Instance, i_InterfaceType);
      }
    }

    /// <summary>
    /// Internal released method that should be overriden
    /// </summary>
    /// <param name="t_Instance"></param>
    protected abstract void InternalInstanceReleased(object t_Instance);

    /// <summary>
    /// Internal resolved method that should be overriden
    /// </summary>
    /// <param name="t_Instance"></param>
    /// <param name="i_InterfaceType"></param>
    protected abstract void InternalInstanceResolved(object t_Instance, Type i_InterfaceType);

    /// <summary>
    /// Checks whehther i_InstanceType has an attribute that is of type T or inherits from it
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_InstanceType"></param>
    /// <returns></returns>
    protected static bool HasAttribute<T>(Type i_InstanceType) where T : Attribute
    {
      foreach (var attr in i_InstanceType.GetCustomAttributes())
      {
        if (attr is T)
        {
          return true;
        }
      }
      return false;
    }

    /// <summary>
    /// Enable Plugin
    /// </summary>
    /// <param name="i_Container"></param>
    public void EnablePlugin(IServiceContainer i_Container)
    {
      IsEnabled = InternalEnablePlugin(i_Container);
    }

    /// <summary>
    /// prepares plugin to be enabled
    /// </summary>
    /// <param name="i_Container"></param>
    /// <returns>Whether the plugin should be enabled or not</returns>
    protected abstract bool InternalEnablePlugin(IServiceContainer i_Container);
  }
}
