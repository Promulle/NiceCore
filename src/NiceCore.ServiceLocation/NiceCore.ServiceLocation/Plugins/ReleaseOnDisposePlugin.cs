﻿using NiceCore.Base;
using System;

namespace NiceCore.ServiceLocation.Plugins
{
  /// <summary>
  /// Plugin for releasing the instance if it is disposed
  /// </summary>
  public class ReleaseOnDisposePlugin : BasePlugin
  {
    private Action<object, IServiceContainer> m_ReleaseAction;
    private IServiceContainer m_ServiceContainer;

    /// <summary>
    /// Checks whether i_Container is null and sets up needed release function
    /// </summary>
    /// <param name="i_Container"></param>
    /// <returns></returns>
    protected override bool InternalEnablePlugin(IServiceContainer i_Container)
    {
      m_ServiceContainer = i_Container;
      m_ReleaseAction = static (r, container) => container.Release(r);
      return i_Container != null;
    }

    /// <summary>
    /// Do nothing
    /// </summary>
    /// <param name="t_Instance"></param>
    protected override void InternalInstanceReleased(object t_Instance)
    {
    }

    /// <summary>
    /// Hookup OnDispose event if t_Instance is IBaseDisposable
    /// </summary>
    /// <param name="t_Instance"></param>
    /// <param name="i_InterfaceType"></param>
    protected override void InternalInstanceResolved(object t_Instance, Type i_InterfaceType)
    {
      if (t_Instance is IBaseDisposable casted)
      {
        casted.OnDisposeStart += OnInstanceBeginsDispose;
      }
    }

    /// <summary>
    /// Handler for dispose, public so releaseCycle will still work
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void OnInstanceBeginsDispose(object sender, EventArgs e)
    {
      if (sender is IBaseDisposable casted)
      {
        casted.OnDisposeStart -= OnInstanceBeginsDispose;
        m_ReleaseAction(sender, m_ServiceContainer);
      }
    }
  }
}
