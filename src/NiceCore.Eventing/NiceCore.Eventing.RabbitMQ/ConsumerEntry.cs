﻿using NiceCore.Base;
using NiceCore.Eventing.RabbitMQ.Channels;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace NiceCore.Eventing.RabbitMQ
{
  /// <summary>
  /// A single registered consumer and it's connection and channel
  /// </summary>
  public class ConsumerEntry : BaseDisposable
  {
    /// <summary>
    /// Connection used for consuming
    /// </summary>
    public IConnection Connection { get; init; }

    /// <summary>
    /// Channel used for consuming
    /// </summary>
    public TopicBasedChannel Channel { get; init; }

    /// <summary>
    /// Consumer that is consuming messages
    /// </summary>
    public AsyncEventingBasicConsumer Consumer { get; init; }

    /// <summary>
    /// Dispose this consumer entry
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    protected override void Dispose(bool i_IsDisposing)
    {
      Channel.Dispose();
      base.Dispose(i_IsDisposing);
    }
  }
}
