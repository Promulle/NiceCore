﻿namespace NiceCore.Eventing.RabbitMQ
{
  /// <summary>
  /// Constanst for NiceCore.Eventing.RabbitMQ
  /// </summary>
  public static class NiceCoreEventingRabbitMQConstants
  {
    /// <summary>
    /// Name of assembly
    /// </summary>
    public static readonly string ASSEMBLY_NAME = "NiceCore.Eventing.RabbitMQ";
  }
}
