﻿using NiceCore.Extensions;
using System;

namespace NiceCore.Eventing.RabbitMQ.Exceptions
{
  /// <summary>
  /// Exception that is thrown if something happens during creation of the connection factory
  /// </summary>
#pragma warning disable RCS1194 // Implement exception constructors.
  public class ConnectionFactoryCreationException : Exception
#pragma warning restore RCS1194 // Implement exception constructors.
  {
    /// <summary>
    /// Constructor filling inner exception
    /// </summary>
    /// <param name="i_Exception"></param>
    /// <param name="i_Configuration"></param>
    public ConnectionFactoryCreationException(Exception i_Exception, IHandlerConfig i_Configuration) : base(BuildMessage(i_Exception, i_Configuration))
    {
    }

    private static string BuildMessage(Exception i_Exception, IHandlerConfig i_Configuration)
    {
      return $"Something went wrong during creation of the connection Factory with Config: {i_Configuration.ToJsonString()}. Exception: {i_Exception}";
    }
  }
}
