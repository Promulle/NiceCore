﻿using NiceCore.Eventing.Impl;
using NiceCore.Eventing.RabbitMQ.Exceptions;
using NiceCore.Extensions;
using NiceCore.ServiceLocation;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Diagnostics;
using NiceCore.Eventing.RabbitMQ.Channels;
using NiceCore.Logging.Extensions;

namespace NiceCore.Eventing.RabbitMQ
{
  /// <summary>
  /// Eventing Handler implementation for rabbitMQ
  /// </summary>
  [InitializeOnResolve]
  [Register(InterfaceType = typeof(IEventingHandler), LifeStyle = LifeStyles.Singleton)]
  public class RabbitMQEventingHandler : BaseEventingHandler<RabbitMQHandlerConfig>
  {
    private const string HANDLER_NAME = "RabbitMQ";

    private readonly Dictionary<string, ConsumerEntry> m_Consumers = new();
    private readonly object m_LockObject = new();
    private ConnectionFactory m_Factory;
    private IConnection m_Connection;
    private TopicBasedChannelFactory m_ChannelFactory;
    private string m_ClientIdentifier;

    /// <summary>
    /// Handler Name
    /// </summary>
    public override string HandlerName { get; } = HANDLER_NAME;

    /// <summary>
    /// Client Identifier Provider
    /// </summary>
    public IClientIdentifierProvider ClientIdentifierProvider { get; set; }
    
    /// <summary>
    /// Actual Logger
    /// </summary>
    public ILogger<RabbitMQEventingHandler> ActualLogger { get; set; } = DiagnosticsCollector.Instance.GetLogger<RabbitMQEventingHandler>();

    /// <summary>
    /// Logger
    /// </summary>
    public override ILogger Logger
    {
      get { return ActualLogger; }
    }

    /// <summary>
    /// Internal Initialize
    /// </summary>
    protected override void InternalInitialize()
    {
      base.InternalInitialize();
      if (ClientIdentifierProvider != null)
      {
        m_ClientIdentifier = ClientIdentifierProvider.GetClientIdentifier();
      }
    }

    /// <summary>
    /// Send message to rabbitmq server
    /// </summary>
    /// <param name="i_Topic"></param>
    /// <param name="i_Data"></param>
    /// <returns></returns>
    protected override Task SendInternal(string i_Topic, byte[] i_Data)
    {
      lock (m_LockObject)
      {
        if (InitializeConnectionAndFactory())
        {
          var cfg = CheckSuppliedConfig();
          var channel = m_ChannelFactory.GetChannelForTopic(i_Topic);
          Logger.Information("Sending new message to rabbitmq server.");
          channel.Publish(i_Data, i_Topic, m_ClientIdentifier, cfg);
        }
      }

      return Task.CompletedTask;
    }

    /// <summary>
    /// subscribe to rabbitmq message
    /// </summary>
    /// <param name="i_Topic"></param>
    /// <param name="i_MessageType"></param>
    protected override void SubscribeInternal(string i_Topic, Type i_MessageType)
    {
      lock (m_LockObject)
      {
        if (IsConfigCorrect(HandlerConfiguration) && InitializeConnectionAndFactory())
        {
          MakeSureConsumerExistsFor(i_Topic);
        }
      }
    }

    private void MakeSureConsumerExistsFor(string i_Topic)
    {
      if (!m_Consumers.ContainsKey(i_Topic))
      {
        Logger.Debug($"No consumer for topic {i_Topic} registered. Creating new one...");
        AddConsumer(i_Topic);
      }
    }

    private void AddConsumer(string i_Topic)
    {
      var cfg = CheckSuppliedConfig();
      Logger.Information("Creating Consumer.");
      var channel = m_ChannelFactory.GetChannelForTopic(i_Topic);
      Logger.Information($"Declaring exchange for topic {i_Topic} as direct exchange.");
      var queueName = channel.SetupQueueAndExchangeIfNeeded(m_ClientIdentifier, cfg);
      var consumer = new AsyncEventingBasicConsumer(channel.InternalChannel);
      async Task ReceivedAction(object sender, BasicDeliverEventArgs args)
      {
        Logger.Information("Received new message from rabbitmq server.");
        var result = await CreateMessageFor(args.Body, i_Topic).ConfigureAwait(false);
        await RaiseMessageReceived(result.Message, result.Parameters).ConfigureAwait(false);
        Logger.Debug($"Message was processed by all expected functions. Sending a Ack to rabbitmq. Tag: {args.DeliveryTag}");
        channel.SingleAck(args.DeliveryTag);
      }

      EventHandler onDisposing = (sender, args) => { consumer.Received -= ReceivedAction; };
      consumer.Received += ReceivedAction;
      var entry = new ConsumerEntry()
      {
        Connection = m_Connection,
        Channel = channel,
        Consumer = consumer
      };
      entry.OnDisposeStart += onDisposing;
      m_Consumers[i_Topic] = entry;
      channel.BasicConsume(queueName, entry.Consumer, cfg);
      Logger.Debug($"Consumer for Topic {i_Topic} successfully created.");
    }

    private bool InitializeConnectionAndFactory()
    {
      return InitializeConnectionFactoryIfNeeded() && InitializeConnectionIfNeeded() && InitializeChannelFactoryIfNeeded();
    }

    private bool InitializeConnectionFactoryIfNeeded()
    {
      if (m_Factory != null)
      {
        return true;
      }

      var cfg = CheckSuppliedConfig();
      if (IsConfigCorrect(cfg))
      {
        Logger.Information("Config verified. Creating Connection Factory...");
        try
        {
          m_Factory = new ConnectionFactory()
          {
            HostName = cfg.HostName,
            VirtualHost = cfg.VirtualHost,
            Password = cfg.Password,
            UserName = cfg.UserName,
            DispatchConsumersAsync = true,
            AutomaticRecoveryEnabled = true,
            NetworkRecoveryInterval = TimeSpan.FromSeconds(10),
            RequestedHeartbeat = TimeSpan.FromSeconds(cfg.HeartbeatInterval),
            Ssl = new()
            {
              Enabled = false
            }
          };
        }
        catch (Exception e)
        {
          throw new ConnectionFactoryCreationException(e, cfg);
        }

        Logger.Information("Connection factory successfully created.");
        return true;
      }

      Logger.Warning($"Could not setup connection factory. Configuration is not filled correctly. Expected the instance to be not null, HostName, Password,Username to be filled. Actual: {HandlerConfiguration.ToJsonString()}");
      return false;
    }

    private bool InitializeChannelFactoryIfNeeded()
    {
      if (m_ChannelFactory == null)
      {
        m_ChannelFactory = new(m_Connection, Logger);
      }
      return true;
    }

    private bool InitializeConnectionIfNeeded()
    {
      if (m_Connection?.IsOpen == true)
      {
        return true;
      }

      if (m_Connection == null)
      {
        m_Connection = CreateConnection();
        return true;
      }

      if (!m_Connection.IsOpen)
      {
        try
        {
          m_Connection.Dispose();
          m_Connection = null;
        }
        catch (Exception ex)
        {
          Logger.Error(ex, "Closed Connection could not be disposed. Creating new one anyway. This may lead to memory problems.");
        }

        m_Connection = CreateConnection();
        return true;
      }

      return false;
    }

    private IConnection CreateConnection()
    {
      Logger.Information("Creating new connection...");
      try
      {
        return m_Factory.CreateConnection();
      }
      catch (Exception ex)
      {
        Logger.Error(ex, "Could not create connection. See exception for details.");
        throw;
      }
    }

    /// <summary>
    /// is Config Correct
    /// </summary>
    /// <param name="i_HandlerConfig"></param>
    /// <returns></returns>
    protected override bool IsConfigCorrect(IHandlerConfig i_HandlerConfig)
    {
      return i_HandlerConfig != null &&
             !string.IsNullOrEmpty(i_HandlerConfig.HostName) &&
             !string.IsNullOrEmpty(i_HandlerConfig.Password) &&
             !string.IsNullOrEmpty(i_HandlerConfig.UserName) && 
             i_HandlerConfig.IsEnabled;
    }

    /// <summary>
    /// Unsubscribe the consumer that is registered for i_Topic
    /// </summary>
    /// <param name="i_Topic"></param>
    public override void Unsubscribe(string i_Topic)
    {
      lock (m_LockObject)
      {
        if (m_Consumers != null && m_Consumers.TryGetValue(i_Topic, out var consumer))
        {
          m_Consumers.Remove(i_Topic);
          m_ChannelFactory.RemoveChannelByTopic(i_Topic);
          consumer.Dispose();
        }
      }
    }

    /// <summary>
    /// Dispose all available consumers and clear the dictionary after
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    protected override void Dispose(bool i_IsDisposing)
    {
      lock (m_LockObject)
      {
        foreach (var entry in m_Consumers.Values)
        {
          entry.Dispose();
        }

        m_Consumers.Clear();
        m_Connection.Dispose();
        m_Connection = null;
      }

      base.Dispose(i_IsDisposing);
    }
  }
}