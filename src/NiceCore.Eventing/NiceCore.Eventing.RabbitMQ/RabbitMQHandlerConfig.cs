﻿using RabbitMQ.Client;

namespace NiceCore.Eventing.RabbitMQ
{
  /// <summary>
  /// Config for RabbitMQ handler -> extends default
  /// </summary>
  public class RabbitMQHandlerConfig : DefaultHandlerConfig
  {
    /// <summary>
    /// Virtual Host
    /// </summary>
    public string VirtualHost { get; init; }

    /// <summary>
    /// Sets autoAck flag when creating consumers
    /// </summary>
    public bool AutoAckConsuming { get; init; }

    /// <summary>
    /// Heartbeat interval in seconds
    /// </summary>
    public uint HeartbeatInterval { get; init; }

    /// <summary>
    /// If messages and queues should persist after restarting the server/if the messages should be persisted
    /// </summary>
    public bool PersistentMessageHandling { get; init; }

    /// <summary>
    /// Exchange Type
    /// </summary>
    public string UsedExchangeType { get; init; } = ExchangeType.Direct;
  }
}
