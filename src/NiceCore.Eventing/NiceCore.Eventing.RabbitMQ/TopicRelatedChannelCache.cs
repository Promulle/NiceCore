﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;

namespace NiceCore.Eventing.RabbitMQ
{
  /// <summary>
  /// Cache that caches channels per topic so that sender/consumer use the same channel instance
  /// </summary>
  public class TopicRelatedChannelCache
  {
    private readonly object m_LockObject = new();
    private readonly Dictionary<string, IModel> m_Channels = new();

    /// <summary>
    /// Function that returns the channel that has been cached for i_Topic, uses i_ModelCreationFunc if none was cached for i_Topic
    /// </summary>
    /// <param name="i_Topic"></param>
    /// <param name="i_ModelCreationFunc"></param>
    /// <returns></returns>
    public IModel GetChannelForTopic(string i_Topic, Func<IModel> i_ModelCreationFunc)
    {
      lock (m_LockObject)
      {
        if (m_Channels.TryGetValue(i_Topic, out var channel))
        {
          return channel;
        }
        var newChannel = i_ModelCreationFunc.Invoke();
        m_Channels[i_Topic] = newChannel;
        return newChannel;
      }
    }
  }
}
