using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Microsoft.VisualBasic;
using NiceCore.Logging.Extensions;
using RabbitMQ.Client;

namespace NiceCore.Eventing.RabbitMQ.Channels.Utils
{
  /// <summary>
  /// Queue Setup Utils
  /// </summary>
  internal static class QueueSetupUtils
  {
    private static readonly Dictionary<string, object> EMPTY_DICT = new();

    /// <summary>
    /// Setup a queue and exchange
    /// </summary>
    /// <param name="i_Channel"></param>
    /// <param name="i_Topic"></param>
    /// <param name="i_ClientIdentifier"></param>
    /// <param name="i_Config"></param>
    /// <param name="i_Logger"></param>
    /// <returns></returns>
    internal static string SetupQueueAndExchangeIfNeeded(IModel i_Channel, string i_Topic, string i_ClientIdentifier, RabbitMQHandlerConfig i_Config, ILogger i_Logger)
    {
      i_Channel.ExchangeDeclare(i_Topic, ExchangeType.Fanout, i_Config.PersistentMessageHandling);
      var queueName = CreateQueueIfNeeded(i_Topic, i_ClientIdentifier, i_Channel, i_Config, i_Logger);
      i_Channel.QueueBind(queueName, i_Topic, string.Empty, EMPTY_DICT);
      return queueName;
    }

    private static string CreateQueueIfNeeded(string i_Topic, string i_ClientIdentifier, IModel t_Channel, RabbitMQHandlerConfig i_Config, ILogger i_Logger)
    {
      try
      {
        var res = t_Channel.QueueDeclare(GenerateQueueName(i_Topic, i_ClientIdentifier), i_Config.PersistentMessageHandling, false, false, null);
        if (res == null)
        {
          throw new InvalidOperationException("QueueDeclare did not return OK");
        }
        return res.QueueName;
      }
      catch (Exception ex)
      {
        i_Logger.Error(ex, $"There was an error declaring the queue for {i_Topic}. (durable: {i_Config.PersistentMessageHandling})");
        throw;
      }
    }

    private static string GenerateQueueName(string i_Topic, string i_ClientIdentifier)
    {
      return $"{i_Topic}_{i_ClientIdentifier}";
    }
  }
}