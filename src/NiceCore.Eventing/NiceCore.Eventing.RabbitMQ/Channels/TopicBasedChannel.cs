using System;
using Microsoft.Extensions.Logging;
using NiceCore.Base;
using NiceCore.Eventing.RabbitMQ.Channels.Utils;
using NiceCore.Logging.Extensions;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace NiceCore.Eventing.RabbitMQ.Channels
{
  /// <summary>
  /// Topic Based Channel
  /// </summary>
  public class TopicBasedChannel : BaseDisposable
  {
    private readonly object m_Lock = new();
    private ILogger m_Logger;
    
    /// <summary>
    /// Internal Channel
    /// </summary>
    public IModel InternalChannel { get; init; }
    
    /// <summary>
    /// Topic of the channel
    /// </summary>
    public string Topic { get; init; }

    /// <summary>
    /// Publish a message on the channel
    /// </summary>
    public void Publish(byte[] i_Data, string i_Exchange, string i_ClientIdentifier, RabbitMQHandlerConfig i_Config)
    {
      lock (m_Lock)
      {
        QueueSetupUtils.SetupQueueAndExchangeIfNeeded(InternalChannel, Topic, i_ClientIdentifier, i_Config, m_Logger);
        var props = InternalChannel.CreateBasicProperties();
        props.Persistent = i_Config.PersistentMessageHandling;
        InternalChannel.BasicPublish(i_Exchange, string.Empty, props, i_Data);  
      }
    }

    /// <summary>
    /// Ack on the channel
    /// </summary>
    public void SingleAck(ulong i_DeliveryTag)
    {
      lock (m_Lock)
      {
        InternalChannel.BasicAck(i_DeliveryTag, false);
      }
    }

    /// <summary>
    /// Basic Consume
    /// </summary>
    /// <param name="i_QueueName"></param>
    /// <param name="i_Consumer"></param>
    /// <param name="i_Config"></param>
    public void BasicConsume(string i_QueueName, AsyncEventingBasicConsumer i_Consumer, RabbitMQHandlerConfig i_Config)
    {
      lock (m_Lock)
      {
        var consumerTag = InternalChannel.BasicConsume(i_QueueName, i_Config.AutoAckConsuming, string.Empty, true, false, null, i_Consumer);
        m_Logger.Debug($"Opened consumer {consumerTag} for queue {i_QueueName}");
      }
    }

    /// <summary>
    /// Setup Queue And Exchange If Needed
    /// </summary>
    /// <param name="i_ClientIdentifier"></param>
    /// <param name="i_Config"></param>
    /// <returns></returns>
    public string SetupQueueAndExchangeIfNeeded(string i_ClientIdentifier, RabbitMQHandlerConfig i_Config)
    {
      lock (m_Lock)
      {
        return QueueSetupUtils.SetupQueueAndExchangeIfNeeded(InternalChannel, Topic, i_ClientIdentifier, i_Config, m_Logger);
      }
    }

    private void HandleChannelError(object i_Sender, CallbackExceptionEventArgs i_Args)
    {
      m_Logger.Error(i_Args.Exception, $"An Error occurred during rabbitmq channel callback: {i_Args.Exception}");
    }
    

    /// <summary>
    /// Creates a new Topic Based Channel
    /// </summary>
    /// <param name="i_Topic"></param>
    /// <param name="i_Model"></param>
    /// <param name="i_Logger"></param>
    /// <returns></returns>
    public static TopicBasedChannel CreateNew(string i_Topic, IModel i_Model, ILogger i_Logger)
    {
      var topicChannel = new TopicBasedChannel()
      {
        InternalChannel = i_Model,
        Topic = i_Topic,
        m_Logger = i_Logger
      };
      i_Model.CallbackException += topicChannel.HandleChannelError;
      return topicChannel;
    }

    /// <summary>
    /// Dispose
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    protected override void Dispose(bool i_IsDisposing)
    {
      InternalChannel.CallbackException -= HandleChannelError;
      if (i_IsDisposing)
      {
        InternalChannel.Dispose();
      }
    }
  }
}