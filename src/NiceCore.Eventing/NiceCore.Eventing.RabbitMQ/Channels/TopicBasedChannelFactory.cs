using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;

namespace NiceCore.Eventing.RabbitMQ.Channels
{
  /// <summary>
  /// Factory that returns Topic Channels for topics
  /// </summary>
  public class TopicBasedChannelFactory
  {
    private readonly object m_LockObject = new();
    private readonly IConnection m_Connection;
    private readonly ILogger m_Logger;
    private readonly Dictionary<string, TopicBasedChannel> m_Channels = new();

    /// <summary>
    /// Ctor
    /// </summary>
    public TopicBasedChannelFactory(IConnection i_Connection, ILogger i_Logger)
    {
      m_Connection = i_Connection;
      m_Logger = i_Logger;
    }

    /// <summary>
    /// Remove Channel By Topic 
    /// </summary>
    /// <param name="i_Topic"></param>
    public void RemoveChannelByTopic(string i_Topic)
    {
      lock (m_LockObject)
      {
        m_Channels.Remove(i_Topic);
      }
    }

    /// <summary>
    /// Function that returns the channel that has been cached for i_Topic, uses i_ModelCreationFunc if none was cached for i_Topic
    /// </summary>
    /// <param name="i_Topic"></param>
    /// <returns></returns>
    public TopicBasedChannel GetChannelForTopic(string i_Topic)
    {
      lock (m_LockObject)
      {
        if (m_Channels.TryGetValue(i_Topic, out var channel))
        {
          return channel;
        }

        var newChannel = m_Connection.CreateModel();
        var topicChannel = TopicBasedChannel.CreateNew(i_Topic, newChannel, m_Logger);
        m_Channels[i_Topic] = topicChannel;
        return topicChannel;
      }
    }
  }
}