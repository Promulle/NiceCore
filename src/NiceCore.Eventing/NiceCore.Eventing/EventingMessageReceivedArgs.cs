using NiceCore.Eventing.Messages;
using NiceCore.Eventing.Messaging.Parameters;

namespace NiceCore.Eventing
{
  /// <summary>
  /// Args for the message received event
  /// </summary>
  public class EventingMessageReceivedArgs
  {
    /// <summary>
    /// Message
    /// </summary>
    public IObjectMessage Message { get; set; }
    
    /// <summary>
    /// parameters
    /// </summary>
    public IMessageSendingParameters Parameters { get; set; }
  }
}