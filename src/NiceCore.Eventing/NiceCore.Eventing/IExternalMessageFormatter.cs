using System;
using System.Threading.Tasks;
using NiceCore.Eventing.Messages;
using NiceCore.Eventing.Messaging.Parameters;

namespace NiceCore.Eventing
{
  /// <summary>
  /// External Message Formatter
  /// </summary>
  public interface IExternalMessageFormatter
  {
    /// <summary>
    /// To External Message
    /// </summary>
    /// <param name="i_Message"></param>
    /// <param name="i_Parameters"></param>
    /// <returns></returns>
    ValueTask<byte[]> ToExternalData(IObjectMessage i_Message, IMessageSendingParameters i_Parameters);
    
    /// <summary>
    /// To Value
    /// </summary>
    /// <returns></returns>
    ValueTask<(IMessageObjectValue Value, IMessageSendingParameters Parameters)> FromExternalData(ReadOnlyMemory<byte> i_ExternalMessageData, IObjectMessage i_Message);
  }
}