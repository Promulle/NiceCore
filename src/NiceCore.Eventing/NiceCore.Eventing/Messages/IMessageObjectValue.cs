﻿using System;

namespace NiceCore.Eventing.Messages
{
  /// <summary>
  /// Non generic message value interface
  /// </summary>
  public interface IMessageObjectValue
  {
    /// <summary>
    /// Guid Identifier
    /// </summary>
     Guid Identifier { get; set; }

    /// <summary>
    /// set Client Identifier
    /// </summary>
    string ClientIdentifier { get; set; }

    /// <summary>
    /// Sets the value
    /// </summary>
    /// <param name="i_Value"></param>
    void SetValue(object i_Value);

    /// <summary>
    /// Returns the value of the message
    /// </summary>
    /// <returns></returns>
    object GetValue();
  }
}
