using NiceCore.Eventing.Messaging.Parameters;
using NiceCore.Eventing.Messaging.Parameters.Impl;

namespace NiceCore.Eventing.Messages
{
  /// <summary>
  /// Message that was received from an eventing handler or should be sent by an eventing handler
  /// </summary>
  public class ExternalMessage
  {
    /// <summary>
    /// Message Object Value
    /// </summary>
    public IMessageObjectValue MessageObjectValue { get; set; }
    
    /// <summary>
    /// Message Parameters
    /// </summary>
    public IMessageSendingParameters MessageParameters { get; set; }
  }
}