﻿using System;
using System.Text;
using NiceCore.Serialization;

namespace NiceCore.Eventing.Messages.RemoteSerialization.Impl
{
  /// <summary>
  /// Base Json Message Value Remote Serializer
  /// </summary>
  public abstract class BaseJsonMessageValueRemoteSerializer : IMessageValueRemoteSerializer
  {
    private readonly Encoding m_Encoding;

    /// <summary>
    /// Key
    /// </summary>
    public abstract string Key { get; }
    
    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_Encoding"></param>
    protected BaseJsonMessageValueRemoteSerializer(Encoding i_Encoding)
    {
      m_Encoding = i_Encoding;
    }
    
    /// <summary>
    /// To Bytes
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public byte[] ToBytes(IMessageObjectValue i_Value)
    {
      return m_Encoding.GetBytes(JSONSerialization.Serialize(i_Value, i_Value.GetType()));
    }

    /// <summary>
    /// To Object
    /// </summary>
    /// <param name="i_Bytes"></param>
    /// <param name="i_Type"></param>
    /// <returns></returns>
    public IMessageObjectValue ToObject(byte[] i_Bytes, Type i_Type)
    {
      var res = JSONSerialization.Deserialize(m_Encoding.GetString(i_Bytes), i_Type);
      if (res is IMessageObjectValue val)
      {
        return val;
      }

      return null;
    }
  }
}