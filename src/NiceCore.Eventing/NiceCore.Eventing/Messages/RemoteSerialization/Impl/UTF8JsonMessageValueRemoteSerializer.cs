﻿using System.Text;
using NiceCore.ServiceLocation;

namespace NiceCore.Eventing.Messages.RemoteSerialization.Impl
{
  /// <summary>
  /// UTF8 Json Message Value Remote Serializer
  /// </summary>
  [Register(InterfaceType = typeof(IMessageValueRemoteSerializer))]
  public class UTF8JsonMessageValueRemoteSerializer : BaseJsonMessageValueRemoteSerializer
  {
    /// <summary>
    /// key
    /// </summary>
    public const string KEY = "uft8-default";
    
    /// <summary>
    /// key
    /// </summary>
    public override string Key { get; } = KEY;
      
    /// <summary>
    /// Ctor
    /// </summary>
    public UTF8JsonMessageValueRemoteSerializer() : base(Encoding.UTF8)
    {
      
    }
  }
}