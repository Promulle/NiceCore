using System.Collections.Generic;
using NiceCore.Base.Services;
using NiceCore.ServiceLocation;

namespace NiceCore.Eventing.Messages.RemoteSerialization.Impl
{
  /// <summary>
  /// message Value Remote Serializer
  /// </summary>
  [InitializeOnResolve]
  [Register(InterfaceType = typeof(IMessageValueRemoteSerializerService), LifeStyle = LifeStyles.Singleton)]
  public class MessageValueRemoteSerializerService : BaseService, IMessageValueRemoteSerializerService
  {
    private readonly Dictionary<string, IMessageValueRemoteSerializer> m_SerializersByKey = new();
    
    /// <summary>
    /// Message Value Remote Serializers
    /// </summary>
    public IList<IMessageValueRemoteSerializer> MessageValueRemoteSerializers { get; set; }

    /// <summary>
    /// Internal Initialize
    /// </summary>
    protected override void InternalInitialize()
    {
      if (MessageValueRemoteSerializers != null)
      {
        foreach (var messageValueRemoteSerializer in MessageValueRemoteSerializers)
        {
          m_SerializersByKey[messageValueRemoteSerializer.Key] = messageValueRemoteSerializer;
        }
      }
    }

    /// <summary>
    /// Gets the correct serializer for i_Key
    /// </summary>
    /// <param name="i_Key"></param>
    /// <returns></returns>
    public IMessageValueRemoteSerializer GetSerializerForKey(string i_Key)
    {
      if (i_Key != null && m_SerializersByKey.TryGetValue(i_Key, out var messageValueRemoteSerializer))
      {
        return messageValueRemoteSerializer;
      }

      if (m_SerializersByKey.TryGetValue(UTF8JsonMessageValueRemoteSerializer.KEY, out var defaultSerializer))
      {
        return defaultSerializer;
      }
      return null;
    }
  }
}