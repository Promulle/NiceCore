using System.Threading.Tasks;

namespace NiceCore.Eventing.Messages.RemoteSerialization
{
  /// <summary>
  /// MessageValue Remote Serializer Service
  /// </summary>
  public interface IMessageValueRemoteSerializerService
  {
    /// <summary>
    /// Get Serializer for Key
    /// </summary>
    /// <param name="i_Key"></param>
    /// <returns></returns>
    public IMessageValueRemoteSerializer GetSerializerForKey(string i_Key);
  }
}