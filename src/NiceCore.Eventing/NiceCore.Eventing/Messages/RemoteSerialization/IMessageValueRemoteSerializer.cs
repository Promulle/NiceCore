﻿using System;

namespace NiceCore.Eventing.Messages.RemoteSerialization
{
  /// <summary>
  /// Serializer that serializes message values for sending/receiving remote messages
  /// </summary>
  public interface IMessageValueRemoteSerializer
  {
    /// <summary>
    /// Key that identifies this serializer
    /// </summary>
    string Key { get; }
    
    /// <summary>
    /// Serializes a message value to a byte array
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    byte[] ToBytes(IMessageObjectValue i_Value);

    /// <summary>
    /// Creates a message object value from i_Bytes
    /// </summary>
    /// <param name="i_Bytes"></param>
    /// <param name="i_Type"></param>
    /// <returns></returns>
    IMessageObjectValue ToObject(byte[] i_Bytes, Type i_Type);
  }
}