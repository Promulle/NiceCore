﻿using System;

namespace NiceCore.Eventing.Messages
{
  /// <summary>
  /// Helper with nicer API to create message
  /// </summary>
  public static class MessageHelper
  {
    /// <summary>
    /// Creates a new Message of type TMsg with i_Value as value
    /// </summary>
    /// <typeparam name="TMsg"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public static TMsg Create<TMsg, TValue>(TValue i_Value) where TMsg : IMessage<TValue>
    {
      var msg = Activator.CreateInstance<TMsg>();
      msg.SetValue(new MessageValue<TValue>() { Value = i_Value });
      return msg;
    }
  }
}
