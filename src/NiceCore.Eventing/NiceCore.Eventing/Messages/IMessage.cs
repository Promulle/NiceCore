﻿using NiceCore.Base;
using System;

namespace NiceCore.Eventing.Messages
{
  /// <summary>
  /// Base interface for message
  /// </summary>
  public interface IMessage<T> : IObjectMessage
  {
    /// <summary>
    /// Value to be sent
    /// </summary>
    IMessageValue<T> Value { get; }

    /// <summary>
    /// Returns the actual value
    /// </summary>
    /// <returns></returns>
    Result<T> GetActualValue();
  }
}
