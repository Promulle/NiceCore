﻿using ProtoBuf;
using System;

namespace NiceCore.Eventing.Messages
{
  /// <summary>
  /// Value for messages
  /// </summary>
  /// <typeparam name="T"></typeparam>
  [ProtoContract]
  public class MessageValue<T> : IMessageValue<T>
  {
#pragma warning disable IDE0032 // needed for function -> disabled style info
    private T m_Value;
#pragma warning restore IDE0032 // needed for function -> disabled style info
    /// <summary>
    /// Identifier
    /// </summary>
    [ProtoMember(1)]
    public Guid Identifier { get; set; } = Guid.NewGuid();

    /// <summary>
    /// Value
    /// </summary>
    [ProtoMember(2)]
    public T Value
    {
      get { return m_Value; }
      set { m_Value = value; }
    }
    
    /// <summary>
    /// Identifier of client that sent the message
    /// </summary>
    [ProtoMember(3)]
    public string ClientIdentifier { get; set; }

    /// <summary>
    /// Returns the value
    /// </summary>
    /// <returns></returns>
    public object GetValue()
    {
      return Value;
    }

    /// <summary>
    /// Set Client Identifier
    /// </summary>
    /// <param name="i_Identifier"></param>
    public void SetClientIdentifier(string i_Identifier)
    {
      ClientIdentifier = i_Identifier;
    }

    /// <summary>
    /// Sets the value to i_Object if i_Object is T
    /// </summary>
    /// <param name="i_Object"></param>
    public void SetValue(object i_Object)
    {
      if (i_Object is T casted)
      {
        m_Value = casted;
      }
    }
  }
}
