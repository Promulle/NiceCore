﻿using System;

namespace NiceCore.Eventing.Messages
{
  /// <summary>
  /// Interface for messages without generic parameter
  /// </summary>
  public interface IObjectMessage
  {
    /// <summary>
    /// Expected message type. MUST BE Overriden if Value Property is not MessageValue of T,
    /// otherwise transporting messages to/from remote might fail
    /// </summary>
    Type ExpectedMessageValueType { get; }
    
    /// <summary>
    /// Message Topic
    /// </summary>
    string MessageTopic { get; }
    
    /// <summary>
    /// Whether a message should be processed in sync (true)
    /// or delegated to task.run (false)
    /// </summary>
    bool IsSync { get; }

    /// <summary>
    /// Origin of a message
    /// </summary>
    MessageOrigin Origin { get; set; }

    /// <summary>
    /// Set Client Identifier
    /// </summary>
    /// <param name="i_Identifier"></param>
    void SetClientIdentifier(string i_Identifier);
    
    /// <summary>
    /// Sets the value
    /// </summary>
    /// <param name="i_Value"></param>
    void SetValue(IMessageObjectValue i_Value);

    /// <summary>
    /// Returns the value of the message
    /// </summary>
    /// <returns></returns>
    IMessageObjectValue GetValue();

    /// <summary>
    /// Get client Identifier
    /// </summary>
    /// <returns></returns>
    string GetClientIdentifier();
  }
}
