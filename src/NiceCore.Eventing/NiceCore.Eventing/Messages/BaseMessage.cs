﻿using NiceCore.Base;
using NiceCore.Eventing.Exceptions;
using System;

namespace NiceCore.Eventing.Messages
{
  /// <summary>
  /// Base implementation for IMessage
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public abstract class BaseMessage<T> : IMessage<T>
  {
#pragma warning disable IDE0032 // needed for functions -> disabled style info
    private IMessageValue<T> m_Value;
#pragma warning restore IDE0032 // needed for functions -> disabled style info

    /// <summary>
    /// Value of message
    /// </summary>
    public IMessageValue<T> Value
    {
      get { return m_Value; }
      init { m_Value = value; }
    }

    /// <summary>
    /// Expected message type. MUST BE Overriden if Value Property is not MessageValue of T
    /// </summary>
    public virtual Type ExpectedMessageValueType
    {
      get
      {
        return typeof(MessageValue<T>);
      }
    }

    /// <summary>
    /// Set messages default to false
    /// </summary>
    public virtual bool IsSync { get; } = false;

    /// <summary>
    /// The Topic of this message
    /// </summary>
    public abstract string MessageTopic { get; }

    /// <summary>
    /// origin of the message
    /// </summary>
    public MessageOrigin Origin { get; set; }

    /// <summary>
    /// Default constructor assigning functions
    /// </summary>
    protected BaseMessage()
    {
      Origin = MessageOrigin.Local;
    }

    /// <summary>
    /// Set Client Identifier
    /// </summary>
    /// <param name="i_Identifier"></param>
    public void SetClientIdentifier(string i_Identifier)
    {
      if (Value != null)
      {
        Value.ClientIdentifier = i_Identifier;  
      }
    }

    /// <summary>
    /// Sets the value to i_Value if the types match
    /// </summary>
    /// <param name="i_Value"></param>
    public void SetValue(IMessageObjectValue i_Value)
    {
      if (i_Value is IMessageValue<T> casted)
      {
        m_Value = casted;
      }
      else
      {
        throw new WrongMessageValueException(typeof(IMessageValue<T>), i_Value.GetType());
      }
    }

    /// <summary>
    /// Returns the value
    /// </summary>
    /// <returns></returns>
    public IMessageObjectValue GetValue()
    {
      return m_Value;
    }

    /// <summary>
    /// Get Client Identifier
    /// </summary>
    /// <returns></returns>
    public string GetClientIdentifier()
    {
      if (m_Value != null)
      {
        return m_Value.ClientIdentifier;
      }

      return null;
    }

    /// <summary>
    /// Returns the actual value
    /// </summary>
    /// <returns></returns>
    public Result<T> GetActualValue()
    {
      if (Value != null)
      {
        return new Result<T>()
        {
          ResultValue = Value.Value,
          Success = true
        };
      }
      return new Result<T>()
      {
        Success = false
      };
    }
  }
}
