using NiceCore.Eventing.Messaging.Parameters;

namespace NiceCore.Eventing.Messages
{
  /// <summary>
  /// Result for Converting from the external message
  /// </summary>
  public struct FromExternalMessageResult
  {
    /// <summary>
    /// Message
    /// </summary>
    public IObjectMessage Message { get; set; }
    
    /// <summary>
    /// Parameters
    /// </summary>
    public IMessageSendingParameters Parameters { get; set; }

    /// <summary>
    /// Create
    /// </summary>
    /// <param name="i_Message"></param>
    /// <param name="i_Parameters"></param>
    /// <returns></returns>
    public static FromExternalMessageResult Create(IObjectMessage i_Message, IMessageSendingParameters i_Parameters)
    {
      return new()
      {
        Message = i_Message,
        Parameters = i_Parameters
      };
    }
  }
}