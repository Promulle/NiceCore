﻿namespace NiceCore.Eventing.Messages
{
  /// <summary>
  /// mesage value interface
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public interface IMessageValue<out T> : IMessageObjectValue
  {
    /// <summary>
    /// Value
    /// </summary>
    T Value { get; }
  }
}
