﻿namespace NiceCore.Eventing.Messages
{
  /// <summary>
  /// Enum declaring where a message came from
  /// </summary>
  public enum MessageOrigin
  {
    /// <summary>
    /// This message was created locally
    /// </summary>
    Local,

    /// <summary>
    /// This messages was received by a handler from a remote server
    /// </summary>
    Remote
  }
}
