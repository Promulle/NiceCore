namespace NiceCore.Eventing.Config
{
  /// <summary>
  /// Constants for MessagingClientIdentifier
  /// </summary>
  public static class MessagingClientIdentifierSettingConstants
  {
    /// <summary>
    /// Key
    /// </summary>
    public const string KEY = "NiceCore.Messages.Client.Identifier";
  }
}