﻿using System;
using NiceCore.Eventing.Messages;
using System.Threading.Tasks;
using NiceCore.Base;
using NiceCore.Eventing.Messaging.Parameters;

namespace NiceCore.Eventing
{
  /// <summary>
  /// Adapter for eventing that delegates messages to/from the eventing handlers
  /// </summary>
  public interface IEventingAdapter : IInitializable
  {
    /// <summary>
    /// Add a new handler to the eventing
    /// </summary>
    /// <param name="i_Handler"></param>
    /// <param name="i_ExternallyManaged"></param>
    void AddHandler(IEventingHandler i_Handler, bool i_ExternallyManaged);
    
    /// <summary>
    /// Sends the message to all added handlers
    /// </summary>
    /// <param name="i_Message"></param>
    /// <param name="i_Parameters"></param>
    /// <returns></returns>
    Task Send(IObjectMessage i_Message, IMessageSendingParameters i_Parameters);

    /// <summary>
    /// This will subscribe all handlers to i_Topic
    /// </summary>
    /// <param name="i_Topic"></param>
    /// <param name="i_MessageType"></param>
    void Subscribe(string i_Topic, Type i_MessageType);

    /// <summary>
    /// Unsubscribes all handlers from i_Topic
    /// </summary>
    /// <param name="i_Topic"></param>
    void Unsubscribe(string i_Topic);
  }
}
