namespace NiceCore.Eventing.Messaging.Impl
{
  /// <summary>
  /// Message Service Send Modes
  /// </summary>
  public enum MessageServiceSendModes
  {
    /// <summary>
    /// Natively run using async mechanics
    /// </summary>
    NativeAsync = 0,
    
    /// <summary>
    /// Force every message send to be async using Task.Run
    /// </summary>
    ForceAsync = 1
  }
}