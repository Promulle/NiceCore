﻿using NiceCore.Base.Services;
using NiceCore.Eventing.Messages;
using NiceCore.Eventing.Messaging.Receiver;
using NiceCore.ServiceLocation;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Diagnostics;
using NiceCore.Eventing.Messaging.Parameters;
using NiceCore.Eventing.Messaging.Receiver.Priority;
using NiceCore.Eventing.Messaging.Receiver.Store;
using NiceCore.Eventing.Messaging.Subscriptions;
using NiceCore.Eventing.Messaging.Utils;
using NiceCore.Logging.Extensions;

namespace NiceCore.Eventing.Messaging.Impl
{
  /// <summary>
  /// Default impl for IMessageService
  /// </summary>
  [InitializeOnResolve]
  [Register(InterfaceType = typeof(IMessageService), LifeStyle = LifeStyles.Singleton)]
  public class DefaultMessageService : BaseService, IMessageService
  {
    private MessageServiceSendModes m_SendMode = MessageServiceSendModes.ForceAsync;
    private readonly object m_LockObject = new();
    internal readonly MessageServiceReceiverStore m_ReceiverStore = new();

    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<IMessageService> Logger { get; set; } = DiagnosticsCollector.Instance.GetLogger<IMessageService>();

    /// <summary>
    /// Messaging Send Model
    /// </summary>
    public IMessagingSendModel MessagingSendModel { get; set; }
    
    /// <summary>
    /// MessageService to eventing adapter bridge
    /// </summary>
    public IMessageServiceToEventingAdapterBridge MessageServiceToEventingAdapterBridge { get; set; }
    
    /// <summary>
    /// Message Invocation priority store
    /// </summary>
    public IMessageInvocationReceiverPriorityStore MessageInvocationReceiverPriorityStore { get; set; }
    
    /// <summary>
    /// Message Service Send Mode Provider
    /// </summary>
    public IMessageServiceSendModeProvider MessageServiceSendModeProvider { get; set; }

    /// <summary>
    /// Default ctor
    /// </summary>
    public DefaultMessageService()
    {
      
    }

    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_Logger"></param>
    public DefaultMessageService(ILogger<IMessageService> i_Logger)
    {
      Logger = i_Logger;
    }
    
    /// <summary>
    /// initialize message service by setting bridge to the store so that unsubscribe messages will be received by the adapter
    /// </summary>
    protected override void InternalInitialize()
    {
      base.InternalInitialize();
      if (MessageServiceToEventingAdapterBridge != null)
      {
        m_ReceiverStore.SetAdapterBridge(MessageServiceToEventingAdapterBridge);  
      }

      if (MessageServiceSendModeProvider != null)
      {
        m_SendMode = MessageServiceSendModeProvider.GetSendMode();
      }
    }

    /// <summary>
    /// Registers a message receiver. Use i_ReceiverHandler to find the receiver for de-registration later.
    /// most of the time i_ReceiverHandler is the function that is called inside the receiver
    /// </summary>
    /// <param name="i_Receiver"></param>
    public MessageSubscription RegisterReceiver(IMessageInvocationReceiver i_Receiver)
    {
      if (i_Receiver.Descriptor == null)
      {
        throw new InvalidOperationException("Could not register a new receiver: Descriptor was null.");
      }
      return m_ReceiverStore.AddReceiver(i_Receiver.Descriptor.HandlerMessageType, i_Receiver);
    }

    /// <summary>
    /// Get priority store
    /// </summary>
    /// <returns></returns>
    public IMessageInvocationReceiverPriorityStore GetPriorityStore()
    {
      return MessageInvocationReceiverPriorityStore;
    }

    /// <summary>
    /// Send Message
    /// </summary>
    /// <param name="i_Message"></param>
    /// <param name="i_Parameters"></param>
    /// <param name="i_SendToRemote"></param>
    public Task SendMessage(IObjectMessage i_Message, IMessageSendingParameters i_Parameters, bool i_SendToRemote)
    {
      if (i_Message.IsSync)
      {
        return SendMessageInternal(i_Message, i_Parameters, i_SendToRemote);
      }

      if (m_SendMode == MessageServiceSendModes.ForceAsync)
      {
        return Task.Run(async () =>
        {
          await SendMessageInternal(i_Message, i_Parameters, i_SendToRemote).ConfigureAwait(false);
        });  
      }
      
      if (m_SendMode == MessageServiceSendModes.NativeAsync)
      {
        return SendMessageInternal(i_Message, i_Parameters, i_SendToRemote);
      }

      throw new NotSupportedException($"DefaultMessageService does not support Send Mode: {m_SendMode} Supported modes: {MessageServiceSendModes.NativeAsync}, {MessageServiceSendModes.ForceAsync}");
    }

    private Task SendMessageInternal(IObjectMessage i_Message, IMessageSendingParameters i_Parameters, bool i_SendToRemote)
    {
      Logger.Information($"Sending message of type {i_Message.GetType().FullName}");
      var registered = m_ReceiverStore.GetReceiversFor(i_Message);
      var sendTask   = MessagingSendModel.SendMessage(i_Message, i_Parameters, registered);
      if (i_SendToRemote && MessageServiceToEventingAdapterBridge != null)
      {
        var externalSendTask = MessageServiceToEventingAdapterBridge.SendMessageToRemote(i_Message, i_Parameters);
        return Task.WhenAll(externalSendTask, sendTask);
      }

      return sendTask;
    }

    /// <summary>
    /// Cleares all registrations
    /// </summary>
    public void Clear()
    {
      lock (m_LockObject)
      {
        m_ReceiverStore.Clear();
      }
    }

    /// <summary>
    /// Dispose for clear call
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    protected override void Dispose(bool i_IsDisposing)
    {
      Clear();
      base.Dispose(i_IsDisposing);
    }
  }
}
