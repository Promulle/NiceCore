using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Base;
using NiceCore.Eventing.Messages;
using NiceCore.Eventing.Messaging.Distribution;
using NiceCore.Eventing.Messaging.Filtering;
using NiceCore.Eventing.Messaging.Parameters;
using NiceCore.Eventing.Messaging.Receiver;
using NiceCore.ServiceLocation;

namespace NiceCore.Eventing.Messaging.Impl
{
  /// <summary>
  /// Default messaging send model
  /// </summary>
  [InitializeOnResolve]
  [Register(InterfaceType = typeof(IMessagingSendModel), LifeStyle = LifeStyles.Singleton)]
  public class DefaultMessagingSendModel : IInitializable, IMessagingSendModel
  {
    private List<IMessageInvocationReceiverFilter> m_OrderedFilters;
    
    /// <summary>
    /// Injected filters
    /// </summary>
    public IList<IMessageInvocationReceiverFilter> Filters {get; set; }
    
    /// <summary>
    /// Factories
    /// </summary>
    public IList<IMessageSendingParameterBasedFactory> Factories { get; set; }
    
    /// <summary>
    /// Distributors
    /// </summary>
    public IList<IMessageDistributor> Distributors { get; set; }

    /// <summary>
    /// Grouper
    /// </summary>
    public IMessageDistributionGrouper Grouper { get; set; }

    /// <summary>
    /// Is initialized
    /// </summary>
    public bool IsInitialized { get; private set; }
    
    /// <summary>
    /// Initialize
    /// </summary>
    public void Initialize()
    {
      if (!IsInitialized)
      {
        m_OrderedFilters = Filters?.OrderBy(r => r.Priority).ToList();
        IsInitialized = true;
      }
    }

    /// <summary>
    /// Send messages
    /// </summary>
    /// <param name="i_Message"></param>
    /// <param name="i_Parameter"></param>
    /// <param name="i_Receivers"></param>
    /// <returns></returns>
    public Task SendMessage(IObjectMessage i_Message, IMessageSendingParameters i_Parameter, IEnumerable<IMessageInvocationReceiver> i_Receivers)
    {
      var factory = GetFactoryFor(i_Parameter);
      var filterParameter = factory?.CreateFilterParameter(i_Parameter);
      var receiversToUse = FilterReceivers(i_Receivers, filterParameter);
      var groups = Grouper.GroupByRequirement(receiversToUse);
      List<Task> distributorTasks = new();
      foreach (var group in groups)
      {
        var distributor = GetDistributorForGroup(group);
        distributorTasks.Add(distributor.Distribute(i_Message, group));
      }
      return Task.WhenAll(distributorTasks);
    }

    private IMessageDistributor GetDistributorForGroup(MessageDistributionGroup i_Group)
    {
      if (Distributors == null || Distributors.Count < 1)
      {
        throw new InvalidOperationException("Tried to send message but no Distributors are configured for sending");
      }
      if (Distributors.Count == 1)
      {
        return Distributors[0];
      }

      var byType = GetDistributorByRequirementType(i_Group.RequirementType);
      if (byType != null)
      {
        return byType;
      }

      var defaultDistributor = GetDistributorByRequirementType(null);
      if (defaultDistributor == null)
      {
        throw new InvalidOperationException($"Tried to distribute message but no default distributor was found and no specific distributor for type {i_Group.RequirementType.FullName} was configured.");
      }
      return defaultDistributor;
    }

    private IMessageDistributor GetDistributorByRequirementType(Type i_RequirementType)
    {
      foreach (var messageDistributor in Distributors)
      {
        if (messageDistributor.RequirementType == i_RequirementType)
        {
          return messageDistributor;
        }
      }

      return null;
    }

    private IEnumerable<IMessageInvocationReceiver> FilterReceivers(IEnumerable<IMessageInvocationReceiver> i_Receivers, IMessageInvocationReceiverFilterParameter i_Parameter)
    {
      if (m_OrderedFilters == null || m_OrderedFilters.Count < 1)
      {
        return i_Receivers;
      }

      var receivers = i_Receivers;
      foreach (var filter in m_OrderedFilters)
      {
        receivers = filter.FilterReceivers(receivers, i_Parameter);
      }
      return receivers;
    }

    private IMessageSendingParameterBasedFactory GetFactoryFor(IMessageSendingParameters i_Parameters)
    {
      if (i_Parameters == null || Factories == null || Factories.Count < 1)
      {
        return null;
      }

      foreach (var factory in Factories)
      {
        if (factory.CanCreateInstancesFor(i_Parameters))
        {
          return factory;
        }
      }

      return null;
    }
  }
}