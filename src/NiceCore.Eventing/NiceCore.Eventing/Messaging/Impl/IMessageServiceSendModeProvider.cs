namespace NiceCore.Eventing.Messaging.Impl
{
  /// <summary>
  /// Message Service Send Mode Provider
  /// </summary>
  public interface IMessageServiceSendModeProvider
  {
    /// <summary>
    /// Get Send Mode
    /// </summary>
    /// <returns></returns>
    MessageServiceSendModes GetSendMode();
  }
}