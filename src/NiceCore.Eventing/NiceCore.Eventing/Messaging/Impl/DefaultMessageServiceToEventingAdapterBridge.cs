using System;
using System.Threading.Tasks;
using NiceCore.Base;
using NiceCore.Base.Services;
using NiceCore.Eventing.Messages;
using NiceCore.Eventing.Messaging.Parameters;
using NiceCore.ServiceLocation;
using NiceCore.ServiceLocation.Helpers;

namespace NiceCore.Eventing.Messaging.Impl
{
  /// <summary>
  /// Impl for message service to adapter bridge
  /// </summary>
  [Register(InterfaceType = typeof(IMessageServiceToEventingAdapterBridge))]
  [InitializeOnResolve]
  public class DefaultMessageServiceToEventingAdapterBridge : BaseService, IMessageServiceToEventingAdapterBridge
  {
    /// <summary>
    /// Eventing adapter that sends/receives messages from remote handlers
    /// </summary>
    public OnDemandResolvedInstance<IEventingAdapter> EventingAdapter { get; set; }
    
    /// <summary>
    /// Servie Container
    /// </summary>
    public IServiceContainer ServiceContainer { get; set; }
    
    /// <summary>
    /// Message Service for communicating with the local message system
    /// </summary>
    public IMessageService MessageService { get; set; }

    /// <summary>
    /// Internal Initialize
    /// </summary>
    protected override void InternalInitialize()
    {
      EventingAdapter = new(ServiceContainer);
    }

    /// <summary>
    /// Subscribe to a remote message
    /// </summary>
    /// <param name="i_Topic"></param>
    /// <param name="i_MessageType"></param>
    public void SubscribeToRemote(string i_Topic, Type i_MessageType)
    {
      EventingAdapter.GetInstance().Subscribe(i_Topic, i_MessageType);
    }

    /// <summary>
    /// Unsubscribe from a remote message
    /// </summary>
    /// <param name="i_Topic"></param>
    public void UnsubscribeFromRemote(string i_Topic)
    {
      EventingAdapter.GetInstance().Unsubscribe(i_Topic);
    }

    /// <summary>
    /// Sends a message to all remote handlers
    /// </summary>
    /// <param name="i_Message"></param>
    /// <param name="i_Parameters"></param>
    /// <returns></returns>
    public Task SendMessageToRemote(IObjectMessage i_Message, IMessageSendingParameters i_Parameters)
    {
      return EventingAdapter.GetInstance().Send(i_Message, i_Parameters);
    }

    /// <summary>
    /// Sends a message from remote to the local message system
    /// </summary>
    /// <param name="i_Message"></param>
    /// <param name="i_Parameters"></param>
    /// <returns></returns>
    public Task ForwardMessageFromRemote(IObjectMessage i_Message, IMessageSendingParameters i_Parameters)
    {
      return MessageService.SendMessage(i_Message, i_Parameters, false);
    }
  }
}