using System;
using System.Threading.Tasks;
using NiceCore.Eventing.Messages;
using NiceCore.Eventing.Messaging.Parameters;

namespace NiceCore.Eventing.Messaging.Fluent
{
  /// <summary>
  /// Base for send builders
  /// </summary>
  /// <typeparam name="TMessage"></typeparam>
  public abstract class BaseSendMessageBuilder<TMessage>
    where TMessage : IObjectMessage
  {
    private readonly TMessage m_Message;
    private readonly IMessageService m_MessageService;
    private bool m_LocalOnly = false;

    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_MessageService"></param>
    /// <param name="i_Message"></param>
    protected BaseSendMessageBuilder(IMessageService i_MessageService, TMessage i_Message)
    {
      if (i_Message == null)
      {
        throw new ArgumentException("Cannot send null message.");
      }

      m_Message = i_Message;
      m_MessageService = i_MessageService;
    }

    /// <summary>
    /// Sets local only
    /// </summary>
    /// <param name="i_Value"></param>
    protected void SetLocalOnly(bool i_Value)
    {
      m_LocalOnly = i_Value;
    }

    /// <summary>
    /// Get parameters
    /// </summary>
    /// <returns></returns>
    protected virtual IMessageSendingParameters GetParametersForSending()
    {
      return null;
    }

    /// <summary>
    /// Send the message
    /// </summary>
    /// <returns></returns>
    public Task Send()
    {
      return m_MessageService.SendMessage(m_Message, GetParametersForSending(), !m_LocalOnly);
    }
  }
}