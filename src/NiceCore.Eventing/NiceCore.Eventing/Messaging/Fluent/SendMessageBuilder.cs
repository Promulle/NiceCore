using NiceCore.Eventing.Messages;

namespace NiceCore.Eventing.Messaging.Fluent
{
  /// <summary>
  /// Send message fluent builder
  /// </summary>
  /// <typeparam name="TMessage"></typeparam>
  public class SendMessageBuilder<TMessage> : BaseSendMessageBuilder<TMessage>
    where TMessage : IObjectMessage
  {
    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_MessageService"></param>
    /// <param name="i_Message"></param>
    public SendMessageBuilder(IMessageService i_MessageService, TMessage i_Message) : base(i_MessageService, i_Message)
    {
      
    }

    /// <summary>
    /// Message will be sent local only
    /// </summary>
    /// <returns></returns>
    public SendMessageBuilder<TMessage> LocalOnly()
    {
      SetLocalOnly(true);
      return this;
    }
  }
}