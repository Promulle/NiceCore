using System;
using System.Collections.Generic;
using System.Linq;
using NiceCore.Collections;
using NiceCore.Eventing.Messaging.Receiver;
using NiceCore.Eventing.Messaging.Receiver.Priority;
using NiceCore.Eventing.Messaging.Subscriptions;

namespace NiceCore.Eventing.Messaging.Fluent
{
  /// <summary>
  /// Fluent for registering receivers
  /// </summary>
  public class RegisterMessageBuilder
  {
    private readonly IMessageService m_MessageService;
    private readonly IMessageInvocationReceiverPriorityStore m_PriorityStore;
    private readonly IMessageInvocationReceiverCreator m_ReceiverCreator;
    private readonly Type m_HandlerMessageType;
    private readonly Type m_RegisterMessageType;

    private IReceiverRegistrationTypeProvider m_ReceiverRegistrationTypeProvider = ReceiverRegistrationTypeProvider.DEFAULT;
    private HashSet<string> m_Tags;
    private MessageSubscriptionUnsubscribeContext m_UnsubscribeContext;
    private IMessageInvocationReceiverRequirement m_Requirement;
    private MessageInvocationReceiverPriority m_Priority;

    /// <summary>
    /// Ctor filling message service and receiver
    /// </summary>
    /// <param name="i_MessageService"></param>>
    /// <param name="i_HandlerMessageType"></param>
    /// <param name="i_RegisterMessageType"></param>
    /// <param name="i_ReceiverCreator"></param>
    /// <param name="i_PriorityStore"></param>
    public RegisterMessageBuilder(Type i_HandlerMessageType,
      Type i_RegisterMessageType,
      IMessageInvocationReceiverCreator i_ReceiverCreator,
      IMessageService i_MessageService,
      IMessageInvocationReceiverPriorityStore i_PriorityStore)
    {
      m_MessageService = i_MessageService;
      m_PriorityStore = i_PriorityStore;
      m_ReceiverCreator = i_ReceiverCreator;
      m_HandlerMessageType = i_HandlerMessageType;
      m_RegisterMessageType = i_RegisterMessageType;
    }

    /// <summary>
    /// Adds the returned subscription to t_Context
    /// </summary>
    /// <param name="t_Context"></param>
    /// <returns></returns>
    public RegisterMessageBuilder InUnsubscribeContext(MessageSubscriptionUnsubscribeContext t_Context)
    {
      m_UnsubscribeContext = t_Context;
      return this;
    }

    /// <summary>
    /// With priority
    /// </summary>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    public RegisterMessageBuilder WithPriority(string i_Name)
    {
      m_Priority = m_PriorityStore.GetPriorityFor(i_Name);
      return this;
    }

    /// <summary>
    /// With Tag
    /// </summary>
    /// <param name="i_Tag"></param>
    /// <returns></returns>
    public RegisterMessageBuilder WithTag(string i_Tag)
    {
      m_Tags ??= new();
      m_Tags.Add(i_Tag);
      return this;
    }

    /// <summary>
    /// With Tags. Replaces previously assigned tags
    /// </summary>
    /// <param name="i_Tags"></param>
    /// <returns></returns>
    public RegisterMessageBuilder WithTags(IEnumerable<string> i_Tags)
    {
      m_Tags = i_Tags.ToHashSet();
      return this;
    }

    /// <summary>
    /// Requirement
    /// </summary>
    /// <param name="i_Requirement"></param>
    /// <returns></returns>
    public RegisterMessageBuilder Requiring(IMessageInvocationReceiverRequirement i_Requirement)
    {
      m_Requirement = i_Requirement;
      return this;
    }

    /// <summary>
    /// Provide Registration Types
    /// </summary>
    /// <param name="i_TypeProvider"></param>
    /// <returns></returns>
    public RegisterMessageBuilder ProvideRegistrationTypes(IReceiverRegistrationTypeProvider i_TypeProvider)
    {
      m_ReceiverRegistrationTypeProvider = i_TypeProvider;
      return this;
    }

    /// <summary>
    /// Register the receiver with the info from the filled fields
    /// </summary>
    public MessageSubscription Register()
    {
      var receiver = m_ReceiverCreator.CreateReceiver(new()
      {
        Requirement = m_Requirement,
        HandlerMessageType = m_HandlerMessageType,
        RegisterMessageType = m_RegisterMessageType,
        Priority = m_Priority,
        Tags = m_Tags ?? NcDefaultSets<string>.Empty,
        ReceiverRegistrationTypeProvider = m_ReceiverRegistrationTypeProvider
      });
      var subscription = m_MessageService.RegisterReceiver(receiver);
      m_UnsubscribeContext?.AddSubscription(subscription);
      return subscription;
    }
  }
}