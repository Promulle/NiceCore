using System;
using NiceCore.Eventing.Messages;
using NiceCore.Eventing.Messaging.Parameters;

namespace NiceCore.Eventing.Messaging.Fluent
{
  /// <summary>
  /// Parameterised send message builder
  /// </summary>
  /// <typeparam name="TMessage"></typeparam>
  /// <typeparam name="TParameters"></typeparam>
  public class ParameterisedSendMessageBuilder<TMessage, TParameters> : BaseSendMessageBuilder<TMessage>
    where TMessage : IObjectMessage
    where TParameters : class, IMessageSendingParameters
  {
    private TParameters m_Parameters;

    /// <summary>
    /// Parameters accessible to enable other code to change parameters -> enables better extension funcs
    /// </summary>
    public TParameters Parameters
    {
      get
      {
        if (m_Parameters == null)
        {
          m_Parameters = Activator.CreateInstance<TParameters>();
        }

        return m_Parameters;
      }
    }

    /// <summary>
    /// Ctor
    /// </summary>
    /// <param name="i_MessageService"></param>
    /// <param name="i_Message"></param>
    public ParameterisedSendMessageBuilder(IMessageService i_MessageService, TMessage i_Message): base(i_MessageService, i_Message)
    {
    }

    /// <summary>
    /// Message will be sent local only
    /// </summary>
    /// <returns></returns>
    public ParameterisedSendMessageBuilder<TMessage, TParameters> LocalOnly()
    {
      SetLocalOnly(true);
      return this;
    }

    /// <summary>
    /// Set parameters complete
    /// </summary>
    /// <param name="i_Parameters"></param>
    /// <returns></returns>
    public ParameterisedSendMessageBuilder<TMessage, TParameters> WithParameters(TParameters i_Parameters)
    {
      m_Parameters = i_Parameters;
      return this;
    }

    /// <summary>
    /// Get parameters
    /// </summary>
    /// <returns></returns>
    protected override IMessageSendingParameters GetParametersForSending()
    {
      return m_Parameters;
    }
  }
}