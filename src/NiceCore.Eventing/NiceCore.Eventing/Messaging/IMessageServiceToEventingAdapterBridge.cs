using System;
using System.Threading.Tasks;
using NiceCore.Eventing.Messages;
using NiceCore.Eventing.Messaging.Parameters;

namespace NiceCore.Eventing.Messaging
{
  /// <summary>
  /// Interface for class used to bridge message service and its internals to the eventingAdapter
  /// </summary>
  public interface IMessageServiceToEventingAdapterBridge
  {
    /// <summary>
    /// Subscribe to a remote for i_Topic with i_MessageType
    /// </summary>
    /// <param name="i_Topic"></param>
    /// <param name="i_MessageType"></param>
    void SubscribeToRemote(string i_Topic, Type i_MessageType);

    /// <summary>
    /// Unsubscribe from Remote
    /// </summary>
    /// <param name="i_Topic"></param>
    void UnsubscribeFromRemote(string i_Topic);

    /// <summary>
    /// Send a message to remote
    /// </summary>
    /// <param name="i_Message"></param>
    /// <param name="i_Parameters"></param>
    Task SendMessageToRemote(IObjectMessage i_Message, IMessageSendingParameters i_Parameters);

    /// <summary>
    /// Sends a message that arrived from remote to the local message system 
    /// </summary>
    /// <param name="i_Message"></param>
    /// <param name="i_Parameters"></param>
    /// <returns></returns>
    Task ForwardMessageFromRemote(IObjectMessage i_Message, IMessageSendingParameters i_Parameters);
  }
}