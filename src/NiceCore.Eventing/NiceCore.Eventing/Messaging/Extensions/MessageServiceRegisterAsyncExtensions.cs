using System;
using System.Threading.Tasks;
using NiceCore.Eventing.Messages;
using NiceCore.Eventing.Messaging.Fluent;
using NiceCore.Eventing.Messaging.Receiver.Creators;
using NiceCore.Eventing.Messaging.Subscriptions;
using Polly;

namespace NiceCore.Eventing.Messaging.Extensions
{
  /// <summary>
  /// Extension methods for async registrations
  /// </summary>
  public static class MessageServiceRegisterAsyncExtensions
  {
    /// <summary>
    /// Register Async
    /// </summary>
    /// <typeparam name="TMessage"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <param name="t_MessageService"></param>
    /// <param name="i_AsyncHandlerFunc"></param>
    public static RegisterMessageBuilder Register<TMessage, TValue>(this IMessageService t_MessageService, Func<TMessage, Task> i_AsyncHandlerFunc)
      where TMessage : class, IMessage<TValue>
    {
      return RegisterAsyncInternal<TMessage, TValue>(typeof(TMessage), t_MessageService, i_AsyncHandlerFunc, null);
    }
    
    /// <summary>
    /// Register Async
    /// </summary>
    /// <typeparam name="TMessage"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <param name="t_MessageService"></param>
    /// <param name="i_AsyncHandlerFunc"></param>
    /// <param name="i_Policy"></param>
    public static RegisterMessageBuilder Register<TMessage, TValue>(this IMessageService t_MessageService, Func<TMessage, Task> i_AsyncHandlerFunc, IAsyncPolicy i_Policy)
      where TMessage : class, IMessage<TValue>
    {
      return RegisterAsyncInternal<TMessage, TValue>(typeof(TMessage), t_MessageService, i_AsyncHandlerFunc, i_Policy);
    }
    
    /// <summary>
    /// Register Async
    /// </summary>
    /// <typeparam name="TMessage"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <typeparam name="TRegisterMessage"></typeparam>
    /// <typeparam name="TRegisterMessageValue"></typeparam>
    /// <param name="t_MessageService"></param>
    /// <param name="i_AsyncHandlerFunc"></param>
    public static RegisterMessageBuilder Register<TMessage, TValue, TRegisterMessage, TRegisterMessageValue>(this IMessageService t_MessageService, Func<TMessage, Task> i_AsyncHandlerFunc)
      where TMessage : class, IMessage<TValue>
      where TRegisterMessage : class, IMessage<TRegisterMessageValue>
    {
      return RegisterAsyncInternal<TMessage, TValue>(typeof(TRegisterMessage), t_MessageService, i_AsyncHandlerFunc, null);
    }

    /// <summary>
    /// Register Async
    /// </summary>
    /// <typeparam name="TMessage"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <param name="t_MessageService"></param>
    /// <param name="i_AsyncHandlerFunc"></param>
    public static MessageSubscription RegisterDirect<TMessage, TValue>(this IMessageService t_MessageService, Func<TMessage, Task> i_AsyncHandlerFunc)
      where TMessage : class, IMessage<TValue>
    {
      return Register<TMessage, TValue>(t_MessageService, i_AsyncHandlerFunc).Register();
    }

    /// <summary>
    /// Register Async
    /// </summary>
    /// <typeparam name="TMessage"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <param name="t_MessageService"></param>
    /// <param name="i_AsyncHandlerFunc"></param>
    /// <param name="i_Policy"></param>
    public static MessageSubscription RegisterDirect<TMessage, TValue>(this IMessageService t_MessageService, Func<TMessage, Task> i_AsyncHandlerFunc, IAsyncPolicy i_Policy) where TMessage : class, IMessage<TValue>
    {
      return Register<TMessage, TValue>(t_MessageService, i_AsyncHandlerFunc, i_Policy).Register();
    }

    private static RegisterMessageBuilder RegisterAsyncInternal<TMessage, TValue>(
      Type i_MessageType,
      IMessageService i_MessageService,
      Func<TMessage, Task> i_AsyncHandlerFunc,
      IAsyncPolicy i_Policy)
      where TMessage : class, IMessage<TValue>
    {
      var invocationReceiverCreator = new AsyncMessageInvocationReceiverCreator<TMessage, TValue>()
      {
        Policy = i_Policy,
        MessageReceiveFunc = i_AsyncHandlerFunc,
        Logger = i_MessageService.Logger
      };
      return new(typeof(TMessage),i_MessageType, invocationReceiverCreator, i_MessageService, i_MessageService.GetPriorityStore());
    }
  }
}