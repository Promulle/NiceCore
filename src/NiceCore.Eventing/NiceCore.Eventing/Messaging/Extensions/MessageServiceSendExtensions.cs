using System.Threading.Tasks;
using NiceCore.Eventing.Messages;
using NiceCore.Eventing.Messaging.Fluent;

namespace NiceCore.Eventing.Messaging.Extensions
{
  /// <summary>
  /// Extensions for sending messages
  /// </summary>
  public static class MessageServiceSendExtensions
  {
    /// <summary>
    /// Sends message with support for polly policies
    /// </summary>
    /// <typeparam name="TMessage"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <param name="i_MessageService"></param>
    /// <param name="i_Message"></param>
    public static SendMessageBuilder<TMessage> SendMessage<TMessage, TValue>(this IMessageService i_MessageService, TMessage i_Message) where TMessage : class, IMessage<TValue>
    {
      return new(i_MessageService, i_Message);
    }
    
    /// <summary>
    /// Sends message with support for polly policies
    /// </summary>
    /// <typeparam name="TMessage"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <param name="i_MessageService"></param>
    /// <param name="i_Message"></param>
    public static Task SendMessageDirect<TMessage, TValue>(this IMessageService i_MessageService, TMessage i_Message) where TMessage : class, IMessage<TValue>
    {
      return SendMessage<TMessage, TValue>(i_MessageService, i_Message).Send();
    }

    /// <summary>
    /// Sends a given instance of IMessage to all registered methods for that type. Only local in this running instance.
    /// This modifies t_Message by setting t_Message.Origin to MessageOrigin.Remote. MessageSent event is still invoked
    /// </summary>
    /// <typeparam name="TMessage"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <param name="i_MessageService"></param>
    /// <param name="t_Message"></param>
    public static SendMessageBuilder<TMessage> SendMessageLocalOnly<TMessage, TValue>(this IMessageService i_MessageService, TMessage t_Message) where TMessage : class, IMessage<TValue>
    {
      return SendMessage<TMessage, TValue>(i_MessageService, t_Message).LocalOnly();
    }
    
    /// <summary>
    /// Sends a given instance of IMessage to all registered methods for that type. Only local in this running instance.
    /// This modifies t_Message by setting t_Message.Origin to MessageOrigin.Remote. MessageSent event is still invoked
    /// </summary>
    /// <typeparam name="TMessage"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <param name="i_MessageService"></param>
    /// <param name="t_Message"></param>
    public static Task SendMessageLocalOnlyDirect<TMessage, TValue>(this IMessageService i_MessageService, TMessage t_Message) where TMessage : class, IMessage<TValue>
    {
      return SendMessage<TMessage, TValue>(i_MessageService, t_Message).LocalOnly().Send();
    }
  }
}