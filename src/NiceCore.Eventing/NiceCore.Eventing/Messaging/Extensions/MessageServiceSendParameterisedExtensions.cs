using NiceCore.Eventing.Messages;
using NiceCore.Eventing.Messaging.Fluent;
using NiceCore.Eventing.Messaging.Parameters;

namespace NiceCore.Eventing.Messaging.Extensions
{
  /// <summary>
  /// Extensions for sending parameterised messages
  /// </summary>
  public static class MessageServiceSendParameterisedExtensions
  {
    /// <summary>
    /// Sends message with support for polly policies
    /// </summary>
    /// <typeparam name="TMessage"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <typeparam name="TParameter"></typeparam>
    /// <param name="i_MessageService"></param>
    /// <param name="i_Message"></param>
    public static ParameterisedSendMessageBuilder<TMessage, TParameter> SendMessage<TMessage, TValue, TParameter>(this IMessageService i_MessageService, TMessage i_Message)
      where TMessage : class, IMessage<TValue>
      where TParameter : class, IMessageSendingParameters
    {
      return new(i_MessageService, i_Message);
    }

    /// <summary>
    /// Sends a given instance of IMessage to all registered methods for that type. Only local in this running instance.
    /// This modifies t_Message by setting t_Message.Origin to MessageOrigin.Remote. MessageSent event is still invoked
    /// </summary>
    /// <typeparam name="TMessage"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <typeparam name="TParameter"></typeparam>
    /// <param name="i_MessageService"></param>
    /// <param name="t_Message"></param>
    public static ParameterisedSendMessageBuilder<TMessage, TParameter> SendMessageLocalOnly<TMessage, TValue, TParameter>(this IMessageService i_MessageService, TMessage t_Message)
      where TMessage : class, IMessage<TValue>
      where TParameter : class, IMessageSendingParameters
    {
      return SendMessage<TMessage, TValue, TParameter>(i_MessageService, t_Message).LocalOnly();
    }
  }
}