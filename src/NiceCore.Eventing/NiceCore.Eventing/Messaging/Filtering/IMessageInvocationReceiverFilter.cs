using System.Collections.Generic;
using NiceCore.Eventing.Messaging.Receiver;

namespace NiceCore.Eventing.Messaging.Filtering
{
  /// <summary>
  /// Filter that filters receivers.
  /// </summary>
  public interface IMessageInvocationReceiverFilter
  {
    /// <summary>
    /// Priority of the filter
    /// </summary>
    int Priority { get; }

    /// <summary>
    /// Filters i_Filters and returns only 
    /// </summary>
    /// <param name="i_Receivers">receivers to be filtered</param>
    /// <param name="i_Parameter">Requirement to filter receivers by</param>
    /// <returns></returns>
    IEnumerable<IMessageInvocationReceiver> FilterReceivers(IEnumerable<IMessageInvocationReceiver> i_Receivers, IMessageInvocationReceiverFilterParameter i_Parameter);
  }
}