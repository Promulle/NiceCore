using System.Collections.Generic;
using System.Linq;
using NiceCore.Eventing.Messaging.Receiver;
using NiceCore.Eventing.Messaging.Receiver.Priority;
using NiceCore.ServiceLocation;

namespace NiceCore.Eventing.Messaging.Filtering.Impl
{
  /// <summary>
  /// Filter by priority
  /// </summary>
  [Register(InterfaceType = typeof(IMessageInvocationReceiverFilter))]
  public class MessageInvocationReceiverPriorityFilter : IMessageInvocationReceiverFilter
  {
    /// <summary>
    /// Priority = 0 this should run first
    /// </summary>
    public int Priority { get; } = 0;
    
    /// <summary>
    /// Priority store
    /// </summary>
    public IMessageInvocationReceiverPriorityStore PriorityStore { get; set; }
    
    /// <summary>
    /// Filter Receivers by priority
    /// </summary>
    /// <param name="i_Receivers"></param>
    /// <param name="i_Requirement"></param>
    /// <returns></returns>
    public IEnumerable<IMessageInvocationReceiver> FilterReceivers(IEnumerable<IMessageInvocationReceiver> i_Receivers, IMessageInvocationReceiverFilterParameter i_Requirement)
    {
      if (i_Requirement == null || i_Requirement.PriorityName == null)
      {
        return i_Receivers;
      }
      var priority = PriorityStore.GetPriorityFor(i_Requirement.PriorityName);
      return FilterReceiversEnumerable(i_Receivers, priority);
    }

    private static IEnumerable<IMessageInvocationReceiver> FilterReceiversEnumerable(IEnumerable<IMessageInvocationReceiver> i_Receivers, MessageInvocationReceiverPriority i_Priority)
    {
      foreach (var invocationReceiver in i_Receivers)
      {
        if (CheckPriorityForReceiver(invocationReceiver, i_Priority))
        {
          yield return invocationReceiver;
        }
      }
    }

    private static bool CheckPriorityForReceiver(IMessageInvocationReceiver i_Receiver, MessageInvocationReceiverPriority i_Priority)
    {
      return i_Priority != null && i_Receiver.Descriptor.Priority.Priority <= i_Priority.Priority;
    }
  }
}