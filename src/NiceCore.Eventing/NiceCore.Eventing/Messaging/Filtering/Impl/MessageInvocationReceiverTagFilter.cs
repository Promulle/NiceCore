using System.Collections.Generic;
using NiceCore.Eventing.Messaging.Receiver;
using NiceCore.ServiceLocation;

namespace NiceCore.Eventing.Messaging.Filtering.Impl
{
  /// <summary>
  /// Message Invocation Receiver Tag Filter
  /// </summary>
  [Register(InterfaceType = typeof(IMessageInvocationReceiverFilter))]
  public class MessageInvocationReceiverTagFilter : IMessageInvocationReceiverFilter
  {
    /// <summary>
    /// Priority
    /// </summary>
    public int Priority { get; } = 1;
    
    /// <summary>
    /// Filter Receivers
    /// </summary>
    /// <param name="i_Receivers"></param>
    /// <param name="i_Parameter"></param>
    /// <returns></returns>
    public IEnumerable<IMessageInvocationReceiver> FilterReceivers(IEnumerable<IMessageInvocationReceiver> i_Receivers,
      IMessageInvocationReceiverFilterParameter i_Parameter)
    {
      if (i_Parameter == null)
      {
        return i_Receivers;
      }
      var byExcludedTags = FilterReceiversByExcludedTags(i_Receivers, i_Parameter.ExcludedTags);
      var byRequiredTags = FilterReceiversByRequiredTags(byExcludedTags, i_Parameter.RequiredTags);
      return byRequiredTags;
    }

    private static IEnumerable<IMessageInvocationReceiver> FilterReceiversByExcludedTags(
      IEnumerable<IMessageInvocationReceiver> i_Receivers,
      IReadOnlyCollection<string> i_ExcludedTags)
    {
      if (i_ExcludedTags == null || i_ExcludedTags.Count == 0)
      {
        return i_Receivers;
      }

      return FilterReceiversByExcludedTagsEnumerable(i_Receivers, i_ExcludedTags);
    }
    
    private static IEnumerable<IMessageInvocationReceiver> FilterReceiversByRequiredTags(
      IEnumerable<IMessageInvocationReceiver> i_Receivers,
      IReadOnlyCollection<string> i_RequiredTags)
    {
      if (i_RequiredTags == null || i_RequiredTags.Count == 0)
      {
        return i_Receivers;
      }

      return FilterReceiversByRequiredTagsEnumerable(i_Receivers, i_RequiredTags);
    }

    private static IEnumerable<IMessageInvocationReceiver> FilterReceiversByExcludedTagsEnumerable(
      IEnumerable<IMessageInvocationReceiver> i_Receivers,
      IReadOnlyCollection<string> i_ExcludedTags)
    {
      foreach (var messageInvocationReceiver in i_Receivers)
      {
        var allExcluded = true;
        foreach (var excludedTag in i_ExcludedTags)
        {
          if (messageInvocationReceiver.Descriptor.Tags.Contains(excludedTag))
          {
            allExcluded = false;
            break;
          }
        }

        if (allExcluded)
        {
          yield return messageInvocationReceiver;
        }
      }
    }
    
    private static IEnumerable<IMessageInvocationReceiver> FilterReceiversByRequiredTagsEnumerable(
      IEnumerable<IMessageInvocationReceiver> i_Receivers,
      IReadOnlyCollection<string> i_RequiredTags)
    {
      foreach (var messageInvocationReceiver in i_Receivers)
      {
        var meetsAllTags = true;
        foreach (var requiredTag in i_RequiredTags)
        {
          if (!messageInvocationReceiver.Descriptor.Tags.Contains(requiredTag))
          {
            meetsAllTags = false;
            break;
          }
        }

        if (meetsAllTags)
        {
          yield return messageInvocationReceiver;  
        }
      }
    }
  }
}