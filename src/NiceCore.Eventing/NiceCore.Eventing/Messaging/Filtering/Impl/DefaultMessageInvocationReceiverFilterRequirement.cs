using System.Collections.Generic;

namespace NiceCore.Eventing.Messaging.Filtering.Impl
{
  /// <summary>
  /// Default message invocation receiver filter requirement
  /// </summary>
  public class DefaultMessageInvocationReceiverFilterRequirement : IMessageInvocationReceiverFilterParameter
  {
    /// <summary>
    /// Name
    /// </summary>
    public string PriorityName { get; init; }

    /// <summary>
    /// Excluded Tags
    /// </summary>
    public IReadOnlyCollection<string> ExcludedTags { get; init; }
    
    /// <summary>
    /// Exclusive Tags
    /// </summary>
    public IReadOnlyCollection<string> RequiredTags { get; init; }
  }
}