using System.Collections.Generic;

namespace NiceCore.Eventing.Messaging.Filtering
{
  /// <summary>
  /// Interface all filter requirements must fulfill
  /// </summary>
  public interface IMessageInvocationReceiverFilterParameter
  {
    /// <summary>
    /// Name of priority
    /// </summary>
    string PriorityName { get; }
    
    /// <summary>
    /// Tags to which the message should not be sent
    /// </summary>
    IReadOnlyCollection<string> ExcludedTags { get; }
    
    /// <summary>
    /// Tags to which the message should be sent exclusively
    /// </summary>
    IReadOnlyCollection<string> RequiredTags { get; }
  }
}