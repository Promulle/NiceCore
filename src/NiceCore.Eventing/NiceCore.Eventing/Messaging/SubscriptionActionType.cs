﻿namespace NiceCore.Eventing.Messaging
{
  /// <summary>
  /// Enum for possible actions taken with any subscription
  /// </summary>
  public enum SubscriptionActionType
  {
    /// <summary>
    /// A new subscription was added
    /// </summary>
    Added,

    /// <summary>
    /// A subscription has been removed
    /// </summary>
    Removed,
  }
}
