namespace NiceCore.Eventing.Messaging.Parameters
{
  /// <summary>
  /// Scope under which the message should be sent
  /// </summary>
  public interface IMessageSendingParameters
  {
  }
}