using NiceCore.Eventing.Messaging.Filtering;

namespace NiceCore.Eventing.Messaging.Parameters
{
  /// <summary>
  /// Interface for factories that create certain data needed for distribution from the passed in parameters
  /// </summary>
  public interface IMessageSendingParameterBasedFactory
  {
    /// <summary>
    /// Checks if this factory should be used to create parameters for parameters
    /// </summary>
    /// <param name="i_Parameters"></param>
    /// <returns></returns>
    bool CanCreateInstancesFor(IMessageSendingParameters i_Parameters);
    
    /// <summary>
    /// Create the parameter used for filtering from the passed parameters
    /// </summary>
    /// <param name="i_Parameters"></param>
    /// <returns></returns>
    IMessageInvocationReceiverFilterParameter CreateFilterParameter(IMessageSendingParameters i_Parameters);
  }
}