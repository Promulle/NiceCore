using NiceCore.Eventing.Messaging.Filtering;
using NiceCore.Eventing.Messaging.Filtering.Impl;
using NiceCore.ServiceLocation;

namespace NiceCore.Eventing.Messaging.Parameters.Impl
{
  /// <summary>
  /// Priority message sending parameter based factory
  /// </summary>
  [Register(InterfaceType = typeof(IMessageSendingParameterBasedFactory))]
  public class DefaultMessageSendingParameterBasedFactory : IMessageSendingParameterBasedFactory
  {
    /// <summary>
    /// Check if the factory can be used to create an instance for i_Parameters
    /// </summary>
    /// <param name="i_Parameters"></param>
    /// <returns></returns>
    public bool CanCreateInstancesFor(IMessageSendingParameters i_Parameters)
    {
      return i_Parameters is DefaultMessageSendingParameters;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="i_Parameters"></param>
    /// <returns></returns>
    public IMessageInvocationReceiverFilterParameter CreateFilterParameter(IMessageSendingParameters i_Parameters)
    {
      if (i_Parameters is DefaultMessageSendingParameters defaultParameters)
      {
        return new DefaultMessageInvocationReceiverFilterRequirement()
        {
          PriorityName = defaultParameters.PriorityName,
          ExcludedTags = defaultParameters.ExcludedTags,
          RequiredTags = defaultParameters.RequiredTags
        };
      }
      return null;
    }
  }
}