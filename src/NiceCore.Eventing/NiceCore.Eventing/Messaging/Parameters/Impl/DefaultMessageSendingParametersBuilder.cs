using System;
using System.Collections.Generic;
using System.Linq;

namespace NiceCore.Eventing.Messaging.Parameters.Impl
{
  /// <summary>
  /// Default Message Sending Parameters Builder
  /// </summary>
  public sealed class DefaultMessageSendingParametersBuilder
  {
    private string m_PriorityName;
    private List<string> m_ExcludedTags;
    private List<string> m_RequiredTags;

    /// <summary>
    /// Exclude Tags
    /// </summary>
    /// <param name="i_Tag"></param>
    /// <returns></returns>
    public DefaultMessageSendingParametersBuilder ExcludeTag(string i_Tag)
    {
      m_ExcludedTags ??= new();
      m_ExcludedTags.Add(i_Tag);
      return this;
    }

    /// <summary>
    /// Excludes tags, removes prev. added exclusion tags
    /// </summary>
    /// <param name="i_Tags"></param>
    /// <returns></returns>
    public DefaultMessageSendingParametersBuilder ExcludeTags(IEnumerable<string> i_Tags)
    {
      m_ExcludedTags = i_Tags.ToList();
      return this;
    }
    
    /// <summary>
    /// Add Reuqired Tags, the target will have to have this tag to receive the message
    /// </summary>
    /// <param name="i_Tag"></param>
    /// <returns></returns>
    public DefaultMessageSendingParametersBuilder RequireTag(string i_Tag)
    {
      m_RequiredTags ??= new();
      m_RequiredTags.Add(i_Tag);
      return this;
    }

    /// <summary>
    /// Sets Required tags, removes prev. added required tags.
    /// the target will have to have all these tags to receive the message
    /// </summary>
    /// <param name="i_Tags"></param>
    /// <returns></returns>
    public DefaultMessageSendingParametersBuilder RequireTags(IEnumerable<string> i_Tags)
    {
      m_RequiredTags = i_Tags.ToList();
      return this;
    }
    
    /// <summary>
    /// With Priority
    /// </summary>
    /// <param name="i_PriorityName"></param>
    /// <returns></returns>
    public DefaultMessageSendingParametersBuilder WithPriority(string i_PriorityName)
    {
      m_PriorityName = i_PriorityName;
      return this;
    }
    
    /// <summary>
    /// Build
    /// </summary>
    /// <returns></returns>
    public DefaultMessageSendingParameters Build()
    {
      return new()
      {
        ExcludedTags = GetTagCollection(m_ExcludedTags),
        RequiredTags = GetTagCollection(m_RequiredTags),
        PriorityName = m_PriorityName
      };
    }

    private static IReadOnlyCollection<string> GetTagCollection(IReadOnlyCollection<string> i_List)
    {
      if (i_List != null)
      {
        return i_List;
      }

      return Array.Empty<string>();
    }
  }
}