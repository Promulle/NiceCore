using System.Collections.Generic;

namespace NiceCore.Eventing.Messaging.Parameters.Impl
{
  /// <summary>
  /// parameters used for sending the priority as parameter
  /// </summary>
  public class DefaultMessageSendingParameters : IMessageSendingParameters
  {
    /// <summary>
    /// Priority name
    /// </summary>
    public string PriorityName { get; init; }
    
    /// <summary>
    /// Excluded Tags
    /// </summary>
    public IReadOnlyCollection<string> ExcludedTags { get; init; }
    
    /// <summary>
    /// Exclusive Tags
    /// </summary>
    public IReadOnlyCollection<string> RequiredTags { get; init; }

    /// <summary>
    /// New
    /// </summary>
    /// <returns></returns>
    public static DefaultMessageSendingParametersBuilder New()
    {
      return new();
    }
  }
}