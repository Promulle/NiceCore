using System;
using Microsoft.Extensions.Logging;
using NiceCore.Eventing.Messages;
using NiceCore.Logging.Extensions;
using Polly;

namespace NiceCore.Eventing.Messaging.Utils
{
  /// <summary>
  /// Message utils
  /// </summary>
  internal static class MessageUtils
  {
    internal static string GetMessageTopicForMessageType(Type i_MessageType)
    {
      return (Activator.CreateInstance(i_MessageType) as IObjectMessage).MessageTopic;
    }

    internal static Func<Action<TMessage>, ISyncPolicy> CreateDefaultSupplier<TMessage, TValue>(ILogger i_Log, TMessage i_Message) where TMessage : class, IMessage<TValue>
    {
      return castedMethod => Policy.Handle<Exception>(ex =>
      {
        i_Log.Error(ex, $"Error processing message {i_Message.Value.Identifier} in method {castedMethod.Method.DeclaringType.Name}.{castedMethod.Method.Name}");
        return true;
      }).Fallback(() => { });
    }
  }
}