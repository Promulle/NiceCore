﻿using NiceCore.Base.Services;
using NiceCore.Eventing.Messages;
using NiceCore.Eventing.Messaging.Receiver;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Eventing.Messaging.Parameters;
using NiceCore.Eventing.Messaging.Receiver.Priority;
using NiceCore.Eventing.Messaging.Subscriptions;

namespace NiceCore.Eventing.Messaging
{
  /// <summary>
  /// Message Service
  /// </summary>
  public interface IMessageService : IService
  {
    /// <summary>
    /// Logger
    /// </summary>
    ILogger<IMessageService> Logger { get; }

    /// <summary>
    /// Registers a message receiver.
    /// </summary>
    /// <param name="i_Receiver"></param>
    MessageSubscription RegisterReceiver(IMessageInvocationReceiver i_Receiver);

    /// <summary>
    /// returns the priority store this message service is using
    /// </summary>
    /// <returns></returns>
    IMessageInvocationReceiverPriorityStore GetPriorityStore();

    /// <summary>
    /// Sends a message and starts processing of all registered methods for this message type
    /// Non generic variant. This still uses the actual Type of i_Message to determine correct functions to run
    /// </summary>
    /// <param name="i_Message"></param>
    /// <param name="i_Parameters"></param>
    /// <param name="i_SendToRemote"></param>
    Task SendMessage(IObjectMessage i_Message, IMessageSendingParameters i_Parameters, bool i_SendToRemote);

    /// <summary>
    /// Cleares all registrations
    /// </summary>
    void Clear();
  }
}
