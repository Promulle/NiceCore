﻿using System;

namespace NiceCore.Eventing.Messaging
{
  /// <summary>
  /// Args for events related to subscriptions
  /// </summary>
  public class SubscriptionArgs
  {
    /// <summary>
    /// Topic of the subscription
    /// </summary>
    public string Topic { get; init; }

    /// <summary>
    /// Type of the message
    /// </summary>
    public Type MessageType { get; init; }

    /// <summary>
    /// Kind of change
    /// </summary>
    public SubscriptionActionType Type { get; init; }

    /// <summary>
    /// Count of subscriptions after this Action has been taken
    /// </summary>
    public uint NewSubscriptionCount { get; init; }
  }
}
