using System.Collections.Generic;
using System.Threading.Tasks;
using NiceCore.Eventing.Messages;
using NiceCore.Eventing.Messaging.Parameters;
using NiceCore.Eventing.Messaging.Receiver;

namespace NiceCore.Eventing.Messaging
{
  /// <summary>
  /// Model that is used to send messages
  /// </summary>
  public interface IMessagingSendModel
  {
    /// <summary>
    /// Send message
    /// </summary>
    /// <param name="i_Message"></param>
    /// <param name="i_Parameter"></param>
    /// <param name="i_Receivers"></param>
    /// <returns></returns>
    Task SendMessage(IObjectMessage i_Message, IMessageSendingParameters i_Parameter, IEnumerable<IMessageInvocationReceiver> i_Receivers);
  }
}