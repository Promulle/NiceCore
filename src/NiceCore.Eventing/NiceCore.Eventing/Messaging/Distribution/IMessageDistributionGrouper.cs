using System.Collections.Generic;
using NiceCore.Eventing.Messaging.Receiver;

namespace NiceCore.Eventing.Messaging.Distribution
{
  /// <summary>
  /// service that groups receivers for a message according to requirements
  /// </summary>
  public interface IMessageDistributionGrouper
  {
    /// <summary>
    /// Group by receivers
    /// </summary>
    /// <param name="i_Receivers"></param>
    /// <returns></returns>
    IReadOnlyCollection<MessageDistributionGroup> GroupByRequirement(IEnumerable<IMessageInvocationReceiver> i_Receivers);
  }
}