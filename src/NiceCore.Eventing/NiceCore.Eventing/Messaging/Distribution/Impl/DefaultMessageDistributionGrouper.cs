using System.Collections.Generic;
using System.Linq;
using NiceCore.Eventing.Messaging.Receiver;
using NiceCore.ServiceLocation;

namespace NiceCore.Eventing.Messaging.Distribution.Impl
{
  /// <summary>
  /// Grouper default impl
  /// </summary>
  [Register(InterfaceType = typeof(IMessageDistributionGrouper))]
  public class DefaultMessageDistributionGrouper : IMessageDistributionGrouper
  {
    /// <summary>
    /// Groups i_Receivers by requirement
    /// </summary>
    /// <param name="i_Receivers"></param>
    /// <returns></returns>
    public IReadOnlyCollection<MessageDistributionGroup> GroupByRequirement(IEnumerable<IMessageInvocationReceiver> i_Receivers)
    {
      var groups = new Dictionary<string, MessageDistributionGroup>();
      foreach (var receiver in i_Receivers)
      {
        var group = GetMessageDistributionGroup(groups, receiver);
        group.Receivers.Add(receiver);
      }
      return groups.Values;
    }

    private static MessageDistributionGroup GetMessageDistributionGroup(IDictionary<string, MessageDistributionGroup> t_ExistingGroups, IMessageInvocationReceiver i_Receiver)
    {
      var key = i_Receiver.Descriptor.Requirement != null ? i_Receiver.Descriptor.Requirement.GetType().FullName : "default";
      if (t_ExistingGroups.TryGetValue(key!, out var group))
      {
        return group;
      }

      var newGroup = new MessageDistributionGroup()
      {
        Receivers = new(),
        RequirementType = i_Receiver.Descriptor.Requirement?.GetType(),
      };
      t_ExistingGroups[key] = newGroup;
      return newGroup;
    }
  }
}