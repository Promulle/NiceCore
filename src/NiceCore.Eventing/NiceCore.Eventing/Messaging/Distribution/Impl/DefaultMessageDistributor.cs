using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Diagnostics;
using NiceCore.Eventing.Messages;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation;

namespace NiceCore.Eventing.Messaging.Distribution.Impl
{
  /// <summary>
  /// Default impl for IMessageDistributor. Used if no other distributors were matched by the requirements
  /// </summary>
  [Register(InterfaceType = typeof(IMessageDistributor))]
  public class DefaultMessageDistributor : IMessageDistributor
  {
    /// <summary>
    /// Requirement type does not matter here so = null
    /// </summary>
    public Type RequirementType { get; } = null;

    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<DefaultMessageDistributor> Logger { get; set; } = DiagnosticsCollector.Instance.GetLogger<DefaultMessageDistributor>();

    /// <summary>
    /// Call receivers with message
    /// </summary>
    /// <param name="i_Message"></param>
    /// <param name="i_Group"></param>
    /// <returns></returns>
    public Task Distribute(IObjectMessage i_Message, MessageDistributionGroup i_Group)
    {
      List<Task> tasks = new();
      Logger.Trace($"Distributing message of type {i_Message.GetType().FullName} to {i_Group.Receivers.Count} receivers");
      foreach (var messageInvocationReceiver in i_Group.Receivers)
      {
        Logger.Trace($"Distributing message of type {i_Message.GetType().FullName} to receiver {messageInvocationReceiver.GetType().FullName}");
        tasks.Add(messageInvocationReceiver.Receive(i_Message));
      }
      return Task.WhenAll(tasks);
    }
  }
}