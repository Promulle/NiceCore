using System;
using System.Collections.Generic;
using NiceCore.Eventing.Messaging.Receiver;

namespace NiceCore.Eventing.Messaging.Distribution
{
  /// <summary>
  /// A single group to be distributed by the distributor
  /// </summary>
  public class MessageDistributionGroup
  {
    /// <summary>
    /// Receivers
    /// </summary>
    public List<IMessageInvocationReceiver> Receivers { get; init; } = new();
    
    /// <summary>
    /// Type of the requirement the receivers have been grouped by, or null if it is the default grouping
    /// </summary>
    public Type RequirementType { get; init; }
  }
}