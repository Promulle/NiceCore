using System;
using System.Threading.Tasks;
using NiceCore.Eventing.Messages;

namespace NiceCore.Eventing.Messaging.Distribution
{
  /// <summary>
  /// Interface for message distributors
  /// </summary>
  public interface IMessageDistributor
  {
    /// <summary>
    /// Type of requirement this distributor should be used for
    /// </summary>
    Type RequirementType { get; }

    /// <summary>
    /// Distribute 
    /// </summary>
    /// <param name="i_Message">Message to be distributed</param>
    /// <param name="i_Group">group of receivers to distribute the message to</param>
    /// <returns></returns>
    Task Distribute(IObjectMessage i_Message, MessageDistributionGroup i_Group);
  }
}