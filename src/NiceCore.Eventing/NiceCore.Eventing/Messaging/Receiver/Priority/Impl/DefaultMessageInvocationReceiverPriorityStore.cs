using System.Collections.Concurrent;
using System.Collections.Generic;
using NiceCore.Base;
using NiceCore.ServiceLocation;

namespace NiceCore.Eventing.Messaging.Receiver.Priority.Impl
{
  /// <summary>
  /// Default impl for IMessageInvocationReceiver Priority store
  /// </summary>
  [InitializeOnResolve]
  [Register(InterfaceType = typeof(IMessageInvocationReceiverPriorityStore), LifeStyle = LifeStyles.Singleton)]
  public class DefaultMessageInvocationReceiverPriorityStore : IMessageInvocationReceiverPriorityStore, IInitializable
  {
    private readonly ConcurrentDictionary<string, MessageInvocationReceiverPriority> m_Priorities = new();

    /// <summary>
    /// Message Invocation receiver priority provider
    /// </summary>
    public IMessageInvocationReceiverPriorityProvider MessageInvocationReceiverPriorityProvider { get; set; }
    
    /// <summary>
    /// Is initialized
    /// </summary>
    public bool IsInitialized { get; private set; }
    
    /// <summary>
    /// Returns the priority that is saved for i_Name. Null if none is
    /// </summary>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    public MessageInvocationReceiverPriority GetPriorityFor(string i_Name)
    {
      return m_Priorities.GetValueOrDefault(i_Name);
    }

    /// <summary>
    /// Add priority
    /// </summary>
    /// <param name="i_Priority"></param>
    public void AddPriority(MessageInvocationReceiverPriority i_Priority)
    {
      m_Priorities[i_Priority.Name] = i_Priority;
    }

    /// <summary>
    /// Initialize with the provided priorities
    /// </summary>
    public void Initialize()
    {
      if (!IsInitialized)
      {
        var priorities = MessageInvocationReceiverPriorityProvider?.GetPriorities();
        if (priorities != null)
        {
          foreach (var priority in priorities)
          {
            AddPriority(priority);
          }
        }

        IsInitialized = true;
      }
    }
  }
}