using System.Collections.Generic;

namespace NiceCore.Eventing.Messaging.Receiver.Priority
{
  /// <summary>
  /// Message invocation receiver priority
  /// </summary>
  public interface IMessageInvocationReceiverPriorityProvider
  {
    /// <summary>
    /// Get Priorities
    /// </summary>
    /// <returns></returns>
    IEnumerable<MessageInvocationReceiverPriority> GetPriorities();
  }
}