namespace NiceCore.Eventing.Messaging.Receiver.Priority
{
  /// <summary>
  /// Message invocation receiver priority store
  /// </summary>
  public interface IMessageInvocationReceiverPriorityStore
  {
    /// <summary>
    /// Returns the priority that can be identified by i_Name
    /// </summary>
    /// <param name="i_Name"></param>
    /// <returns></returns>
    MessageInvocationReceiverPriority GetPriorityFor(string i_Name);

    /// <summary>
    /// Add a priority
    /// </summary>
    void AddPriority(MessageInvocationReceiverPriority i_Priority);
  }
}