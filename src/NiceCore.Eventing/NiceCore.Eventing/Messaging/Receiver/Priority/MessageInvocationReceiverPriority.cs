namespace NiceCore.Eventing.Messaging.Receiver.Priority
{
  /// <summary>
  /// Model for priorities that can be assigned to receivers
  /// </summary>
  public class MessageInvocationReceiverPriority
  {
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; init; }
    
    /// <summary>
    /// Priority
    /// </summary>
    public int Priority { get; init; }
  }
}