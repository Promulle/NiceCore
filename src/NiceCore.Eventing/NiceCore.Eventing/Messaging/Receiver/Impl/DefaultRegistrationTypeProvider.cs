using System;
using System.Collections.Generic;

namespace NiceCore.Eventing.Messaging.Receiver.Impl
{
  /// <summary>
  /// Default impl for Registration Type Provider
  /// </summary>
  public class DefaultRegistrationTypeProvider : IReceiverRegistrationTypeProvider
  {
    /// <summary>
    /// Get Types To Register Receiver
    /// </summary>
    /// <param name="i_Descriptor"></param>
    /// <returns>i_Descriptor.MessageType</returns>
    public IReadOnlyCollection<Type> GetTypesToRegisterReceiver(MessageInvocationReceiverDescriptor i_Descriptor)
    {
      return MessageTypeAsList(i_Descriptor);
    }
    
    /// <summary>
    /// Get Types To Register Receiver
    /// </summary>
    /// <param name="i_Descriptor"></param>
    /// <returns>i_Descriptor.MessageType</returns>
    public static IReadOnlyCollection<Type> MessageTypeAsList(MessageInvocationReceiverDescriptor i_Descriptor)
    {
      return new[] {i_Descriptor.RegisterMessageType};
    }
  }
}