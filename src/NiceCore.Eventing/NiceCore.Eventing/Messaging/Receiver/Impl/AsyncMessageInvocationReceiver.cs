﻿using NiceCore.Eventing.Messages;
using NiceCore.Eventing.Messaging.Receiver.Base;
using Polly;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace NiceCore.Eventing.Messaging.Receiver.Impl
{
  /// <summary>
  /// Async message invocation receiver
  /// </summary>
  /// <typeparam name="TMessage"></typeparam>
  /// <typeparam name="TValue"></typeparam>
  public class AsyncMessageInvocationReceiver<TMessage, TValue> : BaseAsyncMessageInvocationReceiver<TMessage, Func<TMessage, Task>> where TMessage : class, IMessage<TValue>
  {
    /// <summary>
    /// Constructor filling function
    /// </summary>
    /// <param name="i_Descriptor"></param>
    /// <param name="i_MessageReceiveFunc"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Logger"></param>
    public AsyncMessageInvocationReceiver(MessageInvocationReceiverDescriptor i_Descriptor,
      Func<TMessage, Task> i_MessageReceiveFunc,
      IAsyncPolicy i_Policy,
      ILogger i_Logger) : base(i_MessageReceiveFunc,
      i_Descriptor,
      i_Policy,
      i_Logger)
    {
    }

    /// <summary>
    /// Execute message action
    /// </summary>
    /// <param name="i_Method"></param>
    /// <param name="i_Message"></param>
    /// <returns></returns>
    protected override Task ExecuteMessageAction(Func<TMessage, Task> i_Method, TMessage i_Message)
    {
      return i_Method.Invoke(i_Message);
    }
  }
}
