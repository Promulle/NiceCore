using System;
using System.Collections.Generic;

namespace NiceCore.Eventing.Messaging.Receiver
{
  /// <summary>
  /// Default impl for Registration Type Provider
  /// </summary>
  public interface IReceiverRegistrationTypeProvider
  {
    /// <summary>
    /// Get Types To Register Receiver
    /// </summary>
    /// <param name="i_Descriptor"></param>
    /// <returns></returns>
    IReadOnlyCollection<Type> GetTypesToRegisterReceiver(MessageInvocationReceiverDescriptor i_Descriptor);
  }
}