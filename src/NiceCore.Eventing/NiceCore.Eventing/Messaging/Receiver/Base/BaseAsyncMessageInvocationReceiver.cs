﻿using NiceCore.Eventing.Messages;
using NiceCore.Logging;
using Polly;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Logging.Extensions;

namespace NiceCore.Eventing.Messaging.Receiver.Base
{
  /// <summary>
  /// Base receiver for all async receivers
  /// </summary>
  /// <typeparam name="TMessage"></typeparam>
  /// <typeparam name="TFunc"></typeparam>
  public abstract class BaseAsyncMessageInvocationReceiver<TMessage, TFunc> : BaseMessageInvocationReceiver<TMessage, IAsyncPolicy>
    where TMessage : class, IObjectMessage
  {
    private readonly IAsyncPolicy m_AsyncPolicy;
    private readonly TFunc m_MessageFunc;

    /// <summary>
    /// Constructor filling policy
    /// </summary>
    /// <param name="i_Method"></param>
    /// <param name="i_Descriptor"></param>
    /// <param name="i_Policy"></param>
    /// <param name="i_Logger"></param>
    protected BaseAsyncMessageInvocationReceiver(TFunc i_Method,
      MessageInvocationReceiverDescriptor i_Descriptor,
      IAsyncPolicy i_Policy, ILogger i_Logger) : base(i_Descriptor, i_Logger)
    {
      m_MessageFunc = i_Method;
      m_AsyncPolicy = i_Policy;
    }

    /// <summary>
    /// Execute the message action
    /// </summary>
    /// <param name="i_Method"></param>
    /// <param name="i_Message"></param>
    protected abstract Task ExecuteMessageAction(TFunc i_Method, TMessage i_Message);

    /// <summary>
    /// Receive
    /// </summary>
    /// <param name="i_Message"></param>
    /// <param name="i_Policy"></param>
    protected override Task ReceiveInternal(TMessage i_Message, IAsyncPolicy i_Policy)
    {
      var policy = i_Policy ?? m_AsyncPolicy;
      if (policy != null)
      {
        return policy.ExecuteAsync(() => ExecuteMessageAction(m_MessageFunc, i_Message));
      }

      return ExecuteAsync(i_Message);
    }

    private async Task ExecuteAsync(TMessage i_Message)
    {
      try
      {
        await ExecuteMessageAction(m_MessageFunc, i_Message).ConfigureAwait(false);
      }
      catch (Exception ex)
      {
        Logger.Error(ex, "Error when processing message async.");
      }
    }
  }
}
