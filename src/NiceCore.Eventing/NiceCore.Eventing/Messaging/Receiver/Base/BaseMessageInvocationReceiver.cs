﻿using NiceCore.Eventing.Messages;
using NiceCore.Logging;
using Polly;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace NiceCore.Eventing.Messaging.Receiver.Base
{
  /// <summary>
  /// Base Message Invocation Receiver
  /// </summary>
  /// <typeparam name="TMessage"></typeparam>
  /// <typeparam name="TPolicy">Type of policy to use</typeparam>
  public abstract class BaseMessageInvocationReceiver<TMessage, TPolicy> : IMessageInvocationReceiver
    where TMessage : class, IObjectMessage
    where TPolicy : class, IsPolicy
  {
    /// <summary>
    /// Descriptor
    /// </summary>
    public MessageInvocationReceiverDescriptor Descriptor { get; }
    
    /// <summary>
    /// Logger
    /// </summary>
    public ILogger Logger { get; }

    /// <summary>
    /// Constructor filling message type
    /// </summary>
    /// <param name="i_Descriptor"></param>
    /// <param name="i_Logger"></param>
    protected BaseMessageInvocationReceiver(MessageInvocationReceiverDescriptor i_Descriptor, ILogger i_Logger)
    {
      Descriptor = i_Descriptor;
      Logger = i_Logger;
    }

    /// <summary>
    /// Checks whether i_Message should actually be processed by this receiver
    /// </summary>
    /// <param name="i_Message"></param>
    /// <returns></returns>
    public bool IsApplicable(IObjectMessage i_Message)
    {
      return true;
    }

    /// <summary>
    /// IsApplicable that can be overwritten and provides ability for inheriting classes to add custom conditions
    /// </summary>
    /// <param name="i_Message"></param>
    /// <returns></returns>
    protected virtual bool IsApplicableAdditional(TMessage i_Message)
    {
      return true;
    }

    /// <summary>
    /// Receive
    /// </summary>
    /// <param name="t_Message"></param>
    /// <returns></returns>
    public Task Receive(IObjectMessage t_Message)
    {
      return Receive(t_Message, null);
    }

    /// <summary>
    /// Receive
    /// </summary>
    /// <param name="t_Message"></param>
    /// <param name="i_Policy"></param>
    public Task Receive(IObjectMessage t_Message, IsPolicy i_Policy)
    {
      var policy = ConvertPolicy(i_Policy);
      return ReceiveInternal(t_Message as TMessage, policy);
    }

    private TPolicy ConvertPolicy(IsPolicy i_Policy)
    {
      if (i_Policy == null)
      {
        return null;
      }
      if (i_Policy is TPolicy casted)
      {
        return casted;
      }
      throw new InvalidOperationException($"MessageInvocationReceiver of Type {GetType().FullName} expects a passed policy to atleast implement type {typeof(TPolicy).FullName}.");
    }

    /// <summary>
    /// Receive Internal to be implemented by actual receivers
    /// </summary>
    /// <param name="i_Message"></param>
    /// <param name="i_Policy"></param>
    protected abstract Task ReceiveInternal(TMessage i_Message, TPolicy i_Policy);
  }
}
