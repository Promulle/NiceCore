namespace NiceCore.Eventing.Messaging.Receiver
{
  /// <summary>
  /// I Message Invocation receiver creator
  /// </summary>
  public interface IMessageInvocationReceiverCreator
  {
    /// <summary>
    /// Create receiver
    /// </summary>
    /// <returns></returns>
    IMessageInvocationReceiver CreateReceiver(MessageInvocationReceiverDescriptor i_Descriptor);
  }
}