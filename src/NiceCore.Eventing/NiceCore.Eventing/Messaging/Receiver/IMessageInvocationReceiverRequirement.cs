namespace NiceCore.Eventing.Messaging.Receiver
{
  /// <summary>
  /// Requirement a receiver can define. This determines which distributor is used to to invoke the receiver
  /// </summary>
  public interface IMessageInvocationReceiverRequirement
  {
  }
}