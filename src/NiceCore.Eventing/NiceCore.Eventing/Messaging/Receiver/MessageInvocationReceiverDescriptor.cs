using System;
using System.Collections.Generic;
using NiceCore.Collections;
using NiceCore.Eventing.Messaging.Receiver.Priority;

namespace NiceCore.Eventing.Messaging.Receiver
{
  /// <summary>
  /// Receiver descriptor
  /// </summary>
  public class MessageInvocationReceiverDescriptor
  {
    /// <summary>
    /// Message type this receiver deals with
    /// </summary>
    public Type HandlerMessageType { get; init; }
    
    /// <summary>
    /// Message type this receiver deals with
    /// </summary>
    public Type RegisterMessageType { get; init; }

    /// <summary>
    /// Priority
    /// </summary>
    public MessageInvocationReceiverPriority Priority { get; init; }
    
    /// <summary>
    /// Registration Type Provider - might be a singleton instance
    /// </summary>
    public IReceiverRegistrationTypeProvider ReceiverRegistrationTypeProvider { get; init; }

    /// <summary>
    /// Tags that have been applied to the receiver
    /// </summary>
    public IReadOnlySet<string> Tags { get; init; } = NcDefaultSets<string>.Empty;

    /// <summary>
    /// requirement
    /// </summary>
    public IMessageInvocationReceiverRequirement Requirement { get; init; }
  }
}