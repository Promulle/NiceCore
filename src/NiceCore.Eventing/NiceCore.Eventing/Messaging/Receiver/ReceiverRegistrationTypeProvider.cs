using NiceCore.Eventing.Messaging.Receiver.Impl;

namespace NiceCore.Eventing.Messaging.Receiver
{
  /// <summary>
  /// Receiver Registration Type Provider
  /// </summary>
  public static class ReceiverRegistrationTypeProvider
  {
    /// <summary>
    /// Default Registration Type Provider
    /// </summary>
    public static readonly IReceiverRegistrationTypeProvider DEFAULT = new DefaultRegistrationTypeProvider();
  }
}