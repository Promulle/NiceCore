using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Eventing.Messages;
using NiceCore.Eventing.Messaging.Receiver.Impl;
using Polly;

namespace NiceCore.Eventing.Messaging.Receiver.Creators
{
  /// <summary>
  /// Async Message Invocation receiver creator
  /// </summary>
  public class AsyncMessageInvocationReceiverCreator<TMessage, TValue> : IMessageInvocationReceiverCreator
    where TMessage: class, IMessage<TValue>
  {
    /// <summary>
    /// Func
    /// </summary>
    public Func<TMessage, Task> MessageReceiveFunc { get; init; }
    
    /// <summary>
    /// Policy
    /// </summary>
    public IAsyncPolicy Policy { get; init; }
    
    /// <summary>
    /// Logger
    /// </summary>
    public required ILogger Logger { get; init; }
    
    /// <summary>
    /// Creates the correct receiver
    /// </summary>
    /// <param name="i_Descriptor"></param>
    /// <returns></returns>
    public IMessageInvocationReceiver CreateReceiver(MessageInvocationReceiverDescriptor i_Descriptor)
    {
      return new AsyncMessageInvocationReceiver<TMessage, TValue>(i_Descriptor, MessageReceiveFunc, Policy, Logger);
    }
  }
}