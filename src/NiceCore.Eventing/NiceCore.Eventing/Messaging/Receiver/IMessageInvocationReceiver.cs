﻿using NiceCore.Eventing.Messages;
using Polly;
using System.Threading.Tasks;

namespace NiceCore.Eventing.Messaging.Receiver
{
  /// <summary>
  /// Receiver that receives sent messages
  /// </summary>
  public interface IMessageInvocationReceiver
  {
    /// <summary>
    /// Descriptor with information about the receiver
    /// </summary>
    MessageInvocationReceiverDescriptor Descriptor { get; }

    /// <summary>
    /// Check whether this receiver should actually be called for i_Message
    /// </summary>
    /// <param name="i_Message"></param>
    /// <returns></returns>
    bool IsApplicable(IObjectMessage i_Message);

    /// <summary>
    /// Receive that is called if IsApplicable is true
    /// </summary>
    /// <param name="t_Message"></param>
    Task Receive(IObjectMessage t_Message);

    /// <summary>
    /// Receive that is called if IsApplicable is true
    /// </summary>
    /// <param name="t_Message"></param>
    /// <param name="i_Policy">Policy used in case something goes wrong.</param>
    Task Receive(IObjectMessage t_Message, IsPolicy i_Policy);
  }
}
