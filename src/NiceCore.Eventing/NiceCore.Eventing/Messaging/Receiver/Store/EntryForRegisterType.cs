using System;

namespace NiceCore.Eventing.Messaging.Receiver.Store
{
  internal class EntryForRegisterType
  {
    /// <summary>
    /// Position in list
    /// </summary>
    internal required MessageServiceReceiverStoreEntry EntryInList { get; init; }
    
    /// <summary>
    /// Type the entry is registered for
    /// </summary>
    internal required Type RegisterType { get; init; }

    public void Deconstruct(out MessageServiceReceiverStoreEntry o_Entry, out Type o_RegisteredType)
    {
      o_Entry = EntryInList;
      o_RegisteredType = RegisterType;
    }
  }
}