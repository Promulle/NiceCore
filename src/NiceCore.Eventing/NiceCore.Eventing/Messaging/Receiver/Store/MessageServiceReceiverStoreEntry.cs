﻿namespace NiceCore.Eventing.Messaging.Receiver.Store
{
  /// <summary>
  /// Entry for a registered receiver, containing the handler it was registered with
  /// </summary>
  public class MessageServiceReceiverStoreEntry
  {
    /// <summary>
    /// Receiver
    /// </summary>
    public IMessageInvocationReceiver Receiver { get; init; }
  }
}
