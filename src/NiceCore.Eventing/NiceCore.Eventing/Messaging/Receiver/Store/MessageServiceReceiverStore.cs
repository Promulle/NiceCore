using System;
using System.Collections.Generic;
using System.Threading;
using NiceCore.Eventing.Messages;
using NiceCore.Eventing.Messaging.Receiver.Impl;
using NiceCore.Eventing.Messaging.Subscriptions;
using NiceCore.Eventing.Messaging.Utils;

namespace NiceCore.Eventing.Messaging.Receiver.Store
{
  /// <summary>
  /// Store keeping receivers
  /// </summary>
  internal class MessageServiceReceiverStore
  {
    private readonly ReaderWriterLockSlim                                     m_Lock               = new();
    private readonly Dictionary<Type, List<MessageServiceReceiverStoreEntry>> m_ReceiverDictionary = new();
    private          IMessageServiceToEventingAdapterBridge                   m_Bridge;

    public void SetAdapterBridge(IMessageServiceToEventingAdapterBridge i_Bridge)
    {
      m_Bridge = i_Bridge;
    }

    /// <summary>
    /// returns all receivers that are used for i_Message
    /// </summary>
    /// <param name="i_Message"></param>
    /// <returns></returns>
    internal IReadOnlyCollection<IMessageInvocationReceiver> GetReceiversFor(IObjectMessage i_Message)
    {
      try
      {
        m_Lock.EnterReadLock();
        if (m_ReceiverDictionary.TryGetValue(i_Message.GetType(), out var receiverList))
        {
          var res = new List<IMessageInvocationReceiver>(receiverList.Count);
          foreach (var receiver in receiverList)
          {
            if (receiver.Receiver.IsApplicable(i_Message))
            {
              res.Add(receiver.Receiver);
            }
          }

          return res;
        }

        return Array.Empty<IMessageInvocationReceiver>();
      }
      finally
      {
        m_Lock.ExitReadLock();
      }
    }

    /// <summary>
    /// Add a receiver to the store
    /// </summary>
    /// <param name="i_MessageType"></param>
    /// <param name="i_Receiver"></param>
    internal MessageSubscription AddReceiver(Type i_MessageType, IMessageInvocationReceiver i_Receiver)
    {
      try
      {
        m_Lock.EnterWriteLock();
        var typesToRegisterAs = GetTypesToRegisterReceiverAs(i_Receiver);
        var entryPairs        = new List<EntryForRegisterType>();
        foreach (var registerType in typesToRegisterAs)
        {
          if (!CanBeRegistered(i_Receiver.Descriptor.HandlerMessageType, registerType))
          {
            throw new InvalidOperationException($"Could not register message handler that takes {i_Receiver.Descriptor.HandlerMessageType} as parameter. {registerType} must be convertible to {i_Receiver.Descriptor.HandlerMessageType}.");
          }

          if (!m_ReceiverDictionary.TryGetValue(registerType, out var list))
          {
            list                               = new();
            m_ReceiverDictionary[registerType] = list;
          }

          var entry = new MessageServiceReceiverStoreEntry()
          {
            Receiver = i_Receiver
          };
          list.Add(entry);
          entryPairs.Add(new()
          {
            EntryInList  = entry,
            RegisterType = registerType
          });
          m_Bridge?.SubscribeToRemote(MessageUtils.GetMessageTopicForMessageType(registerType), registerType);
        }

        return new(this, new()
        {
          RegisteredEntries = entryPairs
        });
      }
      finally
      {
        m_Lock.ExitWriteLock();
      }
    }

    private static bool CanBeRegistered(Type i_HandlerMessageType, Type i_RegisterMessageType)
    {
      if (i_RegisterMessageType == i_HandlerMessageType)
      {
        return true;
      }

      return i_HandlerMessageType.IsAssignableFrom(i_RegisterMessageType);
    }

    private static IReadOnlyCollection<Type> GetTypesToRegisterReceiverAs(IMessageInvocationReceiver i_Receiver)
    {
      if (i_Receiver.Descriptor.ReceiverRegistrationTypeProvider != null)
      {
        return i_Receiver.Descriptor.ReceiverRegistrationTypeProvider.GetTypesToRegisterReceiver(i_Receiver.Descriptor);
      }

      return DefaultRegistrationTypeProvider.MessageTypeAsList(i_Receiver.Descriptor);
    }

    internal void Unsubscribe(MessageServiceReceiverStoreInternalKey i_Key)
    {
      try
      {
        m_Lock.EnterWriteLock();
        foreach (var (entry, registeredType) in i_Key.RegisteredEntries)
        {
          if (m_ReceiverDictionary.TryGetValue(registeredType, out var list))
          {
            list.Remove(entry);
            //after the listener was removed, nobody is listening for the message type anymore -> unsubscribe from remote
            if (list.Count == 0)
            {
              m_Bridge?.UnsubscribeFromRemote(MessageUtils.GetMessageTopicForMessageType(registeredType));
            }
          }
        }
      }
      finally
      {
        m_Lock.ExitWriteLock();
      }
    }

    /// <summary>
    /// Cleares all registrations
    /// </summary>
    internal void Clear()
    {
      try
      {
        m_Lock.EnterWriteLock();
        m_ReceiverDictionary.Clear();
      }
      finally
      {
        m_Lock.ExitWriteLock();
      }
    }

    /// <summary>
    /// Returns the count for registrations of a certain message type
    /// </summary>
    /// <typeparam name="TMessage"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <returns></returns>
    public uint CountRegistrations<TMessage, TValue>() where TMessage : class, IMessage<TValue>
    {
      return CountRegistrations(typeof(TMessage));
    }

    /// <summary>
    /// Returns the count for registrations of a certain message type
    /// </summary>
    /// <returns></returns>
    public uint CountRegistrations(Type i_MessageType)
    {
      try
      {
        m_Lock.EnterReadLock();
        if (m_ReceiverDictionary.TryGetValue(i_MessageType, out var list))
        {
          return (uint) list.Count;
        }

        return 0;
      }
      finally
      {
        m_Lock.ExitReadLock();
      }
    }
  }
}
