using System.Collections.Generic;

namespace NiceCore.Eventing.Messaging.Receiver.Store
{
  /// <summary>
  /// Key that makes traversing the dictionary faster
  /// </summary>
  internal class MessageServiceReceiverStoreInternalKey
  {
    /// <summary>
    /// Position in list
    /// </summary>
    internal required IReadOnlyCollection<EntryForRegisterType> RegisteredEntries { get; init; }
  }
}