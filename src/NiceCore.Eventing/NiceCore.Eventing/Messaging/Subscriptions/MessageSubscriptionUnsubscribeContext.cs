using System.Collections.Generic;

namespace NiceCore.Eventing.Messaging.Subscriptions
{
  /// <summary>
  /// Context making cancellation of multiple subscriptions easy
  /// </summary>
  public class MessageSubscriptionUnsubscribeContext
  {
    private readonly List<MessageSubscription> m_Subscriptions = new();

    /// <summary>
    /// Adds a subscription to this cancel context
    /// </summary>
    /// <param name="i_Subscription"></param>
    public void AddSubscription(MessageSubscription i_Subscription)
    {
      m_Subscriptions.Add(i_Subscription);
    }

    /// <summary>
    /// Unsubsc
    /// </summary>
    public void UnsubscribeAll()
    {
      foreach (var messageSubscription in m_Subscriptions)
      {
        messageSubscription.Unsubscribe();
      }
      m_Subscriptions.Clear();
    }
  }
}