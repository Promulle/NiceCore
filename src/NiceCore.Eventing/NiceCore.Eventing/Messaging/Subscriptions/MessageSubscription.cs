using NiceCore.Eventing.Messaging.Receiver.Store;

namespace NiceCore.Eventing.Messaging.Subscriptions
{
  /// <summary>
  /// Message Subscription
  /// </summary>
  public class MessageSubscription
  {
    private readonly MessageServiceReceiverStoreInternalKey m_SubscriptionKey;
    private readonly MessageServiceReceiverStore m_Store;
    
    /// <summary>
    /// Ctor
    /// </summary>
    internal MessageSubscription(MessageServiceReceiverStore i_Store, MessageServiceReceiverStoreInternalKey i_EntryKey)
    {
      m_Store = i_Store;
      m_SubscriptionKey = i_EntryKey;
    }

    /// <summary>
    /// Unsubscribe from the subscription, deletes the receiver from the messageService
    /// </summary>
    public void Unsubscribe()
    {
      m_Store.Unsubscribe(m_SubscriptionKey);
    }
  }
}