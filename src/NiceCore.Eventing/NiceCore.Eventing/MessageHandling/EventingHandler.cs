﻿namespace NiceCore.Eventing
{
  /// <summary>
  /// Static Helper class for creating event handler
  /// </summary>
  public static class EventingHandler
  {
    /// <summary>
    /// Create a new builder instance
    /// </summary>
    /// <typeparam name="TEventHandler"></typeparam>
    /// <returns></returns>
    public static EventingHandlerInitializationBuilder For<TEventHandler>() where TEventHandler : IEventingHandler
    {
      return new EventingHandlerInitializationBuilder(typeof(TEventHandler));
    }
  }
}
