﻿using System;

namespace NiceCore.Eventing
{
  /// <summary>
  /// Builder class for EventingHandlerInitialization
  /// </summary>
  public class EventingHandlerInitializationBuilder
  {
    private readonly Type m_Type;

    /// <summary>
    /// Ctor filling handler type
    /// </summary>
    /// <param name="i_HandlerType"></param>
    public EventingHandlerInitializationBuilder(Type i_HandlerType)
    {
      m_Type = i_HandlerType;
    }

    /// <summary>
    /// Supplies the config to be used and creates the eventingHandlerInitialization
    /// </summary>
    /// <param name="i_Config"></param>
    /// <returns></returns>
    public EventingHandlerInitialization ConfiguredBy(IHandlerConfig i_Config)
    {
      return new EventingHandlerInitialization()
      {
        HandlerConfig = i_Config,
        HandlerImplementationType = m_Type
      };
    }
  }
}
