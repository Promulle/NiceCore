﻿using NiceCore.Eventing.Messages;
using System;
using System.Threading.Tasks;
using NiceCore.Eventing.Messaging.Parameters;

namespace NiceCore.Eventing
{
  /// <summary>
  /// Handler that consumes/produces messages to/from the actual eventing server
  /// </summary>
  public interface IEventingHandler : IDisposable
  {
    /// <summary>
    /// Name of Handler impl
    /// </summary>
    string HandlerName { get; }
    
    /// <summary>
    /// Configuration for this handler
    /// </summary>
    IHandlerConfig HandlerConfiguration { get; set; }

    /// <summary>
    /// Links the Handler to the adapter using the given callback
    /// </summary>
    /// <param name="i_AdapterCallback"></param>
    void LinkWithAdapterCallback(Func<IObjectMessage, IMessageSendingParameters, Task> i_AdapterCallback);

    /// <summary>
    /// Sends a message
    /// </summary>
    /// <typeparam name="TMsg"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <param name="i_Message"></param>
    /// <param name="i_Parameters"></param>
    /// <returns></returns>
    Task Send<TMsg, TValue>(TMsg i_Message, IMessageSendingParameters i_Parameters) where TMsg : IMessage<TValue>;

    /// <summary>
    /// Sends a message
    /// </summary>
    /// <param name="i_Message"></param>
    /// <param name="i_Parameters"></param>
    /// <returns></returns>
    Task SendObjectMessage(IObjectMessage i_Message, IMessageSendingParameters i_Parameters);

    /// <summary>
    /// prepares for consuming for message of a topic
    /// </summary>
    /// <param name="i_Topic"></param>
    void Subscribe<TMsg, TValue>(string i_Topic) where TMsg : IMessage<TValue>;

    /// <summary>
    /// Not generic overload for subscribe
    /// </summary>
    /// <param name="i_Topic"></param>
    /// <param name="i_MessageType"></param>
    void Subscribe(string i_Topic, Type i_MessageType);

    /// <summary>
    /// Unsubscribes this handler from i_Topic
    /// </summary>
    /// <param name="i_Topic"></param>
    void Unsubscribe(string i_Topic);
  }
}
