﻿using System;

namespace NiceCore.Eventing
{
  /// <summary>
  /// Initialization for an instance of IEventingHandler
  /// </summary>
  public class EventingHandlerInitialization
  {
    /// <summary>
    /// Type for the handler implementation
    /// </summary>
    public Type HandlerImplementationType { get; init; }

    /// <summary>
    /// Handler Config
    /// </summary>
    public IHandlerConfig HandlerConfig { get; init; }
  }
}
