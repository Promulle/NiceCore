﻿namespace NiceCore.Eventing
{
  /// <summary>
  /// Default config for handlers
  /// </summary>
  public class DefaultHandlerConfig : IHandlerConfig
  {
    /// <summary>
    /// Ip/Hostname
    /// </summary>
    public string HostName { get; init; }
    
    /// <summary>
    /// Serializer Key
    /// </summary>
    public string SerializerKey { get; init; }

    /// <summary>
    /// Is Enabled
    /// </summary>
    public bool IsEnabled { get; init; }

    /// <summary>
    /// Port
    /// </summary>
    public string Port { get; init; }

    /// <summary>
    /// User Name
    /// </summary>
    public string UserName { get; init; }

    /// <summary>
    /// Password
    /// </summary>
    public string Password { get; init; }
  }
}
