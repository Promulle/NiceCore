namespace NiceCore.Eventing
{
  /// <summary>
  /// Provider enabling retrieval of config in code
  /// </summary>
  public interface IHandlerConfigProvider
  {
    /// <summary>
    /// Retrieves the config for a handler
    /// </summary>
    /// <param name="i_Handler"></param>
    /// <returns></returns>
    IHandlerConfig GetConfigForHandler<TConfig>(IEventingHandler i_Handler) where TConfig: IHandlerConfig;
  }
}