﻿using NiceCore.Eventing.Impl.Exceptions;
using NiceCore.Eventing.Messages;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Base.Services;
using NiceCore.Eventing.Messages.RemoteSerialization;
using NiceCore.Eventing.Messaging.Parameters;
using NiceCore.Logging.Extensions;

namespace NiceCore.Eventing.Impl
{
  /// <summary>
  /// Base eventing handler
  /// </summary>
  public abstract class BaseEventingHandler<TConfig> : BaseService, IEventingHandler where TConfig : class, IHandlerConfig
  {
    /// <summary>
    /// Callback invoked to do something on the adapter
    /// </summary>
    protected Func<IObjectMessage, IMessageSendingParameters, Task> m_AdapterCallback;
    
    /// <summary>
    /// Dictionary holding the correct message type for a topic
    /// </summary>
    protected readonly ConcurrentDictionary<string, Type> m_TopicToMessageTypeMap = new();

    /// <summary>
    /// Handler Name
    /// </summary>
    public abstract string HandlerName { get; }

    /// <summary>
    /// Handler Configuration
    /// </summary>
    public IHandlerConfig HandlerConfiguration { get; set; }
    
    /// <summary>
    /// Handler Config Provider
    /// </summary>
    public IHandlerConfigProvider HandlerConfigProvider { get; set; }
    
    /// <summary>
    /// Message Value Remote Serializer
    /// </summary>
    public IMessageValueRemoteSerializerService MessageValueRemoteSerializerService { get; set; }
    
    /// <summary>
    /// External Message Formatter
    /// </summary>
    public IExternalMessageFormatter ExternalMessageFormatter { get; set; }

    /// <summary>
    /// Logger
    /// </summary>
    public abstract ILogger Logger { get; }

    /// <summary>
    /// Internal Initialize
    /// </summary>
    protected override void InternalInitialize()
    {
      base.InternalInitialize();
      if (HandlerConfigProvider != null)
      {
        HandlerConfiguration = HandlerConfigProvider.GetConfigForHandler<TConfig>(this);
      }
    }

    /// <summary>
    /// Link with adapter callback
    /// </summary>
    /// <param name="i_AdapterCallback"></param>
    public void LinkWithAdapterCallback(Func<IObjectMessage, IMessageSendingParameters, Task> i_AdapterCallback)
    {
      m_AdapterCallback = i_AdapterCallback;
    }

    /// <summary>
    /// Send message to actual server
    /// </summary>
    /// <typeparam name="TMsg"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <param name="i_Message"></param>
    /// <param name="i_Parameters"></param>
    /// <returns></returns>
    public Task Send<TMsg, TValue>(TMsg i_Message, IMessageSendingParameters i_Parameters) where TMsg : IMessage<TValue>
    {
      return SendObjectMessage(i_Message, i_Parameters);
    }

    /// <summary>
    /// Send message to actual server
    /// </summary>
    /// <param name="i_Message"></param>
    /// <param name="i_Parameters"></param>
    /// <returns></returns>
    public async Task SendObjectMessage(IObjectMessage i_Message, IMessageSendingParameters i_Parameters)
    {
      if (ExternalMessageFormatter == null)
      {
        throw new InvalidOperationException($"Could not send message: No Instance for {nameof(ExternalMessageFormatter)} found");
      }
      if (IsConfigCorrect(HandlerConfiguration))
      {
        var val = await ExternalMessageFormatter.ToExternalData(i_Message, i_Parameters).ConfigureAwait(false);
        if (val != null && val.Length > 0)
        {
          await SendInternal(i_Message.MessageTopic, val).ConfigureAwait(false);
        }  
      }
    }

    /// <summary>
    /// Send method 
    /// </summary>
    /// <param name="i_Topic"></param>
    /// <param name="i_Data"></param>
    /// <returns></returns>
    protected abstract Task SendInternal(string i_Topic, byte[] i_Data);

    /// <summary>
    /// Subscribe to a certain msg topic
    /// </summary>
    /// <typeparam name="TMsg"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <param name="i_Topic"></param>
    public void Subscribe<TMsg, TValue>(string i_Topic) where TMsg : IMessage<TValue>
    {
      m_TopicToMessageTypeMap[i_Topic] = typeof(TMsg);
      SubscribeInternal<TMsg, TValue>(i_Topic);
    }

    /// <summary>
    /// Non generic overload for Subscribe
    /// </summary>
    /// <param name="i_Topic"></param>
    /// <param name="i_MessageType"></param>
    public void Subscribe(string i_Topic, Type i_MessageType)
    {
      m_TopicToMessageTypeMap[i_Topic] = i_MessageType;
      SubscribeInternal(i_Topic, i_MessageType);
    }

    /// <summary>
    /// Subscribe method implemented by the actual implementation
    /// </summary>
    /// <typeparam name="TMsg"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <param name="i_Topic"></param>
    protected virtual void SubscribeInternal<TMsg, TValue>(string i_Topic) where TMsg : IMessage<TValue>
    {
      SubscribeInternal(i_Topic, typeof(TMsg));
    }

    /// <summary>
    /// Non generic internal subscribe methop
    /// </summary>
    /// <param name="i_Topic"></param>
    /// <param name="i_MessageType"></param>
    protected abstract void SubscribeInternal(string i_Topic, Type i_MessageType);

    /// <summary>
    /// Makes sure that the config that is supplied to the handler is sufficient for the handler to work with
    /// </summary>
    /// <returns></returns>
    protected TConfig CheckSuppliedConfig()
    {
      if (HandlerConfiguration is TConfig)
      {
        return HandlerConfiguration as TConfig;
      }
      throw new IncorrectTypedConfigException(typeof(TConfig), this, HandlerConfiguration.GetType());
    }

    /// <summary>
    /// Checks config for correctness, should be a relatively inexpensive operation
    /// </summary>
    /// <param name="i_HandlerConfig"></param>
    /// <returns></returns>
    protected abstract bool IsConfigCorrect(IHandlerConfig i_HandlerConfig);

    /// <summary>
    /// Creates a message for i_Data
    /// </summary>
    /// <param name="i_Data"></param>
    /// <param name="i_Topic"></param>
    /// <returns></returns>
    protected ValueTask<FromExternalMessageResult> CreateMessageFor(ReadOnlyMemory<byte> i_Data, string i_Topic)
    {
      if (m_TopicToMessageTypeMap.TryGetValue(i_Topic, out var msgType))
      {
        if (msgType.IsAssignableTo(typeof(IObjectMessage)))
        {
          return CreateMessageFromTypeAndData(msgType, i_Data);
        }
        Logger.Error($"Type entry found for eventing topic {i_Topic}: {msgType.FullName}, but this type does not implement {typeof(IObjectMessage).FullName}. Message cannot be created becaue of this.");
      }
      Logger.Error($"Message could not be created from data for topic: {i_Topic}. Either there is no message type registered for the topic or there is another logging output with more details.");
      return ValueTask.FromResult(FromExternalMessageResult.Create(null, null));
    }

    /// <summary>
    /// Raises MessageReceived Event
    /// </summary>
    /// <param name="i_Message"></param>
    /// <param name="i_Parameters"></param>
    protected Task RaiseMessageReceived(IObjectMessage i_Message, IMessageSendingParameters i_Parameters)
    {
      if (m_AdapterCallback != null)
      {
        return m_AdapterCallback.Invoke(i_Message, i_Parameters);  
      }
      Logger.Warning("Message received by handler, but no adapter was connected. Message might be lost.");
      return Task.CompletedTask;
    }

    private async ValueTask<FromExternalMessageResult> CreateMessageFromTypeAndData(Type i_MessageType, ReadOnlyMemory<byte> i_Data)
    {
      if (ExternalMessageFormatter == null)
      {
        throw new InvalidOperationException($"Could not create message from received data: No Instance for {nameof(ExternalMessageFormatter)} found");
      }
      var msg = CreateMessageInstance(i_MessageType);
      if (msg != null)
      {
        msg.Origin = MessageOrigin.Remote;
        var (value, parameters) = await ExternalMessageFormatter.FromExternalData(i_Data, msg).ConfigureAwait(false);
        if (value != null)
        {
          msg.SetValue(value);
        }
        return FromExternalMessageResult.Create(msg, parameters);
      }
      return FromExternalMessageResult.Create(null, null);
    }

    private IObjectMessage CreateMessageInstance(Type i_MessageType)
    {
      try
      {
        return Activator.CreateInstance(i_MessageType) as IObjectMessage;
      }
      catch (MissingMethodException ex)
      {
        Logger.Error(ex, $"Message of type {i_MessageType.FullName} could not be created because the {i_MessageType.Name} does not contain a default constructor.");
      }
      catch (MethodAccessException ex)
      {
        Logger.Error(ex, $"Message of type {i_MessageType.FullName} could not be created because the default constructor could not be accessed.");
      }
      catch (Exception ex)
      {
        Logger.Error(ex, $"Message of type {i_MessageType.FullName} could not be created. See exception for more details.");
      }
      return null;
    }

    /// <summary>
    /// Unsubscribe from i_Topic
    /// </summary>
    /// <param name="i_Topic"></param>
    public abstract void Unsubscribe(string i_Topic);
  }
}
