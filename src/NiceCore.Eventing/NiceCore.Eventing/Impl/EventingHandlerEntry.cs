﻿namespace NiceCore.Eventing.Impl
{
  /// <summary>
  /// Eventing Handler Entry
  /// </summary>
  public sealed class EventingHandlerEntry
  {
    /// <summary>
    /// Handler
    /// </summary>
    public IEventingHandler Handler { get; init; }

    /// <summary>
    /// Whether the handler should be disposed or if it is handled by something else
    /// </summary>
    public bool ExternallyManaged { get; init; }
  }
}