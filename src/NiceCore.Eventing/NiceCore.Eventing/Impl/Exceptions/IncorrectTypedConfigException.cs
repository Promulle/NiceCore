﻿using System;

namespace NiceCore.Eventing.Impl.Exceptions
{
  /// <summary>
  /// Exception that is thrown when a uncorrectly typed config is supplied config is 
  /// </summary>
#pragma warning disable RCS1194 // Implement exception constructors.
  public class IncorrectTypedConfigException : Exception
#pragma warning restore RCS1194 // Implement exception constructors.
  {
    /// <summary>
    /// The Type the handler expected to be supplied
    /// </summary>
    public Type ExpectedConfigType { get; }

    /// <summary>
    /// Eventing Handler
    /// </summary>
    public IEventingHandler EventingHandler { get; }

    /// <summary>
    /// Supplied Config Type
    /// </summary>
    public Type SuppliedConfigType { get; }

    /// <summary>
    /// Constructor filling message based on input parameters
    /// </summary>
    /// <param name="i_ExpectedConfigType"></param>
    /// <param name="i_Handler"></param>
    /// <param name="i_SuppliedConfigType"></param>
    public IncorrectTypedConfigException(Type i_ExpectedConfigType, IEventingHandler i_Handler, Type i_SuppliedConfigType) : base(BuildMessage(i_ExpectedConfigType, i_Handler, i_SuppliedConfigType))
    {
      ExpectedConfigType = i_ExpectedConfigType;
      EventingHandler = i_Handler;
      SuppliedConfigType = i_SuppliedConfigType;
    }

    private static string BuildMessage(Type i_ExpectedConfigType, IEventingHandler i_Handler, Type i_SuppliedConfigType)
    {
      return $"Handler {i_Handler.GetType().FullName} can not work with configs of type {i_SuppliedConfigType.FullName}, please supply an instance that would be assignable to {i_ExpectedConfigType.FullName}";
    }
  }
}
