﻿namespace NiceCore.Eventing.Impl
{
  /// <summary>
  /// Interface for Service that provides the client identifier for the current installation
  /// </summary>
  public interface IClientIdentifierProvider
  {
    /// <summary>
    /// Gets the client identifier
    /// </summary>
    /// <returns></returns>
    string GetClientIdentifier();
  }
}