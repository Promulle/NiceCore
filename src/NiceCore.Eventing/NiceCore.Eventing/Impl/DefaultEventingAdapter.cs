﻿using NiceCore.Base.Services;
using NiceCore.Eventing.Messages;
using NiceCore.Eventing.Messaging;
using NiceCore.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NiceCore.Eventing.Messaging.Parameters;

namespace NiceCore.Eventing.Impl
{
  /// <summary>
  /// Default impl for IEventingAdapter
  /// </summary>
  [InitializeOnResolve]
  [Register(InterfaceType = typeof(IEventingAdapter), LifeStyle = LifeStyles.Singleton)]
  public class DefaultEventingAdapter : BaseService, IEventingAdapter
  {
    private readonly List<EventingHandlerEntry> m_Handlers = new();
    private string m_ClientIdentifier;

    /// <summary>
    /// Message service
    /// </summary>
    public IMessageService MessageService { get; set; }
    
    /// <summary>
    /// Bridge
    /// </summary>
    public IMessageServiceToEventingAdapterBridge MessageServiceToEventingAdapterBridge { get; set; }

    /// <summary>
    /// Injected Handlers
    /// </summary>
    public IList<IEventingHandler> InjectedHandlers { get; set; }
    
    /// <summary>
    /// Client Identifier Provider
    /// </summary>
    public IClientIdentifierProvider ClientIdentifierProvider { get; set; }

    /// <summary>
    /// internal Initialize
    /// </summary>
    protected override void InternalInitialize()
    {
      if (ClientIdentifierProvider != null)
      {
        m_ClientIdentifier = ClientIdentifierProvider.GetClientIdentifier();
      }
      if (InjectedHandlers != null)
      {
        foreach (var handler in InjectedHandlers)
        {
          AddHandler(handler, true);
        }
      }
    }

    /// <summary>
    /// Add a new handler to the eventing
    /// </summary>
    /// <param name="i_Handler"></param>
    /// <param name="i_ExternallyManaged"></param>
    public void AddHandler(IEventingHandler i_Handler, bool i_ExternallyManaged)
    {
      m_Handlers.Add(new EventingHandlerEntry()
      {
        Handler = i_Handler,
        ExternallyManaged = i_ExternallyManaged
      });
      i_Handler.LinkWithAdapterCallback(OnMessageReceivedFromHandler);
    }

    /// <summary>
    /// Unsubscribes all handlers from i_Topic
    /// </summary>
    /// <param name="i_Topic"></param>
    public void Unsubscribe(string i_Topic)
    {
      foreach (var handler in m_Handlers)
      {
        handler.Handler.Unsubscribe(i_Topic);
      }
    }

    /// <summary>
    /// Sends the message to all added handlers
    /// </summary>
    /// <param name="i_Message"></param>
    /// <param name="i_Parameters"></param>
    /// <returns></returns>
    public Task Send(IObjectMessage i_Message, IMessageSendingParameters i_Parameters)
    {
      if (i_Message.Origin == MessageOrigin.Local)
      {
        i_Message.SetClientIdentifier(m_ClientIdentifier);
        var taskList = new List<Task>(m_Handlers.Count);
        foreach (var handler in m_Handlers)
        {
          taskList.Add(handler.Handler.SendObjectMessage(i_Message, i_Parameters));
        }
        return Task.WhenAll(taskList);
      }
      return Task.CompletedTask;
    }

    /// <summary>
    /// This will subscribe all handlers to i_Topic
    /// </summary>
    /// <param name="i_Topic"></param>
    public void Subscribe<TMsg, TValue>(string i_Topic) where TMsg : IMessage<TValue>
    {
      foreach (var handler in m_Handlers)
      {
        handler.Handler.Subscribe<TMsg, TValue>(i_Topic);
      }
    }

    /// <summary>
    /// This will subscribe all handlers to i_Topic
    /// </summary>
    /// <param name="i_Topic"></param>
    /// <param name="i_MessageType"></param>
    public void Subscribe(string i_Topic, Type i_MessageType)
    {
      foreach (var handler in m_Handlers)
      {
        handler.Handler.Subscribe(i_Topic, i_MessageType);
      }
    }

    private Task OnMessageReceivedFromHandler(IObjectMessage i_Message, IMessageSendingParameters i_Parameters)
    {
      var messageClientIdentifier = i_Message.GetClientIdentifier();
      if (messageClientIdentifier == null || messageClientIdentifier != m_ClientIdentifier)
      {
        return MessageServiceToEventingAdapterBridge?.ForwardMessageFromRemote(i_Message, i_Parameters);
      }

      return Task.CompletedTask;
    }

    /// <summary>
    /// Dispose override disposing the handlers
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    protected override void Dispose(bool i_IsDisposing)
    {
      m_Handlers.ForEach(r =>
      {
        if (!r.ExternallyManaged)
        {
          r.Handler.Dispose();
        }
      });
      m_Handlers.Clear();
      base.Dispose(i_IsDisposing);
    }
  }
}
