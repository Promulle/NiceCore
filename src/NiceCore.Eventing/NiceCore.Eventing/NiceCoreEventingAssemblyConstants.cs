﻿namespace NiceCore.Eventing
{
  /// <summary>
  /// Constants for NiceCore.Eventing
  /// </summary>
  public static class NiceCoreEventingAssemblyConstants
  {
    /// <summary>
    /// Name of the assembly
    /// </summary>
    public static readonly string ASSEMBLY_NAME = "NiceCore.Eventing";
  }
}
