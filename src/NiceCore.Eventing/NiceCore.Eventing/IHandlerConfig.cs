﻿namespace NiceCore.Eventing
{
  /// <summary>
  /// Config for Handler
  /// </summary>
  public interface IHandlerConfig
  {
    /// <summary>
    /// Host Name
    /// </summary>
    string HostName { get; }
    
    /// <summary>
    /// Serializer Key
    /// </summary>
    string SerializerKey { get; }
    
    /// <summary>
    /// Is Enabled
    /// </summary>
    bool IsEnabled { get; }

    /// <summary>
    /// port
    /// </summary>
    string Port { get; }

    /// <summary>
    /// User Name
    /// </summary>
    string UserName { get; }

    /// <summary>
    /// Password
    /// </summary>
    string Password { get; }
  }
}
