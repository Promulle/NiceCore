﻿using System;

namespace NiceCore.Eventing.Exceptions
{
  /// <summary>
  /// Exception thrown if the wrong type of message value is trying to be set to a message
  /// </summary>
#pragma warning disable RCS1194 // Implement exception constructors.
  public class WrongMessageValueException : Exception
#pragma warning restore RCS1194 // Implement exception constructors.
  {
    /// <summary>
    /// Type where isAssignableFrom (ActualType) should be true for
    /// </summary>
    public Type MinimumType { get; }

    /// <summary>
    /// Type that was actually passed to SetValue
    /// </summary>
    public Type ActualType { get; }

    /// <summary>
    /// Ctor filling message using type
    /// </summary>
    /// <param name="i_MinimumExpectedType">Type that has to be assignableFrom by i_ActualType</param>
    /// <param name="i_ActualType">Actual type of message value passed</param>
    public WrongMessageValueException(Type i_MinimumExpectedType, Type i_ActualType) : base(BuildMessage(i_MinimumExpectedType, i_ActualType))
    {
    }

    private static string BuildMessage(Type i_MinimumExpectedType, Type i_ActualType)
    {
      return $"Can not set value to message, expected type {i_ActualType.FullName} to be assignable to {i_MinimumExpectedType.FullName} but was not.";
    }
  }
}
