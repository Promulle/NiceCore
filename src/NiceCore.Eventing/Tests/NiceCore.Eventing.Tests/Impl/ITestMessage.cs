using NiceCore.Eventing.Messages;

namespace NiceCore.Eventing.Tests.Impl
{
  /// <summary>
  /// Testmessage
  /// </summary>
  public interface ITestMessage : IMessage<string>
  {
    
  }
}