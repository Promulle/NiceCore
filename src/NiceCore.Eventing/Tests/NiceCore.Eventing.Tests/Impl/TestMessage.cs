﻿using NiceCore.Eventing.Messages;

namespace NiceCore.Eventing.Tests.Impl
{
  /// <summary>
  /// TestMessage
  /// </summary>
  public class TestMessage : BaseMessage<string>, ITestMessage
  {
    /// <summary>
    /// TOPIC
    /// </summary>
    public static readonly string TOPIC = "Test";

    /// <summary>
    /// TOPIC
    /// </summary>
    public override string MessageTopic { get; } = TOPIC;

    /// <summary>
    /// Create Test Message
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public static TestMessage Create(string i_Value)
    {
      return new TestMessage()
      {
        Value = new MessageValue<string>()
        {
          Value = i_Value
        }
      };
    }
  }
}
