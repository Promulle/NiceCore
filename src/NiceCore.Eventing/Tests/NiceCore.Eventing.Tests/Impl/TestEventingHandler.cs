﻿using NiceCore.Eventing.Impl;
using NiceCore.Eventing.Messages;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NiceCore.Diagnostics;

namespace NiceCore.Eventing.Tests.Impl
{
  /// <summary>
  /// Test impl for Eventing Handler
  /// </summary>
  public class TestEventingHandler : BaseEventingHandler<DefaultHandlerConfig>
  {
    /// <summary>
    /// messages
    /// </summary>
    public bool MessageFromServiceReceived { get; private set; } = false;

    /// <summary>
    /// Actual Logger
    /// </summary>
    public ILogger<TestEventingHandler> ActualLogger { get; set; } = DiagnosticsCollector.Instance.GetLogger<TestEventingHandler>();

    /// <summary>
    /// Logger
    /// </summary>
    public override ILogger Logger => ActualLogger;
    
    /// <summary>
    /// Handler
    /// </summary>
    public override string HandlerName { get; } = "Test";

    /// <summary>
    /// Fire message as if it came from a remote server
    /// </summary>
    public void MessageReceived()
    {
      RaiseMessageReceived(MessageHelper.Create<TestMessage, string>(""), null);
    }
    
    /// <summary>
    /// Unsubscribe
    /// </summary>
    /// <param name="i_Topic"></param>
    public override void Unsubscribe(string i_Topic)
    {
    }

    /// <summary>
    /// Send
    /// </summary>
    /// <param name="i_Topic"></param>
    /// <param name="i_Data"></param>
    /// <returns></returns>
    protected override Task SendInternal(string i_Topic, byte[] i_Data)
    {
      MessageFromServiceReceived = true;
      return Task.CompletedTask;
    }

    /// <summary>
    /// Subscribe
    /// </summary>
    /// <typeparam name="TMsg"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <param name="i_Topic"></param>
    protected override void SubscribeInternal<TMsg, TValue>(string i_Topic)
    {
    }

    /// <summary>
    /// Subscribe
    /// </summary>
    /// <param name="i_Topic"></param>
    /// <param name="i_MessageType"></param>
    protected override void SubscribeInternal(string i_Topic, Type i_MessageType)
    {
    }

    /// <summary>
    /// Test impl
    /// </summary>
    /// <param name="i_HandlerConfig"></param>
    /// <returns></returns>
    protected override bool IsConfigCorrect(IHandlerConfig i_HandlerConfig)
    {
      return true;
    }
  }
}
