using NiceCore.Eventing.Messages;

namespace NiceCore.Eventing.Tests.Impl
{
  /// <summary>
  /// Test Message 2
  /// </summary>
  public class TestMessage2 : TestMessage
  {
    /// <summary>
    /// Create Test Message
    /// </summary>
    /// <param name="i_Value"></param>
    /// <returns></returns>
    public static TestMessage2 Create2(string i_Value)
    {
      return new TestMessage2()
      {
        Value = new MessageValue<string>()
        {
          Value = i_Value
        }
      };
    }
  }
}