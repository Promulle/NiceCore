using System;
using System.Collections.Generic;
using NiceCore.Eventing.Messaging.Receiver;

namespace NiceCore.Eventing.Tests.Impl
{
  /// <summary>
  /// Test Register Type Provider
  /// </summary>
  public class TestRegisterTypeProvider : IReceiverRegistrationTypeProvider
  {
    /// <summary>
    /// Get Types To Register Receiver
    /// </summary>
    /// <param name="i_Descriptor"></param>
    /// <returns></returns>
    public IReadOnlyCollection<Type> GetTypesToRegisterReceiver(MessageInvocationReceiverDescriptor i_Descriptor)
    {
      return new[]
      {
        typeof(TestMessage),
        typeof(TestMessage2)
      };
    }
  }
}