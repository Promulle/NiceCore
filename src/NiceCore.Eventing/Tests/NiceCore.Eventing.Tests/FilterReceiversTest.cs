using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.Logging.Abstractions;
using NiceCore.Eventing.Messages;
using NiceCore.Eventing.Messaging;
using NiceCore.Eventing.Messaging.Distribution;
using NiceCore.Eventing.Messaging.Distribution.Impl;
using NiceCore.Eventing.Messaging.Extensions;
using NiceCore.Eventing.Messaging.Filtering;
using NiceCore.Eventing.Messaging.Filtering.Impl;
using NiceCore.Eventing.Messaging.Impl;
using NiceCore.Eventing.Messaging.Parameters;
using NiceCore.Eventing.Messaging.Parameters.Impl;
using NiceCore.Eventing.Messaging.Receiver.Priority.Impl;
using NUnit.Framework;

namespace NiceCore.Eventing.Tests
{
  /// <summary>
  /// Tests for filtering receivers
  /// </summary>
  [TestFixture]
  public class FilterReceiversTest
  {
    /// <summary>
    /// Tests filtering by exclusive tags
    /// </summary>
    [Test]
    public async Task TestTagExcludingFilter()
    {
      var service = CreateService();
      var flagA = false;
      var flagB = false;
      service.Register<Impl.TestMessage, string>(async _ => flagA = true)
        .WithTag("A")
        .Register();
      service.Register<Impl.TestMessage, string>(async _ => flagB = true)
        .WithTag("B")
        .Register();
      var parameters = DefaultMessageSendingParameters.New()
        .ExcludeTag("B")
        .Build();
      await service.SendMessage(new Impl.TestMessage()
      {
        Origin = MessageOrigin.Local,
        Value = new MessageValue<string>()
        {
          Value = "s"
        }
      }, parameters, false).ConfigureAwait(false);
      flagA.Should().BeTrue();
      flagB.Should().BeFalse();
    }

    /// <summary>
    /// Tests filtering by exclusive tags
    /// </summary>
    [Test]
    public async Task TestTagExcludingFilterAdditivity()
    {
      var service = CreateService();
      var flagA = false;
      var flagB = false;
      service.Register<Impl.TestMessage, string>(async _ => flagA = true)
        .WithTag("A")
        .WithTag("B")
        .Register();
      service.Register<Impl.TestMessage, string>(async _ => flagB = true)
        .WithTag("C")
        .Register();
      var parameters = DefaultMessageSendingParameters.New()
        .ExcludeTag("B")
        .Build();
      await service.SendMessage(new Impl.TestMessage()
      {
        Origin = MessageOrigin.Local,
        Value = new MessageValue<string>()
        {
          Value = "s"
        }
      }, parameters, false).ConfigureAwait(false);
      flagA.Should().BeFalse();
      flagB.Should().BeTrue();
    }

    /// <summary>
    /// Tests filtering by exclusive tags
    /// </summary>
    [Test]
    public async Task TestTagRequireFilter()
    {
      var service = CreateService();
      var flagA = false;
      var flagB = false;
      service.Register<Impl.TestMessage, string>(async _ => flagA = true)
        .WithTag("A")
        .WithTag("B")
        .Register();
      service.Register<Impl.TestMessage, string>(async _ => flagB = true)
        .WithTag("C")
        .Register();
      var parameters = DefaultMessageSendingParameters.New()
        .RequireTag("C")
        .Build();
      await service.SendMessage(new Impl.TestMessage()
      {
        Origin = MessageOrigin.Local,
        Value = new MessageValue<string>()
        {
          Value = "s"
        }
      }, parameters, false).ConfigureAwait(false);
      flagA.Should().BeFalse();
      flagB.Should().BeTrue();
    }

    /// <summary>
    /// Tests filtering by exclusive tags
    /// </summary>
    [Test]
    public async Task TestTagRequireFilterAdditivity()
    {
      var service = CreateService();
      var flagA = false;
      var flagB = false;
      service.Register<Impl.TestMessage, string>(async _ => flagA = true)
        .WithTag("A")
        .WithTag("B")
        .Register();
      service.Register<Impl.TestMessage, string>(async _ => flagB = true)
        .WithTag("C")
        .Register();
      var parameters = DefaultMessageSendingParameters.New()
        .RequireTag("C")
        .RequireTag("D")
        .Build();
      await service.SendMessage(new Impl.TestMessage()
      {
        Origin = MessageOrigin.Local,
        Value = new MessageValue<string>()
        {
          Value = "s"
        }
      }, parameters, false).ConfigureAwait(false);
      flagA.Should().BeFalse();
      flagB.Should().BeFalse();
    }

    /// <summary>
    /// Tests filtering by exclusive tags
    /// </summary>
    [Test]
    public async Task TestTagCombineRequireAndExclude()
    {
      var service = CreateService();
      var flagA = false;
      var flagB = false;
      var flagC = false;
      service.Register<Impl.TestMessage, string>(async _ => flagA = true)
        .WithTag("A")
        .WithTag("B")
        .Register();
      service.Register<Impl.TestMessage, string>(async _ => flagB = true)
        .WithTag("B")
        .WithTag("C")
        .Register();
      service.Register<Impl.TestMessage, string>(async _ => flagC = true)
        .WithTag("E")
        .WithTag("C")
        .Register();
      var parameters = DefaultMessageSendingParameters.New()
        .RequireTag("C")
        .ExcludeTag("E")
        .Build();
      await service.SendMessage(new Impl.TestMessage()
      {
        Origin = MessageOrigin.Local,
        Value = new MessageValue<string>()
        {
          Value = "s"
        }
      }, parameters, false).ConfigureAwait(false);
      flagA.Should().BeFalse();
      flagB.Should().BeTrue();
      flagC.Should().BeFalse();
    }

    private static DefaultMessageService CreateService()
    {
      var sendModel = new DefaultMessagingSendModel()
      {
        Distributors = new List<IMessageDistributor>()
        {
          new DefaultMessageDistributor()
        },
        Factories = new List<IMessageSendingParameterBasedFactory>()
        {
          new DefaultMessageSendingParameterBasedFactory()
        },
        Filters = new List<IMessageInvocationReceiverFilter>()
        {
          new MessageInvocationReceiverPriorityFilter(),
          new MessageInvocationReceiverTagFilter()
        },
        Grouper = new DefaultMessageDistributionGrouper()
      };
      sendModel.Initialize();
      return new()
      {
        MessagingSendModel = sendModel,
        Logger = NullLogger<IMessageService>.Instance,
        MessageInvocationReceiverPriorityStore = new DefaultMessageInvocationReceiverPriorityStore()
      };
    }
  }
}