﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.Logging.Abstractions;
using NiceCore.Eventing.Impl;
using NiceCore.Eventing.Messages;
using NiceCore.Eventing.Messaging;
using NiceCore.Eventing.Messaging.Distribution;
using NiceCore.Eventing.Messaging.Distribution.Impl;
using NiceCore.Eventing.Messaging.Extensions;
using NiceCore.Eventing.Messaging.Filtering;
using NiceCore.Eventing.Messaging.Filtering.Impl;
using NiceCore.Eventing.Messaging.Impl;
using NiceCore.Eventing.Messaging.Parameters;
using NiceCore.Eventing.Messaging.Parameters.Impl;
using NiceCore.Eventing.Messaging.Receiver.Priority.Impl;
using NiceCore.Eventing.Tests.Impl;
using NiceCore.ServiceLocation;
using NiceCore.ServiceLocation.Helpers;
using NSubstitute;
using NUnit.Framework;

namespace NiceCore.Eventing.Tests
{
  /// <summary>
  /// Tests for DefaultEventingAdapter impl
  /// </summary>
  [TestFixture]
  public class DefaultEventAdapterTests
  {
    /// <summary>
    /// Integrate with message service -> sending
    /// </summary>
    [Test]
    public async Task IntegrateWithMessageServiceSend()
    {
      var msgService = CreateService();
      var handler = new TestEventingHandler();
      var formatter = Substitute.For<IExternalMessageFormatter>();
      formatter.ToExternalData(Arg.Any<IObjectMessage>(), Arg.Any<IMessageSendingParameters>()).Returns(ValueTask.FromResult(Encoding.UTF8.GetBytes("Hallo")));
      handler.ExternalMessageFormatter = formatter;
      var adapter = CreateInstance(msgService, handler);
      adapter.Initialize();
      await msgService.SendMessageDirect<TestMessage, string>(MessageHelper.Create<TestMessage, string>("")).ConfigureAwait(false);
      handler.MessageFromServiceReceived.Should().BeTrue();
    }

    /// <summary>
    /// Integration with message service -> receiving from remote server
    /// </summary>
    [Test]
    public async Task IntegrateWithMessageServiceReceive()
    {
      var msgService = CreateService();
      var handler = new TestEventingHandler();
      var adapter = CreateInstance(msgService, handler);
      var flag = false;
      msgService.RegisterDirect<TestMessage, string>(async msg => flag = true);
      handler.MessageReceived();
      await Task.Delay(10);
      flag.Should().BeTrue();
    }

    private static DefaultMessageService CreateService()
    {
      return new()
      {
        MessagingSendModel = new DefaultMessagingSendModel()
        {
          Distributors = new List<IMessageDistributor>()
          {
            new DefaultMessageDistributor()
          },
          Factories = new List<IMessageSendingParameterBasedFactory>()
          {
            new DefaultMessageSendingParameterBasedFactory()
          },
          Filters = new List<IMessageInvocationReceiverFilter>()
          {
            new MessageInvocationReceiverPriorityFilter(),
            new MessageInvocationReceiverTagFilter()
          },
          Grouper = new DefaultMessageDistributionGrouper()
        },
        Logger = NullLogger<IMessageService>.Instance,
        MessageInvocationReceiverPriorityStore = new DefaultMessageInvocationReceiverPriorityStore()
      };
    }

    private static DefaultEventingAdapter CreateInstance(DefaultMessageService i_MessageService, TestEventingHandler i_Handler)
    {
      var bridge = new DefaultMessageServiceToEventingAdapterBridge()
      {
        MessageService = i_MessageService,
      };
      var adapter = new DefaultEventingAdapter
      {
        MessageService = i_MessageService,
        MessageServiceToEventingAdapterBridge = bridge
      };
      var container = Substitute.For<IServiceContainer>();
      container.Resolve<IEventingAdapter>().Returns(adapter);
      bridge.EventingAdapter = new OnDemandResolvedInstance<IEventingAdapter>(container);
      i_MessageService.MessageServiceToEventingAdapterBridge = bridge;
      adapter.AddHandler(i_Handler, false);
      adapter.Initialize();
      return adapter;
    }
  }
}