﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using NiceCore.Eventing.Messages;
using NiceCore.Eventing.Messaging.Distribution;
using NiceCore.Eventing.Messaging.Distribution.Impl;
using NiceCore.Eventing.Messaging.Impl;
using NiceCore.Eventing.Tests.Impl;
using NiceCore.Eventing.Messaging.Extensions;
using NiceCore.Eventing.Messaging.Filtering;
using NiceCore.Eventing.Messaging.Filtering.Impl;
using NiceCore.Eventing.Messaging.Parameters;
using NiceCore.Eventing.Messaging.Parameters.Impl;
using NiceCore.Eventing.Messaging.Receiver.Priority.Impl;
using NUnit.Framework;

namespace NiceCore.Eventing.Tests
{
  /// <summary>
  /// Tests for message service
  /// </summary>
  [TestFixture]
  public class MessageServiceTests
  {
    private static DefaultMessageService CreateMessageService()
    {
      return new DefaultMessageService()
      {
        MessagingSendModel = new DefaultMessagingSendModel()
        {
          Distributors = new List<IMessageDistributor>()
          {
            new DefaultMessageDistributor()
          },
          Factories = new List<IMessageSendingParameterBasedFactory>()
          {
            new DefaultMessageSendingParameterBasedFactory()
          },
          Filters = new List<IMessageInvocationReceiverFilter>()
          {
            new MessageInvocationReceiverPriorityFilter()
          },
          Grouper = new DefaultMessageDistributionGrouper()
        },
        MessageInvocationReceiverPriorityStore = new DefaultMessageInvocationReceiverPriorityStore()
      };
    }

    /// <summary>
    /// Tests if register works (increases registrations count)
    /// </summary>
    [Test]
    public void Register()
    {
      var inst = CreateMessageService();
      inst.RegisterDirect<TestMessage, string>(TestMethod);

      inst.m_ReceiverStore.CountRegistrations<TestMessage, string>().Should().Be(1);
    }

    /// <summary>
    /// Tests sending of messages
    /// </summary>
    [Test]
    public async Task SendNonGeneric()
    {
      var inst = CreateMessageService();
      var flag = false;
      inst.RegisterDirect<TestMessage, string>(async msg => flag = true);
      var msg = TestMessage.Create(string.Empty);
      await inst.SendMessageDirect<TestMessage, string>(msg).ConfigureAwait(false);
      flag.Should().BeTrue();
    }

    /// <summary>
    /// Tests sending of message
    /// </summary>
    [Test]
    public async Task SendGeneric()
    {
      var inst = CreateMessageService();
      var flag = false;
      var msg = TestMessage.Create(string.Empty);
      inst.RegisterDirect<TestMessage, string>(async _ => flag = true);
      await inst.SendMessageDirect<TestMessage, string>(msg).ConfigureAwait(false);
      flag.Should().BeTrue();
    }

    /// <summary>
    /// Tests unregistering of functions
    /// </summary>
    [Test]
    public void Unregister()
    {
      var inst = CreateMessageService();
      var sub = inst.RegisterDirect<TestMessage, string>(TestMethod);
      inst.m_ReceiverStore.CountRegistrations<TestMessage, string>().Should().Be(1);
      sub.Unsubscribe();
      inst.m_ReceiverStore.CountRegistrations<TestMessage, string>().Should().Be(0);
    }

    /// <summary>
    /// Test Different Types
    /// </summary>
    [Test]
    public async Task TestDifferentTypes()
    {
      var inst = CreateMessageService();
      var flag = false;
      inst.Register<ITestMessage, string, TestMessage, string>(async (msg) => flag = msg != null && msg.GetActualValue().ResultValue == "test").Register();
      var message = new TestMessage()
      {
        Value = new MessageValue<string>()
        {
          Value = "test"
        }
      };
      await inst.SendMessage(message, null, false).ConfigureAwait(false);
      flag.Should().BeTrue();
    }
    
    /// <summary>
    /// Test Different Types
    /// </summary>
    [Test]
    public async Task TestRegisterProvider()
    {
      var inst = CreateMessageService();
      var count = 0;
      var sub = inst.Register<ITestMessage, string>(async (msg) =>
      {
        if (msg.GetActualValue().ResultValue == "test")
        {
          count++;
        }
      }).ProvideRegistrationTypes(new TestRegisterTypeProvider()).Register();
      var message = TestMessage.Create("test");
      var message2 = TestMessage2.Create2("test");
      await inst.SendMessage(message, null, false).ConfigureAwait(false);
      await inst.SendMessage(message2, null, false).ConfigureAwait(false);
      count.Should().Be(2);
      inst.m_ReceiverStore.CountRegistrations(typeof(TestMessage)).Should().Be(1);
      inst.m_ReceiverStore.CountRegistrations(typeof(TestMessage2)).Should().Be(1);
      sub.Unsubscribe();
      inst.m_ReceiverStore.CountRegistrations(typeof(TestMessage)).Should().Be(0);
      inst.m_ReceiverStore.CountRegistrations(typeof(TestMessage2)).Should().Be(0);
    }
    
    private Task TestMethod(TestMessage i_Message)
    {
      return Task.CompletedTask;
    }
  }
}