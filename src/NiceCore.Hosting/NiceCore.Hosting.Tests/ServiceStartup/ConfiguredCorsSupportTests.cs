using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using NiceCore.Hosting.ASPNetCore.Configurability.Cors;
using NiceCore.Services.Config;
using NiceCore.Services.Config.Impl;
using NiceCore.Services.Default.Config;
using NUnit.Framework;

namespace NiceCore.Hosting.Tests.ServiceStartup
{
  /// <summary>
  /// Test Configured Cors Support
  /// </summary>
  [TestFixture]
  [Parallelizable]
  public class ConfiguredCorsSupportTests
  {
    /// <summary>
    /// Test Nothing Configured
    /// </summary>
    [Test]
    [Parallelizable]
    public void TestNothingConfigured()
    {
      var serviceCollection = new ServiceCollection();
      var inst = new DefaultConfiguredCorsSupport();
      inst.Initialize();
      inst.ApplyCorsBuilder(serviceCollection);
      serviceCollection.Count.Should().Be(0);
    }

    /// <summary>
    /// Test Configured Allow Any
    /// </summary>
    [Test]
    [Parallelizable]
    public async Task TestConfiguredAllowAny()
    {
      var dict = new Dictionary<string, string>()
      {
        {"NiceCore:Hosting:Asp:Cors:AllowAnyOrigin", "true"},
        {"NiceCore:Hosting:Asp:Cors:AllowAnyMethod", "true"},
        {"NiceCore:Hosting:Asp:Cors:AllowAnyHeader", "true"},
        {"NiceCore:Hosting:Asp:Cors:WithCredentials", "false"},
      };
      var ncConfiguration = new DefaultNcConfiguration()
      {
        ConfigurationSources = new List<INcConfigurationSource>()
        {
          new DefaultInMemoryNcConfigurationSource(dict)
        }
      };
      ncConfiguration.Initialize();
      
      var serviceCollection = new ServiceCollection();
      var inst = new DefaultConfiguredCorsSupport()
      {
        Configuration = ncConfiguration
      };
      inst.Initialize();
      inst.ApplyCorsBuilder(serviceCollection);
      serviceCollection.Should().NotBeEmpty();

      var provider = serviceCollection.BuildServiceProvider();
      var policyProvider = provider.GetRequiredService<ICorsPolicyProvider>();
      policyProvider.Should().NotBeNull();
      var policy = await policyProvider.GetPolicyAsync(new DefaultHttpContext(), null).ConfigureAwait(false);
      policy.Should().NotBeNull();
      policy!.AllowAnyHeader.Should().BeTrue();
      policy.AllowAnyMethod.Should().BeTrue();
      policy.AllowAnyOrigin.Should().BeTrue();
      policy.Headers.Should().NotBeNullOrEmpty().And.Contain("*");
      policy.Origins.Should().NotBeNullOrEmpty().And.Contain("*");
      policy.Methods.Should().NotBeNullOrEmpty().And.Contain("*");
      policy.ExposedHeaders.Should().BeNullOrEmpty();
      policy.SupportsCredentials.Should().BeFalse();
    }
    
    /// <summary>
    /// Test Configured Allow Any
    /// </summary>
    [Test]
    [Parallelizable]
    public async Task TestConfiguredAllowSpecific()
    {
      var dict = new Dictionary<string, string>()
      {
        {"NiceCore:Hosting:Asp:Cors:AllowAnyOrigin", "False"},
        {"NiceCore:Hosting:Asp:Cors:AllowAnyMethod", "False"},
        {"NiceCore:Hosting:Asp:Cors:AllowAnyHeader", "False"},
        {"NiceCore:Hosting:Asp:Cors:WithCredentials", "true"},
        {"NiceCore:Hosting:Asp:Cors:AllowedOrigins:0", "SomeFantasyAddress"},
        {"NiceCore:Hosting:Asp:Cors:AllowedOrigins:1", "AnotherFantasyAddress"},
        {"NiceCore:Hosting:Asp:Cors:AllowedHeaders:0", "SomeFantasyHeader"},
        {"NiceCore:Hosting:Asp:Cors:AllowedMethods:0", "SomeFantasyMethod"},
      };
      var ncConfiguration = new DefaultNcConfiguration()
      {
        ConfigurationSources = new List<INcConfigurationSource>()
        {
          new DefaultInMemoryNcConfigurationSource(dict)
        }
      };
      ncConfiguration.Initialize();
      
      var serviceCollection = new ServiceCollection();
      var inst = new DefaultConfiguredCorsSupport()
      {
        Configuration = ncConfiguration
      };
      inst.Initialize();
      inst.ApplyCorsBuilder(serviceCollection);
      serviceCollection.Should().NotBeEmpty();

      var provider = serviceCollection.BuildServiceProvider();
      var policyProvider = provider.GetRequiredService<ICorsPolicyProvider>();
      policyProvider.Should().NotBeNull();
      var policy = await policyProvider.GetPolicyAsync(new DefaultHttpContext(), null).ConfigureAwait(false);
      policy.Should().NotBeNull();
      policy!.AllowAnyHeader.Should().BeFalse();
      policy.AllowAnyMethod.Should().BeFalse();
      policy.AllowAnyOrigin.Should().BeFalse();
      policy.Headers.Should().NotBeNullOrEmpty().And.HaveCount(1).And.Contain("SomeFantasyHeader");
      policy.Origins.Should().NotBeNullOrEmpty().And.HaveCount(2).And.Contain("somefantasyaddress").And.Contain("anotherfantasyaddress");
      policy.Methods.Should().NotBeNullOrEmpty().And.HaveCount(1).And.Contain("SomeFantasyMethod");
      policy.ExposedHeaders.Should().BeNullOrEmpty();
    }
  }
}