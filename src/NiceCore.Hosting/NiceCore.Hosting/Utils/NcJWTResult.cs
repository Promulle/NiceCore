// Copyright 2021 Sycorax Systemhaus GmbH

using System;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;

namespace NiceCore.Hosting.Utils
{
  /// <summary>
  /// Result of the generation of a token
  /// </summary>
  public struct NcJWTResult
  {
    /// <summary>
    /// Time the token Expires
    /// </summary>
    public required DateTime? ExpirationTime { get; init; }
    
    /// <summary>
    /// generated Token
    /// </summary>
    public required string GeneratedToken { get; init; }
    
    /// <summary>
    /// Token
    /// </summary>
    public required JwtSecurityToken Token { get; init; }
  }
}
