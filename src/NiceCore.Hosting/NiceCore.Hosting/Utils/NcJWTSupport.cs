﻿using Microsoft.IdentityModel.Tokens;
using NiceCore.Base;
using NiceCore.Hosting.Tokens;
using NiceCore.Logging;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Logging;
using NiceCore.Logging.Extensions;

namespace NiceCore.Hosting.Utils
{
  /// <summary>
  /// Utils for the creation/validation of JWT Tokens
  /// </summary>
  public static class NcJWTSupport
  {
    /// <summary>
    /// Claim Type for user id
    /// </summary>
    public static readonly string TOKEN_CLAIM_USER_ID = "user_id";

    /// <summary>
    /// Generate a jwt token for a specific user
    /// </summary>
    /// <param name="i_UserId"></param>
    /// <returns></returns>
    public static NcJWTResult GenerateJWTToken(string i_UserId)
    {
      return GenerateJWTToken(i_UserId, 30, TimeKind.Minutes, null);
    }

    /// <summary>
    /// Generates a jwt token for a specific user that expires after i_Expiration has passed in i_TimeKind, signed by i_Secret
    /// </summary>
    /// <param name="i_Expiration">time until the token expires</param>
    /// <param name="i_TimeKind">unit of the provided time</param>
    /// <param name="i_Secret">Secret that is used to sign the token, if null, the token is not signed</param>
    /// <param name="i_ClaimFactoryMethods">Methods that are used to add claims to the generated token</param>
    /// <param name="i_Options">options for creating/validating jwt tokens</param>
    /// <returns></returns>
    public static NcJWTResult GenerateJWTToken(int? i_Expiration, TimeKind? i_TimeKind, string i_Secret, IEnumerable<Func<Claim>> i_ClaimFactoryMethods, IJwTokenOptions i_Options)
    {
      var tokenHandler = new JwtSecurityTokenHandler();
      var descriptor = new SecurityTokenDescriptor
      {
        Subject = new ClaimsIdentity(i_ClaimFactoryMethods.Select(r => r.Invoke()), "jwt")
      };
      if (i_Expiration != null && i_TimeKind != null)
      {
        descriptor.Expires = i_TimeKind.Value.GetTimeIn(i_Expiration.Value);
      }
      if (!string.IsNullOrEmpty(i_Secret))
      {
        var keyBytes = Encoding.UTF8.GetBytes(i_Secret);
        descriptor.SigningCredentials = i_Options.GetSigningCredentials(keyBytes);
      }
      var token = tokenHandler.CreateToken(descriptor);
      if (token is not JwtSecurityToken castedToken)
      {
        throw new InvalidOperationException("Creating a jwt token somehow did not result in a jwt token being created");
      }

      return new()
      {
        Token = castedToken,
        ExpirationTime = descriptor.Expires,
        GeneratedToken = tokenHandler.WriteToken(token)
      };
    }

    /// <summary>
    /// Generates a jwt token for a specific user that expires after i_Expiration has passed in i_TimeKind, signed by i_Secret
    /// </summary>
    /// <param name="i_UserId">The id of the user that is added to the token</param>
    /// <param name="i_Expiration">time until the token expires</param>
    /// <param name="i_TimeKind">unit of the provided time</param>
    /// <param name="i_Secret">Secret that is used to sign the token, if null, the token is not signed</param>
    /// <returns></returns>
    public static NcJWTResult GenerateJWTToken(string i_UserId, int i_Expiration, TimeKind i_TimeKind, string i_Secret)
    {
      return GenerateJWTToken(i_Expiration, i_TimeKind, i_Secret, new List<Func<Claim>>() { () => new Claim(TOKEN_CLAIM_USER_ID, i_UserId) }, new DefaultJwTokenOptions());
    }

    /// <summary>
    /// Generates a jwt token for a specific user that does not expire, signed by i_Secret
    /// </summary>
    /// <param name="i_ClaimFactoryMethods"></param>
    /// <param name="i_Secret">Secret that is used to sign the token, if null, the token is not signed</param>
    /// <param name="i_Options"></param>
    /// <returns></returns>
    public static NcJWTResult GenerateNotExpiringJWTToken(IEnumerable<Func<Claim>> i_ClaimFactoryMethods, string i_Secret, IJwTokenOptions i_Options)
    {
      return GenerateJWTToken(null, null, i_Secret, i_ClaimFactoryMethods, i_Options);
    }

    /// <summary>
    /// Validate the token
    /// </summary>
    /// <param name="i_Token"></param>
    /// <param name="i_Logger"></param>
    /// <returns></returns>
    public static NcTokenValidationResult ValidateJWTToken(string i_Token, ILogger i_Logger)
    {
      return ValidateJWTToken(i_Token, null, new DefaultJwTokenOptions(), i_Logger);
    }

    /// <summary>
    /// Validate the token using i_Secret as key for signing
    /// </summary>
    /// <param name="i_Token"></param>
    /// <param name="i_Secret"></param>
    /// <param name="i_Options">options for validation of tokens</param>
    /// <param name="i_Logger"></param>
    /// <returns></returns>
    public static NcTokenValidationResult ValidateJWTToken(string i_Token, string i_Secret, IJwTokenOptions i_Options, ILogger i_Logger)
    {
      try
      {
        return ValidateToken(i_Token, i_Secret, i_Options);
      }
      catch (Exception ex)
      {
        i_Logger.Error(ex, "Token could not be validated. Either an error occured or the token is not valid.");
        return new NcTokenValidationResult()
        {
          ClaimsPrincipal = null,
          ParsedToken = null,
          Success = false,
          UserID = null
        };
      }
    }

    private static NcTokenValidationResult ValidateToken(string i_Token, string i_Secret, IJwTokenOptions i_Options)
    {
      var tokenHandler = new JwtSecurityTokenHandler();

      if (i_Secret != null)
      {
        var keyBytes = Encoding.UTF8.GetBytes(i_Secret);
        var claims = tokenHandler.ValidateToken(i_Token, i_Options.GetTokenValidationParameters(keyBytes), out var token);
        var jwtToken = token as JwtSecurityToken;
        return new NcTokenValidationResult()
        {
          ParsedToken = jwtToken,
          Success = true,
          UserID = GetUserIDFromDefaultClaim(jwtToken),
          ClaimsPrincipal = claims
        };
      }
      else
      {
        var token = tokenHandler.ReadToken(i_Token) as JwtSecurityToken;
        return new NcTokenValidationResult()
        {
          ParsedToken = token,
          Success = true,
          ClaimsPrincipal = new ClaimsPrincipal(new ClaimsIdentity(token.Claims)),
          UserID = GetUserIDFromDefaultClaim(token),
        };
      }
    }

    private static string GetUserIDFromDefaultClaim(JwtSecurityToken i_Token)
    {
      return i_Token.Claims.FirstOrDefault(r => r.Type.Equals(TOKEN_CLAIM_USER_ID, StringComparison.OrdinalIgnoreCase))?.Value;
    }
  }
}
