using System;
using NiceCore.Hosting.Tokens;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Hosting.Config
{
  /// <summary>
  /// Token Setting
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public sealed class TokenSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// Key
    /// </summary>
    public const string KEY = "NiceCore.Hosting.Token";

    /// <summary>
    /// Key
    /// </summary>
    public string Key { get; } = KEY;

    /// <summary>
    /// Setting Type
    /// </summary>
    public Type SettingType { get; } = typeof(TokenSettingsModel);
  }
}