﻿namespace NiceCore.Hosting.Permissions
{
  /// <summary>
  /// Use case that holds username and password
  /// </summary>
  public class PermissionHostingBasicAuthUseCase : IPermissionHostingUseCase
  {
    /// <summary>
    /// Endpoint called
    /// </summary>
    public string EndpointCalled { get; init; }

    /// <summary>
    /// UserName
    /// </summary>
    public string UserName { get; init; }

    /// <summary>
    /// Password
    /// </summary>
    public string Password { get; init; }
  }
}
