﻿namespace NiceCore.Hosting.Permissions
{
  /// <summary>
  /// Constants for Hosting use case kinds
  /// </summary>
  public static class HostingUseCaseKinds
  {
    /// <summary>
    /// BASIC Auth based use case
    /// </summary>
    public static readonly string BASIC = "BASIC";

    /// <summary>
    /// TOKEN based use case
    /// </summary>
    public static readonly string TOKEN = "TOKEN";
  }
}
