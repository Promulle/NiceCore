﻿using NiceCore.Permissions;

namespace NiceCore.Hosting.Permissions
{
  /// <summary>
  /// Use case for calls to hosted endpoints
  /// </summary>
  public interface IPermissionHostingUseCase : IPermissionUseCase
  {
    /// <summary>
    /// The endpoint that has been called
    /// </summary>
    string EndpointCalled { get; }
  }
}
