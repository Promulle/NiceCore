﻿namespace NiceCore.Hosting.Permissions
{
  /// <summary>
  /// Use case that holds the session token
  /// </summary>
  public class PermissionHostingTokenUseCase : IPermissionHostingUseCase
  {
    /// <summary>
    /// Called endpoint
    /// </summary>
    public string EndpointCalled { get; init; }

    /// <summary>
    /// Token associated with the session the call has been made in
    /// </summary>
    public string Token { get; init; }
  }
}
