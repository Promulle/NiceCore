﻿using Microsoft.IdentityModel.Tokens;
using System.Collections.Generic;

namespace NiceCore.Hosting.Tokens
{
  /// <summary>
  /// Configurator that is applied to the collection of ISecurityTokenValidator in Startup
  /// </summary>
  public interface ISecurityTokenValidatorConfigurator
  {
    /// <summary>
    /// Applies changes to the t_Validators collection
    /// </summary>
    /// <param name="t_Validators"></param>
    void Apply(IList<ISecurityTokenValidator> t_Validators);
  }
}
