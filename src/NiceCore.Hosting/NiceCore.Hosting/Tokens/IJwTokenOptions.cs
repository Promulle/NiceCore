﻿using Microsoft.IdentityModel.Tokens;

namespace NiceCore.Hosting.Tokens
{
  /// <summary>
  /// Options used for creating/validating tokens
  /// </summary>
  public interface IJwTokenOptions
  {
    /// <summary>
    /// Returns the validation parameters that should be used
    /// </summary>
    /// <returns></returns>
    TokenValidationParameters GetTokenValidationParameters(byte[] i_KeyBytes);

    /// <summary>
    /// Returns the signing credentials that should be used when creating new tokens
    /// </summary>
    /// <returns></returns>
    SigningCredentials GetSigningCredentials(byte[] i_KeyBytes);
  }
}
