﻿using Microsoft.IdentityModel.Tokens;
using System;

namespace NiceCore.Hosting.Tokens
{
  /// <summary>
  /// Default impl for IJwTokens
  /// </summary>
  public sealed class DefaultJwTokenOptions : IJwTokenOptions
  {
    /// <summary>
    /// Gets signing credentials
    /// </summary>
    /// <returns></returns>
    public SigningCredentials GetSigningCredentials(byte[] i_KeyBytes)
    {
      return new SigningCredentials(new SymmetricSecurityKey(i_KeyBytes), SecurityAlgorithms.HmacSha256Signature);
    }

    /// <summary>
    /// gets validation parameters
    /// </summary>
    /// <returns></returns>
    public TokenValidationParameters GetTokenValidationParameters(byte[] i_KeyBytes)
    {
      return new TokenValidationParameters()
      {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey(i_KeyBytes),
        ValidateIssuer = false,
        ValidateAudience = false,
        ClockSkew = TimeSpan.Zero
      };
    }
  }
}
