﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace NiceCore.Hosting.Tokens
{
  /// <summary>
  /// Result of a token validation
  /// </summary>
  public class NcTokenValidationResult
  {
    /// <summary>
    /// Success
    /// </summary>
    public bool Success { get; init; }

    /// <summary>
    /// Claims
    /// </summary>
    public ClaimsPrincipal ClaimsPrincipal { get; init; }

    /// <summary>
    /// Value of user id, only filled if default user claim was filled
    /// </summary>
    public string UserID { get; init; }

    /// <summary>
    /// Token
    /// </summary>
    public JwtSecurityToken ParsedToken { get; init; }
  }
}
