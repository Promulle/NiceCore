﻿using System.Collections.Generic;
using System.Reflection;

namespace NiceCore.Hosting
{
  /// <summary>
  /// Interface that is used to determine what assemblies should be searched for controllers
  /// </summary>
  public interface IControllerAssembly
  {
    /// <summary>
    /// Returns assemblies that should also be used as application parts
    /// usefull if you want to add assembly controllers that are in libraries that do not expose an ability to add them
    /// </summary>
    /// <returns></returns>
    IEnumerable<Assembly> GetAdditionalAssemblies();
  }
}
