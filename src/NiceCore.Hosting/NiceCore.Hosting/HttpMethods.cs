﻿namespace NiceCore.Hosting
{
  /// <summary>
  /// Enum for GET,POST,DELETE
  /// </summary>
  public enum HttpMethods
  {
    /// <summary>
    /// None specified
    /// </summary>
    NONE,

    /// <summary>
    /// Get 
    /// </summary>
    GET,

    /// <summary>
    /// Post
    /// </summary>
    POST,

    /// <summary>
    /// Delete
    /// </summary>
    DELETE,

    /// <summary>
    /// Put
    /// </summary>
    PUT,
  }
}
