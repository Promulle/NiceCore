﻿using NiceCore.Permissions;
using NiceCore.ServiceLocation;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace NiceCore.Hosting.Services.Impl
{
  /// <summary>
  /// Service impl for IPermissionKeyCreationServie
  /// </summary>
  [Register(InterfaceType = typeof(IPermissionKeyCreationService))]
  public class DefaultPermissionKeyCreationService : IPermissionKeyCreationService
  {
    /// <summary>
    /// All handler for permission key creation
    /// </summary>
    public List<IPermissionKeyCreationHandler> PermissionKeyCreationHandlers { get; set; } = new();
    
    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<DefaultPermissionKeyCreationService> Logger { get; set; }

    /// <summary>
    /// Create the appropriate Key
    /// </summary>
    /// <param name="i_PermissionKey"></param>
    /// <param name="i_PermissionKeyType"></param>
    /// <returns></returns>
    public IPermissionKey CreateKey(string i_PermissionKey, Type i_PermissionKeyType)
    {
      if (!typeof(IPermissionKey).IsAssignableFrom(i_PermissionKeyType))
      {
        var msg = $"Cannot create a permission key of type {i_PermissionKeyType.FullName} for key {i_PermissionKey} since it does not implement {typeof(IPermissionKey).FullName}.";
        Logger.LogError(msg);
        throw new NotSupportedException(msg);
      }
      var handler = PermissionKeyCreationHandlers.Find(r => r.PermissionKeyType.Equals(i_PermissionKeyType));
      if (handler == null)
      {
        var msg = $"Cannot create a permission key of type {i_PermissionKeyType.FullName} for key {i_PermissionKey} since there is no creation handler registered for this type. To fix this, register a handler class that implements {typeof(IPermissionKeyCreationHandler).FullName} and sets the {nameof(IPermissionKeyCreationHandler.PermissionKeyType)} property to {i_PermissionKeyType}.";
        Logger.LogError(msg);
        throw new InvalidOperationException(msg);
      }
      return handler.CreateKey(i_PermissionKey);
    }
  }
}
