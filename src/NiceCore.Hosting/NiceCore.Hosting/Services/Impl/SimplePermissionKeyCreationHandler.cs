﻿using NiceCore.Permissions.Impl;
using NiceCore.ServiceLocation;

namespace NiceCore.Hosting.Services.Impl
{
  /// <summary>
  /// Impl for creation of simple permission keys
  /// </summary>
  [Register(InterfaceType = typeof(IPermissionKeyCreationHandler))]
  public class SimplePermissionKeyCreationHandler : BasePermissionKeyCreationHandler<SimplePermissionKey>
  {
    /// <summary>
    /// Set value
    /// </summary>
    /// <param name="t_PermissionKey"></param>
    /// <param name="i_Key"></param>
    protected override void SetKey(SimplePermissionKey t_PermissionKey, string i_Key)
    {
      t_PermissionKey.PermissionKeyName = i_Key;
    }
  }
}
