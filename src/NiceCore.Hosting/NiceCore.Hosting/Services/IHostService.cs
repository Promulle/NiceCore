﻿using NiceCore.Base.Services;
using NiceCore.ServiceLocation;
using System.Threading.Tasks;

namespace NiceCore.Hosting.Services
{
  /// <summary>
  /// Interface for service that hosts and starts server
  /// </summary>
  public interface IHostService : IService
  {
    /// <summary>
    /// Container for managing registrations
    /// </summary>
    IServiceContainer Container { get; set; }

    /// <summary>
    /// starts the hostservice
    /// </summary>
    /// <param name="i_TaskCompletionSource">Task completion source that is used to signal when the webserver has booted</param>
    Task Run(TaskCompletionSource i_TaskCompletionSource);
  }
}
