﻿using NiceCore.Permissions;
using System;

namespace NiceCore.Hosting.Services
{
  /// <summary>
  /// Handler interface for possible extensions for permission Key Creation -> support for custom PermissionKey types in hosting attributes
  /// </summary>
  public interface IPermissionKeyCreationHandler
  {
    /// <summary>
    /// Type of the key
    /// </summary>
    Type PermissionKeyType { get; }

    /// <summary>
    /// Creates a key for i_Permission Key, the type of the Key should be decided by the implementation.
    /// </summary>
    /// <param name="i_PermissionKey"></param>
    /// <returns></returns>
    IPermissionKey CreateKey(string i_PermissionKey);
  }
}
