﻿using NiceCore.Hosting.Tokens;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using NiceCore.Hosting.Utils;

namespace NiceCore.Hosting.Services
{
  /// <summary>
  /// Service that uses default settings for token creation/validation
  /// </summary>
  public interface INcJWTTokenService
  {
    /// <summary>
    /// Generate a JWT Token
    /// </summary>
    /// <param name="i_UserID"></param>
    /// <returns></returns>
    NcJWTResult GenerateJWTToken(string i_UserID);

    /// <summary>
    /// Generate a JWT Token
    /// </summary>
    /// <param name="i_Claims"></param>
    /// <returns></returns>
    NcJWTResult GenerateJWTToken(IEnumerable<Func<Claim>> i_Claims);

    /// <summary>
    /// Generate a JWT Token
    /// </summary>
    /// <param name="i_UserID"></param>
    /// <param name="i_TokenSettingKeys"></param>
    /// <returns></returns>
    NcJWTResult GenerateJWTToken(string i_UserID, ITokenSettingKeys i_TokenSettingKeys);

    /// <summary>
    /// Generate a JWT Token
    /// </summary>
    /// <param name="i_Claims"></param>
    /// <param name="i_TokenSettingKeys"></param>
    /// <returns></returns>
    NcJWTResult GenerateJWTToken(IEnumerable<Func<Claim>> i_Claims, ITokenSettingKeys i_TokenSettingKeys);

    /// <summary>
    /// Validates the token
    /// </summary>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    NcTokenValidationResult ValidateJWTToken(string i_Token);

    /// <summary>
    /// Validates the token
    /// </summary>
    /// <param name="i_Token"></param>
    /// <param name="i_TokenSettingKeys"></param>
    /// <returns></returns>
    NcTokenValidationResult ValidateJWTToken(string i_Token, ITokenSettingKeys i_TokenSettingKeys);

    /// <summary>
    /// Generate token with custom claims
    /// </summary>
    /// <param name="i_Claims"></param>
    /// <returns></returns>
    NcJWTResult GenerateNotExpiringJWTToken(IEnumerable<Func<Claim>> i_Claims);

    /// <summary>
    /// Generate token with custom claims
    /// </summary>
    /// <param name="i_Claims"></param>
    /// <param name="i_TokenSettingKeys"></param>
    /// <returns></returns>
    NcJWTResult GenerateNotExpiringJWTToken(IEnumerable<Func<Claim>> i_Claims, ITokenSettingKeys i_TokenSettingKeys);
  }
}
