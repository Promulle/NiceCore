﻿using NiceCore.Base.Instances;
using NiceCore.Permissions;
using System;

namespace NiceCore.Hosting.Services
{
  /// <summary>
  /// Base impl for IPermissionKeyCreation Handler
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public abstract class BasePermissionKeyCreationHandler<T> : IPermissionKeyCreationHandler where T : class, IPermissionKey
  {
    /// <summary>
    /// Type of key
    /// </summary>
    public Type PermissionKeyType { get; } = typeof(T);

    /// <summary>
    /// Function that actually sets the value of the key instance t_PermissionKey
    /// </summary>
    /// <param name="t_PermissionKey">instance to set value to</param>
    /// <param name="i_Key">Value to set Key from</param>
    protected abstract void SetKey(T t_PermissionKey, string i_Key);

    /// <summary>
    /// Create the key
    /// </summary>
    /// <param name="i_PermissionKey"></param>
    /// <returns></returns>
    public IPermissionKey CreateKey(string i_PermissionKey)
    {
      var inst = CreateKeyInstance();
      SetKey(inst, i_PermissionKey);
      return inst;
    }

    /// <summary>
    /// Creates the instance of the key
    /// </summary>
    /// <returns></returns>
    protected virtual T CreateKeyInstance()
    {
      return NcInstanceFactory.CreateInstance<T>();
    }
  }
}
