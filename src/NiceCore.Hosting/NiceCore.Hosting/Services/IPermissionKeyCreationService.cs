﻿using NiceCore.Permissions;
using System;

namespace NiceCore.Hosting.Services
{
  /// <summary>
  /// Service that is needed to create a permission key instance from attributes
  /// </summary>
  public interface IPermissionKeyCreationService
  {
    /// <summary>
    /// Creates a permission key of type i_PermissionKeyType
    /// Throws NotSupportedException if i_PermissionKeyType cannot be assigned to IPermissionKey
    /// </summary>
    /// <param name="i_PermissionKey"></param>
    /// <param name="i_PermissionKeyType"></param>
    /// <returns></returns>
    public IPermissionKey CreateKey(string i_PermissionKey, Type i_PermissionKeyType);
  }
}
