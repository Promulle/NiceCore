﻿using NiceCore.Base.Services;

namespace NiceCore.Hosting.Services
{
  /// <summary>
  /// Interface for services that can be hosted by IHostService
  /// </summary>
  public interface IHostableService : IService
  {
  }
}
