﻿namespace NiceCore.Hosting
{
  /// <summary>
  /// interface for services that provide a runtime endpoint
  /// </summary>
  public interface IEndpointProvidingService
  {
    /// <summary>
    /// returns the endpint. without /
    /// </summary>
    /// <returns></returns>
    string GetEndpoint();
  }
}