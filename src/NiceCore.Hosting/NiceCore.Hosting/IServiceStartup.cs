﻿using NiceCore.Base;
using NiceCore.Hosting.Services;
using System.Collections.Generic;

namespace NiceCore.Hosting
{
  /// <summary>
  /// Interface for custom defined startup classes, that can map services to routes
  /// </summary>
  public interface IServiceStartup : IInitializable
  {
    /// <summary>
    /// Services that should be mapped by this startup class
    /// </summary>
    List<IHostableService> Services { get; }
  }
}
