using System;
using System.Collections.Generic;
using System.Reflection;

namespace NiceCore.Hosting
{
  /// <summary>
  /// Base impl of IControllerAssembly
  /// </summary>
  public abstract class BaseControllerAssembly : IControllerAssembly
  {
    /// <summary>
    /// Return additional assemblies
    /// </summary>
    /// <returns></returns>
    public virtual IEnumerable<Assembly> GetAdditionalAssemblies()
    {
      return Array.Empty<Assembly>();
    }
  }
}
