﻿using NiceCore.Hosting.Services;
using NiceCore.Rapid;
using NiceCore.Rapid.ApplicationBuilding;
using NiceCore.Rapid.Default;
using NiceCore.Rapid.Hosting;
using NiceCore.Rapid.Storage;
using NiceCore.Services.Config.Impl.AssemblyBased;
using NiceCore.Services.Default.Config;
using NiceCore.Services.Default.Scheduling;
using NiceCore.Storage;
using System.Threading.Tasks;
using NiceCore.Diagnostics;

namespace NiceCore.DefaultHost
{
  /// <summary>
  /// Entry class
  /// </summary>
  static class Program
  {
    /// <summary>
    /// Main function
    /// </summary>
    private static async Task Main()
    {
      DiagnosticsCollector.Instance.Configure();
      var extStore = new DefaultAppBuilderExtensionStore();
      var hostApp = await NcApplicationBuilder.Create<NcHostingApplication>()
        .UseDefaults()
        .HostService(extStore).Default()
        .StorageService(extStore).Default()
        .Configuration.Default()
        .Configuration.ConfigurationSources.Add<EntryAssemblyJSONNcConfigurationSource>()
        .ScheduleServiceHost.Default()
        .Build().ConfigureAwait(false);
      await hostApp.Run().ConfigureAwait(false);
      await hostApp.Shutdown().ConfigureAwait(false);
    }
  }
}
