﻿using NiceCore.Services.Config.Impl.AssemblyBased;

namespace NiceCore.DefaultHost
{
  /// <summary>
  /// Config builder for default host
  /// </summary>
  public class DefaultHostConfigBuilder : BaseAssemblyXMLNcConfigurationSource
  {
    /// <summary>
    /// Optional
    /// </summary>
    public override bool IsOptional { get; } = true;

    /// <summary>
    /// reload
    /// </summary>
    public override bool ReloadOnChange { get; set; } = true;

    /// <summary>
    /// prio
    /// </summary>
    public override int Priority { get; } = 0;
  }
}
