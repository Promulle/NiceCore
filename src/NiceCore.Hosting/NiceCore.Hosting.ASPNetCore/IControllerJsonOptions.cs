﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace NiceCore.Hosting.ASPNetCore
{
  /// <summary>
  /// Options that can be registered to change how controllers interact with json data
  /// </summary>
  public interface IControllerSystemTextJsonOptions
  {
    /// <summary>
    /// Options Func that configures the jsonoptions for controllers
    /// </summary>
    Action<JsonOptions> OptionsFunc { get; }
  }
}
