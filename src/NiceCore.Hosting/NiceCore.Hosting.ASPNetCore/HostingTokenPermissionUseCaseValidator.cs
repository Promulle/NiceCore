﻿using NiceCore.Hosting.Permissions;
using NiceCore.Hosting.Services;
using NiceCore.ServiceLocation;
using NiceCore.Services.Permissions;
using System.Threading.Tasks;

namespace NiceCore.Hosting.ASPNetCore
{
  /// <summary>
  /// Validator for hosting use cases involving tokens
  /// </summary>
  [Register(InterfaceType = typeof(IPermissionUseCaseValidator))]
  public class HostingTokenPermissionUseCaseValidator : BasePermissionUseCaseValidator<PermissionHostingTokenUseCase>
  {
    /// <summary>
    /// Configuration
    /// </summary>
    public INcJWTTokenService TokenService { get; set; }

    /// <summary>
    /// Validate
    /// </summary>
    /// <param name="i_UseCase"></param>
    /// <returns></returns>
    protected override Task<bool> ValidateInternal(PermissionHostingTokenUseCase i_UseCase)
    {
      return Task.FromResult(TokenService.ValidateJWTToken(i_UseCase.Token).Success);
    }
  }
}
