using Microsoft.AspNetCore.Mvc;

namespace NiceCore.Hosting.ASPNetCore
{
  /// <summary>
  /// Nc Api Behavior Options Configurator
  /// </summary>
  public interface INcApiBehaviorOptionsConfigurator
  {
    /// <summary>
    /// Apply the configurator to i_Options
    /// </summary>
    /// <param name="i_Options"></param>
    void Apply(ApiBehaviorOptions i_Options);
  }
}