﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NiceCore.Base.Services;
using NiceCore.Hosting.Services;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using NiceCore.Hosting.ASPNetCore.Config.Model;
using NiceCore.Hosting.ASPNetCore.Config.Settings;
using NiceCore.Hosting.ASPNetCore.Plugins;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation.Helpers;
using IServiceContainer = NiceCore.ServiceLocation.IServiceContainer;

namespace NiceCore.Hosting.ASPNetCore
{
  /// <summary>
  /// Baseimpl of hostservice
  /// </summary>
  public abstract class BaseHostService : BaseService, IHostService
  {
    private const string HOSTING_ASP_MODE = "Hosting.Asp.ServerMode";
    private const string HOSTING_USE_NC_CONTAINER = "NiceCore.Hosting.Asp.UseNiceCoreContainer";
    private const string HOSTING_MODE_KESTREL = "Kestrel";
    private const string HOSTING_MODE_IIS = "IIS";
    private const string HOSTING_MODE_IIS_INTEGRATION = "IISIntegration";
    private const string HOSTING_MODE_IIS_BOTH = "IISBoth";

    /// <summary>
    /// Service startup
    /// </summary>
    public IAspNetStartup ServiceStartup { get; set; }

    /// <summary>
    /// Configuration
    /// </summary>
    public INcConfiguration Configuration { get; set; }

    /// <summary>
    /// Host that runs WebServer
    /// </summary>
    public IHost WebHostInstance { get; set; }

    /// <summary>
    /// Container
    /// </summary>
    public IServiceContainer Container { get; set; }
    
    /// <summary>
    /// Logger Factory
    /// </summary>
    public ILoggerFactory LoggerFactory { get; set; }
    
    /// <summary>
    /// logger
    /// </summary>
    public abstract ILogger Logger { get;}

    /// <summary>
    /// Prepares an webhost builder with i_Startup
    /// </summary>
    /// <param name="i_Startup"></param>
    /// <returns></returns>
    protected virtual IHostBuilder PrepareBuilder(IAspNetStartup i_Startup)
    {
      Logger.Information("Starting to prepare builder for Webhost");
      try
      {
        var builder = Host.CreateDefaultBuilder()
          .ConfigureWebHostDefaults(webBuilder =>
          {
            if (Configuration != null)
            {
              Logger.Information("Setting configuration of asp to be nc configuration");
              webBuilder.UseConfiguration(Configuration);
              var useUrls = Configuration.GetValue<string>("NiceCore:Hosting:Asp:Urls", null);
              if (useUrls != null)
              {
                Logger.Information($"Configuring urls to be used: {useUrls}");
                webBuilder.UseUrls(useUrls);
              }
            }

            PrepareWebHostBuilder(webBuilder);
            webBuilder.UseStartup(_ => i_Startup);
            ConfigureWebHostWithPlugins(webBuilder);
          }).ConfigureLogging(logger =>
          {
            logger.ClearProviders();
            logger.Services.AddSingleton<ILoggerFactory>(LoggerFactory);
            var loggerReg = logger.Services.FirstOrDefault(r => r.ServiceType == typeof(ILogger<>));
            if (loggerReg != null)
            {
              Logger.Information("Remove wrong loggers...");
              logger.Services.Remove(loggerReg);
            }
          });
        ApplyNiceCoreContainerIfConfigured(builder);
        ApplyWindowsServiceIfConfigured(builder);
        return builder;
      }
      catch (Exception ex)
      {
        Logger.Critical(ex, "An Error occurred during prepation of IHostBuilder. Cannot start server.");
        throw;
      }
    }

    private void ConfigureWebHostWithPlugins(IWebHostBuilder t_Builder)
    {
      using var plugins = Container.LocateAll<INcWebHostBuildingPlugin>();
      if (plugins.HasInstances())
      {
        foreach (var plugin in plugins.Instances())
        {
          plugin.Configure(t_Builder, Configuration);
        }  
      }
    }

    private void ApplyWindowsServiceIfConfigured(IHostBuilder i_HostBuilder)
    {
      var settings = Configuration?.GetValue<NcWindowsServiceSettingModel>(NcWindowsServiceSetting.KEY, null);
      if (settings != null && settings.UseWindowsService)
      {
        var serviceName = settings.ServiceName ?? "NiceCore Hosted Service";
        Logger.Information($"Enabling use of server as windows service with name: {serviceName}");
        i_HostBuilder.UseWindowsService(options =>
        {
          options.ServiceName = serviceName;
        });
      }
    }

    private void ApplyNiceCoreContainerIfConfigured(IHostBuilder i_HostBuilder)
    {
      if (Configuration?.GetValue(HOSTING_USE_NC_CONTAINER, false) ?? false)
      {
        Logger.Information($"Setting ServiceProvider: Using NiceCore ServiceContainer of type: {Container.GetType().FullName}");
        i_HostBuilder.UseServiceProviderFactory(new NcServiceProviderFactory(Container));
      }
    }

    /// <summary>
    /// Settings for web builder
    /// </summary>
    /// <param name="t_Builder"></param>
    protected virtual void PrepareWebHostBuilder(IWebHostBuilder t_Builder)
    {
      var mode = Configuration?.GetValue<string>(HOSTING_ASP_MODE, null);
      Logger.Debug($"Server mode configured as: {mode}. Configuration service found: {Configuration != null}");
      if (string.IsNullOrEmpty(mode) || mode.Equals(HOSTING_MODE_KESTREL, StringComparison.OrdinalIgnoreCase))
      {
        t_Builder.UseKestrel();
      }
      else if (mode.Equals(HOSTING_MODE_IIS, StringComparison.OrdinalIgnoreCase))
      {
        t_Builder.UseIIS();
      }
      else if (mode.Equals(HOSTING_MODE_IIS_INTEGRATION, StringComparison.OrdinalIgnoreCase))
      {
        t_Builder.UseIISIntegration();
      }
      else if (mode.Equals(HOSTING_MODE_IIS_BOTH, StringComparison.OrdinalIgnoreCase))
      {
        t_Builder.UseIIS();
        t_Builder.UseIISIntegration();
      }
      else
      {
        Logger.Critical($"The specified hosting mode ({mode}) is invalid. Available: {HOSTING_MODE_KESTREL}(default), {HOSTING_MODE_IIS}, {HOSTING_MODE_IIS_INTEGRATION}, {HOSTING_MODE_IIS_BOTH}. This might cause the server to not boot at all.");
      }
    }

    /// <summary>
    /// Runs the host
    /// </summary>
    /// <param name="i_TaskCompletionSource">Task completion source that is used to signal when the webserver has booted</param>
    public Task Run(TaskCompletionSource i_TaskCompletionSource)
    {
      Logger.Information("Starting webhost...");
      ServiceStartup.BootCompletionSource = i_TaskCompletionSource;
      try
      {
        WebHostInstance = PrepareBuilder(ServiceStartup)
          .Build();
      }
      catch (Exception ex)
      {
        Logger.Critical(ex, "Something went wrong during Building of webhost, cannot start server.");
        throw;
      }

      try
      {
        return WebHostInstance.RunAsync();
      }
      catch (Exception ex)
      {
        Logger.Critical(ex, "An error occurred during RunAsync of WebHostInstance. Server will not work anymore.");
        throw;
      }
    }

    /// <summary>
    /// Dipose override
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    protected override void Dispose(bool i_IsDisposing)
    {
      Logger.Information("Stopping webhost...");
      WebHostInstance.StopAsync().GetAwaiter().GetResult();
      base.Dispose(i_IsDisposing);
    }
  }
}
