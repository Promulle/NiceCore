namespace NiceCore.Hosting.ASPNetCore.Config.Model
{
  /// <summary>
  /// Server is Alive Setting Model
  /// </summary>
  public sealed class ServerIsAliveEndpointSettingModel
  {
    /// <summary>
    /// Default Endpoint Path
    /// </summary>
    public const string DEFAULT_ENDPOINT_PATH = "/isAlive";
    
    /// <summary>
    /// Endpoint enabled
    /// </summary>
    public bool? EndpointEnabled { get; set; }
    
    /// <summary>
    /// Endpoint path - optional
    /// </summary>
    public string EndpointPath { get; set; }
  }
}