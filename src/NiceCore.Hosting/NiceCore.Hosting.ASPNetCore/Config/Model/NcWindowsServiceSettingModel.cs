namespace NiceCore.Hosting.ASPNetCore.Config.Model
{
  /// <summary>
  /// Windows service setting model
  /// </summary>
  public class NcWindowsServiceSettingModel
  {
    /// <summary>
    /// Enables hosting as windows service
    /// </summary>
    public bool UseWindowsService { get; set; }
    
    /// <summary>
    /// Service Name
    /// </summary>
    public string ServiceName { get; set; }
  }
}
