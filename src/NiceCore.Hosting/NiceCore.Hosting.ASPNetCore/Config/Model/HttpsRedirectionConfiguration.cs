﻿namespace NiceCore.Hosting.ASPNetCore.Config.Model
{
  /// <summary>
  /// Config model for Https Redirectiin
  /// </summary>
  public class HttpsRedirectionConfiguration
  {
    /// <summary>
    /// Is HTTPS redirection enabled
    /// </summary>
    public bool UseHttpsRedirection { get; set; }

    /// <summary>
    /// Https port to redirect to. Must be filled.
    /// </summary>
    public int? Port { get; set; }

    /// <summary>
    /// If the redirection request should send HttpStatusCode.PermanentRedirect or HttpStatusCode.TemporaryRedirect
    /// </summary>
    public bool UseTemporaryStatusCode { get; set; } = true;
  }
}
