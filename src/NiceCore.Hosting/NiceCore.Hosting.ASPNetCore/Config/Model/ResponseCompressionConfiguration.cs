﻿using System.IO.Compression;

namespace NiceCore.Hosting.ASPNetCore.Config.Model
{
  /// <summary>
  /// Type retrieved when searching for compression settings
  /// </summary>
  public class ResponseCompressionConfiguration
  {
    /// <summary>
    /// Whether response compression should be used
    /// </summary>
    public bool UseResponseCompression { get; set; }

    /// <summary>
    /// Whether https responses should be compressed too
    /// </summary>
    public bool EnableForHTTPS { get; set; }

    /// <summary>
    /// level to be used for compression
    /// </summary>
    public CompressionLevel Level { get; set; }
  }
}
