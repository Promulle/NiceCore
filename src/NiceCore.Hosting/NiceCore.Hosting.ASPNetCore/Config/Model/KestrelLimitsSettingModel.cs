namespace NiceCore.Hosting.ASPNetCore.Config.Model
{
  /// <summary>
  /// Kestrel Limits Setting Model
  /// </summary>
  public sealed class KestrelLimitsSettingModel
  {
    /// <summary>
    /// Count Of Streams per HTTP2 Connection
    /// </summary>
    public int? Http2MaxStreamCount { get; set; }
    
    /// <summary>
    /// Max Concurrent Connections
    /// </summary>
    public long? MaxConcurrentConnections { get; set; }
    
    /// <summary>
    /// Max Concurrent Upgraded Connections
    /// </summary>
    public long? MaxConcurrentUpgradedConnections { get; set; }
  }
}