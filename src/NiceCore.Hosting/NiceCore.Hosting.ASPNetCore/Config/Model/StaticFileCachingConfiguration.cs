﻿using NiceCore.Base;
using System.Collections.Generic;

namespace NiceCore.Hosting.ASPNetCore.Config.Model
{
  /// <summary>
  /// Config Model for settings related to caching static files
  /// </summary>
  public class StaticFileCachingConfiguration
  {
    /// <summary>
    /// Whether to use caching for static files or not
    /// </summary>
    public bool UseStaticFileCaching { get; set; }

    /// <summary>
    /// Whether the requests to root paths should be cached. This is only used if RootPath != null and not empty
    /// </summary>
    public bool CacheRoot { get; set; }

    /// <summary>
    /// Paths to check if the request is root, this is only used of CacheRoot is false
    /// </summary>
    public List<string> RootPaths { get; set; }
    
    /// <summary>
    /// Paths to check if the static result of a request should be cached. Uses startWith for compare
    /// </summary>
    public List<string> CacheExclusionStartPaths { get; set; }

    /// <summary>
    /// Duration until the cache expires
    /// </summary>
    public long ExpireDuration { get; set; }

    /// <summary>
    /// Time Kind for duration
    /// </summary>
    public TimeKind ExpireDurationKind { get; set; }
  }
}
