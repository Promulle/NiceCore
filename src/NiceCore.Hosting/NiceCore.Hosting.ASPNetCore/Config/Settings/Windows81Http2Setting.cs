using System;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Hosting.ASPNetCore.Config.Settings
{
  /// <summary>
  /// Setting to enable http2 on windows8.1 systems that have the correct patch
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public sealed class Windows81Http2Setting : IConfigurableSettingModel
  {
    /// <summary>
    /// Key
    /// </summary>
    public const string KEY = "NiceCore.Hosting.Asp.EnableWindows81Http2";

    /// <summary>
    /// Key
    /// </summary>
    public string Key { get; } = KEY;

    /// <summary>
    /// Setting Type
    /// </summary>
    public Type SettingType { get; } = typeof(bool);
  }
}