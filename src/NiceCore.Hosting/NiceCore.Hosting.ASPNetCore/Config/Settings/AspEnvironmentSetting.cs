using System;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Hosting.ASPNetCore.Config.Settings
{
  /// <summary>
  /// Asp Environment setting, overrides the default environment values provided by DOTNET_ENVIRONMENT and ASPNETCORE_ENVIRONMENT
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class AspEnvironmentSetting: IConfigurableSettingModel
  {
    /// <summary>
    /// KEY
    /// </summary>
    public static readonly string KEY = "NiceCore.Hosting.Asp.Environment";

    /// <summary>
    /// Value Development
    /// </summary>
    public static readonly string VALUE_DEVELOPMENT = "Development";

    /// <summary>
    /// Value Production
    /// </summary>
    public static readonly string VALUE_PRODUCTION = "Production";

    /// <summary>
    /// Value Demo System
    /// </summary>
    public static readonly string VALUE_DEMO = "Demo";

    /// <summary>
    /// Key
    /// </summary>
    public string Key { get; } = KEY;

    /// <summary>
    /// Setting Type
    /// </summary>
    public Type SettingType { get; } = typeof(string);
  }
}