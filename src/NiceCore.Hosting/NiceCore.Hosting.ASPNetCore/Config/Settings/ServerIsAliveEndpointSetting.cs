using System;
using NiceCore.Hosting.ASPNetCore.Config.Model;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Hosting.ASPNetCore.Config.Settings
{
  /// <summary>
  /// Server is Alive Endpoint Setting
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class ServerIsAliveEndpointSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// Key
    /// </summary>
    public const string KEY = "NiceCore.Hosting.ASP.Endpoints.IsAliveEndpoint";

    /// <summary>
    /// key
    /// </summary>
    public string Key { get; } = KEY;

    /// <summary>
    /// Setting Type
    /// </summary>
    public Type SettingType { get; } = typeof(ServerIsAliveEndpointSettingModel);
  }
}