using System;
using NiceCore.Hosting.ASPNetCore.Config.Model;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Hosting.ASPNetCore.Config.Settings
{
  /// <summary>
  /// Settings for limits in Kestrel Server
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class KestrelLimitsSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// KEY of setting
    /// </summary>
    public const string KEY = "NiceCore.Hosting.ASP.KestrelLimits";

    /// <summary>
    /// Key
    /// </summary>
    public string Key { get; } = KEY;

    /// <summary>
    /// Type of setting
    /// </summary>
    public Type SettingType { get; } = typeof(KestrelLimitsSettingModel);
  }
}