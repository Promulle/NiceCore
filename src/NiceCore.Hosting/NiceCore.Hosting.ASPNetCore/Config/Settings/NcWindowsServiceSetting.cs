using System;
using NiceCore.Hosting.ASPNetCore.Config.Model;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Hosting.ASPNetCore.Config.Settings
{
  /// <summary>
  /// Windows Service Setting
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class NcWindowsServiceSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// Key
    /// </summary>
    public static readonly string KEY = "NiceCore.Hosting.ASP.WindowsService";

    /// <summary>
    /// Key
    /// </summary>
    public string Key { get; } = KEY;

    /// <summary>
    /// Setting Type
    /// </summary>
    public Type SettingType { get; } = typeof(NcWindowsServiceSettingModel);
  }
}
