﻿using NiceCore.Hosting.ASPNetCore.Config.Model;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using System;

namespace NiceCore.Hosting.ASPNetCore.Config.Settings
{
  /// <summary>
  /// Setting that determines how the server handles caching of static files
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class StaticFileCachingSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// Key for Static file caching
    /// </summary>
    public static readonly string KEY = "NiceCore.Hosting.Asp.StaticFileCaching";

    /// <summary>
    /// Key
    /// </summary>
    public string Key { get; } = KEY;

    /// <summary>
    /// Setting Type
    /// </summary>
    public Type SettingType { get; } = typeof(StaticFileCachingConfiguration);
  }
}
