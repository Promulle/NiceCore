using System;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Hosting.ASPNetCore.Config.Settings
{
  /// <summary>
  /// ASP Metric Setting
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class AspMetricsSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// Key
    /// </summary>
    public const string KEY = "NiceCore.Hosting.ASP.EnableDefaultMetricCollection";

    /// <summary>
    /// key
    /// </summary>
    public string Key { get; } = KEY;
    
    /// <summary>
    /// Setting Type
    /// </summary>
    public Type SettingType { get; } = typeof(bool);
  }
}