﻿using NiceCore.Hosting.ASPNetCore.Config.Model;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using System;

namespace NiceCore.Hosting.ASPNetCore.Config.Settings
{
  /// <summary>
  /// Setting for https redirection
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class HttpsRedirectionSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// Key of setting
    /// </summary>
    public static readonly string KEY = "NiceCore.Hosting.ASP.HttpsRedirect";

    /// <summary>
    /// KEY
    /// </summary>
    public string Key { get; } = KEY;

    /// <summary>
    /// Type of setting
    /// </summary>
    public Type SettingType { get; } = typeof(HttpsRedirectionConfiguration);
  }
}
