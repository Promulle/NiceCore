using System;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Hosting.ASPNetCore.Config.Settings
{
  /// <summary>
  /// Setting whether or not to force authorization on all controllers
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class ForceControllerAuthorizationSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// KEY
    /// </summary>
    public static readonly string KEY = "NiceCore.Hosting.ASP.ForceControllerAuthorization";

    /// <summary>
    /// Key
    /// </summary>
    public string Key { get; } = KEY;

    /// <summary>
    /// Bool
    /// </summary>
    public Type SettingType { get; } = typeof(bool);
  }
}
