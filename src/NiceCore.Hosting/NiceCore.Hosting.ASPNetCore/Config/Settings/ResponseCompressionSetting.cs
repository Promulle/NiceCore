﻿using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using System;
using NiceCore.Hosting.ASPNetCore.Config.Model;

namespace NiceCore.Hosting.ASPNetCore.Config.Settings
{
  /// <summary>
  /// Setting that configures Response compression
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class ResponseCompressionSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// Key
    /// </summary>
    public static readonly string KEY = "NiceCore.Hosting.Asp.ResponseCompression";

    /// <summary>
    /// Key
    /// </summary>
    public string Key { get; } = KEY;

    /// <summary>
    /// Setting Type
    /// </summary>
    public Type SettingType { get; } = typeof(ResponseCompressionConfiguration);
  }
}
