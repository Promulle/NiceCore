﻿using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace NiceCore.Hosting.ASPNetCore
{
  /// <summary>
  /// Events that can be registered, which are used by startup for setting specific event handling
  /// </summary>
  public interface IJwtEvents
  {
    /// <summary>
    /// Returns events to be used
    /// </summary>
    /// <returns></returns>
    JwtBearerEvents GetEvents();
  }
}
