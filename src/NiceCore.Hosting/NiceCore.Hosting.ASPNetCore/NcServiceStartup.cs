﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Net.Http.Headers;
using Microsoft.OpenApi.Models;
using NiceCore.Base;
using NiceCore.Hosting.ASPNetCore.Config.Model;
using NiceCore.Hosting.ASPNetCore.Config.Settings;
using NiceCore.Hosting.Services;
using NiceCore.Hosting.Tokens;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using NiceCore.Extensions;
using NiceCore.Hosting.ASPNetCore.Configurability;
using NiceCore.Hosting.ASPNetCore.Configurability.Auth;
using NiceCore.Hosting.ASPNetCore.Configurability.Cors;
using NiceCore.Hosting.ASPNetCore.Extensions;
using NiceCore.Hosting.ASPNetCore.Filters.DefaultHttpContextTraceIdentifiers;
using NiceCore.Hosting.ASPNetCore.Json;
using NiceCore.Hosting.ASPNetCore.StartupModules.Metrics;
using NiceCore.Hosting.ASPNetCore.StartupModules.StaticFiles;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation.Helpers;
using OpenTelemetry.Metrics;

namespace NiceCore.Hosting.ASPNetCore
{
  /// <summary>
  /// Base for service startups
  /// </summary>
  [Register(InterfaceType = typeof(IAspNetStartup))]
  public class NcServiceStartup : IAspNetStartup
  {
    private const string CONFIG_KEY_USE_INSIGHTS = "NiceCore.Hosting.Asp.UseApplicationInsights";

    /// <summary>
    /// Services that should be mapped during configure
    /// </summary>
    public List<IHostableService> Services { get; } = new();

    /// <summary>
    /// Objects that determine assemblies that are used to search for controllers
    /// </summary>
    public List<IControllerAssembly> ControllerAssemblies { get; } = new();

    /// <summary>
    /// PermissionKeyCreationService
    /// </summary>
    public IPermissionKeyCreationService PermissionKeyCreationService { get; set; }

    /// <summary>
    /// Configuration
    /// </summary>
    public INcConfiguration Configuration { get; set; }

    /// <summary>
    /// Options accessor
    /// </summary>
    public IJwtTokenOptionsAccessor JwtTokenOptionsAccessor { get; set; }

    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<NcServiceStartup> Logger { get; set; }

    /// <summary>
    /// Whether this startup is initialized or not
    /// </summary>
    public bool IsInitialized { get; private set; }

    /// <summary>
    /// Container
    /// </summary>
    public IServiceContainer Container { get; set; }

    /// <summary>
    /// JsonSerializer Support
    /// </summary>
    public IJsonSerializerSupport JsonSerializerSupport { get; set; }

    /// <summary>
    /// Json use function provider
    /// </summary>
    public IJsonUseFunctionProvider JsonUseFunctionProvider { get; set; }

    /// <summary>
    /// NcApiBehaviorOptionsConfigurator
    /// </summary>
    public INcApiBehaviorOptionsConfigurator NcApiBehaviorOptionsConfigurator { get; set; }

    /// <summary>
    /// Task Completion source for boot of the webserver
    /// </summary>
    public TaskCompletionSource BootCompletionSource { get; set; }

    /// <summary>
    /// Setting for response compression
    /// </summary>
    public ResponseCompressionConfiguration ResponseCompressionConfiguration { get; set; }

    /// <summary>
    /// Setting for https redirection
    /// </summary>
    public HttpsRedirectionConfiguration HttpsRedirectionConfiguration { get; set; }

    /// <summary>
    /// Kestrel Limit Settings
    /// </summary>
    public KestrelLimitsSettingModel KestrelLimitsSettings { get; set; }

    /// <summary>
    /// Server Is Alive Endpoint Setting
    /// </summary>
    public ServerIsAliveEndpointSettingModel ServerIsAliveEndpointSettings { get; set; }

    /// <summary>
    /// Configured Cors Support
    /// </summary>
    public IConfiguredCorsSupport ConfiguredCorsSupport { get; set; }
    
    /// <summary>
    /// Static Files Startup module
    /// </summary>
    public IStaticFilesStartupModule StaticFilesStartupModule { get; set; }
    
    /// <summary>
    /// Metrics Startup Module
    /// </summary>
    public IMetricsStartupModule MetricsStartupModule { get; set; }

    /// <summary>
    /// Force the authorization on controllers that dont have it
    /// </summary>
    public bool? ForceControllerAuthorization { get; set; }
    
    /// <summary>
    /// Is Development Environment
    /// </summary>
    public bool IsDevelopmentEnvironment { get; set; }

    /// <summary>
    /// Initialize
    /// </summary>
    public void Initialize()
    {
      if (!IsInitialized)
      {
        LoadConfigurations();
        Services.AddRange(Container.ResolveAll<IHostableService>());
        ControllerAssemblies.AddRange(Container.ResolveAll<IControllerAssembly>());
        IsInitialized = true;
      }
    }

    private void LoadConfigurations()
    {
      StaticFilesStartupModule.Configure();
      MetricsStartupModule.Configure();
      ResponseCompressionConfiguration = Configuration?.GetValue<ResponseCompressionConfiguration>(ResponseCompressionSetting.KEY, null);
      HttpsRedirectionConfiguration = Configuration?.GetValue<HttpsRedirectionConfiguration>(HttpsRedirectionSetting.KEY, null);
      ForceControllerAuthorization = Configuration?.GetValue(ForceControllerAuthorizationSetting.KEY, false);
      KestrelLimitsSettings = Configuration?.GetValue<KestrelLimitsSettingModel>(KestrelLimitsSetting.KEY, null);
      IsDevelopmentEnvironment = IsDevEnvironment();
      ServerIsAliveEndpointSettings = Configuration?.GetValue<ServerIsAliveEndpointSettingModel>(ServerIsAliveEndpointSetting.KEY, null);
      ConfiguredCorsSupport?.Initialize();
      if (Configuration?.GetValue(Windows81Http2Setting.KEY, false) == true)
      {
        AppContext.SetSwitch("Microsoft.AspNetCore.Server.Kestrel.EnableWindows81Http2", true);
      }
    }

    /// <summary>
    /// Configure NiceCore routing
    /// </summary>
    /// <param name="i_Application"></param>
    protected virtual void ConfigureRouting(IApplicationBuilder i_Application)
    {
      i_Application.UseEndpoints(routeBuilder =>
      {
        var builder = routeBuilder.MapControllers();
        ApplyForceAuthorizationIfNeeded(builder);
        ApplyServerIsAliveEndpointSettings(routeBuilder);
      });
    }

    private void ApplyServerIsAliveEndpointSettings(IEndpointRouteBuilder i_Builder)
    {
      if (ServerIsAliveEndpointSettings?.EndpointEnabled == true)
      {
        var pattern = ServerIsAliveEndpointSettings.EndpointPath ?? ServerIsAliveEndpointSettingModel.DEFAULT_ENDPOINT_PATH;
        i_Builder.MapGet(pattern, async (ctx) =>
        {
          ctx.Response.StatusCode = (int) HttpStatusCode.OK;
          await ctx.Response.CompleteAsync().ConfigureAwait(false);
        });
      }
    }

    private void ApplyForceAuthorizationIfNeeded(ControllerActionEndpointConventionBuilder t_Builder)
    {
      if (ForceControllerAuthorization == true)
      {
        t_Builder.RequireAuthorization();
      }
    }


    /// <summary>
    /// prepare app builder
    /// </summary>
    /// <param name="t_Builder"></param>
    /// <returns></returns>
    protected virtual IApplicationBuilder PrepareApplicationBuilder(IApplicationBuilder t_Builder)
    {
      if (HttpsRedirectionConfiguration?.UseHttpsRedirection == true)
      {
        Logger.Information("Initializing https redirection");
        t_Builder.UseHttpsRedirection();
      }
      MetricsStartupModule.ApplyMetrics(t_Builder);
      
      t_Builder.UseRouting();
      
      t_Builder.UseAuthentication();
      t_Builder.UseAuthorization();
      t_Builder.UseDefaultFiles();
      Logger.Information("Configuring static files...");
      ConfigureCors(t_Builder);
      StaticFilesStartupModule.ConfigureUseStaticFiles(t_Builder);
      return t_Builder.UseForwardedHeaders(new ForwardedHeadersOptions
      {
        ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto,
      });
    }

    private void ConfigureCors(IApplicationBuilder t_Builder)
    {
      var corsConfigProvider = Container.Resolve<ICorsSettingsProvider>();
      if (corsConfigProvider != null)
      {
        Logger.Information($"Applying cors settings by type {corsConfigProvider.GetType().FullName}");
        corsConfigProvider.ApplyCorsSettings(t_Builder);
        Container.Release(corsConfigProvider);
      }
      else if (ConfiguredCorsSupport != null)
      {
        Logger.Information("Using ConfiguredCorsSupport for CORS Settings");
        ConfiguredCorsSupport.ApplyCorsBuilder(t_Builder);
      }
      else
      {
        Logger.Information("No settings were registered for CORS. Server will use default settings.");
      }
    }

    /// <summary>
    /// Prepare Services
    /// </summary>
    /// <param name="i_Services"></param>
    /// <returns></returns>
    protected virtual IServiceCollection PrepareServices(IServiceCollection i_Services)
    {
      return i_Services
        .Configure<ForwardedHeadersOptions>(options =>
        {
          options.ForwardedHeaders =
            ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
        });
    }

    /// <summary>
    /// Method for configuring services
    /// </summary>
    /// <param name="i_Services"></param>
    /// <returns></returns>
    public void ConfigureServices(IServiceCollection i_Services)
    {
      Logger.Information($"Starting {nameof(ConfigureServices)}");
      StaticFilesStartupModule.ApplyDistributedMemoryCacheIfNeeded(i_Services);
      // If using Kestrel:
      i_Services.Configure<KestrelServerOptions>(options =>
      {
        if (KestrelLimitsSettings != null)
        {
          if (KestrelLimitsSettings.MaxConcurrentConnections != null)
          {
            options.Limits.MaxConcurrentConnections = KestrelLimitsSettings.MaxConcurrentConnections;
          }

          if (KestrelLimitsSettings.Http2MaxStreamCount != null)
          {
            options.Limits.Http2.MaxStreamsPerConnection = KestrelLimitsSettings.Http2MaxStreamCount.Value;
          }

          if (KestrelLimitsSettings.MaxConcurrentUpgradedConnections != null)
          {
            options.Limits.MaxConcurrentUpgradedConnections = KestrelLimitsSettings.MaxConcurrentUpgradedConnections;
          }
        }

        options.AllowSynchronousIO = true;
      });

      // If using IIS:
      i_Services.Configure<IISServerOptions>(options => options.AllowSynchronousIO = true);
      MetricsStartupModule.AddMetrics(i_Services);
      
      if (NcApiBehaviorOptionsConfigurator != null)
      {
        i_Services.Configure<ApiBehaviorOptions>(options =>
        {
          Logger.Information($"Applying {nameof(INcApiBehaviorOptionsConfigurator)} of type {NcApiBehaviorOptionsConfigurator.GetType()}");
          NcApiBehaviorOptionsConfigurator.Apply(options);
        });
      }

      ApplyHttpsRedirectionIfNeeded(i_Services);
      ApplyResponseCompressionIfNeeded(i_Services);
      if (IsDevelopmentEnvironment)
      {
        Logger.Information("Initializing Swagger...");
        i_Services.AddSwaggerGen(c => c.SwaggerDoc("v1", new OpenApiInfo {Title = "NiceCore", Version = "v1"}));
      }

      ApplyAdditionalServicesBeforeAddControllers(i_Services);
      var controllerBuilder = i_Services.AddControllers(mvcOptions =>
      {
        AddDefaultHttpContextTraceIdentifierFilterIfNeeded(mvcOptions);
        Logger.Information("Applying json support");
        JsonSerializerSupport.ApplyJsonSupport(mvcOptions, Container);
        using var controllerOptions = Container.LocateAll<INcControllerOptionConfigurator>();
        if (controllerOptions.HasInstances())
        {
          foreach (var configurator in controllerOptions.Instances())
          {
            Logger.Information($"Applying {nameof(INcControllerOptionConfigurator)} of type {configurator.GetType()}");
            configurator.ApplyOptions(mvcOptions);
          }
        }
      }).AddControllersAsServices();
      ConfiguredCorsSupport?.ApplyCorsBuilder(i_Services);
      AddApplicationPartsForAssemblies(controllerBuilder);
      JsonUseFunctionProvider.ApplyJsonUseFunctions(controllerBuilder);
      i_Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
      NcAuthorizationSettingProvider.ApplyAuthorization(i_Services, Logger, Container);
      NcAuthenticationSettingProvider.ApplyAuthentication(i_Services, Container, JwtTokenOptionsAccessor, Logger);
      if (Configuration?.GetValue(CONFIG_KEY_USE_INSIGHTS, false) ?? false)
      {
        Logger.Information("Add application insights for azure");
        //i_Services.AddApplicationInsightsTelemetry();
      }

      PrepareServices(i_Services);
      Logger.Information($"{nameof(ConfigureServices)} finished.");
    }

    private void AddDefaultHttpContextTraceIdentifierFilterIfNeeded(MvcOptions t_Options)
    {
      Logger.Information("Applying DefaultHttpContextTraceIdentifierFilter if needed.");
      var setting = Configuration?.GetValue(DefaultHttpContextTraceIdentifierFilterSetting.KEY, DefaultHttpContextTraceIdentifierFilterSettingModel.Default());
      if (setting != null && setting.Use)
      {
        Logger.Information($"Applying DefaultHttpContextTraceIdentifierFilter with header name: {setting.HeaderName}");
        t_Options.Filters.Add(new DefaultHttpContextTraceIdentifierFilter(setting.HeaderName));
      }
    }

    private void ApplyAdditionalServicesBeforeAddControllers(IServiceCollection t_Services)
    {
      var additionalServices = Container.ResolveAll<IAdditionalServicesBeforeAddControllers>();
      foreach (var services in additionalServices)
      {
        Logger.Information($"Registering additional services from class: {services.GetType()}");
        services.AddServices(t_Services);
      }
    }

    private void ApplyHttpsRedirectionIfNeeded(IServiceCollection t_Services)
    {
      if (HttpsRedirectionConfiguration?.UseHttpsRedirection == true)
      {
        Logger.Information("Applying HttpsRedirection");
        t_Services.AddHttpsRedirection(options =>
        {
          options.RedirectStatusCode = HttpsRedirectionConfiguration.UseTemporaryStatusCode ? (int) HttpStatusCode.TemporaryRedirect : (int) HttpStatusCode.PermanentRedirect;
          if (options.HttpsPort != null && HttpsRedirectionConfiguration.Port != null)
          {
            Logger.Information($"Applying HttpsRedirection for port {HttpsRedirectionConfiguration.Port.Value}");
            options.HttpsPort = HttpsRedirectionConfiguration.Port.Value;
          }
        });
      }
    }

    private void ApplyResponseCompressionIfNeeded(IServiceCollection t_Services)
    {
      if (ResponseCompressionConfiguration?.UseResponseCompression == true)
      {
        Logger.Information($"Response compression is enabled, using compression of level: {ResponseCompressionConfiguration.Level} for https too: {ResponseCompressionConfiguration.EnableForHTTPS}");
        t_Services.Configure<GzipCompressionProviderOptions>(options => options.Level = ResponseCompressionConfiguration.Level);
        t_Services.Configure<BrotliCompressionProviderOptions>(options => options.Level = ResponseCompressionConfiguration.Level);
        t_Services.AddResponseCompression(options =>
        {
          options.EnableForHttps = ResponseCompressionConfiguration.EnableForHTTPS;
          options.Providers.Add<BrotliCompressionProvider>();
          options.Providers.Add<GzipCompressionProvider>();
        });
      }
      else
      {
        Logger.Information("Response compression is not enabled.");
      }
    }

    private void AddApplicationPartsForAssemblies(IMvcBuilder t_Builder)
    {
      ControllerAssemblies.ForEach(controllerAssembly =>
      {
        Logger.Debug($"Adding assembly of type {controllerAssembly.GetType().FullName} to assemblies to search for controllers.");
        t_Builder.AddApplicationPart(controllerAssembly.GetType().Assembly);
        controllerAssembly.GetAdditionalAssemblies().ForEachItem(assembly =>
        {
          Logger.Debug($"Adding additional assembly {assembly.FullName} to assemblies to search for controllers");
          t_Builder.AddApplicationPart(assembly);
        });
      });
    }

    /// <summary>
    /// Configure
    /// </summary>
    /// <param name="app"></param>
    /// <param name="i_HostApplicationLifetime"></param>
    public void Configure(IApplicationBuilder app, IHostApplicationLifetime i_HostApplicationLifetime)
    {
      Logger.Information($"Start of {nameof(Configure)} (Building the asp Application)");
      Logger.Information("Registering ApplicationStarted event");
      // app.Use(async (context, nextFunc) =>
      // {
      //   Console.WriteLine(context.Request.Path);
      //   await nextFunc.Invoke(context).ConfigureAwait(false);
      // });
      i_HostApplicationLifetime.ApplicationStarted.Register(() =>
      {
        if (BootCompletionSource != null)
        {
          BootCompletionSource.SetResult();
        }

        const string serverReadyMessage = "### Server is now ready to receive requests. ###";
        Logger.LogDebug(serverReadyMessage);
        Console.WriteLine(serverReadyMessage);
      });
      if (ResponseCompressionConfiguration?.UseResponseCompression == true)
      {
        Logger.Information("Enabling response compression on app");
        app.UseResponseCompression();
      }

      SetupDevelopmentEnvironmentSettings(app);
      var appPrepared = PrepareApplicationBuilder(app);
      Logger.Information("setting up custom routing");
      ConfigureRouting(appPrepared);
    }

    private void SetupDevelopmentEnvironmentSettings(IApplicationBuilder i_App)
    {
      if (IsDevelopmentEnvironment)
      {
        Logger.Information("Initialize swagger in Application Builder");
        i_App.UseSwagger();
        i_App.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "NiceCore"));
        Logger.Information("Use Developer Exception Page");
        i_App.UseDeveloperExceptionPage();
      }
    }

    private bool IsDevEnvironment()
    {
      var env = Configuration?.GetValue<string>(AspEnvironmentSetting.KEY, null);
      if (env != null && env == AspEnvironmentSetting.VALUE_DEVELOPMENT)
      {
        Logger.Information($"Environment 'Development' configured for {AspEnvironmentSetting.KEY}.");
        return true;
      }

      Logger.Debug($"No environment configured for {AspEnvironmentSetting.KEY}. Environment will be treated as 'Production'.");
      return false;
    }
  }
}