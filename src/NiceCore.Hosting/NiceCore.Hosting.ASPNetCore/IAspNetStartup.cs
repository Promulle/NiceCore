﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Threading.Tasks;

namespace NiceCore.Hosting.ASPNetCore
{
  /// <summary>
  /// Startup for asp net core
  /// </summary>
  public interface IAspNetStartup : IServiceStartup
  {
    /// <summary>
    /// Source that is completed when the webserver for this startup is ready to take requests
    /// </summary>
    TaskCompletionSource BootCompletionSource { get; set; }

    /// <summary>
    /// Configure
    /// </summary>
    /// <param name="t_App"></param>
    /// <param name="i_HostApplicationLifetime"></param>
    void Configure(IApplicationBuilder t_App, IHostApplicationLifetime i_HostApplicationLifetime);

    /// <summary>
    /// Method for configuring services
    /// </summary>
    /// <param name="i_Services"></param>
    /// <returns></returns>
    void ConfigureServices(IServiceCollection i_Services);
  }
}
