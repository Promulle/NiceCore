﻿using Microsoft.AspNetCore.Authorization;
using NiceCore.Hosting.Utils;
using NiceCore.Permissions;
using NiceCore.Permissions.Impl;
using NiceCore.Services.Permissions;
using System.Security.Claims;
using System.Threading.Tasks;

namespace NiceCore.Hosting.ASPNetCore.Auth
{
  /// <summary>
  /// Handler for HasPermission requirements
  /// </summary>
  public class HasPermissionHandler : AuthorizationHandler<HasPermissionRequirement>
  {
    /// <summary>
    /// Permission Service
    /// </summary>
    public IPermissionService PermissionService { get; set; }

    /// <summary>
    /// Key factory for permission keys
    /// </summary>
    public IAuthorizationPermissionKeyFactory AuthorizationPermissionKeyFactory { get; set; }

    /// <summary>
    /// Optional accessor for customizing the way a user claim is accessed
    /// </summary>
    public IAuthorizationUserClaimAccessor AuthorizationUserClaimAccessor { get; set; }

    /// <summary>
    /// Service for creating context for permission service
    /// </summary>
    public IAuthorizationPermissionServiceContextFactory AuthorizationPermissionServiceContextFactory { get; set; }

    /// <summary>
    /// Handle requirement
    /// </summary>
    /// <param name="i_Context"></param>
    /// <param name="i_Requirement"></param>
    /// <returns></returns>
    protected override async Task HandleRequirementAsync(AuthorizationHandlerContext i_Context, HasPermissionRequirement i_Requirement)
    {
      var claim = GetUserIDClaim(i_Context);
      if (claim != null)
      {
        using var context   = await GetContext(i_Context).ConfigureAwait(false);
        var       key       = await CreatePermissionKeyFromRequirement(i_Context, i_Requirement).ConfigureAwait(false);
        var       isAllowed = await PermissionService.IsAllowed(claim.Value, key, context).ConfigureAwait(false);
        if (isAllowed)
        {
          i_Context.Succeed(i_Requirement);
        }
      }
    }

    private Claim GetUserIDClaim(AuthorizationHandlerContext i_Context)
    {
      if (AuthorizationUserClaimAccessor != null)
      {
        return AuthorizationUserClaimAccessor.GetUserIDClaim(i_Context);
      }

      return i_Context.User.FindFirst(NcJWTSupport.TOKEN_CLAIM_USER_ID);
    }

    private Task<IPermissionKey> CreatePermissionKeyFromRequirement(AuthorizationHandlerContext i_Context, HasPermissionRequirement i_Requirement)
    {
      if (AuthorizationPermissionKeyFactory != null)
      {
        return AuthorizationPermissionKeyFactory.CreateKey(i_Context, i_Requirement);
      }

      return Task.FromResult<IPermissionKey>(SimplePermissionKey.Of(i_Requirement.PermissionKey));
    }

    private ValueTask<IPermissionServiceContext> GetContext(AuthorizationHandlerContext i_Context)
    {
      if (AuthorizationPermissionServiceContextFactory != null)
      {
        return AuthorizationPermissionServiceContextFactory.CreateContext(i_Context);
      }

      return ValueTask.FromResult<IPermissionServiceContext>(new HasPermissionDummyContext());
    }
  }
}
