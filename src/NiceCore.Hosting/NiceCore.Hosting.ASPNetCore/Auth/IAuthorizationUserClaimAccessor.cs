﻿using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace NiceCore.Hosting.ASPNetCore.Auth
{
  /// <summary>
  /// Accessor for customising the way to access the user id claim
  /// </summary>
  public interface IAuthorizationUserClaimAccessor
  {
    /// <summary>
    /// Return the claim used for user id
    /// </summary>
    /// <param name="i_Context"></param>
    /// <returns></returns>
    Claim GetUserIDClaim(AuthorizationHandlerContext i_Context);
  }
}
