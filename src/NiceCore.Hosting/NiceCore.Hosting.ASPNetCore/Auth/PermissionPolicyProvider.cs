﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;

namespace NiceCore.Hosting.ASPNetCore.Auth
{
  /// <summary>
  /// Provider for dynamically creating permissions
  /// </summary>
  public class PermissionPolicyProvider : DefaultAuthorizationPolicyProvider
  {
    private readonly IOptions<AuthorizationOptions> m_AuthOptions;
    private readonly object m_Lock = new ();
    /// <summary>
    /// Optional matcher enabling more complex scenarios for policies based on names
    /// </summary>
    public IPolicyRequirementMatcher PolicyRequirementMatcher { get; set; }

    /// <summary>
    /// Options filling ctor
    /// </summary>
    /// <param name="i_Options"></param>
    public PermissionPolicyProvider(IOptions<AuthorizationOptions> i_Options) : base(i_Options)
    {
      m_AuthOptions = i_Options;
    }

    /// <summary>
    /// Override dynamically creating policies
    /// </summary>
    /// <param name="i_PolicyName"></param>
    /// <returns></returns>
    public override async Task<AuthorizationPolicy> GetPolicyAsync(string i_PolicyName)
    {
      var existingPolicy = await base.GetPolicyAsync(i_PolicyName).ConfigureAwait(false);
      if (existingPolicy == null)
      {
        lock (m_Lock)
        {
          // ReSharper disable once ConditionIsAlwaysTrueOrFalse -> might not be true because of multithreading
          if (existingPolicy == null)
          {
            var policy = CreateNewPolicy(i_PolicyName);
            m_AuthOptions.Value.AddPolicy(i_PolicyName, policy);
            return policy;
          }
        }
      }
      return existingPolicy;
    }

    private AuthorizationPolicy CreateNewPolicy(string i_PolicyName)
    {
      return new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme)
        .AddRequirements(GetRequirementForPolicyName(i_PolicyName))
        .Build();
    }

    private IAuthorizationRequirement GetRequirementForPolicyName(string i_PolicyName)
    {
      if (PolicyRequirementMatcher != null)
      {
        return PolicyRequirementMatcher.CreateCorrectPolicy(i_PolicyName);
      }
      return new HasPermissionRequirement(i_PolicyName);
    }
  }
}
