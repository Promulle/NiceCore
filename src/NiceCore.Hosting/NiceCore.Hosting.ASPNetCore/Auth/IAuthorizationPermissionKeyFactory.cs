﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NiceCore.Permissions;

namespace NiceCore.Hosting.ASPNetCore.Auth
{
  /// <summary>
  /// Factory used for creating permission keys for authorizationhandlers from parameters available in authorizationhandlers
  /// </summary>
  public interface IAuthorizationPermissionKeyFactory
  {
    /// <summary>
    /// Create a key based on the available parameters
    /// </summary>
    /// <param name="i_Context"></param>
    /// <param name="i_Requirement"></param>
    /// <returns></returns>
    Task<IPermissionKey> CreateKey(AuthorizationHandlerContext i_Context, HasPermissionRequirement i_Requirement);
  }
}
