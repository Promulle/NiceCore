﻿using NiceCore.Base;
using NiceCore.Services.Permissions;

namespace NiceCore.Hosting.ASPNetCore.Auth
{
  /// <summary>
  /// Dummy impl of IPermissionServiceContext in case no creator is registered
  /// </summary>
  public class HasPermissionDummyContext : BaseDisposable, IPermissionServiceContext
  {
  }
}
