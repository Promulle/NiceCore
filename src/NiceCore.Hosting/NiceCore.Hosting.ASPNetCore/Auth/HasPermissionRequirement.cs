﻿using Microsoft.AspNetCore.Authorization;

namespace NiceCore.Hosting.ASPNetCore.Auth
{
  /// <summary>
  /// Requirement holding a specific permission
  /// </summary>
  public class HasPermissionRequirement : IAuthorizationRequirement
  {
    /// <summary>
    /// Key of permission
    /// </summary>
    public string PermissionKey { get; }

    /// <summary>
    /// Ctor filling permission key
    /// </summary>
    /// <param name="i_PermissionKey"></param>
    public HasPermissionRequirement(string i_PermissionKey)
    {
      PermissionKey = i_PermissionKey;
    }
  }
}
