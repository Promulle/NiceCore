﻿using Microsoft.AspNetCore.Authorization;

namespace NiceCore.Hosting.ASPNetCore.Auth
{
  /// <summary>
  /// Service that creates the appropriate requierment for the policy provided
  /// </summary>
  public interface IPolicyRequirementMatcher
  {
    /// <summary>
    /// Creates the correct policy
    /// </summary>
    /// <param name="i_PolicyName"></param>
    /// <returns></returns>
    IAuthorizationRequirement CreateCorrectPolicy(string i_PolicyName);
  }
}
