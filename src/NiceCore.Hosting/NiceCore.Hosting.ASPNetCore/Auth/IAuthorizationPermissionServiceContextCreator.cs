﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NiceCore.Services.Permissions;

namespace NiceCore.Hosting.ASPNetCore.Auth
{
  /// <summary>
  /// Service that creates the appropriate permissionservicecontext instance when calling the permission service in authorizationhandler calls
  /// </summary>
  public interface IAuthorizationPermissionServiceContextFactory
  {
    /// <summary>
    /// Create a context from the authorization handler context
    /// </summary>
    /// <returns></returns>
    ValueTask<IPermissionServiceContext> CreateContext(AuthorizationHandlerContext i_Context);
  }
}
