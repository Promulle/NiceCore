using System.Collections.Generic;
using NiceCore.Hosting.ASPNetCore.Json.Profiles;

namespace NiceCore.Hosting.ASPNetCore.Json
{
  /// <summary>
  /// Interface that provides a list of serializer profiles to use
  /// </summary>
  public interface IJsonSerializerProfileProvider
  {
    /// <summary>
    /// Returns all profiles
    /// </summary>
    /// <returns></returns>
    IEnumerable<JsonSerializerProfile> GetProfiles();
  }
}
