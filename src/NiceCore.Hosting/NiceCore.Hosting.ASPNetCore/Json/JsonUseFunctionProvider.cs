using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NiceCore.Logging;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation;

namespace NiceCore.Hosting.ASPNetCore.Json
{
  /// <summary>
  /// Use Function Provider
  /// </summary>
  [Register(InterfaceType = typeof(IJsonUseFunctionProvider))]
  public class JsonUseFunctionProvider : IJsonUseFunctionProvider
  {
    /// <summary>
    /// Json Mode Provider
    /// </summary>
    public IJsonModeProvider JsonModeProvider { get; set; }
    
    /// <summary>
    /// Options for newtonsoft json
    /// </summary>
    public IControllerNewtonsoftJsonOptions NewtonsoftJsonOptions { get; set; }
    
    /// <summary>
    /// Options for system.text.json
    /// </summary>
    public IControllerSystemTextJsonOptions SystemTextJsonOptions { get; set; }
    
    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<JsonUseFunctionProvider> Logger { get; set; }

    /// <summary>
    /// Applies the use functions
    /// </summary>
    /// <param name="t_Builder"></param>
    public void ApplyJsonUseFunctions(IMvcBuilder t_Builder)
    {
      if (JsonModeProvider != null)
      {
        Logger.Information("Applying specified json mode");
        var mode = JsonModeProvider.GetModeToUse();
        if (mode == JsonModes.SystemTextJson)
        {
          if (SystemTextJsonOptions != null && SystemTextJsonOptions.OptionsFunc != null)
          {
            Logger.Information($"Applying SystemTextJson Options from class {SystemTextJsonOptions.GetType().Name}");
            t_Builder.AddJsonOptions(SystemTextJsonOptions.OptionsFunc);
          }
          else
          {
            Logger.Information("Applying Default SystemTextJson Options");
            t_Builder.AddJsonOptions(_ => {});
          }
        }
        else if (mode == JsonModes.NewtonSoft)
        {
          if (NewtonsoftJsonOptions != null && NewtonsoftJsonOptions.OptionsFunc != null)
          {
            Logger.Information($"Applying NewtonsoftJson Options from class {NewtonsoftJsonOptions.GetType().Name}");
            t_Builder.AddNewtonsoftJson(NewtonsoftJsonOptions.OptionsFunc);
          }
          else
          {
            Logger.Information("Applying Default NewtonsoftJson Options");
            t_Builder.AddNewtonsoftJson();
          }
        }
        else if (mode == JsonModes.Both)
        {
          Logger.Information("Applying NewtonsoftJson Options for mode: Both");
          t_Builder
            .AddNewtonsoftJson();
        }
      }
    }
  }
}
