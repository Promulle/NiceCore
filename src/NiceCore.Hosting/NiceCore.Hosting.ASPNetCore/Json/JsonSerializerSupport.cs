using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using NiceCore.Hosting.ASPNetCore.Json.Formatters;
using NiceCore.ServiceLocation;

namespace NiceCore.Hosting.ASPNetCore.Json
{
  /// <summary>
  /// Serializer Support
  /// </summary>
  [Register(InterfaceType = typeof(IJsonSerializerSupport))]
  public class JsonSerializerSupport : IJsonSerializerSupport
  {
    /// <summary>
    /// Profile Provider
    /// </summary>
    public IJsonSerializerProfileProvider JsonSerializerProfileProvider { get; set; }
    
    /// <summary>
    /// Applies support for json provided in i_Options
    /// </summary>
    /// <param name="i_Options"></param>
    /// <param name="i_Container"></param>
    public void ApplyJsonSupport(MvcOptions i_Options, IServiceContainer i_Container)
    {
      if (JsonSerializerProfileProvider != null)
      {
        var profiles = JsonSerializerProfileProvider.GetProfiles().ToList();
        if (profiles.Count > 0)
        {
          profiles.ForEach(profile =>
          {
            profile.CreateFormatters(i_Options, i_Container);
          });
          var inputFormatter = new NcInputFormatter(profiles);
          var outputFormatter = new NcOutputFormatter(profiles);
          i_Options.InputFormatters.Insert(0, inputFormatter);
          i_Options.OutputFormatters.Insert(0, outputFormatter);
        }
      }
    }
  }
}
