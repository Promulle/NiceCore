namespace NiceCore.Hosting.ASPNetCore.Json
{
  /// <summary>
  /// Modes used to determine which serializer to use
  /// </summary>
  public enum JsonModes
  {
    /// <summary>
    /// System text json
    /// </summary>
    SystemTextJson,
    
    /// <summary>
    /// Newton soft
    /// </summary>
    NewtonSoft,
    
    /// <summary>
    /// both
    /// </summary>
    Both,
  }
}
