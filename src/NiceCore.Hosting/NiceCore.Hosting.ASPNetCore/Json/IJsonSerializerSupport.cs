using Microsoft.AspNetCore.Mvc;
using NiceCore.ServiceLocation;

namespace NiceCore.Hosting.ASPNetCore.Json
{
  /// <summary>
  /// Interface for service used by ncservicestartup to apply json support
  /// </summary>
  public interface IJsonSerializerSupport
  {
    /// <summary>
    /// Applies support for json provided in i_Options
    /// </summary>
    /// <param name="i_Options"></param>
    /// <param name="i_Container"></param>
    void ApplyJsonSupport(MvcOptions i_Options, IServiceContainer i_Container);
  }
}
