using Microsoft.Extensions.DependencyInjection;

namespace NiceCore.Hosting.ASPNetCore.Json
{
  /// <summary>
  /// Provider that applies the add functions for newtonsoftjson and systemtextjsonfunctions
  /// </summary>
  public interface IJsonUseFunctionProvider
  {
    /// <summary>
    /// Applies use functions
    /// </summary>
    /// <param name="t_Builder"></param>
    void ApplyJsonUseFunctions(IMvcBuilder t_Builder);
  }
}
