using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using NiceCore.ServiceLocation;

namespace NiceCore.Hosting.ASPNetCore.Json.Profiles
{
  /// <summary>
  /// Profile for a serializer to be used by nicecore
  /// </summary>
  public abstract class JsonSerializerProfile
  {
    /// <summary>
    /// Whether the serializer is default 
    /// </summary>
    public bool IsDefault { get; set; }
    
    /// <summary>
    /// Criteria used to decide whether to use this or not. Not needed if IsDefault = true
    /// </summary>
    public Func<Endpoint, bool> Criteria { get; set; }
    
    /// <summary>
    /// Input FOrmatter to use
    /// </summary>
    public TextInputFormatter InputFormatter { get; private set; }
    
    /// <summary>
    /// Output Formatter
    /// </summary>
    public TextOutputFormatter OutputFormatter { get; private set; }

    /// <summary>
    /// Create formatters to use.
    /// </summary>
    /// <param name="i_Options"></param>
    /// <param name="i_Container"></param>
    public void CreateFormatters(MvcOptions i_Options, IServiceContainer i_Container)
    {
      InputFormatter = CreateInputFormatter(i_Options, i_Container);
      OutputFormatter = CreateOutputFormatter(i_Options, i_Container);
    }

    /// <summary>
    /// Function creating the input formatter
    /// </summary>
    /// <param name="i_Options"></param>
    /// <param name="i_Container"></param>
    /// <returns></returns>
    protected abstract TextInputFormatter CreateInputFormatter(MvcOptions i_Options, IServiceContainer i_Container);
    
    /// <summary>
    /// Function creating the input formatter
    /// </summary>
    /// <param name="i_Options"></param>
    /// <param name="i_Container"></param>
    /// <returns></returns>
    protected abstract TextOutputFormatter CreateOutputFormatter(MvcOptions i_Options, IServiceContainer i_Container);
  }
}
