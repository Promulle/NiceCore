using System;
using System.Buffers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.ObjectPool;
using Microsoft.Extensions.Options;
using NiceCore.ServiceLocation;

namespace NiceCore.Hosting.ASPNetCore.Json.Profiles
{
  /// <summary>
  /// Specialized profile for newtonsoftjson
  /// </summary>
  public class NewtonsoftJsonSerializerProfile : JsonSerializerProfile
  {
    /// <summary>
    /// Options
    /// </summary>
    public Action<MvcNewtonsoftJsonOptions> OptionsFunc { get; init; }
    
    /// <summary>
    /// Create input formatter
    /// </summary>
    /// <param name="i_Options"></param>
    /// <param name="i_Container"></param>
    /// <returns></returns>
    protected override TextInputFormatter CreateInputFormatter(MvcOptions i_Options, IServiceContainer i_Container)
    {
      var loggerFactory        = i_Container.Resolve<ILoggerFactory>();
      var mvcNewtonsoftOptions = i_Container.Resolve<IOptions<MvcNewtonsoftJsonOptions>>();
      var nwOptions            = mvcNewtonsoftOptions.Value;
      if (OptionsFunc != null)
      {
        OptionsFunc.Invoke(nwOptions);
      }
      var arrayPool          = i_Container.Resolve<ArrayPool<char>>();
      var objectPoolProvider = i_Container.Resolve<ObjectPoolProvider>();
      i_Container.Release(objectPoolProvider);
      i_Container.Release(arrayPool);
      i_Container.Release(mvcNewtonsoftOptions);
      i_Container.Release(loggerFactory);
      return new NewtonsoftJsonInputFormatter(loggerFactory.CreateLogger(typeof(NewtonsoftJsonInputFormatter)), nwOptions.SerializerSettings, arrayPool, objectPoolProvider, i_Options, nwOptions);
    }

    /// <summary>
    /// Create output formatter
    /// </summary>
    /// <param name="i_Options"></param>
    /// <param name="i_Container"></param>
    /// <returns></returns>
    protected override TextOutputFormatter CreateOutputFormatter(MvcOptions i_Options, IServiceContainer i_Container)
    {
      var arrayPool = i_Container.Resolve<ArrayPool<char>>();
      var mvcNewtonsoftOptions = i_Container.Resolve<IOptions<MvcNewtonsoftJsonOptions>>();
      var nwOptions = mvcNewtonsoftOptions.Value;
      if (OptionsFunc != null)
      {
        OptionsFunc.Invoke(nwOptions);
      }
      i_Container.Release(arrayPool);
      i_Container.Release(mvcNewtonsoftOptions);
      return new NewtonsoftJsonOutputFormatter(nwOptions.SerializerSettings, arrayPool, i_Options, nwOptions);
    }
  }
}
