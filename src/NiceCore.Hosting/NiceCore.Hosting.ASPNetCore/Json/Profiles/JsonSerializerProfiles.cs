using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace NiceCore.Hosting.ASPNetCore.Json.Profiles
{
  /// <summary>
  /// Helper class for creating serializer profiles
  /// </summary>
  public static class JsonSerializerProfiles
  {
    /// <summary>
    /// SystemTextJson Profile
    /// </summary>
    /// <param name="i_Options"></param>
    /// <returns></returns>
    public static JsonSerializerProfile SystemTextJsonDefaultProfile(Action<JsonOptions> i_Options = null)
    {
      return new SystemTextJsonSerializerProfile()
      {
        Criteria = null,
        IsDefault = true,
        OptionsFunc = i_Options
      };
    }
    
    /// <summary>
    /// SystemTextJson Profile
    /// </summary>
    /// <param name="i_Criteria"></param>
    /// <param name="i_Options"></param>
    /// <returns></returns>
    public static JsonSerializerProfile SystemTextJsonProfile(Func<Endpoint, bool> i_Criteria, Action<JsonOptions> i_Options = null)
    {
      return new SystemTextJsonSerializerProfile()
      {
        Criteria    = i_Criteria,
        IsDefault   = false,
        OptionsFunc = i_Options
      };
    }
    
    /// <summary>
    /// Creates a default profile for newtonsoftjson
    /// </summary>
    /// <param name="i_Options"></param>
    /// <returns></returns>
    public static JsonSerializerProfile NewtonsoftJsonDefaultProfile(Action<MvcNewtonsoftJsonOptions> i_Options = null)
    {
      return new NewtonsoftJsonSerializerProfile()
      {
        IsDefault = true,
        Criteria = null,
        OptionsFunc = i_Options
      };
    }
    
    /// <summary>
    /// Creates a criteria based profile for newtonsoftjson
    /// </summary>
    /// <param name="i_CriteriaFunc"></param>
    /// <param name="i_Options"></param>
    /// <returns></returns>
    public static JsonSerializerProfile NewtonsoftJsonProfile(Func<Endpoint, bool> i_CriteriaFunc, Action<MvcNewtonsoftJsonOptions> i_Options = null)
    {
      return new NewtonsoftJsonSerializerProfile()
      {
        Criteria = i_CriteriaFunc,
        IsDefault = false,
        OptionsFunc = i_Options
      };
    }
  }
}
