using System;
using System.Linq;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NiceCore.ServiceLocation;

namespace NiceCore.Hosting.ASPNetCore.Json.Profiles
{
  /// <summary>
  /// Specialized profile for system.text.json
  /// </summary>
  public class SystemTextJsonSerializerProfile : JsonSerializerProfile
  {
    /// <summary>
    /// Options
    /// </summary>
    public Action<JsonOptions> OptionsFunc { get; init; }

    /// <summary>
    /// Create Input formatter
    /// </summary>
    /// <param name="i_Options"></param>
    /// <param name="i_Container"></param>
    /// <returns></returns>
    protected override TextInputFormatter CreateInputFormatter(MvcOptions i_Options, IServiceContainer i_Container)
    {
      var loggerFactory = i_Container.Resolve<ILoggerFactory>();
      var options = i_Container.Resolve<IOptions<JsonOptions>>();
      var actualOptions = options.Value;
      actualOptions = new JsonOptions();
      if (OptionsFunc != null)
      {
        OptionsFunc.Invoke(actualOptions);
      }
      var logger = loggerFactory.CreateLogger<SystemTextJsonInputFormatter>();
      i_Container.Release(options);
      i_Container.Release(loggerFactory);
      return new SystemTextJsonInputFormatter(actualOptions, logger);
    }

    /// <summary>
    /// Create output formatter
    /// </summary>
    /// <param name="i_Options"></param>
    /// <param name="i_Container"></param>
    /// <returns></returns>
    protected override TextOutputFormatter CreateOutputFormatter(MvcOptions i_Options, IServiceContainer i_Container)
    {
      var options = i_Container.Resolve<IOptions<JsonOptions>>();
      var actualOptions = options.Value;
      if (OptionsFunc != null)
      {
        OptionsFunc.Invoke(actualOptions);
      }
      i_Container.Release(options);
      return new SystemTextJsonOutputFormatter(actualOptions.JsonSerializerOptions);
    }
  }
}
