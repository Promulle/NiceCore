using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using NiceCore.Base.Constants;
using NiceCore.Hosting.ASPNetCore.Json.Profiles;

namespace NiceCore.Hosting.ASPNetCore.Json.Formatters
{
  /// <summary>
  /// Input formatter that delegates the 
  /// </summary>
  public class NcInputFormatter : TextInputFormatter
  {
    private readonly List<JsonSerializerProfile> m_Profiles;
    /// <summary>
    /// Input formatter
    /// </summary>
    public NcInputFormatter(List<JsonSerializerProfile> i_Profiles)
    {
      SupportedEncodings.Add(UTF16EncodingLittleEndian);
      SupportedEncodings.Add(UTF8EncodingWithoutBOM);
      SupportedEncodings.Add(Encoding.UTF8);
      SupportedMediaTypes.Add(ContentTypes.APP_JSON);
      m_Profiles = i_Profiles;
    }

    /// <summary>
    /// Read Request Body async
    /// </summary>
    /// <param name="i_Context"></param>
    /// <param name="i_Encoding"></param>
    /// <returns></returns>
    public override Task<InputFormatterResult> ReadRequestBodyAsync(InputFormatterContext i_Context, Encoding i_Encoding)
    {
      var endpoint = i_Context.HttpContext.GetEndpoint();
      var formatter = FormatterFunctions.GetFormatter(m_Profiles, endpoint, profile => profile.InputFormatter);
      return formatter.ReadRequestBodyAsync(i_Context);
    }
  }
}
