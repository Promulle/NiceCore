using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using NiceCore.Base.Constants;
using NiceCore.Hosting.ASPNetCore.Json.Profiles;

namespace NiceCore.Hosting.ASPNetCore.Json.Formatters
{
  /// <summary>
  /// Formatter delegating between formatters
  /// </summary>
  public class NcOutputFormatter : TextOutputFormatter
  {
    private readonly List<JsonSerializerProfile> m_Profiles;

    /// <summary>
    /// Ctor
    /// </summary>
    public NcOutputFormatter(List<JsonSerializerProfile> i_Profiles)
    {
      m_Profiles = i_Profiles;
      SupportedEncodings.Add(Encoding.UTF8);
      SupportedEncodings.Add(Encoding.Unicode);
      SupportedMediaTypes.Add(ContentTypes.APP_JSON);
    }
    
    /// <summary>
    /// Write Response Async
    /// </summary>
    /// <returns></returns>
    public override Task WriteResponseBodyAsync(OutputFormatterWriteContext i_Context, Encoding i_SelectedEncoding)
    {
      var endpoint = i_Context.HttpContext.GetEndpoint();
      var formatter = FormatterFunctions.GetFormatter(m_Profiles, endpoint, profile => profile.OutputFormatter);
      return formatter.WriteResponseBodyAsync(i_Context, i_SelectedEncoding);
    }
  }
}
