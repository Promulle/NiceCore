using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using NiceCore.Hosting.ASPNetCore.Json.Profiles;

namespace NiceCore.Hosting.ASPNetCore.Json.Formatters
{
  /// <summary>
  /// Formatter functions
  /// </summary>
  public static class FormatterFunctions
  {
    /// <summary>
    /// Return the applicable formatter
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static T GetFormatter<T>(List<JsonSerializerProfile> i_AllProfiles, Endpoint i_Endpoint, Func<JsonSerializerProfile, T> i_GetFormatterFunc)
    {
      if (i_AllProfiles.Count == 1)
      {
        return i_GetFormatterFunc.Invoke(i_AllProfiles[0]);
      }
      var firstMatchingSerializer = i_AllProfiles.FirstOrDefault(r => r.Criteria != null && r.Criteria.Invoke(i_Endpoint));
      if (firstMatchingSerializer != null)
      {
        return i_GetFormatterFunc.Invoke(firstMatchingSerializer);
      }
      var defaultSerializer = i_AllProfiles.FirstOrDefault(r => r.IsDefault);
      if (defaultSerializer == null)
      {
        throw new InvalidOperationException("Serializers not setup correctly. Could not find a matching serializer for the endpoint and no default serializer was found.");
      }
      return i_GetFormatterFunc.Invoke(defaultSerializer);
    }
  }
}
