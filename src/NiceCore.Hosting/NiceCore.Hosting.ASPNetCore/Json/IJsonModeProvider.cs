namespace NiceCore.Hosting.ASPNetCore.Json
{
  /// <summary>
  /// Provider for mode to use
  /// </summary>
  public interface IJsonModeProvider
  {
    /// <summary>
    /// Returns the mode to use
    /// </summary>
    /// <returns></returns>
    JsonModes GetModeToUse();
  }
}
