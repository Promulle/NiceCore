namespace NiceCore.Hosting.ASPNetCore.StartupModules
{
  /// <summary>
  /// StartupModule
  /// </summary>
  public interface IStartupModule
  {
    /// <summary>
    /// Configure
    /// </summary>
    void Configure();
  }
}