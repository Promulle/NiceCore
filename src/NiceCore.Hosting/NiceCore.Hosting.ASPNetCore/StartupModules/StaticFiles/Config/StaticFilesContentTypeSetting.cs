using System;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Hosting.ASPNetCore.StartupModules.StaticFiles.Config
{
  /// <summary>
  /// StaticFilesContentTypeSetting
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class StaticFilesContentTypeSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// KEY
    /// </summary>
    public const string KEY = "NiceCore.Hosting.Asp.StaticFiles.ContentTypes";

    /// <summary>
    /// Key
    /// </summary>
    public string Key { get; } = KEY;

    /// <summary>
    /// Setting Type
    /// </summary>
    public Type SettingType { get; } = typeof(StaticFilesContentTypeConfigurationModel);
  }
}