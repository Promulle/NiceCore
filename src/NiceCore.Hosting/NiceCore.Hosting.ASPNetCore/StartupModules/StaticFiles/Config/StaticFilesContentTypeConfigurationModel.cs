using System.Collections.Generic;

namespace NiceCore.Hosting.ASPNetCore.StartupModules.StaticFiles.Config
{
  /// <summary>
  /// StaticFilesContentTypeConfigurationModel
  /// </summary>
  public class StaticFilesContentTypeConfigurationModel
  {
    /// <summary>
    /// Use Custom Content Types
    /// </summary>
    public bool UseCustomContentTypes { get; set; }
    
    /// <summary>
    /// Content Type Overrides
    /// </summary>
    public Dictionary<string, string> ContentTypeOverrides { get; set; }
  }
}