using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace NiceCore.Hosting.ASPNetCore.StartupModules.StaticFiles
{
  /// <summary>
  /// Static Files Startup Module
  /// </summary>
  public interface IStaticFilesStartupModule : IStartupModule
  {
    /// <summary>
    /// Apply Distributed MemoryCacheIfNeeded
    /// </summary>
    /// <param name="t_Services"></param>
    void ApplyDistributedMemoryCacheIfNeeded(IServiceCollection t_Services);
    
    /// <summary>
    /// Configure Use Static Files
    /// </summary>
    /// <param name="t_Builder"></param>
    void ConfigureUseStaticFiles(IApplicationBuilder t_Builder);
  }
}