using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NiceCore.Base;
using NiceCore.Hosting.ASPNetCore.Config.Model;
using NiceCore.Hosting.ASPNetCore.Config.Settings;
using NiceCore.Hosting.ASPNetCore.StartupModules.StaticFiles.Config;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Hosting.ASPNetCore.StartupModules.StaticFiles
{
  /// <summary>
  /// Static Files Startup Module
  /// </summary>
  [Register(InterfaceType = typeof(IStaticFilesStartupModule))]
  public class StaticFilesStartupModule : IStaticFilesStartupModule
  {
    /// <summary>
    /// Setting for Static file caching
    /// </summary>
    public StaticFileCachingConfiguration StaticFileCachingConfiguration { get; set; }

    /// <summary>
    /// Configuration
    /// </summary>
    public INcConfiguration Configuration { get; set; }

    /// <summary>
    /// logger
    /// </summary>
    public ILogger<StaticFilesStartupModule> Logger { get; set; }

    /// <summary>
    /// Configure
    /// </summary>
    public void Configure()
    {
      StaticFileCachingConfiguration = Configuration?.GetValue<StaticFileCachingConfiguration>(StaticFileCachingSetting.KEY, null);
    }

    /// <summary>
    /// Apply Distributed MemoryCacheIfNeeded
    /// </summary>
    /// <param name="t_Services"></param>
    public void ApplyDistributedMemoryCacheIfNeeded(IServiceCollection t_Services)
    {
      if (StaticFileCachingConfiguration?.UseStaticFileCaching == true)
      {
        t_Services.AddDistributedMemoryCache();
      }
    }

    /// <summary>
    /// Configure Use Static Files
    /// </summary>
    /// <param name="t_Builder"></param>
    public void ConfigureUseStaticFiles(IApplicationBuilder t_Builder)
    {
      if (StaticFileCachingConfiguration?.UseStaticFileCaching != true)
      {
        Logger.Information("Static file caching is not enabled, just use default providing.");
        t_Builder.UseStaticFiles();
        return;
      }

      Logger.LogInformation("Enabling static file caching");
      var kind = StaticFileCachingConfiguration.ExpireDurationKind;
      var value = StaticFileCachingConfiguration.ExpireDuration;
      Logger.Information($"Info about expiring of cache: Time Unit: {kind} Duration: {value}");
      if (ShouldNotCacheIsConfigured())
      {
        t_Builder.Use(async (ctx, next) =>
        {
          if (ShouldNotCacheRoot(ctx) || ShouldNotCacheDefault(ctx))
          {
            ApplyNoCacheTo(ctx);
          }

          await next().ConfigureAwait(false);
        });
      }

      t_Builder.UseStaticFiles(GetOptions(kind, value));
    }

    private StaticFileOptions GetOptions(TimeKind i_TimeKind, long i_TimeValue)
    {
      return new()
      {
        OnPrepareResponse = ctx =>
        {
          var headers = ctx.Context.Response.GetTypedHeaders();
          if (headers.CacheControl == null)
          {
            headers.CacheControl = new()
            {
              MaxAge = i_TimeKind.ToTimeSpan(i_TimeValue),
              Public = true
            };
            headers.Expires = i_TimeKind.GetTimeIn(i_TimeValue);
          }
        },
        ContentTypeProvider = GetConfiguredContentTypeProvider()
      };
    }

    private IContentTypeProvider GetConfiguredContentTypeProvider()
    {
      var config = Configuration.GetValue<StaticFilesContentTypeConfigurationModel>(StaticFilesContentTypeSetting.KEY, null);
      if (config?.UseCustomContentTypes != true || config.ContentTypeOverrides == null || config.ContentTypeOverrides.Count == 0)
      {
        return null;
      }

      var contentTypeProvider = new FileExtensionContentTypeProvider();
      foreach (var (key, value) in config.ContentTypeOverrides)
      {
        contentTypeProvider.Mappings[key] = value;
      }

      return contentTypeProvider;
    }

    private bool ShouldNotCacheDefault(HttpContext i_Context)
    {
      if (i_Context.Request.Path.Value == null || StaticFileCachingConfiguration?.CacheExclusionStartPaths == null || StaticFileCachingConfiguration.CacheExclusionStartPaths.Count < 1)
      {
        return false;
      }
      foreach (var excludedStartPath in StaticFileCachingConfiguration.CacheExclusionStartPaths)
      {
        if (i_Context.Request.Path.Value.StartsWith(excludedStartPath))
        {
          return true;
        }
      }

      return false;
    }

    private bool ShouldNotCacheRoot(HttpContext i_Context)
    {
      return !StaticFileCachingConfiguration.CacheRoot && StaticFileCachingConfiguration.RootPaths.Contains(i_Context.Request.Path.Value);
    }

    private bool ShouldNotCacheIsConfigured()
    {
      return (!StaticFileCachingConfiguration.CacheRoot && StaticFileCachingConfiguration.RootPaths?.Count > 0) || StaticFileCachingConfiguration.CacheExclusionStartPaths?.Count > 0;
    }

    private static void ApplyNoCacheTo(HttpContext t_Context)
    {
      var headers = t_Context.Response.GetTypedHeaders();
      headers.CacheControl = new()
      {
        NoCache = true,
        Public = true
      };
      headers.Expires = DateTime.Now;
    }
  }
}