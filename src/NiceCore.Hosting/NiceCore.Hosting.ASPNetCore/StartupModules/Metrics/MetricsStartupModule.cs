using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using NiceCore.Hosting.ASPNetCore.Config.Settings;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Hosting.ASPNetCore.StartupModules.Metrics
{
  /// <summary>
  /// Metrics Startup Module
  /// </summary>
  [Register(InterfaceType = typeof(IMetricsStartupModule))]
  public class MetricsStartupModule : IMetricsStartupModule
  {
    private bool m_MetricCollection = false;
    
    /// <summary>
    /// Configuration
    /// </summary>
    public INcConfiguration Configuration { get; set; }
    
    /// <summary>
    /// Configurator
    /// </summary>
    public IMetricsCollectionConfigurator Configurator { get; set; }
    
    /// <summary>
    /// Configure
    /// </summary>
    public void Configure()
    {
      if (Configurator != null)
      {
        m_MetricCollection = Configuration?.GetValue(AspMetricsSetting.KEY, false) ?? false;  
      }
    }

    /// <summary>
    /// Add Metrics
    /// </summary>
    /// <param name="t_ServiceCollection"></param>
    public void AddMetrics(IServiceCollection t_ServiceCollection)
    {
      if (m_MetricCollection)
      {
        Configurator.AddMetrics(t_ServiceCollection);
      }
    }
    
    /// <summary>
    /// Apply Metrics
    /// </summary>
    /// <param name="t_Builder"></param>
    public void ApplyMetrics(IApplicationBuilder t_Builder)
    {
      if (m_MetricCollection)
      {
        Configurator.ApplyMetricsToApp(t_Builder);
      }
    }
  }
}