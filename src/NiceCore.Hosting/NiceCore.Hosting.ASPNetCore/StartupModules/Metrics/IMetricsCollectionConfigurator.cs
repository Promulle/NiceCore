using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace NiceCore.Hosting.ASPNetCore.StartupModules.Metrics
{
  /// <summary>
  /// Metrics collection configurator
  /// </summary>
  public interface IMetricsCollectionConfigurator
  {
    /// <summary>
    /// Apply Metrics to App
    /// </summary>
    /// <param name="t_Builder"></param>
    void ApplyMetricsToApp(IApplicationBuilder t_Builder);

    /// <summary>
    /// Register Metrics
    /// </summary>
    void AddMetrics(IServiceCollection t_Collection);
  }
}