using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace NiceCore.Hosting.ASPNetCore.StartupModules.Metrics
{
  /// <summary>
  /// Metrics Startup Module
  /// </summary>
  public interface IMetricsStartupModule : IStartupModule
  {
    /// <summary>
    /// Add Metrics
    /// </summary>
    /// <param name="t_ServiceCollection"></param>
    void AddMetrics(IServiceCollection t_ServiceCollection);

    /// <summary>
    /// Apply Metrics
    /// </summary>
    /// <param name="t_Builder"></param>
    void ApplyMetrics(IApplicationBuilder t_Builder);
  }
}