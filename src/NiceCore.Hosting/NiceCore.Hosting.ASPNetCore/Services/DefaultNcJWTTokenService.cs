﻿using NiceCore.Hosting.Services;
using NiceCore.Hosting.Tokens;
using NiceCore.Hosting.Utils;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.Extensions.Logging;

namespace NiceCore.Hosting.ASPNetCore.Services
{
  /// <summary>
  /// Impl for INcJWTTokenService
  /// </summary>
  [Register(InterfaceType = typeof(INcJWTTokenService), LifeStyle = LifeStyles.Singleton)]
  public class DefaultNcJWTTokenService : INcJWTTokenService
  {
    /// <summary>
    /// Configuration
    /// </summary>
    public INcConfiguration Configuration { get; set; }

    /// <summary>
    /// Options to use for token creation/validation
    /// </summary>
    public IJwtTokenOptionsAccessor JwtTokenOptionsAccessor { get; set; }
    
    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<DefaultNcJWTTokenService> Logger { get; set; }

    /// <summary>
    /// Generate token
    /// </summary>
    /// <param name="i_UserID"></param>
    /// <returns></returns>
    public NcJWTResult GenerateJWTToken(string i_UserID)
    {
      var claims = new List<Func<Claim>>()
      {
        () => new Claim(NcJWTSupport.TOKEN_CLAIM_USER_ID, i_UserID),
      };
      return GenerateJWTToken(claims, DefaultTokenSettings.GetKeys());
    }

    /// <summary>
    /// Generate token with custom claims
    /// </summary>
    /// <param name="i_Claims"></param>
    /// <returns></returns>
    public NcJWTResult GenerateJWTToken(IEnumerable<Func<Claim>> i_Claims)
    {
      return GenerateJWTToken(i_Claims, DefaultTokenSettings.GetKeys());
    }

    /// <summary>
    /// Generate token with custom claims
    /// </summary>
    /// <param name="i_Claims"></param>
    /// <returns></returns>
    public NcJWTResult GenerateNotExpiringJWTToken(IEnumerable<Func<Claim>> i_Claims)
    {
      return GenerateNotExpiringJWTToken(i_Claims, DefaultTokenSettings.GetKeys());
    }

    /// <summary>
    /// Generate token
    /// </summary>
    /// <param name="i_UserID"></param>
    /// <param name="i_TokenSettingKeys"></param>
    /// <returns></returns>
    public NcJWTResult GenerateJWTToken(string i_UserID, ITokenSettingKeys i_TokenSettingKeys)
    {
      var claims = new List<Func<Claim>>()
      {
        () => new Claim(NcJWTSupport.TOKEN_CLAIM_USER_ID, i_UserID),
      };
      return GenerateJWTToken(claims, i_TokenSettingKeys);
    }

    /// <summary>
    /// Generate token with custom claims
    /// </summary>
    /// <param name="i_Claims"></param>
    /// <param name="i_TokenSettingKeys"></param>
    /// <returns></returns>
    public NcJWTResult GenerateJWTToken(IEnumerable<Func<Claim>> i_Claims, ITokenSettingKeys i_TokenSettingKeys)
    {
      var tokenSettings = JwtTokenOptionsAccessor.GetSettings(i_TokenSettingKeys);
      return NcJWTSupport.GenerateJWTToken(tokenSettings.ExpirationTime, tokenSettings.ExpirationTimeKind, tokenSettings.Secret, i_Claims, JwtTokenOptionsAccessor.GetTokenOptions());
    }

    /// <summary>
    /// Generate token with custom claims
    /// </summary>
    /// <param name="i_Claims"></param>
    /// <param name="i_TokenSettingKeys"></param>
    /// <returns></returns>
    public NcJWTResult GenerateNotExpiringJWTToken(IEnumerable<Func<Claim>> i_Claims, ITokenSettingKeys i_TokenSettingKeys)
    {
      var tokenSettings = JwtTokenOptionsAccessor.GetSettings(i_TokenSettingKeys);
      return NcJWTSupport.GenerateNotExpiringJWTToken(i_Claims, tokenSettings.Secret, JwtTokenOptionsAccessor.GetTokenOptions());
    }

    /// <summary>
    /// Validate token
    /// </summary>
    /// <param name="i_Token"></param>
    /// <returns></returns>
    public NcTokenValidationResult ValidateJWTToken(string i_Token)
    {
      return ValidateJWTToken(i_Token, DefaultTokenSettings.GetKeys());
    }

    /// <summary>
    /// Validate token
    /// </summary>
    /// <param name="i_Token"></param>
    /// <param name="i_TokenSettingKeys"></param>
    /// <returns></returns>
    public NcTokenValidationResult ValidateJWTToken(string i_Token, ITokenSettingKeys i_TokenSettingKeys)
    {
      var tokenSettings = JwtTokenOptionsAccessor.GetSettings(i_TokenSettingKeys);
      return NcJWTSupport.ValidateJWTToken(i_Token, tokenSettings.Secret, JwtTokenOptionsAccessor.GetTokenOptions(), Logger);
    }
  }
}
