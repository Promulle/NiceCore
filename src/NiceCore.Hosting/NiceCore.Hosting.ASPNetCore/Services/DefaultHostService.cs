﻿using Microsoft.AspNetCore.Hosting;
using NiceCore.Hosting.Services;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using System.Net;
using Microsoft.Extensions.Logging;

namespace NiceCore.Hosting.ASPNetCore.Services
{
  /// <summary>
  /// Default implementation of IHostService
  /// </summary>
  [InitializeOnResolve]
  [Register(InterfaceType = typeof(IHostService), LifeStyle = LifeStyles.Singleton)]
  public class DefaultHostService : BaseHostService
  {
    private const string KEY_SECTION = "ServerConfig";
    private const string PATH_VALUE_PORT = KEY_SECTION + ".Port";
    private const string PATH_VALUE_ADDRESS = KEY_SECTION + ".Address";
    private const int DEFAULT_PORT = 8080;

    /// <summary>
    /// Ip Adress of server
    /// </summary>
    public string ServerIP { get; set; } = IPAddress.Loopback.ToString();

    /// <summary>
    /// Port of server
    /// </summary>
    public int Port { get; set; } = DEFAULT_PORT;

    /// <summary>
    /// Configuration
    /// </summary>
    public INcConfiguration NcConfiguration { get; set; }
    
    /// <summary>
    /// Logger
    /// </summary>
    public ILogger<DefaultHostService> HostServiceLogger { get; set; }

    /// <summary>
    /// logger
    /// </summary>
    public override ILogger Logger
    {
      get
      {
        return HostServiceLogger;
      }
    }


    /// <summary>
    /// Reads configuration and sets needed values according to read data.
    /// </summary>
    protected override void InternalInitialize()
    {
      base.InternalInitialize();
      ServiceStartup.Initialize();
      ReadConfig();
    }

    /// <summary>
    /// Reads config if existing, else it will use loopback as IP and 8080 as port
    /// </summary>
    private void ReadConfig()
    {
      ServerIP = NcConfiguration.GetValue<string>(PATH_VALUE_ADDRESS, null);
      Port = NcConfiguration.GetValue<int>(PATH_VALUE_PORT, 8080);
      if (ServerIP == default)
      {
        ServerIP = IPAddress.Loopback + "";
      }
      if (Port == default)
      {
        Port = DEFAULT_PORT;
      }
    }

    /// <summary>
    /// override for kestrel
    /// </summary>
    /// <param name="i_Builder"></param>
    /// <returns></returns>
    protected override void PrepareWebHostBuilder(IWebHostBuilder i_Builder)
    {
      base.PrepareWebHostBuilder(i_Builder);
    }
  }
}
