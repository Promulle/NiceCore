namespace NiceCore.Hosting.ASPNetCore.Filters.DefaultHttpContextTraceIdentifiers
{
  /// <summary>
  /// Settings for Default HttpContext TraceIdentifier
  /// </summary>
  public sealed class DefaultHttpContextTraceIdentifierFilterSettingModel
  {
    private const string DEFAULT_HEADER_NAME = "RequestId";
    
    /// <summary>
    /// Whether to use the 
    /// </summary>
    public bool Use { get; set; }

    /// <summary>
    /// Name of header the trace identifier filter sets
    /// </summary>
    public string HeaderName { get; set; } = DEFAULT_HEADER_NAME;

    /// <summary>
    /// Creates a setting filled with Defaultvalues 
    /// </summary>
    /// <returns></returns>
    public static DefaultHttpContextTraceIdentifierFilterSettingModel Default()
    {
      return new DefaultHttpContextTraceIdentifierFilterSettingModel()
      {
        Use = false,
        HeaderName = DEFAULT_HEADER_NAME
      };
    }
  }
}