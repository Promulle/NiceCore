// Copyright 2021 Sycorax Systemhaus GmbH

using System;
using Microsoft.AspNetCore.Mvc.Filters;

namespace NiceCore.Hosting.ASPNetCore.Filters.DefaultHttpContextTraceIdentifiers
{
  /// <summary>
  /// Default Impl of a filter that sets the request id to the headers of the FE
  /// </summary>
  public sealed class DefaultHttpContextTraceIdentifierFilter : IActionFilter
  {
    private readonly string m_TraceIdentifierHeaderName;
    
    /// <summary>
    /// Ctor filling trace identifier header name
    /// </summary>
    /// <param name="i_TraceIdentifierHeaderName">Name of Header that gets the HttpContext.TraceIdentifier as name</param>
    public DefaultHttpContextTraceIdentifierFilter(string i_TraceIdentifierHeaderName)
    {
      m_TraceIdentifierHeaderName = i_TraceIdentifierHeaderName ?? throw new ArgumentException($"Could not create {nameof(DefaultHttpContextTraceIdentifierFilter)}. Parameter {nameof(i_TraceIdentifierHeaderName)} was null.");
    }
    
    /// <summary>
    /// Before Execute
    /// </summary>
    /// <param name="context"></param>
    public void OnActionExecuting(ActionExecutingContext context)
    {
      // Nothing here
    }

    /// <summary>
    /// Set TraceIdentifier Header here
    /// </summary>
    /// <param name="context"></param>
    public void OnActionExecuted(ActionExecutedContext context)
    {
      //if there was already data written to the response, this would fail so I check for readonly of headers here
      if (!context.HttpContext.Response.Headers.IsReadOnly)
      {
        context.HttpContext.Response.Headers[m_TraceIdentifierHeaderName] = context.HttpContext.TraceIdentifier;
      }
    }
  }
}
