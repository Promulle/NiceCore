using System;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Hosting.ASPNetCore.Filters.DefaultHttpContextTraceIdentifiers
{
  /// <summary>
  /// Setting for HttpContet
  /// </summary>
  [Register(InterfaceType = typeof(IConfigurableSettingModel))]
  public class DefaultHttpContextTraceIdentifierFilterSetting : IConfigurableSettingModel
  {
    /// <summary>
    /// KEY
    /// </summary>
    public const string KEY = "NiceCore.Hosting.Asp.Filters.DefaultHttpContextTraceIdentifierFilter";

    /// <summary>
    /// KEy
    /// </summary>
    public string Key { get; } = KEY;

    /// <summary>
    /// Setting Type
    /// </summary>
    public Type SettingType { get; } = typeof(DefaultHttpContextTraceIdentifierFilterSettingModel);
  }
}