using Microsoft.Extensions.DependencyInjection;

namespace NiceCore.Hosting.ASPNetCore.Extensions
{
  /// <summary>
  /// Interface marking objects that enable registering of additional services to the serviceCollection
  /// that is used in ConfigureServices of the NcServiceStartup.
  /// This is used before AddControllers is called.
  /// </summary>
  public interface IAdditionalServicesBeforeAddControllers
  {
    /// <summary>
    /// Function that enables adding services to t_Services
    /// </summary>
    /// <param name="t_Services"></param>
    void AddServices(IServiceCollection t_Services);
  }
}
