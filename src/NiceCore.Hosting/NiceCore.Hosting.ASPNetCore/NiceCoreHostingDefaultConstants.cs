﻿namespace NiceCore.Hosting.ASPNetCore
{
  /// <summary>
  /// Constants for Hosting
  /// </summary>
  public static class NiceCoreHostingASPNetCoreConstants
  {
    /// <summary>
    /// Name for assembly
    /// </summary>
    public static readonly string ASSEMBLY_NAME = "NiceCore.Hosting.ASPNetCore";
  }
}
