using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;

namespace NiceCore.Hosting.ASPNetCore.Configurability.Cors
{
  /// <summary>
  /// Default Configures Cors Support
  /// </summary>
  [Register(InterfaceType = typeof(IConfiguredCorsSupport))]
  public class DefaultConfiguredCorsSupport : IConfiguredCorsSupport
  {
    private CorsConfigurationModel m_Model;

    /// <summary>
    /// Configuration
    /// </summary>
    public INcConfiguration Configuration { get; set; }

    /// <summary>
    /// Load settings
    /// </summary>
    public void Initialize()
    {
      m_Model = Configuration?.GetValue<CorsConfigurationModel>(CorsConfigurationSetting.KEY, null);
    }

    /// <summary>
    /// Applies settings to i_Collection
    /// </summary>
    /// <param name="i_Collection"></param>
    public void ApplyCorsBuilder(IServiceCollection i_Collection)
    {
      if (m_Model != null)
      {
        i_Collection.AddCors(builder =>
        {
          builder.AddDefaultPolicy(policyBuilder =>
          {
            ConfigureCredentials(policyBuilder);
            ConfigureOrigins(policyBuilder);
            ConfigureHeaders(policyBuilder);
            ConfigureMethods(policyBuilder);
          });
        });
      }
    }

    private void ConfigureCredentials(CorsPolicyBuilder t_Builder)
    {
      if (m_Model.WithCredentials != null)
      {
        if (m_Model.WithCredentials == true)
        {
          t_Builder.AllowCredentials();
        }
        else
        {
          t_Builder.DisallowCredentials();
        }
      }
    }

    private void ConfigureOrigins(CorsPolicyBuilder t_Builder)
    {
      if (m_Model.AllowAnyOrigin)
      {
        t_Builder.AllowAnyOrigin();
      }
      else
      {
        t_Builder.WithOrigins(m_Model.AllowedOrigins);
      }
    }

    private void ConfigureHeaders(CorsPolicyBuilder t_Builder)
    {
      if (m_Model.AllowAnyHeader)
      {
        t_Builder.AllowAnyHeader();
      }
      else
      {
        t_Builder.WithHeaders(m_Model.AllowedHeaders);
      }
    }

    private void ConfigureMethods(CorsPolicyBuilder t_Builder)
    {
      if (m_Model.AllowAnyMethod)
      {
        t_Builder.AllowAnyMethod();
      }
      else
      {
        t_Builder.WithMethods(m_Model.AllowedMethods);
      }
    }

    /// <summary>
    /// Applies the cors Settings
    /// </summary>
    /// <param name="i_ApplicationBuilder"></param>
    public void ApplyCorsBuilder(IApplicationBuilder i_ApplicationBuilder)
    {
      if (m_Model != null)
      {
        i_ApplicationBuilder.UseCors();
      }
    }
  }
}