using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace NiceCore.Hosting.ASPNetCore.Configurability.Cors
{
  /// <summary>
  /// Interface for config based cors support
  /// </summary>
  public interface IConfiguredCorsSupport
  {
    /// <summary>
    /// Load Settings
    /// </summary>
    void Initialize();
    
    /// <summary>
    /// Apply Cors Builder to ServiceCollection
    /// </summary>
    /// <param name="i_Collection"></param>
    void ApplyCorsBuilder(IServiceCollection i_Collection);
    
    /// <summary>
    /// Apply Cors Builder to ApplicationBuilder
    /// </summary>
    /// <param name="i_ApplicationBuilder"></param>
    void ApplyCorsBuilder(IApplicationBuilder i_ApplicationBuilder);
  }
}