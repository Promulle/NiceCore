using System;

namespace NiceCore.Hosting.ASPNetCore.Configurability.Cors
{
  /// <summary>
  /// Cors Configuration Model
  /// </summary>
  public class CorsConfigurationModel
  {
    /// <summary>
    /// Allow Any Origin
    /// </summary>
    public bool AllowAnyOrigin { get; set; }
    
    /// <summary>
    /// Allow Any Header
    /// </summary>
    public bool AllowAnyHeader { get; set; }
    
    /// <summary>
    /// Allow Any Method
    /// </summary>
    public bool AllowAnyMethod { get; set; }
    
    /// <summary>
    /// With Credentials - True = AllowCredentials False = DisallowCredentials NULL = Dont touch
    /// </summary>
    public bool? WithCredentials { get; set; }

    /// <summary>
    /// Allowed Origins
    /// </summary>
    public string[] AllowedOrigins { get; set; } = Array.Empty<string>();
    
    /// <summary>
    /// Allowed Headers
    /// </summary>
    public string[] AllowedHeaders { get; set; } = Array.Empty<string>();
    
    /// <summary>
    /// Allowed Methods
    /// </summary>
    public string[] AllowedMethods { get; set; } = Array.Empty<string>();
  }
}