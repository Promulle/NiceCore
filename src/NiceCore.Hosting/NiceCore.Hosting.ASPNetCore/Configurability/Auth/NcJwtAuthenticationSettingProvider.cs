using System;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NiceCore.Hosting.Tokens;
using NiceCore.Logging;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation;

namespace NiceCore.Hosting.ASPNetCore.Configurability.Auth
{
  /// <summary>
  /// NiceCore JWT Impl
  /// </summary>
  public class NcJwtAuthenticationSettingProvider : IAuthenticationSettingProvider
  {
    /// <summary>
    /// Service Container
    /// </summary>
    public IServiceContainer ServiceContainer { get; }
    
    /// <summary>
    /// JwtToken Option Accessor
    /// </summary>
    public IJwtTokenOptionsAccessor JwtTokenOptionsAccessor { get; }
    
    /// <summary>
    /// Logger
    /// </summary>
    public ILogger Logger { get; }

    /// <summary>
    /// Ctor filling dependencies
    /// </summary>
    /// <param name="i_Accessor"></param>
    /// <param name="i_Container"></param>
    /// <param name="i_Logger"></param>
    public NcJwtAuthenticationSettingProvider(IJwtTokenOptionsAccessor i_Accessor, IServiceContainer i_Container, ILogger i_Logger)
    {
      ServiceContainer = i_Container;
      JwtTokenOptionsAccessor = i_Accessor;
      Logger = i_Logger;
    }
    
    /// <summary>
    /// Apply Authentication
    /// </summary>
    /// <param name="t_Services"></param>
    public void ApplyAuthentication(IServiceCollection t_Services)
    {
      t_Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
        .AddJwtBearer(options =>
        {
          options.RequireHttpsMetadata = false;
          AddCustomTokenValidatorIfNeeded(options);
          options.SaveToken                 = true;
          options.TokenValidationParameters = JwtTokenOptionsAccessor.GetValidationParametersByConfig(DefaultTokenSettings.GetKeys());
          AddJWTEventsIfNeeded(options);
        });
    }
    
    private void AddCustomTokenValidatorIfNeeded(JwtBearerOptions t_Options)
    {
      foreach (var configurator in ServiceContainer.ResolveAll<ISecurityTokenValidatorConfigurator>())
      {
        Logger.Information($"Applying changes to {t_Options.SecurityTokenValidators} collection using configurator of type {configurator.GetType().FullName}");
        try
        {
          configurator.Apply(t_Options.SecurityTokenValidators);
        }
        catch (Exception ex)
        {
          Logger.Error(ex, $"Error applying changes to {t_Options.SecurityTokenValidators} using configurator of type {configurator.GetType().FullName}");
        }
        ServiceContainer.Release(configurator);
      }
    }

    private void AddJWTEventsIfNeeded(JwtBearerOptions t_Options)
    {
      var optionalEvents = ServiceContainer.Resolve<IJwtEvents>();
      if (optionalEvents != null)
      {
        Logger.Information($"Adding custom events defined by instance of type {optionalEvents.GetType().FullName}");
        t_Options.Events = optionalEvents.GetEvents();
        ServiceContainer.Release(optionalEvents);
      }
    }
  }
}
