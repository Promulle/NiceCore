using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation;

namespace NiceCore.Hosting.ASPNetCore.Configurability.Auth
{
  /// <summary>
  /// Helper class for applying authorization setting provider
  /// </summary>
  public static class NcAuthorizationSettingProvider
  {
    /// <summary>
    /// Apply Authorization
    /// </summary>
    public static void ApplyAuthorization(IServiceCollection t_Services, ILogger i_Logger, IServiceContainer i_Container)
    {
      var registeredProvider = i_Container.Resolve<IAuthorizationSettingProvider>();
      if (registeredProvider != null)
      {
        i_Logger.Debug($"Applying Authorization with registered provider of type {registeredProvider.GetType()}");
        registeredProvider.ApplyAuthorization(t_Services);
        i_Container.Release(registeredProvider);
      }
      else
      {
        i_Logger.Debug($"Applying Authorization with default provider of type {nameof(NcJwtAuthorizationSettingProvider)}");
        new NcJwtAuthorizationSettingProvider().ApplyAuthorization(t_Services);
      }
    }
  }
}
