using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NiceCore.Hosting.Tokens;
using NiceCore.Logging.Extensions;
using NiceCore.ServiceLocation;

namespace NiceCore.Hosting.ASPNetCore.Configurability.Auth
{
  /// <summary>
  /// Helper class for applying authentication to the services
  /// </summary>
  public static class NcAuthenticationSettingProvider
  {
    /// <summary>
    /// Apply authentication
    /// </summary>
    /// <param name="t_Services"></param>
    /// <param name="i_Container"></param>
    /// <param name="i_Accessor"></param>
    /// <param name="i_Logger"></param>
    public static void ApplyAuthentication(IServiceCollection t_Services, IServiceContainer i_Container, IJwtTokenOptionsAccessor i_Accessor, ILogger i_Logger)
    {
      var registeredAuthenticationProvider = i_Container.Resolve<IAuthenticationSettingProvider>();
      if (registeredAuthenticationProvider != null)
      {
        i_Logger.Debug($"Applying Authentication with registered provider of type {registeredAuthenticationProvider.GetType()}");
        registeredAuthenticationProvider.ApplyAuthentication(t_Services);
      }
      else
      {
        i_Logger.Debug($"Applying Authentication with default provider of type {nameof(NcJwtAuthorizationSettingProvider)}");
        new NcJwtAuthenticationSettingProvider(i_Accessor, i_Container, i_Logger).ApplyAuthentication(t_Services);
      }
    }
  }
}
