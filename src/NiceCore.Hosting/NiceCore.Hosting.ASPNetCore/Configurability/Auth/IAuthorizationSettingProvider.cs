using Microsoft.Extensions.DependencyInjection;

namespace NiceCore.Hosting.ASPNetCore.Configurability.Auth
{
  /// <summary>
  /// Interface for providers used to apply authorization for the app
  /// </summary>
  public interface IAuthorizationSettingProvider
  {
    /// <summary>
    /// Applies the authorization to ncservice startup
    /// </summary>
    /// <param name="t_Services"></param>
    void ApplyAuthorization(IServiceCollection t_Services);
  }
}
