using Microsoft.Extensions.DependencyInjection;

namespace NiceCore.Hosting.ASPNetCore.Configurability.Auth
{
  /// <summary>
  /// Authentication setting provider
  /// </summary>
  public interface IAuthenticationSettingProvider
  {
    /// <summary>
    /// Applies authentication
    /// </summary>
    /// <param name="t_Services"></param>
    void ApplyAuthentication(IServiceCollection t_Services);
  }
}
