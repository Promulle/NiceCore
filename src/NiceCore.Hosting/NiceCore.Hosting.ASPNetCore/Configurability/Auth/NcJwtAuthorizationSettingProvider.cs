using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using NiceCore.Hosting.ASPNetCore.Auth;

namespace NiceCore.Hosting.ASPNetCore.Configurability.Auth
{
  /// <summary>
  /// Default NC Jwt Authorization setting provider
  /// </summary>
  public class NcJwtAuthorizationSettingProvider : IAuthorizationSettingProvider
  {
    /// <summary>
    /// Apply auth
    /// </summary>
    /// <param name="t_Services"></param>
    public void ApplyAuthorization(IServiceCollection t_Services)
    {
      t_Services.AddSingleton<IAuthorizationPolicyProvider, PermissionPolicyProvider>();
      t_Services.AddSingleton<IAuthorizationHandler, HasPermissionHandler>();
      t_Services.AddAuthorization(options =>
      {
        var policyBuilder = new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme);
        policyBuilder         = policyBuilder.RequireAuthenticatedUser();
        options.DefaultPolicy = policyBuilder.Build();
      });
    }
  }
}
