using System;
using Microsoft.AspNetCore.Builder;

namespace NiceCore.Hosting.ASPNetCore.Configurability
{
  /// <summary>
  /// Interface used to configure cors setting
  /// </summary>
  [Obsolete("Replaced by Configured CORS Policies")]
  public interface ICorsSettingsProvider
  {
    /// <summary>
    /// Applies settings about cors
    /// </summary>
    /// <param name="t_Builder"></param>
    void ApplyCorsSettings(IApplicationBuilder t_Builder);
  }
}
