﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NiceCore.Hosting.ASPNetCore
{
  /// <summary>
  /// Comparer impl for endpoints
  /// </summary>
  public class EndpointComparer : IComparer<string>
  {
    /// <summary>
    /// Compare by endpoint
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public int Compare(string x, string y)
    {
      var scoreLeft = GetGenericScoreOf(x);
      var scoreRight = GetGenericScoreOf(y);
      if (scoreLeft < scoreRight)
      {
        return -1;
      }
      else if (scoreLeft > scoreRight)
      {
        return 1;
      }
      return 0;
    }
    /// <summary>
    /// Calculates how "generic" an endpoint is.
    /// greater = more generic
    /// </summary>
    /// <param name="i_Endpoint"></param>
    /// <returns></returns>
    private static int GetGenericScoreOf(string i_Endpoint)
    {
      if (i_Endpoint != null)
      {
        var score = 0;
        var layers = i_Endpoint.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries).ToList();
        foreach (var layer in layers)
        {
          if (layer.Contains("{") && layer.Contains("}"))
          {
            score++;
            if (layer.Contains("{*"))
            {
              score += 10;
            }
          }
          else
          {
            score--;
          }
        }
        return score;
      }
      return -1;
    }
  }
}
