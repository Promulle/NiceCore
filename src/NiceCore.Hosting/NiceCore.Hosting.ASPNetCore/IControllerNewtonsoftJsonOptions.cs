using System;
using Microsoft.AspNetCore.Mvc;

namespace NiceCore.Hosting.ASPNetCore
{
  /// <summary>
  /// Options to be used for newtonsoft json
  /// </summary>
  public interface IControllerNewtonsoftJsonOptions
  {
    /// <summary>
    /// Func
    /// </summary>
    Action<MvcNewtonsoftJsonOptions> OptionsFunc { get; set; }
  }
}
