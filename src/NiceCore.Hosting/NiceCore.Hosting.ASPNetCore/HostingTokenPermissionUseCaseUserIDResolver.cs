﻿using NiceCore.Hosting.Permissions;
using NiceCore.Hosting.Services;
using NiceCore.ServiceLocation;
using NiceCore.Services.Permissions;
using System.Threading.Tasks;

namespace NiceCore.Hosting.ASPNetCore
{
  /// <summary>
  /// User id resolver for Token hosting use case
  /// </summary>
  [Register(InterfaceType = typeof(IPermissionUseCaseUserIDResolver))]
  public class HostingTokenPermissionUseCaseUserIDResolver : BasePermissionUseCaseUserIDResolver<PermissionHostingTokenUseCase>
  {
    /// <summary>
    /// Token service
    /// </summary>
    public INcJWTTokenService NcJWTTokenService { get; set; }

    /// <summary>
    /// Get user id
    /// </summary>
    /// <param name="i_UseCase"></param>
    /// <returns></returns>
    protected override Task<object> GetUserIDInternal(PermissionHostingTokenUseCase i_UseCase)
    {
      var userId = NcJWTTokenService.ValidateJWTToken(i_UseCase.Token).UserID;
      return Task.FromResult<object>(userId);
    }
  }
}
