using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using NiceCore.Services.Config;

namespace NiceCore.Hosting.ASPNetCore.Plugins.Impl
{
  /// <summary>
  /// NcWebHostBuildingPlugin that enables the use of the server as TestServer
  /// </summary>
  public class TestServerNcWebHostBuildingPlugin : INcWebHostBuildingPlugin
  {
    /// <summary>
    /// Configure t_Builder to be used as TestServer
    /// </summary>
    /// <param name="t_Builder"></param>
    /// <param name="i_Configuration"></param>
    public void Configure(IWebHostBuilder t_Builder, INcConfiguration i_Configuration)
    {
      t_Builder.ConfigureServices(services => services.AddSingleton<IServer>(serviceProvider => new TestServer(serviceProvider)));
    }
  }
}