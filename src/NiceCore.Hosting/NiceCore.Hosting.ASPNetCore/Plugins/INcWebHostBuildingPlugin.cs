using Microsoft.AspNetCore.Hosting;
using NiceCore.Services.Config;

namespace NiceCore.Hosting.ASPNetCore.Plugins
{
  /// <summary>
  /// Interface for plugins that customize the building of the webhost
  /// </summary>
  public interface INcWebHostBuildingPlugin
  {
    /// <summary>
    /// Configures t_Builder
    /// </summary>
    /// <param name="t_Builder"></param>
    /// <param name="i_Configuration"></param>
    void Configure(IWebHostBuilder t_Builder, INcConfiguration i_Configuration);
  }
}