using Microsoft.AspNetCore.Mvc;

namespace NiceCore.Hosting.ASPNetCore
{
  /// <summary>
  /// Interface that enables customization of the controller adding process (for adding global filters for example)
  /// </summary>
  public interface INcControllerOptionConfigurator
  {
    /// <summary>
    /// Apply the configurator to the given options
    /// </summary>
    /// <param name="i_Options"></param>
    void ApplyOptions(MvcOptions i_Options);
  }
}
