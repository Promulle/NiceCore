﻿using System;
using System.Net.Http.Headers;
using System.Text;

namespace NiceCore.Hosting.ASPNetCore.Utils
{
  /// <summary>
  /// Utils for basic auth string manipulation
  /// </summary>
  public static class BasicAuthValueUtil
  {
    /// <summary>
    /// parse
    /// </summary>
    /// <param name="i_Value"></param>
    /// <param name="i_Encoding">Encoding to use to convert</param>
    /// <returns></returns>
    public static (string userName, string password) Parse(AuthenticationHeaderValue i_Value, Encoding i_Encoding)
    {
      var bytes = Convert.FromBase64String(i_Value.Parameter);
      if (bytes.Length > 0)
      {
        var valueAsString = i_Encoding.GetString(bytes);
        if (!valueAsString.Contains(':'))
        {
          throw new InvalidOperationException("Could not parse value of header for basic authentication. The header is probably not correctly formatted.");
        }
        var nameAndPw = valueAsString.Split(new[] { ':' }, 2);
        return (nameAndPw[0], nameAndPw[1]);
      }
      return (null, null);
    }
  }
}
