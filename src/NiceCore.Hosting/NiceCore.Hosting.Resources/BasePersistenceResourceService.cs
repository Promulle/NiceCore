﻿using NiceCore.Entities;
using NiceCore.Hosting.Resources.QueryRequests;
using NiceCore.Storage;

namespace NiceCore.Hosting.Resources
{
  /// <summary>
  /// Resource service for persistence query requests
  /// </summary>
  public abstract class BasePersistenceResourceService<TResource, TKey, TResourceDTO> : BaseResourceService<TResource, TKey, TResourceDTO> where TResource : IDomainEntity<TKey>
  {
    /// <summary>
    /// StorageService
    /// </summary>
    public IStorageService StorageService { get; set; }

    /// <summary>
    /// Internal initialize
    /// </summary>
    protected override void InternalInitialize()
    {
      base.InternalInitialize();
      StorageService.Initialize();
    }

    /// <summary>
    /// Gets the connection name to be used for querying the resources
    /// </summary>
    /// <returns></returns>
    protected abstract string GetConnectionName();

    /// <summary>
    /// Returns a persistnce ResourceRequest
    /// </summary>
    /// <returns></returns>
    protected override IResourceRequest<TResource, TKey> GetResourceRequest()
    {
      return new PersistenceResourceRequest<TResource, TKey>(StorageService.Conversation(GetConnectionName()));
    }
  }
}
