﻿using NiceCore.Base;
using NiceCore.Entities;
using NiceCore.Storage;
using NiceCore.Storage.Conversations;
using System.Linq;

namespace NiceCore.Hosting.Resources.QueryRequests
{
  /// <summary>
  /// Impl of IResourceQueryRequest for database calls
  /// </summary>
  public class PersistenceResourceRequest<TResource, TKey> : BaseDisposable, IResourceRequest<TResource, TKey> where TResource : IDomainEntity<TKey>
  {
    private IQueryable<TResource> m_Query;
    private IConversation m_Conversation;

    /// <summary>
    /// Fill constructor
    /// </summary>
    public PersistenceResourceRequest(IConversation i_Conversation)
    {
      m_Conversation = i_Conversation;
    }

    /// <summary>
    /// Apply
    /// </summary>
    public void ApplyChanges()
    {
      m_Conversation.Apply();
    }

    /// <summary>
    /// Deletes an instance
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <returns></returns>
    public void Delete(TResource i_Instance)
    {
      m_Conversation.Delete<TResource, TKey>(i_Instance);
    }

    /// <summary>
    /// Returns a cached query or returns a new if none is existend
    /// </summary>
    /// <returns></returns>
    public IQueryable<TResource> GetQuery()
    {
      if (m_Query == null)
      {
        m_Query = m_Conversation.Query<TResource, TKey>();
      }
      return m_Query;
    }

    /// <summary>
    /// Save or update
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <returns></returns>
    public void SaveOrUpdate(TResource i_Instance)
    {
      m_Conversation.SaveOrUpdate<TResource, TKey>(i_Instance);
    }

    /// <summary>
    /// Dispose conversation
    /// </summary>
    /// <param name="i_IsDisposing"></param>
    protected override void Dispose(bool i_IsDisposing)
    {
      m_Conversation.Dispose();
      base.Dispose(i_IsDisposing);
    }
  }
}
