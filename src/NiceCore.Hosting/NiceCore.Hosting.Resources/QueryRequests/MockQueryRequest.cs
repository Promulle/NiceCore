﻿using NiceCore.Base;
using NiceCore.Entities;
using System.Collections.Generic;
using System.Linq;

namespace NiceCore.Hosting.Resources.QueryRequests
{
  /// <summary>
  /// QueryRequest for mocked Data
  /// </summary>
  /// <typeparam name="TResource"></typeparam>
  /// <typeparam name="TKey"></typeparam>
  public class MockResourceRequest<TResource, TKey> : BaseDisposable, IResourceRequest<TResource, TKey> where TResource : IDomainEntity<TKey>
  {
    private readonly IEnumerable<TResource> m_Data;

    /// <summary>
    /// Fill constructor
    /// </summary>
    public MockResourceRequest(IEnumerable<TResource> i_MockedData)
    {
      m_Data = i_MockedData;
    }

    /// <summary>
    /// Nothing
    /// </summary>
    public void ApplyChanges()
    {
    }

    /// <summary>
    /// Nothing
    /// </summary>
    /// <param name="i_Instance"></param>
    public void Delete(TResource i_Instance)
    {
    }

    /// <summary>
    /// Returns a query
    /// </summary>
    /// <returns></returns>
    public IQueryable<TResource> GetQuery()
    {
      return m_Data.AsQueryable();
    }

    /// <summary>
    /// Nothing
    /// </summary>
    /// <param name="i_Instance"></param>
    public void SaveOrUpdate(TResource i_Instance)
    {
    }
  }
}
