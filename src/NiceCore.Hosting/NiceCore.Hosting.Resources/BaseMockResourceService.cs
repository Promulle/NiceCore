﻿using NiceCore.Entities;
using NiceCore.Hosting.Resources.QueryRequests;
using System.Collections.Generic;

namespace NiceCore.Hosting.Resources
{
  /// <summary>
  /// BaseResourceService for mocked data
  /// </summary>
  public abstract class BaseMockResourceService<TResource, TResourceKey, TResourceDTO> : BaseResourceService<TResource, TResourceKey, TResourceDTO> where TResource : IDomainEntity<TResourceKey>
  {
    /// <summary>
    /// Returns a mock request
    /// </summary>
    /// <returns></returns>
    protected override IResourceRequest<TResource, TResourceKey> GetResourceRequest()
    {
      return new MockResourceRequest<TResource, TResourceKey>(GetMockData());
    }

    /// <summary>
    /// Returns mocked data
    /// </summary>
    /// <returns></returns>
    protected abstract IEnumerable<TResource> GetMockData();
  }
}
