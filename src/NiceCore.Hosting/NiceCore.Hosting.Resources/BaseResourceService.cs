﻿using NiceCore.Base.Constants;
using NiceCore.Base.Services;
using NiceCore.Entities;
using NiceCore.ObjectMapping;
using NiceCore.Storage;
using System.Collections.Generic;
using System.Linq;

namespace NiceCore.Hosting.Resources
{
  /// <summary>
  /// Base impl of resource service with long key resources
  /// </summary>
  /// <typeparam name="TResource"></typeparam>
  /// <typeparam name="TResourceDTO"></typeparam>
  public abstract class BaseDefaultResourceService<TResource, TResourceDTO> : BaseResourceService<TResource, long, TResourceDTO> where TResource : IDomainEntity<long>
  {
  }

  /// <summary>
  /// Base impl for IResourceService
  /// </summary>
  [HostedService(UseRuntimeEndpoint = true)]
  public abstract class BaseResourceService<TResource, TResourceKey, TResourceDTO> : BaseService, IResourceService<TResource, TResourceKey, TResourceDTO> where TResource : IDomainEntity<TResourceKey>
  {
    /// <summary>
    /// Config for resource to dto
    /// </summary>
    protected IMappingConfig<TResource, TResourceDTO> ResourceToDTOConfig { get; set; }

    /// <summary>
    /// Config for dto to resource
    /// </summary>
    protected IMappingConfig<TResourceDTO, TResource> DTOToResourceConfig { get; set; }

    /// <summary>
    /// Service for mapping
    /// </summary>
    public IObjectMappingService ObjectMappingService { get; set; }

    /// <summary>
    /// set configs
    /// </summary>
    protected override void InternalInitialize()
    {
      base.InternalInitialize();
      ResourceToDTOConfig = GetResourceToDTOConfig();
      DTOToResourceConfig = GetDTOToResourceConfig();
    }

    /// <summary>
    /// Returns a list of all resources for this type
    /// </summary>
    /// <returns></returns>
    [HostedServiceOperation(ContentType = ContentTypes.APP_JSON, Endpoint = "", Method = HttpMethods.GET)]
    public IEnumerable<TResourceDTO> GetAll()
    {
      using (var req = GetResourceRequest())
      {
        var list = PrepareQuery(req.GetQuery())
           .ToList();
        return ObjectMappingService.MapCollection<TResource, TResourceDTO>(list);
      }
    }

    /// <summary>
    /// Gets a resource by id
    /// </summary>
    /// <param name="i_Key"></param>
    /// <returns></returns>
    [HostedServiceOperation(ContentType = ContentTypes.APP_JSON, Endpoint = "/{id}", Method = HttpMethods.GET)]
    [OperationParameter(MethodParameterName = "i_Key", ParameterName = "id", ParameterType = ParameterTypes.Url)]
    public TResourceDTO GetById(TResourceKey i_Key)
    {
      using (var req = GetResourceRequest())
      {
        var entity = PrepareQuery(req.GetQuery().WhereIdMatches(r => r.ID, i_Key))
          .ToList()
          .FirstOrDefault();
        return ObjectMappingService.Map(entity, ResourceToDTOConfig);
      }
    }

    /// <summary>
    /// Gets a resource by id
    /// </summary>
    /// <param name="i_Key"></param>
    /// <returns></returns>
    [HostedServiceOperation(ContentType = ContentTypes.APP_JSON, Endpoint = "/{id}", Method = HttpMethods.DELETE)]
    [OperationParameter(MethodParameterName = "i_Key", ParameterName = "id", ParameterType = ParameterTypes.Url)]
    public void DeleteById(TResourceKey i_Key)
    {
      using (var req = GetResourceRequest())
      {
        var entity = PrepareQuery(req.GetQuery().WhereIdMatches(r => r.ID, i_Key))
          .ToList()
          .FirstOrDefault();
        if (entity != null)
        {
          req.Delete(entity);
          req.ApplyChanges();
        }
      }
    }

    /// <summary>
    /// Returns the entityName as endpoint, no leading slash
    /// </summary>
    /// <returns></returns>
    public string GetEndpoint()
    {
      return typeof(TResource).Name;
    }

    /// <summary>
    /// Saves or updates a resource
    /// </summary>
    /// <param name="i_Dto"></param>
    [HostedServiceOperation(ContentType = ContentTypes.APP_JSON, Endpoint = "", Method = HttpMethods.PUT)]
    [OperationParameter(MethodParameterName = "i_Dto", ParameterType = ParameterTypes.RequestBody)]
    public void Put(TResourceDTO i_Dto)
    {
      var resource = ObjectMappingService.Map(i_Dto, DTOToResourceConfig);
      using (var req = GetResourceRequest())
      {
        req.SaveOrUpdate(resource);
        req.ApplyChanges();
      }
    }



    /// <summary>
    /// prepares the query if needed
    /// </summary>
    /// <param name="i_ResourceQuery"></param>
    /// <returns></returns>
    protected virtual IQueryable<TResource> PrepareQuery(IQueryable<TResource> i_ResourceQuery)
    {
      return i_ResourceQuery;
    }

    /// <summary>
    /// Returns a query request for the resource
    /// </summary>
    /// <returns></returns>
    protected abstract IResourceRequest<TResource, TResourceKey> GetResourceRequest();

    /// <summary>
    /// Mapping config for mapping resource to dto
    /// </summary>
    protected virtual IMappingConfig<TResource, TResourceDTO> GetResourceToDTOConfig()
    {
      return NcMappingConfig.NewConfig<TResource, TResourceDTO>();
    }

    /// <summary>
    /// Mapping config for mapping resource to dto
    /// </summary>
    protected virtual IMappingConfig<TResourceDTO, TResource> GetDTOToResourceConfig()
    {
      return NcMappingConfig.NewConfig<TResourceDTO, TResource>();
    }
  }
}
