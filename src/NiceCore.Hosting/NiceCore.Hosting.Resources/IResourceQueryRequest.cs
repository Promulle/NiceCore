﻿using NiceCore.Entities;
using System;
using System.Linq;

namespace NiceCore.Hosting.Resources
{
  /// <summary>
  /// A query used to query/manipulate resources
  /// </summary>
  public interface IResourceRequest<TResource, TKey> : IDisposable where TResource : IDomainEntity<TKey>
  {
    /// <summary>
    /// Gets the real query
    /// </summary>
    /// <returns></returns>
    IQueryable<TResource> GetQuery();

    /// <summary>
    /// Save or update instance
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <returns></returns>
    void SaveOrUpdate(TResource i_Instance);

    /// <summary>
    /// Deletes a resource
    /// </summary>
    /// <param name="i_Instance"></param>
    /// <returns></returns>
    void Delete(TResource i_Instance);

    /// <summary>
    /// Apply changes
    /// </summary>
    void ApplyChanges();
  }
}
