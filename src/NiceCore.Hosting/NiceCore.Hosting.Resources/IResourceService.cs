﻿using NiceCore.Hosting.Services;
using System.Collections.Generic;

namespace NiceCore.Hosting.Resources
{
  /// <summary>
  /// service for retrieving resources
  /// </summary>
  public interface IResourceService<TResource, TResourceKey, TResourceDTO> : IEndpointProvidingService, IHostableService
  {
    /// <summary>
    /// returns all elements of TResource
    /// </summary>
    /// <returns></returns>
    IEnumerable<TResourceDTO> GetAll();

    /// <summary>
    /// Gets a resource by id
    /// </summary>
    /// <param name="i_Key"></param>
    /// <returns></returns>
    TResourceDTO GetById(TResourceKey i_Key);

    /// <summary>
    /// Saves or updates i_Dto
    /// </summary>
    /// <param name="i_Dto"></param>
    void Put(TResourceDTO i_Dto);
  }
}
