﻿# Auditing entities
NiceCore supports auditing for stored entities.
To use auditing for a certain connection, set the "AllowAuditing" property of the corresponding Connection Definition to true.

If this property is true when it is checked (In the NH implementation that would be when the config is created) all entities that were mapped 
in some way will be added as audited.

To query for entities that are audited you can use the following API provided in IConversation:
```csharp
    /// <summary>
    /// Find the object with i_ID in a specific revision. If auditing is not enabled, this function queries for the current object where id = i_ID
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="i_ID"></param>
    /// <param name="i_Revision"></param>
    /// <returns></returns>
    T FindRevision<T>(long i_ID, long i_Revision) where T : IBaseDomainEntity;

    /// <summary>
    /// Find the object with i_ID in a specific revision. If auditing is not enabled, this function queries for the current object where id = i_ID
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId">Type of Id</typeparam>
    /// <param name="i_ID"></param>
    /// <param name="i_Revision"></param>
    /// <returns></returns>
    T FindRevision<T, TId>(TId i_ID, long i_Revision) where T : IDomainEntity<TId>;
```
If you use this api on a connection where auditing is disabled, the current entity which corresponds to i_ID will be returned instead.
The audit implementation in the NH implementation is envers.