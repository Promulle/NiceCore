﻿param(
    [string]$fileName = '',
    [string]$target = 'build',
    [string]$mode = 'inc',
    [string]$setVersion = '',
    [string]$all = 'false',
    [string]$dir = ''
)


function Set-Version {
    param([string]$projfileName)
    $trueFileName = Resolve-Path $projfileName -ErrorAction Continue
    if ([System.IO.File]::Exists($trueFileName)){
        $fileCont = Get-Content $trueFileName
        $found = [string]$fileCont -match '\d*[.]\d*[.]\d*[.]*\d*'
        if ($found){
            $changeIndex = -1
            if ($mode -eq 'inc'){
                if ($target -eq 'build'){
                    $changeIndex = 3
                } elseif ($target -eq 'rev'){
                    $changeIndex = 2
                } elseif ($target -eq 'minor'){
                    $changeIndex = 1
                } elseif ($target -eq 'major'){
                    $changeIndex = 0
                } else {
                    Write-Host 'no supported target specified'
                    Write-Host 'supported: build, rev, minor, major'
                }
                foreach($version in $matches){
                    Write-Output $version
                    $versionSplit = $version[0].Split(".")
                    $toIncrease = -1
            
                    if ($changeIndex -ne -1){
                        [int]$toIncrease = [convert]::ToInt32($versionSplit[$changeIndex])
                        $increased = $toIncrease + 1
                        Write-Output $increased
                        $versionSplit[$changeIndex] = [convert]::ToString($increased)
                        $complete = $versionSplit[0] + '.' + $versionSplit[1] + '.' + $versionSplit[2] + '.' + $versionSplit[3]
                        Write-Output $complete
                        $fileCont -replace $version[0], $complete | Out-File $trueFileName
                    }
                }
            } elseif ($mode -eq 'set'){
                foreach($version in $matches){
                    $fileCont -replace $version[0], $setVersion | Out-File $trueFileName
                }
            }
        }
    } else {
        Write-Output 'file ' $fileName ' does not exist'
    }
}

function isFileCorrect([string] $file) {
	return !$file.Contains("Examples") -and !$file.Contains(".Test"); 
}

if ($all -eq 'true'){
    foreach($file in [System.IO.Directory]::GetFiles($dir, '*.csproj', 'AllDirectories')){
		if (isFileCorrect $file) {
			Set-Version $file
		}
    }
} else {
    Set-Version $fileName
}