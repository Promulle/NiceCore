﻿param(
    [string]$fileName = '',
    [string]$mode = 'build'
)
$fileName = Resolve-Path $fileName -ErrorAction Continue
if ([System.IO.File]::Exists($fileName)){
    $fileCont = Get-Content $fileName
    $found = [string]$fileCont -match '\d*[.]\d*[.]\d*[.]*\d*'
    if ($found){
        $changeIndex = -1
        if ($mode -eq 'build'){
            $changeIndex = 3
        } elseif ($mode -eq 'rev'){
            $changeIndex = 2
        } elseif ($mode -eq 'minor'){
            $changeIndex = 1
        } elseif ($mode -eq 'major'){
            $changeIndex = 0
        } else {
            Write-Host 'no supported mode specified'
            Write-Host 'supported: build, rev, minor, major'
        }
        foreach($version in $matches){
            Write-Output $version
            $versionSplit = $version[0].Split(".")
            $toIncrease = -1
            
            if ($changeIndex -ne -1){
                [int]$toIncrease = [convert]::ToInt32($versionSplit[$changeIndex])
                $increased = $toIncrease + 1
                Write-Output $increased
                $versionSplit[$changeIndex] = [convert]::ToString($increased)
                $complete = $versionSplit[0] + '.' + $versionSplit[1] + '.' + $versionSplit[2] + '.' + $versionSplit[3]
                Write-Output $complete
                $fileCont -replace $version[0], $complete | Out-File $fileName
            }
        }
    }
} else {
    Write-Output 'file ' $fileName ' does not exist'
}