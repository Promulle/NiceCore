using System.Threading;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using NiceCore.Concurrency;

namespace NiceCore.Benchmarks
{
  [MemoryDiagnoser(false)]
  public class LockBenchmark
  {
    private static readonly SemaphoreSlim m_SemLock = new SemaphoreSlim(1, 1);
    private static readonly OrderedLock m_OrderLock = new OrderedLock();

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    [Benchmark]
    public async Task RunSemaphore()
    {
      await m_SemLock.WaitAsync().ConfigureAwait(false);
      m_SemLock.Release();
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    [Benchmark]
    public async Task RunOrderLock()
    {
      await m_OrderLock.Wait().ConfigureAwait(false);
      m_OrderLock.Release();
    }
  }
}