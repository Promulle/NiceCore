using BenchmarkDotNet.Attributes;
using NiceCore.Benchmarks.Model;

namespace NiceCore.Benchmarks
{
  [MemoryDiagnoser(false)]
  public class ReflectionBenchmarks
  {
    [Benchmark]
    public bool IsSubClassOf()
    {
      return typeof(TestType).IsSubclassOf(typeof(BaseType));
    }

    [Benchmark]
    public bool IsAssignableFrom()
    {
      return typeof(BaseType).IsAssignableFrom(typeof(TestType));
    }
    
    [Benchmark]
    public bool IsAssignableTo()
    {
      return typeof(TestType).IsAssignableTo(typeof(BaseType));
    }
  }
}