﻿using BenchmarkDotNet.Attributes;
using NiceCore.Base.Instances;
using NiceCore.Benchmarks.Model;
using System;

namespace NiceCore.Benchmarks
{
  public class NcInstanceFactoryBenchmark
  {
    private const string NAME = "Hans Paul";
    private const int VALUE = 42;

    [Benchmark]
    public ObjectToCreate ActivatorCreateParameterless()
    {
      return Activator.CreateInstance<ObjectToCreate>();
    }

    [Benchmark]
    public ObjectToCreate ActivatorCreateParameterlessNonGeneric()
    {
      return Activator.CreateInstance(typeof(ObjectToCreate)) as ObjectToCreate;
    }

    [Benchmark]
    public ObjectToCreate ActivatorCreateParameters()
    {
      return Activator.CreateInstance(typeof(ObjectToCreate), NAME, VALUE) as ObjectToCreate;
    }

    [Benchmark]
    public ObjectToCreate NcInstanceFactoryCreateParameterless()
    {
      return NcInstanceFactory.CreateInstance<ObjectToCreate>();
    }

    [Benchmark]
    public ObjectToCreate NcInstanceFactoryCreateParameters()
    {
      return NcInstanceFactory.CreateInstance<ObjectToCreate>(NAME, 42);
    }

    [Benchmark]
    public ObjectToCreate NcInstanceFactoryCreateParametersWithPossibleNull()
    {
      return NcInstanceFactory.CreateInstance<ObjectToCreate>(InstanceParameter.For(null, typeof(string)), InstanceParameter.For(42));
    }
  }
}
