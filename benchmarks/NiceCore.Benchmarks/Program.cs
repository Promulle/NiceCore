﻿using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Running;

namespace NiceCore.Benchmarks
{
  internal static class Program
  {
    private static void Main(string[] args)
    {
      //var summary = BenchmarkRunner.Run(typeof(Program).Assembly, cfg);
      BenchmarkRunner.Run<ReflectionBenchmarks>();
    }
  }
}
