﻿using BenchmarkDotNet.Attributes;
using Microsoft.Extensions.Logging;
using NiceCore.Benchmarks.Model;
using NiceCore.Logging.Extensions;

namespace NiceCore.Benchmarks
{
  [MemoryDiagnoser(false)]
  public class LoggingBenchmarks
  {
    private static readonly ILogger m_Logger = new BenchmarkLogger(LogLevel.Critical);
    
    [Benchmark()]
    public void DefaultWay()
    {
      var day = 10;
      m_Logger.LogDebug($"Today is the day {day}");
    }
    
    [Benchmark()]
    public void DefaultWayNoParameters()
    {
      var day = 10;
      m_Logger.LogDebug("Today is the day 10");
    }
    
    [Benchmark()]
    public void DefaultWayNotInterpolated()
    {
      var day = 10;
      m_Logger.LogDebug("Today is the day {day}", day);
    }
    
    [Benchmark()]
    public void NewWay()
    {
      var day = 10;
      m_Logger.Debug($"Today is the day {day}");
    }
    
    [Benchmark()]
    public void NewWayNoParameters()
    {
      var day = 10;
      m_Logger.Debug("Today is the day 10");
    }
  }
}