﻿using BenchmarkDotNet.Attributes;
using NiceCore.Benchmarks.Model;
using NiceCore.Caches;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace NiceCore.Benchmarks
{
  public class ContainerBenchmarks
  {
    private static readonly ObjectToCreate m_Instance = new();
    private const string VALUE = "Hans Paul";
    private static readonly PropertyInfo m_Property = Array.Find(typeof(ObjectToCreate).GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance), r => r.Name == nameof(ObjectToCreate.Name));

    [Benchmark]
    public void SetValueReflection()
    {
      m_Property.SetValue(m_Instance, VALUE);
    }

    [Benchmark]
    public void SetValueCached()
    {
      var key = new PropertySetterCacheKey()
      {
        DeclaringType = typeof(ObjectToCreate),
        PropertyName = nameof(ObjectToCreate.Name),
        PropertyType = typeof(string)
      };
      var func = ExpressionCaches.PropertySetterCache.Get(key);
      func.Invoke(m_Instance, VALUE);
    }

    [Benchmark]
    public List<PropertyInfo> GetProperties()
    {
      return typeof(ObjectToCreate).GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance).ToList();
    }
  }
}
