﻿using AutoBogus;
using BenchmarkDotNet.Attributes;
using NiceCore.Benchmarks.Model;
using NiceCore.Filtering;
using System.Collections.Generic;
using System.Linq;

namespace NiceCore.Benchmarks
{
  public class FilterBenchmark
  {
    private readonly static List<FilterableObject> list = new AutoFaker<FilterableObject>()
      .Configure(builder => builder.WithLocale("de-DE"))
      .RuleFor(fake => fake.Name, fake => fake.Random.String())
      .RuleFor(fake => fake.Number, fake => fake.Random.Int())
      .RuleFor(fake => fake.Description, fake => fake.Random.String())
      .Generate(2000);

    private static readonly DefaultNcFilter subFilter = NcFilters.CreateAnd(
      NcFilterConditions.CreateGreater($"{nameof(FilterableObject.Name)}.{nameof(FilterableObject.Name.Length)}", 50),
      NcFilterConditions.CreateLess($"{nameof(FilterableObject.Name)}.{nameof(FilterableObject.Name.Length)}", 1000)
      );

    private static readonly DefaultNcFilter filter = NcFilters.CreateAnd(new List<DefaultNcFilter>() { subFilter },
      NcFilterConditions.CreateGreater(nameof(FilterableObject.Number), 100),
      NcFilterConditions.CreateLess($"{nameof(FilterableObject.Description)}.{nameof(FilterableObject.Description.Length)}", 10000)
      );

    [Benchmark]
    public List<FilterableObject> FilterByFilter()
    {
      var options = new NcFilterTranslationOptions()
      {
        AdditionalProviders = AdditionalFilterOperationProviders.Empty()
      };
      return list.FilterBy(filter, options).ToList();
    }

    [Benchmark]
    public List<FilterableObject> FilterByLINQ()
    {
      return list.Where(r => r.Number > 100 && r.Name.Length > 50 && r.Name.Length < 1000 && r.Description.Length < 10000).ToList();
    }
  }
}
