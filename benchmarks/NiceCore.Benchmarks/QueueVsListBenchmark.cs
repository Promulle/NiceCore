using System.Collections.Concurrent;
using System.Collections.Generic;
using BenchmarkDotNet.Attributes;

namespace NiceCore.Benchmarks
{
  [MemoryDiagnoser(false)]
  public class QueueVsListBenchmark
  {
    private static readonly Queue<int> m_Queue = new Queue<int>();
    private static readonly List<int> m_List = new List<int>();
    private static readonly ConcurrentQueue<int> m_Concurrent = new ConcurrentQueue<int>();

    [IterationSetup]
    public void Setup()
    {
      m_Queue.Enqueue(10);
      m_List.Add(10);
      m_Concurrent.Enqueue(10);
    }

    [IterationCleanup]
    public void Clean()
    {
      m_Queue.Clear();
      m_List.Clear();
      m_Concurrent.Clear();
    }

    [Benchmark]
    public void AddConcurrent()
    {
      m_Concurrent.Enqueue(10);
    }

    [Benchmark]
    public void AddList()
    {
      m_List.Add(10);
    }

    [Benchmark]
    public void AddQueue()
    {
      m_Queue.Enqueue(10);
    }

    [Benchmark]
    public int TryDequeueConcurrent()
    {
      if (m_Concurrent.TryDequeue(out var res))
      {
        return res;
      }
      return -1;
    }

    [Benchmark]
    public int DequeueList()
    {
      var res = m_List[0];
      m_List.RemoveAt(0);
      return res;
    }

    [Benchmark]
    public int DequeueQueue()
    {
      return m_Queue.Dequeue();
    }

    [Benchmark]
    public int TryDequeueList()
    {
      if (m_List.Count > 0)
      {
        var res = m_List[0];
        m_List.RemoveAt(0);
        return res;
      }

      return -1;
    }

    [Benchmark]
    public int TryDequeueQueue()
    {
      if (m_Queue.TryDequeue(out var res))
      {
        return res;
      }

      return -1;
    }
  }
}