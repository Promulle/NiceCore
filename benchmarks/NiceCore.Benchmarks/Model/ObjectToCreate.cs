﻿namespace NiceCore.Benchmarks.Model
{
  /// <summary>
  /// ObjectToCreate
  /// </summary>
  public class ObjectToCreate
  {
    public string Name { get; set; }
    public int Value { get; set; }

    public ObjectToCreate()
    {
    }

    public ObjectToCreate(string i_Name, int i_Value)
    {
      Name = i_Name;
      Value = i_Value;
    }
  }
}
