﻿using System;
using Microsoft.Extensions.Logging;

namespace NiceCore.Benchmarks.Model
{
  public class BenchmarkLogger : ILogger
  {
    private readonly LogLevel m_Level;
    public BenchmarkLogger(LogLevel i_Level)
    {
      m_Level = i_Level;
    }
    
    public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
    {
      try
      {

      }
      catch (Exception )
      {
        
      }
    }

    public bool IsEnabled(LogLevel logLevel)
    {
      return logLevel >= m_Level;
    }

    public IDisposable BeginScope<TState>(TState state)
      where TState : notnull
    {
      return null;
    }
  }
}