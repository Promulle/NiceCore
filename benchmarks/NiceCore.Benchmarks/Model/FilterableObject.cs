﻿namespace NiceCore.Benchmarks.Model
{
  public class FilterableObject
  {
    public string Name { get; set; }

    public int Number { get; set; }

    public string Description { get; set; }
  }
}
