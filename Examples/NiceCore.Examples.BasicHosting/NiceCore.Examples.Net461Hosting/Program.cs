﻿using NiceCore.Hosting.Services;
using NiceCore.ServiceLocation.Container;
using System;

namespace NiceCore.Examples.Net461Hosting
{
  class Program
  {
    static void Main(string[] args)
    {
      var container = new DefaultServiceContainer();
      container.Initialize();
      var hostService = container.Resolve<IHostService>();
      if (hostService != null)
      {
        hostService.Run();
      }
      else
      {
        Console.WriteLine("No hostservice found.");
      }
    }
  }
}
