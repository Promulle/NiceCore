﻿using NiceCore.Base.ServiceLocation;
using NiceCore.Hosting.Config;
using NiceCore.Services.Config;

namespace NiceCore.Examples.Net461Hosting
{
  /// <summary>
  /// Net461
  /// </summary>
  [Register(InterfaceType = typeof(IConfigBuilder), LifeStyle = LifeStyles.Singleton)]
  public class net461HostingConfigBuilder : BaseConfigBuilder
  {
    /// <summary>
    /// Default constructor
    /// </summary>
    public net461HostingConfigBuilder()
    {
      SectionTypes.Add(typeof(IHostingConfigSection));
    }
  }
}
