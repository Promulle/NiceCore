﻿using NiceCore.ServiceLocation;
using NiceCore.Storage.DomainBuilder;

namespace NiceCore.Examples.BasicHosting
{
  /// <summary>
  /// Domain-Builder
  /// </summary>
  [Register(InterfaceType = typeof(IDomainBuilder))]
  public class DomainBuilder : BaseDomainBuilder
  {
    /// <summary>
    /// Default constructor
    /// </summary>
    public DomainBuilder()
    {
      AllowSchemaUpdate = true;
      m_AllowedConversations.Add("test");
    }
  }
}
