﻿using NiceCore.Hosting.Services;
using NiceCore.Rapid;
using NiceCore.Rapid.Default;
using NiceCore.Rapid.Hosting;
using NiceCore.Services.Default.Scheduling;
using System;

namespace NiceCore.Examples.BasicHosting
{
  class Program
  {
    private static INcHostingApplication m_App;
    static void Main(string[] args)
    {
      m_App = NcApplicationBuilder.Create<NcHostingApplication>()
        .UseDefaults()
        .HostService().Inject<IHostService>()
        .ScheduleServiceHost.With<DefaultScheduleServiceHost>()
        .Build();
      if (m_App.HostService == null)
      {
        Console.WriteLine("No hostservice found.");
      }
      m_App.Run();
      m_App.Shutdown();
    }
  }
}
