﻿namespace NiceCore.Examples.BasicHosting.Objects
{
  /// <summary>
  /// Class for testing of post
  /// </summary>
  public class BasicDescription
  {
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// Value
    /// </summary>
    public int Value { get; set; }
  }
}
