﻿namespace NiceCore.Examples.BasicHosting
{
  /// <summary>
  /// Dto
  /// </summary>
  public class TestResourceDTO
  {
    /// <summary>
    /// Name of resource
    /// </summary>
    public string Name { get; set; }
  }
}
