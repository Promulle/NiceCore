﻿using NiceCore.Base.Services;
using NiceCore.Examples.BasicHosting.Objects;
using NiceCore.Hosting;
using NiceCore.Hosting.Services;
using NiceCore.ServiceLocation;
using System;

namespace NiceCore.Examples.BasicHosting.Services
{
  /// <summary>
  /// Service für Put-Requests
  /// </summary>
  [HostedService(Endpoint = "/put")]
  [Register(InterfaceType = typeof(IHostableService))]
  public class PutTestService : BaseService, IHostableService
  {
    /// <summary>
    /// Put Test
    /// </summary>
    /// <param name="i_Description"></param>
    [HostedServiceOperation(ContentType = ContentTypes.APP_JSON, Endpoint = "/test", Method = HttpMethods.PUT)]
    [OperationParameter(MethodParameterName = "i_Description", ParameterType = ParameterTypes.RequestBody)]
    public void UseObject(BasicDescription i_Description)
    {
      Console.WriteLine(i_Description.Name);
      Console.WriteLine(i_Description.Value);
    }
  }
}
