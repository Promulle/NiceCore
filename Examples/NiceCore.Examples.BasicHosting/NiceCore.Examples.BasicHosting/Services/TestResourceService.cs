﻿using NiceCore.Hosting.Resources;
using NiceCore.Hosting.Services;
using NiceCore.ServiceLocation;

namespace NiceCore.Examples.BasicHosting.Services
{
  /// <summary>
  /// Test for resource service
  /// </summary>
  [InitializeOnResolve]
  [Register(InterfaceType = typeof(IHostableService))]
  public class TestResourceService : BasePersistenceResourceService<TestResource, long, TestResourceDTO>
  {

    /// <summary>
    /// Initialize
    /// </summary>
    protected override void InternalInitialize()
    {
      base.InternalInitialize();
      ObjectMappingService.Initialize();
    }


    /// <summary>
    /// Return test
    /// </summary>
    /// <returns></returns>
    protected override string GetConnectionName()
    {
      return "test";
    }
  }
}
