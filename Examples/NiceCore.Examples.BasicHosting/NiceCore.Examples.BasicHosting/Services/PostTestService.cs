﻿using NiceCore.Base.Services;
using NiceCore.Examples.BasicHosting.Objects;
using NiceCore.Hosting;
using NiceCore.Hosting.Services;
using NiceCore.ServiceLocation;
using System.IO;

namespace NiceCore.Examples.BasicHosting.Services
{
  /// <summary>
  /// TestService for Post-requests
  /// </summary>
  [HostedService(Endpoint = "/post")]
  [Register(InterfaceType = typeof(IPostTestService))]
  [Register(InterfaceType = typeof(IHostableService))]
  public class PostTestService : BaseService, IPostTestService
  {
    /// <summary>
    /// Writes received element to file
    /// </summary>
    /// <param name="i_Description"></param>
    [HostedServiceOperation(ContentType = ContentTypes.APP_JSON, Endpoint = "/test", Method = HttpMethods.POST)]
    [OperationParameter(MethodParameterName = "i_Description", ParameterType = ParameterTypes.RequestBody)]
    public void WriteToFile(BasicDescription i_Description)
    {
      if (i_Description != null)
      {
        if (File.Exists("test.txt"))
        {
          File.Delete("test.txt");
        }
        File.WriteAllText("test.txt", i_Description.Name + "_" + i_Description.Value);
      }
    }
  }
}
