﻿using NiceCore.Base.Services;
using NiceCore.ServiceLocation;
using NiceCore.Services.Scheduling;
using System;

namespace NiceCore.Examples.BasicHosting.Services
{
  /// <summary>
  /// Test for scheduled service
  /// </summary>
  [Register(InterfaceType = typeof(IScheduledService))]
  public class ScheduledService : BaseService, IScheduledService
  {
    /// <summary>
    /// Return all 30 secs
    /// </summary>
    /// <returns></returns>
    public IScheduleEntry CreateScheduleEntry()
    {
      return new DefaultIncrementalScheduleEntry()
      {
        ExecuteOnScheduled = true,
        Schedulable = this,
        SpanBetweenExecutions = TimeSpan.FromSeconds(30),
      };
    }
    /// <summary>
    /// Execute
    /// </summary>
    /// <returns></returns>
    public bool Execute()
    {
      Console.WriteLine("Hello, " + DateTime.Now.ToShortTimeString());
      return true;
    }
  }
}
