﻿using NiceCore.Examples.BasicHosting.Objects;
using NiceCore.Hosting.Services;

namespace NiceCore.Examples.BasicHosting.Services
{
  /// <summary>
  /// Service for testing post requests
  /// </summary>
  public interface IPostTestService : IHostableService
  {
    /// <summary>
    /// Writes to file
    /// </summary>
    /// <param name="i_Description"></param>
    void WriteToFile(BasicDescription i_Description);
  }
}