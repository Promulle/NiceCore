﻿using NiceCore.Base.Services;
using NiceCore.Examples.BasicHosting.Objects;
using NiceCore.Hosting;
using NiceCore.Hosting.Services;
using NiceCore.ServiceLocation;
using System;

namespace NiceCore.Examples.BasicHosting.Services
{
  /// <summary>
  /// Test service for json via get
  /// </summary>
  [Register(InterfaceType = typeof(IHostableService))]
  [HostedService(Endpoint = "/get")]
  public class GetJsonTestService : BaseService, IGetJsonTestService
  {
    /// <summary>
    /// Returns new object in json
    /// </summary>
    /// <param name="i_ID"></param>
    /// <returns></returns>
    [HostedServiceOperation(ContentType = ContentTypes.APP_JSON, Endpoint = "/test/{id}", Method = HttpMethods.GET)]
    [OperationParameter(MethodParameterName = "i_ID", ParameterName = "id", ParameterType = ParameterTypes.Url)]
    public BasicDescription GetTestObject(string i_ID)
    {
      var res = new BasicDescription()
      {
        Name = "New",
        Value = Convert.ToInt32(i_ID),
      };
      return res;
    }
  }
}
