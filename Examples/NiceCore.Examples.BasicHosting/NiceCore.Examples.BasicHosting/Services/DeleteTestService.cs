﻿using NiceCore.Base.Services;
using NiceCore.Hosting;
using NiceCore.Hosting.Services;
using NiceCore.ServiceLocation;
using System;

namespace NiceCore.Examples.BasicHosting.Services
{
  /// <summary>
  /// TestService for delete-requests
  /// </summary>
  [Register(InterfaceType = typeof(IDeleteTestService))]
  [Register(InterfaceType = typeof(IHostableService))]
  [HostedService(Endpoint = "/delete")]
  public class DeleteTestService : BaseService, IDeleteTestService
  {
    /// <summary>
    /// Testfunctions for delete method
    /// </summary>
    /// <param name="i_ID"></param>
    [HostedServiceOperation(ContentType = ContentTypes.TEXT_HTML, Endpoint = "/{id}", Method = HttpMethods.DELETE)]
    [OperationParameter(MethodParameterName = "i_ID", ParameterName = "id", ParameterType = ParameterTypes.Url)]
    public void Print(string i_ID) //URL parameters will be interpreted as string
    {
      Console.WriteLine("ID: " + i_ID);
    }
  }
}
