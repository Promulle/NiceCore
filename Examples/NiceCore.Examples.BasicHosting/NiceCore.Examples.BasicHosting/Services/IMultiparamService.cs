﻿using NiceCore.Hosting.Services;

namespace NiceCore.Examples.BasicHosting.Services
{
  /// <summary>
  /// Interface for multi service
  /// </summary>
  public interface IMultiparamService : IHostableService
  {
    /// <summary>
    /// Receive method
    /// </summary>
    /// <param name="i_One"></param>
    /// <param name="i_ID"></param>
    void Receive(string i_One, string i_ID);
  }
}