﻿using NiceCore.Hosting.Services;

namespace NiceCore.Examples.BasicHosting.Services
{
  /// <summary>
  /// Service for testing delete requests
  /// </summary>
  public interface IDeleteTestService : IHostableService
  {
    /// <summary>
    /// test
    /// </summary>
    /// <param name="i_ID"></param>
    void Print(string i_ID);
  }
}