﻿using NiceCore.Examples.BasicHosting.Objects;
using NiceCore.Hosting.Services;

namespace NiceCore.Examples.BasicHosting.Services
{
  /// <summary>
  /// Test service for json via get
  /// </summary>
  public interface IGetJsonTestService : IHostableService
  {
    /// <summary>
    /// Returns new object in json
    /// </summary>
    /// <param name="i_ID"></param>
    /// <returns></returns>
    BasicDescription GetTestObject(string i_ID);
  }
}