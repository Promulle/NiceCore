﻿using NiceCore.Base.Services;
using NiceCore.Hosting;
using NiceCore.Hosting.Services;
using NiceCore.ServiceLocation;
using System;

namespace NiceCore.Examples.BasicHosting.Services
{
  /// <summary>
  /// Service for testing multiple parameters
  /// </summary>
  [Register(InterfaceType = typeof(IHostableService))]
  [HostedService(Endpoint = "/multi")]
  public class MultiparamService : BaseService, IMultiparamService
  {
    /// <summary>
    /// Receive taking multiple parameters
    /// </summary>
    [HostedServiceOperation(ContentType = ContentTypes.TEXT_HTML, Endpoint = "/p1/{id}", Method = HttpMethods.GET)]
    [OperationParameter(MethodParameterName = "i_ID", ParameterName = "id", ParameterType = ParameterTypes.Url)]
    public void Receive(string i_One, string i_ID)
    {
      if (i_One == null)
      {
        i_One = "Null";
      }
      Console.WriteLine(i_One + "");
      Console.WriteLine(i_ID + "");
    }
  }
}
