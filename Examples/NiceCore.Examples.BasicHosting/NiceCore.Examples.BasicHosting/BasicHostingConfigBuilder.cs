﻿using NiceCore.Hosting.Config.FileProvider;
using NiceCore.Hosting.Config.Server;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using NiceCore.Storage;

namespace NiceCore.Examples.BasicHosting
{
  /// <summary>
  /// ConfigBuilder for basic hosting example
  /// </summary>
  [Register(InterfaceType = typeof(IXmlConfigBuilder), LifeStyle = LifeStyles.Singleton)]
  public class BasicHostingConfigBuilder : BaseXmlConfigBuilder
  {
    /// <summary>
    /// Default constructor
    /// </summary>
    public BasicHostingConfigBuilder()
    {
      SectionTypes.Add(typeof(IFileProviderConfigSection));
      SectionTypes.Add(typeof(IServerConfigSection));
      SectionTypes.Add(typeof(IStorageConfigSection));
    }
  }
}
