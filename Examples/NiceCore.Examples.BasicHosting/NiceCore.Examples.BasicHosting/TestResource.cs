﻿using NiceCore.Entities;

namespace NiceCore.Examples.BasicHosting
{
  /// <summary>
  /// TestResource
  /// </summary>
  public class TestResource : BaseDomainEntity
  {
    /// <summary>
    /// Name
    /// </summary>
    public virtual string Name { get; set; }
  }
}
