﻿using NiceCore.Eventing;
using NiceCore.Eventing.Messaging;
using NiceCore.Examples.RabbitMQ.Lib;
using NiceCore.Rapid;
using NiceCore.ServiceLocation.Discovery;
using NiceCore.ServiceLocation.Plugins;
using Serilog;
using Serilog.Core;
using System;
using System.Threading.Tasks;
using NiceCore.Eventing.Messaging.Extensions;
using NiceCore.Eventing.Messaging.Impl;
using NiceCore.ServiceLocation.CastleWindsor;
using NiceCore.Services.Config;
using NiceCore.Services.Config.Impl.AssemblyBased;
using NiceCore.Services.Default.Config;

namespace NiceCore.Examples.Eventing.RabbitMQ.Sender
{
  class Program
  {
    static async Task Main(string[] args)
    {
      Console.WriteLine("Enter rabbitmq host name:");
      var hostName = Console.ReadLine();
      var app = await NcApplicationBuilder.Create()
        .Discovery.With<LegacyRegistrationDiscovery>()
        .Container.With<CastleWindsorServiceContainer>()
        .Container.DependsOn(r => (r as CastleWindsorServiceContainer).IsStandalone = false)
        .Container.DependsOn(r => r.Plugins.Add(new InitializePlugin()))
        .Container.DependsOn(r => r.Plugins.Add(new ReleaseOnDisposePlugin()))
        .Configuration.With<DefaultNcConfiguration>()
        .ConfigurationSources.Add<EntryAssemblyJSONNcConfigurationSource>()
        .EventingAdapter.Inject<IEventingAdapter>()
        .UsedEventingHandlers.AddInjected<IEventingHandler>()
        .Build();

      await app.Run();
      var messageService = app.ServiceContainer.Resolve<IMessageService>();
      messageService.Register<RabbitTestMessage, TestObject>(ObjectConsumer.DisplayObject).Register();
      while (true)
      {
        Console.WriteLine("Press any key to send object.");
        Console.ReadLine();
        await messageService.SendMessage(RabbitTestMessage.Create(CreateObjectToBeSent()), null, true);
      }

      Console.WriteLine("Press any key to exit.");
      Console.ReadLine();
      app.Shutdown();
    }

    private static TestObject CreateObjectToBeSent()
    {
      return new TestObject()
      {
        Name = "No Internet.",
        Number = 3,
      };
    }

    private static Logger CreateLogger()
    {
      return new LoggerConfiguration()
        .WriteTo.Console()
        .MinimumLevel.Debug()
        .CreateLogger();
    }
  }
}