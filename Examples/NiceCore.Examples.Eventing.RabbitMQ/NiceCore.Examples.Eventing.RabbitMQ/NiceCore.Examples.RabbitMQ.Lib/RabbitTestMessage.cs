﻿using NiceCore.Eventing.Messages;

namespace NiceCore.Examples.RabbitMQ.Lib
{
  public class RabbitTestMessage : BaseMessage<TestObject>
  {
    public static readonly string TOPIC = "TEST-OBJECT";
    public override string MessageTopic { get; } = TOPIC;

    public static RabbitTestMessage Create(TestObject i_Value)
    {
      return new RabbitTestMessage()
      {
        Value = new MessageValue<TestObject>()
        {
          Value = i_Value
        }
      };
    }
  }
}
