﻿using System;

namespace NiceCore.Examples.RabbitMQ.Lib
{
  public static class ObjectConsumer
  {
    public static void DisplayObject(RabbitTestMessage i_Message)
    {
      Console.WriteLine($"Object: Name: {i_Message.Value.Value.Name}, Number: {i_Message.Value.Value.Number}, Identifier: {i_Message.Value.Identifier}");
    }
  }
}
