﻿using ProtoBuf;

namespace NiceCore.Examples.RabbitMQ.Lib
{
  [ProtoContract]
  public class TestObject
  {
    [ProtoMember(1)]
    public string Name { get; set; }

    [ProtoMember(2)]
    public int Number { get; set; }
  }
}
