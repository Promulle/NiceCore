﻿using NiceCore.Eventing;
using NiceCore.Eventing.Messaging;
using NiceCore.Examples.RabbitMQ.Lib;
using NiceCore.ServiceLocation.Discovery;
using NiceCore.ServiceLocation.Plugins;
using RabbitMQ.Client;
using Serilog;
using Serilog.Core;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NiceCore.Eventing.Messaging.Extensions;
using NiceCore.Eventing.Messaging.Impl;
using NiceCore.Eventing.RabbitMQ;
using NiceCore.Rapid;
using NiceCore.ServiceLocation.CastleWindsor;
using NiceCore.Services.Config.Impl.AssemblyBased;
using NiceCore.Services.Default.Config;

namespace NiceCore.Examples.RabbitMQ.Receiver
{
  class Program
  {
    private static readonly Dictionary<Guid, int> dict = new();
    static async Task Main(string[] args)
    {
      Console.WriteLine("Enter rabbitmq host name:");
      var hostName = Console.ReadLine();
      var app = await NcApplicationBuilder.Create()
        .Discovery.With<LegacyRegistrationDiscovery>()
        .Container.With<CastleWindsorServiceContainer>()
        .Container.DependsOn(r => (r as CastleWindsorServiceContainer).IsStandalone = false)
        .Container.DependsOn(r => r.Plugins.Add(new InitializePlugin()))
        .Container.DependsOn(r => r.Plugins.Add(new ReleaseOnDisposePlugin()))
        .Configuration.With<DefaultNcConfiguration>()
        .ConfigurationSources.Add<EntryAssemblyJSONNcConfigurationSource>()
        .EventingAdapter.Inject<IEventingAdapter>()
        .UsedEventingHandlers.AddInjected<IEventingHandler>()
        .Build();
      var msgService = app.ServiceContainer.Resolve<IMessageService>();
      msgService.Register<RabbitTestMessage, TestObject>(ObjectConsumer.DisplayObject).Register();
      msgService.Register<RabbitTestMessage, TestObject>(SecondFunction).Register();
      Console.WriteLine("Waiting for messages to be received...");
      Console.ReadLine();
    }

    private static void SecondFunction(RabbitTestMessage i_Message)
    {
      Console.WriteLine("Second Function: Message " + i_Message.Value.Identifier + " from " + i_Message.Value.ClientIdentifier);
    }
  }
}
