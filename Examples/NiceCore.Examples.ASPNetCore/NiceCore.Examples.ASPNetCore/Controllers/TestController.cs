﻿using Microsoft.AspNetCore.Mvc;
using NiceCore.Examples.ASPNetCore.Model;
using NiceCore.Examples.ASPNetCore.Services;
using NiceCore.ServiceLocation;

namespace NiceCore.Examples.ASPNetCore.Controllers
{
  [ApiController]
  [Route("api/[controller]")]
  [Register(InterfaceType = typeof(TestController))]
  public class TestController : ControllerBase
  {
    public IObjectService ObjectService { get; set; }

    [HttpGet("create")]
    public ActionResult<MyObject> Create()
    {
      return new ActionResult<MyObject>(ObjectService.CreateNewObject());
    }
  }
}
