﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NiceCore.Examples.ASPNetCore.Model;
using NiceCore.ServiceLocation;
using System;

namespace NiceCore.Examples.ASPNetCore.Controllers
{
  [ApiController]
  [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = Permissions.SecuredObject.READ)]
  [Route("api/[controller]")]
  [Register(InterfaceType = typeof(SecuredController))]
  public class SecuredController : ControllerBase
  {
    [HttpGet("get")]
    public SecuredObject Get()
    {
      return new SecuredObject()
      {
        Description = Guid.NewGuid().ToString(),
        Name = Guid.NewGuid().ToString()
      };
    }
  }
}
