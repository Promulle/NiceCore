﻿using Microsoft.AspNetCore.Mvc;
using NiceCore.Examples.ASPNetCore.Model;
using NiceCore.Hosting.Services;
using NiceCore.ServiceLocation;

namespace NiceCore.Examples.ASPNetCore.Controllers
{
  [ApiController]
  [Route("api/[controller]")]
  [Register(InterfaceType = typeof(AuthController))]
  public class AuthController : ControllerBase
  {
    public INcJWTTokenService TokenService { get; set; }

    [HttpPost("user")]
    public string AuthUser(UserCredentialDTO i_UserData)
    {
      return TokenService.GenerateJWTToken(i_UserData.UserName);
    }
  }
}
