﻿using NiceCore.Examples.ASPNetCore.Model;

namespace NiceCore.Examples.ASPNetCore.Services
{
  public interface IObjectService
  {
    public MyObject CreateNewObject();
  }
}
