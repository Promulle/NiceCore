﻿using NiceCore.Examples.ASPNetCore.Model;
using NiceCore.ServiceLocation;
using System;

namespace NiceCore.Examples.ASPNetCore.Services.Impl
{
  [Register(InterfaceType = typeof(IObjectService))]
  public class DefaultObjectService : IObjectService
  {
    private readonly Random m_Random = new();
    public MyObject CreateNewObject()
    {
      return new MyObject()
      {
        Name = Guid.NewGuid().ToString(),
        Value = m_Random.Next()
      };
    }
  }
}
