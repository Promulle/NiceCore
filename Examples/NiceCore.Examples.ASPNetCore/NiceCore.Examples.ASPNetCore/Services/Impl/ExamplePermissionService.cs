﻿using NiceCore.Permissions;
using NiceCore.Permissions.Impl;
using NiceCore.ServiceLocation;
using NiceCore.Services.Permissions;
using System.Threading.Tasks;

namespace NiceCore.Examples.ASPNetCore.Services.Impl
{
  [Register(InterfaceType = typeof(IPermissionService))]
  public class ExamplePermissionService : IPermissionService
  {
    public Task<bool> IsAllowed(object i_UserId, IPermissionKey i_PermissionKey)
    {
      if (i_PermissionKey is SimplePermissionKey simpleKey)
      {
        var userId = i_UserId.ToString();
        var permissions = Users.GetPermitted(userId);
        return Task.FromResult(permissions.Contains(simpleKey.PermissionKeyName));
      }
      return Task.FromResult(false);
    }
  }
}
