using NiceCore.Hosting.Services;
using NiceCore.Rapid;
using NiceCore.Rapid.Default;
using NiceCore.Rapid.Hosting;
using NiceCore.ServiceLocation.CastleWindsor;
using System.Threading.Tasks;

namespace NiceCore.Examples.ASPNetCore
{
  /// <summary>
  /// Program
  /// </summary>
  public static class Program
  {
    public static async Task Main()
    {
      var app = NcApplicationBuilder.Create<NcHostingApplication>()
        .UseDefaults()
        .Container.With(new CastleWindsorServiceContainer())
        .Container.DependsOn(r => (r as CastleWindsorServiceContainer).IsStandalone = false)
        .HostService().Inject<IHostService>()
        .Build();

      await app.Run().ConfigureAwait(false);
      await app.Shutdown().ConfigureAwait(false);
    }
  }
}
