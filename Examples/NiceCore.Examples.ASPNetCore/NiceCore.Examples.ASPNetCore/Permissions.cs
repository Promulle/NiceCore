﻿namespace NiceCore.Examples.ASPNetCore
{
  public static class Permissions
  {
    public static class SecuredObject
    {
      public const string READ = "SecuredObject.Read";
    }
  }
}
