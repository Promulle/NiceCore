﻿namespace NiceCore.Examples.ASPNetCore.Model
{
  public class MyObject
  {
    public string Name { get; set; }
    public int Value { get; set; }
  }
}
