﻿namespace NiceCore.Examples.ASPNetCore.Model
{
  public class SecuredObject
  {
    public string Name { get; set; }
    public string Description { get; set; }
  }
}
