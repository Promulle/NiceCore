﻿using System.Collections.Generic;

namespace NiceCore.Examples.ASPNetCore
{
  public static class Users
  {
    public static readonly string ADMIN = "AdminId";
    public static readonly string USER = "UserId";

    private static readonly Dictionary<string, List<string>> m_UserPermissions = CreatePermitted();
    public static List<string> GetPermitted(string i_UserId)
    {
      if (m_UserPermissions.TryGetValue(i_UserId, out var list))
      {
        return list;
      }
      return new List<string>();
    }

    private static Dictionary<string, List<string>> CreatePermitted()
    {
      return new Dictionary<string, List<string>>()
      {
        {ADMIN, new List<string>() { Permissions.SecuredObject.READ } },
        {USER, new List<string>() }
      };
    }
  }
}
