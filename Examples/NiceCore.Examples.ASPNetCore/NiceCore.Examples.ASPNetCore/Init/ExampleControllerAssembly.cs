﻿using NiceCore.Hosting;
using NiceCore.ServiceLocation;

namespace NiceCore.Examples.ASPNetCore.Init
{
  /// <summary>
  /// controller Assembly
  /// </summary>
  [Register(InterfaceType = typeof(IControllerAssembly))]
  public class ExampleControllerAssembly : IControllerAssembly
  {
  }
}
