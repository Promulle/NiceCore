﻿using NiceCore.UI.Base;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NiceCore.Examples.VersionTool.Domain
{
  /// <summary>
  /// info describing a version
  /// </summary>
  public class VersionInfoDto : BaseNotifyObject
  {
    private const string VERSION_SEPARATOR = ".";
    private string m_Major;
    private string m_Minor;
    private string m_Revision;
    private string m_Build;
    /// <summary>
    /// Majornumber
    /// </summary>
    public string Major
    {
      get { return m_Major; }
      set { SetField(ref m_Major, value); }
    }
    /// <summary>
    /// Property for displaying the version as proper string
    /// </summary>
    public string Display
    {
      get { return ToString(); }
    }
    /// <summary>
    /// Minornumber
    /// </summary>
    public string Minor
    {
      get { return m_Minor; }
      set { SetField(ref m_Minor, value); }
    }
    /// <summary>
    /// RevisionNumber
    /// </summary>
    public string Revision
    {
      get { return m_Revision; }
      set { SetField(ref m_Revision, value); }
    }
    /// <summary>
    /// BuildNumber
    /// </summary>
    public string Build
    {
      get { return m_Build; }
      set { SetField(ref m_Build, value); }
    }
    /// <summary>
    /// Override returning correct format
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
      StringBuilder builder = new StringBuilder();
      builder.Append(Major);
      builder.Append(VERSION_SEPARATOR);
      builder.Append(Minor);
      builder.Append(VERSION_SEPARATOR);
      builder.Append(Revision);
      builder.Append(VERSION_SEPARATOR);
      builder.Append(Build);
      return builder.ToString();
    }
    /// <summary>
    /// Parses value to version info
    /// </summary>
    /// <param name="i_Value"></param>
    public void Parse(string i_Value)
    {
      List<string> parts = i_Value.Split(new char[] { VERSION_SEPARATOR[0] }, 4, System.StringSplitOptions.RemoveEmptyEntries).ToList();
      if (parts.Count == 4)
      {
        Major = parts[0];
        Minor = parts[1];
        Revision = parts[2];
        Build = parts[3];
      }
    }
  }
}
