﻿using NiceCore.Base.Services;
using System.Collections.Generic;

namespace NiceCore.Examples.VersionTool.Domain
{
  /// <summary>
  /// Interface for VersionService
  /// </summary>
  public interface IVersionService : IService
  {
    /// <summary>
    /// Reads solution file for projects and then parses info about every project found
    /// </summary>
    /// <param name="i_SolutionFile"></param>
    /// <returns></returns>
    List<ProjectInfoDto> GetProjectInfo(string i_SolutionFile);
    /// <summary>
    /// Parses info for one ProjectFile
    /// </summary>
    /// <param name="i_ProjectFile"></param>
    /// <returns></returns>
    ProjectInfoDto ParseProjectInfo(string i_ProjectFile);
    /// <summary>
    /// Increments build number on all projects in i_SolutionPath
    /// </summary>
    /// <param name="i_SolutionPath"></param>
    void IncrementBuildVersion(string i_SolutionPath);
    /// <summary>
    /// Increments build number on i_ProjectFilePath
    /// </summary>
    /// <param name="i_ProjectFilePath"></param>
    void IncrementProjectBuildVersion(string i_ProjectFilePath);
    /// <summary>
    /// Updates version of all projects in solution
    /// </summary>
    /// <param name="i_SolutionPath"></param>
    /// <param name="i_NewVersion"></param>
    void UpdateVersion(string i_SolutionPath, VersionInfoDto i_NewVersion);
    /// <summary>
    /// Updates Version of a single project
    /// </summary>
    /// <param name="i_ProjectFilePath"></param>
    /// <param name="i_NewVersion"></param>
    void UpdateProjectVersion(string i_ProjectFilePath, VersionInfoDto i_NewVersion);
  }
}