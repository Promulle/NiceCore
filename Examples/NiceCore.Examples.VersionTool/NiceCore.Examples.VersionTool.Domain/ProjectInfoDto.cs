﻿using NiceCore.UI.Base;

namespace NiceCore.Examples.VersionTool.Domain
{
  /// <summary>
  /// Dto for project info
  /// </summary>
  public class ProjectInfoDto : BaseNotifyObject
  {
    private string m_Name;
    private string m_FilePath;
    private VersionInfoDto m_FileVersion;
    private VersionInfoDto m_AssemblyVersion;
    /// <summary>
    /// Name of Project
    /// </summary>
    public string Name
    {
      get { return m_Name; }
      set { SetField(ref m_Name, value); }
    }
    /// <summary>
    /// Filepath to csproj file
    /// </summary>
    public string FilePath
    {
      get { return m_FilePath; }
      set { SetField(ref m_FilePath, value); }
    }
    /// <summary>
    /// FileVersion of Project
    /// </summary>
    public VersionInfoDto FileVersion
    {
      get { return m_FileVersion; }
      set { SetField(ref m_FileVersion, value); }
    }
    /// <summary>
    /// AssemblyVersion of Project
    /// </summary>
    public VersionInfoDto AssemblyVersion
    {
      get { return m_AssemblyVersion; }
      set { SetField(ref m_AssemblyVersion, value); }
    }
  }
}
