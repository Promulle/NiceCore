﻿using NiceCore.Base.Services;
using NiceCore.ServiceLocation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace NiceCore.Examples.VersionTool.Domain
{
  /// <summary>
  /// Service for changing/loading versions from csproj files
  /// </summary>
  [Register(InterfaceType = typeof(IVersionService))]
  public class VersionService : BaseService, IVersionService
  {
    private const string KEY_FILEVERSION = "FileVersion";
    private const string KEY_ASSEMBLYVERSION = "AssemblyVersion";
    private const string KEY_PROPERTY_GROUP = "PropertyGroup";
    private const string KEY_SOLUTION_PROJECT = "Project";
    /// <summary>
    /// Updates version of all projects in solution
    /// </summary>
    /// <param name="i_SolutionPath"></param>
    /// <param name="i_NewVersion"></param>
    public void UpdateVersion(string i_SolutionPath, VersionInfoDto i_NewVersion)
    {
      if (File.Exists(i_SolutionPath))
      {
        var projects = GetProjectPaths(i_SolutionPath);
        foreach (var proj in projects)
        {
          UpdateProjectVersion(proj, i_NewVersion);
        }
      }
    }
    /// <summary>
    /// Updates Version of a single project
    /// </summary>
    /// <param name="i_ProjectFilePath"></param>
    /// <param name="i_NewVersion"></param>
    public void UpdateProjectVersion(string i_ProjectFilePath, VersionInfoDto i_NewVersion)
    {
      if (File.Exists(i_ProjectFilePath))
      {
        var xdoc = new XmlDocument();
        xdoc.Load(i_ProjectFilePath);
        var propGroup = xdoc.ChildNodes[0].ChildNodes[0];
        XmlNode assemblyVersionNode = null;
        XmlNode fileVersionNode = null;
        foreach (XmlNode node in propGroup.ChildNodes)
        {
          if (node.LocalName == KEY_ASSEMBLYVERSION)
          {
            assemblyVersionNode = node;
          }
          else if (node.LocalName == KEY_FILEVERSION)
          {
            fileVersionNode = node;
          }
        }
        if (fileVersionNode != null)
        {
          fileVersionNode.InnerText = i_NewVersion.ToString();
        }
        else
        {
          fileVersionNode = xdoc.CreateElement(KEY_FILEVERSION);
          fileVersionNode.InnerText = i_NewVersion.ToString();
          propGroup.AppendChild(fileVersionNode);
        }
        if (assemblyVersionNode != null)
        {
          assemblyVersionNode.InnerText = i_NewVersion.ToString();
        }
        else
        {
          assemblyVersionNode = xdoc.CreateElement(KEY_ASSEMBLYVERSION);
          assemblyVersionNode.InnerText = i_NewVersion.ToString();
          propGroup.AppendChild(assemblyVersionNode);
        }
        xdoc.Save(i_ProjectFilePath);
      }
    }
    /// <summary>
    /// Returns list of info about projects in i_SolutionFile
    /// </summary>
    /// <param name="i_SolutionFile"></param>
    /// <returns></returns>
    public List<ProjectInfoDto> GetProjectInfo(string i_SolutionFile)
    {
      if (File.Exists(i_SolutionFile))
      {
        var res = new List<ProjectInfoDto>();
        var projects = GetProjectPaths(i_SolutionFile);
        foreach (var proj in projects)
        {
          res.Add(ParseProjectInfo(proj));
        }
        return res;
      }
      return null;
    }
    private List<string> GetProjectPaths(string i_SolutionFile)
    {
      var res = new List<string>();
      var solPath = Path.GetDirectoryName(i_SolutionFile);
      foreach (var line in File.ReadAllLines(i_SolutionFile))
      {
        if (line.StartsWith(KEY_SOLUTION_PROJECT))
        {
          var split = line.Split(new char[] { ',' }, 3, System.StringSplitOptions.RemoveEmptyEntries);
          if (split.Length == 3)
          {
            var relPath = split[1].Replace("\"", "").Trim();
            res.Add(solPath + Path.DirectorySeparatorChar + relPath);
          }
        }
      }
      return res;
    }
    /// <summary>
    /// Parses a single projectfile for Information
    /// </summary>
    /// <param name="i_ProjectFile"></param>
    /// <returns></returns>
    public ProjectInfoDto ParseProjectInfo(string i_ProjectFile)
    {
      if (File.Exists(i_ProjectFile))
      {
        var res = new ProjectInfoDto();
        res.FilePath = Directory.GetParent(i_ProjectFile).Name + Path.DirectorySeparatorChar + Path.GetFileName(i_ProjectFile);
        res.Name = Path.GetFileNameWithoutExtension(res.FilePath);
        var xdoc = new XmlDocument();
        xdoc.LoadXml(File.ReadAllText(i_ProjectFile));
        bool bFound = false;
        for (int i = 0; i < xdoc.ChildNodes[0].ChildNodes.Count && !bFound; i++)
        {
          var node = xdoc.ChildNodes[0].ChildNodes[i];
          if (node.LocalName == KEY_PROPERTY_GROUP)
          {
            foreach (XmlNode subnode in node.ChildNodes)
            {
              if (subnode.LocalName == KEY_FILEVERSION)
              {
                res.FileVersion = new VersionInfoDto();
                res.FileVersion.Parse(subnode.InnerText);
              }
              else if (subnode.LocalName == KEY_ASSEMBLYVERSION)
              {
                res.AssemblyVersion = new VersionInfoDto();
                res.AssemblyVersion.Parse(subnode.InnerText);
              }
            }
          }
          bFound = res.FileVersion != null && res.AssemblyVersion != null;
        }
        return res;
      }
      return null;
    }
    /// <summary>
    /// Increments build number on all projects in i_SolutionPath
    /// </summary>
    /// <param name="i_SolutionPath"></param>
    public void IncrementBuildVersion(string i_SolutionPath)
    {
      var projects = GetProjectInfo(i_SolutionPath);
      foreach (var proj in projects)
      {
        IncrementProjectBuildVersion(proj.FilePath);
      }
    }
    /// <summary>
    /// Increments build number on i_ProjectFilePath
    /// </summary>
    /// <param name="i_ProjectFilePath"></param>
    public void IncrementProjectBuildVersion(string i_ProjectFilePath)
    {
      IncrementProjectBuildVersion(ParseProjectInfo(i_ProjectFilePath));
    }
    /// <summary>
    /// Increments build number on i_ProjectFilePath
    /// </summary>
    /// <param name="i_Project"></param>
    private void IncrementProjectBuildVersion(ProjectInfoDto i_Project)
    {
      i_Project.AssemblyVersion.Build = Convert.ToString(Convert.ToInt32(i_Project.AssemblyVersion.Build) + 1);
      i_Project.FileVersion.Build = Convert.ToString(Convert.ToInt32(i_Project.FileVersion.Build) + 1);
      UpdateProjectVersion(i_Project.FilePath, i_Project.AssemblyVersion);
    }
  }
}
