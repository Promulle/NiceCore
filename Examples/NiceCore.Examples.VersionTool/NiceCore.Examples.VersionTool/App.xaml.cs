﻿using Avalonia;
using Avalonia.Markup.Xaml;
using NiceCore.Rapid;
using NiceCore.Rapid.Default;

namespace NiceCore.Examples.VersionTool
{
  /// <summary>
  /// App class
  /// </summary>
  public class App : Application
  {
    /// <summary>
    /// Initializes app
    /// </summary>
    public override void Initialize()
    {
      AvaloniaXamlLoader.Load(this);
      
    }
  }
}
