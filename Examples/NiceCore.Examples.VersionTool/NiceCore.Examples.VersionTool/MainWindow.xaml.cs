﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using NiceCore.Examples.VersionTool.MainDialog;
using NiceCore.Rapid;
using NiceCore.Rapid.Application;
using NiceCore.Rapid.Default;

namespace NiceCore.Examples.VersionTool
{
  /// <summary>
  /// mainwindow
  /// </summary>
  public class MainWindow : Window
  {
    private IMainPresenter m_MainPresenter;
    private INcApplication m_App;
    /// <summary>
    /// Default constrcutor
    /// </summary>
    public MainWindow()
    {
      InitializeComponent();
#if DEBUG
      this.AttachDevTools();
#endif
      m_App = NcApplicationBuilder.Create()
        .UseDefaults()
        .Build();
      m_MainPresenter = m_App.ServiceContainer.Resolve<IMainPresenter>();
      m_MainPresenter.Execute();
      Content = m_MainPresenter.PresentedViewModel.Content;
    }

    private void InitializeComponent()
    {
      AvaloniaXamlLoader.Load(this);
    }
  }
}
