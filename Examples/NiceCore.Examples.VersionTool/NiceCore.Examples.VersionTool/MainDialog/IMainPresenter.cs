﻿using NiceCore.UI.Presenter;

namespace NiceCore.Examples.VersionTool.MainDialog
{
  /// <summary>
  /// Interface for main Presenter
  /// </summary>

  public interface IMainPresenter : IPresenter
  {
  }
}
