﻿using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace NiceCore.Examples.VersionTool.MainDialog
{
  /// <summary>
  /// Mainview
  /// </summary>
  public class MainView : UserControl
  {
    /// <summary>
    /// Default constructor
    /// </summary>
    public MainView()
    {
      this.InitializeComponent();
    }

    private void InitializeComponent()
    {
      AvaloniaXamlLoader.Load(this);
    }
  }
}
