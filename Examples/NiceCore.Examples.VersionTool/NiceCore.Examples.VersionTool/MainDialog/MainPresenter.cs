﻿using Avalonia.Controls;
using NiceCore.Examples.VersionTool.Domain;
using NiceCore.ServiceLocation;
using NiceCore.UI.Presenter;
using NiceCore.UI.Services;
using System.Collections.Generic;

namespace NiceCore.Examples.VersionTool.MainDialog
{
  /// <summary>
  /// MainPresenter
  /// </summary>
  [Register(InterfaceType = typeof(IMainPresenter))]
  public class MainPresenter : BasePresenter<MainViewModel>, IMainPresenter
  {
    /// <summary>
    /// Service for handling versions and projectInfo
    /// </summary>
    public IVersionService VersionService { get; set; }
    /// <summary>
    /// Service for object sources
    /// </summary>
    public IObjectSourceService ObjectSourceService { get; set; }
    /// <summary>
    /// Executes presenter
    /// </summary>
    public override void Execute()
    {
      base.Execute();
      Container.ResolvePropertiesForInstance(ViewModel);
      ViewModel.ProjectInfo = ObjectSourceService.GetNewCollectionSource<ProjectInfoDto>(this);
      ViewModel.ProjectInfo.RefreshCollectionMethod = ReadSolution;
    }
    /// <summary>
    /// Command impl for OpenSolutionCommand
    /// </summary>
    /// <param name="i_Args"></param>
    public void OpenSolution(object i_Args)
    {
      OpenFileDialog ofd = new OpenFileDialog();
      ofd.AllowMultiple = false;
      var openTask = ofd.ShowAsync();
      openTask.Wait();
      if (openTask.Result.Length > 0)
      {
        ViewModel.SolutionPath = openTask.Result[0];
        ViewModel.ProjectInfo.Refresh();
      }
    }
    /// <summary>
    /// Updates version of projects in Solution
    /// </summary>
    /// <param name="i_Args"></param>
    public void Update(object i_Args)
    {
      if (ViewModel.SolutionPath != null)
      {
        VersionService.UpdateVersion(ViewModel.SolutionPath, ViewModel.UpdateVersion);
        ViewModel.ProjectInfo.Refresh();
      }
    }
    private IEnumerable<ProjectInfoDto> ReadSolution()
    {
      return VersionService.GetProjectInfo(ViewModel.SolutionPath);
    }
  }
}
