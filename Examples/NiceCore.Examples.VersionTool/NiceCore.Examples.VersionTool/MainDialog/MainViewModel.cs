﻿using NiceCore.Examples.VersionTool.Domain;
using NiceCore.UI.Avalonia.ViewModel;
using NiceCore.UI.Base;
using System.Windows.Input;

namespace NiceCore.Examples.VersionTool.MainDialog
{
  /// <summary>
  /// ViewModel for main view
  /// </summary>
  public class MainViewModel : VisualViewModel<MainView>
  {
    private ICollectionSource<ProjectInfoDto> m_ProjectInfo;
    private VersionInfoDto m_UpdateVersion;
    private string m_SolutionPath;
    /// <summary>
    /// ProjectInfo found
    /// </summary>
    public ICollectionSource<ProjectInfoDto> ProjectInfo
    {
      get { return m_ProjectInfo; }
      set { SetField(ref m_ProjectInfo, value); }
    }
    /// <summary>
    /// Path to solution
    /// </summary>
    public string SolutionPath
    {
      get { return m_SolutionPath; }
      set { SetField(ref m_SolutionPath, value); }
    }
    /// <summary>
    /// Version to update to
    /// </summary>
    public VersionInfoDto UpdateVersion
    {
      get { return m_UpdateVersion; }
      set { SetField(ref m_UpdateVersion, value); }
    }
    /// <summary>
    /// Command for opening solutions
    /// </summary>
    public ICommand OpenSolutionCommand
    {
      get { return CommandService.GetOrCreate((Presenter as MainPresenter).OpenSolution); }
    }
    /// <summary>
    /// Command that updates all projects in solution
    /// </summary>
    public ICommand UpdateCommand
    {
      get { return CommandService.GetOrCreate((Presenter as MainPresenter).Update); }
    }
    /// <summary>
    /// Default constructor
    /// </summary>
    public MainViewModel()
    {
      UpdateVersion = new VersionInfoDto();
    }
  }
}
