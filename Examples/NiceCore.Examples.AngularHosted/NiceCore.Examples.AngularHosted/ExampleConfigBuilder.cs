﻿using NiceCore.Hosting.Config;
using NiceCore.ServiceLocation;
using NiceCore.Services.Config;
using NiceCore.Storage;

namespace NiceCore.Examples.AngularHosted
{
  /// <summary>
  /// ConfigBuilder for basic hosting example
  /// </summary>
  [Register(InterfaceType = typeof(IXmlConfigBuilder))]
  public class ExampleConfigBuilder : BaseXmlConfigBuilder
  {
    /// <summary>
    /// Default constructor
    /// </summary>
    public ExampleConfigBuilder()
    {
      SectionTypes.Add(typeof(IHostingConfigSection));
      SectionTypes.Add(typeof(IStorageConfigSection));
    }
  }
}
