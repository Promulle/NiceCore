﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using NiceCore.Examples.ObjectSource.MainDialog;
using NiceCore.Rapid;
using NiceCore.Rapid.Application;
using NiceCore.Rapid.Default;
using NiceCore.ServiceLocation;

namespace NiceCore.Examples.ObjectSource
{
  /// <summary>
  /// Main window class
  /// </summary>
  public class MainWindow : Window
  {
    private IServiceContainer m_Container;
    private IMainPresenter m_MainPresenter;
    private INcApplication m_App;
    /// <summary>
    /// Default constructor
    /// </summary>
    public MainWindow()
    {
      InitializeComponent();
#if DEBUG
      this.AttachDevTools();
#endif
      m_App = NcApplicationBuilder
        .Create()
        .UseDefaults()
        .Build();
      m_Container = m_App.ServiceContainer;
      m_MainPresenter = m_Container.Resolve<IMainPresenter>();
      m_MainPresenter.Execute();
      Content = m_MainPresenter.PresentedViewModel.Content;
    }

    private void InitializeComponent()
    {
      AvaloniaXamlLoader.Load(this);
    }
  }
}
