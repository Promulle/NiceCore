﻿using Avalonia;
using Avalonia.Markup.Xaml;

namespace NiceCore.Examples.ObjectSource
{
  /// <summary>
  /// Avalonia app class
  /// </summary>
  public class App : Application
  {
    /// <summary>
    /// InitApp
    /// </summary>
    public override void Initialize()
    {
      AvaloniaXamlLoader.Load(this);
    }
  }
}
