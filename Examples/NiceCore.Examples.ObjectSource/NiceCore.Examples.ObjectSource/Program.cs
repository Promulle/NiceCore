﻿using Avalonia;
using Avalonia.Logging.Serilog;

namespace NiceCore.Examples.ObjectSource
{
  /// <summary>
  /// Avalonia program
  /// </summary>
  class Program
  {
    static void Main(string[] args)
    {
      BuildAvaloniaApp().Start<MainWindow>();
    }
    /// <summary>
    /// Build avalonia app
    /// </summary>
    /// <returns></returns>
    public static AppBuilder BuildAvaloniaApp()
        => AppBuilder.Configure<App>()
            .UsePlatformDetect()
            .LogToDebug();
  }
}
