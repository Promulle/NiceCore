﻿using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace NiceCore.Examples.ObjectSource.MainDialog
{
  /// <summary>
  /// View
  /// </summary>
  public class MainView : UserControl
  {
    /// <summary>
    /// Default ctor
    /// </summary>
    public MainView()
    {
      this.InitializeComponent();
    }

    private void InitializeComponent()
    {
      AvaloniaXamlLoader.Load(this);
    }
  }
}
