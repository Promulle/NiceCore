﻿using NiceCore.UI.Presenter;

namespace NiceCore.Examples.ObjectSource.MainDialog
{
  /// <summary>
  /// Interface for main presenter
  /// </summary>
  public interface IMainPresenter : IPresenter
  {
  }
}
