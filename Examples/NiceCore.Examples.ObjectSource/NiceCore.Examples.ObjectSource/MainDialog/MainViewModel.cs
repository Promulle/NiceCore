﻿using NiceCore.UI.Avalonia.ViewModel;
using NiceCore.UI.Base;

namespace NiceCore.Examples.ObjectSource.MainDialog
{
  /// <summary>
  /// ViewModel for example
  /// </summary>
  public class MainViewModel : VisualViewModel<MainView>
  {
    private ICollectionSource<int> m_Collection;
    /// <summary>
    /// CollectionSource for view
    /// </summary>
    public ICollectionSource<int> Collection
    {
      get { return m_Collection; }
      set { SetField(ref m_Collection, value); }
    }
    /// <summary>
    /// Default constructor
    /// </summary>
    public MainViewModel()
    {
      Collection = new DefaultCollectionSource<int>();
    }
  }
}
