﻿using NiceCore.ServiceLocation;
using NiceCore.UI.Presenter;
using System;
using System.Collections.Generic;

namespace NiceCore.Examples.ObjectSource.MainDialog
{
  /// <summary>
  /// Presenter for example
  /// </summary>
  [Register(InterfaceType = typeof(IMainPresenter))]
  public class MainPresenter : BasePresenter<MainViewModel>, IMainPresenter
  {
    private Random m_Generator;
    /// <summary>
    /// Executes presenter
    /// </summary>
    public override void Execute()
    {
      base.Execute();
      ViewModel.Collection.RefreshCollectionMethod = GetNext;
      m_Generator = new Random();
    }
    /// <summary>
    /// refresh func for collection source in viewModel
    /// </summary>
    /// <returns></returns>
    private IEnumerable<int> GetNext()
    {
      var res = new List<int>();
      for (int i = 0; i < 10; i++)
      {
        res.Add(m_Generator.Next(0, 1000));
      }
      return res;
    }
  }
}
