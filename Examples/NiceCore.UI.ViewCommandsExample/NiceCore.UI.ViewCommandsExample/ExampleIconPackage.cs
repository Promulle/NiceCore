﻿using NiceCore.ServiceLocation;
using NiceCore.UI.Base;

namespace NiceCore.UI.ViewCommandsExample
{
  [Register(InterfaceType = typeof(IIconPackage))]
  public class ExampleIconPackage : BaseIconPackage
  {
    public ExampleIconPackage()
    {
      IconNames.Add("Icon_1");
      IconNames.Add("Icon_2");
      IconNames.Add("Icon_3");
      IconNames.Add("Icon_4");
    }
  }
}
