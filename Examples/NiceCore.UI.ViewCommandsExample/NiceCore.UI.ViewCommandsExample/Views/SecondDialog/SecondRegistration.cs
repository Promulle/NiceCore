﻿using NiceCore.ServiceLocation;
using NiceCore.UI.CommandRegistrations;
using NiceCore.UI.Presenter;
using NiceCore.UI.Presenter.Commands;
using NiceCore.UI.Services.Icons;

namespace NiceCore.UI.ViewCommandsExample.Views.SecondDialog
{
  [Register(InterfaceType = typeof(IViewCommandRegistration))]
  public class SecondRegistration : BasePresenterExecutionViewCommandRegistration
  {
    public IIconService IconService { get; set; }

    protected override PresenterExecutionViewCommandRequest CreateRequest(IPresenterExecutor i_Executor)
    {
      return base.CreateRequest(i_Executor)
        .WithPresenterType<SecondPresenter>()
        .WithIcon(IconService.GetIconPath("Icon_2"))
        .WithCaption("Second Dialog") as PresenterExecutionViewCommandRequest;
    }
  }
}
