﻿using NiceCore.ServiceLocation;
using NiceCore.UI.Presenter;

namespace NiceCore.UI.ViewCommandsExample.Views.InjectedDialog
{
  [Register(InterfaceType = typeof(IInjectedPresenter))]
  public class InjectedPresenter : BasePresenter<InjectedViewModel>, IInjectedPresenter
  {
    public override void Execute()
    {
      base.Execute();
    }
  }
}
