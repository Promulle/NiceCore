﻿using NiceCore.UI.Presenter;

namespace NiceCore.UI.ViewCommandsExample.Views.InjectedDialog
{
  public interface IInjectedPresenter : IPresenter
  {
  }
}