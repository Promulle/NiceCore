﻿using NiceCore.ServiceLocation;
using NiceCore.UI.CommandRegistrations;
using NiceCore.UI.Presenter;
using NiceCore.UI.Presenter.Commands;
using NiceCore.UI.Services.Icons;

namespace NiceCore.UI.ViewCommandsExample.Views.InjectedDialog
{
  [Register(InterfaceType = typeof(IViewCommandRegistration))]
  public class InjectedRegistration : BasePresenterExecutionViewCommandRegistration
  {
    public IIconService IconService { get; set; }

    public IServiceContainer Container { get; set; }

    protected override PresenterExecutionViewCommandRequest CreateRequest(IPresenterExecutor i_Executor)
    {
      return base.CreateRequest(i_Executor)
        .WithInjectedPresenter<IInjectedPresenter>(Container)
        .WithCaption("Injected Dialog")
        .WithIcon(IconService.GetIconPath("Icon_3"))
        .AsPresenterExecutionRequest();
    }
  }
}
