﻿using NiceCore.ServiceLocation;
using NiceCore.UI.CommandRegistrations;
using NiceCore.UI.Presenter;
using NiceCore.UI.Presenter.Commands;
using NiceCore.UI.Services.Icons;

namespace NiceCore.UI.ViewCommandsExample.Views.CommandExamples
{
  [Register(InterfaceType = typeof(IViewCommandRegistration))]
  public class CommandExampleRegistration : BasePresenterExecutionViewCommandRegistration
  {
    public IIconService IconService { get; set; }

    public IServiceContainer Container { get; set; }

    protected override PresenterExecutionViewCommandRequest CreateRequest(IPresenterExecutor i_Executor)
    {
      return base.CreateRequest(i_Executor)
        .WithInjectedPresenter<ICommandsExamplePresenter>(Container)
        .WithIcon(IconService.GetIconPath("Icon_1"))
        .WithCaption("First Dialog") as PresenterExecutionViewCommandRequest;
    }
  }
}
