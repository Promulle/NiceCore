﻿using NiceCore.ServiceLocation;
using NiceCore.UI.Presenter;
using NiceCore.UI.ViewCommands;
using NiceCore.UI.ViewCommandsExample.Views.HostedDialog;
using NiceCore.UI.WindowHosting;
using System.Windows;

namespace NiceCore.UI.ViewCommandsExample.Views.CommandExamples
{
  [Register(InterfaceType = typeof(ICommandsExamplePresenter))]
  public class CommandsExamplePresenter : BasePresenter<CommandsExampleViewModel>, ICommandsExamplePresenter
  {
    private IViewCommand m_ContextCommand;
    private IViewCommand m_CloseCommand;
    private IViewCommand m_ShowCommandInWindowCommand;

    public IWindowHostService WindowHostService { get; set; }

    public override void Execute()
    {
      base.Execute();
      m_ContextCommand = ViewCommand.New(ShowWindow, _ => true)
        .WithCaption("Show Window")
        .WithIcon(IconService.GetIconPath("Icon_3"))
        .ToCommand();

      m_CloseCommand = ViewCommand.New(Close, _ => true)
        .WithCaption("Close")
        .ToCommand();

      m_ShowCommandInWindowCommand = ViewCommand.New(ShowInWindowCommand, _ => true)
        .WithCaption("Show Presenter in Window")
        .ToCommand();

      PresenterContextCommandService.AddCommand(this, m_ContextCommand);
      PresenterContextCommandService.AddCommand(this, m_CloseCommand);
      PresenterContextCommandService.AddCommand(this, m_ShowCommandInWindowCommand);
    }

    private async void ShowInWindowCommand(object i_Args)
    {
      var presenter = new HostedPresenter();
      presenter.Execute();
      var window = WindowHostService.HostPresenterInWindow(presenter);
      await window.ShowAsync().ConfigureAwait(false);
      presenter.Dispose();
    }

    private void Close(object i_Args)
    {
      Dispose();
    }

    private void ShowWindow(object i_Args)
    {
      MessageBox.Show("Hello!");
    }
  }
}
