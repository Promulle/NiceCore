﻿using NiceCore.UI.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NiceCore.UI.ViewCommandsExample.Views.Start
{
    public interface IExampleStartViewPresenter : IStartViewPresenter
    {
    }
}
