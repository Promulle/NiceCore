﻿using NiceCore.UI.Base;
using NiceCore.UI.Presenter.StartViews;
using NiceCore.UI.ViewCommands;
using System;
using System.Linq;

namespace NiceCore.UI.ViewCommandsExample.Views.Start
{
  /// <summary>
  /// CommandModel
  /// </summary>
  public class ExampleCommandModel : ICommandModel
  {
    /// <summary>
    /// Groupings
    /// </summary>
    public ICollectionSource<IViewCommandGrouping> Groupings { get; set; }

    /// <summary>
    /// Add Command
    /// </summary>
    /// <param name="i_Command"></param>
    /// <returns></returns>
    public bool AddCommand(IViewCommand i_Command)
    {
      if (Groupings.Items.Count == 0)
      {
        CreateDefaultGrouping();
      }
      var grouping = SelectGrouping(i_Command);
      if (!grouping.Commands.Any(r => r.Identifier == i_Command.Identifier))
      {
        grouping.Commands.Add(i_Command);
        i_Command.Grouping = new ViewCommandGroupingInfo(null, grouping);
        return true;
      }
      return false;
    }

    private IViewCommandGrouping SelectGrouping(IViewCommand i_Command)
    {
      if (i_Command.Parent == null)
      {
        return Groupings.Items[0];
      }
      CreateContextGroupingIfNeeded();
      return Groupings.Items[1];
    }

    private void CreateDefaultGrouping()
    {
      Groupings.CollectionAdd(new DefaultViewCommandGrouping());
    }

    private void CreateContextGroupingIfNeeded()
    {
      if (Groupings.Items.Count == 1)
      {
        Groupings.CollectionAdd(new DefaultViewCommandGrouping()
        {
          Name = "Context",
        });
      }
    }

    private void RemoveContextGroupingIfNeeded()
    {
      if (Groupings.Items[1].Commands.Count == 0)
      {
        Groupings.CollectionRemove(Groupings.Items[1]);
      }
    }

    /// <summary>
    /// Remove command
    /// </summary>
    /// <param name="i_Identifier"></param>
    /// <returns></returns>
    public bool RemoveCommand(Guid i_Identifier)
    {
      var command = Groupings.Items.SelectMany(r => r.Commands).FirstOrDefault(r => r.Identifier == i_Identifier);
      if (command != null)
      {
        command.Grouping.Actual.Commands.Remove(command);
        command.Grouping = null;
        RemoveContextGroupingIfNeeded();
        return true;
      }
      return false;
    }
  }
}
