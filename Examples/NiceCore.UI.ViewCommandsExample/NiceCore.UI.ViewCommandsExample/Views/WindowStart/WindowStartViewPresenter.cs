﻿using NiceCore.ServiceLocation;
using NiceCore.UI.Presenter;
using NiceCore.UI.Presenter.Execution;
using NiceCore.UI.Presenter.StartViews;
using NiceCore.UI.ViewCommands;
using NiceCore.UI.ViewCommandsExample.Views.Start;
using System;

namespace NiceCore.UI.ViewCommandsExample.Views.WindowStart
{
  [Register(InterfaceType = typeof(IWindowStartViewPresenter))]
  public class WindowStartViewPresenter : BaseStartViewWithInjectedCommandsPresenter<WindowStartViewViewModel>, IWindowStartViewPresenter
  {
    private IPresenter m_CurrentDisplayed;

    protected override ICommandModel CreateCommandModel()
    {
      return new ExampleCommandModel
      {
        Groupings = ObjectSourceService.GetNewCollectionSource<IViewCommandGrouping>(this)
      };
    }

    protected override IPresenterExecutor CreatePresenterExecutor()
    {
      return new DefaultPresenterExecutor();
    }

    protected override void PrepareStartView()
    {
      base.PrepareStartView();
      PresenterExecutor.PresenterExecuted += OnNewPresenterExecuted;
    }

    private void OnNewPresenterExecuted(object sender, IPresenter e)
    {
      m_CurrentDisplayed?.Dispose();
      m_CurrentDisplayed = e;
      m_CurrentDisplayed.OnDisposeStart += OnCurrentPresenterDisposing;
      ViewModel.ControlContent = e.PresentedViewModel.Content;
    }

    private void OnCurrentPresenterDisposing(object sender, EventArgs e)
    {
      m_CurrentDisplayed.OnDisposeStart -= OnCurrentPresenterDisposing;
      m_CurrentDisplayed = null;
      ViewModel.ControlContent = null;
    }

    protected override void Dispose(bool i_IsDisposing)
    {
      PresenterExecutor.PresenterExecuted -= OnNewPresenterExecuted;
      base.Dispose(i_IsDisposing);
    }

    protected override string GetStartViewTitle()
    {
      return "Started: " + DateTime.Now.ToShortTimeString();
    }
  }
}
