﻿using NiceCore.UI.ViewModels.StartViews;

namespace NiceCore.UI.ViewCommandsExample.Views.WindowStart
{
  public class WindowStartViewViewModel : BaseStartViewWithCommandsViewModel
  {
    private object m_Content;
    private object m_ControlContent;

    public override object Content
    {
      get { return m_Content; }
      set { SetField(ref m_Content, value); }
    }

    public object ControlContent
    {
      get { return m_ControlContent; }
      set { SetField(ref m_ControlContent, value); }
    }

    public override void Initialize()
    {
      Content = new WindowStartViewWindow()
      {
        DataContext = this,
      };
    }
  }
}
