﻿using NiceCore.UI.Presenter;

namespace NiceCore.UI.ViewCommandsExample.Views.WindowStart
{
  public interface IWindowStartViewPresenter : IStartViewPresenter
  {
  }
}
