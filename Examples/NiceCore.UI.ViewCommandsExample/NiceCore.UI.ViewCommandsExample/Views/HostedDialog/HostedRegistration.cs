﻿using NiceCore.ServiceLocation;
using NiceCore.UI.CommandRegistrations;
using NiceCore.UI.Presenter;
using NiceCore.UI.Presenter.Commands;
using NiceCore.UI.Services.Icons;
using NiceCore.UI.WindowHosting;
using NiceCore.UI.WindowHosting.Configuration;

namespace NiceCore.UI.ViewCommandsExample.Views.HostedDialog
{
  [Register(InterfaceType = typeof(IViewCommandRegistration))]
  public class HostedRegistration : BasePresenterExecutionViewCommandRegistration
  {
    public IIconService IconService { get; set; }

    public IWindowHostService WindowHostService { get; set; }

    protected override PresenterExecutionViewCommandRequest CreateRequest(IPresenterExecutor i_Executor)
    {
      return base.CreateRequest(i_Executor)
        .WithPresenterType<HostedPresenter>()
        .HostInWindow(WindowHostService, WindowShowStrategy.NonBlocking, GetWindowConfig())
        .WithCaption("Hosted Dialog")
        .WithIcon(IconService.GetIconPath("Icon_4"))
        .AsPresenterExecutionRequest();
    }

    private IWindowConfiguration GetWindowConfig()
    {
      return new DefaultWindowConfigurationRequest()
        .Titled("Hosted Dialog")
        .ToConfiguration();
    }
  }
}
