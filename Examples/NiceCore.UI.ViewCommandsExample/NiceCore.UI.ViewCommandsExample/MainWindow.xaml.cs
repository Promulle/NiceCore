﻿using NiceCore.Rapid;
using NiceCore.Rapid.Default;
using NiceCore.Rapid.UI;
using NiceCore.UI.ViewCommandsExample.Views.Start;
using System.ComponentModel;
using System.Windows;

namespace NiceCore.UI.ViewCommandsExample
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    private readonly INcUIApplication m_App;
    private bool m_IsForeignClosing;

    public MainWindow()
    {
      InitializeComponent();
      m_App = NcApplicationBuilder.Create<NcUIApplication>()
        .UseDefaults()
                                                                                                                                        //.StartView().ForInjectedPresenter<IWindowStartViewPresenter>()  // host IWindowStartViewPresenter in the window instance it will create on its own
                                                                                                                                        .StartView().FromWindowInjected<IExampleStartViewPresenter>(this) // host IExampleStartViewPresenter in this WindowInstance
        .StartView().AddCloseHandler(() =>
        {
          m_IsForeignClosing = true;
          Close();
        })
        .Build();

      m_App.Run();
      //Hide();
    }

    private void Window_Closing(object sender, CancelEventArgs e)
    {
      if (!m_IsForeignClosing)
      {
        m_App.Shutdown();
      }
    }
  }
}
