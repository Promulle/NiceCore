This repository uses third-party nuget packages which can be licensed differently than the repository itself.
This folder contains all licenses/notices for third-party projects used by this repository.
If I have missed any or something does not comply to a used librarie's license, 
please feel free to open an issue about it or contact this email: promulle1337@gmail.com.
These issues will be fixed as fast as possible.
NiceCore is in now way affiliated with any third-party dependency.
AutoMapper - https://github.com/AutoMapper/AutoMapper
fluent-nhibernate - https://github.com/nhibernate/fluent-nhibernate
Newtonsoft.Json - https://github.com/JamesNK/Newtonsoft.Json
NHibernate - https://github.com/nhibernate
NHibernate-Envers - https://github.com/nhibernate/nhibernate-envers
Serilog - https://github.com/serilog/serilog
Log4Net - https://github.com/apache/logging-log4net
RabbitMQ - https://github.com/rabbitmq/rabbitmq-dotnet-client
protobuf-net - https://github.com/protobuf-net/protobuf-net
System.IdentityModel.Tokens.JWT - https://github.com/AzureAD/azure-activedirectory-identitymodel-extensions-for-dotnet/blob/dev/LICENSE.txt
FluentValidation - https://github.com/FluentValidation/FluentValidation
BenchmarkDotNet - https://github.com/dotnet/BenchmarkDotNet
AutoBogus - https://github.com/nickdodd79/AutoBogus
Polly - https://github.com/App-vNext/Polly
Castle.Windsor - https://github.com/castleproject/Windsor
Swashbuckle.AspNetCore - https://github.com/domaindrivendev/Swashbuckle.AspNetCore
Castle.Windsor.MsDependencyInjection - https://github.com/volosoft/castle-windsor-ms-adapter
Cronos - https://github.com/HangfireIO/Cronos