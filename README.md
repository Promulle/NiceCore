NiceCore is a C# mvvm-focused application development library based on .NET Core.  
This library is a very early state but will be improved over time.  
Feel free to add suggestions or Corrections.  
Currently this library runs on .Net5

To develop using this library and Visual Studio you will need Visual Studio 2019 or newer with the workload Net 5 installed.
To develop gui applications with this library and avalonia it is very much recommended to install the Avalonia Visual Studio plugin.
For more information regarding Avalonia see: https://github.com/AvaloniaUI/Avalonia and http://avaloniaui.net/docs/quickstart/  
For examples on how to build an application with this library and avalonia you should take a look at the Examples folder and the projects in it.

All dependencies that are provided, are provided via Nuget. Licenses of each apply (see ThirdPartyLicenses directory).
NiceCore is in no way affiliated with any of those.
Any link/resource the source code or it's documentation contains are provided for further information only. I do not have any control about the contents of those 
and will therefore not take any responsibility for anything that may arise from your usage of them.
    
Running Unit-Tests:
Using the NiceUnitTesting library I wrote you can directly debug all Unit test projects. Just execute the project you want to debug.

Unless specifically stated otherwise, contributions that are intentionally made to this repository are licensed under the MIT License.
If your contribution is not licensed under MIT I might not accept it.